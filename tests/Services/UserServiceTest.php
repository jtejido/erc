<?php

use App\Models\ActingOriginatingAgent;
use App\Models\ClassRegistration;
use App\Models\Contact;
use App\Models\User;
use App\Utilities\Constant;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as FakerFactory;

class UserServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function setup()
    {
        parent::setup();

        $this->services['user'] = \App::make(App\Services\UserService::class);
        $this->services['registration'] = \App::make(App\Services\RegistrationService::class);
        $this->services['contact']      = \App::make(App\Services\ContactService::class);
    }

    /**
     * @test
     */
    public function givenValidUserDataShouldSucceedWithUserLinkedWithContact()
    {

        $faker = FakerFactory::create();

        $fname = $faker->firstName;

        $user = [
            'first_name' => $fname,
            'last_name' => $faker->lastName,
            'company'   => $faker->company,
            'title' => $faker->sentence(),
            'address1'  => $faker->address,
            'address2'  => $faker->streetAddress,
            'city'  => $faker->city,
            'state' => $faker->state,
            'zip'  => $faker->postcode,
            'phone' => $faker->phoneNumber,
            'fax'   => $faker->phoneNumber,
            'email' => $faker->email
        ];

        $response = $this->services['user']->save($user);

        $this->assertTrue(($response instanceof App\Models\User));
        $this->assertEquals($response->contact->first_name, $fname);
    }

    /**
     * @test
     */
    public function saveContactOfClassAsActingAgentOfRegistrationOrder()
    {
        $user = factory(App\Models\User::class)->create();
        $agent = factory(App\Models\User::class)->create();
        $this->be($user);

        $class = $this->registerUserToClass($user, $agent);

        $agent_order = ActingOriginatingAgent::where('acting_agent_id', $user->id)->first();
        
        $response = $this->services['user']->saveAsAgent($user);

        $this->assertEquals($class->registrations->first()->id, $agent_order->orderable_id);
    }

    /**
     * @test
     */
    public function saveAgentAsAgentOfClass()
    {
        $user = factory(App\Models\User::class)->create();
        $this->be($user);

        $class = $this->registerUserToClass($user, $user);

        $agent_order = ActingOriginatingAgent::where('acting_agent_id', $user->id)->count();
        
        $response = $this->services['user']->saveAsAgent($user);

        $this->assertTrue(!$agent_order);
    }


    private function registerUserToClass (User $user, User $agent)
    {
        
        $course = App\Models\Course::whereIn('course_type_id', [1,2])->get()->random();
        $class = $course->classes()->active()->get()->random()->first();

        $this->services['registration']->createRegistration([
            'agent'             => $agent->id,
            'contacts'          => [$user->contact->id],
            'registrationId'    => $class->id,
            'registrationType'  => Constant::COURSE_CLASS_OBJECT,
            'status'            => Constant::PENDING
        ]);

        return $class;
    }
}
