<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Contact;
use App\Models\ClassRegistration;
use App\Models\Order;
use App\Utilities\Constant;
use Carbon\Carbon;

class PaymentServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function setup() {
        parent::setup();
        $this->services['registration'] = \App::make(App\Services\RegistrationService::class);
        $this->services['contact']      = \App::make(App\Services\ContactService::class);
        $this->services['payment']      = \App::make(App\Services\PaymentService::class);
    }

    /**
     * @atest
     * Checkout with valid Credit Card data
     */
    public function givenValidCreditCardDataUponCheckoutShouldSucceed()
    {

        $user = factory(App\Models\User::class)->create();
        $this->be($user);
        $user->contact()->associate(factory(App\Models\Contact::class)->create())->save();
        
        $course = App\Models\Course::whereIn('course_type_id', [1,2])->get()->random();
        \Log::info('Course Selected: ['.$course->id.'] '.$course->title);
        $class = App\Models\CourseClass::where('course_id', $course->id)->get()->random();
        // \Log::info($course->classes()->active()->get()->toArray());
        // $class = $course->classes()->active()->get()->random()->first();
        \Log::info('Class Selected: ['.$class->id.'] '.$course->instructor_name);
        
        $registrableId   = in_array($course->course_type_id, [1,2]) ? $course : $class;
        $registrableType = in_array($course->course_type_id, [1,2]) ? Constant::COURSE_OBJECT : Constant::COURSE_CLASS_OBJECT;
        $this->services['registration']->createRegistration([
            'agent'             => $user->id,
            'contacts'          => [$user->contact->id],
            'registrationId'    => $registrationId,
            'registrationType'  => $registrableType,
            'status'            => Constant::PENDING
        ]);


        $order = Order::first();

        $payment = [
            'card_number' => '4007000000027',
            'cvv'         => rand(100, 999),
            'user_id'     => $user->id,
            'year'        => Carbon::now()->addYears(rand(2,4))->year,
            'month'       => rand(1,12),
            'item'        => $order->items()->lists('id'),
            'order_id'    => $order->id,
            'description' => 'Payment of Order',
            'email'       => $user->email,
            'first_name'  => $user->contact->first_name,
            'last_name'   => $user->contact->last_name,
            'zip'         => $user->contact->zip,
            'amount'      => $order->items()->sum('price_charged'),
            'address'     => str_replace("\n", '', $user->contact->address),
        ];
        $payment['card_exp'] = $payment['year'] . '-' . $payment['month'];

        $transaction = $this->services['payment']->doPayment($payment);
        // Log::info($this->emails);
        $this->assertEquals(App\Models\Order::first()->status, Constant::COMPLETE);
    }

    /**
     * @atest
     */
    public function givenAnOrderWithAnAdditionalAttendeeShouldShowAttendees()
    {
        $user = User::whereEmail('user@ercweb.com')->first();
        $user->contact()->associate(factory(App\Models\Contact::class)->create())->save();
        $this->be($user);
        $this->makeRegistration($user, 1);
        

        $order = Order::first();
        $this->makePayment($user, $order);
        
        $this->assertEquals(App\Models\Order::first()->status, Constant::COMPLETE);
    }

    /**
     * @test
     */
    public function givenPreviousOrderShouldReflectCurrentOrderInEmail()
    {
        $user = User::whereEmail('user@ercweb.com')->first();
        $user->contact()->associate(factory(App\Models\Contact::class)->create());
        $user->save();
        $this->be($user);
        $available = App\Models\Course::whereIn('course_type_id', [1,2])->lists('id')->toArray();
        $course = App\Models\Course::find($available[array_rand($available)]);
        \Log::info('Course Selected: ['.$course->id.'] '.$course->title);
        $class = App\Models\CourseClass::where('course_id', $course->id)->get()->random();

        \Log::info('Class Selected: ['.$class->id.'] '.$course->instructor_name);
        $this->makeRegistration($user, $class, 1);
        
        $order = Order::first();
        $this->makePayment($user, $order);
        
        $user = factory(App\Models\User::class)->create();
        $user->contact()->associate(factory(App\Models\Contact::class)->create());
        $user->save();
        $this->be($user);
        $course = App\Models\Course::find($available[array_rand($available)]);
        \Log::info('Course Selected: ['.$course->id.'] '.$course->title);
        $class = App\Models\CourseClass::where('course_id', $course->id)->get()->random();
        \Log::info('Class Selected: ['.$class->id.'] '.$course->instructor_name);
        $this->makeRegistration($user, $class);
        $order = Order::last();
        $this->makePayment($user, $order);

    }

    /**
     * Make a registration
     * @param  App\Models\User $user
     * @param  int $additional_attendees
     */
    public function makeRegistration($user, $class, $additional_attendees = 0) {
        
        $default_addition = range(1,$additional_attendees);
        if ($additional_attendees === 1) {
            $default_addition = [1];
        }

        $contact_ids = [];
        if ($additional_attendees > 0) {
            foreach ($default_addition as $addition) {

                $registered_attendee = factory(App\Models\Contact::class,'email')->make()->toArray();
                // Log::info($user->toArray());
                // Log::info($user->contact->toArray());
                // Log::info('Authorized User is '.$user->contact->name);
                // Log::info($registered_attendee);

                
                
                xdebug_break();
                $contact = $this->services['contact']->saveContact($registered_attendee);
                $contact_ids[] = $contact->id;
            }
        }

        $this->services['registration']->createRegistration([
            'agent'             => $user->id,
            'contacts'          => array_merge([$user->contact->id], $contact_ids),
            'registrationId'    => $class->id,
            'registrationType'  => Constant::COURSE_CLASS_OBJECT,
            'status'            => Constant::PENDING
        ]);
    }

    public function makePayment($user, $order) {
        $payment = [
            'card_number' => '4007000000027',
            'cvv'         => rand(100, 999),
            'user_id'     => $user->id,
            'year'        => Carbon::now()->addYears(rand(2,4))->year,
            'month'       => rand(1,12),
            'item'        => $order->items()->lists('id'),
            'order_id'    => $order->id,
            'description' => 'Payment of Order',
            'email'       => $user->email,
            'first_name'  => $user->contact->first_name,
            'last_name'   => $user->contact->last_name,
            'zip'         => $user->contact->zip,
            'amount'      => $order->items()->sum('price_charged'),
            'address'     => str_replace("\n", '', $user->contact->address),
        ];
        $payment['card_exp'] = $payment['year'] . '-' . $payment['month'];

        $transaction = $this->services['payment']->doPayment($payment);
    }
}
