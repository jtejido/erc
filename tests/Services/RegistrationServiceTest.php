<?php

use App\Models\ActingOriginatingAgent;
use App\Models\ClassRegistration;
use App\Models\Contact;
use App\Models\User;
use App\Utilities\Constant;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RegistrationServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function setup() {
        parent::setup();
        $this->services['registration'] = \App::make(App\Services\RegistrationService::class);
        $this->services['contact']      = \App::make(App\Services\ContactService::class);
    }

    /*
     * @test
     */
    public function testGivenValidDataShouldRegister()
    {
        $user = factory(App\Models\User::class)->create();
        $user->contact()->associate(factory(App\Models\Contact::class)->create())->save();
        $this->be($user);
        
        $course = App\Models\Course::whereIn('course_type_id', [1,2])->get()->random();
        $class = $course->classes()->active()->get()->random()->first();

        $this->services['registration']->createRegistration([
            'agent'             => $user->id,
            'contacts'          => [$user->contact->id],
            'registrationId'    => $class->id,
            'registrationType'  => Constant::COURSE_CLASS_OBJECT,
            'status'            => Constant::PENDING
        ]);
        
        $this->assertEquals(ClassRegistration::count(), 1);
        $this->assertEquals(ActingOriginatingAgent::count(), 0);
    }

    /**
     * @test
     */
    public function givenAnAttendeeIsRegisteredShouldReflectOriginatingAgent()
    {
        $user = factory(App\Models\User::class)->create();
        $user->contact()->associate(factory(App\Models\Contact::class)->create())->save();
        $this->be($user);
        
        $registered_attendee = factory(App\Models\Contact::class,'email')->make()->toArray();
        Log::info($user);
        Log::info($user->contact);
        Log::info('Authorized User is '.$user->contact->name);
        Log::info($registered_attendee);

        $course = App\Models\Course::whereIn('course_type_id', [1,2])->get()->random();
        $class = $course->classes()->active()->get()->random()->first();

        $contact = $this->services['contact']->saveContact($registered_attendee);
        $this->services['registration']->createRegistration([
            'agent'             => $user->id,
            'contacts'          => [$user->contact->id, $contact->id],
            'registrationId'    => $class->id,
            'registrationType'  => Constant::COURSE_CLASS_OBJECT,
            'status'            => Constant::PENDING
        ]);
        
        $this->assertEquals(ClassRegistration::count(), 2);
        $this->assertEquals(ActingOriginatingAgent::count(), 1);
    }
}
