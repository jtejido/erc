<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Faker\Factory as FakerFactory;

class ContactServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function setup()
    {
        parent::setup();

        $this->services['contact'] = \App::make(App\Services\ContactService::class);
    }

    /**
     * @test
     */
    public function givenValidContactDataButWithoutEmailShouldSucceed()
    {
        $user = factory(App\Models\User::class)->create();
        $member = factory(App\Models\User::class)->create();
        $user->roles()->create([
            'role'=>'1'
        ]);
        $member->roles()->create([
            'role'=>'2'
        ]);
        $this->be($user);

        $faker = FakerFactory::create();

        $contact = [
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'company'   => $faker->company,
            'title' => $faker->sentence(),
            'address1'  => $faker->address,
            'address2'  => $faker->streetAddress,
            'city'  => $faker->city,
            'state' => $faker->state,
            'zip'  => $faker->postcode,
            'phone' => $faker->phoneNumber,
            'fax'   => $faker->phoneNumber,
            'user_id'   => $member->id
        ];

        $this->assertEquals(0, $member->addressBooks()->count());

        $responseArray = $this->services['contact']->saveContact($contact);

        $this->assertEquals(1, $member->addressBooks()->count());

        $this->assertTrue($responseArray['response']);
    }

    /**
     * @test
     */
    public function givenValidEmailWithExistingAccountShouldSucceed()
    {
        $user = factory(App\Models\User::class)->create();

        $this->be($user);

        $existingUser = \App\Models\User::whereHas('roles', function($query) {
            $query->where('roles.id', '=', \App\Utilities\Constant::MEMBER);
        })->first();

        $responseArray = $this->services['contact']->saveContact(['match_user_id' => $existingUser->id]);

        $this->assertTrue($responseArray['response']);
    }
}
