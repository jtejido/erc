<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Contact;
use App\Models\ClassRegistration;
use App\Utilities\Constant;

class PlaceholderServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function setup() {
        parent::setup();
        $this->services['placeholder'] = \App::make(App\Services\Mailer\PlaceholderService::class);
    }

    private function getTemplate($name = 'variables')
    {
        $templates      = base_path('resources/views/emails').'/';
        $css       = file_get_contents($templates.'/templates/zurb-foundation/foundation-live.css');
        $content   = file_get_contents($templates.'test/'.$name.'.html');

        $css       = "<style>{$css}.</style>";
        $content   = str_replace('<!-- <style> -->', $css, $content);
        $email_tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';

        $content   = $email_tag."\n".$content;
        return $content;
    }


    /**
     * @test
     */
    public function templateShouldRemoveTagSpacesAndOnlyLeaveOneNewline()
    {
        $tagsWithSpaces = "<strong>something</strong>    \n\n<strong>else</strong> <strong>yeah</strong>";
        $expected = "<strong>something</strong>\n<strong>else</strong>\n<strong>yeah</strong>";

        $this->assertEquals($expected, $this->services['placeholder']->removeTagSpaces($tagsWithSpaces));

    }

    /**
     * @atest
     * 
     * Given valid single character it should replace with correct variable
     */
    public function givenValidDataShouldRegister()
    {
        
        $output = $this->services['placeholder']->parse($this->getTemplate());

        $this->assertTrue(str_contains($output, '{{ $a }}'));
    }

    /**
     * @atest
     * 
     * Plural shortcode should be parsed first before processing non-plural codes
     */
    public function givenPluralShortcodeShouldRender()
    {
        
        $output = $this->services['placeholder']->shortcodify($this->getTemplate());
        $this->assertTrue(str_contains($output, '@foreach ($classes as $class)'));
        $this->assertTrue(str_contains($output, '@endforeach'));
    }

    /**
     * @test
     * 
     * Variables in plural shortcode content should be rendered
     */
    public function givenNestedShortcodesUnderPluralShouldRender()
    {
        $output = $this->services['placeholder']->shortcodify($this->getTemplate());

        $this->assertTrue(str_contains($output, '{{ $class->name }}'));
        $this->assertTrue(str_contains($output, '{{ $class->start_date }}'));
        $this->assertTrue(str_contains($output, '{{ $class->instructor }}'));
    }

    /**
     * @test
     * 
     * Simple shortcodes should be rendered normally
     */
    public function givenSimpleShortcodesShouldRenderProperly()
    {
        $output = $this->services['placeholder']->shortcodify($this->getTemplate());

        $this->assertTrue(str_contains($output, '{{ $a }}'));
        $this->assertTrue(str_contains($output, '{{ $bb }}'));
        $this->assertTrue(str_contains($output, '{{ $name }}'));
        $this->assertTrue(str_contains($output, '{{ $first_name }}'));
        $this->assertTrue(str_contains($output, '{{ $last_name }}'));
        $this->assertTrue(str_contains($output, '{{ $city }}'));
    }


    /**
     * @test
     * 
     * `user` shortcode will be forced to be rendered as $users
     */
    public function givenSimpleObjectWillStillBeConsideredAsArray()
    {
        $output = $this->services['placeholder']->shortcodify($this->getTemplate());
        $this->assertTrue(str_contains($output, '@if (!empty($users)'));
        $this->assertTrue(str_contains($output, '@foreach ($users as $user)'));
    }


    /**
     * @test
     * 
     * All variables in template should be properly replaced
     * with valid values
     */
    public function givenValidReplacementDataShouldRender()
    {
        $link = 'http://www.google.com';
        $img_link = 'http://www.google.com/testimagelink';
        $given = [
            'a'          => 'a',
            'bb'         => 'bb',
            'name'       => 'name',
            'first_name' => 'first_name',
            'last_name'  => 'last_name',
            'city'       => 'city',
            'test_link' => $link,
            'book_imglink'    => $img_link,
            'users'      => [
                (object) [
                    'name' => 'User Name'
                ],
            ],
            'classes'    => [
                (object) [
                    'name'       => 'Class Name',
                    'start_date' => 'Start Date',
                    'instructor' => 'Instructor',
                    'category'   => 'Category Name',
                    'type'       => 'class',
                ]
            ],
        ];

        
        $output = $this->services['placeholder']->parse($this->getTemplate(), $given);

        $this->assertTrue(str_contains($output, '<p class="single">a</p>'));
        $this->assertTrue(str_contains($output, '<p class="two-characters">bb</p>'));
        $this->assertTrue(str_contains($output, '<p class="basic-variable">name</p>'));
        $this->assertTrue(str_contains($output, '<p class="underscored">first_name</p>'));
        $this->assertTrue(str_contains($output, '<p class="underscored-with-spaces">last_name</p>'));
        $this->assertTrue(str_contains($output, '<p class="object-replacement">Hi User Name</p>'));
        $this->assertTrue(str_contains($output, '<li>Class Name on Start Date by Instructor</li>'));
        $this->assertTrue(str_contains($output, '<a href="http://www.google.com">http://www.google.com</a>'));
        $this->assertTrue(str_contains($output, '<img src="'.$img_link.'" height="100%" width="100%" />'));
    }

    /**
     * @test
     * 
     * if statement should render properly
     * for both value checking 'if' statement,
     * and 'if' statement with a present (not-null) value
     */
    public function givenClassAndOnlineTrainingShouldRenderIfStatementProperly()
    {        
        $output = $this->services['placeholder']->shortcodify($this->getTemplate('if_conditions'));
        
        $this->assertTrue(str_contains($output, '@if ($class->type == "online")'));
        $this->assertTrue(str_contains($output, "@endif"));
        $this->assertTrue(str_contains($output, "@foreach (\$class->attendees as \$attendee)"));
    }


    /**
     * @test
     */
    public function givenClassAndOnlineTrainingShouldRenderOutputProperly()
    {
        $ol_course = App\Models\Course::find(21);

        $email = str_random(10).'@'.str_random(4).'.com';
        $given = [
            'a'          => 'a',
            'bb'         => 'bb',
            'name'       => 'name',
            'first_name' => 'first_name',
            'last_name'  => 'last_name',
            'city'       => 'city',
            'users'      => [
                (object) ['name' => 'User Name']
            ],
            'hasClass'      => true,
            'classes'    => [
                (object) [
                    'name'       => 'Class Name',
                    'start_date' => 'Start Date',
                    'instructor' => 'Instructor',
                    'category'   => 'Class Category',
                    'type'       => 'class',
                    'hasAttendees' => true,
                    'attendees'  => [
                        (object) [
                            'name'  => 'Attendee Name with Email',
                            'email' => $email,
                        ],
                        (object) [
                            'name'  => 'Attendee Name without Email',
                            'email' => ''
                        ],
                    ],
                    'courses' => [
                        (object) [
                            'title' => 'Course Title',
                            'course_link'      => 'http://ercweb.com'
                        ]
                    ]
                ],
                (object) [
                    'name'        => $ol_course->title,
                    'sku'         => $ol_course->sku,
                    'duration'    => $ol_course->duration,
                    'category'    => $ol_course->courseCategories->first()->category->name,
                    'type'        => 'online',
                    'hasAttendees' => false,
                    'attendees'   => [
                        // no attendees
                    ],
                    'courses'   => []
                ],
            ],
            'payment' => [
            ]
        ];

        
        $output = $this->services['placeholder']->parse($this->getTemplate('if_conditions'), $given);

        $this->assertTrue(str_contains($output, '<p>Welcome</p>'));
        $this->assertTrue(str_contains($output, '<li>Attendee Name with Email '.$email.'</li>'));
        $this->assertTrue(str_contains($output, '<li>Attendee Name without Email (No Email)</li>'));
        $this->assertTrue(str_contains($output, '<li>Hazardous Materials Transportation &gt; How to Ship Dry Ice by Ground and Air - Online Training (Self Paced) </li>'));
        $this->assertTrue(str_contains($output, '<li>Course Title - <a href="http://ercweb.com">http://ercweb.com</a></li>'));

    }

}
