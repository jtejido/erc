<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function setup() {
        parent::setup();
    }

    public function testBasicExample()
    {
        $users      = App\Models\User::all();
        $management = App\Models\User::management();

        // check if all users are present from the seed
        $this->assertEquals(6, $users->count());

        // check if only mamangement is found
        $this->assertEquals(3, $management->count());
        $this->assertEquals([
            'erc_admin@ercweb.com',
            'admin@ercweb.com',
            'csr@ercweb.com',
        ], $management->lists('email')->toArray());
    }
}
