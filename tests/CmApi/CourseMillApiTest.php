<?php

use App\Repositories\ApiModels\CmStudent;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repositories\ApiRequest\GetStudents;
use App\Repositories\Auth\BasicAuth;
use Illuminate\Support\Collection;
use \Mockery as m;
use Carbon\Carbon;

class CourseMillApiTest extends TestCase
{

    public $faker;
    public $collection;
    public $basicAuth;

    /**
     * CourseMillApiTest constructor.
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Faker\Factory::create();
        $this->basicAuth = new BasicAuth();
        $this->collection = new Collection();
    }


    public function testCreateUpdateAndGetStudent()
    {

        $req = new \App\Repositories\ApiRequest\CreateUpdateStudents($this->basicAuth, $this->collection);
        $student = new CmStudent('cmtest', 'fname','lname');

        // Create new Student
        $response = $req->create($student);
        $this->assertEquals(1, $response->created);
        $this->assertEquals(0, $response->updated);

        $id = $response->ids[0]->id;

        // Update same Student
        $student = new CmStudent('cmtest', 'fname','lname');
        $student->setEmail('test@yahoo.com');
        $student->setAddress("address");
        $student->setCity("Chicago");
        $student->setState("Alaska");
        $student->setZip("11888");
        $response = $req->update($id, $student);
        $this->assertEquals(1, $response->updated);
        $this->assertEquals(0, $response->created);

        // Get Student
        $req = new GetStudents($this->basicAuth, $this->collection);
        $response = $req->listAll(["$id"]);

        $this->assertEquals(1, sizeof($response));
        $this->assertEquals($id, $response[0]->id);
        $this->assertEquals('test@yahoo.com',$response[0]->email);
    }

    public function testCreateStudentWithOrg()
    {
        $this->basicAuth = new BasicAuth();
        $this->collection = new Collection();

        $req = new \App\Repositories\ApiRequest\CreateUpdateStudents($this->basicAuth, $this->collection);
        $student = new CmStudent('cmtest', 'fname','lname');
        $student->addSubOrg("Supervisor");

        // Create new Student
        $response = $req->create($student);
        $id = $response->ids[0]->id;

        // Get Student
        $req = new GetStudents($this->basicAuth, $this->collection);
        $response = $req->listAll(["$id"]);
        $this->assertEquals("Supervisor",$response[0]->subOrgs[0]->{'0'});
    }

    public function testUpdateStudentWithOrg()
    {
        $this->basicAuth = new BasicAuth();
        $this->collection = new Collection();

        $req = new \App\Repositories\ApiRequest\CreateUpdateStudents($this->basicAuth, $this->collection);
        $student = new CmStudent('cmtest', 'fname','lname');

        // Create new Student
        $response = $req->create($student);
        $id = $response->ids[0]->id;
        $student->addSubOrg("Supervisor");
        $req->update($id, $student);

        // Get Student
        $req = new GetStudents($this->basicAuth, $this->collection);
        $response = $req->listAll(["$id"]);
        $this->assertEquals("Supervisor",$response[0]->subOrgs[0]->{'0'});
    }

    public function testIsExisting()
    {
        $this->basicAuth = new BasicAuth();
        $this->collection = new Collection();

        $req = new \App\Repositories\ApiRequest\CreateUpdateStudents($this->basicAuth, $this->collection);
        $student = new CmStudent('cmtest', 'fname','lname');

        // Create new Student
        $response = $req->create($student);
        $id = $response->ids[0]->id;
        $student->addSubOrg("Supervisor");
        $req->update($id, $student);

        // Get Student
        $req = new GetStudents($this->basicAuth, $this->collection);
        $response = $req->isExisting($id);
        $this->assertTrue($response);
        $response = $req->isExisting("otherID");
        $this->assertFalse($response);
    }

    public function testEnrollStudent()
    {
        $req = new \App\Repositories\ApiRequest\EnrollStudents($this->basicAuth, $this->collection);
        $enrollStudent = new \App\Repositories\ApiModels\EnrollStudent();
        $enrollStudent->setId('celestinobeier757');
        $enrollStudent->setCourse('DOT-IATA-CCLQ-2');
        $enrollStudent->setOrgId(env('CM_API_ORGID'));
        $enrollStudent->setStart(Carbon::parse('now')->toDateTimeString());

        $response = $req->enroll($enrollStudent);

        $this->assertTrue($response['created']);
    }
}
