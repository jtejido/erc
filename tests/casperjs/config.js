var _ = require('underscore');

var retina = false;

var pixelRatio = 1;
var width = 1280;
var height = 800;

if (retina) {
    pixelRatio = 2;
    width = 1280*2;
    height = 800*2;
}
var config = {
    viewportSize: { width: (width*pixelRatio), height: (height*pixelRatio) },
    pageSettings: {
        userAgent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/602.1.25 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.10"
    },
    logLevel: "info",
    onWaitTimeout: 10000,
    verbose: false
};

casper.options = _.extend(casper.options,config);

/**
 * Developer note:
 *
 * phantom.injectJs() works, but injects first before page js assets.
 * Even casper.on(*) with any event in http://docs.casperjs.org/en/latest/events-filters.html
 * It will look like ajax calls are called from an injected iframe, hence cross-site requests being blocked
 */
// No:
// phantom.injectJs('client.js'); 
// Yes:
// casper.options.clientScripts.push("client.js");
casper.options.clientScripts.push("client.js");

casper.on('started', function(){
    // retina quality
    if (retina) {
        casper.zoom(4);        
    }
});

// casper.log(JSON.stringify(casper.options,null,2));
