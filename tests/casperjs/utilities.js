function extract(variable) {
    for (var key in variable) {
        window[key] = variable[key];
    }
}

// var compact = function() {
//     var obj = {};
//     console.log('xxx~ > '+JSON.stringify(arguments));
//     Array.prototype.forEach.call(arguments, function (elem) {
//         obj[elem] = window[elem];
//     });
//     console.log('xxx~ > '+JSON.stringify(obj));
//     return obj;
// }

/*
 * High Level imports
 */
    const fs = require('fs');
    const util = require('util');

    var PWD = fs.workingDirectory;
    var ROOT = PWD+'/../../';
    var env  = require('dotenv').parse(fs.read('./../../.env'));
    var moment = require('moment');

/*
 * Task Level imports
 */
    var Faker = require('faker');
    var _ = require('underscore');

    var Vault = require("vault");
    Vault.DASH = ['-'];
    var vault = new Vault({
        phrase: "",
        length: 12,
        symbol: 0,
        number: 1,
        dash: 3,
        space: 0
    });
    var Audit = {
        data: {
            audit_path: '',
            preliminary: 0,
            case: null
        },

        /**
         * Initialize audit by indicating scenario and test case
         * 
         * @param  int _preliminary scenario
         * @param  int _case        test case
         */
        init: function(_preliminary, _case) {
            this.preliminary = _preliminary;
            this.audit_path = 'images/'+this.preliminary+'/';
            if (_case) {
                this.case = _case;
                this.audit_path += _case+'/';
            }
            // this ensures that every test case is independent
            // from one another. always start with a clear session.
            phantom.clearCookies();
        },
        /**
         * Capture specific area
         * 
         * @param  string name page name
         * @param  string area selector
         */
        screen: function(name, area) {
            var image_name = this.audit_path+name+'.png';
            if (area) {
                casper.captureSelector(image_name, area);
                return;
            } else {
                casper.captureSelector(image_name, 'body');
            }

            // enable this to capture viewport height/width
            // casper.capture(image_name);
        }
    };

/*
 * declarations
 */
    var base_url = env.APP_URL;

/*
 * Prepare variables to be available to each test
 */
    var items = [];
    // High Level imports
    items.push({
        fs: fs,
        util: util,
        PWD: PWD,
        env: env
    });

    // Task Level imports
    items.push({
        Faker: Faker,
        vault: vault,
        Audit: Audit
    });

    // declarations
    items.push({
        base_url: base_url
    });

var casperLogPath = ROOT+'storage/logs/casperjs.log';
var casperLog = function(message) {
    // http://phantomjs.org/api/fs/
    // fs is a phantomjs implementation, not the nodejs one
    fs.write(casperLogPath, message+"\n", 'w');
};

casper.on("log", function(data, request) {
    if (casper.logLevels.indexOf(data.level) < casper.logLevels.indexOf(casper.options.logLevel)) {
        return; // skip logging
    }

    var date = moment(new Date(data.date)).format('Y-MM-DD hh:mm:ssA');
    var text = util.format("%s [%s] %s", date, data.level, data.message);
    casperLog(text);
});

var generatePassword = function() {
    return vault.generate();
};

var init = function() {
    _.each(items, function(e,i,l) {
        extract(e);
    });
}

var hasOwnProperty = function(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
        (!(prop in proto) || proto[prop] !== obj[prop]);
}

if ( Object.prototype.hasOwnProperty ) {
    var hasOwnProperty = function(obj, prop) {
        return obj.hasOwnProperty(prop);
    }
}

var empty = function(obj, property) {
    if (!hasOwnProperty) {
        return false;
    }

    if (obj[property]) {
        return true;
    }

    return false;
}

var utilities = {
    extract: extract,
    items: items,
    init: init,
    has: hasOwnProperty,
    empty: empty,
    generatePassword: generatePassword
};

module.exports = utilities;