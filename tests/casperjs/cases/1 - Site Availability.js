/*
 * Load .env, configure casper, and include all required libraries
 */
var use = require('../utilities'); use.init();

// XPath selector
/**
 * XPath selector
 *
 * Note: not possible to be moved to utilities
 * 
 */
var x = require('casper').selectXPath;

/*
 * Globally available tools
 *
 * Faker    - https://github.com/marak/faker.js/
 * _        -  http://underscorejs.org/
 *
 * Variables
 *
 * env      - Variables in .env accessible via env.[key]
 *            e.g. APP_URL via `env.APP_URL`
 * PWD      - Current directory of the script
 */
Audit.init(1);

casper.test.begin('Site Availability', 1, {
    setUp: function(test) {
        
    },
    test: function (test) {

        casper.start(base_url, function() {
            //
        });

        casper.then(function(){
            test.assertTitle("Environmental Resource Center", "ercweb title is as expected");
            Audit.screen('home');
        });

        casper.run(function() {
            test.done();
        });
    }
});