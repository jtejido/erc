/*
 * Load .env, configure casper, and include all required libraries
 */
var use = require('../utilities'); use.init();

// XPath selector
/**
 * XPath selector
 *
 * Note: not possible to be moved to utilities
 * 
 */
var x = require('casper').selectXPath;

/*
 * Globally available tools
 *
 * Faker    - https://github.com/marak/faker.js/
 * _        -  http://underscorejs.org/
 *
 * Variables
 *
 * env      - Variables in .env accessible via env.[key]
 *            e.g. APP_URL via `env.APP_URL`
 * PWD      - Current directory of the script
 */

Audit.init(4, 1);

var given = {
    first_name: Faker.fake("{{name.firstName}}"),
    last_name: Faker.fake("{{name.lastName}}"),
    title: Faker.fake("{{lorem.words}}"),
    email: Faker.fake("{{internet.email}}"),
    password: 'asd123',
    re_password: 'asd123',
    company: Faker.fake("{{company.companyName}}"),
    address: Faker.fake("{{address.streetAddress}}"),
    address2: Faker.fake("{{address.secondaryAddress}}"),
    city: 'New York',
    state: 'New York',
    zip_code: '10001',
    phone_number: '1231231239',
    fax_number: ''
}

var emails = null;

casper.test.begin('Registration with complete details should pass.js', 4, {
    setUp: function(test) {
        //
    },
    test: function (test) {
        /*
         * This was commented out, because it was used to test
         * if the headless browser really deletes the cookies
         * Hence default guest landing should be shown
         * and no logged in user from previous tests should be present
         *
         * re-enable this block and check image when issues seems to occur
         */
        casper.start(base_url, function() {
            //
        });

        casper.then(function() {
            this.waitForSelector(x("//a[normalize-space(text())='Login/Register']"),
               function success() {
                    this.click(x("//a[normalize-space(text())='Login/Register']"));
               },
               function fail() {
                   test.fail('Login button not present')
           });
        });

        casper.then(function() {
            Audit.screen('button', 'form .panel.panel-default');
            casper.waitForSelector(x("//a[normalize-space(text())='Sign Up']"),
               function success() {
                   test.assertExists(x("//a[normalize-space(text())='Sign Up']"));
                   this.click(x("//a[normalize-space(text())='Sign Up']"));
               },
               function fail() {
                   test.fail('Sign up button does not exist');
           });
        })
        
        casper.then(function() {
            this.evaluate(function() {
                // this is to allow us to override the value.
                $('input[name="phone_number"]').unbind();
            });
            this.fill('form#registration_form', given);
        });

        casper.then(function() {
            this.evaluate(function() {
                // this restores the input mask, allowing us user-expected phone number field appearance
                $('input[name="phone_number"]').inputmask();
            });
            Audit.screen('fields');
        })

        casper.then(function() {
            casper.waitForSelector(".btn.btn-primary.btn-register",
               function success() {
                   test.assertExists(".btn.btn-primary.btn-register");
                   this.click(".btn.btn-primary.btn-register");
               },
               function fail() {
                   test.fail('Submit registration button not found')
           });
        })

        casper.then(function(){
            Audit.screen('landing');
           var textUponSignup = "Course Listing";
           casper.waitForSelector(x("//*[contains(text(), \'"+textUponSignup+"\')]"),
               function success() {
                   test.assertExists(x("//*[contains(text(), \'"+textUponSignup+"\')]"));
                 },
               function fail() {
                   test.fail('Expected text upon login not found');
           });
        });

        casper.thenOpen(base_url+'/test/emails', function() {
            emails = JSON.parse(this.getPageContent());
            test.assertEquals('ua-register', emails.data[0].slug);
            // require('utils').dump(emails);
        });

        if (!base_url.match('ercweb.sitegage.com')) {
          var id = null;
          casper.thenOpen(base_url+':1080').then(function() {
              id = casper.evaluate(function(_email) {
                  $('td:contains("'+_email+'")').click();
                  return $('td:contains("'+_email+'")').parent().data('message-id');
              }, given.email);
          });

          casper.wait(1000, function() {
              Audit.screen('email', 'iframe[src="/messages/'+id+'.html"]');
          });
        }

        casper.run(function() {
            test.done();
        });
    }
});