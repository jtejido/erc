/*
 * Load .env, configure casper, and include all required libraries
 */
var use = require('../utilities'); use.init();

// XPath selector
/**
 * XPath selector
 *
 * Note: not possible to be moved to utilities
 * 
 */
var x = require('casper').selectXPath;

/*
 * Globally available tools
 *
 * Faker    - https://github.com/marak/faker.js/
 * _        -  http://underscorejs.org/
 *
 * Variables
 *
 * env      - Variables in .env accessible via env.[key]
 *            e.g. APP_URL via `env.APP_URL`
 * PWD      - Current directory of the script
 */
var given = {
    email: Faker.fake("{{internet.email}}"),
    password: use.generatePassword()
};
Audit.init(2, 2);

casper.test.begin('Login with improper credentials should fail (wrong email)', 1, {
    setUp: function(test) {
        
    },
    test: function (test) {
        /*
         * This was commented out, because it was used to test
         * if the headless browser really deletes the cookies
         * Hence default guest landing should be shown
         * and no logged in user from previous tests should be present
         *
         * re-enable this block and check image when issues seems to occur
         */
        casper.start(base_url, function() {
        //     this.waitForSelector("body",
        //         function pass () {
                    Audit.screen('landing');
        //         }
        //     );
        });

        casper.then(function() {
            casper.waitForSelector(x("//a[normalize-space(text())='Login/Register']"),
               function success() {
                   this.click(x("//a[normalize-space(text())='Login/Register']"));
               },
               function fail() {
                   test.fail('Login button not present')
           });
        })
        
        casper.then(function() {
             casper.waitForSelector("input[name='email']",
                 function success() {
                     this.sendKeys("input[name='email']", given.email);
                 },
                 function fail() {
                     test.fail('Email address field not found');
             });
             casper.waitForSelector("input[name='password']",
                 function success() {
                     this.sendKeys("input[name='password']", given.password);
                 },
                 function fail() {
                     test.fail('Email address field not found');
             });
        });
        casper.then(function() {
            Audit.screen('fields', 'form .panel.panel-default');
        })
        casper.then(function(){
             casper.waitForSelector("form .panel-inner.panel-footer .btn.btn-primary",
                 function success() {
                     this.click("form .panel-inner.panel-footer .btn.btn-primary");
                 },
                 function fail() {
                     test.fail('Submit button not found.')
             });
        });

        casper.then(function() {
            // for debugging only
            // Audit.screen('invalid-pre');

            var textUponLogin = "These credentials do not match our records";
             casper.waitForSelector(x("//*[contains(text(), \'"+textUponLogin+"\')]"),
                 function success() {
                     test.assertExists(x("//*[contains(text(), \'"+textUponLogin+"\')]"));
                   },
                 function fail() {
                     test.fail('Expected text upon login failure not found');
             });
        });
        casper.then(function() {
            Audit.screen('invalid', 'form .panel.panel-default');
        })

        casper.run(function() {
            test.done();
        });
    }
});