/*
 * Load .env, configure casper, and include all required libraries
 */
var use = require('../utilities'); use.init();

// XPath selector
/**
 * XPath selector
 *
 * Note: not possible to be moved to utilities
 * 
 */
var x = require('casper').selectXPath;

/*
 * Globally available tools
 *
 * Faker    - https://github.com/marak/faker.js/
 * _        -  http://underscorejs.org/
 *
 * Variables
 *
 * env      - Variables in .env accessible via env.[key]
 *            e.g. APP_URL via `env.APP_URL`
 * PWD      - Current directory of the script
 */
Audit.init(2, 1);
var given = {
    email: 'user@ercweb.com',
    password: 'asd123'
};

casper.test.begin('Login with proper credentials should pass', 4, {
    setUp: function(test) {

    },
    test: function (test) {
      casper.start(base_url, function() {

        casper.waitForSelector(x("//a[normalize-space(text())='Login/Register']"),
             function success() {
                 test.assertExists(x("//a[normalize-space(text())='Login/Register']"));
             },
             function fail() {
                 test.fail('Login button not present')
         });
      });
      casper.then(function() {
          Audit.screen('navigate', x("//a[normalize-space(text())='Login/Register']"));
      });
      casper.then(function() {
          this.click(x("//a[normalize-space(text())='Login/Register']"));
      });
      
      casper.then(function() {
          this.fill('#login-form', given);
           Audit.screen('fields', 'form .panel.panel-default');

           test.assertExists("form .panel-inner.panel-footer .btn.btn-primary");

      });

      casper.then(function() {
        this.click("form .panel-inner.panel-footer .btn.btn-primary");
        // this was added because the landing page loads too long
        Audit.screen('pre-landing');
      })

      casper.wait(2000, function() {
        Audit.screen('landing');
      });
      var textUponLogin = "Training and Order History";
       casper.waitForSelector(x("//*[contains(text(), \'"+textUponLogin+"\')]"),
           function success() {
               test.assertExists(x("//*[contains(text(), \'"+textUponLogin+"\')]"));
           },
           function fail() {
               test.fail('Expected text upon login not found');
       });

      casper.run(function() {
          test.done();
      });
    }
});