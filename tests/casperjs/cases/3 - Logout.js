/*
 * Load .env, configure casper, and include all required libraries
 */
var use = require('../utilities'); use.init();

// XPath selector
/**
 * XPath selector
 *
 * Note: not possible to be moved to utilities
 * 
 */
var x = require('casper').selectXPath;

/*
 * Globally available tools
 *
 * Faker    - https://github.com/marak/faker.js/
 * _        -  http://underscorejs.org/
 *
 * Variables
 *
 * env      - Variables in .env accessible via env.[key]
 *            e.g. APP_URL via `env.APP_URL`
 * PWD      - Current directory of the script
 */
var given = {
    email: "user@ercweb.com",
    password: 'asd123'
};

Audit.init(3);

casper.test.begin('Logout', 1, {
    setUp: function(test) {

    },
    test: function (test) {
        /*
         * This was commented out, because it was used to test
         * if the headless browser really deletes the cookies
         * Hence default guest landing should be shown
         * and no logged in user from previous tests should be present
         *
         * re-enable this block and check image when issues seems to occur
         */
        casper.start(base_url, function() {
            //
        });

        casper.then(function() {
            this.waitForSelector(x("//a[normalize-space(text())='Login/Register']"),
               function success() {
                    Audit.screen('button', x("//a[normalize-space(text())='Login/Register']"));
                    this.click(x("//a[normalize-space(text())='Login/Register']"));
               },
               function fail() {
                   test.fail('Login button not present')
           });
        })
        
        casper.then(function() {
            this.waitForSelector("input[name='email']",
                function success() {
                    this.sendKeys("input[name='email']", given.email);
                },
                function fail() {
                    test.fail('Email address field not found');
            });            

            this.waitForSelector("input[name='password']",
                function success() {
                    this.sendKeys("input[name='password']", given.password);
                },
                function fail() {
                    test.fail('Email address field not found');
            });
        });

        casper.then(function() {
            Audit.screen('fields', 'form .panel.panel-default');
        })

        casper.then(function(){
             this.waitForSelector("form .panel-inner.panel-footer .btn.btn-primary",
                 function success() {
                     this.click("form .panel-inner.panel-footer .btn.btn-primary");
                 },
                 function fail() {
                     test.fail('Submit button not found.')
             });
        });

        casper.then(function() {
            this.evaluate(function() {
                // click open the user's name to open menu
                $('.navbar-menu-auth a:first').click();
            });
            // hover on top of logout link
            this.mouse.move(x("//a[normalize-space(text())='Logout']"));
        });

        casper.then(function() {
            // capture the dropdown menu
            Audit.screen('button', '.navbar-menu-auth .dropdown-menu');
        });

        casper.then(function() {
            this.waitForSelector(x("//a[normalize-space(text())='Logout']"),
               function success() {
                   this.click(x("//a[normalize-space(text())='Logout']"));
               },
               function fail() {
                   test.fail('Logout button not present')
           });
        })

        casper.then(function() {
            casper.waitForSelector(x("//a[normalize-space(text())='Login/Register']"),
                 function success() {
                     test.assertExists(x("//a[normalize-space(text())='Login/Register']"));
                 },
                 function fail() {
                     test.fail('Was not able to logout')
             });
        });

        casper.then(function() {
            Audit.screen('logged-out', x("//a[normalize-space(text())='Login/Register']"));
        });

        casper.run(function() {
            test.done();
        });
    }
});