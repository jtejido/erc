#!/bin/sh

cd tests/casperjs;

TEST_TARGET="cases/";
PWD=`pwd`;
ROOT=$(cd "$PWD/../../"; pwd);

if [ $# -gt 0 ]; then
    if [ -f "$ROOT/$1" ]; then
        TEST_TARGET="$ROOT/$1"
    fi
fi

casperjs test \
--post=post.js \
--includes=config.js \
--fail-fast \
--log-level=debug \
"${TEST_TARGET}"