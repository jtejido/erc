@extends('main.redirect_layout')

@section('stylesheets')
    @parent
    <style media="screen">
        ul.student-list {
            margin: 0 30px;
        }
        ul.student-list li {

        }
        ul.student-list li label {
            font-size: 1.2em;
        }
        ul.student-list li input[type=checkbox] {
            margin-left: -25px;
        }
        strong { font-weight: bold; }
        small { font-size: 0.7em; }
    </style>

@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

            <div class="error-page">
                <h2 class="headline text-yellow"> 404</h2>

                <div class="error-content">

                    <br/><br/>

                    <p class="lead">
                        We could not find the page you were looking for.
                        Meanwhile, you may <a href="{{ url('/') }}">return to dashboard</a>.
                    </p>

                </div>
                <!-- /.error-content -->
            </div>
            <!-- /.error-page -->

            <br/><br/><br/>

        </div>
    </section>
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

        })(jQuery);
    </script>
@endsection
