<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notification</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to modify this?</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="notification-cancel" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                <button type="button" id="notification-ok" class="btn btn-primary notification-ok btn-flat">Ok</button>
            </div>
        </div>
    </div>
</div>