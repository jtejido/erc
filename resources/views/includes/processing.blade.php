<div class="modal fade" tabindex="-1" role="dialog" id="processing_modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-refresh fa-spin"></i> <strong>Order Update</strong></h4>
            </div>
            <div class="modal-body">
                <p class="lead">Your order was updated by the CSR.
                    This page will now reload to reflect the changes.</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->