<div class="row">
    <div class="col-md-6">
        <h3 class="panel-title item-title">Billing Address:</h3>
        <p>{!! $paymentTransaction->complete_billing_address !!}</p>
    </div>
    <div class="col-md-6">
        <h3 class="panel-title item-title">Shipping Address:</h3>
        <p>{!! $paymentTransaction->complete_shipping_address !!}</p>
    </div>
</div>
@if($paymentTransaction->isCheck())
    <h5 style="margin-bottom: 10px; display: block;">Check Number : {{ $paymentTransaction->masked_cc }}</h5>
@elseif($paymentTransaction->isCash())
    <h5 style="margin-bottom: 10px; display: block;">Payment via CASH</h5>
@else
    <h5 style="margin-bottom: 10px; display: block;">Credit Card Transaction ID: {{ $paymentTransaction->transaction_id }}</h5>
    <h5 style="margin-bottom: 10px; display: block;">Credit Card Number (masked) : {{ $paymentTransaction->masked_cc }}</h5>
@endif
<h5 style="margin-bottom: 10px; display: block;">Payment Date: {{ $paymentTransaction->created_at->format('F d, Y') }}</h5>
<h5 style="margin-bottom: 10px; display: block;">Payment Amount: ${{ number_format($paymentTransaction->amount, 2) }}</h5>

@if ($paymentTransaction->items)

    @foreach(json_decode($paymentTransaction->items)->items as $item)
        {!! OrderUtility::extractItemsFromPayment($paymentTransaction, $user, $item) !!}
    @endforeach
    <div class="subtotal"><strong>Shipping:</strong><span class="total-amount pull-right"><strong>{{ $paymentTransaction->shipping_fee }}</strong></span></div>
    <div class="subtotal"><strong>Sales Tax:</strong><span class="total-amount pull-right"><strong>{{ $paymentTransaction->state_tax_formatted }}</strong></span></div>
    <div class="subtotal"><strong>Total</strong><span class="total-amount pull-right"><strong>${{ number_format(json_decode($paymentTransaction->items)->total_amount, 2) }}</strong></span></div>
@endif
<hr>