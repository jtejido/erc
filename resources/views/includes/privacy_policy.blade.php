<div class="modal fade" id="ppModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header blue-bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="cancellationModal">
                    Provicy Policy
                </h4>
            </div>

            <div class="modal-body">
                <p class="margin-bottom">
                In order for you to receive the products and services you order or the information you request, you must provide us with your name, address, and other essential information. Financial information collected, such as credit card number and expiration date, is used only to charge your account for the products and services you purchase. It will not be given to any third party other than the credit card processing and authorization service.

                We have put security measures in place to protect the loss, misuse, or alteration of information under our control. All orders are processed via a secure server (SSL).

                If collected, personal information, such as your name and email address, is used to recognize your account. Occasionally Environmental Resource Center will use individually identifiable information, like your email address, in order to contact you to provide product or order information. Environmental Resource Center may also distribute various newsletters or advertisements to you. You can subscribe and unsubscribe from these lists at any time.

                Environmental Resource Center may occasionally share your mailing address with pre-screened companies who offer relevant products or services of interest to our customers. If you do not wish to have your mailing address shared, please let us know by sending an email to service@ercweb.com, or by calling 1-800-537-2372, or by faxing to 919-342-0807, or by sending your request via postal mail to Environmental Resource Center, 101 Center Pointe Drive, Cary, NC, 27513-5706.

                If you have any questions about this policy or any Environmental Resource Center services please send you comments to service@ercweb.com.
                </p>

                <div class="text-center">
                    <button type="button" id="notification-cancel" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>



