<div class="modal fade" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notification</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to modify this?</p>
            </div>
            <div class="modal-footer">
                @if (! Auth::user()->isMember())
                    <span id="doNotEmailContainer" style="display: none">
                    <input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                    <label for="doNotEmail">Do Not Email Agent And Attendees</label>&nbsp;
                    </span>
                @endif
                <button type="button"
                        id="notification-cancel"
                        class="btn @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-default'}}@endif btn-flat" data-dismiss="modal">Close</button>
                <button type="button"
                        id="notification-ok"
                        class="btn @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-primary'}}@endif notification-ok btn-flat">Ok</button>
            </div>
        </div>
    </div>
</div>