@if (count($students))
    @foreach ($students as $student)
        @if(is_string($student))

            @if (Auth::user()->isMember())
                <a class="link-to-user label label-info btn-flat btn-xs" href="{{ route('admin.user_details', $student['params']) }}">{!! $student !!}</a>
            @else
                <span class="label label-info btn-flat btn-xs">
                    <a class="link-to-user" href="{{ route('admin.user_details', $student['params']) }}">{!! $student !!}</a>
                </span>
            @endif

        @else

            @if (Auth::user()->isMember())
                <a class="link-to-user label label-info btn-flat btn-xs">
                    {{ $student['name'] }}

                    @if ($student['cancelled'])
                        <span class="label label-warning">
                            CANCELLED
                        </span>
                    @endif

                    @if ($student['swapped'])
                        <span type="button" class="label label-danger"
                            data-container="body"
                            data-toggle="popover"
                            data-placement="top" data-content="{{ $student['swapNote'] }}">
                            SWAPPED
                        </span>
                    @endif

                    @if ($student['new'])
                        <span type="button" class="label label-success">
                            NEW
                        </span>
                    @endif
                </a>

                @if(has_role([App\Utilities\Constant::ROLE_ADMIN, App\Utilities\Constant::ROLE_CSR])
                 AND !$student['cancelled'] AND !$student['swapped'] AND !$student['new'])
                    <a href="#"
                       onclick="return confirm('Are you sure you want to resend confirmation to attendee {{ $student['name'] }}?')"
                            ><i class="fa fa-envelope"></i></a>&nbsp;
                @endif
            @else
                <span class="label label-info btn-flat btn-xs" title="{{ $student['id'] }}">
                    <a class="link-to-user" href="{{ route('admin.user_details', $student['params']) }}">
                        {{ $student['name'] }}
                    </a>

                    @if ($student['cancelled'])
                        <span type="button" class="label label-warning cancellation-note"
                              data-container="body"
                              data-toggle="popover"
                              data-placement="top" data-content="{{ $student['cancelNote'] }}">
                            CANCELLED
                        </span>
                    @endif

                    @if ($student['swapped'])
                        <span type="button" class="label label-danger cancellation-note"
                              data-container="body"
                              data-toggle="popover"
                              data-placement="top" data-content="{{ $student['swapNote'] }}">
                            SWAPPED
                        </span>
                    @endif

                    @if ($student['new'])
                        <span type="button" class="label label-success">
                            NEW
                        </span>
                    @endif

                    @if(has_role([App\Utilities\Constant::ROLE_ADMIN, App\Utilities\Constant::ROLE_CSR])
                    AND !$student['cancelled'] AND !$student['swapped'] AND !$student['new'])
                        <a href="#"
                           onclick="return confirm('Are you sure you want to resend confirmation to attendee {{ $student['name'] }}?')"
                                ><i class="fa fa-envelope"></i></a>&nbsp;
                    @endif
                </span>
            @endif
        @endif
    @endforeach
@endif