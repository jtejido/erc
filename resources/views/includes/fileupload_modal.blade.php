<div class="modal fade" id="addFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(['url' => route($route),
                            'id'        => 'addFileForm',
                            'method'    => 'post',
                            'files'     => true]) !!}
            <input type="hidden" name="file_id" value="0" />
            @if(isset($route_back))
            <input type="hidden" name="route_back" value="{{$route_back}}" />
            @endif
            <input type="hidden" name="item_id" value="{{$item_id}}" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add File</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, $attributes = ['class' => 'form-control', 'required' => 'required'])  !!}
                </div>
                @if(isset($type))
                    @if(sizeof($type) == 1)
                        {!! Form::hidden('type', array_values($type)[0]) !!}
                    @else
                        <div class="form-group">
                            {!! Form::label('type', 'Type') !!}
                            {!! Form::select('type', $type, null, $attributes = ['class' => 'form-control', 'required' => 'required'])  !!}
                        </div>
                    @endif
                @else
                    {!! Form::hidden('type', 'FILE') !!}
                @endif
                @if(isset($file_type))

                    <div class="form-group">
                        {!! Form::label('material_type', 'Type') !!}
                        {!! Form::select('material_type', $file_type, null, $attributes = ['class' => 'form-control', 'id' => 'material-type'])  !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('file', 'Link', ['class' => 'link hidden']) !!}
                        {!! Form::text('path', '', ['class' => 'form-control hidden link']) !!}
                    </div>

                @endif

                <div class="form-group">
                    {!! Form::label('file', 'Upload File', ['class' => 'file-caption-main']) !!}
                    {!! Form::file('file', null, $attributes = ['class' => 'form-control hidden file'])  !!}
                </div>

            </div>
            <div class="modal-footer">

                <input type="checkbox" name="doNotEmailInvoice" id="doNotEmailInvoice" />&nbsp;
                <label for="doNotEmailInvoice">Do Not Email Agent And Attendees</label>
                &nbsp;&nbsp;
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>