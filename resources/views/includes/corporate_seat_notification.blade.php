<div class="modal fade" id="csModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="cancellationModal">
                    Corporate Seat Subscription
                </h4>
            </div>

            <div class="modal-body" style="line-height: 20px">
                <p class="margin-bottom">
                    To register for a Corporate Subscription, call 800-537-2372 and ask to speak with a customer service representative.
                </p>

                <div class="text-center">
                    <button type="button" id="notification-cancel" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>



