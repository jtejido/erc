@if (Session::has('success_message'))
    <div>
        <div class="alert alert-success alert-dismissible box box-success box-solid" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('success_message') }}
        </div>
    </div>
@endif