@if (Session::has('error_message'))
    <div>
        <div class="alert alert-danger alert-dismissible box box-solid" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('error_message') }}
        </div>
    </div>
@endif