<div class="modal fade" id="siteModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header blue-bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="cancellationModal">
                    Site Map
                </h4>
            </div>

            <div class="modal-body site-map text-left row">

                <div class="col-md-10 text-left">
                    <a href="{{ route('home') }}" target="_blank">Home</a>
                </div>

                <div class="col-md-10 text-left">
                    <a href="{{ route('courses') }}" target="_blank">Courses/Classes</a>
                </div>

                <div class="col-md-10 text-left">
                    <a href="/about-us" target="_blank">About Us</a>
                </div>

                <div class="col-md-10 text-left">
                    <a href="javascript:void(0)">Consulting</a>

                    <div class="col-md-12 col-md-offset-1">
                        <a href="/consulting/consulting-page" target="_blank">Consulting Page</a>
                        <a href="/consulting/audits"  target="_blank">Audits</a>
                        <a href="/consulting/permits" target="_blank">Permits</a>
                        <a href="/consulting/hazardous-materialsdangerous-goods-classification" target="_blank">Hazardous Materials/Dangerous Goods Classification</a>
                        <a href="/consulting/sara-title-ii-compliance-and-reporting" target="_blank">SARA Title III Compliance and Reporting</a>
                        <a href="/consulting/plans-and-procedures" target="_blank">Plans and Procedures</a>
                        <a href="/consulting/ehs-program-management" target="_blank">EHS Program Management</a>
                    </div>
                </div>

                <div class="col-md-10 text-left">
                    <a href="{{ route('products') }}" target="_blank">Products</a>
                </div>

                <div class="col-md-10 text-left">
                    <a href="javascript:void(0)">Resources</a>

                    <div class="col-md-12 col-md-offset-1">
                        <a href="/regulations" target="_blank">Reg of the Day</a>
                        <a href="/tips" target="_blank">Tips of the Week</a>
                        <a href="{{ route('resource.classcalendar') }}" target="_blank">Schedule of Classes</a>
                    </div>
                </div>

                <div class="col-md-10 text-left">
                    <a href="{{ route('get.login') }}" target="_blank">Sign In</a>
                </div>

                <div class="col-md-10 text-left">
                    <a href="{{ route('user.register') }}" target="_blank">Sign Up</a>
                </div>

                <div class="text-center col-md-12">
                    <button type="button" id="notification-cancel" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>



