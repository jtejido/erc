<div class="modal fade" id="swapNotificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Notification</h4>
            </div>
            <div class="modal-body">
                <p>
                    Based on the group discounts the following students will also be changed for the
                    <span class="bold">{{ $pairedClass }}</span>.
                </p>

                @foreach ($pairedClassSwapNames as $pairedClassSwapName)
                    <br>
                    <p class="col-md-offset-1">&#8226; {{ $pairedClassSwapName }}</p>
                @endforeach

                <br>
                <p>Do you want to proceed?</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="notification-cancel"
                        class="btn @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-default'}}@endif btn-flat" data-dismiss="modal">Close</button>
                <button type="button" id="notification-ok"
                        class="btn @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-primary'}}@endif notification-ok btn-flat">Ok</button>
            </div>
        </div>
    </div>
</div>