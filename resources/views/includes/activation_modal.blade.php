<div class="modal fade" id="activationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="cancellationModal">
                    Activation
                </h4>
            </div>

            {!! Form::open([
                'method' => 'POST',
                'url' => route('activation.post')
            ]) !!}
            <div class="modal-body">
                {!! Form::input('hidden', 'subscription_id', $yearlySub->id) !!}

                {!! Form::rLabel('activation_code', 'Activation Code: ', ['class' => 'col-sm-4 control-label']) !!}

                <div class="form-group col-md-7">
                    {!! Form::input('text', 'activation_code', '' , [
                    'class' => 'form-control',
                    'id'    => 'activation_code',
                    'style' => 'display: inline;',
                    'required' => 'required'
                    ]) !!}
                </div>
                <div class="text-right">
                    <button type="button" id="notification-cancel" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                    <button type="submit" id="cancel-new-registration-btn" class="btn btn-primary notification-ok btn-flat">Activate</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>



