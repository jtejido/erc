@extends('main.main_layout')

@section('main_content')
<h1>Login</h1>
<section class="login">
        <div class="container-fluid">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                <div class="row authentication-top">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-inner panel-heading">Login</div>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="panel-inner panel-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                
                                </div>
                                <div class="form-group">
                                    <label class=" control-label">Password</label>
                                    <input type="password" class="form-control" name="password">
                                
                                </div>

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                                    <span> Not a member? </span> 
                                    <a class="btn btn-link" href=""> Click here </a> 
                                    <span> to register </span>
                                </div>                                                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row authentication-bottom">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="panel-inner panel-footer">
                            <button type="submit" class="btn btn-primary">Login</button>
                        </div>  
                    </div>
                </div>
            </form>
        </div>
    </section> 
<hr>

<section class="register">
    <div class="authentication-top"> </div>
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-inner panel-heading">Register</div>
            <div class="panel-inner panel-body">
                <form id="registration_form" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      
                          


                      <div class="row">             
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">First Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Last Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Title: <span></span></label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                          </div>
                        </div>            
                        <div class="col-md-6">    
                          <div class="form-group">
                            <label class=" control-label">E-Mail: <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Password: <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Retype Password: <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="re_password">                               
                          </div>
                        </div>
                      </div>                  

                      
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class=" control-label">Company: <span></span></label>
                            <input type="text" class="form-control" name="company" value="{{ old('company') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Address: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label"></label>
                            <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">City: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">State: <span class="text-danger">*</span></label>
                            <select class="form-control" name="state">
                                @foreach ($state_list as $key => $value)
                                    <option value="{{ $value['state'] }}" <?php if(old('state') == $value['state']){ echo "selected"; } ?> >{{ $value['state'] }}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">Zip: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="zip_code" name="zip_code" maxlength="5" value="{{ old('zip_code') }}">
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-sm-6">    
                          <div class="form-group phone">
                            <label class=" control-label">Phone: <span class="text-danger">*</span></label>
                            <input type="hidden" id="phone_number" name="phone_number" value="">                                    
                            <div class="form-group">
                                <input type="text" class="form-control text-center" id="phone1" name="phone1" maxlength="3" value="{{ old('phone1') }}">
                            </div>
                            <div class="form-group" >
                                <input type="text" class="form-control text-center" id="phone2" name="phone2" maxlength="3" value="{{ old('phone2') }}">
                            </div>
                            <div class="form-group" >
                                <input type="text" class="form-control text-center" id="phone3" name="phone3" maxlength="4" value="{{ old('phone3') }}">
                            </div>
                            <div class="form-group">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </div>                                    
                                <div class="form-group" >
                                    <input type="text" class="form-control text-center" id="phone4" name="phone4" maxlength="4" value="{{ old('phone4') }}">
                                </div>
                          </div>
                        </div>
                        <div class="col-sm-6">    
                          <div class="form-group fax">
                            <label class=" control-label">Fax: <span></span></label>
                            <input type="hidden" id="fax_number" name="fax_number" value="">
                              <div class="form-group" >
                                <input type="text" class="form-control text-center" id="fax1" name="fax1" maxlength="3" value="{{ old('fax1') }}">
                              </div>
                              <div class="form-group" >
                                <input type="text" class="form-control text-center" id="fax2" name="fax2" maxlength="3" value="{{ old('fax2') }}">
                              </div>
                              <div class="form-group" >
                                <input type="text" class="form-control text-center" id="fax3" name="fax3" maxlength="4" value="{{ old('fax3') }}">
                              </div>
                          </div>
                        </div>
                      
                    </div>
                </form> 
            </div>     
          
                  <div class="panel-inner panel-footer">                     
                        <div class="form-group">
                              <button type="submit" class="btn btn-primary" form="registration_form"> Submit </button>
                        </div>
                    </div>
          
        </div>
        </div>     
    </div>
  </section>

<hr>
<h2>Courses</h2>
    <script>
        (function($) {
            $('#type, #category').on('change', function() {
                $('#filters').submit();
            });
        })(jQuery);
    </script>

<section class="content">
        <div class="container">
            <div class="row">
                <div class="page-title">        
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Course Listing </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="courses">
                <div class="inner">
                    <div class="row">
                    <h3>Please choose what course you are interested in</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <button type="button" class="all"></button>
                            <label> All </label>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <button type="button" type="button" class="webinars"></button>
                            <label> Webinars </label>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <button type="button" type="button" type="button" class="seminars"></button>
                            <label> Seminars </label>
                        </div>                      
                        <div class="col-md-3 col-sm-6">
                            <button type="button" type="button" type="button" class="cbt"></button>
                            <label> CBTs </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filters">

                <script>
                    (function($) {
                        $('#type, #category').on('change', function() {
                            $('#filters').submit();
                        });
                    })(jQuery);
                </script>

                
                <div class="inner">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="wrap">
                                <label for="type"> Filter Type of Training </label>
                                <select name="type" id="type" class="form-control" form="course_filter"style="margin-bottom: 20px">
                                    <option value="">All</option>
                                    @foreach ($courseTypes as $type)
                                        <option value="{{ $type->id }}" @if($typeFilter == $type->id) selected @endif>{{ $type->name }}</option>
                                    @endforeach
                                </select>
                            </div>  
                        </div>
                        <div class="col-sm-4">
                            <div class="wrap">
                                <label for="type">Filter By Category: </label>
                                <select name="category" id="category" class="form-control" form="course_filter" style="margin-bottom: 20px">
                                    <option value="">All</option>
                                    @foreach ($courseCategories as $category)
                                        <option value="{{ $category->id }}" @if($categoryFilter == $category->id) selected @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>  
                        </div>
                        <div class="col-sm-4">
                            <div class="wrap">
                                <label> Search </label>
                                <input type="text" name="" form="course_filter" class="search">
                                </select>
                            </div>  
                        </div>
                    </div>
                    <div class="row">
                        <form id="course_filter">
                            <input type="submit" value="GO" class="go">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="course-listings">

            @if ($cbts)
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 column">
                                <div class="{{ str_slug( $category->name ) }}">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <h4>New - Online Training</h4>
                                <ul>
                                    @foreach ($cbts as $cbt)
                                        <li>
                                        <a href="{{ route('course.show', ['id' => $cbt->id]) }}">{{ $cbt->title }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


            @if ($categories)
                @foreach ($categories as $category)
                    
                    <div class="content-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-5 column">
                                    <div class="{{ str_slug( $category->name ) }}">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h4>{{ $category->name }}</h4>
                                    @if($typeFilter)
                                        @if (count(CourseUtility::getCoursesGivenParams($category, $typeFilter)))
                                            <ul>
                                                @foreach (CourseUtility::getCoursesGivenParams($category, $typeFilter) as $type => $course)
                                                    <li><a href="{{ route('course.show', ['id' => $course->id])  }}">{{ $course->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <div class="alert alert-danger">
                                                <p>No courses found.</p>
                                            </div>
                                        @endif
                                    @else
                                        {{-- No filters provided --}}
                                        @foreach($courseTypes as $courseType)
                                            <?php $courses = CourseUtility::getCoursesGivenParams($category, $courseType->id) ?>
                                            @if(count($courses))
                                            <div class="class-wrapper">
                                                <p class="class">{{ str_plural($courseType->name) }}</p>
                                                <ul>
                                                    @foreach ($courses as $course)
                                                        <li><a href="{{ route('course.show', ['id' => $course->id])  }}">{{ $course->title }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                @endforeach
            @endif
        </div>

    </section>
<hr>
<h2>Classes</h2>
    <section class="main-container container-fluid">
        <div class="page-header">
            <h3>Classes</h3>
        </div>

        @if ($registrations->count())
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Date</th>
                    <th>SKU</th>
                    <th>Course</th>
                    <th>Location</th>
                    <th>Agent</th>
                </tr>

                @foreach ($registrations as $registration)
                    <tr>
                        <td>{{ date('F d, Y', strtotime($registration->created_at)) }}</td>
                        <td>
                            @if ($registration->isCourse())
                                {{ $registration->registrable->sku }}
                            @else
                                {{ $registration->registrable->course->sku }}
                            @endif
                        </td>
                        <td>
                            @if ($registration->isCourse())
                                {{ $registration->registrable->title }}
                            @else
                                {{ $registration->registrable->course->title }}
                            @endif
                        </td>
                        <td>
                            @if (!$registration->isCourse())
                                {{ $registration->registrable->city }}, {{ $registration->registrable->state }}
                            @endif
                        </td>
                        <td>{{ $registration->agent->contact->first_name }} {{ $registration->agent->contact->last_name }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <i>No registrations.</i>
        @endif
    </section>
<hr>
<h2>Classes (Empty)</h2>
    <section class="main-container container-fluid">
        <div class="page-header">
            <h3>Classes</h3>
        </div>
        <i>No registrations.</i>
    </section>
<hr>
<hr>
@endsection