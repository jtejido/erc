<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@section('title') Environmental Resource Center @show</title>

    @section('stylesheets')
        {!! HTML::style('vendor/common.css') !!}
        {!! HTML::style('vendor/common_2.css') !!}
    @show

    @section('javascripts-top')
        {!! HTML::script('vendor/common.js') !!}
        <script>
        (function(d, script) {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');

            var trident = ua.indexOf('Trident/');
            if ((msie > 0 || trident > 0) == false) {
                script = d.createElement('script');
                script.type = 'text/javascript';
                script.async = true;
                script.onload = function(){
                    // remote script has loaded
                };
                script.src = window.location.origin + '/js/non-ie.js';
                d.getElementsByTagName('head')[0].appendChild(script);
            }
        }(document));
        </script>
    @show
</head>

<body class="skin-blue sidebar-mini admin {{ $page }} @yield('body-class', '')" ng-app="ercApp">
<input type="hidden" id="session-expired" value="{{ route('session.expired') }}">

@yield('body')
<script type="text/ng-template" id="save-success.html">
    <div class="ui-notification custom-template">
        @{{ message }} <em>successfully saved</em>.
    </div>
</script>
<script type="text/ng-template" id="save-error.html">
    <div class="ui-notification custom-template">
        @{{ message }} <em>was not saved</em>.
    </div>
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@section('javascripts')
@show

<script>
    $(function () {
        if(localStorage.expandedMenu==0) {
            $("body").addClass('sidebar-collapse');
        }

        $('body').bind('expanded.pushMenu', function() {
            localStorage.expandedMenu = 1;
        });

        $('body').bind('collapsed.pushMenu', function() {
            localStorage.expandedMenu = 0;
        });
    });
</script>
</body>
</html>