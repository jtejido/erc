
<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="classModalLabel">
        Cancellation
    </h4>
</div>

@if (count($customers))
    {!! Form::open([
            'method' => 'POST',
            'url' => route('registration.cancel'),
            'id' => 'cancel-complete-registration'
            ]) !!}
    <div class="modal-body">

    <div class="form-group text-center text-danger">
        <span>
            Add reason of cancellation on each registration.
        </span>
    </div>

    {!! Form::input('hidden', 'registration_order_id', $registrationOrderId) !!}
    {!! Form::input('hidden', 'agent_id', $agentId) !!}

    @foreach ($customers as $customer)

        {!! Form::input('hidden', 'class_registration_ids[]', $customer['id']) !!}

        <div>
            {!! Form::label('name', $customer['name'], ['class' => 'col-sm-4 control-label']) !!}
            <div class="form-group col-sm-8">
                {!! Form::textarea('notes['. $customer['id'] .']', null, [
                    'size' => '30x2',
                    'class' => 'form-control',
                    'style' => 'display: inline;',
                    'required' => 'required',
                    ])  !!}
            </div>
        </div>

    @endforeach


        @if (($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count()) or
             ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
                     $deferredGroupDiscount['class_combination_discount_ids']->count()))

            <div class="well well-sm col-md-12" style="float: left; width: 100%;">

                @if ($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count())
                    @foreach ($deferredHalfDiscount['class_registration_ids'] as $classRegIds)
                        {!! Form::input('hidden', 'deferred_half_price_class_registration_ids[]', $classRegIds) !!}
                    @endforeach

                    <div class="text-center">
                        <span class="required small">
                            There are half price discounts that will be considered void once cancellation is done.
                        </span>
                    </div>
                @endif

                @if ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
                     $deferredGroupDiscount['class_combination_discount_ids']->count())
                    @foreach ($deferredGroupDiscount['class_registration_ids'] as $classRegIds)
                        {!! Form::input('hidden', 'deferred_group_discounts_class_registration_ids[]', $classRegIds) !!}
                    @endforeach

                    @foreach ($deferredGroupDiscount['class_combination_discount_ids'] as $classCombinationIds)
                        {!! Form::input('hidden', 'deferred_class_combination_discount_ids[]', $classCombinationIds) !!}
                    @endforeach

                    <div class="text-center">
                        <span class="required small">
                            There are group discounts that will be considered void once cancellation is done.
                        </span>
                    </div>
                @endif

            </div>

        @endif

        <table class="table">
            <tr>
                <td>
                    {!! Form::label('paid', 'Amount Paid for Cancelled Registrations :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id="amount-paid" data-amount="{{ $paid }}">
                        ${{ number_format($paid, 2) }}
                    </span>
                    <input type="hidden" name="amount_paid" value="{{ $paid }}">
                </td>
            </tr>
            @if ($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count())
            <tr>
                <td>
                    {!! Form::label('deferred_half_price_discounts', 'Cancelled Half Price Discount Fees  :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id='deferred-half-price-fees'>
                        ${{ number_format($deferredHalfDiscount['amount'], 2) }}
                    </span>
                    <input type="hidden" name="deferred_half_price_fees" value="{{ $deferredHalfDiscount['amount'] }}">
                </td>
            </tr>
            @endif

            @if ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
            $deferredGroupDiscount['class_combination_discount_ids']->count())
            <tr>
                <td>
                    {!! Form::label('deferred_half_price_discounts', 'Cancelled Group Discount Fees :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id='additional-fees'>
                        ${{ number_format($deferredGroupDiscount['amount'], 2) }}
                    </span>
                    <input type="hidden" name="deferred-group-discount-fees" value="{{ $deferredGroupDiscount['amount'] }}">
                </td>
            </tr>
            @endif
            @if ($additionalFees)
            <tr>
                <td>
                    {!! Form::label('additional_fees', 'Additional Fees :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id='additional-fees'>
                        ${{ number_format($additionalFees, 2) }}
                    </span>
                    <input type="hidden" name="additional_fees" value="{{ $additionalFees }}">
                </td>
            </tr>
            @endif
            @if ($deductions)
            <tr>
                <td>
                    {!! Form::label('deductions', 'Deductions :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id='deductions'>
                        ${{ number_format($deductions, 2) }}
                    </span>
                    <input type="hidden" name="deductions" value="{{ $deductions }}">
                </td>
            </tr>
            @endif
            <tr>
                <td>
                    {!! Form::label('subtotal_refund', 'Total Amount of Refund :', ['class' => 'control-label']) !!}
                </td>
                <td>
                    <span id='subtotal-refund'>
                        ${{ number_format($subTotalRefund, 2) }}
                    </span>
                    <input type="hidden" name="refund" value="{{ $subTotalRefund }}">
                </td>
            </tr>

            <tr>
                <td>
                    <strong>{!! Form::label('new_order_total', 'New Order Total :', ['class' => 'control-label']) !!}</strong>
                </td>
                <td>
                    <span id='order-total'>
                        ${{ number_format($newOrderTotal, 2) }}
                    </span>
                    <input type="hidden" name="order_total" value="{{ $newOrderTotal }}">
                </td>
            </tr>

        </table>


        <div style="text-align: center">
            <button type="button" class="btn blue-btn btn-close" data-dismiss="modal">Close</button>
            <button type="submit" id="notification-ok" class="btn blue-btn proceed-cancellation">Proceed With Cancellation</button>
        </div>
    </div>
    {!! Form::close() !!}
@endif

