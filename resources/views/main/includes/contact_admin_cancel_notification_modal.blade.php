<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Notification</h4>
</div>
<div class="modal-body">
    <p>
        The registrations you're about to cancel is within the restriction of our Cancellation Policy.
    </p>

    <br>
    <p>
        Please contact CSR to proceed with this cancellation.
    </p>
</div>
<div class="modal-footer">
    <button type="button" class="btn blue-btn" data-dismiss="modal">Close</button>
</div>