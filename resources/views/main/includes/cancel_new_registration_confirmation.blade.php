<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="cancellationModal">
        Cancellation
    </h4>
</div>

{!! Form::open([
    'method' => 'POST',
    'url' => route('new_registrations.cancel'),
]) !!}
<div class="modal-body">
    <p>Are you sure you want to cancel this registrations?</p>

    @if (count($customers))
        @foreach($customers as $customer)
            {!! Form::input('hidden', 'class_registration_ids[]', $customer['id']) !!}
        @endforeach
    @endif

    <div class="text-right">
        <button type="button" id="notification-cancel" class="btn blue-btn" data-dismiss="modal">Close</button>
        <button type="submit" id="cancel-new-registration-btn" class="btn blue-btn">Yes</button>
    </div>
</div>
{!! Form::close() !!}