<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- 3rd Party CSS -->
    <link href="{{ public_path('/css/print.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Playfair+Display+SC" rel="stylesheet">
    <style>
        html, body {
            height: 203mm;
            width: 297mm;
            font-size: 1em;
        }
        .table-bordered td { padding:5px; }
        .text-center {page-break-after: always;text-align: center;}

        @media print {
            .text-center {page-break-after: always;}
        }

    </style>

</head>

<body>
<div class="container">

    @foreach($students->chunk(10) as $s)

    <div class="text-center" >
        <br/>

        <h4 class="course">{{ $course }}</h4>
        <h4 class="instructor">{{ $instructor }}</h4>
        <h4 class="address">{{ $address }} {{ $date }}</h4>
        <br/>

        <table class="table-bordered">
            <thead>
                <tr>
                    <td width="1%">Attended?</td>
                    <td>Name</td>
                    <td>Company</td>
                    <td>State</td>
                    <td width="20%">Remarks</td>
                    <td width="1%">Certificate Delivered</td>
                </tr>
            </thead>
            <tbody>
            @foreach($s as $student)
                <tr>
                    <td></td>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->company }}</td>
                    <td>{{ $student->state }}</td>
                    <td></td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
        @endforeach
</div>
</body>
</html>