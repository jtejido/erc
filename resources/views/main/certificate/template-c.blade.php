<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- 3rd Party CSS -->
    <link href="{{ public_path('/css/print.css') }}" rel="stylesheet">
    <style>
        html, body {
            width: 203mm;
            height: 296mm;
            font-size: 1.1em;
            padding: 0;
            margin: 0;
        }
        .container { margin: 0; padding: 0; }
        .table-bordered td { padding:5px; }
        .text-center {text-align: center;}
        .list-container {page-break-after: always;}


    </style>

</head>

<body>
<div class="container">

    @foreach($students->chunk(15) as $s)
        <div class="list-container">
        <div class="text-center">
            <br/><br/><br/><br/>

            <h3 class="course">{{ $course }}</h3>
            <h3 class="instructor">{{ $instructor }}</h3>
            <h3 class="address">{{ $address }} {{ $date }}</h3>
            <br/>
        </div>
        <div>

            <table class="table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th width="45%">Name</th>
                    <th>Signature</th>
                </tr>
                </thead>
                <tbody>
                @foreach($s as $student)
                    <tr>
                        <td>{{ $student->name }}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        </div>
    @endforeach
</div>
</body>
</html>