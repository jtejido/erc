<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- 3rd Party CSS -->
    <link href="{{ public_path('/css/print.css') }}" rel="stylesheet">
    <style>
        html, body {
            height: 203mm;
            width: 297mm;
            font-size: 1.2em;
            margin: 0; padding: 0;
        }
        h1, h2 {padding: 0; margin: 0;}
        .fill-25 { height: 65mm; }
        .fill-35 { height: 83mm; }
        .list-label { height: 30mm; }
        .table-bordered td { padding:5px; }
        .text-center {text-align: center;}
        .list-container {page-break-after: always;}
        h1 {font-size: 2.3em; }


    </style>

</head>

<body>
<div class="container">

    @foreach($students->chunk(2) as $s)

        <div class="list-container text-center">


            <div class="fill-25">&nbsp;</div>
            <div class="list-label">
                <h1>{{ $s->first()->name }}</h1>
                <h2>{{ $s->first()->city }}, {{ $s->first()->state }} / {{ $s->first()->title }}</h2>
            </div>
            @if($s->count() > 1)
            <div class="fill-35">&nbsp;</div>
            <div class="">
                <h1>{{ $s->last()->name }}</h1>
                <h2>{{ $s->last()->city }}, {{ $s->last()->state }} / {{ $s->last()->title }}</h2>
            </div>
            @endif


        </div>

    @endforeach
</div>
</body>
</html>