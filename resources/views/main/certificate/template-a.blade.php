<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<!-- 3rd Party CSS -->
		<link href="{{ public_path('/css/print.css') }}" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Dancing+Script|Playfair+Display+SC" rel="stylesheet">
		<style>
			.container {
				border: 20px solid black;
				height: 100%;
				background-image: url("{{ public_path('/img/ERC-LOGO-GREY.jpg')  }}");
				background-position: center center;
				background-repeat: no-repeat;
			}
			.text-center {
				text-align: center;
			}
			h1.title {
				margin-top: 25px;
				margin-bottom: 25px;
				font-family: 'Playfair Display SC', serif;
				font-size: 4em;
			}
			.tbl-container {
				position: relative;
				bottom: 15px;
			}
			.tbl-container-img {
				position: relative;
				bottom: 45px;
			}
			.tbl-img {
				position: relative;
				bottom: 80px;
			}
			.tbl-ceu {
				position: relative;
				top: 27px;
			}
			.td-name {
				border-top: 1px solid black;
				text-align: center;
				width: 300px;
			}
			.td-sig {
				font-family: 'Dancing Script', cursive;
				text-align: center;
				font-size: 1.2em;
			}
			.td-img {
				position: relative;
				top: 40px;
				width: 100%;
			}
			.td-cert {
				position: relative;
				bottom: 110px;
			}
			/*.td-cert {*/
				/*text-align: right;*/
				/*padding-top: 20px;*/
			/*}*/
		  	html, body {
			    height: 203mm;
			    width: 297mm;
			    font-size: 1.1em;
			}
			h2.course {
				font-weight: bold;
			}
			span.name {
				font-weight: bold;
				/*text-decoration: underline;*/
			}

		</style>

	</head>

	<body>
		<div class="container">
			<div class="text-center">
				<h1 class="title">Certificate</h1>
				<p>
				This is to certify that<br/>
				<span class="name">{{$name}}</span><br/>
				{{$title}}<br/>
				has successfully completed
				</p>
				<h2 class="course">{{$course}}</h2>
				<p>
				{{ $citation }}
				</p>
				<p>presented by</p>
				<h2 class="course">{{ trans('erc.name') }}</h2>
				<h3 class="address">{{ trans('erc.address') }}</h3>
				<h4>{{ trans('erc.url') }}</h4>
			</div>
			<br/><br/>
			<div>
				<div class="col-xs-4 col-md-4 @if ($sig_is_image){{ 'tbl-container-img' }}@else{{ 'tbl-container' }}@endif">
					@if($instructor != '')
						<table class="@if ($sig_is_image){{ 'tbl-img' }}@endif">
							<tr>
								<td class="@if (!$sig_is_image){{ 'td-sig' }}@endif">
									@if ($sig_is_image)
										<img class="td-img" src="{{ public_path('signatures/'.$instructor_sig) }}"/>
									@else
										{{$instructor_sig}}
									@endif
								</td>
							</tr>
							<tr><td class='td-name'>{{$instructor}}, Instructor</td></tr>
						</table>
					@else
						&nbsp;
					@endif
				</div>
				@if ($in_transportation)
					<div class="col-xs-3 col-md-3 tbl-ceu" style="margin-left: 8.33333333%">
						<table>
							<tr><td class="text-center"></td></tr>
							<tr><td class='td-name'>Employer</td></tr>
						</table>
					</div>
				@endif
				<div class="col-xs-3 col-md-3 pull-right">
					@if($date != '')
						<table>
							<tr><td class="text-center">{{$date}}</td></tr>
							<tr><td class='td-name'>Date</td></tr>
						</table>
					@else
						&nbsp;
					@endif
				</div>
			</div>
			<br/>
			<div>
				<div class="col-xs-12 text-right @if($sig_is_image){{ 'td-cert' }}@endif">
					<i>Certificate Number:</i>&nbsp;{{$cert_num}}
				</div>
			</div>
		</div>
	</body>
</html>