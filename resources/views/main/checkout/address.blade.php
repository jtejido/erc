<div class="panel-details panel">
    <div class="panel-inner panel-heading">
        <h2> Billing Address</h2>
    </div>
        <div class="panel-inner panel-body">
        <div class="form-group col-md-8 checkbox">
            {!! Form::checkbox('address_same_as_billing', '',
                $userDetails->address_same_as_billing ? true : false, [
                    'class' => 'same-as',
                    'id'    => 'address_same_as_billing',
                    'style' => 'display: inline',
            ]) !!}
        </div>
        <div class="address_same_as_billing" >
            <div class="form-group col-md-8">
                {!! Form::input('text', 'billing_address', isset($userDetails) ? $userDetails->billing_address : '', [
                'class' => 'form-control',
                'id'    => 'billing_address',
                'style' => 'display: inline;',
                'required' => 'required',
                ]) !!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::input('text', 'billing_city', isset($userDetails) ? $userDetails->billing_city : '', [
                'class' => 'form-control',
                'id'    => 'billing_city',
                'style' => 'display: inline;',
                'required'  => 'required'
                ]) !!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::select('billing_state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->billing_state : '', [
                    'id'    => 'billing_state',
                    'class' => 'form-control',
                    'style' => 'display: inline;',
                    'required' => 'required'
                ]) !!}
            </div>
            <div class="form-group col-md-8">
                {!! Form::input('text', 'billing_zip', isset($userDetails) ? $userDetails->billing_zip : '', [
                    'class' => 'form-control',
                    'id'    => 'billing_zip',
                    'style' => 'display: inline;',
                    'required'  => 'required'
                ]) !!}
            </div>
        </div>
    </div>
</div>