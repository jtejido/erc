<h3 class="panel-title item-title">{{ $title }}</h3>

<div class="row summary-info">
    <div class="col-xs-12 attendees-info">
        <ul class="list">
            @foreach ($classRegistrations as $classRegistration)
                <li>
                    {{ ucfirst($classRegistration->contact->first_name) }} {{ ucfirst($classRegistration->contact->last_name) }}
                    <span class="price pull-right">${{ $classRegistration->item_charge }}</span>
                </li>
            @endforeach
        </ul>

        <div class="subtotal">Subtotal<span class="total-amount pull-right">${{ number_format($total, 2) }}</span></div>
    </div>
</div>