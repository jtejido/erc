<div class="row item-book">
    <input type="hidden" name="total_price" class="total_price" value="{{ $total_notax }}"/>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-9"><h3 class="panel-title item-title" data-book="{{ $book->name }}" data-weight="{{ ($book->weight * $productOrder->quantity) }}">{{ $book->name }} </h3></div>
            <div class="col-xs-3"><span class="price pull-right">{{ $productOrder->product_price }} x {{ $productOrder->quantity }}</span></div>
        </div>
    </div>

    <div class="col-xs-12 attendees-info">
        <div class="subtotal">Subtotal<span class="total-amount pull-right">${{ number_format($total, 2) }}</span></div>
    </div>
</div>