@if (!Auth::user() || Auth::user()->isMember())
    <div class="col-md-5 checkout-wrapper">
        <div class="payment-details panel">
            <div class="panel-inner panel-heading">
                <h2> Order Summary </h2>
            </div>
            <div class="panel-inner panel-body">
                @if ($items)

                    @foreach ($items as $item)
                        {!! OrderUtility::extractItems($item, $user, true) !!}
                    @endforeach

                    @if ($order && (($combinationDiscounts && $combinationDiscounts->count()) ||
                                    ($halfPriceDiscounts && $halfPriceDiscounts->count()) ||
                                    ($couponDiscounts && $couponDiscounts->count())) ||
                                    ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count()) ||
                                    ($militaryDiscounts && $militaryDiscounts->count()) ||
                                    ($subscriptionDiscounts && $subscriptionDiscounts->count()) ||
                                    ($creditDiscounts && $creditDiscounts->count()))

                        <h3 class="panel-title item-title order-discount-title">Discounts Summary</h3>

                        <div class="row summary-info">
                            @if ($subscriptionDiscounts && $subscriptionDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">Yearly Subscription Discount</p>
                                    <ul class="list">
                                        @foreach ($subscriptionDiscounts as $subscriptionDiscount)
                                            <li>
                                                {{ DiscountUtility::extractSubscriptionDiscount($subscriptionDiscount) }}
                                                <span class="price pull-right">${{ $subscriptionDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal
                                        <span class="total-amount pull-right">${{ number_format($subscriptionDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($combinationDiscounts && $combinationDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">HWM DOT Registration</p>
                                    <ul class="list">
                                        @foreach ($combinationDiscounts as $combinationDiscount)
                                            <li>
                                                {{ DiscountUtility::extractClassCombinationInfo($combinationDiscount) }}
                                                <span class="price pull-right">${{ $combinationDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal
                                        <span class="total-amount pull-right">${{ number_format($combinationDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($halfPriceDiscounts && $halfPriceDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">Group Discounts</p>
                                    <ul class="list">
                                        @foreach ($halfPriceDiscounts as $halfPriceDiscount)
                                            <li>
                                                {{ DiscountUtility::extractHalfPriceInfo($halfPriceDiscount) }}
                                                <span class="price pull-right">${{ $halfPriceDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal
                                        <span class="total-amount pull-right">${{ number_format($halfPriceDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($couponDiscounts && $couponDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">Coupon Discounts</p>
                                    <ul class="list">
                                        @foreach ($couponDiscounts as $couponDiscount)
                                            <li>
                                                {{ DiscountUtility::extractCouponDiscountInfo($couponDiscount) }}
                                                <span class="price pull-right">${{ $couponDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal
                                        <span class="total-amount pull-right">${{ number_format($couponDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">Order Adjustment Discounts</p>
                                    <ul class="list">
                                        @foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount)
                                            <li>
                                                {{ DiscountUtility::extractOrderAdjustment($orderAdjustmentDiscount) }}
                                                <span class="price pull-right">${{ $orderAdjustmentDiscount->adjusted_deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal<span class="total-amount pull-right">${{ number_format($orderAdjustmentDiscounts->sum('computed_deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($militaryDiscounts && $militaryDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">GSA Pricing Discounts</p>
                                    <ul class="list">
                                        @foreach ($militaryDiscounts as $militaryDiscount)
                                            <li>
                                                {{ DiscountUtility::extractMilitaryDiscount($militaryDiscount) }}
                                                <span class="price pull-right">${{ $militaryDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal<span class="total-amount pull-right">${{ number_format($militaryDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($creditDiscounts && $creditDiscounts->count())
                                <div class="col-xs-12">
                                    <p class="attendees">Corporate Seat Discounts</p>
                                    <ul class="list">
                                        @foreach ($creditDiscounts as $creditDiscount)
                                            <li>
                                                {{ DiscountUtility::extractCreditDiscount($creditDiscount) }}
                                                <span class="price pull-right">${{ $creditDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="subtotal">
                                        Subtotal<span class="total-amount pull-right">${{ number_format($creditDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <hr/>

                    @endif

                    @if (!is_null($total))
                        <div class="row">
                            <div class="col-xs-12 sub-total-box">
                                <p class="info-box-text">Total</p>
                                <span class="info-box-number pull-right total-price">${{ number_format($total, 2) }}</span>
                            </div>
                        </div>
                    @endif
                @else
                    <i>No Items.</i>
                @endif
                    <div class="loading" style="display:none; position: absolute; top: 50%; left: 45%">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
            </div>
        </div>
    </div>
@else
    <div class="col-md-5">
        <div class="panel panel-default order-items">
            <div class="panel-heading display-table" >
                <div class="row display-tr" >
                    <h3 class="panel-title display-td" >Payable Summary</h3>
                </div>
            </div>
            <div class="panel-body">
                @if ($items)

                    @foreach ($items as $item)
                        {!! OrderUtility::extractItems($item, $user, true) !!}
                    @endforeach

                    @if ($order && (($combinationDiscounts && $combinationDiscounts->count()) ||
                                    ($halfPriceDiscounts && $halfPriceDiscounts->count()) ||
                                    ($couponDiscounts && $couponDiscounts->count())) ||
                                    ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count()) ||
                                    ($militaryDiscounts && $militaryDiscounts->count()) ||
                                    ($subscriptionDiscounts && $subscriptionDiscounts->count()) ||
                                    ($creditDiscounts && $creditDiscounts->count()))

                        <h3 class="panel-title item-title">Discounts Summary</h3>

                        <div class="row summary-info">
                            @if ($subscriptionDiscounts && $subscriptionDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">Yearly Subscription Discounts</p>
                                    <ul class="list">
                                        @foreach ($subscriptionDiscounts as $subscriptionDiscount)
                                            <li>
                                                {{ DiscountUtility::extractSubscriptionDiscount($subscriptionDiscount) }}
                                                <span class="price pull-right">${{ $subscriptionDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p>
                                        <span class="total-amount">${{ number_format($subscriptionDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($combinationDiscounts && $combinationDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">HWM and DOT Registration</p>
                                    <ul class="list">
                                        @foreach ($combinationDiscounts as $combinationDiscount)
                                            <li>
                                                {{ DiscountUtility::extractClassCombinationInfo($combinationDiscount) }}
                                                <span class="price pull-right">${{ $combinationDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p>
                                        <span class="total-amount">${{ number_format($combinationDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($halfPriceDiscounts && $halfPriceDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">Group Discounts</p>
                                    <ul class="list">
                                        @foreach ($halfPriceDiscounts as $halfPriceDiscount)
                                            <li>
                                                {{ DiscountUtility::extractHalfPriceInfo($halfPriceDiscount) }}
                                                <span class="price pull-right">${{ $halfPriceDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p>
                                        <span class="total-amount">${{ number_format($halfPriceDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($couponDiscounts && $couponDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">Coupon Discounts</p>
                                    <ul class="list">
                                        @foreach ($couponDiscounts as $couponDiscount)
                                            <li>
                                                {{ DiscountUtility::extractCouponDiscountInfo($couponDiscount) }}
                                                <span class="price pull-right">${{ $couponDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p><span class="total-amount">${{ number_format($couponDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">Order Adjustment Discounts</p>
                                    <ul class="list">
                                        @foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount)
                                            <li>
                                                {{ DiscountUtility::extractOrderAdjustment($orderAdjustmentDiscount) }}
                                                <span class="price pull-right">${{ $orderAdjustmentDiscount->adjusted_deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p><span class="total-amount">${{ number_format($orderAdjustmentDiscounts->sum('computed_deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($militaryDiscounts && $militaryDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">GSA Pricing Discounts</p>
                                    <ul class="list">
                                        @foreach ($militaryDiscounts as $militaryDiscount)
                                            <li>
                                                {{ DiscountUtility::extractMilitaryDiscount($militaryDiscount) }}
                                                <span class="price pull-right">${{ $militaryDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p><span class="total-amount">${{ number_format($militaryDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif

                            @if ($creditDiscounts && $creditDiscounts->count())
                                <div class="col-xs-12 attendees-info">
                                    <p class="attendees">Corporate Seat Discounts</p>
                                    <ul class="list">
                                        @foreach ($creditDiscounts as $creditDiscount)
                                            <li>
                                                {{ DiscountUtility::extractCreditDiscount($creditDiscount) }}
                                                <span class="price pull-right">${{ $creditDiscount->deduction }}</span>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="total">
                                        <p>Total : </p><span class="total-amount">${{ number_format($creditDiscounts->sum('deduction'), 2) }}</span>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <hr>

                    @endif

                    @if ($total > 0)
                        <div class="row">
                            <div class="col-md-12 sub-total-box">
                                <span class="info-box-text">Total :</span>
                                <span class="info-box-number">${{ number_format($total, 2) }}</span>
                            </div>
                        </div>
                    @endif
                @else
                    <i>No Items.</i>
                @endif
                    <div class="loading" style="display:none; position: absolute; top: 50%; left: 45%">
                        <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
            </div>
        </div>
    </div>
@endif


