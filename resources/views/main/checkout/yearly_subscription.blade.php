<h3 class="panel-title item-title">Yearly Subscription</h3>

<div class="row summary-info">
    <div class="col-xs-12 attendees-info">
        <p class="subscribers">Subscribers :</p>
        <ul class="list">
            @foreach ($attendees as $attendee)
                <li>
                    {{ ucfirst($attendee->agent->contact->first_name) }} {{ ucfirst($attendee->agent->contact->last_name) }}
                    <span class="price pull-right">${{ $attendee->temporary_price }}</span>
                </li>
            @endforeach
        </ul>

        <div class="subtotal">Subtotal<span class="total-amount pull-right">${{ number_format($total, 2) }}</span></div>
    </div>
</div>

<hr>