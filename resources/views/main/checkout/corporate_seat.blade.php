<h3 class="panel-title item-title">Corporate Seat</h3>

<div class="row summary-info">
    <div class="col-xs-12 attendees-info">
        <ul class="list">
            {{ $corpSeat->credits }} Seats
            <span class="price pull-right">${{ $corpSeat->price }}</span>
        </ul>

        <div class="subtotal">Subtotal<span class="total-amount pull-right">${{ number_format($total, 2) }}</span></div>
    </div>
</div>

<hr>