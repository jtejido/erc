<div class="col-md-7">
    <div class="panel panel-default credit-card-box" id="card-payment-form">
        <div class="panel-heading display-table" >
            <div class="row display-tr" >
                <h3 class="panel-title display-td" >Card Payment Details</h3>
                <div class="display-td">
                    <img class="img-responsive" src="/img/accepted_cc.png">
                </div>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open([
                'route' => ['admin.card_checkout', 'user_id' => $user->id]])
            !!}

            <input type="hidden" name="skip-email-notification" value="1" />

            @include('errors.form_errors')

            @if (Session::has('error_message'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {!! Session::get('error_message') !!}
                </div>
            @endif

            @if (isset($order))
                {!! Form::input('hidden', 'order_id', $order->id) !!}
            @endif

            @if (isset($total))
                {!! Form::input('hidden', 'amount', $total) !!}
                {!! Form::input('hidden', 'current_total', $total) !!}
                {!! Form::input('hidden', 'shipping_fee', 0, [ 'class' => 'shipping_fee' ]) !!}
                {!! Form::input('hidden', 'state_tax', 0, [ 'class' => 'state_tax' ]) !!}
                {!! Form::input('hidden', 'user_id', $user->id) !!}
                {!! Form::input('hidden', 'description', 'Payment of Order') !!}
            @endif

            @if (isset($items)>0)
                @foreach ($items as $item)
                    <input type="hidden" name="items[]" value="{{ $item->id }}" />
                @endforeach
            @endif

            <div class="row">
                <div class="col-xs-4 col-md-3">
                    <div class="form-group">
                        {!! Form::rLabel('first_name', 'First Name') !!}
                        {!! Form::input(
                            'text',
                            'first_name',
                            $user->contact->first_name, [
                                'placeholder' => 'John',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
                <div class="col-xs-4 col-md-3">
                    <div class="form-group">
                        {!! Form::rLabel('last_name', 'Last Name') !!}
                        {!! Form::input(
                            'text',
                            'last_name',
                            $user->contact->last_name, [
                                'placeholder' => 'Doe',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
                <div class="col-xs-4 col-md-6">
                    <div class="form-group">
                        {!! Form::rLabel('email', 'Email') !!}
                        {!! Form::input(
                            'text',
                            'email',
                            $user->email, [
                                'placeholder' => 'john@example.com',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
            </div>

            <hr>

            <div class="row card-info">
                <div class="col-xs-7 col-md-7">
                    <div class="form-group">
                        {!! Form::rLabel('card_number', 'Credit Card Number') !!}
                        <div class="input-group">
                            {!! Form::input(
                                'text',
                                'card_number',
                                '', [
                                    'placeholder' => 'Credit Card Number',
                                    'class' => 'form-control numeric-only',
                                    'required' => 'required'
                                 ]
                            ) !!}
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        </div>
                    </div>
                </div>


                <div class="col-xs-3 col-md-5">
                    <div class="form-group">
                        {!! Form::rLabel('cvv', 'CVV') !!}
                        {!! Form::input(
                            'text',
                            'cvv',
                            '', [
                                'placeholder' => 'Security',
                                'class' => 'form-control numeric-only',
                                'required' => 'required'
                             ]
                        ) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-7 col-md-7">
                    <div class="form-group">
                        {!! Form::rLabel('exp_month', 'Expiration Month') !!}
                        {!! Form::select('exp_month', $months, '', [
                            'class' => 'form-control',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="col-xs-3 col-md-5">
                    <div class="form-group">
                        {!! Form::rLabel('exp_year', 'Expiration Year') !!}
                        {!! Form::select('exp_year', $years, '', [
                            'class' => 'form-control',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">

                <div class="col col-xs-12 col-sm-6">
                    <div class="billing_container">
                        <label><strong>Billing Address</strong></label>
                        <div class="panel-form-address">
                            <input type="hidden" name="billing_custom" class="custom_flag" value="1"/>
                            <div class="">
                                <div class="form-group">
                                    <label for="billing_company">Company/Name:</label>
                                    <input name="billing_company" type="text" class="form-control" required
                                           value="{{ $order->billing_company }}" />
                                </div>
                                <div class="form-group">
                                    <label for="billing_address1">Address:</label>
                                    <input name="billing_address1" type="text" class="form-control" required
                                           value="{{ $order->billing_address }}" />
                                </div>
                                <div class="form-group">
                                    <label for="billing_city">City:</label>
                                    <input name="billing_city" type="text" class="form-control" required
                                           value="{{ $order->billing_city }}" />
                                </div>
                                <div class="form-group">
                                    <label for="billing_state">State:</label>
                                    {!! Form::select('billing_state', $states, $order->billing_state, [
                                            'class' => 'form-control'
                                        ]) !!}
                                </div>
                                <div class="form-group">
                                    <label for="billing_zip">Zip:</label>
                                    <input name="billing_zip" type="number" class="form-control" maxlength="5" size="5" required
                                           value="{{ $order->billing_zip }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col col-xs-12 col-sm-6">
                    <div class="shipment_container">
                        <label><strong>Shipping Address</strong></label>
                        <div class="panel-form-address">
                            <input type="hidden" name="shipping_rates" value=""/>
                            <input type="hidden" name="shipping_custom" class="custom_flag" value="1"/>
                            <input type="hidden" name="tax_rate" value="0"/>
                            <input type="hidden" name="shipment_state_default" value="{{ $order->shipping_state }}"/>
                            <div class="">
                                <div class="form-group">
                                    <label for="shipment_company">Company/Name:</label>
                                    <input name="shipment_company" type="text" class="form-control" required
                                           value="{{ $order->shipping_company }}" />
                                </div>
                                <div class="form-group">
                                    <label for="shipment_address1">Address:</label>
                                    <input name="shipment_address1" type="text" class="form-control" required
                                           value="{{ $order->shipping_address }}" />
                                </div>
                                <div class="form-group">
                                    <label for="shipment_city">City:</label>
                                    <input name="shipment_city" type="text" class="form-control" required
                                           value="{{ $order->shipping_city }}" />
                                </div>
                                <div class="form-group">
                                    <label for="shipment_state">State:</label>
                                    {!! Form::select('shipment_state', $states, $order->shipping_state, [
                                        'class' => 'form-control'
                                    ]) !!}
                                </div>

                                <div class="form-group" style="display: none">
                                    <label for="shipment_county">County:</label>
                                    {!! Form::select('shipment_county', $counties, $county, [
                                        'class' => 'form-control',
                                    ]) !!}
                                </div>

                                <div class="form-group">
                                    <label for="shipment_zip">Zip:</label>
                                    <input name="shipment_zip" type="number" class="form-control" maxlength="5" size="5" required
                                           value="{{ $order->shipping_zip }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-flat btn-warning btn-lg btn-block btn-pay"
                            type="submit"
                            @if (!isset($order)) disabled @endif>
                        Place Order</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>