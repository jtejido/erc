@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Successful Payment </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="cart">
                <div class="inner col-md-8 col-md-offset-2">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        Your order has been processed. An email is coming to confirm your purchase.
                        <a href="{{ route('user.home') }}">Click here</a> to view transaction
                    </div>
                </div>
            </div>
        </div>

        @include('includes.notification_modal')
    </section>
@endsection

@section('javascripts')
    @parent

    @if (Session::has('purchase') and Session::get('purchase'))
        <script>
            gtag('event', 'purchase', {
                "transaction_id": "{{ $transaction_id }}",
                "value": "{{ $value }}",
                "currency": "USD",
                "tax": "{{ $tax }}",
                "shipping": "{{ $shipping }}",
                "items": {!! json_encode($items) !!}
            });
        </script>

        <?php Session::forget('purchase'); ?>
    @endif

    @if(session('TYPE') == 'COURSE')
        @if(env('APP_ENV') == 'production')
            <!-- Google Code for Purchase Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1071984319;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "UUfnCPOX4HUQv92U_wM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/1071984319/?label=UUfnCPOX4HUQv92U_wM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        @else
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1017959962;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "k8AlCJbK4HUQmqyz5QM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1017959962/?label=k8AlCJbK4HUQmqyz5QM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        @endif
    @elseif(session('TYPE') == 'PRODUCT')
        @if(env('APP_ENV') == 'production')
            <!-- Google Code for Handbooks Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1071984319;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "NLPICLX25XUQv92U_wM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/1071984319/?label=NLPICLX25XUQv92U_wM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        @else
        <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 1017959962;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "uoTmCJ365nUQmqyz5QM";
            var google_remarketing_only = false;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1017959962/?label=uoTmCJ365nUQmqyz5QM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
        @endif
    @elseif(session('TYPE') == 'PRODUCT_COURSE')
        @if(env('APP_ENV') == 'production')
            <!-- Google Code for Book + Course Conversion Conversion Page -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1071984319;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "PIkDCPbU4XUQv92U_wM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript"
                    src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt=""
                         src="//www.googleadservices.com/pagead/conversion/1071984319/?label=PIkDCPbU4XUQv92U_wM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        @else
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 1017959962;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "cXUZCMuJ0nUQmqyz5QM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display:inline;">
                    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1017959962/?label=cXUZCMuJ0nUQmqyz5QM&amp;guid=ON&amp;script=0"/>
                </div>
            </noscript>
        @endif

    @endif
@endsection