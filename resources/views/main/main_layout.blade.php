@extends('layout')

@section('stylesheets')
    @parent

    {!! HTML::style(elixir('css/app.css')) !!}
    {!! HTML::style(elixir('css/admin-lte.css')) !!}
    {!! HTML::style(elixir('css/admin-lte-2.css')) !!}

    <style>
        .numbered-list {
            list-style: decimal;
        }
        .numbered-list li {
            margin-left: 30px;
            line-height: 1.5em;
        }
    </style>

    {!! HTML::script(elixir('js/admin/angular.js')) !!}
    {!! HTML::script(elixir('js/admin/angular/app.js')) !!}

    @include('main.includes.google_analytics')

@endsection

@section('body')

    @section('header')
        {{-- Header Section --}}
    @show

    <div class="container main-wrapper">
        @yield('main_content')
    </div>

    {{--<section id="contact_us">--}}
        {{--<div class="container">--}}
            {{--<div class="inner">--}}
                {{--<div class="row col-md-8 col-md-offset-2">--}}
                    {{--<div class="col-sm-9 column description-container" style="height: 130px !important;">--}}
                        {{--<div class="button-wrapper">--}}
                            {{--<div class="inner-wrap">--}}
                                {{--<h5>Find out what makes our company special.</h5>--}}
                        {{--<h5> Environmental Resource Center understands that your company is unique. </h5>--}}
                        {{--<span class="description"> That's why we strive to ensure that our services and training programs always meet your specific needs.--}}
                            {{--<a href="{{ url("/contact-us")  }}" style="text-decoration: underline;">Find out what makes our company special.</a></span>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-3 button-container column" style="height: 130px !important;">--}}
                        {{--<div class="button-wrapper">--}}

                            {{--<div class="inner-wrap">--}}

                                {{--<button><a href="/contact-us">CONTACT US</a></button>--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    <footer>
        <div id="footer">
            <div class="row">
                <div class="col-sm-3">
<pre>
Environmental Resource Center &reg;
101 Center Pointe Drive
Cary, North Carolina 27513-5706</pre>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <li><a href="/contact-us" >Contact Us</a></li>
                        <li><a href="" data-toggle="modal" class="ppButtonModal">Privacy Policy</a></li>
                        <li><a href="" data-toggle="modal" class="siteButtonModal">Sitemap</a></li>
                    </ul>
                    <p>
                        © {{ date('Y') }} Environmental Resource Center &reg;. All rights reserved.
                    </p>
                </div>
                <div class="col-sm-3">
                    <h3 class="phone"><a href="tel:+8005372372"><i class="fa fa-phone-square" aria-hidden="true"></i> 800-537-2372</a></h3>
                </div>
            </div>
        </div>
    </footer>
@stop

@section('javascripts')
    @parent

    {!! HTML::script(elixir('js/admin/admin-lte.js')) !!}
    {!! HTML::script(elixir('js/frontend/frontend.js')) !!}

    <script>
        var Comm100API=Comm100API||{};(function(t){function e(e){var a=document.createElement("script"),c=document.getElementsByTagName("script")[0];a.type="text/javascript",a.async=!0,a.src=e+t.site_id,c.parentNode.insertBefore(a,c)}t.chat_buttons=t.chat_buttons||[],t.chat_buttons.push({code_plan:208,div_id:"live-chat"}),t.site_id=46669,t.main_code_plan=208,e("https://chatserver.comm100.com/livechat.ashx?siteId="),setTimeout(function(){t.loaded||e("https://hostedmax.comm100.com/chatserver/livechat.ashx?siteId=")},5e3)})(Comm100API||{});
        (function($){

            $('.live-chat-img').on('click', function() {
                $('#live-chat').find('a').find('img').click();
            });

            $.get('/cp/session', function(data) {
                var msg = 'The coupon <strong>'+
                        data.coupon_code +
                        '</strong> will be applied on your next transaction.';
                if (data.has_coupon) {
                    $.notify({
                        icon: 'fa fa-info-circle',
                        message: msg}, {
                        placement: {
                            from: "bottom",
                            align: "center"
                        },
                        delay: 0 });
                }
            });

            $('#ppButtonModal').on('click', function(e) {
                e.preventDefault();

                $('#ppModal').modal('show');
            });

            $('#siteButtonModal').on('click', function(e) {
                e.preventDefault();

                $('#siteModal').modal('show');
            });
        })(jQuery);

    </script>

@endsection