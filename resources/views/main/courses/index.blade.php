@extends('main.main_layout')

@section('meta')
    <meta name="title" content="Our Courses | Environmental Resource Center">
    <meta name="description" content="Our courses are carefully selected and specifically tailored for your needs. We provide the most relevant course for everyone. Click here to register.">
@endsection

@section('title')
    Our Courses | Environmental Resource Center
@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Course Listing  </h1>
                    </div>
                </div>
            </div>
        </div>
        <div style="max-width:1170px" class="course-filters-container container">
            <div class="courses col-md-10 col-md-offset-1 hidden-xs">
                <div class="inner">

                    <div class="row icons-container">
                        <div class="main-class col-sm-2 col-xs-5">
                            <a href="/courses" class="all"></a>
                            <label> All </label>
                        </div>
                        <div class="main-class col-sm-2 col-xs-5">
                            <a href="/courses?type=1" class="seminars"></a>
                            <label> Seminars </label>
                        </div>
                        <div class="main-class col-sm-2 col-xs-5">
                            <a href="/courses?type=2" type="button" class="webinars"></a>
                            <label> Webcasts </label>
                        </div>
                        <div class="main-class col-sm-2 col-xs-5">
                            <a href="/courses?type=3" class="cbt"></a>
                            <label class="schedule"> Online <br/> Training </label>
                        </div>
                        <div class="main-class col-sm-2 col-xs-5">
                            <a href="/class_calendar" class="seminars"></a>
                            <label class="schedule"> Schedule <br>of Classes </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filters col-md-10 col-md-offset-1">

                <div class="inner">
                    <div class="row">
                        {!! Form::open(['id' => 'filters', 'method' => 'GET']) !!}
                            <div class="col-sm-2">
                                <div class="wrap">
                                    <label for="type">Type </label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">All</option>
                                        @foreach ($courseTypes as $type)
                                            <option value="{{ $type->id }}" @if($typeFilter == $type->id) selected @endif>{{ $type->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="wrap">
                                    <label for="type">Location </label>
                                    <select name="location" id="location" class="form-control">
                                        <option value="">All</option>
                                        @foreach ($generalLocations as $location)
                                            <option value="{{ $location->id }}" @if($locationFilter == $location->id) selected @endif>{{ $location->location }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="wrap">
                                    <label for="type">Category </label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="">All</option>
                                        @foreach ($courseCategories as $category)
                                            <option value="{{ $category->id }}" @if($categoryFilter == $category->id) selected @endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="wrap">
                                    <label>Keyword</label>
                                    <input type="text" name="searchq" class="search" value="{{ $searchq }}">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" value="GO" class="go">
                                <a href="{{ route('courses') }}" class="go">Show All</a>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="course-listings">

            @if ($categories)
                @foreach ($categories as $category)

                        {{--
                           */ $total = 0;
                              $display = 'block';
                           /*
                       --}}
                        @foreach($courseTypes as $courseType)
                            {{--
                                */
                                $courses = CourseUtility::getCoursesGivenParams($category, $searchq, $courseType->id, $locationFilter);
                                $total += $courses->count();
                                /*
                            --}}
                        @endforeach

                        @if (!$total)
                            {{-- */ $display = 'none'; /* --}}
                        @endif
                    <div class="content-wrapper-course content-wrapper-course-list" style="display: {{ $display }};">
                        <div class="container">
                            <div class="course-list-wrapper row col-md-10 col-md-offset-1">
                                <div class="col-md-5 column">
                                    <div class="{{ str_slug( $category->name ) }} col-image">
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h4>{{ $category->name }}</h4>
                                    @if($typeFilter)

                                        @if (CourseUtility::getCoursesGivenParams($category, $searchq, $typeFilter, $locationFilter)->count())
                                            <ul>
                                                @foreach (CourseUtility::getCoursesGivenParams($category, $searchq, $typeFilter, $locationFilter) as $type => $course)
                                                    <li><a href="{{ route('course.show', ['id' => $course->slug])  }}">{{ $course->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        @else
                                            <p class="no-course"><i>No courses found.</i></p>
                                        @endif
                                    @else

                                        @foreach($courseTypes as $courseType)
                                            {{--
                                                */
                                                $courses = CourseUtility::getCoursesGivenParams($category, $searchq, $courseType->id, $locationFilter);
                                                /*
                                            --}}

                                            @if($courses->count())
                                                <div class="class-wrapper">
                                                    <p class="class">
                                                        @if($courseType->id == 3)
                                                            ONLINE TRAINING
                                                        @else
                                                            {{ str_plural($courseType->name) }}
                                                        @endif
                                                    </p>
                                                    <ul>
                                                        @foreach ($courses as $course)
                                                            <li><a href="{{ route('course.show', ['id' => $course->slug])  }}">{{ $course->title }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                        @endforeach

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

            <div class="content-wrapper-course">
                <div class="container">
                    <div class="course-list-wrapper row col-md-12">
                        <div class="col-md-10 col-md-offset-1 subscription-container">
                            <h4 class="col-md-3">Subscriptions</h4>
                            <a href="{{ url('training/subscription') }}" class="col-md-4">Annual Subscription</a>
                            <a href="{{ url('training/subscription') }}" class="btn-corporate-seat col-md-4">Corporate Subscription</a>
                            <img class="live-chat-img" src="/img/erc/live_chat.png" alt="Live Chat" height="36" style="float: right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="live-chat" class="hidden"></div>

    @include('includes.corporate_seat_notification')
@stop

@section('javascripts')
    @parent
    <script>

        (function($) {

            $('#type, #category, #location').on('change', function() {
                $('#filters').submit();
            });

            $('.course-listings .content-wrapper-course-list').each(function () {
                var col_right = $(this).find('.col-md-7'),
                    col_left = $(this).find('.col-md-5');
                if(col_left.height() > col_right.height()) {
                    col_left.find('.col-image').css('display','none');
                }
            });

        })(jQuery);
    </script>
@endsection