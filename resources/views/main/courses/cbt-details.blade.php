@extends('main.main_layout')

@section('meta')
    <meta name="title" content="{{ $course->meta_title }}">
    <meta name="description" content="{{ $course->meta_desc }}">
@endsection

@section('title')
    {{ $course->title }} | Environmental Resource Center
@endsection

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Course Details  </h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="container course-details">


            <div class="inner col-lg-10 col-lg-offset-1">

                <h2 class="text-center title">{{ $course->title }}</h2>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="desc">{!! $course->description !!}</div>
                        @if ($course->courseType->id == \App\Utilities\Constant::COMPUTER_BASED)
                            <p class="text-center">
                                <br/>
                                <a class="register-btn btn-lg btn btn-primary" href="{{ route('contacts.register', ['course_id' => $course->id]) }}">
                                    Register
                                </a>
                                <br/>
                                <br/>
                            </p>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 related">
                        <div class="details-container">
                            <div class="details-header">
                                <img src="/img/details.png" alt="Details Header">
                            </div>
                            <div class="details" style="margin-bottom: 10px">
                                <dl class="dl-horizontal">
                                    <dt>Code</dt>
                                    <dd>{{  $course->sku }}</dd>
                                    <dt>Price</dt>
                                    <dd>${{ $course->price }}</dd>
                                    <dt>Length</dt>
                                    <dd>{{ $course->duration }}</dd>
                                    @if(!empty($course->time))
                                        <dt>Time</dt>
                                        <dd>{{ $course->time }}</dd>
                                    @endif
                                    <dt>Category</dt>
                                    <dd>
                                        @if ($course->courseCategories->count())
                                            <div class="course-category">
                                                @foreach ($course->courseCategories as $courseCat)
                                                    {{ $courseCat->category->name }}<br>
                                                @endforeach
                                            </div>
                                        @endif
                                    </dd>
                                    @if(!empty($course->additional_info))
                                        <dt>More Info</dt>
                                        <dd class="more-info">{{ $course->additional_info }}</dd>
                                    @endif
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 related">

                        <div class="related-course-container">
                            <div class="related-course-header">
                                <img src="/img/courses.png" alt="Details Header">
                            </div>
                            <div class="panel panel-default btn-flat">
                                <div class="panel-body">
                                    <ul class="related-course-list">
                                        @foreach($course->courses as $c)
                                            <li>{{ $c->title }}&nbsp;({!! $c->getLinkedCourseTitle() !!})</li>
                                        @endforeach
                                        @if (!$course->courses->count())
                                            <li><em>Currently, there are no related courses.</em></li>
                                        @endif
                                    </ul>
                                    <hr/>
                                    <p class="text-center"><a href="{{ route('resource.classcalendar') }}" class="btn btn-default"><i class="fa fa-calendar" aria-hidden="true"></i> View Schedule of Classes</a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 related">
                        <div class="related-course-container">
                            <div class="related-course-header">
                                <img src="/img/reviews.jpg" alt="Details Header">
                            </div>
                            <div class="panel panel-default btn-flat">
                                <div class="panel-body">
                                    {!! $course->reviews !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>

        </div>
        <br/>

        @include('_mid-footer-content')
    </section>
@stop
@section('javascripts')
    @parent
    <script type="text/javascript">

        var Comm100API=Comm100API||{};(function(t){function e(e){var a=document.createElement("script"),c=document.getElementsByTagName("script")[0];a.type="text/javascript",a.async=!0,a.src=e+t.site_id,c.parentNode.insertBefore(a,c)}t.chat_buttons=t.chat_buttons||[],t.chat_buttons.push({code_plan:208,div_id:"live-chat"}),t.site_id=46669,t.main_code_plan=208,e("https://chatserver.comm100.com/livechat.ashx?siteId="),setTimeout(function(){t.loaded||e("https://hostedmax.comm100.com/chatserver/livechat.ashx?siteId=")},5e3)})(Comm100API||{})

    </script>
    <script>
        (function($) {
            $('.more-info-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    text: function(api){
                        return $(this).next('.more-info-box').clone();
                    }
                },
                show: {
                    event: 'click'
                },
                hide: {
                    event: 'click mouseleave'
                }
            });

            $('.more-info-btn').on('click', function(e) {
                var $that = $(this);
                e.preventDefault();
                if($that.find('.flag').text() == 'More')
                    $that.find('.flag').text('Less')
                else
                    $that.find('.flag').text('More');
            });


            $('.table-scroll').DataTable( {
                scrollY:        '260px',
                scrollCollapse: true,
                paging:         false,
                info:       false,
                searching:     false,
                columns: [
                    { "width": "150px" },
                    null,
                    { "width": "50px" },
                ]
            } );


            $('.news').slick({
                dots: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    }]
            });

        })(jQuery);
    </script>
@endsection