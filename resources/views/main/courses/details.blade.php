@extends('main.main_layout')

@section('title')
{{ $course->title }} | Environmental Resource Center
@endsection

@section('meta')
    <meta name="title" content="{{ $course->meta_title }}">
    <meta name="description" content="{{ $course->meta_desc }}">
@endsection

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Course Details  </h1>
                    </div>
                </div>
            </div>
        </div>


        <div class="container course-details">


            <div class="inner col-lg-10 col-lg-offset-1">

                <h2 class="text-center title">{{ $course->title }}</h2>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="desc">{!! $course->description !!}</div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="details-container">
                            <div class="details-header">
                                <img src="/img/details.png" alt="Details Header">
                            </div>
                            <div class="details">
                                <dl class="dl-horizontal">
                                    <dt>Code</dt>
                                    <dd>{{  $course->sku }}</dd>
                                    <dt>Price</dt>
                                    <dd>${{ $course->price }}</dd>
                                    <dt>Length</dt>
                                    <dd>{{ $course->duration }}</dd>
                                    @if(!empty($course->time))
                                        <dt>Time</dt>
                                        <dd>{{ $course->time }}</dd>
                                    @endif
                                    <dt>Category</dt>
                                    <dd>
                                    @if ($course->courseCategories->count())
                                        <div class="course-category">
                                            @foreach ($course->courseCategories as $courseCat)
                                                {{ $courseCat->category->name }}<br>
                                            @endforeach
                                        </div>
                                    @endif
                                    </dd>
                                    @if(!empty($course->additional_info))
                                    <dt>More Info</dt>
                                    <dd class="more-info">{{ $course->additional_info }}</dd>
                                    @endif
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="dates-container">
                            <div class="dates-header">
                                @if ($course->courseType->id == \App\Utilities\Constant::WEBCAST)
                                    <img src="/img/erc/class-schedule-header.png" alt="Details Header">
                                @else
                                    <img src="/img/class.png" alt="Details Header">
                                @endif
                            </div>

                            <div class="dates-body">

                            @if ($course->courseType->id == \App\Utilities\Constant::COMPUTER_BASED)
                                <p class="text-center">
                                <br/>
                                    <a class="blue-btn register-btn" href="{{ route('contacts.register', ['course_id' => $course->id]) }}">
                                        Register
                                    </a>

                                </p>
                            @else

                            @if (!$active->isEmpty())
                                <table class="table table-scroll">
                                    <tbody>
                                    @forelse ($active as $class)
                                        <tr>

                                            <td>
                                                {{ $class->dateRangeOnly }}<br/>
                                            </td>

                                            <td>
                                                @if ($course->course_type_id == \App\Utilities\Constant::SEMINAR)
                                                    {{ $class->shortCity }}
                                                @elseif ($course->course_type_id == \App\Utilities\Constant::WEBCAST)
                                                    Webcast
                                                @endif

                                                <small>
                                                    <button class="btn btn-xs btn-default more-info-btn">
                                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                        &nbsp;<span class="flag">More</span> Info
                                                    </button>
                                                    <div class="more-info-box">
                                                        <strong>Time: {{ $class->time }}<br/></strong>
                                                        @if ($course->course_type_id == \App\Utilities\Constant::SEMINAR)
                                                            {!! $class->location_name_link !!}<br/>
                                                            {{ $class->location_address }}<br/>
                                                            Phone: {{ $class->location_phone }}
                                                        @endif
                                                    </div>
                                                </small>


                                            </td>

                                            <td>
                                                <a class="btn btn-info btn-xs"
                                                   href="{{ route('classes.related', [
                                        'class_id'  => $class->id]) }}">
                                                    Register
                                                </a>
                                            </td>

                                        </tr>
                                    @empty
                                        @if ($course->course_type_id != \App\Utilities\Constant::COMPUTER_BASED)
                                            <tr>
                                                <td colspan="5">
                                                    <div class="button-wrap">
                                                        <a class="btn btn-danger pull-right no-class">
                                                            No Classes Offered
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforelse
                                    </tbody>

                                </table>
                                @else
                                    <div class="details">
                                        <p class="text-center">Please contact us if you are interested in this class.</p>
                                    </div>
                            @endif
                            @endif
                            </div>

                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row" style="clear:both">
                    <div class="col-md-6 col-xs-12 related">

                        <div class="related-course-container">
                            <div class="related-course-header">
                                <img src="/img/courses.png" alt="Details Header">
                            </div>
                            <div class="panel panel-default btn-flat">
                                <div class="panel-body">
                                    <ul class="related-course-list">
                                        @foreach($course->courses as $c)
                                            <li>{{ $c->title }}&nbsp;({!! $c->getLinkedCourseTitle() !!})</li>
                                        @endforeach
                                        @if (!$course->courses->count())
                                            <li><em>Currently, there are no related courses.</em></li>
                                        @endif
                                    </ul>
                                    <hr/>
                                    <p class="text-center">
                                        <a href="{{ route('resource.classcalendar') }}" class="btn btn-default"><i class="fa fa-calendar" aria-hidden="true"></i> View Schedule of Classes</a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 related">
                        <div class="related-course-container">
                            <div class="related-course-header">
                                <img src="/img/reviews.jpg" alt="Details Header">
                            </div>
                            <div class="panel panel-default btn-flat">
                                <div class="panel-body">
                                    {!! $course->reviews !!}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <br/>
            </div>

        </div>
        <br/>

        @include('_mid-footer-content')
    </section>
@stop
@section('javascripts')
    @parent
    <script type="text/javascript">

        var Comm100API=Comm100API||{};(function(t){function e(e){var a=document.createElement("script"),c=document.getElementsByTagName("script")[0];a.type="text/javascript",a.async=!0,a.src=e+t.site_id,c.parentNode.insertBefore(a,c)}t.chat_buttons=t.chat_buttons||[],t.chat_buttons.push({code_plan:208,div_id:"live-chat"}),t.site_id=46669,t.main_code_plan=208,e("https://chatserver.comm100.com/livechat.ashx?siteId="),setTimeout(function(){t.loaded||e("https://hostedmax.comm100.com/chatserver/livechat.ashx?siteId=")},5e3)})(Comm100API||{})

    </script>
    <script>
        (function($) {
            $('.more-info-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    text: function(api){
                        return $(this).next('.more-info-box').clone();
                    }
                },
                show: {
                    event: 'click'
                },
                hide: {
                    event: 'click mouseleave'
                }
            });

            $('.more-info-btn').on('click', function(e) {
                var $that = $(this);
                e.preventDefault();
                    if($that.find('.flag').text() == 'More')
                        $that.find('.flag').text('Less')
                    else
                        $that.find('.flag').text('More');
            });


            $('.table-scroll').DataTable( {
                scrollY:        '260px',
                scrollCollapse: true,
                paging:         false,
                info:       false,
                searching:     false,
                "ordering": false,
                columns: [
                    { "width": "150px" },
                    null,
                    { "width": "50px" },
                ]
            } );


            $('.news').slick({
                dots: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    }]
            });
        })(jQuery);
    </script>
@endsection