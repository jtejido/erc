@extends('main.main_layout')

@section('title')
    State Handouts | Environmental Resource Center
@endsection

@section('main_content')
    <section class="book-page">
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> State Handouts  </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="box-header">

            </div>

            <div class="box-body">
                @foreach($types as $type)
                <div class="row states">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h2>{{ $type['type'] }}</h2>
                        <hr/>
                        @foreach($handouts[$type['id']] as $handout)
                            <a href="{{ route('main.course.getmaterial', [$handout->files()->first()->filename]) }}" class="btn btn-default" title="Download"><i class="fa fa-download" aria-hidden="true"></i> {{ $handout->state }}</a>
                        @endforeach
                        <hr/>
                    </div>
                </div>

                    <br/>
                @endforeach

                <br/><br/>

            </div>

        </div>
    </section>
@stop

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

        })(jQuery);
    </script>
@endsection

@section('stylesheets')
    @parent
    <style>
        .states .btn {
            margin: 5px;
        }
    </style>
@endsection