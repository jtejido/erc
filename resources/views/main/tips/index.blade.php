@extends('main.main_layout')
@section('body-class', 'index')
@section('main_content')
<script type="text/javascript">
    {{--if (typeof data == 'undefined') {--}}
        {{--window.data = {};--}}
    {{--}--}}
    {{--var _tips = {!! $tips->toJson() !!};--}}
    {{--data.tips = _tips;--}}
</script>
    <section class="main">
        <div class="container-fluid">
            <div class="row">


                    <div class="container">
                        <div class="row">
                            <div class="page-title">
                                <div class="border"></div>
                                <div class="inner">
                                    <h1> Tips of the Week Archive </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="row">
                            <div class="box-header">
                                <span style="text-align: center;">
                                    <form action="" class="form-inline">
                                        <div class="form-group" >
                                            <input type="text" class="form-control" name="searchQ" placeholder="Search" value="{{ $searchQ }}">
                                            <button type="submit" title="Search" class="btn btn-sm btn-info"><i class="fa fa-icon fa-search"></i></button>
                                            <button type="button" title="Reset" id="resetFilter" class="btn btn-sm btn-default"><i class="fa fa-icon fa-refresh"></i></button>
                                        </div>
                                    </form>
                                </span>
                            </div>
                        </div>

                        <div class="row">

                            <div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if ($tips->count())
                                            @foreach ($tips as $tip)
                                                <tr>
                                                    <td>{{ $tip->published_date->format('F j, Y') }}</td>
                                                    <td>{{ $tip->title }}</td>
                                                    <td>{{ $tip->category[0]->name }}</td>
                                                    <td>
                                                        <a href="/tips/show/{{ $tip->slug }}" class="btn btn-xs btn-info go-to-article"> View </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="4">No data.</td>
                                            </tr>
                                        @endif
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <hr/>

                            <div class="pull-right">
                                {!! $tips->appends(['page' => $page, 'searchQ' => $searchQ])->render() !!}
                            </div>
                        </div>
                    </div>
            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </section><!-- .main -->
    
@stop

@section('javascripts')
    <script>
            $('#resetFilter').on('click', function() {
                var $form = $(this).closest('form');
                $form.find("input[name='searchQ']").val('');
                $form.submit();
            });

    </script>
@endsection