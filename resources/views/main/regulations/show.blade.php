@extends('main.main_layout')

@section('title')
    {{ $reg->title }} | Environmental Resource Center
@endsection

@section('meta')
    <meta property="og:url" content="{{ route('main.regulations.edit', $reg->id) }}">
    <meta property="og:type" content="article">
    <meta property="og:title" content="{{ $reg->title }}">
    <meta property="og:description" content="{{ str_limit(strip_tags($reg->content), 300, '...') }}">
    <?php $path = '/img/erc/homepage/article-1.jpg'; ?>

    @if ($reg->photo and $reg->photo_mime)
        <?php $path = route('reg.photo', $reg->id); ?>
        <meta property="og:image" content="{{ $path }}">
    @else
        <meta property="og:image" content="{{ route('home') }}{{ $path }}">
    @endif
@endsection

@section('body-class', 'show')
@section('stylesheets')
    @parent
    <style type="text/css">
        .regulations.show .inner h1 {

        }
    </style>
@endsection
@section('main_content')

    <section class="main">
        <div class="container-fluid">
            <div class="row">
            
                <div class="page-title">

                    <div class="border">

                        <!-- STATIC BUTTONS -->
                        <div class="col-md-10 col-md-offset-1">
                            <div class="inner">
                                <div class="main-class">
                                    <a href="{{ route('courses')  }}?type=3" type="button" target="_blank" class="cbt"></a>
                                    <label> Online Training </label>
                                </div>
                                <div class="main-class">
                                    <a href="{{ route('courses')  }}" type="button" target="_blank" class="training-register"></a>
                                    <label> Register for Training </label>
                                </div>
                                <div class="main-class">
                                    <a href="/docs/compliance_calendar.pdf" type="button" target="_blank" class="calendar"></a>
                                    <label> Compliance Calendar </label>
                                </div>
                                <div class="main-class">
                                    <a href="{{ route('resource.classcalendar') }}" type="button" target="_blank" class="class-sched"></a>
                                    <label> Schedule of Classes </label>
                                </div>                      
                            </div>
                        </div>
                        <!-- STATIC BUTTONS END -->                  
                    
                    </div>
                    <div class="inner">
                         <h1> {{ $reg->title }}  </h1>   
                    </div>
                </div>                          
                
            </div>
        </div>
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="inner">
                    <div class="static-wrap">
                        <div class="entry">
                            <div class="text-center lead">
                                <strong>{{ $reg->published_date->format('F d, Y') }}</strong>
                            </div>


                            {!! html_entity_decode($reg->content) !!}
                            <p>
                                Get the training you need to correctly ship batteries by air and ground by attending the 
                                <a href="{{ route('course.show', 53) }}">How to Ship Batteries by Ground and Air - Webcast</a>
                                or <a href="{{ route('course.show', 20) }}">How to Ship Batteries by Ground and Air - Online Training</a>.
                            </p>
                            <p>
                                Learn more about how to ship dangerous goods properly and meet your mandatory training requirements by attending Environmental Resource Center's 
                                <a href="{{ route('course.show', 10) }}">Transportation of Dangerous Goods: Compliance with IATA Regulations</a>
                                seminar, <a href="{{ route('course.show', 27) }}">IATA Regulations - Webcast</a>,
                                <a href="{{ route('course.show', 28) }}">IATA Dangerous Goods Update - Webcast</a>, or
                                <a href="{{ route('course.show', 52) }}">DOT/IATA How to Ship Hazardous Materials and Dangerous Goods by Ground and Air - Webcast</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </section>

    <!-- STATIC SIDEBAR -->
    {{--<section class="sidebar">--}}
        {{--<div class="container">--}}
            {{--<div class="col-md-10 col-md-offset-1">--}}
                {{--<div class="inner">--}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<h2> Reg of the Day</h2>--}}
                        {{--<div class="wrapper">--}}
                            {{--@if ($regulation)--}}
                                {{--<p class="title"> {{ $regulation->title }} </p>--}}
                                {{--<p class="description">--}}
                                    {{--{!! str_limit(strip_tags($regulation->content), 30, '...') !!}--}}
                                {{--</p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--@if ($regulation)--}}
                            {{--<a href="{{ route('main.regulations.index') }}" class="view"> View all </a>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<div id="live-chat" class="hidden"></div>--}}
                        {{--<img src="/img/erc/live-chat.png" class="banner live-chat-img" >--}}
                        {{--<div class="sign-up-wrapper">--}}
                            {{--<a href="http://care.ercweb.com/lists/?p=subscribe&id=2" target="_blank" class="btn btn-primary"> SIGN UP</a>--}}
                            {{--<p>For Reg of the Day, Tips of the Week, Discounts and Special Offers</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<h2> Tips of the Week </h2>--}}
                        {{--<div class="wrapper">--}}
                            {{--<p class="title"> Environmental </p>--}}
                            {{--@if ($tipsEnv)--}}
                                {{--<p class="description"> {{ $tipsEnv->title }} </p>--}}
                            {{--@endif--}}
                            {{--<p class="title"> Safety </p>--}}
                            {{--@if ($tipsSafe)--}}
                                {{--<p class="description"> {{ $tipsSafe->title }} </p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--@if ($tipsEnv or $tipsSafe)--}}
                            {{--<a href="{{ route('main.tips.index') }}" class="view"> View all </a>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    <!-- STATIC SIDEBAR END -->

    @include('_mid-footer-content')

@stop

@section('javascripts')

    <script>
        (function($) {

            $('.news').slick({
                dots: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    }]
            });
        })(jQuery)
    </script>
@endsection
