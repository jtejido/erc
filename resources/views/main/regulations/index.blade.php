@extends('main.main_layout')

@section('meta')
    <meta name="title" content="Environmental Resource Center">
    <meta name="description" content="We manage our system based on the latest regulation and trends. We keep in mind our EHS policy and implementation. Check our Plans and Procedures to know more.">
@endsection

@section('body-class', 'index')
@section('main_content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _regs = {!! $regulations->toJson() !!};
    data.regs = _regs;
</script>

    <section class="main" ng-controller="MainRegulations">
        <div class="container-fluid">
            <div class="row">

                    <div class="container">
                        <div class="row">
                            <div class="page-title">
                                <div class="border"></div>
                                <div class="inner">
                                    <h1> Reg of the Day Archive </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1" ng-cloak>
                        <div class="row">
                        <div class="col-md-6 col-md-offset-3 select-filter"> 
                            <div class="form-group col-xs-4">
                                <label for="month_view">Month</label>
                                <select name="month_view" class="form-control"
                                    ng-model="data.current_view.month"
                                    ng-change="refresh_regs()"
                                    ng-options="month for month in months">
                                </select>
                            </div>
                            <div class="form-group col-xs-4">
                                <label for="year_view">Year</label>
                                <select name="year_view" class="form-control"
                                    ng-model="data.current_view.year"
                                    ng-change="refresh_regs()"
                                    ng-options="year for year in years">
                                </select>
                            </div>
                            <div class="form-group col-xs-4">
                                <label for="text_view">Search</label>
                                <input type="text" class="form-control" name="text_view" placeholder="Search"
                                    ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }"
                                    ng-change="refresh_regs()"
                                    ng-model="data.current_view.text">
                            </div>
                        </div>
                        </div>



                        <div class="row">

                            <div ng-cloak>
                                <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="reg in active_regs track by $index" ng-if="reg">
                                        <td>@{{ reg.published_date.date | moment:'MMMM D, YYYY' }}</td>
                                        <td>@{{ reg.title }}</td>
                                        <td>@{{ reg.category[0].name }}</td>
                                        <td>
                                            <a href="/regulations/show/@{{ reg.slug }}" class="btn btn-xs btn-info go-to-article"> View </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                </div>
                            </div>
                            <hr/>

                        </div>

                    </div>

            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </section><!-- .main -->
    
@stop

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

        })(jQuery);
    </script>
@endsection