@if ($user->isMember())
   <!-- Extract data for frontend view -->

    <div class="item panel">
            <div class="panel-inner panel-heading">
                <h2>
                    <a data-registrable="1" data-agent="7" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item pull-right" href="javascript:void(0)">
                        <i class="fa fa-close pull-right"></i>
                    </a>
                    {{ $title }}</h2>

            </div>
            <div class="panel-inner panel-body">

                <div class="attendees">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="p-label">
                                <p>Subscribers:</p>
                            </div>
                            <div class="">
                            </div>
                        </div>

                        <div class="col-sm-8">
                            @foreach ($attendees as $attendee)
                                <div>
                                    <div class="column col-xs-6">
                                    <span class="name">
                                        {{ ucfirst($attendee->agent->contact->first_name) }}
                                        {{ ucfirst($attendee->agent->contact->last_name) }}
                                    </span>
                                    </div>

                                    <div class="column col-xs-6">
                                        <span class="price">${{ number_format($attendee->temporary_price, 2) }}</span>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="subtotal">
                    <div class="row">
                        <div class="column col-xs-7">
                            <p>Total Subscriptions Cost : </p>
                        </div>
                        <div class="column col-xs-5">
                            <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                        </div>
                    </div>
                </div>
            </div>

    </div>
@else
    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <a href="javascript:void(0)" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item"><i class="fa fa-remove"></i></a>
                </div>
            </div>
            <div class="box-body">

                <div class="attendees">
                    <h5>Subscribers</h5>
                    <a href="{{ route('admin.yearly_subscription', ['user_id' => $client->id]) }}"
                       class="btn btn-info btn-flat btn-xs pull-right">Add/Edit Subscribers</a>

                    <ul class="list">
                        @foreach ($attendees as $attendee)
                            <li>
                                {{ ucfirst($attendee->agent->contact->first_name) }} {{ ucfirst($attendee->agent->contact->last_name) }}
                                <span class="price pull-right">
                                    $<input type="text" class="price" value="{{ $attendee->temporary_price }}" data-original-price="{{ $attendee->paid_price }}" />
                                    {{-- */ $class = $item->status == App\Utilities\Constant::PENDING ? 'change-price' : 'order-modification-change-price'; /* --}}
                                    <button class="btn btn-flat btn-xs btn-success {{ $class }}"
                                            data-orderable-id="{{ $attendee->id }}"
                                            data-orderable-type="{{ \App\Utilities\Constant::YEARLY_SUBSCRIPTION }}"
                                            data-adjustable-id="{{ $attendee->id }}"
                                            data-adjustable-type="{{ \App\Utilities\Constant::YEARLY_SUBSCRIPTION }}"
                                            >
                                        Change
                                    </button>
                                    <input type="hidden" id="order-item-id" value="{{ $item->id }}">
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="total">
                    <b>Total Cost : </b>
                    <span class="total-cost pull-right">${{ number_format($total, 2) }}</span>
                </div>
            </div>
        </div>
    </div>
@endif
