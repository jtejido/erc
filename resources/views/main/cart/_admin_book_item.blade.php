<div class="col-md-12 box-container">
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $book->name }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                @if ($noRemove)
                    <a href="javascript:void(0)" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item"><i class="fa fa-remove"></i></a>
                @endif
            </div>
        </div>
        <div class="box-body">
            <input type="hidden" id="order-item-id" value="{{ $item->id }}">
            <div class="row">
                <div class="col-md-5">
                    <div class="thumbnail btn-flat">
                        @if($book->thumbnail)
                            <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
                        @else
                            <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                        @endif

                    </div>
                </div>
                <div class="col-md-7">

                    {{-- */ $class = $status == App\Utilities\Constant::PENDING ? 'change-price' : 'order-modification-change-price'; /* --}}

                    $ <input type="text" class="price"
                             style="display:inline;width:85px"
                             data-original-price="{{ $productOrder->price }}"
                             value="{{ $productOrder->price }}">
                    <button class="btn btn-flat btn-xs btn-success {{ $class }}"
                            data-orderable-id="{{ $productOrder->id }}"
                            style="margin-left: 20px; margin-bottom: 5px; width: 90px;"
                            data-orderable-type="{{ \App\Utilities\Constant::PRODUCT_ORDERS }}"
                            data-adjustable-id="{{ $productOrder->id }}"
                            data-adjustable-type="{{ \App\Utilities\Constant::PRODUCT_ORDERS }}"
                    >Change
                    </button>


                    <br/>

                    <strong>SKU:</strong>&nbsp;{!! $book->getLinkToBook($book->code, true) !!}&nbsp;&nbsp;&nbsp;
                    <strong>Weight:</strong>&nbsp;{{ $book->getWeight() }}

                    <hr/>
                    {!! Form::open(['method' => 'POST', 'url' => route('products.cart.update'), 'class' => 'form-inline']) !!}
                    {!! Form::hidden('book_id', $book->id) !!}
                    {!! Form::hidden('client_id', $client->id) !!}
                    {!! Form::hidden('status', $status) !!}
                    {!! Form::hidden('weight', $book->getWeight()) !!}
                    <div class="form-inline {{ ($errors->get('quantity')) ? 'has-error' : '' }}">
                        {!! Form::label('quantity', 'Qty') !!}
                        {!! Form::number('quantity', $productOrder->quantity,
                            $attributes = [
                            'min'   => '1',
                            'style' => 'display:inline;width:55px;height:30px',
                            'class' => 'form-control qty', 'required' => 'required'])  !!}
                        <button type="submit" class="btn btn-sm btn-default btn-flat"><i class="fa fa-edit"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="total">
                <b>Total Cost : </b>
                <span class="total-cost pull-right">${{ number_format($total, 2) }}</span>
            </div>
        </div>
    </div>
</div>