@if ($user->isMember())
@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
        }
        .qty {
            width: 55px !important;
            display: inline;
            margin-bottom: 0px !important;
        }
    </style>
@endsection
<div class="item book panel">
        <div class="panel-inner panel-heading">
            <h2>{{ $book->name }}</h2>
        </div>

        <div class="panel-inner panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="thumbnail btn-flat">
                        @if($book->thumbnail)
                            <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
                        @else
                            <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                        @endif

                    </div>
                </div>
                <div class="col-md-7">
                    <span class="price">
                        @if($discounted)
                            <em><del>{{ $book->getPrice() }}</del></em>&nbsp;{{ $book->getDiscountedPrice() }}
                        @else
                            {{ $book->getPrice() }}
                        @endif
                    </span>
                    <strong>SKU:</strong>&nbsp;<code>{{ $book->code }}</code>&nbsp;&nbsp;&nbsp;
                    <strong>Weight:</strong>&nbsp;<code>{{ $book->getWeight() }}</code>
                    <hr/>
                    {!! Form::open(['method' => 'POST', 'url' => route('products.cart.add'), 'class' => 'form-inline']) !!}
                    {!! Form::hidden('book_id', $book->id) !!}
                    {!! Form::hidden('quantity', 1) !!}
                    {!! Form::hidden('discounted', $discounted) !!}
                    <div class="form-group">
                        <button  class="btn btn-sm btn-warning btn-flat"><i class="fa fa-cart-plus"></i>&nbsp;Add</button>&nbsp;
                    </div>
                    {!! Form::close() !!}

                    <br/>
                </div>
            </div>
        </div>
</div>
@endif