@if ($user->isMember())
    <!-- Extract data for frontend view -->

    <div class="item panel">
            <div class="panel-inner panel-heading">
                <h2>
                    <a data-registrable="1" data-agent="7" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item pull-right" href="javascript:void(0)">
                        <i class="fa fa-close pull-right"></i>
                    </a>
                    {{ $title }}</h2>

            </div>
            <div class="panel-inner panel-body">

                <div class="attendees">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="p-label">
                                <p>Number of Seats:</p>
                            </div>
                            <div class="">
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div>
                                <div class="column col-xs-6">
                                    <span class="name">
                                        {{ $corpSeat->credits }} Seats
                                    </span>
                                </div>

                                <div class="column col-xs-6">
                                    <span class="price">${{ number_format($total, 2) }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="subtotal">
                    <div class="row">
                        <div class="column col-xs-6">
                            <p>Total Cost : </p>
                        </div>
                        <div class="column col-xs-6">
                            <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                        </div>
                    </div>
                </div>
            </div>
    </div>

@else
    
    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <a href="javascript:void(0)" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item"><i class="fa fa-remove"></i></a>
                </div>
            </div>
            <div class="box-body">

                <div class="attendees">
                    <h5>Corporate Seat</h5>
                    <a href="{{ route('admin.corporate_seat', ['user_id' => $client->id]) }}"
                       class="btn btn-info btn-flat btn-xs pull-right">Edit Corporate Seat Credit</a>
                    <ul class="list">
                        <li>
                            {{ $corpSeat->credits }} Seats
                            <span class="pull-right">
                                $<input type="text" class="price" value="{{ $total }}" data-original-price="{{ $item->price_charged }}" />
                                {{-- */ $class = $item->status == App\Utilities\Constant::PENDING ? 'change-price' : 'order-modification-change-price'; /* --}}
                                <button class="btn btn-flat btn-xs btn-success {{ $class }}"
                                        data-orderable-id="{{ $corpSeat->id }}"
                                        data-orderable-type="{{ \App\Utilities\Constant::CORPORATE_SEAT }}"
                                        data-adjustable-id="{{ $corpSeat->id }}"
                                        data-adjustable-type="{{ \App\Utilities\Constant::CORPORATE_SEAT }}"
                                        >
                                    Change
                                </button>
                                <input type="hidden" id="order-item-id" value="{{ $item->id }}">
                            </span>
                        </li>
                    </ul>
                </div>

                <div class="total">
                    <b>Total Cost : </b>
                    <span class="total-cost pull-right">${{ number_format($total, 2) }}</span>
                </div>
            </div>
        </div>
    </div>
@endif