@if (Auth::user() && Auth::user()->isMember())

    <div class="item panel">
            <div class="panel-inner panel-heading">
                <h2>{{ $agentName }} Corporate Seat Credit</h2>
            </div>

            <div class="panel-inner panel-body">

                <div class="attendees">
                    {{-- */ $text = $credits > 1 ? 'credits' : 'credit'; /* --}}
                    <strong class="select-credit-label">Select which class registration will the credits apply
                        <span style="display: inline" class="label label-warning">({{ $credits }} {{ $text }})</span></strong>
                    <hr/>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::open(['method' => 'POST',
                                    'url' => route('credit.give'),
                                    'id' => 'class-credit-discount-form']) !!}

                            {!! Form::input('hidden', 'user_id', $agentId) !!}
                            {!! Form::input('hidden', 'order_id', $orderId) !!}

                            <ul class="list">
                            @foreach ($attendees as $classId => $attendee)
                                <li>
                                    {!! Form::checkbox('class_ids[]', $classId, '', [
                                            'class' => 'checkbox class-credit'
                                        ]) !!}
                                    {{ $attendee }}
                                </li>
                            @endforeach
                            </ul>
                            <hr/>
                            <div class="text-center">
                                <button type="submit" id="btn-give-credit" class="btn btn-info btn-lg" disabled>Use Credit</button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>

@else

    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">
                    {{ $agentName }} Corporate Seat Credit
                    <span class="credits pull-right" data-count-credits="{{ $credits }}">{{ $credits }} credits</span>
                </h3>
            </div>
            <div class="box-body">

                <div class="attendees">
                    <h5><b>Select Which Class Registration Will The Credits Apply</b></h5>

                    {!! Form::open(['method' => 'POST',
                                    'url' => route('credit.give'),
                                    'id' => 'class-credit-discount-form']) !!}

                    {!! Form::input('hidden', 'user_id', $agentId) !!}
                    {!! Form::input('hidden', 'order_id', $orderId) !!}

                    <ul class="list">
                        @foreach ($attendees as $classId => $attendee)
                            <li>
                                <p>
                                    {!! Form::checkbox('class_ids[]', $classId, '', [
                                        'class' => 'checkbox class-credit'
                                    ]) !!}
                                    <span class="price" data-class-id="{{ $classId }}">{{ $attendee }}</span>
                                </p>
                            </li>
                        @endforeach

                        <button type="submit" id="btn-give-credit" class="btn btn-flat btn-success" disabled>Use Credit</button>
                    </ul>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endif