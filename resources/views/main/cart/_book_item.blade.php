@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
        }
        .qty {
            width: 55px !important;
            display: inline;
            margin-bottom: 0px !important;
        }
    </style>
@endsection
<div class="item book panel">
        <div class="panel-inner panel-heading">
            <h2>
                @if ($noRemove)
                    <a data-registrable="1" data-agent="7" data-href="{{ route('cart.remove_item') }}"
                       data-id="{{ $item->id }}" class="btn-remove remove-item pull-right" href="javascript:void(0)">
                        <i class="fa fa-close pull-right"></i>
                    </a>
                @endif
                {{ $book->name }}
            </h2>
        </div>

        <div class="panel-inner panel-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="thumbnail btn-flat">
                        @if($book->thumbnail)
                            <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
                        @else
                            <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                        @endif

                    </div>
                </div>
                <div class="col-md-7">
                    <span class="price">{{ $productOrder->product_price }}</span>
                    <strong>SKU:</strong>&nbsp;{!! $book->getLinkToBook() !!}&nbsp;&nbsp;&nbsp;
                    <strong>Weight:</strong>&nbsp;{{ $book->getWeight() }}
                    <hr/>
                    {!! Form::open(['method' => 'POST', 'url' => route('products.cart.update'), 'class' => 'form-inline']) !!}
                        {!! Form::hidden('book_id', $book->id) !!}
                        {!! Form::hidden('status', $status) !!}
                    <div class="form-inline {{ ($errors->get('quantity')) ? 'has-error' : '' }}">

                        {{-- */ $readOnly = ($status == App\Utilities\Constant::COMPLETE || $status == App\Utilities\Constant::INVOICED)
                                            ? true
                                            : false;  /* --}}
                        {!! Form::label('quantity', 'Qty: ', ['style'=>'width:auto']) !!}

                        @if ($readOnly)
                            <strong class="qty" style="display:inline; width:55px; height: 30px">{{ $productOrder->quantity }}</strong>
                        @else
                            {!! Form::number('quantity', $productOrder->quantity,
                                $attributes = ['class' => 'form-control qty',
                                                'min'       => '1',
                                                'style' => 'display:inline; width:55px; height: 30px',
                                                'required' => 'required'])  !!}
                            <button type="submit" class="btn btn-sm btn-default btn-flat"><i class="fa fa-edit"></i></button>
                        @endif
                    </div>
                    {!! Form::close() !!}
                    <br/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">

                </div>
                <div class="col-sm-3">
                    <div class="subtotal-container">
                        <small>Total Cost: </small>
                        <div>
                            <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>