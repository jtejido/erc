@if ($user->isMember())
    <!-- Extract data for frontend view -->

    <div class="item panel">

            <div class="panel-inner panel-heading">
                <h2>
                    @if ($canRemove)
                        <a data-registrable="1" data-agent="7" data-href="{{ route('cart.remove_item') }}"
                           data-id="{{ $item->id }}" class="btn-remove remove-item pull-right" href="javascript:void(0)">
                            <i class="fa fa-close pull-right"></i>
                        </a>
                    @endif
                    {{ $title }}
                </h2>

            </div>
            <div class="panel-inner panel-body">

                <div class="row">
                    <div class="col-sm-5">
                        @if (strlen($schedule) || strlen($venue))
                            <div class="description">

                                <div class="schedule">{{ $schedule }}</div>
                                <small>
                                    @if (isset($venue))
                                        {!! $venue !!}
                                    @endif
                                </small>
                            </div>
                        @endif
                        <h4 class="attendee-label">Attendees:</h4>
                        <div class="attendees-modification">
                            <a href="{{ $attendeesRoute }}"
                               class="btn btn-info"><i class="fa fa-plus"></i>  {{ $attendeeText }}</a>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="list">
                        @foreach ($classRegistrations as $classRegistration)
                            <div class="list-row">
                                <div class="column col-xs-7">
                                    {{ ucfirst($classRegistration->contact->first_name) }} {{ ucfirst($classRegistration->contact->last_name) }}
                                </div>
                                <div class="column col-xs-5">
                                @if ($classRegistration->corporate_seat_credit_applied)
                                        <span class="price">
                                            ${{ $classRegistration->item_charge }}
                                            <button class="btn btn-flat btn-xs btn-danger btn-remove-credit"
                                                    data-id="{{ $classRegistration->id }}"
                                                    data-url="{{ route('credit.remove') }}">
                                                Remove Credit
                                            </button>
                                        </span>
                                @else

                                        <span class="price">${{ $classRegistration->item_charge }}</span>

                                @endif
                                </div>

                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-9">

                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Total Attendees Cost:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

    </div>


@else

    <!-- Extract data for admin/csr view -->

    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $title }}</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    @if ($canRemove)
                        <a href="javascript:void(0)" data-href="{{ route('cart.remove_item') }}"
                           data-id="{{ $item->id }}" class="btn-remove remove-item"><i class="fa fa-remove"></i></a>
                    @endif
                </div>
            </div>
            <div class="box-body">

                @if (strlen($schedule) || strlen($venue))
                    <div class="details">
                        <p class="schedule"><i>{{ $schedule }}</i> <div class="venue pull-right">{!! $venue !!}</div></p>
                    </div>
                @endif

                <div class="attendees">
                    <h5>Attendees</h5>
                    <a href="{{ $attendeesRoute }}"
                       class="btn btn-info btn-flat btn-xs pull-right">{{ $attendeeText }}</a>

                    <ul class="list">
                        @foreach ($classRegistrations as $classRegistration)
                            <li>
                                {{ ucfirst($classRegistration->contact->first_name) }} {{ ucfirst($classRegistration->contact->last_name) }}
                                <span class="pull-right item-price-container">
                                    @if ($classRegistration->subscriptionDiscount)
                                        $<input type="text" class="price no-border" value="{{ $classRegistration->item_charge }}" disabled>
                                    @elseif ($classRegistration->corporate_seat_credit_applied)
                                        $<input type="text" class="price no-border" value="{{ $classRegistration->item_charge }}" disabled>
                                        <button class="btn btn-flat btn-xs btn-danger btn-remove-credit"
                                                data-id="{{ $classRegistration->id }}"
                                                data-url="{{ route('credit.remove') }}">
                                            Remove Credit
                                        </button>
                                    @else
                                        @if ($canRemove)
                                            $<input type="text" class="price"
                                                    data-original-price="{{ $classRegistration->item_charge }}"
                                                    value="{{ $classRegistration->item_charge }}">
                                            <button class="btn btn-flat btn-xs btn-success change-price"
                                                    data-orderable-id="{{ $classRegistration->id }}"
                                                    data-orderable-type="{{ \App\Utilities\Constant::REGISTRATION_ORDER }}"
                                                    >
                                                Change
                                            </button>
                                        @else
                                            {{ $classRegistration->item_charge }}
                                        @endif
                                    @endif
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <hr>

                <div class="total">
                    <b>Total Cost : </b>
                    <span class="total-cost pull-right">${{ number_format($total, 2) }}</span>
                </div>
            </div>
        </div>
    </div>
@endif

