@extends('layout')

@section('stylesheets')
    @parent

    {!! HTML::style(elixir('css/app.css')) !!}
    {!! HTML::style(elixir('css/admin-lte.css')) !!}
    {!! HTML::style(elixir('css/admin-lte-2.css')) !!}
    {!! HTML::script(elixir('js/admin/angular.js')) !!}
    {!! HTML::script(elixir('js/admin/angular/app.js')) !!}

    @include('main.includes.google_analytics')

@endsection

@section('body')

    @section('header')
        {{-- Header Section --}}
    @show

    <div class="container main-wrapper">
        @yield('main_content')
    </div>

    <footer>
        <div id="footer">
            <div class="row">
                <div class="col-sm-4">
<pre>
Environmental Resource Center
101 Center Pointe Drive
Cary, North Carolina 27513-5706</pre>
                </div>
                <div class="col-sm-4">
                    <ul>
                        <li><a href="/contact-us" >Contact Us</a></li>
                        <li><a href="" data-toggle="modal" class="ppButtonModal">Privacy Policy</a></li>
                        <li><a href="" data-toggle="modal" class="siteButtonModal">Sitemap</a></li>
                    </ul>
                    <p>
                        © {{ date('Y') }} Environmental Resource Center. All rights reserved.
                    </p>
                </div>
                <div class="col-sm-4">
                    <h3 class="phone"><a href="tel:+8005372372"><i class="fa fa-phone-square" aria-hidden="true"></i> 800-537-2372</a></h3>
                </div>
            </div>
        </div>
    </footer>
@stop

@section('javascripts')
    @parent

    {!! HTML::script(elixir('js/admin/admin-lte.js')) !!}
    {!! HTML::script(elixir('js/frontend/frontend.js')) !!}

    <script>
        (function($){
            $.get('/cp/session', function(data) {
                var msg = 'The coupon <strong>'+
                        data.coupon_code +
                        '</strong> will be applied on your next transaction.';
                if (data.has_coupon) {
                    $.notify({
                        icon: 'fa fa-info-circle',
                        message: msg}, {
                        placement: {
                            from: "bottom",
                            align: "center"
                        },
                        delay: 0 });
                }
            });

            $('#ppButtonModal').on('click', function(e) {
                e.preventDefault();

                $('#ppModal').modal('show');
            });

            $('#siteButtonModal').on('click', function(e) {
                e.preventDefault();

                $('#siteModal').modal('show');
            });
        })(jQuery);

    </script>

@endsection