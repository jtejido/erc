@extends('main.main_layout')

@section('stylesheets')
    @parent
    <style media="screen">
        ul.student-list {
            margin: 0 30px;
        }
        ul.student-list li {

        }
        ul.student-list li label {
            font-size: 1.2em;
        }
        ul.student-list li input[type=checkbox] {
            margin-left: -25px;
        }
        strong { font-weight: bold; }
        small { font-size: 0.7em; }
    </style>

@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> {{ $landing_page->title }}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container coupon-landing">
            @if($preview)
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
                    <div class="alert alert-warning text-center">
                        THIS IS A PREVIEW ONLY
                    </div>
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
                    <div class="box">
                        <div class="box-header">

                        </div>

                        <div class="box-body">


                            <p class="lead text-center">
                            <label>Coupon Code:</label>
                            <strong>{{ $code }}</strong>
                            </p>

                            <hr/>


                            <div class="row">
                                <div class="col-xs-12">
                                    {!! $landing_page->content !!}
                                </div>
                            </div>

                            <hr/>
                            @if($seminars_count)
                            <h3 class="course-type seminar">Seminars</h3>
                            @foreach($seminars->groupBy('course_id') as $key => $seminar_group)
                                @if($coupon_course_type == 'Course' &&
                                    $discountableIds->contains(function ($v, $k) use ($key) {
                                        return $k == $key;
                                    }) || $coupon_course_type == 'CourseClass')
                                <div class="course-group">
                                    <strong class="course-title">{{ $seminar_group->first()->title }}</strong>
                                    <ul>
                                        @foreach($seminar_group as $seminar)
                                        <li>
                                            <a target="_blank"
                                               href="{{ route('cp.redirect.class', [$uuid, 'classes.related', $seminar->id]) }}">
                                                {{ $seminar->titleLocation }}
                                            </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            @endforeach
                            @endif
                            <br/>
                            @if($webcasts_count)
                            <h3 class="course-type webcast">Webcasts</h3>
                            @foreach($webcasts->groupBy('course_id') as $key => $seminar_group)
                                @if($coupon_course_type == 'Course' &&
                                    $discountableIds->contains(function ($v, $k) use ($key) {
                                        return $k == $key;
                                    }) || $coupon_course_type == 'CourseClass')
                                <div class="course-group">
                                    <strong class="course-title">{{ $seminar_group->first()->title }}</strong>
                                    <ul>
                                        @foreach($seminar_group as $seminar)
                                            <li>

                                                <a target="_blank"
                                                   href="{{ route('cp.redirect.class', [$uuid, 'classes.related', $seminar->id]) }}">
                                                    {{ $seminar->titleLocation }}
                                                </a>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            @endforeach
                            <br/>
                            @endif

                            @if($cbts_count)
                                <h3 class="course-type seminar">Online Training</h3>
                                @foreach($cbts as $key => $cbt)
                                    @if($coupon_course_type == 'Course' &&
                                        $discountableIds->contains(function ($v, $k) use ($key, $cbt) {
                                            return $k == $cbt->id;
                                        }))
                                        <div class="course-group">
                                            <ul>
                                                <li>
                                                    <a target="_blank"
                                                       href="{{ route('cp.redirect.class', [$uuid, 'contacts.register', $cbt->id]) }}">
                                                        {{ $cbt->title }}
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                @endforeach
                            @endif



                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

        })(jQuery);
    </script>
@endsection
