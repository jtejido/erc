@extends('main.main_layout')

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Payment Details </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="payment-details cart">
                <input type="hidden"
                       name="validate_url"
                       value="{{ route('ajax.file.validate') }}"
                       id="path-to-validate-file">

                <input type="hidden" name="order_tax" value="{{$order->order_tax}}">
                <input type="hidden" name="shipping_fee" value="{{$order->shipping_rates}}">

                <div class="col-md-7">
                    @foreach ($invoices as $invoice)

                    {!! Form::open([
                            'route' => 'user.invoice_checkout',
                            'enctype' => 'multipart/form-data'
                        ])
                    !!}

                        <div class="inner checkout-wrapper">
                                <div class="pay-by-invoice panel">
                                    <div class="panel-inner panel-heading">
                                        <h2>Invoice Summary</h2>
                                    </div>

                                    <div class="panel-inner panel-body">

                                        @include('includes.success_message')

                                        <div class="error-container"></div>

                                        <div class="row">

                                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                                            <input type="hidden" name="invoice_payment" value="{{ $invoice->id }}">

                                            <div class="col-sm-6">
                                                <div class="form-group change-purchase-order">
                                                    <button class="btn btn-change btn-flat btn-change-invoice"
                                                            data-target="select-purchase-order"
                                                            data-hidden="change-purchase-order">
                                                        Upload Purchase Order
                                                    </button>
                                                </div>

                                                <div class="form-group select-purchase-order hidden">
                                                    <button class="btn btn-upload btn-flat">
                                                        Select Purchase Order
                                                    </button>

                                                    <button href="#" class="btn btn-danger btn-flat btn-cancel-change"
                                                       data-target="change-purchase-order"
                                                       data-hidden="select-purchase-order">Cancel</button>

                                                    {!! Form::file('po_file', [
                                                        'accept'    => 'application/pdf, image/*',
                                                        'id'        => 'po_file',
                                                        'class'     => 'upload-file hidden'])
                                                    !!}
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                {{-- */ $hiddenPoClass = ''; /* --}}

                                                @if ($invoice->poImage)
                                                    <div class="form-group change-purchase-order">
                                                        {!! HTML::image(route('invoice_image.preview',
                                                            ['type' => \App\Utilities\Constant::PO_IMAGE,
                                                            'order_id' => $order->id,
                                                            'invoice_id' => $invoice->id,
                                                            'file_name' => $invoice->poImage]),
                                                            'Payment Order Image', [
                                                            'class' => 'payment-image img-responsive center-block',
                                                        ]) !!}

                                                        <div class="download-payment">
                                                            @if (isset($invoice->poFile))
                                                                <a href="{{ route('invoice_image.preview',
                                                                    ['type' =>  \App\Utilities\Constant::PO_FILE,
                                                                     'order_id' => $order->id,
                                                                     'invoice_id' => $invoice->id,
                                                                     'file_name' => $invoice->poFile]) }}"
                                                                   class="text-center" download="{{ $invoice->poFile }}">
                                                                    Download Payment Order
                                                                </a>

                                                            @else
                                                                <a href="javascript:void(0)"
                                                                   data-order_id="{{ $order->id }}"
                                                                   data-type="{{ \App\Utilities\Constant::PO_IMAGE }}"
                                                                   data-file_name="{{ $invoice->poImage  }}"
                                                                   data-invoice_id="{{ $invoice->id }}"
                                                                   class="text-center download-invoice">
                                                                    Download Payment Order
                                                                </a>
                                                            @endif

                                                        </div>
                                                        {{-- */ $hiddenPoClass = 'hidden'; /* --}}
                                                    </div>
                                                @endif

                                                <div class="form-group select-purchase-order {{ $hiddenPoClass }}">
                                                    {!! HTML::image('img/placeholder-image.jpg', 'PO Image/File', [
                                                        'class' => 'po_file payment-image img-responsive center-block'
                                                    ]) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row invoice-details">
                                            <div class="col-sm-6 po">
                                                <label for="po_number">Purchase Order Number : </label>
                                            </div>

                                            <div class="col-sm-6">
                                                <input type="text" name="po_number" value="{{ $invoice->po_number }}">
                                            </div>
                                        </div>

                                        <div class="row hidden">
                                            <hr>
                                        </div>

                                        <div class="row hidden">

                                            <div class="col-sm-6">

                                                <div class="form-group change-check">
                                                    <button class="btn btn-flat btn-change btn-change-invoice"
                                                            data-target="select-check"
                                                            data-hidden="change-check">
                                                        Change Check
                                                    </button>
                                                </div>

                                                <div class="form-group select-check hidden">
                                                    <button class="btn btn-upload btn-flat">
                                                        Select Check
                                                    </button>
                                                    <button href="#" class="btn btn-danger btn-flat btn-cancel-change"
                                                       data-target="change-check"
                                                       data-hidden="select-check">Cancel</button>

                                                    {!! Form::file('check_file', [
                                                        'accept'   => 'application/pdf, image/*',
                                                        'id'       => 'check_file',
                                                        'class'    => 'upload-file hidden'])
                                                    !!}
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                {{-- */ $hiddenCheckClass = ''; /* --}}

                                                @if ($invoice->checkImage)
                                                    <div class="form-group change-check">
                                                        {!! HTML::image(route('invoice_image.preview',
                                                        ['type' => \App\Utilities\Constant::CHECK_IMAGE,
                                                        'order_id' => $order->id,
                                                        'invoice_id' => $invoice->id,
                                                        'file_name' => $invoice->checkImage]), 'Check Image', [
                                                            'class' => 'payment-image img-responsive center-block',
                                                        ]) !!}


                                                        <div class="download-payment">

                                                            @if (isset($invoice->checkFile))
                                                                <a href="{{ route('invoice_image.preview',
                                                                    ['type' =>  \App\Utilities\Constant::CHECK_FILE,
                                                                     'order_id' => $order->id,
                                                                     'invoice_id' => $invoice->id,
                                                                     'file_name' => $invoice->checkFile]) }}"
                                                                   class="text-center" download="{{ $invoice->checkFile }}">Download Check</a>

                                                            @else
                                                                <a href="javascript:void(0)"
                                                                   data-order_id="{{ $order->id }}"
                                                                   data-type="{{ \App\Utilities\Constant::CHECK_IMAGE }}"
                                                                   data-file_name="{{ $invoice->checkImage  }}"
                                                                   data-invoice_id="{{ $invoice->id }}"
                                                                   class="text-center download-invoice">
                                                                    Download Check
                                                                </a>
                                                            @endif

                                                        </div>

                                                        {{-- */ $hiddenCheckClass = 'hidden'; /* --}}
                                                    </div>
                                                @endif

                                                <div class="form-group select-check {{ $hiddenCheckClass }}">
                                                    {!! HTML::image('img/placeholder-image.jpg', 'Check Image/File', [
                                                        'class' => 'check_file payment-image img-responsive center-block'
                                                        ]) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row invoice-details hidden">
                                            <div class="col-sm-6 po">
                                                <label for="po_number">Check Number : </label>
                                            </div>

                                            <div class="col-sm-6">
                                                <input type="text" name="check_number" value="{{ $invoice->check_number }}">
                                            </div>
                                        </div>

                                        @if ($invoice->recipients->count())
                                            <div class="row invoice-details">
                                                <div class="col-sm-12">
                                                    <div class="attendees">
                                                        <h5>Other Recipients</h5>

                                                        <ul class="list">
                                                            @foreach ($invoice->recipients as $recipient)
                                                                <li>{{ $recipient->email }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>


                            <div class="cart-navigation row">
                                <div class="col-md-6 column">
                                    <a href="{{ route('user.home') }}" class="btn btn-primary btn-flat pull-left">
                                        Cancel
                                    </a>
                                </div>
                                <div class="col-md-6 column">
                                    <button type="submit"
                                            class="btn btn-warning btn-flat pull-right btn-pay"
                                            @if (!isset($order)) disabled @endif>
                                        Save Payment Details
                                    </button>
                                </div>
                            </div>
                        </div>

                    {!! Form::close() !!}

                    @endforeach
                </div>

                @include('main.checkout.payment_details')
            </div>
        </div>
    </section>

@endsection

@section('javascripts')
    @parent
    <script>
        $(function(){
            var amount = $('[name="shipping_fee"]').val();
            var order_tax = $('[name="order_tax"]').val();
            var fee_template = function(amount, title) {
                var ie_bff = ' <div class="row" style="margin-bottom: 5px">';
                ie_bff += '<div class="col-md-8">';
                ie_bff +=  '<span class="text-left">'+title+'</span></div>';
                ie_bff +=  '<div class="col-md-4 text-right">'+amount+'</div>';
                ie_bff += '</div>';
                return ie_bff;
            };

            if(amount != 0)
                $('.sub-total-box').last().parent().before($(fee_template(amount, 'Shipping Fee:')));
            if(order_tax != 0)
                $('.sub-total-box').last().parent().before($(fee_template(order_tax, 'Sales Tax:')));
        });
    </script>
@stop