@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Transaction Details </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="cart">
                <div class="inner col-md-8 col-md-offset-2">
                    @include('includes.success_message')

                    {!! OrderUtility::extractItems($item, $user) !!}

                    <div class="cart-navigation row">
                        <div class="col-md-4 col-md-offset-4 column">
                            <a href="{{ route('user.home') }}" class="btn blue-btn to-user-portal">
                                Return
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection