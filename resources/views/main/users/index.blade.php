@extends('main.main_layout')

@section('main_content')
    <section class="main-container user-detail">
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> {{ $userDetails->name }} </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-detail-wrap">
            <div class="row">
                <div class="col-xs-12">
                    <p class="text-center">{{ $userDetails->address }}</p>
                </div>
            </div>
            <br/>
            <div class="row details">
                <div class="col-sm-3 col-sm-offset-3 col-xs-offset-2">
                    <strong>Billing:</strong>
                    <div class="checkbox form-group">
                        <label for="same_as_billing">
                            <input type="checkbox" class="user-options" name="address_same_as_billing"
                                   data-url="{{ route('attribute.edit', ['contact_id' => $userDetails->id]) }}"
                                   @if ($userDetails->address_same_as_billing) checked @endif>


                        </label>
                        Same as Address
                    </div>
                    <strong>Shipping:</strong>
                    <div class="checkbox form-group">
                        <label for="same_as_shipping">
                            <input type="checkbox" class="user-options" name="address_same_as_shipping"
                                   data-url="{{ route('attribute.edit', ['contact_id' => $userDetails->id]) }}"
                                   @if ($userDetails->address_same_as_shipping) checked @endif>

                        </label>
                        Same as Address
                    </div>
                </div>
                <div class="col-sm-4 col-xs-offset-2 col-sm-offset-0">
                <strong>Credits:</strong>
                    @if($user &&
                        !$user->has_military_discount &&
                        !$user->getCorporateSeatsCredit() &&
                        !isset($hasSubscription)
                    )
                        <div class="text-center"><em>No Credits Currently</em></div>
                    @endif
                @if ($user && $user->has_military_discount)
                        <div class="checkbox form-group pull-right-sm">
                            <label for="military">
                                <input type="checkbox" class="user-options" name="has_military_discount"
                                       checked readonly disabled>
                                GSA Customer
                            </label>
                        </div>
                @endif
                @if ($user && $user->getCorporateSeatsCredit())
                        <div class="checkbox form-group">
                            <label for="military">
                                <input type="checkbox" class="subscription-options" name="has_subscription" checked readonly disabled>
                                {{ $user->getCorporateSeatsCredit() }} Corporate Seats
                            </label>
                        </div>
                @endif
                @if (isset($hasSubscription))
                    <div class="checkbox form-group">
                        <label for="military">
                            <input type="checkbox" class="subscription-options" name="has_subscription"
                                   checked readonly disabled>
                            Yearly Subscription
                        </label>
                        <br/>
                        <small style="display: block; padding-left: 45px;">
                         ( {{ $subscriptionType }}

                            @if (isset($expDate))
                                - Expires on {{ $expDate }})
                            @else
                                )
                            @endif

                            @if (!$subscription->activated)
                                <a href="javascript:void(0)" class="activate-subscription" data-url="{{ route('activation.get') }}"
                                   data-id="{{ $subscription->id }}"><span class="label label-danger">Activate</span></a>
                            @endif
                        </small>
                    </div>
                @endif

            </div>
            </div>
            <br/><br/>

            <div class="clearfix">

                @include('includes.success_message')
                @include('includes.error_message')

                <div class="training-order-title">
                    @if (!isset($hasSubscription))
                        <a href="{{ route('yearly_subscription') }}" class="btn-primary address-book btn">Yearly Subscription</a>
                    @endif

                    @if (!$user->corporate_seat_credits)
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#csModal" class="btn-primary address-book btn-corporate-seat btn">Corporate Seat Subscription</a>
                    @endif

                    <a href="{{ route('address_book.edit') }}" class="btn-primary address-book btn">Address Book</a>
                    <h3>Training and Order History</h3>
                </div>

                <div class="table-wrap">
                <table id="table-clients" class="table">
                    <thead>
                    <tr>
                        <th>Transaction Date</th>
                        <th>Title</th>
                        <th>Agent</th>
                        <th>Status</th>
                        <th width="120">Notes</th>
                        <th width="135px">Details</th>
                    </tr>
                    </thead>

                </table>
                </div> <!-- table-wrap -->
            </div>

            @include('includes.corporate_seat_notification')
        </div> <!-- .user-detail-wrap -->
    </section>
@endsection
@section('javascripts')
    @parent

    <script>
        (function($){
            $(document).ready(function(){

            $('#table-clients').dataTable({
                "order": [[ 0, "desc" ]],
                "stateSave": true,
                "pageLength": 10,
                "processing": true,
                "serverSide": false,
                "ajax": "/order_history",
                "columns": [
                    { "data": "trans_date" },
                    { "data": "title" },
                    { "data": "agent" },
                    { "data": "status" },
                    { "data": "notes" },
                    { "data": "actions" },
                ],
               "columnDefs": [
                   { "orderable": false, "targets": 5 }
               ]
           });


            });
        })(jQuery);
    </script>

@endsection
