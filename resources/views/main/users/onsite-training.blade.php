@extends('main.main_layout')

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Training Details </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="inner col-md-10 col-md-offset-1 user-training-details">
                <fieldset>
                    <div class="panel panel-primary btn-flat">
                        <div class="panel-inner panel-heading btn-flat">
                            <h2 class="text-center">On-site Training</h2>
                        </div>
                        <div class="panel-inner panel-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="panel panel-default btn-flat">
                                        <div class="panel-heading btn-flat">Course Materials</div>
                                        <div class="panel-body">
                                            <div class="control-group">
                                                <ul class="students list">
                                                    @foreach($course->materials() as $material)
                                                        <li><a href="{{ route('main.course.getmaterial', [$material->filename]) }}"
                                                               style="font-size: 14px !important;">{{ $material->title }}</a></li>
                                                    @endforeach
                                                    @if(!$course->materials()->count())
                                                        <div class="text-center">No materials available.</div>
                                                    @endif

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="panel panel-default btn-flat">
                                        <div class="panel-heading btn-flat">Uploaded Certificates</div>
                                        <div class="panel-body">
                                            <div class="control-group">
                                                <ul class="students list">
                                                    @foreach($course->certificates() as $material)
                                                        <li><a href="{{ route('main.course.getmaterial', [$material->filename]) }}"
                                                               style="font-size: 14px !important;">{{ $material->title }}</a></li>
                                                    @endforeach
                                                    @if(!$course->certificates()->count())
                                                        <div class="text-center">No certificates available.</div>
                                                    @endif

                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr/>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h5 class="title">On-site Training</h5>
                                    <dl class="dl-horizontal">
                                        <dt>Date</dt>
                                        <dd>{{ $course->training_date->toDateString() }}</dd>
                                        <dt>Address</dt>
                                        <dd>{{ $course->address }}</dd>
                                        <dt>Agent</dt>
                                        <dd>{{ $course->agent->contact->name }}</dd>
                                    </dl>
                                </div>
                            </div>



                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

        })(jQuery);
    </script>
@endsection
