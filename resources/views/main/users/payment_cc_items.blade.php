@if ($object == \App\Utilities\Constant::REGISTRATION_ORDER or
     $object == \App\Utilities\Constant::YEARLY_SUBSCRIPTION or
     $object == \App\Utilities\Constant::CORPORATE_SEAT or
     $object == \App\Utilities\Constant::PRODUCT_ORDERS)

    <h4 class="panel-title item-title">
        @if ($object == \App\Utilities\Constant::REGISTRATION_ORDER)
            {!! \App\Facades\OrderUtility::getPaymentClassTitle($item->id) !!}
        @elseif ($object == \App\Utilities\Constant::YEARLY_SUBSCRIPTION)
            Yearly Subscription
        @elseif ($object == \App\Utilities\Constant::CORPORATE_SEAT)
            Corporate Seat
        @elseif ($object == \App\Utilities\Constant::PRODUCT_ORDERS)
            Products
        @endif

    </h4>

    <div class="row summary-info">
        <div class="col-xs-12 attendees-info">
            <ul class="list">
                @foreach ($contents as $content)
                    <li>
                        @if ($object == \App\Utilities\Constant::CORPORATE_SEAT)
                            {{ $content->seats }}
                        @elseif ($object == \App\Utilities\Constant::PRODUCT_ORDERS)
                            {{ $content->title }} (x{{ $content->quantity }})
                        @else
                            {{ $content->name }}
                        @endif

                        <span class="price pull-right">${{ number_format($content->price_paid, 2) }}</span>
                    </li>
                @endforeach
            </ul>

        </div>
    </div>

@endif

@if ($object == \App\Utilities\Constant::ORDER_ADJUSTMENT_OBJECT)
    <h4 class="panel-title item-title">
        Order Modification
    </h4>

    <div class="row summary-info">
        <div class="col-xs-12 attendees-info">
            <ul class="list">
                @foreach ($contents as $content)
                    <li>
                        @if ($adjustmentObject == \App\Utilities\Constant::CLASS_REGISTRATION)
                            {{ $content->description }}
                        @elseif ($adjustmentObject == \App\Utilities\Constant::PRODUCT_ORDERS)
                            {{ $content->description }}
                        @else
                            {{ $content->description }}
                        @endif

                        <span class="price pull-right">${{ number_format($content->price_paid, 2) }}</span>
                    </li>
                @endforeach
            </ul>

        </div>
    </div>
@endif


