@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Yearly Subscription</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="main-container container-fluid course-attendees-container">

        <section class="yearly-subscription">
            <table class="table">
                <thead>
                <tr>
                    <th>Subscription</th>
                    <th>Price</th>
                    <th>Description</th>
                </tr>
                </thead>

                @foreach($types as $type)
                    <tr>
                        <td>
                            <input type="radio" class="subscription-option" name="subscription-type"
                                   value="{{ $type->id }}"
                                   id="sub-{{ $type->id }}"
                                   @if (isset($stype) && $stype == $type->id) checked @endif>
                            <strong>{{ $type->name }}</strong>
                        </td>

                        <td>
                            ${{ number_format($type->price, 2) }}
                        </td>
                        <td>
                            @if ($type->name == 'Webcast')
                                <p>
                                    You can attend all Environmental, Transportation, and Safety Training seminars and webcasts we offer for one full year. Subscription year begins with your first class.
                                </p>
                            @else
                                <p>
                                    You can attend all Environmental, Transportation, and Safety Training webcasts we offer for one full year. Subscription year begins with your first class.
                                </p>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </section>

        <section class="attendees-list">

            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @include('errors.form_errors')
            @include('includes.success_message')

            <div class="panel panel-primary">

                <div class="panel-heading">
                    <label>
                        <input type="checkbox" name="contact[]" value="{{ $user->contact->id }}"
                               class="contact">
                        {{ $user->contact->first_name }} {{ $user->contact->last_name }}
                    </label>
                </div>
                <div class="panel-body">
                    <div class="info">
                        <label for="email">Email: </label> <p>{{ $user->email }}</p>
                    </div>
                </div>
            </div>

            @if ($contacts->count() > 0)
                @foreach ($contacts as $contact)

                    @if ($contact->user)
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <label>
                                    <input type="checkbox" name="contact[]" value="{{ $contact->id }}"
                                           class="contact">
                                    {{ $contact->first_name }} {{ $contact->last_name }}
                                </label>

                                @if (!$contact->user and !$contact->note)

                                    <a href="javascript:void(0)" class="edit-contact pull-right"
                                       data-id="{{ $contact->id }}"
                                       data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                                    </a>

                                @endif
                            </div>
                            <div class="panel-body">
                                @if($contact->user)
                                    <div class="info">
                                        <label for="email">Email: </label>
                                        <p>{{ $contact->user ? $contact->user->email : 'No email' }}</p>
                                    </div>
                                @endif

                                @if (!$contact->user and !$contact->note)
                                    <div class="info">
                                        @if (strlen($contact->address1) &&
                                             strlen($contact->city) &&
                                             strlen($contact->state) &&
                                             strlen($contact->zip))
                                            <p class="contact-address">{{ $contact->address1 }} {{ $contact->address2 }}<br/>
                                                {{ $contact->city }}, {{ $contact->state }} {{ $contact->zip }}</p>
                                        @endif
                                    </div>
                                @endif

                                @if ($contact->note)
                                    <div class="info">
                                        <label for="note">Note: </label>
                                        <p>{{ $contact->note }}</p>
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
            <div class="text-center">
                <button class="btn btn-primary" id="add-contact"
                        data-user_id="{{ $user->id }}"
                        data-url="{{ route('contact.form.get') }}">
                    <i class="fa fa-user-plus"></i>&nbsp;New Contact
                </button>
            </div>

            <div class="transaction-buttons row text-center">

                <a href="{{ route('user.home') }}" class="btn btn-default btn-lg">
                    Go Back
                </a>

                <button class="btn btn-info btn-lg" id="add-contact-to-subscription"
                        data-agent="{{ $user->id }}">
                    Add To Subscription
                </button>
            </div>

            <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title" id="classModalLabel">Save Contact</h5>
                        </div>
                        <div class="modal-body">
                            <i>Loading...</i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        (function($) {

            $('#add-contact-to-subscription').on('click', function() {
                var $this = $(this),
                    contacts = [],
                    $checked = $('input[name="subscription-type"]:checked'),
                    id = $checked.val(),
                    userId = $this.data('agent');

                $('.contact').each(function() {
                    if ($(this).prop('checked')) {
                        contacts.push($(this).val());
                    }
                });

                if (typeof id != 'undefined' && contacts.length) {

                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('contact.add_to_subscription') }}",
                        data : {
                            user_id : userId,
                            contacts : contacts,
                            subscription_id : id
                        },
                        beforeSend : function (data) {
                            $this.attr('disabled', true);
                        },
                        success : function (response) {
                            if (response) {
                                window.location.href = response;
                            }
                        }
                    });
                }


            });
        })(jQuery);
    </script>
@endsection
