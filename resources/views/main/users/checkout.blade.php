@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Checkout </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="cart">
                <div class="inner col-md-12">
                    @include('main.checkout.card_invoice_details')
                    @include('main.checkout.payment_details')
                </div>
            </div>
        </div>
    </section>
@endsection


@section('javascripts')
    @parent

    <script>
        $(document).ready(function() {

            var $elem_county_default = $('[name="shipment_county_default"]');

            var CheckoutShipping = {
                loading: false,
                cont: 'card-payment-form',
                elem_county_default: $('[name="shipment_county_default"]'),
                init: function(cont)
                {
                    this.cont = cont;
                    this.loading = false;

                    if($('[data-book]').length > 0)
                    {
                        this.startShippingLayout();
                        this.calculateShippingTotal('po_shipping_zip')
                    }
                },
                startShippingLayout: function()
                {

                    if($('.shipping_price_div').length < 1){
                        $('.sub-total-box').parent().before($(CheckoutShipping.template(0.00)));
                        $('.sub-total-box').parent().before($(CheckoutShipping.template_tax(0.00, 0)));
                    } else {
                        $('[name="shipment_zip"]').val('');
                        this.resetShippingRates();
                    }

                    if($('input[name="shipment_state_default"]').val() == 'North Carolina') {
                        this.elem_county_default.attr('required', 'required');
                        this.elem_county_default.parent().show();
                    }

                    $('[type="submit"]').attr('disabled',true)

                },
                resetShippingRates: function() {
                    $('.shipping_price_div').find('#shipping-rate').text('$0');
                    $('[name="shipping_fee"]').val(0);
                    $('[name="state_tax"]').val(0);
                    $('.tax_div').find('#state-tax').text('$0');
                    $('.tax_div').find('#state-tax-percentage').text('0');
                    $('.total-price').text('$'+$('[name="amount"]').val());
                },
                template: function(data){
                    var amount = (data < 1)?'$0.00':'$'+data;
                    var ie_bff = ' <div class="row shipping_price_div" style="margin-bottom:7px;">';
                    ie_bff += '<div class="col-md-8">';
                    ie_bff +=  '<span class="text-left">Shipping (UPS Provider):</span></div>';
                    ie_bff +=  '<div class="col-md-4 text-right" id="shipping-rate">'+amount+'</div>';
                    ie_bff += '</div>';
                    return ie_bff;
                },
                template_tax: function(data, rate){
                    var amount = (data < 1) ? '$0.00' : '$' + data;
                    var ie_bff = ' <div class="row tax_div" style="margin-bottom:7px;">';
                    ie_bff += '<div class="col-md-8">';
                    ie_bff +=  '<span class="text-left">Sales Tax (<em id="state-tax-percentage">'+rate+'</em>%):</span></div>';
                    ie_bff +=  '<div class="col-md-4 text-right" id="state-tax">'+amount+'</div>';
                    ie_bff += '</div>';
                    return ie_bff;
                },
                calculateShippingTotal: function(clas){

                    var class_state = 'shipment_state', class_county = 'shipment_county';

                    if(clas === 'shipment_zip') {
                        class_state = 'shipment_state';
                        class_county = 'shipment_county';
                    } else {
                        class_state = 'po_shipping_state';
                        class_county = 'shipment_county_default';
                    }

                    var elem            = $('#' + this.cont + ' [name="'+clas+'"]');
                    var elem_state      = $('#' + this.cont + ' [name="'+class_state+'"]');
                    var elem_county     = $('#' + this.cont + ' [name="'+class_county+'"]');
                    var zip             = elem.val();
                    var totalWeight     = CheckoutShipping.getWeight();

                    var data = {
                        'order_id'      : $('[name="order_id"]').val(),
                        'current_total' : $('[name="current_total"]').val(),
                        'zip'           : zip,
                        'weight'        : totalWeight,
                        'name'          : $('[name="first_name"]').val() + ' ' + $('[name="last_name"]').val(),
                        'state'         : elem_state.val(),
                        'county'        : elem_county.val()
                    };

                    this.resetShippingRates();

                    elem.off('keyup blur').off('keyup blur').on('keyup blur', function(e){
                        data.zip =  elem.val();
                        if(data.zip.length >= 5){
                            elem.attr('disabled',true);
                            $('#' + this.cont + '[type="submit"]').attr('disabled',true);
                            CheckoutShipping.fetch(elem,data);
                            return true;
                        }
                    });

                    elem_state.off('change').change(function(e){
                        data.state = $(this).val();
                        if(data.state == 'North Carolina') {
                            elem_county.parent().show();
                            elem_county.attr('required', 'required');
                        } else {
                            elem_county.removeAttr('required');
                            elem_county.parent().hide();
                            CheckoutShipping.fetch(elem,data);
                        }
                        return true;
                    });

                    elem_county.off('change').change(function (e) {
                        data.county = $(this).val();
                        if(data.county != '')
                            CheckoutShipping.fetch(elem,data);
                    });

                    this.elem_county_default.off('change').change(function (e) {
                        data.county = $(this).val();
                        if(data.county != '')
                            CheckoutShipping.fetch(elem,data);
                    });

                    CheckoutShipping.fetch(elem,data);
                    return true;


                },
                showLoading: function() {
                    $('.payment-details .loading').show();
                    $('html,body').animate({
                        scrollTop: $('.payment-details').offset().top
                    }, 500);
                    this.loading = true;
                },
                hideLoading: function () {
                    $('.payment-details .loading').hide();
                    this.loading = false;
                },
                fetch: function(elem,data) {

                    if(this.loading) return false;

                    CheckoutShipping.showLoading();

                    $.ajax({
                        url: window.location.origin + '/ups-shipping-rate',
                        type: 'post',
                        data: data,
                        success: function(res){
                            elem.removeAttr('disabled');
                            CheckoutShipping.hideLoading();
                            CheckoutShipping.updateItemTax(res);
                        },
                        error: function(res){
                            CheckoutShipping.startShippingLayout();
                            elem.removeAttr('disabled');
                            elem.focus();
                            CheckoutShipping.hideLoading();
                            alert('Invalid zip code');
                            return true;
                        }
                    });
                },
                getWeight: function(){
                    var w = 0;
                    $('[data-weight]').each(function(k,v){
                        w += parseFloat($(this).data('weight'));
                    });
                    return w;
                },
                addZeroes:function( num ) {
                    // Cast as number
                    var num = Number(num);
                    // If not a number, return 0
                    if (isNaN) {
                        return 0;
                    }
                    // If there is no decimal, or the decimal is less than 2 digits, toFixed
                    if (String(num).split(".").length < 2 || String(num).split(".")[1].length<=2 ){
                        num = num.toFixed(2);
                    }
                    // Return the number
                    return num;
                },
                updateItemTax: function (result) {
                    var shipping_rates = result.shipping_rates;
                    var total_price =  parseFloat(result.total);

                    $('#state-tax').text('$'+result.state_tax);
                    $('#shipping-rate').text('$'+shipping_rates);
                    $('.sub-total-box').last().find('span').last().text('$' + total_price);
                    $('#state-tax-percentage').text(result.state_tax_percent);

                    $('[type="submit"]').removeAttr('disabled');
                    $('#' + this.cont + ' [name="state_tax"]').val(result.state_tax);
                    $('#' + this.cont + ' [name="shipping_fee"]').val(shipping_rates);
                    $('#' + this.cont + ' [name="current_total"]').val(result.total);

                }
            };

            CheckoutShipping.init('card-payment-form');

            $('input[name="cvv"]').NumericOnly({
                length: 4
            });

            $('input[name="shipment_zip"]').NumericOnly({
                length: 5
            });

            $('div.panel-option a.edit').click(function(event) {
                event.stopImmediatePropagation();
            });

            $('[name="payment-option"]').change(function(e){
                if($(this).val() == 'invoice') {
                    CheckoutShipping.init('invoice-payment-form');
                } else {
                    CheckoutShipping.init('card-payment-form');
                }

                e.preventDefault();
                e.stopImmediatePropagation();
            });

            $('div.panel-option').click(function (e) {
                var $that = $(this);
                if($that.hasClass('panel-selected'))
                    return false;
                $that.siblings().removeClass('panel-selected');
                $that.addClass('panel-selected');
                if($that.hasClass('panel-form-button')) {
                    $that.prev().find('i.fa').css('visibility', 'hidden');
                    $that.next().slideDown();
                    $that.next().find("input[type='text']").attr('required','required');
                    $that.next().find("input[type='number']").attr('required','required');
                    $elem_county_default.removeAttr('required');
                    $elem_county_default.parent().hide();
                    $that.find('i.fa').removeClass('fa-plus-circle');
                    $that.find('i.fa').addClass('fa-check-circle');
                    $that.find('i.fa').css('color','green');
                    $that.next().find('input.custom_flag').val(1);
                    if($that.parent().hasClass('shipment_container') && $('[data-book]').length > 0) {
                        CheckoutShipping.calculateShippingTotal('shipment_zip');
                    }
                } else {
                    $that.find('i.fa').css('visibility', 'visible');
                    $that.next().next().hide();
                    $that.next().next().find('input.custom_flag').val(0);
                    if($('input[name="shipment_state_default"]').val() === 'North Carolina'
                        && $('[data-book]').length > 0) {
                        $elem_county_default.attr('required', 'required');
                        $elem_county_default.parent().show();
                    } else {
                        $elem_county_default.removeAttr('required');
                        $elem_county_default.parent().hide();
                    }
                    $that.next().next().find("input[type='text']").removeAttr('required');
                    $that.next().next().find("input[type='number']").removeAttr('required');
                    $that.next().find('i.fa').removeClass('fa-check-circle');
                    $that.next().find('i.fa').addClass('fa-plus-circle');
                    $that.next().find('i.fa').css('color','inherit');
                    if($that.parent().hasClass('shipment_container') && $('[data-book]').length > 0) {
                        CheckoutShipping.calculateShippingTotal('po_shipping_zip');
                    }
                }
            });

        });

    </script>
@endsection