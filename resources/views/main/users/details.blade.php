@extends('main.main_layout')

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Training Details </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
                <div class="inner col-md-10 col-md-offset-1 user-training-details">
                    <fieldset>
                    <div class="panel panel-primary btn-flat">
                            <div class="panel-inner panel-heading btn-flat page-title">
                                <h2 class="text-center">{{ $title }}<br/>
                                <small>
                                    {{ $class->titleLocation }}
                                </small></h2>
                            </div>
                            <div class="panel-inner panel-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="panel panel-default btn-flat @if(!$materials->count()) hidden-sm hidden-xs @endif">
                                            <div class="panel-heading btn-flat">Course Materials</div>
                                            <div class="panel-body">
                                                <div class="control-group">
                                                    <ul class="students list">
                                                        @if ($materials->count() and isset($materials_not_expired) and $materials_not_expired)
                                                            @foreach($materials as $material)
                                                                @if ($material->is_file)
                                                                <li><a href="{{ route('main.course.getmaterial', [$material->filename]) }}"
                                                                       style="font-size: 14px !important;">{{ $material->title }}</a></li>
                                                                @else
                                                                    <li><a target="_blank" href="{{ $material->path }}"
                                                                           style="font-size: 14px !important;">{{ $material->title }}</a></li>
                                                                @endif
                                                            @endforeach
                                                        @elseif ($materials->count() and !$materials_not_expired)
                                                            <li style="list-style-type: none;"><div class="text-center">Subscription to materials is not available.</div></li>
                                                        @endif

                                                        @if(!$materials->count() && $available)
                                                            <li><div class="text-center">No materials available.</div></li>
                                                        @endif

                                                    </ul>
                                                    @if(!$available)
                                                        <div class="text-center">
                                                            <a href="{{ route('course.show', [$course->id]) }}">Sign Up Again</a>
                                                        </div>
                                                        <br/>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default btn-flat">
                                            <div class="panel-heading btn-flat">Book Updates</div>
                                            <div class="panel-body">
                                                <div class="control-group">
                                                    @if ($ebooks->count() and isset($materials_not_expired) and $materials_not_expired)
                                                        @foreach($ebooks as $book)

                                                            <ul class="students list">
                                                                <li>{{ $book->name }} <small>({!! $book->getLinkToBook() !!})</small></li>
                                                                @foreach($book->files as $ebook)
                                                                    <li>
                                                                        <strong style="display: inline-block; width: 150px; font-size: 12px">
                                                                            {{ $ebook->updated_at->toFormattedDateString() }}
                                                                        </strong>
                                                                        <a href="{{ route('main.course.getmaterial', [$ebook->filename]) }}">{{ $ebook->title }}</a>
                                                                    </li>
                                                                @endforeach

                                                                @if(!$book->files->count() && $available)
                                                                    <li><small>No book updates available.</small></li>
                                                                @endif
                                                            </ul>
                                                        @endforeach
                                                    @elseif ($ebooks->count() and isset($materials_not_expired) and !$materials_not_expired)
                                                        <li style="list-style-type: none;"><div class="text-center">Subscription to the book update is not available.</div></li>
                                                    @endif

                                                    @if(!$ebooks->count() && $available)
                                                        <ul class="students list">
                                                            <li class="text-center">No book updates available.</li>
                                                        </ul>
                                                    @endif
                                                    @if(!$available)
                                                        <div class="text-center">
                                                            <br/>
                                                            <a href="{{ route('course.show', [$course->id]) }}">Sign Up Again</a>
                                                            <br/><br/>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="well btn-flat">These files are available for download until <strong>{{ $available_date->toFormattedDateString() }}</strong></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="panel panel-default btn-flat">
                                            <div class="panel-heading btn-flat">Attendees</div>
                                            <div class="panel-body">
                                                {{--*/ $count = 0 /*--}}
                                                <ul class="students list attendees">
                                                    @foreach($students as $student)
                                                        <li>
                                                            <div class="col-sm-5 no-padding">{{ $student['name'] }}</div>
                                                            @if($student['cert'] && $student['confirmed'] == '1')
                                                                {{--*/ $count++ /*--}}
                                                                <div class="col-sm-7 no-padding"><a href="{{ route('download.cert', [
                                                                    'user_id' => $student['id'],
                                                                    'regOrderId' => $student['regOrderId']]) }}"
                                                                   title="Download Certificate"
                                                                   class="btn btn-xs btn-primary btn-flat">
                                                                    <i class="fa fa-download" ></i> Download</a>
                                                                    </div>
                                                            @else
                                                                  <div class="not_avail col-sm-7 no-padding">
                                                                      <small>Certificate not yet available.</small>
                                                                  </div>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                    @if($count)
                                                        <li>
                                                            <a href="{{ route('download.users.certs', [$regOrderId]) }}"
                                                               class="btn btn-flat btn-xs btn-primary">
                                                                <i class="fa fa-download"></i> Download All</a></li>
                                                    @endif
                                                    @if(!count($students))
                                                        <li>No attendees</li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="details">
                                            <dl class="dl-horizontal">
                                                <dt>Code</dt>
                                                <dd>{{  $course->sku }}</dd>
                                                <dt>Price</dt>
                                                <dd>${{ $course->price }}</dd>
                                                <dt>Length</dt>
                                                <dd>{{ $course->duration }}</dd>
                                                <dt>Category</dt>
                                                <dd>
                                                    @if ($course->courseCategories->count())
                                                        <div class="course-category">
                                                            @foreach ($course->courseCategories as $courseCat)
                                                                {{ $courseCat->category->name }}<br>
                                                            @endforeach
                                                        </div>
                                                @endif
                                                </dt>
                                            </dl>
                                        </div>
                                        <div class='desc'>{!! $description !!}</div>
                                        @if($rel_courses->count())<hr class="visible-sm visible-xs"/>@endif
                                    </div>
                                    <div class="col-md-5">

                                        @if($course->isCBT())
                                        <div class="panel panel-default btn-flat">
                                            <div class="panel-heading btn-flat">Login Instructions</div>
                                            <div class="panel-body">
                                                <div class="control-group">
                                                    <p>To login to H.E.L.P.™ and begin your training, follow these steps:</p>
                                                    <ol class="numbered-list">
                                                        <li>In a web browser, go to: http://learning.ercweb.com</li>
                                                        <li>Login with your User ID: <strong>{{ $user->contact->course_mill_user_id }}</strong></li>
                                                        <li>Enter the password you previously set for your H.E.L.P.™ account. If this is a NEW account please use <strong>{{ $user->contact->course_mill_user_id }}</strong> (case sensitive) as your temporary password.</li>
                                                        <li>You will be directed to your H.E.L.P.™ dashboard where you can start your training, print your course materials, view your transcript, and participate in the course’s Discussion Board.</li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($course->isWebcast())
                                        <div class="panel panel-default btn-flat">
                                            <div class="panel-heading btn-flat">Additional Resources</div>
                                            <div class="panel-body">
                                                    <dl>
                                                        <dt><strong>Webcast Link</strong></dt>
                                                        <dd>
                                                            <a
                                                                    target="_blank"
                                                                    id="meetingId"
                                                                    style="font-weight: normal; text-decoration: underline; padding-right: 10px"
                                                                    href="{{ $regOrder->meetingLink }}">{{ $regOrder->meetingLink }}</a>
                                                            <button class="copy-btn btn btn-default btn-xs"
                                                                    id="copy-btn"
                                                                    title="Copy"
                                                                    data-tooltip="Copy"
                                                                    data-clipboard-target="#meetingId"><i class="fa fa-copy"></i>
                                                            </button>

                                                        </dd>
                                                        <dt><strong>Online Test Link</strong></dt>
                                                        <dd>
                                                            <a
                                                                    target="_blank"
                                                                    id="examId"
                                                                    style="font-weight: normal; text-decoration: underline; padding-right: 10px"
                                                                    href="{{ $regOrder->testLink }}">{{ $regOrder->testLink }}</a>
                                                            <button class="copy-btn btn btn-default btn-xs"
                                                                    id="copy-btn"
                                                                    title="Copy"
                                                                    data-tooltip="Copy"
                                                                    data-clipboard-target="#examId"><i class="fa fa-copy"></i>
                                                            </button>
                                                        </dd>
                                                    </dl>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="panel panel-default btn-flat @if(!$rel_courses->count()) hidden-sm hidden-xs @endif">
                                            <div class="panel-heading btn-flat">Related Courses</div>
                                            <div class="panel-body">
                                                <ul class="students list">
                                                    @foreach($rel_courses as $rel_course)
                                                        <li>
                                                            {{ $rel_course->title }} ({!! $rel_course->getLinkedCourseTitle() !!})
                                                        </li>
                                                    @endforeach
                                                    @if(!$rel_courses->count())
                                                        <li><div class="text-center">No associated courses</div></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                    </div>
                    </fieldset>
                </div>
        </div>
    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            var clipboard = new Clipboard('.copy-btn');
            $('.copy-btn, .go-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    attr: 'data-tooltip'
                }
            });
            clipboard.on('success', function(e) {
                $('.copy-btn').qtip('option', 'content.text', 'Copy');
                $(e.trigger).qtip('option', 'content.text', 'Copied!');
            });
        })(jQuery);
    </script>
@endsection
