@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Edit Account Profile </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="account-info-container">
                <div class="inner row">
                    <div class="col-md-6">
                            <div class="account-info panel">
                                <div class="panel-inner panel-heading">
                                    <h2>User Account</h2>
                                </div>

                                {!! Form::open([
                                    'method' => 'POST',
                                    'url' => route('email.edit_profile'),
                                    'id' => 'edit-email-form',
                                    'class' => 'edit-profile-form'])
                                    !!}

                                    <div class="panel-inner panel-body">

                                        @if (Session::has('edit_email'))
                                            <div class="form-group col-md-12">
                                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                                            </div>
                                        @endif

                                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                                        {!! Form::label('email', 'Email: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('email', 'email', isset($user) ? $user->email : '' , [
                                            'class' => 'form-control',
                                            'id'    => 'email',
                                            'style' => 'display: inline;',
                                            ]) !!}

                                            @if ($errors->has('email'))
                                                {!! $errors->first('email', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif

                                        </div>
                                        {!! Form::label('Password', 'Password: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                                {!! Form::input('password', 'password',  '', [
                                                'class' => 'form-control',
                                                'id'    => 'password',
                                                'style' => 'display: inline;',
                                                ]) !!}

                                                @if ($errors->has('password'))
                                                    {!! $errors->first('password', '<div class="alert alert-danger">:message</div>') !!}
                                                @endif

                                                <span class="profile-notif">
                                                    <i class="small">Password is required to change email.</i>
                                                </span>

                                                @if ($user->contact->course_mill_user_id)
                                                    <span class="profile-notif">
                                                        <i class="small">
                                                            Updating the email will also update the email in CourseMill.
                                                        </i>
                                                    </span>
                                                @endif

                                                <button type="submit" class="btn btn-primary btn-save">
                                                    Save
                                                </button>
                                            </div>
                                    </div>

                                {!! Form::close() !!}


                                {!! Form::open(['method' => 'POST', 'url' => route('password.edit_profile'),  'class' => 'edit-profile-form']) !!}

                                    <div class="panel-inner panel-body">

                                        <h4 class="label-divide">Change Password</h4>

                                        @if (Session::has('edit_password'))
                                            <div class="form-group col-md-12">
                                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                                            </div>
                                        @endif

                                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                                        {!! Form::label('new_password', 'New Password: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('password', 'new_password', '', [
                                            'class' => 'form-control',
                                            'id'    => 'new_password',
                                            'style' => 'display: inline;',
                                            ]) !!}

                                            @if ($errors->has('new_password'))
                                                {!! $errors->first('new_password', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::label('re_new_password', 'Confirm New Password: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('password', 're_new_password', '', [
                                            'class' => 'form-control',
                                            'id'    => 're_new_password',
                                            'style' => 'display: inline;',
                                            ]) !!}

                                            @if ($errors->has('re_new_password'))
                                                {!! $errors->first('re_new_password', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::label('old_password', 'Old Password: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('password', 'old_password', '', [
                                            'class' => 'form-control',
                                            'id'    => 'old_password',
                                            'style' => 'display: inline;',
                                            ]) !!}

                                            @if ($errors->has('old_password'))
                                                {!! $errors->first('old_password', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif

                                            <button type="submit" class="btn btn-primary btn-save btn-save-email">Save</button>
                                        </div>
                                    </div>

                                {!! Form::close() !!}



                                {!! Form::open([
                                'id'    => 'toggleUserStatus',
                                'method' => 'POST',
                                'url' => route('user.toggleUserStatus'),
                                'class' => 'edit-profile-form'
                                ]) !!}

                                {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}
                                @inject('userService', 'App\Services\UserService')
                                <div class="panel-inner panel-body">

                                    <h4 class="label-divide">Deactivate Account</h4>
                                    <div class="form-group">
                                        @if ($user->contact->course_mill_user_id)
                                            <span class="profile-notif">
                                                <i class="small">
                                                    Updating the status will also update the status in CourseMill.
                                                </i>
                                            </span>
                                        @endif
                                    </div>

                                    @if($userService->validForDeactivation($user->contact->id))
                                        <div class="col-md-4 col-md-offset-4 margin-bottom">
                                            <button type="button" data-text="de-activate" class="btn btn-danger submitButton">
                                                Deactivate Account
                                            </button>
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <p class="alert alert-warning alert-deactivate">Account cannot be deactivated due to an active class</p>
                                            <ul class='class-list'>
                                            @foreach ($userService->getActiveClasses($user->contact->id) as $class)
                                                <li><i class="small">{{ $class->course->title }}</i></li>
                                            @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                {!! Form::close() !!}

                            </div>
                    </div>

                    <div class="col-md-6">
                            <div class="account-info panel">
                                <div class="panel-inner panel-heading">
                                    <h2>Personal Information</h2>
                                </div>

                                {!! Form::open(['method' => 'POST', 'url' => route('personal_info.edit_profile'),  'class' => 'edit-profile-form']) !!}

                                    <div class="panel-inner panel-body">

                                        @if (Session::has('edit_personal_info'))
                                            <div class="form-group col-md-12">
                                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                                            </div>
                                        @endif

                                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}
                                        {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                                        {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'first_name', isset($userDetails) ? $userDetails->first_name : '', [
                                            'class' => 'form-control',
                                            'id'    => 'first_name',
                                            'style' => 'display: inline;',
                                            'required'  => 'required'
                                            ]) !!}

                                            @if ($errors->has('first_name'))
                                                {!! $errors->first('first_name', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'last_name', isset($userDetails) ? $userDetails->last_name : '', [
                                            'class' => 'form-control',
                                            'id'    => 'last_name',
                                            'style' => 'display: inline;',
                                            'required'  => 'required'
                                            ]) !!}

                                            @if ($errors->has('last_name'))
                                                {!! $errors->first('last_name', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('company', 'Company: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'company', isset($userDetails) ? $userDetails->company : '', [
                                            'class' => 'form-control',
                                            'id'    => 'company',
                                            'style' => 'display: inline;',
                                            'required' => 'required',
                                            ]) !!}

                                            @if ($errors->has('company'))
                                                {!! $errors->first('company', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'title', isset($userDetails) ? $userDetails->title : '', [
                                            'class' => 'form-control',
                                            'id'    => 'company',
                                            'style' => 'display: inline;',
                                            ]) !!}

                                            @if ($errors->has('title'))
                                                {!! $errors->first('title', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('address1', 'Address1: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'address1', isset($userDetails) ? $userDetails->address1 : '', [
                                            'class' => 'form-control',
                                            'id'    => 'address1',
                                            'style' => 'display: inline;',
                                            'required' => 'required',
                                            ]) !!}

                                            @if ($errors->has('address1'))
                                                {!! $errors->first('address1', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::label('address2', 'Address2: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'address2', isset($userDetails) ? $userDetails->address2 : '', [
                                            'class' => 'form-control',
                                            'id'    => 'address2',
                                            'style' => 'display: inline;'
                                            ]) !!}

                                            @if ($errors->has('address2'))
                                                {!! $errors->first('address2', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('city', 'City: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'city', isset($userDetails) ? $userDetails->city : '', [
                                            'class' => 'form-control',
                                            'id'    => 'city',
                                            'style' => 'display: inline;',
                                            'required'  => 'required'
                                            ]) !!}

                                            @if ($errors->has('city'))
                                                {!! $errors->first('city', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('state', 'State: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::select('state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->state : '', [
                                                'id'    => 'state',
                                                'class' => 'form-control',
                                                'style' => 'display: inline;',
                                                'required' => 'required'
                                            ]) !!}

                                            @if ($errors->has('state'))
                                                {!! $errors->first('state', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('zip', 'Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'zip', isset($userDetails) ? $userDetails->zip : '', [
                                            'class' => 'form-control',
                                            'id'    => 'zip',
                                            'style' => 'display: inline;',
                                            'required'  => 'required'
                                            ]) !!}

                                            @if ($errors->has('zip'))
                                                {!! $errors->first('zip', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                        {!! Form::rLabel('phone', 'Phone: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('text', 'phone',  isset($userDetails) ? $userDetails->phone : '', [
                                                'class' => 'form-control',
                                                'id'    => 'phone',
                                                'style' => 'display: inline;',
                                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                                'data-mask',
                                                'required' => 'required',
                                            ]) !!}

                                            @if ($errors->has('phone'))
                                                {!! $errors->first('phone', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif
                                        </div>

                                            {!! Form::label('phone_local', 'Extension: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'phone_local',  isset($userDetails) ? $userDetails->phone_local : '', [
                                                    'class' => 'form-control',
                                                    'id'    => 'phone_local',
                                                    'style' => 'display: inline;',
                                                ]) !!}

                                                @if ($errors->has('phone'))
                                                    {!! $errors->first('phone', '<div class="alert alert-danger">:message</div>') !!}
                                                @endif
                                            </div>

                                        {!! Form::label('fax', 'Fax: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8">
                                            {!! Form::input('tel', 'fax', isset($userDetails) ? $userDetails->fax : '', [
                                                'class' => 'form-control',
                                                'id'    => 'fax',
                                                'style' => 'display: inline;',
                                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                                'data-mask',
                                                'placeholder' => 'Fax'
                                            ]) !!}

                                            @if ($errors->has('fax'))
                                                {!! $errors->first('fax', '<div class="alert alert-danger">:message</div>') !!}
                                            @endif

                                            <button type="submit" class="btn btn-primary btn-save">Save</button>
                                        </div>
                                    </div>

                                {!! Form::close() !!}


                                {!! Form::open(['method' => 'POST', 'url' => route('same_as_billing_or_shipping.edit_profile'),  'class' => 'edit-profile-form']) !!}

                                    {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                                    {!! Form::input('hidden', 'address_same_as_billing', 0) !!}

                                    <div class="panel-inner panel-body">

                                        @if (Session::has('edit_billing_info'))
                                            <div class="form-group col-md-12">
                                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                                            </div>
                                        @endif

                                        <h4 class="label-divide">Billing</h4>

                                        {!! Form::label('same_as_billing', 'Same As Address: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8 checkbox">
                                            {!! Form::checkbox('address_same_as_billing', '',
                                                $userDetails->address_same_as_billing ? true : false, [
                                                    'class' => 'same-as',
                                                    'id'    => 'address_same_as_billing',
                                                    'style' => 'display: inline',
                                            ]) !!}
                                        </div>

                                        {{-- */ $displayBilling = $user->contact->address_same_as_billing ? 'none' : 'block'; /* --}}

                                        <div class="address_same_as_billing" style="clear:both; display: {{ $displayBilling }};">

                                            {!! Form::rLabel('billing_address', 'Billing Address: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'billing_address', isset($userDetails) ? $userDetails->billing_address : '', [
                                                'class' => 'form-control',
                                                'id'    => 'billing_address',
                                                'style' => 'display: inline;',
                                                'required' => 'required',
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('billing_city', 'Billing City: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'billing_city', isset($userDetails) ? $userDetails->billing_city : '', [
                                                'class' => 'form-control',
                                                'id'    => 'billing_city',
                                                'style' => 'display: inline;',
                                                'required'  => 'required'
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('billing_state', 'Billing State: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::select('billing_state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->billing_state : '', [
                                                    'id'    => 'billing_state',
                                                    'class' => 'form-control',
                                                    'style' => 'display: inline;',
                                                    'required' => 'required'
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('billing_zip', 'Billing Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'billing_zip', isset($userDetails) ? $userDetails->billing_zip : '', [
                                                'class' => 'form-control',
                                                'id'    => 'billing_zip',
                                                'style' => 'display: inline;',
                                                'required'  => 'required'
                                                ]) !!}

                                                <button type="submit" class="btn btn-primary btn-save">Save</button>
                                            </div>

                                        </div>
                                    </div>

                                {!! Form::close() !!}


                                {!! Form::open(['method' => 'POST', 'url' => route('same_as_billing_or_shipping.edit_profile'),  'class' => 'edit-profile-form']) !!}

                                    {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                                    {!! Form::input('hidden', 'address_same_as_shipping', 0) !!}

                                    <div class="panel-inner panel-body" style="padding-bottom: 15px !important;">

                                        @if (Session::has('edit_shipping_info'))
                                            <div class="form-group col-md-12">
                                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                                            </div>
                                        @endif

                                        <h4 class="label-divide">Shipping</h4>

                                        {!! Form::label('address_same_as_shipping', 'Same As Address: ', ['class' => 'col-sm-4 control-label']) !!}
                                        <div class="form-group col-md-8 checkbox">
                                            {!! Form::checkbox('same_as_shipping', '', $userDetails->address_same_as_shipping? true : false, [
                                                'class' => 'same-as',
                                                'id'    => 'address_same_as_shipping',
                                                'style' => 'display: inline;'
                                            ]) !!}
                                        </div>

                                        {{-- */ $displayShipping = $user->contact->address_same_as_shipping ? 'none' : 'block'; /* --}}

                                        <div class="address_same_as_shipping" style="clear:both; display: {{ $displayShipping }}">

                                            {!! Form::rLabel('shipping_address', 'Shipping Address: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'shipping_address', isset($userDetails) ? $userDetails->shipping_address : '', [
                                                'class' => 'form-control',
                                                'id'    => 'shipping_address',
                                                'style' => 'display: inline;',
                                                'required' => 'required',
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('shipping_city', 'Shipping City: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'shipping_city', isset($userDetails) ? $userDetails->shipping_city : '', [
                                                'class' => 'form-control',
                                                'id'    => 'shipping_city',
                                                'style' => 'display: inline;',
                                                'required'  => 'required'
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('shipping_state', 'Shipping State: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::select('shipping_state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->shipping_state : '', [
                                                    'id'    => 'billing_state',
                                                    'class' => 'form-control',
                                                    'style' => 'display: inline;',
                                                    'required' => 'required'
                                                ]) !!}
                                            </div>

                                            {!! Form::rLabel('shipping_zip', 'Shipping Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                                            <div class="form-group col-md-8">
                                                {!! Form::input('text', 'shipping_zip', isset($userDetails) ? $userDetails->shipping_zip : '', [
                                                'class' => 'form-control',
                                                'id'    => 'shipping_zip',
                                                'style' => 'display: inline;',
                                                'required'  => 'required'
                                                ]) !!}

                                                <button type="submit" class="btn btn-primary btn-save">Save</button>
                                            </div>
                                        </div>
                                    </div>

                                {!! Form::close() !!}
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.confirmation_modal')
@endsection

@section('javascripts')
    @parent

    <script>
        (function ($){
            $('.same-as').on('change', function() {
                var $this = $(this),
                    id    = $this.attr('id'),
                    $container = $('div.' + id),
                    flag = $this.prop('checked'),
                    contact_id    = "{{ $userDetails->id }}";

                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('same_as_address') }}",
                        data : {
                            contact_id : contact_id,
                            column : id,
                            flag: flag
                        },
                        beforeSend : function(data) {
                            $this.after("<label><i>Saving...</i></label>");
                        },
                        success : function(response) {
                            setTimeout(function() {
                                $this.siblings('label').remove();
                                if(flag)
                                    $container.hide();
                                else
                                    $container.show();
                            }, 1500)


                        }
                    });

            });
            var $form = $('#toggleUserStatus');
            $('.submitButton').on('click', function() {
                var text = $(this).data('text');
                Modal.showConfirmationModal(
                        "Are you sure you want to "+text+" this user?",
                        'User Account',
                        function() {
                            $form.submit();
                            return false;
                        });
                return false;
            });
            $( "#company" ).autocomplete({
                source: "/api/companies",
                minLength: 2,
            });
        })(jQuery)
    </script>
@endsection