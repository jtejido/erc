<div class="item panel">
    <div class="panel-inner panel-heading">
        <h2>Discounts Summary</h2>
    </div>
    <div class="panel-inner panel-body">

        <div class="row">
            <div class="col-sm-9">

            </div>
            <div class="col-sm-3">

            </div>
        </div>

        @if ($subscriptionDiscounts && $subscriptionDiscounts->count())
            <div class="attendees discounts">
                <h5>Yearly Subscription Discount</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($subscriptionDiscounts as $subscriptionDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractSubscriptionDiscount($subscriptionDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $subscriptionDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($subscriptionDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        @if ($militaryDiscounts && $militaryDiscounts->count())
            <div class="attendees discounts">
                <h5>GSA Pricing Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($militaryDiscounts as $militaryDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractMilitaryDiscount($militaryDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $militaryDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($militaryDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        @endif

        @if ($halfPriceDiscounts && $halfPriceDiscounts->count())
            <div class="attendees discounts">
                <h5>Group Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($halfPriceDiscounts as $halfPriceDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractHalfPriceInfo($halfPriceDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $halfPriceDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($halfPriceDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if ($combinationDiscounts && $combinationDiscounts->count())
            <div class="attendees discounts">
                <h5>HWM-DOT Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($combinationDiscounts as $combinationDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractClassCombinationInfo($combinationDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $combinationDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($combinationDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if ($couponDiscounts && $couponDiscounts->count())
            <div class="attendees discounts">
                <h5>Coupon Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($couponDiscounts as $couponDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractCouponDiscountInfo($couponDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $couponDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($couponDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count())
            <div class="attendees discounts">
                <h5>Order Adjustment Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractOrderAdjustment($orderAdjustmentDiscount) }}

                                            @if ($orderAdjustmentDiscount->adjustable_type == \App\Utilities\Constant::CLASS_REGISTRATION and
                                                $orderAdjustmentDiscount->adjustable->is_new)
                                                <span class="label label-warning">New</span>
                                            @endif

                                            @if ($orderAdjustmentDiscount->adjustment_notes)
                                                <i class="fa fa-info-circle" data-container="body"
                                                   data-toggle="popover"
                                                   data-placement="top"
                                                   data-content="{!! $orderAdjustmentDiscount->adjustment_notes . ' Created: ' . $orderAdjustmentDiscount->created_at->format('F d, Y') !!}">
                                                </i>
                                            @endif
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $orderAdjustmentDiscount->adjusted_deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($orderAdjustmentDiscounts->sum('computed_deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        @endif

        @if ($creditDiscounts && $creditDiscounts->count())
            <div class="attendees discounts">
                <h5>Corporate Seat Discounts</h5>
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list">
                            @foreach ($creditDiscounts as $creditDiscount)
                                <li>
                                    <span class="col-xs-9 no-padding">
                                    {{ DiscountUtility::extractCreditDiscount($creditDiscount) }}
                                    </span>
                                    <span class="col-xs-3 no-padding">
                                    <span class="price pull-right">${{ $creditDiscount->deduction }}</span>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <div class="subtotal-container">
                            <small>Discount Sub-total:</small>
                            <div>
                                <span class="total-price subtotal-price"> ${{ number_format($creditDiscounts->sum('deduction'), 2) }}</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        @endif

    </div>
</div>