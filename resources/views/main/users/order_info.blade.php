@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Order Details </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="cart">
                <div class="inner col-md-10 col-md-offset-1">

                    @include('includes.success_message')

                    @if ($items)
                        @foreach ($items as $item)
                            <div class="order-items">
                                {!! OrderUtility::extractItems($item, $user, null, $items) !!}
                            </div>
                        @endforeach
                    @else
                        <i>No Orders found.</i>
                    @endif

                    @if ($order && (($combinationDiscounts && $combinationDiscounts->count()) ||
                                    ($halfPriceDiscounts && $halfPriceDiscounts->count()) ||
                                    ($couponDiscounts && $couponDiscounts->count())) ||
                                    ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count()) ||
                                    ($militaryDiscounts && $militaryDiscounts->count()) ||
                                    ($subscriptionDiscounts && $subscriptionDiscounts->count()) ||
                                    ($creditDiscounts && $creditDiscounts->count()))

                        @include('main.users._discount-summary')
                    @endif

                    @if (!is_null($total))
                        <div class="item current-total">
                        @if(!is_null($shipping_rates) AND $shipping_rates > 0)
                        <div class="row">
                            <div class="col-sm-9">

                            </div>
                            <div class="col-sm-3 subtotal-main-container">
                                <div class="subtotal-container">
                                    <small>Shipping Fee:</small>
                                    <div>
                                        <span class="total-price subtotal-price"> ${{ number_format($shipping_rates, 2) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                            @if(!is_null($order_tax) AND $order_tax > 0)
                                <div class="row">
                                    <div class="col-sm-9">

                                    </div>
                                    <div class="col-sm-3 subtotal-main-container">
                                        <div class="subtotal-container">
                                            <small>Sales Tax:</small>
                                            <div>
                                                <span class="total-price subtotal-price"> ${{ number_format($order_tax, 2) }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        <div class="row">
                            <div class="col-sm-9">

                            </div>
                            <div class="col-sm-3 subtotal-main-container">
                                <div class="subtotal-container">
                                    <small>Current Total:</small>
                                    <div>
                                        <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                    @else
                        <p class="no-order"><i>No orders.</i></p>
                    @endif

                    @if ($order and $orderAdjustments->count())
                        {!! OrderModificationUtility::extractOrderAdjustment($orderAdjustments, $order->paid_total) !!}
                    @endif

                    @if ($order and $orderAdjustmentHistories->count())
                        {!! OrderModificationUtility::extractOrderAdjustmentHistory($orderAdjustmentHistories) !!}
                    @endif

                    <div class="text-center padding-bottom" style="padding-bottom: 15px">
                            <a href="{{ route('user.home') }}" class="btn btn-default btn-lg">
                                Back
                            </a>
                    </div>

                </div>
            </div>
        </div>

        @include('includes.notification_modal')
        @include('includes.cancellation_modal')
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        (function() {
            cancelRegistration.init();
            confirmationHandler.init();
        })();
    </script>
@endsection

