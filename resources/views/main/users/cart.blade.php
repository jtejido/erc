@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Shopping Cart <small id="admin_viewing" style="display: none">Admin is Viewing</small> </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="cart">
                <div class="inner col-md-10 col-md-offset-1">

                    @include('includes.success_message')
                    @include('includes.error_message')

                    @if (Session::has('coupon_applied') && Session::get('coupon_applied'))
                        {{-- */ $label = 'success' /* --}}
                    @else
                        {{-- */ $label = 'danger' /* --}}
                    @endif

                    @if (Session::has('coupon_message'))

                        <div class="alert alert-{{ $label }} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ Session::get('coupon_message') }}
                        </div>

                    @endif

                    @if ($items)
                        @foreach ($items as $item)
                            <div class="order-items">
                                {!! OrderUtility::extractItems($item, $user, null, $items) !!}
                            </div>
                        @endforeach
                    @endif

                    @if ($order && $corpSeatCredits)
                        {!! OrderUtility::extractCredits($order, $user, $corpSeatCredits) !!}
                    @endif

                    @if ($order && (($combinationDiscounts && $combinationDiscounts->count()) ||
                                    ($halfPriceDiscounts && $halfPriceDiscounts->count()) ||
                                    ($couponDiscounts && $couponDiscounts->count())) ||
                                    ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count()) ||
                                    ($militaryDiscounts && $militaryDiscounts->count()) ||
                                    ($subscriptionDiscounts && $subscriptionDiscounts->count()) ||
                                    ($creditDiscounts && $creditDiscounts->count()))

                        @include('main.users._discount-summary')
                    @endif

                    @if (!is_null($total))
                        <div class="item current-total">
                            <div class="row">
                                <div class="col-sm-9">

                                </div>
                                <div class="col-sm-3">
                                    <div class="subtotal-container">
                                        <small>Current Total:</small>
                                        <div>
                                            <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-sm-12 coupon-navigation">
                            <div class="text-center coupon-text">
                                <label><strong>Coupon Code</strong>&nbsp;&nbsp;<input  type="text" id="coupon-code" value="{{ Session::get('coupon_code') }}"
                                    style="width: 300px; display: inline; vertical-align: middle"/>
                                </label>
                                <input  type="hidden" id="order" value="{{ ($order) ? $order->id : 0 }}">
                                <button class="apply-coupon" id="apply-coupon"
                                        style="display: inline; width: 150px;  vertical-align: middle"
                                        data-href="{{ route('coupon.apply') }}">
                                    Apply
                                </button>
                            </div>
                        </div>
                    </div>


                    <div class="cart-navigation row">
                        <div class="col-md-6 column">
                            <a href="{{ route('courses') }}" class="btn btn-primary shopping-class pull-left">
                                <i class="fa fa-arrow-left"></i> Continue Shopping
                            </a>
                        </div>

                        @if ($total > 0)
                            <div class="col-md-6 column">
                                <a href="{{ route('user.checkout.get') }}" class="btn btn-warning checkout-class pull-right">
                                    <i class="fa fa-shopping-cart"></i> Proceed to Checkout
                                </a>
                            </div>
                        @else
                            <div class="col-md-6 column">
                                <button data-user_id="{{ $user->id }}" data-url="{{ route('order.complete') }}"
                                        class="btn btn-warning checkout-class pull-right cart-complete-order">

                                    <i class="fa fa-check"></i> Complete Order
                                </button>
                            </div>
                        @endif
                    </div>
                    @else
                        <p class="no-order"><i>No orders.</i></p>
                    @endif
                </div>
            </div>
        </div>

        @include('includes.notification_modal')
        @include('includes.processing')
    </section>
    <input type="hidden" name="user_id" id="user_id" value="{{ auth()->user()->id }}" />
    <input type="hidden" name="order_id" id="order_id" value="{{ $order->id or 0 }}" />

@endsection
@section('javascripts')
    @parent
<script>
    confirmationHandler.init();

    var user_id = $('#user_id').val();
    var order_id = $('#order_id').val();
    var socket = io("{{ env('SOCKET_URL') }}");

    socket.on("erc-channel:App\\Events\\EditOrderEvent", function(message){
        var data = message.data;

        if(data.order_id == order_id || data.user_id  == user_id) {
            if(data.save_flag == 1) {
                $('#processing_modal').modal({
                    'backdrop': 'static',
                    'keyboard': false
                });
                setTimeout(function(){ location.reload(); }, 3000);
            }

        }
    });
</script>
@endsection