@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Payment Summary </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="cart">
                <div class="inner col-md-12">
                    <div class="col-md-6 col-md-offset-3 checkout-wrapper">
                        <div class="payment-details panel">
                            <div class="panel-inner panel-heading">
                                <h2> Order Payment Summary </h2>
                            </div>
                            <div class="panel-inner panel-body">
                                @foreach ($order->paymentTransactions as $paymentTransaction)
                                    @if($paymentTransaction->isCheck())
                                        <li style="margin-bottom: 10px; display: block;">Check Number : {{ $paymentTransaction->masked_cc }}</li>
                                    @elseif($paymentTransaction->isCash())
                                        <li style="margin-bottom: 10px; display: block;">Payment via CASH</li>
                                    @else
                                        <li style="margin-bottom: 10px; display: block;">Credit Card Transaction ID: {{ $paymentTransaction->transaction_id }}</li>
                                        <li style="margin-bottom: 10px; display: block;">Credit Card Number (masked) : {{ $paymentTransaction->masked_cc }}</li>
                                    @endif
                                    <li style="margin-bottom: 10px; display: block;">Payment Date: {{ $paymentTransaction->created_at->format('F d, Y') }}</li>
                                    <li style="margin-bottom: 10px; display: block;">Payment Amount: ${{ number_format($paymentTransaction->amount, 2) }}</li>

                                    @if ($paymentTransaction->items)
                                        @foreach(json_decode($paymentTransaction->items)->items as $item)
                                            {!! OrderUtility::extractItemsFromPayment($paymentTransaction, $user, $item) !!}
                                        @endforeach
                                        <div class="subtotal" style="margin-bottom:0px;">Shipping Fee<span class="total-amount pull-right">{{ $paymentTransaction->shipping_fee }}</span></div>
                                        <div class="subtotal" style="margin-bottom:0px;">Sales Tax<span class="total-amount pull-right">{{ $paymentTransaction->state_tax_formatted }}</span></div>
                                        <div class="subtotal">Total<span class="total-amount pull-right">${{ number_format(json_decode($paymentTransaction->items)->total_amount, 2) }}</span></div>
                                    @endif
                                    <hr>

                                @endforeach


                            </div>
                        </div>
                        <div class="text-center">
                            <a class="btn btn-warning"
                               href="{{ route('receipt', ['order_id' => $order->id]) }}">
                                <i class="fa fa-print" aria-hidden="true"></i> Download Receipt</a>
                        </div>
                        <br/>
                        <div class="text-center padding-bottom" style="padding-bottom: 15px">
                            <a href="{{ route('user.home') }}" class="btn btn-default btn-lg">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
