@extends('main.main_layout')
@section('body-class', 'index')
@section('main_content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }

    var _links = {!! $links->toJson() !!};
    var _categories = {!! $categories->toJson() !!};
    data.links = _links;
    data.categories = _categories;

</script>
    <section class="main" ng-controller="MainLinks">
        <div class="container-fluid">
            <div class="row">

                    <div class="container">
                        <div class="row">
                            <div class="page-title">
                                <div class="border"></div>
                                <div class="inner">
                                    <h1> Helpful Links </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                        <div class="inner">
                            <div class="entry">
                                <div class="form-group col-md-2">
                                    <label for="category">Category</label>
                                    <select name="category" class="form-control"
                                        ng-model="data.current_view.category"
                                        ng-change="refresh_links()"
                                        ng-options="category.name for category in categories track by category.id">
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="text_view">Search</label>
                                    <input type="text" class="form-control" name="text_view"
                                        placeholder="Search" 
                                        ng-model-options="{ updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } }"
                                        ng-change="refresh_links()"
                                        ng-model="data.current_view.text">
                                </div>
                                
                                <div class="row"
                                    ng-repeat="category in categories"
                                    ng-if="category.id != 0"
                                    ng-show="(data.current_view.category.id === 0
                                        || data.current_view.category.id === category.id
                                    )"
                                    >
                                    <h2>@{{ category.name }}</h2>
                                    <hr>
                                    <div class="col-md-12"
                                        ng-repeat="link in active_links | category_view:category.id track by $index"
                                        ng-if="link.name">
                                        <a href="@{{ link.url }}" target="_blank">
                                            @{{ link.name }}
                                        </a>
                                        <p><em>@{{ link.url }}</em></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </section><!-- .main -->
    
@stop