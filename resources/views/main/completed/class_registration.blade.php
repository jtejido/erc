@if ($user->isMember())
        <!-- Extract data for frontend view -->

<div class="item panel">

        <div class="panel-inner panel-heading">
            <h2>{{ $title }}</h2>
        </div>
        <div class="panel-inner panel-body">

            <div class="row">
                <div class="col-sm-5">
                    @if (strlen($schedule) || strlen($venue))
                        <div class="description">

                            <div class="schedule">{{ $schedule }}</div>
                            <small>
                                @if (isset($venue))
                                    {!! $venue !!}
                                @endif
                            </small>
                        </div>
                    @endif
                    <h4 class="attendee-label">Attendees:</h4>
                        <div class="attendees-modification">
                            <a href="{{ $attendeesRoute }}"
                               class="btn btn-info"><i class="fa fa-exchange"></i> {{ $attendeeText }}</a>

                            <a href="{{ route('order.attendees.add.get', $item->id) }}"
                               class="btn btn-info"><i class="fa fa-plus"></i> ADD</a>
                        </div>
                </div>
                <div class="col-sm-7">
                    <div class="list-container">
                    <div class="list" id="list">
                    @foreach ($classRegistrations as $classRegistration)
                        <div class="list-row">
                            <div class="column col-xs-8">
                                <label>
                            @if ($classRegistration->is_new)

                                    <input type="checkbox"
                                           name="class-registrations"
                                           class="new checkbox-cancel-registration"
                                           data-is_new="1"
                                           value="{{ $classRegistration->id }}"
                                           style="height: 13px;">

                            @elseif ($originatingAgent and !$isCBT  and in_array($classRegistration->id, $activeClassRegistrations) and isset($orderId) and !$newRegistrations and !$newRegistrationsInOrder and !$orderAdjustments)

                                    <input type="checkbox"
                                           class="existing checkbox-cancel-registration"
                                           data-is_new="0"
                                           name="class-registrations"
                                           value="{{ $classRegistration->id }}"
                                           style="height: 13px;">
                            @endif
                            {{ ucfirst($classRegistration->contact->first_name) }} {{ ucfirst($classRegistration->contact->last_name) }}
                                </label>
                            @if ($classRegistration->is_cancelled)
                                    <small class="label label-danger">{{ \App\Utilities\Constant::CANCELLED }}</small>
                            @endif
                            @if ($classRegistration->is_new)
                                    <small class="label label-warning">NEW</small>
                            @endif
                            </div>

                            <div class="column col-xs-4">
                                <span class="price">${{ $classRegistration->item_charge }}</span>
                            </div>
                        </div>
                    @endforeach
                    </div>
                    <div class="cancel-button-container">

                        @if (($classRegistration->is_new) or ($originatingAgent and
                              !$isCBT  and
                              count($activeClassRegistrations) and
                              isset($orderId) and
                              !$newRegistrations and
                              !$newRegistrationsInOrder and
                              !$orderAdjustments))
                            <button id="cancel-registration"
                                    class="btn btn-danger cancel-registration"
                                    data-agent_id="{{ $agent->id }}"
                                    data-order_id="{{ $orderId }}"
                                    data-order_item_id="{{ $orderItemId }}"
                                    data-registration_order_id="{{ $registrationOrderId }}" disabled>
                                <i class="fa fa-ban"></i> Cancel Registration
                            </button>

                            <input type="hidden" id="cancel-url" value="{{ route('registration.query.cancellable') }}">
                        @endif
                    </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-9">
                    @if ($newRegistrations or $newRegistrationsInOrder or $orderAdjustments)
                        <div class="existing-adjustment-notif text-danger">
                                There are unpaid/unconfirmed adjustments. No cancellation for paid registrations.
                            </div>
                    @endif
                </div>
                <div class="col-sm-3">
                    <div class="subtotal-container">
                        <small>Total Attendees Cost:</small>
                        <div>
                            <span class="total-price subtotal-price"> ${{ number_format($total, 2) }}</span>
                        </div>
                    </div>
                </div>
            </div>




        </div>

</div>


@else

<!-- Extract data for admin/csr view -->

<div class="col-md-12 box-container">
    <div class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $title }}</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">

            <input type="hidden" id="order-item-id" value="{{ $item->id }}">

            @if (strlen($schedule) || strlen($venue))
                <div class="details">
                    <p class="schedule"><i>{{ $schedule }}</i> <div class="venue pull-right">{!! $venue !!}</div></p>
                </div>
            @endif

            <div class="attendees list-container">
                <h5>Attendees</h5>

                <ul class="list">
                    @foreach ($classRegistrations as $classRegistration)
                        <li class="attendee">

                            @if ($classRegistration->is_new)
                                <input type="checkbox" class="checkbox checkbox-cancel-registration"
                                       name="class-registrations"
                                       data-is_new="1"
                                       value="{{ $classRegistration->id }}">
                            @elseif ($originatingAgent and !$isCBT  and in_array($classRegistration->id, $activeClassRegistrations) and isset($orderId) and !$newRegistrations and !$newRegistrationsInOrder and !$orderAdjustments)
                                <input type="checkbox"
                                       class="checkbox checkbox-cancel-registration"
                                       data-is_new="0"
                                       name="class-registrations" value="{{ $classRegistration->id }}">
                            @else
                                <span class="checkbox"></span>
                            @endif

                            {{ ucfirst($classRegistration->contact->first_name) }} {{ ucfirst($classRegistration->contact->last_name) }}

                            @if ($classRegistration->is_new)
                                <span class="label label-warning">New</span>
                            @endif

                            <span class="pull-right item-price-container">
                                @if ($classRegistration->is_cancelled)
                                    <span class="label label-warning col-xs-4 text-center cancel-label">
                                        {{ \App\Utilities\Constant::CANCELLED }}
                                    </span>

                                    <a href="javascript:void(0)"
                                       data-id="{{ $classRegistration->id }}"
                                       data-user_id="{{ $agent->id }}"
                                       class="label label-success col-xs-4 col-xs-offset-1 cancel-label restore-cancelled">
                                        {{ \App\Utilities\Constant::RESTORE }}
                                    </a>
                                    <span class="price col-xs-3 pull-right">
                                        {{ $classRegistration->item_charge }}
                                    </span>
                                @elseif ($classRegistration->subscriptionDiscount)
                                    $<input type="text" class="price no-border" value="{{ $classRegistration->item_charge }}" disabled>
                                    @if (!$newRegistrations and !$newRegistrationsInOrder and !$orderAdjustments and !$isCBT)
                                        <a href="{{ route('reschedule.edit', [
                                                'orderItemId'   => $classRegistration->registrationOrder->orderItem->id,
                                                'contactId'     => $classRegistration->attendee_contact_id
                                            ]) }}" class="btn btn-flat btn-xs btn-warning">
                                                <i class="fa fa-calendar" aria-hidden="true"></i> Re-schedule
                                            </a>
                                    @endif
                                @elseif ($classRegistration->corporate_seat_credit_applied)
                                    $<input type="text" class="price no-border" value="{{ $classRegistration->item_charge }}" disabled>
                                    @if (($item->status == \App\Utilities\Constant::PENDING or $item->status == \App\Utilities\Constant::IN_PROCESS))
                                        <button class="btn btn-flat btn-xs btn-danger btn-remove-credit"
                                                data-id="{{ $classRegistration->id }}"
                                                data-url="{{ route('credit.remove') }}">
                                            Remove Credit
                                        </button>
                                    @endif
                                    @if (!$newRegistrations and !$newRegistrationsInOrder and !$orderAdjustments and !$isCBT)
                                        <a href="{{ route('reschedule.edit', [
                                                'orderItemId'   => $classRegistration->registrationOrder->orderItem->id,
                                                'contactId'     => $classRegistration->attendee_contact_id
                                            ]) }}" class="btn btn-flat btn-xs btn-warning">
                                                <i class="fa fa-calendar" aria-hidden="true"></i> Re-schedule
                                            </a>
                                    @endif
                                @else
                                    @if ($canRemove)
                                        $<input type="text" class="price"
                                                data-original-price="{{ $classRegistration->item_charge }}"
                                                value="{{ $classRegistration->item_charge }}">
                                        <button class="btn btn-flat btn-xs btn-success order-modification-change-price"
                                                data-adjustable-id="{{ $classRegistration->id }}"
                                                data-adjustable-type="{{ \App\Utilities\Constant::CLASS_REGISTRATION }}"
                                                >
                                            Change
                                        </button>
                                        @if (!$newRegistrations and !$newRegistrationsInOrder and !$orderAdjustments and !$isCBT)
                                            <a href="{{ route('reschedule.edit', [
                                                'orderItemId'   => $classRegistration->registrationOrder->orderItem->id,
                                                'contactId'     => $classRegistration->attendee_contact_id
                                            ]) }}" class="btn btn-flat btn-xs btn-warning">
                                                <i class="fa fa-calendar" aria-hidden="true"></i> Re-schedule
                                            </a>
                                        @endif
                                    @else
                                        {{ $classRegistration->item_charge }}
                                    @endif
                                @endif
                            </span>
                        </li>
                    @endforeach

                    <li>

                        @if (($classRegistration->is_new) or ($originatingAgent and
                         !$isCBT  and
                         count($activeClassRegistrations) and
                         isset($orderId) and
                         !$newRegistrations and
                         !$newRegistrationsInOrder and
                         !$orderAdjustments))

                            <button id="cancel-registration"
                                    class="btn btn-flat btn-sm btn-danger btn-xs cancel-registration order-modification-btn"
                                    data-agent_id="{{ $agent->id }}"
                                    data-order_id="{{ $orderId }}"
                                    data-order_item_id="{{ $orderItemId }}"
                                    data-registration_order_id="{{ $registrationOrderId }}" disabled>
                                Cancel Registration
                            </button>

                            <input type="hidden" id="cancel-url" value="{{ route('admin.registration.query.cancellable') }}">

                        @endif

                        @if (count($activeClassRegistrations))
                            <a href="{{ $attendeesRoute }}"
                               class="btn btn-info btn-flat btn-xs order-modification-btn">
                                {{ $attendeeText }} <i class="fa fa-arrow-circle-right"></i>
                            </a>
                        @endif

                        <a href="{{ route('admin.order.attendees.add.get', [$agent->id, $orderItemId]) }}"
                           class="btn btn-info btn-flat btn-xs order-modification-btn">
                            Add Attendees <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </li>

                    @if ($newRegistrations or $newRegistrationsInOrder or $orderAdjustments)
                        <li>
                            <span class="required text-center">There are unpaid/unconfirmed adjustments. No cancellation for paid registrations.</span>
                        </li>
                    @endif
                </ul>
            </div>

            <hr>

            <div class="total">
                <b>Total Cost : </b>
                <span class="total-cost pull-right">${{ number_format($total, 2) }}</span>
            </div>
        </div>
    </div>
</div>
@endif

