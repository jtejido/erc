<div class="col-md-7 checkout-wrapper">

    <div class="pay-by-credit payment-option panel">
        <div class="panel-inner panel-heading">
            <h2> Payment Option </h2>
        </div>

        <div class="panel-inner panel-body">
            <select name="payment-option" class="form-control"  {{ (!$ccOnly) ? '' : 'disabled' }}>
                <option value="card">Credit / Debit Card</option>
                <option value="invoice">Purchase Order</option>
            </select>

            @if($ccOnly)
                <div class="form-control" style="height: auto"><small>NOTE: Your cart contains an Online Training which can only be paid via credit card.</small></div>
            @endif

        </div>
    </div>


    {!! Form::open([
        'id' => 'card-payment-form',
        'route' => 'order_mod.checkout.credit_card'])
    !!}
    <div class="pay-by-credit panel">
        <div class="panel-inner panel-heading">
            <h2> Pay by Credit Card </h2>
        </div>

        <div class="panel-inner panel-body">
            <div class="row">

                <div class="col-sm-12">

                    @include('errors.form_errors')

                    @if (Session::has('error_message'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! Session::get('error_message') !!}
                        </div>
                    @endif

                </div>

                @if (isset($order))
                    {!! Form::input('hidden', 'order_id', $order->id) !!}
                @endif

                @if (isset($total))
                    {!! Form::input('hidden', 'amount', $total) !!}
                    {!! Form::input('hidden', 'sales_tax', $sales_tax) !!}
                    {!! Form::input('hidden', 'user_id', $user->id) !!}
                    {!! Form::input('hidden', 'description', 'Payment of Order') !!}
                @endif

                @if (isset($items))
                    @foreach ($items as $item)
                        <input type="hidden" name="items[]" value="{{ $item->id }}" />
                    @endforeach
                @endif

                <div class="col-sm-3">
                    {!! Form::input('text', 'first_name', $user->contact->first_name, [
                        'placeholder' => 'First Name',
                        'required' => 'required'
                    ]) !!}
                </div>
                <div class="col-sm-3">
                    {!! Form::input('text', 'last_name', $user->contact->last_name, [
                        'placeholder' => 'Last Name',
                        'required' => 'required'
                    ]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::input('email', 'email', $user->email, [
                        'placeholder' => 'Email',
                        'required' => 'required'
                    ]) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    {!! Form::input('text', 'card_number', '', [
                        'placeholder' => 'Credit Card Number',
                        'required' => 'required'
                    ]) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::input('text', 'cvv', '', [
                        'placeholder' => 'CVV',
                        'required' => 'required'
                    ]) !!}
                </div>
            </div>

            <div class="row row-expiration">
                <div class="col-sm-8">
                    {!! Form::select('exp_month', $months, '', [
                    'class' => 'form-control cc-info',
                    'required' => 'required'
                ]) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::select('exp_year', $years, '', [
                    'class' => 'form-control cc-info',
                    'required' => 'required'
                ]) !!}
                </div>
            </div>
        </div>
    </div>

    @include('main.checkout.shipping_billing')

    <div class="cart-navigation row">
        <div class="col-md-6 column">
            <a href="{{ route('user.shopping_cart') }}" class="btn btn-primary pull-left">
                Cancel
            </a>
        </div>
        <div class="col-md-6 column">
            <button type="submit" class="btn btn-warning pull-right" @if(!isset($order)) disabled @endif>
                Place My Order
            </button>
        </div>
    </div>

    {!! Form::close() !!}

    {!! Form::open([
        'id' => 'invoice-payment-form' ,
        'class' => 'hidden',
        'route' => 'order_mod.checkout.invoice',
        'enctype' => 'multipart/form-data'])
    !!}
    <div class="pay-by-invoice panel">
        <div class="panel-inner panel-heading">
            <h2> Pay by Purchase Order </h2>
        </div>

        <div class="panel-inner panel-body">

            @if (isset($order))
                {!! Form::input('hidden', 'order_id', $order->id) !!}
            @endif

            @if (isset($total))
                {!! Form::input('hidden', 'amount', $total) !!}
                {!! Form::input('hidden', 'sales_tax', $sales_tax) !!}
                {!! Form::input('hidden', 'user_id', $user->id) !!}
                {!! Form::input('hidden', 'description', 'Payment of Order') !!}
                {!! Form::input('hidden', 'validate_url', route('ajax.file.validate'), ['id' => 'path-to-validate-file']) !!}
            @endif

            @if (isset($items))
                @foreach ($items as $item)
                    <input type="hidden" name="items[]" value="{{ $item->id }}" />
                @endforeach
            @endif

            <div class="row">

                <div class="col-sm-12">

                    @include('errors.form_errors')

                    @if (Session::has('error_message'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! Session::get('error_message') !!}
                        </div>
                    @endif

                </div>

                <div class="col-sm-6 po">
                    <button class="btn btn-upload btn-flat" @if (!isset($order)) disabled @endif>
                        Select Purchase Order
                    </button>

                    {!! Form::file('po_file', [
                        'accept'    => 'application/pdf, image/*',
                        'id'        => 'po_file',
                        'class'     => 'upload-file hidden'])
                    !!}
                </div>

                <div class="col-sm-6">
                    {!! HTML::image('img/placeholder-image.jpg', 'Purchase Order Image/File', ['class' => 'po_file payment-image']) !!}
                </div>
            </div>

            <div class="row invoice-details">
                <div class="col-sm-6">
                    {!! Form::label('po_number', 'Purchase Order Number :') !!}
                </div>

                <div class="col-sm-6 po">
                    {!! Form::input('text', 'po_number', '', [
                        'placeholder' => 'Purchase Order Number',
                    ]) !!}
                </div>
            </div>

            <div class="row">
                <hr>
            </div>

            <div class="row">
                <div class="col-sm-6 po">
                    <button class="btn btn-upload btn-flat" @if (!isset($order)) disabled @endif>
                        Select Check
                    </button>

                    {!! Form::file('check_file', [
                        'accept'   => 'application/pdf, image/*',
                        'id'       => 'check_file',
                        'class'    => 'upload-file hidden'])
                    !!}


                </div>

                <div class="col-sm-6">
                    {!! HTML::image('img/placeholder-image.jpg', 'Check Image/File', ['class' => 'check_file payment-image']) !!}
                </div>
            </div>

            <div class="row invoice-details">
                <div class="col-sm-6">
                    {!! Form::label('check_number', 'Check Number :') !!}
                </div>

                <div class="col-sm-6 po">
                    {!! Form::input('text', 'check_number', '', [
                        'placeholder' => 'Check Number',
                    ]) !!}
                </div>
            </div>

            <div class="row invoice-details">
                <div class="col-sm-12">
                    {!! Form::label('other_recipients', 'Other Email Recipients :') !!}
                    {!! Form::input('text', 'other_recipients', '', [
                        'class' => 'ei btn-flat',
                        'placeholder' => 'Other Emails Recipients'
                    ]) !!}
                </div>
            </div>

        </div>
    </div>

    @include('main.checkout.shipping_billing')


    <div class="cart-navigation row">
        <div class="col-md-6 column">
            <a href="" class="btn btn-primary pull-left">
                Cancel
            </a>
        </div>
        <div class="col-md-6 column">
            <button type="submit"
                    class="btn btn-warning pull-right btn-pay"
                    @if (!isset($order)) disabled @endif>
                Place My Order
            </button>
        </div>
    </div>

    {!! Form::close() !!}
</div>
