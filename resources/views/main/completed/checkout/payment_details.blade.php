<div class="col-md-5 checkout-wrapper">
        <div class="payment-details order-modification-payment-details panel">
            <div class="panel-inner panel-heading">
                <h2> Payment Summary </h2>
            </div>
            <div class="panel-inner panel-body">
                @if ($items->count())
                    {!! OrderModificationUtility::extractOrderAdjustment($items, $total, true) !!}

                    @if ($balance > 0)
                        <div class="row">
                            <div class="col-xs-12 sub-total-box">
                                <p class="info-box-text">Balance</p>
                                <span class="info-box-number pull-right total-price">${{ number_format($balance, 2) }}</span>
                            </div>
                        </div>
                    @endif
                @endif
            </div>
        </div>
</div>