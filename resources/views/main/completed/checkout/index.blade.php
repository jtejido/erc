@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Checkout </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="cart">
                <div class="inner col-md-12">
                    @include('main.completed.checkout.card_invoice_details')
                    @include('main.completed.checkout.payment_details')
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        $(document).ready(function() {

            var CheckoutShipping = {
                init: function(cont)
                {

                    this.cont = cont;

                    if($('[data-book]').length > 0)
                    {
                        this.startShippingLayout();
                        this.calculateShippingTotal('po_shipping_zip')
                    }
                },
                startShippingLayout: function()
                {

                    if($('.shipping_price_div').length < 1){
                        $('.sub-total-box').parent().before($(CheckoutShipping.template(0.00)));
                    }else{
                        $('[name="shipment_zip"]').val('');
                        this.resetShippingRates();
                    }

                    $('[type="submit"]').attr('disabled',true)

                },
                resetShippingRates: function() {
                    $('.shipping_price_div').find('#shipping-rate').text('$0.00');
                    $('.total-price').text('$'+$('[name="amount"]').val());
                    $('[name="shipping_rates"]').val('');
                },
                template: function(data){
                    var amount = (data < 1)?'$0.00':'$'+data;
                    var ie_bff = ' <div class="row shipping_price_div" style="margin-bottom:7px;">';
                    ie_bff += '<div class="col-md-6">';
                    ie_bff +=  '<span class="text-left">Shipping (UPS Provider):</span></div>';
                    ie_bff +=  '<div class="col-md-6 text-right" id="shipping-rate">'+amount+'</div>';
                    ie_bff += '</div>';
                    return ie_bff;
                },
                calculateShippingTotal: function(clas){

                    var elem = $('#' + this.cont + ' [name="'+clas+'"]');
                    var zip = elem.val();
                    var totalWeight = CheckoutShipping.getWeight();
                    var data = {
                        'order_id': $('[name="order_id"]').val(),
                        'zip': zip,
                        'weight':totalWeight,
                        'name': $('[name="first_name"]').val() + ' ' + $('[name="last_name"]').val()
                    };

                    this.resetShippingRates();

                    elem.off('keyup blur').on('keyup blur', function(e){
                        data.zip =  elem.val();
                        if(data.zip.length >= 5){
                            elem.attr('disabled',true);
                            $('#' + this.cont + '[type="submit"]').attr('disabled',true);
                            CheckoutShipping.fetch(elem,data);
                            return true;
                        }
                    });

                    if(elem.val() > 0){
                        CheckoutShipping.fetch(elem,data);
                        return true;
                    }
                },
                fetch: function(elem,data){

                    $('.payment-details .loading').show();
                    $('html,body').animate({
                        scrollTop: $('.payment-details').offset().top
                    }, 500);

                    $.ajax({
                        url: window.location.origin + '/ups-shipping-rate',
                        type: 'post',
                        data: data,
                        success: function(res){
                            var shipping_rates = res.shipping_rates;
                            var total_price =  parseFloat(res.total,2);
                            elem.removeAttr('disabled')
                            $('.payment-details .loading').hide();
                            $('[type="submit"]').removeAttr('disabled',true)
                            $('#shipping-rate').text( '$'+shipping_rates);
                            $('.sub-total-box').last().find('span').last().text('$' + total_price);
                            $('[name="shipping_rates"]').val(shipping_rates);

                        },
                        error: function(res){
                            CheckoutShipping.startShippingLayout()
                            $('.payment-details .loading').hide();
                            elem.removeAttr('disabled')
                            elem.focus()
                            alert('Invalid zip code')
                        }
                    });
                },
                getWeight: function(){
                    var w = 0;
                    $('[data-weight]').each(function(k,v){
                        w += parseFloat($(this).data('weight'));
                    });
                    return w;
                },
                addZeroes:function( num ) {
                    // Cast as number
                    var num = Number(num);
                    // If not a number, return 0
                    if (isNaN) {
                        return 0;
                    }
                    // If there is no decimal, or the decimal is less than 2 digits, toFixed
                    if (String(num).split(".").length < 2 || String(num).split(".")[1].length<=2 ){
                        num = num.toFixed(2);
                    }
                    // Return the number
                    return num;
                }
            };

            CheckoutShipping.init('card-payment-form');

            $('input[name="cvv"]').NumericOnly({
                length: 4
            })
            $('input[name="shipment_zip"]').NumericOnly({
                length: 5
            });

            $('div.panel-option a.edit').click(function(event) {
                event.stopImmediatePropagation();
            });

            $('[name="payment-option"]').change(function(e){
                if($(this).val() == 'invoice') {
                    CheckoutShipping.init('invoice-payment-form');
                } else {
                    CheckoutShipping.init('card-payment-form');
                }

                e.preventDefault();
                e.stopImmediatePropagation();
            });

            $('div.panel-option').click(function (e) {
                var $that = $(this);
                if($that.hasClass('panel-selected'))
                    return false;
                $that.siblings().removeClass('panel-selected');
                $that.addClass('panel-selected');
                if($that.hasClass('panel-form-button')) {
                    $that.prev().find('i.fa').css('visibility', 'hidden');
                    $that.next().slideDown();
                    $that.next().find("input[type='text']").attr('required','required');
                    $that.next().find("input[type='number']").attr('required','required');
                    $that.find('i.fa').removeClass('fa-plus-circle');
                    $that.find('i.fa').addClass('fa-check-circle');
                    $that.find('i.fa').css('color','green');
                    $that.next().find('input.custom_flag').val(1);
                    if($that.parent().hasClass('shipment_container') && $('[data-book]').length > 0) {
                        CheckoutShipping.calculateShippingTotal('shipment_zip')
                    }
                } else {
                    $that.find('i.fa').css('visibility', 'visible');
                    $that.next().next().hide();
                    $that.next().next().find('input.custom_flag').val(0);
                    $that.next().next().find("input[type='text']").removeAttr('required');
                    $that.next().next().find("input[type='number']").removeAttr('required');
                    $that.next().find('i.fa').removeClass('fa-check-circle');
                    $that.next().find('i.fa').addClass('fa-plus-circle');
                    $that.next().find('i.fa').css('color','inherit');
                    if($that.parent().hasClass('shipment_container') && $('[data-book]').length > 0) {
                        CheckoutShipping.calculateShippingTotal('po_shipping_zip')
                    }
                }
            });

        });

    </script>
@endsection