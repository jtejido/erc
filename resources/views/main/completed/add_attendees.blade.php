@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Course Attendees</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    @if(isset($class))
                        <p class="lead text-center">
                            You are registering for the class<br/></p>
                        <h3 class="text-center">{{ $class->title }}</h3><br/>
                        <p class="lead text-center">
                            @if($class->course->isSeminar())
                            at <span>{{ $class->completeLocation }}</span><br/>
                            @endif
                            on {{ $class->getDateTimeRange() }}. <br/>
                        </p>
                    @elseif(isset($course))<p class="lead text-center">
                        You are registering for the class <strong>{{ $course->title }}</strong>
                    </p>

                    @endif
                </div>

            </div>
        </div>

    </section>

    {{-- */ $noContact = 0; $registrable = $class ?: $course; $classId = $class ? $class->id : 0;  /* --}}

    <section class="main-container container-fluid course-attendees-container">

        <section class="attendees-list">

            <div class="alert alert-warning alert-attendee" role="alert">
                Toggle the checkbox beside the name of the attendee then click <strong>Register</strong>
            </div>

            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @include('errors.form_errors')

            @if ($agent)

                {{-- */

                    $isContactRegistered = OrderUtility::isContactRegistered($agent->contact->id, $registrable);

                /* --}}

                <div class="panel {{ ($isContactRegistered) ? 'panel-default' : 'panel-primary' }}">
                    <div class="panel-heading">
                        <label>
                            @if(!$isContactRegistered)
                            <input type="checkbox" name="contact[]" value="{{ $agent->contact->id }}"
                                   class="contact">
                            @endif
                            {{ $agent->contact->first_name }} {{ $agent->contact->last_name }}
                        </label>

                        @if (!$agent->contact->user and !$agent->contact->note)

                            <a href="javascript:void(0)" class="edit-contact pull-right"
                               data-course_id="{{ $course->id }}"
                               data-class_id="{{ $classId }}"
                               data-id="{{ $agent->contact->id }}"
                               data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                            </a>

                        @endif
                    </div>
                    <div class="panel-body">
                        <div class="info">
                            <label for="email">Email: </label>
                            <p>{{ $agent->email }}</p>
                        </div>

                        @if (!$agent->contact->user and !$agent->contact->note)
                            <div class="info">
                                <label for="address">Address: </label>
                                @if (strlen($agent->contact->address1) &&
                                     strlen($agent->contact->city) &&
                                     strlen($agent->contact->state) &&
                                     strlen($agent->contact->zip))
                                    <p>{{ $agent->contact->address1 }} {{ $agent->contact->address2 }}</p>
                                    <p class="block">{{ $agent->contact->city }}, {{ $agent->contact->state }} {{ $agent->contact->zip }}</p>
                                @endif
                            </div>
                        @endif

                        @if ($agent->contact->note)
                            <div class="info">
                                <label for="note">Note: </label>
                                <p>{{ $agent->contact->note }}</p>
                            </div>
                        @endif

                    </div>
                </div>
            @endif

            @if ($contacts->count() > 0)
                @foreach ($contacts as $contact)

                    {{-- */

                            $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $registrable);

                        /* --}}

                    <div class="panel {{ ($isContactRegistered) ? 'panel-default' : 'panel-primary' }}">
                        <div class="panel-heading">
                            <label>
                                @if(!$isContactRegistered)
                                <input type="checkbox" name="contact[]" value="{{ $contact->id }}"
                                       class="contact">
                                @endif
                                {{ $contact->first_name }} {{ $contact->last_name }}
                            </label>

                            @if (!$contact->user and !$contact->note)

                                <a href="javascript:void(0)" class="edit-contact pull-right"
                                   data-course_id="{{ $course->id }}"
                                   data-class_id="{{ $classId }}"
                                   data-id="{{ $contact->id }}"
                                   data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                                </a>

                            @endif
                        </div>
                        <div class="panel-body">
                            <div class="info">
                                <label for="email">Email: </label>
                                <p>{{ $contact->user ? $contact->user->email : 'No email' }}</p>
                            </div>

                            @if (!$contact->user and !$contact->note)
                                <div class="info">
                                    <label for="address">Address: </label>
                                    @if (strlen($contact->address1) &&
                                         strlen($contact->city) &&
                                         strlen($contact->state) &&
                                         strlen($contact->zip))
                                        <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                        <p class="block">{{ $contact->city }}, {{ $contact->state }} {{ $contact->zip }}</p>
                                    @endif
                                </div>
                            @endif

                            @if ($contact->note)
                                <div class="info">
                                    <label for="note">Note: </label>
                                    <p>{{ $contact->note }}</p>
                                </div>
                            @endif

                        </div>
                    </div>
                @endforeach
            @endif

            <div class="text-center">
                <button class="btn btn-primary" id="add-contact"
                        data-course_id="{{ $course->id }}"
                        data-class_id="{{ $classId }}"
                        data-user_id="{{ $user->id }}"
                        data-url="{{ route('contact.form.get') }}">
                    <i class="fa fa-user-plus"></i>&nbsp;New Contact
                </button>
            </div>


            <div class="transaction-buttons row text-center">

                    <a href="{{ route('order.info', $order->id) }}" class="btn btn-default btn-lg">
                        Go Back
                    </a>

                    <button class="btn btn-info btn-lg" id="register"
                            data-course_id="{{ $course->id }}"
                            data-class_id="{{ $classId }}"
                            data-user_id="{{ $user->id }}"
                            data-url="{{ route('order.attendees.add.post') }}">
                        Register
                    </button>
                <input type="hidden" id="item-id" value="{{ $orderItem->id }}">
                <input type="hidden" id="is-swap" value="0">
            </div>

            <!-- Modal for adding / editing contact -->

            <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title" id="classModalLabel">Save Contact</h5>
                        </div>
                        <div class="modal-body">
                            <i>Loading...</i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

