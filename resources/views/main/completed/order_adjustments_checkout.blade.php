@foreach ($orderAdjustments as $orderAdjustment)
    <h3 class="panel-title item-title adjustment-date">
        Date Created: {{ $orderAdjustment->created_at->toFormattedDateString() }}
    </h3>

    <div class="row summary-info adjustment-summary">
        <div class="col-xs-12 col-md-11 attendees-info">
            <p class="details-label">Details :</p>
            <ul class="list">
                <li>
                    <p>
                        @if ($orderAdjustment->adjustment_action == \App\Utilities\Constant::ADDED)
                            <span class="price label label-success action-label">
                                {{ $orderAdjustment->adjustment_action }}
                            </span>
                        @else
                            <span class="price label label-danger action-label">
                                {{ $orderAdjustment->adjustment_action }}
                            </span>
                        @endif
                    </p>
                </li>
                <li><span class="note">{{ $orderAdjustment->note }}</span></li>
            </ul>

            <div class="total">
                <span class="amount-text">Amount : &nbsp;</span>
                <span class="total-amount pull-right">
                    ${{ number_format($orderAdjustment->amount, 2) }}
                </span>
            </div>
        </div>
    </div>

    <hr>
@endforeach