@if (Auth::user()->isMember())

    <div class="item panel">
            <div class="panel-inner panel-heading">
                <h2 >Current Order Adjustments Summary</h2>
            </div>
            <div class="panel-inner panel-body">

                <div class="attendees adjustments">

                    @if ($subTotal > 0)
                        <p class="required-text text-center">
                            Please pay the balance due for it to reflect in order total.
                        </p>
                    @endif

                    <ul class="list">
                        @foreach ($orderAdjustments as $orderAdjustment)
                            <li>
                                <input type="hidden" class="order_adjustment_ids" value="{{ $orderAdjustment->id }}">
                                <div class="col-sm-8">
                                    <strong>
                                        {{ $orderAdjustment->created_at->toFormattedDateString() }}
                                    </strong>
                                    <div class="order-adjustment-note">
                                        {{ $orderAdjustment->note }}
                                    </div>
                                </div>

                                <div class="col-sm-4 no-padding">
                                    <div class="order-adjustment-price">
                                    @if ($orderAdjustment->adjustment_action == \App\Utilities\Constant::ADDED)
                                        <small class="adjustment-label text-center label label-success">
                                            {{ $orderAdjustment->adjustment_action }}
                                        </small>
                                    @else
                                        <small class="adjustment-label text-center label label-danger">
                                            {{ $orderAdjustment->adjustment_action }}
                                        </small>
                                    @endif


                                    <span class="price pull-right">
                                        ${{ number_format($orderAdjustment->amount, 2) }}
                                    </span>
                                    </div>
                                </div>
                            </li>

                        @endforeach
                    </ul>
                </div>

                <div class="adjustments-breakdown">
                    <div class="order-modification-amount">
                        <b class="col-xs-8">Paid Total: </b>
                        <strong class="col-xs-4">${{ number_format($total, 2) }}</strong>
                    </div>


                    @if ($added > 0)
                        <div class="order-modification-amount">
                            <b class="col-xs-8">Additional Fees: </b>
                            <strong class="col-xs-4">${{ number_format($added, 2) }}</strong>
                        </div>
                    @endif

                    @if ($deducted > 0)
                        <div class="order-modification-amount">
                            <b class="col-xs-8">Refunds: </b>
                            <strong class="col-xs-4">${{ number_format($deducted, 2) }}</strong>
                        </div>
                    @endif

                    <div class="order-modification-amount">
                        <b class="col-xs-8">Amount Due: </b>
                        <strong class="col-xs-4">${{ number_format($subTotal, 2) }}</strong>
                    </div>

                    @if ($refund > 0)
                        <div class="order-modification-amount">
                            <b class="col-xs-8">Refund Due: </b>
                            <strong class="col-xs-4">${{ number_format($refund, 2) }}</strong>
                        </div>
                    @endif

                    @if ($subTotal > 0)
                        <div class="cart-navigation completed-proceed-checkout">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('order_mod.checkout', $order->id) }}"
                                   class="btn blue-btn checkout">Proceed To Checkout</a>
                            </div>
                        </div>
                    @else
                        <div class="cart-navigation notify-accounting-container col-md-6 col-md-offset-3">
                            <button id="notify-accounting-modification" class="btn blue-btn center-block" data-url="{{ route('order_mod.confirm') }}">
                                Confirm Order Adjustment
                            </button>
                        </div>
                    @endif
                </div>
            </div>

    </div>


@else

    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Current Order Adjustments Summary</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

            @if ($subTotal > 0)
                <p class="required text-center">
                    Please pay the balance due for it to reflect in order total.
                </p>
            @endif

            @foreach ($orderAdjustments as $orderAdjustment)
                <div class="attendees">
                    <div>
                        <input type="hidden" class="order_adjustment_ids" value="{{ $orderAdjustment->id }}">
                        <span class="discount-title col-md-5">
                            {{ $orderAdjustment->note }}

                            <p class="adjustment-date">
                                <span class="price">
                                    Date Created: {{ $orderAdjustment->created_at->toFormattedDateString() }}
                                </span>
                            </p>
                        </span>

                        @if ($orderAdjustment->adjustment_action == \App\Utilities\Constant::ADDED)
                            <span class="price col-md-2 label label-success">
                                {{ $orderAdjustment->adjustment_action }}
                            </span>
                        @else
                            <span class="price col-md-2 label label-danger">
                                {{ $orderAdjustment->adjustment_action }}
                            </span>
                        @endif

                        <span class="price col-md-5">
                            <span>$</span>
                            <input type="text"
                                   class="order-adjustment-amount order-adjustment-{{ $orderAdjustment->id }}"
                                   data-original-price="{{ number_format($orderAdjustment->amount, 2) }}"
                                   value="{{ number_format($orderAdjustment->amount, 2) }}">

                            <button id="order-adjustment-{{ $orderAdjustment->id }}"
                                    data-id="{{ $orderAdjustment->id }}"
                                    class="btn btn-success btn-flat btn-xs change-order-adjustment-price">
                                Change
                            </button>
                            <button data-id="{{ $orderAdjustment->id }}"
                                    class="btn btn-danger btn-flat btn-xs remove-order-adjustment">
                                <i class="fa fa-remove"></i>
                            </button>
                        </span>


                    </div>
                </div>

                <div class="col-md-12">
                    <hr>
                </div>
            @endforeach

                <div class="order-modification-amount">
                    <b class="col-xs-9">Paid Total: </b>
                    <strong class="col-xs-3">${{ number_format($total, 2) }}</strong>
                </div>


                @if ($added > 0)
                    <div class="order-modification-amount">
                        <b class="col-xs-9">Additional Fees: </b>
                        <strong class="col-xs-3">${{ number_format($added, 2) }}</strong>
                    </div>
                @endif

                @if ($deducted > 0)
                    <div class="order-modification-amount">
                        <b class="col-xs-9">Refunds: </b>
                        <strong class="col-xs-3">${{ number_format($deducted, 2) }}</strong>
                    </div>
                @endif

                @if ($sales_tax > 0)
                    <div class="order-modification-amount">
                        <b class="col-xs-9">Sales Tax: </b>
                        <strong class="col-xs-3">${{ number_format($sales_tax, 2) }}</strong>
                    </div>
                @endif

                <div class="order-modification-amount">
                    <b class="col-xs-9">Amount Due: </b>
                    <strong class="col-xs-3">${{ number_format($subTotal, 2) }}</strong>
                </div>

                @if ($refund > 0)
                    <div class="order-modification-amount">
                        <b class="col-xs-9">Refund Due: </b>
                        <strong class="col-xs-3">${{ number_format($refund, 2) }}</strong>
                    </div>
                @endif

                @if ($subTotal > 0)
                    <div class="order-modification-amount col-md-4 col-md-offset-4">
                        <a href="{{ route('admin.order_mod.checkout', $order->id) }}"
                           class="btn btn-flat btn-info checkout">Proceed To Checkout</a>
                    </div>
                @else
                    <div class="order-modification-amount notify-accounting-container col-md-6 col-md-offset-3">
                        <button id="notify-accounting-modification" class="btn btn-flat btn-info center-block" data-url="{{ route('order_mod.confirm') }}">
                            Confirm Order Adjustment
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endif