@if (!Auth::user()->isMember())

    <div class="col-md-12 box-container">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Order Adjustments History</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">

                @foreach ($orderAdjustmentHistories as $orderAdjustmentHistory)
                    <div class="attendees">
                        <div>
                            <span class="discount-title col-md-8">
                                {{ $orderAdjustmentHistory->note }}

                                <p class="adjustment-date">
                                    <span class="price">
                                        Date Created: {{ $orderAdjustmentHistory->created_at->toFormattedDateString() }}
                                    </span>
                                </p>
                            </span>

                            @if ($orderAdjustmentHistory->adjustment_action == \App\Utilities\Constant::ADDED)
                                <span class="price col-md-2 label label-success">
                                {{ $orderAdjustmentHistory->adjustment_action }}
                                </span>
                            @elseif ($orderAdjustmentHistory->adjustment_action == \App\Utilities\Constant::DEDUCTED)
                                <span class="price col-md-2 label label-danger">
                                    {{ $orderAdjustmentHistory->adjustment_action }}
                                </span>
                            @else
                                <span class="price col-md-2 label label-warning">
                                    {{ $orderAdjustmentHistory->adjustment_action }}
                                </span>
                            @endif

                            <span class="price col-md-2">
                                @if ($orderAdjustmentHistory->amount)
                                    <span>${{ number_format($orderAdjustmentHistory->amount, 2) }}</span>
                                @endif
                            </span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <hr>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endif