@extends('main.main_layout')

@section('meta')
    <meta name="title" content="Class Calendar | Environmental Resource Center">
    <meta name="description" content="Here are our training schedule. Call us at 800-537-2372 to get more details.">
@endsection

@section('title')
    Class Calendar | Environmental Resource Center
@endsection

@section('stylesheets')
    @parent
    <style media="screen">
        .fc-agendaWeek-view .fc-day-grid-event > .fc-content {
            white-space: normal !important;
        }
        .fc-widget-content .fc-time-grid-container {
            height: 50px !important;
        }
        .fc-widget-content .fc-time-grid-container table {
            display: none;
        }
    </style>
@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Class Calendar  </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="overflow: visible">
                <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-center" style="padding:10px">
                            <p>
                            <label><i class="fa fa-info-circle"></i> Legend </label>
                                    @foreach($course_types as $ct)
                                    <span class="label" style="color: #FFF; background-color:{{$ct->color}}">{{ $ct->name }}</span>&nbsp;
                                    @endforeach
                            </p>
                        </div>
                    </div>
                </div>

                        <div id='calendar'></div>

            </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek'
                },
                eventRender: function(event, element) {
                    element.find('.fc-title').html(event.title);
                    element.qtip({
                        content: {
                            text: event.description,
                            title: event.title,
                        },
                        position : {
                            adjust : {
                                screen : true,
                                method: 'none shift',
                                y: 10
                            },
                            my: 'top center',
                            at: 'bottom',
                            target: 'mouse'
                        },
                        style: {
                            classes: 'qtip-bootstrap'
                        }

                    });
                },
                events: '/class_calendar/data',
                eventLimit: true, // for all non-agenda views
                weekends: false
            })
        })(jQuery);
    </script>
@endsection