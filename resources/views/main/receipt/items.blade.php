@if ($object == \App\Utilities\Constant::REGISTRATION_ORDER or
     $object == \App\Utilities\Constant::YEARLY_SUBSCRIPTION or
     $object == \App\Utilities\Constant::CORPORATE_SEAT or
     $object == \App\Utilities\Constant::PRODUCT_ORDERS)

    @foreach ($contents as $content)
        <tr style="border: none;">

            <td style="padding: 5px;">
                @if ($object == \App\Utilities\Constant::REGISTRATION_ORDER)
                    {!! \App\Facades\OrderUtility::getPaymentClassTitle($item->id) !!} -
                    <?php
                        $classRegistration = \App\Models\ClassRegistration::find($content->id);
                        echo $classRegistration->contact->name;
                    ?>
                @elseif ($object == \App\Utilities\Constant::YEARLY_SUBSCRIPTION)
                    Yearly Subscription
                @elseif ($object == \App\Utilities\Constant::CORPORATE_SEAT)
                    Corporate Seat
                @elseif ($object == \App\Utilities\Constant::PRODUCT_ORDERS)
                    <?php $product = \App\Models\ProductOrders::find($item->id); ?>
                    <?php echo $product->title(); ?>
                @endif
            </td>


            <td class="text-center" style="padding: 5px;">
                @if ($object == \App\Utilities\Constant::PRODUCT_ORDERS)
                    {{ $content->quantity }}
                @else
                    1
                @endif
            </td>

            <td class="text-center">
                @if ($object == \App\Utilities\Constant::REGISTRATION_ORDER)
                    <?php $registrationOrder = \App\Models\RegistrationOrder::find($item->id); ?>
                    $<?php echo number_format($registrationOrder->originalPrice(), 2); ?>
                @elseif ($object == \App\Utilities\Constant::YEARLY_SUBSCRIPTION)
                    <?php $yearlySub = \App\Models\ClientYearlySubscription::find($content->id) ;?>
                    ${{ number_format($yearlySub->yearlySubscription->type->price, 2) }}
                @elseif ($object == \App\Utilities\Constant::CORPORATE_SEAT)
                    <?php $corporateCredit = \App\Models\ClientCorporateSeatCredit::find($content->id); ?>
                    {{ $corporateCredit->rate }}
                @elseif ($object == \App\Utilities\Constant::PRODUCT_ORDERS)
                    <?php $product_order = \App\Models\ProductOrders::find($item->id); ?>
                    <?php echo $product_order->product_rate; ?>
                @endif
            </td>

            <td class="text-center" style="padding: 5px;">
                ${{ number_format($content->price_paid, 2) }}
            </td>
        </tr>
    @endforeach
@endif

@if ($object == \App\Utilities\Constant::ORDER_ADJUSTMENT_OBJECT)

    @foreach ($contents as $content)
        <tr>
            <td style="padding: 5px;">
                Order Adjustment - {{ $content->description or '' }}
            </td>

            <td class="text-center" style="padding: 5px;">
                1
            </td>

            <td class="text-center">
                ${{ number_format($content->price_paid, 2) }}
            </td>

            <td class="text-center" style="padding: 5px;">
                ${{ number_format($content->price_paid, 2) }}
            </td>
        </tr>
    @endforeach
@endif

{{-- Check Discounts --}}

@foreach ($contents as $content)
    @if ($object == \App\Utilities\Constant::REGISTRATION_ORDER)
        <?php $registrationOrder = \App\Models\RegistrationOrder::find($item->id); ?>
        <?php $classRegistration = \App\Models\ClassRegistration::find($content->id); ?>

        @if ($classRegistration->subscriptionDiscount)
            <?php
               $subscriptionDiscount = $classRegistration->subscriptionDiscount;
            ?>

            @if ($subscriptionDiscount->deduction > 0)
                <tr style="border: none;">
                    <td style="padding: 5px;">
                        Annual Subscription Discount - {{ $classRegistration->contact->name }}
                    </td>


                    <td class="text-center" style="padding: 5px;">

                    </td>

                    <td class="text-center">
                        - ${{ number_format($subscriptionDiscount->deduction, 2) }}
                    </td>

                    <td class="text-center" style="padding: 5px;">
                        - ${{ number_format($subscriptionDiscount->deduction, 2) }}
                    </td>
                </tr>
            @endif
        @endif

        @if ($classRegistration->classMilitaryDiscount)
            <?php
                $cmDiscount = $classRegistration->classMilitaryDiscount;
            ?>

            <tr style="border: none;">
                <td style="padding: 5px;">
                    GSA Pricing Discount - {{ $classRegistration->contact->name }}
                </td>


                <td class="text-center" style="padding: 5px;">

                </td>

                <td class="text-center">
                    - ${{ number_format($cmDiscount->deduction, 2) }}
                </td>

                <td class="text-center" style="padding: 5px;">
                    - ${{ number_format($cmDiscount->deduction, 2) }}
                </td>
            </tr>
        @endif

        @if ($classRegistration->is_half_priced and !$classRegistration->is_cancelled and $classRegistration->classHalfPriceDiscount)
            <?php $chDiscount = $classRegistration->classHalfPriceDiscount; ?>

            @if ($chDiscount->deduction > 0)
                <tr style="border: none;">
                    <td style="padding: 5px;">
                        1/2 Price Discount {{ $classRegistration->contact->name }}
                    </td>


                    <td class="text-center" style="padding: 5px;">

                    </td>

                    <td class="text-center">
                        - ${{ number_format($chDiscount->deduction, 2) }}
                    </td>

                    <td class="text-center" style="padding: 5px;">
                        - ${{ number_format($chDiscount->deduction, 2) }}
                    </td>
                </tr>
            @endif
        @endif

        <?php
            $classCombinationDiscount = \App\Models\ClassCombinationDiscount::where('class_reg_one', $content->id)
                    ->orWhere('class_reg_two', $content->id)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->first();
        ?>

        @if (!is_null($classCombinationDiscount))
            <?php
                $amount = 100;
            ?>
            <tr style="border: none;">
                <td style="padding: 5px;">
                    HWM-DOT Discount - {{ $classRegistration->contact->name }}
                </td>


                <td class="text-center" style="padding: 5px;">
                </td>

                <td class="text-center">
                    - ${{ number_format($amount, 2) }}
                </td>

                <td class="text-center" style="padding: 5px;">
                    - ${{ number_format($amount, 2) }}
                </td>
            </tr>
        @endif

        @if ($classRegistration->classCouponDiscounts->count())
            @foreach($classRegistration->classCouponDiscounts as $classCouponDiscount)
                <tr style="border: none;">
                    <td style="padding: 5px;">
                        Coupon Discount - {{ $classRegistration->contact->name }}
                    </td>


                    <td class="text-center" style="padding: 5px;">

                    </td>

                    <td class="text-center">
                        - ${{ number_format($classCouponDiscount->deduction, 2) }}
                    </td>

                    <td class="text-center" style="padding: 5px;">
                        - ${{ number_format($classCouponDiscount->deduction, 2) }}
                    </td>
                </tr>
            @endforeach
        @endif

        @if ($classRegistration->classCreditDiscount and !$classRegistration->classCreditDiscount->is_deleted)
            <?php $creditDiscount = $classRegistration->classCreditDiscount; ?>

            <tr style="border: none;">
                <td style="padding: 5px;">
                    Corporate Seat Discount - {{ $classRegistration->contact->name }}
                </td>


                <td class="text-center" style="padding: 5px;">

                </td>

                <td class="text-center">
                    - ${{ number_format($creditDiscount->deduction, 2) }}
                </td>

                <td class="text-center" style="padding: 5px;">
                    - ${{ number_format($creditDiscount->deduction, 2) }}
                </td>
            </tr>
        @endif

    @endif
@endforeach

