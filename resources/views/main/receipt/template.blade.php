<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- 3rd Party CSS -->
    <link href="{{ public_path('/css/print.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Playfair+Display+SC" rel="stylesheet">
    <style>
        .container {
            height: 100%;
        }
        table {
            border-collapse: collapse;
            border: 2px solid #b3b1b1 !important;
        }

        .text-center {
            text-align: center;
        }
        h1.title {
            margin-top: 25px;
            margin-bottom: 25px;
            font-family: 'Playfair Display SC', serif;
            font-size: 4em;
        }
        .tbl-container {
            position: relative;
            bottom: 15px;
        }
        .tbl-container-img {
            position: relative;
            bottom: 45px;
        }
        .tbl-img {
            position: relative;
            bottom: 80px;
        }
        .tbl-ceu {
            position: relative;
            top: 27px;
        }
        .td-name {
            border-top: 1px solid black;
            text-align: center;
            width: 300px;
        }
        .td-sig {
            font-family: 'Dancing Script', cursive;
            text-align: center;
            font-size: 1.2em;
        }
        .td-img {
            position: relative;
            top: 40px;
            width: 100%;
        }
        .td-cert {
            position: relative;
            bottom: 110px;
        }
        /*.td-cert {*/
        /*text-align: right;*/
        /*padding-top: 20px;*/
        /*}*/
        html, body {
            height: 203mm;
            width: 297mm;
            font-size: 1.1em;
        }
        h2.course {
            font-weight: bold;
        }
        span.name {
            font-weight: bold;
            /*text-decoration: underline;*/
        }

    </style>

</head>

<body>
<div class="container">
    <header style="overflow: auto;">
        <div class="logo" style="width: 74%; float: left;">
            <img src="{{ url('img/receipt_logo.png') }}" alt="Logo" style="width: 100%;">
        </div>
        <div class="date" style="width: 20%; float: right; margin-top: 50px;">
            <table class="table" border="2" style="width: 100%;">
                <tbody>
                    <tr>
                        <td class="text-center" style="padding: 10px;">Date</td>
                        <td class="text-center" style="padding: 10px;">{{ $paymentTransaction->created_at->format('m/d/Y') }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </header>

    <div class="shipping" style="margin-top: 100px; overflow: auto">
        <div class="sold-to" style="width: 48%; float: left;">
            <table class="table" border="2" style="width: 100%;">
                <tbody>
                    <tr>
                        <td style="padding: 10px;">Sold To</td>
                    </tr>
                    <tr>
                        <td style="padding: 10px;">
                            {!! $paymentTransaction->complete_billing_address !!}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="ship-to" style="width: 48%; float: right;">
            <table class="table" border="2" style="width: 100%;">
                <tbody>
                <tr>
                    <td style="padding: 10px;">Ship To</td>
                </tr>
                <tr>
                    <td style="padding: 10px;">
                        {!! $paymentTransaction->complete_shipping_address !!}
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="po" style="margin-top: 20px; overflow: auto;">
        <table class="table" border="2" style="width: 60%; float: right;">
            <tbody>
            <tr>
                <td class="text-center" style="padding: 10px;">Purchase Order #</td>
                <td class="text-center" style="padding: 10px;">Ordered By</td>
            </tr>
            <tr>
                <td class="text-center" style="padding: 10px;"></td>
                <td class="text-center" style="padding: 10px;">{{ $user->contact->name }}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="order-summary" style="margin-top: 20px;">
        <table class="table" border="2" style="width: 100%;">
            <tbody>
                <tr>
                    <td class="text-center" style="padding: 10px; background-color: #ccc;">Description</td>
                    <td class="text-center" style="padding: 10px; background-color: #ccc;">Qty</td>
                    <td class="text-center" style="padding: 10px; background-color: #ccc;">Rate</td>
                    <td class="text-center" style="padding: 10px; background-color: #ccc;">Amount</td>
                </tr>


                @if ($paymentTransaction->items)
                    @foreach(json_decode($paymentTransaction->items)->items as $item)
                        {!! OrderUtility::extractItemsFromPaymentReceipt($paymentTransaction, $user, $item) !!}
                    @endforeach

                    <tr>
                        <td rowspan="5" style="padding-top: 60px;">
                            We are incorporated; EIN# 52-1215051 <br/>
                            Contact Christie Brooks with questions regarding this document (919-469-1585 x 270 or <br/> cbrooks@ercweb.com).</td>
                        <td class="text-center">Shipping</td>
                        <td class="text-center" colspan="2">{{ $paymentTransaction->shipping_fee }}</td>
                    </tr>

                    <tr>
                        <td class="text-center">Sales Tax</td>
                        <td class="text-center" colspan="2">{{ $paymentTransaction->state_tax_formatted }}</td>
                    </tr>

                    <tr>
                        <td class="text-center">Payment Via</td>
                        <td class="text-center" colspan="2">{{ $paymentTransaction->card_brand }}</td>
                    </tr>

                    <tr>
                        @if($paymentTransaction->isCheck())
                        <td class="text-center">Check #</td>
                        @else
                        <td class="text-center">Credit Card #</td>
                        @endif
                        <td class="text-center" colspan="2">{{ $paymentTransaction->masked_cc }}</td>
                    </tr>

                    <tr>
                        <td class="text-center" style="padding-top: 30px;">Total Paid</td>
                        <td class="text-center" style="font-weight: 700; padding-top: 30px;" colspan="2">${{ number_format($paymentTransaction->amount, 2) }}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
</body>
</html>