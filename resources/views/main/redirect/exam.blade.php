@extends('main.main_layout')

@section('stylesheets')
    @parent
    <style media="screen">
        ul.student-list {
            margin: 0 30px;
        }
        ul.student-list li {

        }
        ul.student-list li label {
            font-size: 1.2em;
        }
        ul.student-list li input[type=checkbox] {
            margin-left: -25px;
        }
        strong { font-weight: bold; }
        small { font-size: 0.7em; }
        div.disabled { color: #aaa; }
    </style>
@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> {{ $page_title }} </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
                    <div class="box">
                        <input type="hidden" name="regOrderId" id="regOrderId" value="{{ $regOrderId }}" />
                        @include('admin.includes.success_message')
                        @include('admin.includes.form_errors')
                        <div class="box-header">
                            <p class="lead">
                                Hi There! <br/><br/>You are now ready to take the test for the webcast entitled <strong>{{ $webinar->title }}.</strong>
                            </p>
                        </div>

                        @if($webinar->started)

                        <div class="box-body">
                            <div id="box-list">
                                <div class="well">
                                    <strong>Pre-requisite:</strong> Watch webcast. In case you missed it, please contact customer support.<br/><br/>
                                    Choose your name below and click <strong>"Take Test Now"</strong> to begin.
                                </div>
                                <ul class="student-list">
                                    @foreach($students as $student)
                                        <li>
                                            <div class="checkbox {{ ($student->exam_flag) ? 'disabled' : '' }}">
                                                <label>
                                                    @if($student->exam_flag)
                                                        <input class='student-select'
                                                               name="student"
                                                               type="radio"
                                                               disabled="disabled"
                                                               value="{{ $student->id }}">
                                                    @else
                                                        <input class='student-select'
                                                               name="student"
                                                               type="radio"
                                                               value="{{ $student->id }}">
                                                    @endif
                                                    {{ $student->name }}
                                                </label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>


                                <div class="text-center">
                                    <button class="btn btn-info" id="join-meeting">
                                        <i class="fa fa-arrow-circle-right"></i>&nbsp;Take Test Now
                                    </button>
                                </div>

                            </div>

                            <div id="box-link" style="display:none">
                                <div class="text-center">
                                    <div class="lead">You will be redirected to your exam. Please make sure that pop-ups are enabled for this website.</div>
                                    <br/>
                                    <a class="btn btn-lg btn-warning"
                                       href="#"
                                       onclick="javascript:location.reload()">
                                        <i class="fa fa-refresh"></i>&nbsp;Re-open Test</a>
                                </div>
                            </div>

                        </div>
                        @else
                                <div class="box-body alert alert-warning text-center">
                                    The exam for this webinar will be available <strong>{{ $webinar->start->diffForHumans() }}</strong>
                                </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.alert_modal')
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            var clipboard = new Clipboard('.copy-btn');
            $('#copy-btn').qtip({
                hide: 'unfocus',
                style: {
                    classes: 'qtip-bootstrap'
                }
            });
            clipboard.on('success', function(e) {
                $('#copy-btn').attr('title', 'Copied!');
            });

            $('#join-meeting').on('click', function(e) {

                e.preventDefault;

                var $input = $("input[type=radio]:checked:enabled.student-select"),
                        checked = $input.length,
                        regOrderId = $('#regOrderId').val(),
                        students = [];

                if (!checked) {
                    Modal.showAlertModal('You must select a user to begin test.', 'Invalid Input');
                    return false;
                }

                var student = $input.val();

                console.log(student);

                $.ajax({
                    type: 'POST',
                    data: { student:student, regOrderId:regOrderId },
                    success: function (response) {
                        if (response.updated) {
                            $('#box-list').hide("slow", function () {
                                $('#box-link').show(2000);
                                window.open(
                                        response.link,
                                        '_blank'
                                );
                                return false;
                            });
                        } else {
                            Modal.showAlertModal(response.message);
                        }
                        return false;
                    }
                });

            });
        })(jQuery);
    </script>
@endsection
