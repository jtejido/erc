@extends('main.main_layout')

@section('stylesheets')
    @parent
    <style media="screen">
        ul.student-list {
            margin: 0 30px;
        }
        ul.student-list li {

        }
        ul.student-list li label {
            font-size: 1.2em;
        }
        ul.student-list li input[type=checkbox] {
            margin-left: -25px;
        }
        strong { font-weight: bold; }
        small { font-size: 0.7em; }
    </style>
@endsection

@section('main_content')
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="page-title">
                <div class="border"></div>
                <div class="inner">
                    <h1> {{ $page_title }} </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-lg-offset-3 col-md-offset-3">
        <div class="box">
            @include('admin.includes.success_message')
            @include('admin.includes.form_errors')
            <div class="box-header">
                <p class="lead">
                    Hi There! <br/><br/>Welcome to the webcast entitled <strong>{{ $webinar->title }}</strong>.
                    This webcast will be available from <code>{{ $webinar->date_range }}</code>.
                </p>
                <em class="lead">
                    <em></em><strong>Course Material:</strong>  From your User Profile, click on Course Details to print or download your course materials.</em>
                </p>
            </div>

            @if($webinar->isOngoingOnDay)
                <input type="hidden" name="regOrderId" id="regOrderId" value="{{ $regOrderId }}" />
                <div class="box-body">
                <div id="box-list">
                    <div class="well">To start, choose your name below and click Join Now!</div>
                    <ul class="student-list">
                        @foreach($students as $student)
                        <li>
                            <div class="checkbox {{ ($student->meeting_flag) ? 'disabled' : '' }}">
                                <label>
                                    <input class='student-select'
                                           name="student[]"
                                           type="checkbox"
                                           value="{{ $student->id }}">
                                    {{ $student->name }}
                                    @if($student->meeting_flag)
                                        <small><em>already logged in</em></small>
                                    @endif
                                </label>
                            </div>
                        </li>
                        @endforeach
                    </ul>


                    <div class="text-center">
                        <button class="btn btn-info" id="join-meeting">
                            <i class="fa fa-arrow-circle-right"></i>&nbsp;Join Now
                        </button>
                    </div>

                </div>

                <div id="box-link" style="display:none">
                    <div class="text-center">
                        <div class="input-group">
                            <input type="text"
                                   id="meetingId"
                                   style="height: 60px; font-size: 2em"
                                   class="form-control" value="{{ $meeting_id }}" />

                            <div class="input-group-addon">
                                <button class="copy-btn btn btn-default"
                                        id="copy-btn"
                                        title="Copy"
                                        data-clipboard-target="#meetingId"><i class="fa fa-copy"></i>
                                </button>
                            </div>

                        </div>
                        <br/>
                        <a class="btn btn-lg btn-warning"
                           target="_blank"
                           href="https://signin.webex.com/collabs/#/meetings/joinbynumber?TrackID=&hbxref=https%3A%2F%2Fwww.webex.com%2F&goid=attend-meeting">
                            <i class="fa fa-external-link"></i>&nbsp;Join Meeting</a>
                    </div>
                </div>

            </div>
            @else
                @if (!$webinar->active)
                    <div class="box-body alert alert-warning text-center">
                        This webinar is no longer active.
                    </div>
                @else
                    <div class="box-body alert alert-warning text-center">
                        The webinar will be available <strong>{{ $webinar->start_date->diffForHumans() }}</strong>
                    </div>
                @endif

            @endif

        </div>
            </div>
        </div>
    </div>
</section>
@include('includes.alert_modal')
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            var clipboard = new Clipboard('.copy-btn');
            $('#copy-btn').qtip({
                hide: 'unfocus',
                style: {
                    classes: 'qtip-bootstrap'
                }
            });
            clipboard.on('success', function(e) {
                $('#copy-btn').attr('title', 'Copied!');
            });

            $('#join-meeting').on('click', function(e) {

                e.preventDefault;

                var $input = $("input[type=checkbox]:checked:enabled.student-select"),
                        checked = $input.length,
                        regOrderId = $('#regOrderId').val(),
                        students = [];

                if (!checked) {
                    Modal.showAlertModal('You must choose at least 1 user', 'Invalid Input');
                    return false;
                }

                $input.each(function(index) {
                    students.push($(this).val());
                });

                $.ajax({
                    type: 'POST',
                    data: { students:students, regOrderId:regOrderId },
                    success: function (response) {
                        if (response.updated) {
                            $('#box-list').hide("slow", function () {
                                $('#box-link').show(2000);
                                window.open(
                                        "{{ $meeting_link }}",
                                        '_blank'
                                );
                                return false;
                            });
                        }
                        return false;
                    }
                });

            });
        })(jQuery);
    </script>
@endsection
