@extends('main.main_layout')

@section('meta')
    <meta name="title" content="">
    <meta name="description" content="More and more people finds satisfactions on our products. What are you waiting for? Give us a call at 800-537-2372 to know more. Or click here to get started.">
@endsection

@section('title')
    Our Products | Environmental Resource Center
@endsection

@section('main_content')
    <section class="book-page">
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Products  </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="box-header">
                <div class="actions">
                    @include('admin.includes.success_message')

                    {!! Form::open(['method' => 'GET', 'class' => 'status-filter form-inline']) !!}
                    <div class="form-group">
                        {!! Form::select('sortby', $sort , $sortby, [
                            'id'    => 'status',
                            'class' => 'form-control sortby',
                        ]) !!}
                        {!! Form::text('keyword', $keyword, [
                                    'class' => 'form-control',
                                    'placeholder'   => 'Search'
                                    ]) !!}
                        <button type="submit" title="Search" class="btn btn-sm btn-info"><i class="fa fa-icon fa-search"></i></button>
                        <button type="button" title="Reset" id="resetFilter" class="btn btn-sm btn-default"><i class="fa fa-icon fa-refresh"></i></button>
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>

            <div class="box-body">

                    <div class="row no-gutter">
                        @foreach($books as $book)
                            @include('main.books._book')
                        @endforeach
                    </div>


                @if (!$books->appends(['product_category' => $category])->count())
                    <i>No books Available.</i>
                @endif

            </div>
            <div class="text-center">{!! $books->appends(['sortby' => $sortby, 'keyword' => $keyword])->render() !!}</div>
        </div>
    </section>
@stop

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            $("select#status").change(function() {
                $(this).closest('form').submit();
            });
            $('#resetFilter').on('click', function() {
                var $form = $(this).closest('form');
                $form.find("input[name='keyword']").val('');
                $form.find("select[name='sortby']").val('0');
                $form.submit();
            });
        })(jQuery);
    </script>
@endsection