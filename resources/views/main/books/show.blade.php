@extends('main.main_layout')

@section('title')
    {{ $book->name }} | Environmental Resource Center
@endsection

@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
            margin: 10px 0;
        }
        .qty {
            width: 55px !important;
            display: inline;
        }
    </style>
@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> View Book  </h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container book-container">
            <div>
                <div class="box-header">
                    <div class="actions">
                        @include('admin.includes.success_message')
                        @include('admin.includes.form_errors')
                    </div>
                </div>

                <div class="box-body">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="thumbnail btn-flat">
                                @if($book->thumbnail)
                                    <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
                                @else
                                    <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                                @endif

                            </div>
                        </div>
                        <div class="col-md-7 book-details">
                            <h2>{{ $book->name }}</h2>
                            <h3 class="price">{{ $book->getPrice() }}</h3>
                            <strong>SKU:</strong>&nbsp;<code>{{ $book->code }}</code>&nbsp;&nbsp;&nbsp;
                            <strong>Weight:</strong>&nbsp;<code>{{ $book->getWeight() }}</code>
                            <hr/>
                            <p style="line-height: 25px">{!! $book->description  !!}</p>
                            <hr/>
                            {!! Form::open([
                                'method'    => 'POST',
                                'url'       => route('products.cart.add'),
                                'class'     => 'form-inline']) !!}
                                {!! Form::hidden('book_id', $book->id) !!}
                            <div class="form-group">
                                {!! Form::label('quantity', 'Qty') !!}
                                {!! Form::number('quantity', 1, [
                                    'min'       => '1',
                                    'class'     => 'form-control qty',
                                    'required'  => 'required'])  !!}
                                <button  class="btn btn-sm btn-success btn-flat"><i class="fa fa-cart-plus"></i>&nbsp;Add to Cart</button>&nbsp;
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@stop

@section('javascripts')
    @parent
@endsection