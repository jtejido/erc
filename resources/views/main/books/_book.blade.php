<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="thumbnail btn-flat">
        <a href="{{ route($products_show, [$book->slug]) }}">
        @if($book->thumbnail)
            <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
        @else
            <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
        @endif
        </a>
        <div class="caption">
            <h3 class="book-title"><a href="{{ route($products_show, [$book->slug]) }}">{{ $book->shortName }}</a></h3>
            <h3 class="price">{{ $book->getPrice() }}</h3>
            <p>
                <strong>SKU:</strong>&nbsp;<code>{{ $book->code }}</code>&nbsp;&nbsp;&nbsp;
                <strong>Weight:</strong>&nbsp;<code>{{ $book->getWeight() }}</code>
            </p>
            <hr/>
            <div class="book-buttons">
                {!! Form::open(['method' => 'POST', 'url' => route('products.cart.add')]) !!}
                <a href="{{ route($products_show, [$book->slug]) }}" class="btn btn-flat btn-sm btn-primary"><i class="fa fa-info-circle"></i>&nbsp;More info</a>&nbsp;
                @if ($page_flag)
                    {!! Form::hidden('book_id', $book->id) !!}
                    {!! Form::hidden('quantity', '1') !!}
                    <button  class="btn btn-flat btn-sm btn-success"><i class="fa fa-cart-plus"></i>&nbsp;Add to Cart</button>&nbsp;
                @elseif (isset($user))
                    {!! Form::hidden('user_id', $user->id) !!}
                    {!! Form::hidden('book_id', $book->id) !!}
                    {!! Form::hidden('quantity', '1') !!}
                    <button  class="btn btn-flat btn-sm btn-success"><i class="fa fa-cart-plus"></i>&nbsp;Add to Cart</button>&nbsp;
                @endif
                @if (!$page_flag && !isset($user) && has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                    <a href="{{ route('admin.books.edit', [$book->id]) }}" class="btn btn-flat btn-sm btn-warning"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>