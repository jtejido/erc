@extends('redirect')

@section('stylesheets')
    @parent

    {!! HTML::style(elixir('css/app.css')) !!}
    {!! HTML::style(elixir('css/admin-lte.css')) !!}
    {!! HTML::style(elixir('css/admin-lte-2.css')) !!}
    {!! HTML::script(elixir('js/admin/angular.js')) !!}
    {!! HTML::script(elixir('js/admin/angular/app.js')) !!}
@endsection

@section('body')

@section('header')
    {{-- Header Section --}}
@show

<div class="container main-wrapper">
    @yield('main_content')
</div>



<footer>
    <div id="footer">
        <div class="row">
            <div class="col-sm-3">
<pre>
Environmental Resource Center &reg;
101 Center Pointe Drive
Cary, North Carolina 27513-5706</pre>
            </div>
            <div class="col-sm-6">
                <ul>
                    <li><a href="/contact-us" >Contact Us</a></li>
                    <li><a href="" data-toggle="modal" class="ppButtonModal">Privacy Policy</a></li>
                    <li><a href="" data-toggle="modal" class="siteButtonModal">Sitemap</a></li>
                </ul>
                <p>
                    © {{ date('Y') }} Environmental Resource Center &reg;. All rights reserved.
                </p>
            </div>
            <div class="col-sm-3">
                <h3 class="phone"><a href="tel:+8005372372"><i class="fa fa-phone-square" aria-hidden="true"></i> 800-537-2372</a></h3>
            </div>
        </div>
    </div>
</footer>
@stop

@section('javascripts')
    @parent

    {!! HTML::script(elixir('js/admin/admin-lte.js')) !!}
    {!! HTML::script(elixir('js/frontend/frontend.js')) !!}
@endsection