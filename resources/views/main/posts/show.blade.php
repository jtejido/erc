@extends('main.main_layout')

@section('title')
    {{ $post->name }} | Environmental Resource Center
@endsection

@section('meta')
    <meta name="title" content="{{ $post->meta_title }}">
    <meta name="description" content="{{ $post->meta_desc }}">
@endsection

@section('title')
    {{ $post->meta_title }}
@endsection

@section('stylesheets')
    @parent
    <style>
        .text-black {
            color : #000;
        }
        .default-form label {
            max-width: inherit;
        }
    </style>

@stop
@section('main_content')

    <section class="main post-show">
        <div class="container-fluid">
            <div class="row">

            </div>
            <div class="row">


                <div class="page-title">



                    <div class="border">

                        <!-- STATIC BUTTONS -->
                        <div class="row">
                        <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                            <div class="inner">
                                <div class="main-class">
                                    <a href="{{ route('courses')  }}?type=3" type="button" target="_blank" class="cbt"></a>
                                    <label> Online Training </label>
                                </div>
                                <div class="main-class">
                                    <a href="{{ route('courses')  }}" type="button" target="_blank"  class="training-register"></a>
                                    <label> Register for Training </label>
                                </div>
                                <div class="main-class">
                                    <a href="/docs/compliance_calendar.pdf" type="button" target="_blank" class="calendar"></a>
                                    <label> Compliance Calendar </label>
                                </div>
                                <div class="main-class">
                                    <a href="{{ route('resource.classcalendar') }}" type="button" target="_blank" class="class-sched"></a>
                                    <label> Schedule of Classes </label>
                                </div>
                            </div>
                        </div>
                        </div>
                        <!-- STATIC BUTTONS END -->

                    </div>
                    <div class="inner">
                        <h1> {{ $post->name }}  </h1>
                    </div>
                </div>

            </div>
        </div>
        @if (Request::url() == url('/contact-us'))
            <div class="container">
                <div class="col-md-10 col-md-offset-1">
                    <div class="pull-left contact-us-form">
                        <h3 style="margin-top: 10px">Leave Us A Message</h3>
                        <hr/>
                        <form action="{{ route('contact.us.send') }}" method="POST" class="form-horizontal">
                            {!! csrf_field() !!}

                            <div class="form-group">
                                <label for="name" class="col-sm-3 text-black control-label">Full Name <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Full Name" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-sm-3 text-black control-label">Email <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone" class="col-sm-3 text-black control-label">Phone</label>
                                <div class="col-sm-9">
                                    <input type="text" name="phone" class="form-control"
                                           id="phone" placeholder="Phone Number"
                                           data-inputmask='"mask" : "(999) 999-9999"' data-mask>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="message" class="col-sm-3 text-black control-label">Message</label>
                                <div class="col-sm-9">
                                    <textarea name="message" class="form-control" style="height:150px; width; 100%" required></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>

                        @include('includes.success_message')
                        @include('includes.error_message')
                    </div>
                    <div class="inner">
                        <div class="static-wrap">
                            <div class="entry">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        @else
            <div class="container">
                <div class="col-md-10 col-md-offset-1">
                    <div class="inner">
                        <div class="static-wrap">
                            <div class="entry">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        @endif

        @if(Request::url() == url("/training/on-site-and-customized-training"))
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="inner">
                    <div class="static-wrap">
                        <div class="entry">
                            <div class="col-md-10">
                            @if (count($errors) > 0)
                                <div class="col-sm-offset-3 col-sm-7">
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @if(\Session::has('successMessage'))
                                <div class="col-sm-offset-3 col-sm-7">
                                    <div class="alert alert-success">
                                        <strong style="color:#fff;">{{ \Session::get('successMessage') }}</strong>
                                    </div>
                                </div>
                            @endif
                            {!! Form::open([
                                'class' => 'form-horizontal default-form',
                                'route' => 'post.custom-training',
                                'id' => 'customTraining'])
                            !!}
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Name <span class="text-danger">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Company <span class="text-danger">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" name="company" class="form-control" id="inputEmail3" placeholder="Company" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Email <span class="text-danger">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Phone number <span class="text-danger">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" name="phone_number" class="form-control" id="inputEmail3" placeholder="Phone number" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Training Requested <span class="text-danger">*</span></label>
                                    <div class="col-sm-7">
                                        <input type="text" name="training_requested" class="form-control" id="inputEmail3" placeholder="Training Requested" required="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Number of students</label>
                                    <div class="col-sm-7">
                                        <input type="text" name="number_students" class="form-control" id="inputEmail3" placeholder="Number of students" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-7 col-sm-offset-3">
                                        <div class="checkbox">
                                            <label class="text-black">
                                                <input type="checkbox" name="for_call" value="Yes" checked="checked"> Please call me to discuss training
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 text-black control-label">Other Comments</label>
                                    <div class="col-sm-7">
                                        <textarea name="comments" class="form-control" style="height:150px;"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-7 text-right">
                                        <button class="btn btn-primary" id="btnSubmit">Submit</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @if(Request::url() == url("/training/subscription"))
            <div class="content-wrapper-course">
                <div class="container">
                    <div class="course-list-wrapper row col-md-10 col-md-offset-1">
                        <div class="col-md-10 col-md-offset-2 subscription-container">
                            <h4 class="col-md-4">Subscriptions</h4>
                            <a href="{{ route('yearly_subscription') }}" class="btn btn-primary">Annual Subscription</a>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#csModal" class="btn btn-primary btn-corporate-seat">Corporate Subscription</a>
                        </div>
                    </div>
                </div>
            </div>
        <br/>
            @include('includes.corporate_seat_notification')
        @endif

    </section>

    {{--<!-- STATIC SIDEBAR -->--}}
    {{--<section class="sidebar">--}}
        {{--<div class="container">--}}
            {{--<div class="col-md-10 col-md-offset-1">--}}
                {{--<div class="inner">--}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<h2> Reg of the Day</h2>--}}
                        {{--<div class="wrapper">--}}

                            {{--@if ($regulation)--}}
                                {{--<p class="title"> {{ $regulation->title }} </p>--}}
                                {{--<p class="description">--}}
                                    {{--{!! str_limit(strip_tags($regulation->content), 30, '...') !!}--}}
                                {{--</p>--}}
                            {{--@endif--}}
                        {{--</div>--}}

                        {{--@if ($regulation)--}}
                            {{--<a href="{{ route('main.regulations.index') }}" class="view"> View all </a>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<div id="live-chat" class="hidden"></div>--}}
                        {{--<img src="/img/erc/live-chat.png" class="banner live-chat-img" >--}}
                        {{--<div class="sign-up-wrapper">--}}
                            {{--<a href="http://care.ercweb.com/lists/?p=subscribe&id=2" target="_blank" class="btn btn-primary"> SIGN UP</a>--}}
                            {{--<p>For Reg of the Day, Tips of the Week, Discounts and Special Offers</p>--}}
                        {{--</div>--}}
                    {{--</div>                  --}}
                    {{--<div class="col-sm-4 sidebar-container">--}}
                        {{--<h2> Tips of the Week </h2>--}}
                        {{--<div class="wrapper">--}}
                            {{--<p class="title"> Environmental </p>--}}
                                {{--@if ($tipsEnv)--}}
                                    {{--<p class="description"> {{ $tipsEnv->title }} </p>--}}
                                {{--@endif--}}
                            {{--<p class="title"> Safety </p>--}}
                            {{--@if ($tipsSafe)--}}
                                {{--<p class="description"> {{ $tipsSafe->title }} </p>--}}
                            {{--@endif--}}
                        {{--</div>--}}
                        {{--@if ($tipsEnv or $tipsSafe)--}}
                            {{--<a href="{{ route('main.tips.index') }}" class="view"> View all </a>--}}
                        {{--@endif--}}
                    {{--</div>      --}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>    --}}
    {{--<!-- STATIC SIDEBAR END -->--}}
    @include('_mid-footer-content')
@stop

@section('javascripts')
    @parent

    <script>
        (function($) {

            $('.news').slick({
                dots: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            arrows: false,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false,
                        }
                    }]
            });
            $("#btnSubmit").click(function(e){
                e.stopImmediatePropagation()
                e.preventDefault()
                $(this).attr('disabled',true);
                $("#customTraining").submit();
            });

            $('[data-mask]').inputmask();
        })(jQuery)
    </script>

@endsection