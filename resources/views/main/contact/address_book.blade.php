@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Address Book</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main-container container-fluid course-attendees-container">
        <section class="attendees-list">

            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @include('errors.form_errors')

            @include('includes.success_message')

            @if ($contacts->count() > 0)
                @foreach ($contacts as $contact)

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            {{ $contact->first_name }} {{ $contact->last_name }}

                            <div class="pull-right">
                                @if (!$contact->user and !$contact->note)
                                    <a href="javascript:void(0)" class="edit-contact"
                                       data-id="{{ $contact->id }}"
                                       data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                                    </a>
                                @endif

                                <a href="javascript:void(0)" class="delete-contact"
                                   data-id="{{ $contact->id }}"
                                   data-user_id="{{ $user->id }}"
                                   data-url="{{ route('contact.delete') }}"><i class="fa fa-remove"></i>
                                </a>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="info">
                                <label for="email">Email: </label>
                                <p>{{ $contact->user ? $contact->user->email : 'No email' }}</p>
                            </div>

                            @if (!$contact->user and !$contact->note)
                                <div class="info">
                                    <label for="address">Address: </label>
                                    @if (strlen($contact->address1) &&
                                         strlen($contact->city) &&
                                         strlen($contact->state) &&
                                         strlen($contact->zip))
                                        <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                        <p class="block">{{ $contact->city }}, {{ $contact->state }} {{ $contact->zip }}</p>
                                    @endif
                                </div>
                            @endif

                            @if ($contact->note)
                                <div class="info">
                                    <label for="note">Note: </label>
                                    <p>{{ $contact->note }}</p>
                                </div>
                            @endif
                        </div>
                    </div>

                @endforeach
            @endif

            <div class="transaction-buttons row text-center">
                <button class="btn btn-lg btn-primary" id="add-contact"
                        data-user_id="{{ $user->id }}"
                        data-url="{{ route('contact.form.get') }}">
                    <i class="fa fa-user-plus"></i>&nbsp;New Contact
                </button>
            </div>

            @include('includes.notification_modal')
            <!-- Modal for adding / editing class -->

            <div class="modal fade" id="contactModal" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title" id="classModalLabel">Save Contact</h5>
                        </div>
                        <div class="modal-body">
                            <i>Loading...</i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection


