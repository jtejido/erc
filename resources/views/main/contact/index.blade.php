@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Course Attendees</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    @if(isset($class))
                    <p class="lead text-center">
                        You are registering for the class<br/></p>
                        <h3 class="text-center">{{ $class->title }}</h3><br/>
                    @if ($course->course_type_id == \App\Utilities\Constant::SEMINAR)
                    <p class="lead text-center">
                        at <strong>{{ $class->completeLocation }}</strong><br/>
                        Phone: <strong>{{ $class->location_phone }}</strong>
                    </p>
                    @endif
                        <p class="lead text-center">
                            on {{ $class->getDateTimeRange() }}. <br/>
                        </p>
                    @elseif(isset($course))<p class="lead text-center">
                        You are registering for the class <strong>{{ $course->title }}</strong>
                    </p>

                    @endif
                </div>

            </div>
        </div>

    </section>

    {{-- */ $noContact = 0; $registrable = $class ?: $course; $classId = $class ? $class->id : 0;  /* --}}

    <section class="main-container container-fluid course-attendees-container">

        <section class="attendees-list">

            <div class="alert alert-warning alert-attendee" role="alert">
                @if (isset($orderItemId))
                    "Check" and "Uncheck" the attendee you want to swap, then click <strong>Swap</strong>
                @else
                    Toggle the checkbox beside the name of the attendee then click <strong>Register</strong>
                @endif
            </div>

            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @include('errors.form_errors')

            <input type="hidden" id="item-id" value="@if (isset($orderItemId)) {{ $orderItemId }} @else 0 @endif">
            <input type="hidden" id="reg-count" value="@if (isset($regCount)) {{ $regCount }} @else 0 @endif">
            <input type="hidden" id="is-swap" value="@if (isset($orderItemId)) 1 @else 0 @endif">


            @if (!in_array($user->contact->id, $notIn))

                {{-- */
                    $noContact++;

                    $checked = (!count($attendees) and !isset($orderItemId))
                         ? 'checked'
                         : (in_array($user->contact->id, $attendees)
                         ? 'checked' : '');

                    $noSwapYearlySub = in_array($user->contact->id, $noSwapYearlySubs);
                    $noSwapGSA = in_array($user->contact->id, $noSwapGSAs);
                    $noSwapCancel = in_array($user->contact->id, $noSwapCancelled);

                    $disabled = (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId)) ? 'disabled' : '';

                    $isContactRegistered = OrderUtility::isContactRegistered($user->contact->id, $registrable);

                /* --}}

                <div class="panel {{ ($isContactRegistered and !isset($orderItemId)) ? 'panel-default' : 'panel-primary' }}">
                    <div class="panel-heading">
                        <label>
                            @if(!$isContactRegistered or isset($orderItemId))
                            <input type="checkbox" name="contact[]" value="{{ $user->contact->id }}"
                                   class="contact" {{ $checked }} {{ $disabled }}>
                            @endif
                            {{ $user->contact->first_name }} {{ $user->contact->last_name }}
                        </label>
                    </div>
                    <div class="panel-body">
                        <div class="info">
                            <label for="email">Email: </label> <p>{{ $user->email }}</p>
                        </div>

                        @if (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId))
                            <div class="info">
                                <p>
                                    <span class="required">
                                        Attendee can not be swapped
                                        @if ($noSwapCancel)
                                            because registration is cancelled.
                                        @elseif ($noSwapYearlySub)
                                            because of Yearly Subscription.
                                        @elseif ($noSwapGSA)
                                            because of GSA Discount.
                                        @endif
                                    </span>
                                </p>
                            </div>
                        @endif
                    </div>
                </div>

            @endif

            @if ($contacts->count() > 0)
                @foreach ($contacts as $contact)

                        {{-- */
                            $noContact++;

                            $checked = (is_array($attendees) && in_array($contact->id, $attendees)
                                        ? 'checked'
                                        : Session::has('contact_id') &&
                                          Session::get('contact_id') == $contact->id
                                        ? 'checked' : '');

                            $noSwapYearlySub = in_array($contact->id, $noSwapYearlySubs);
                            $noSwapGSA = in_array($contact->id, $noSwapGSAs);
                            $noSwapCancel = in_array($contact->id, $noSwapCancelled);

                            $disabled = (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId)) ? 'disabled' : '';

                            $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $registrable);

                        /* --}}

                        <div class="panel {{ ($isContactRegistered and !isset($orderItemId)) ? 'panel-default' : 'panel-primary' }}">
                            <div class="panel-heading">
                                <label>
                                    @if(!$isContactRegistered or isset($orderItemId))
                                    <input type="checkbox" name="contact[]" value="{{ $contact->id }}"
                                           class="contact" {{ $checked }} {{ $disabled }}>
                                    @endif
                                    {{ $contact->first_name }} {{ $contact->last_name }}
                                </label>

                                @if (!$contact->user and !$contact->note)

                                    <a href="javascript:void(0)" class="edit-contact pull-right"
                                       data-course_id="{{ $course->id }}"
                                       data-class_id="{{ $classId }}"
                                       data-id="{{ $contact->id }}"
                                       data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                                    </a>

                                @endif
                            </div>
                            <div class="panel-body">
                                @if($contact->user)
                                <div class="info">
                                    <label for="email">Email: </label>
                                    <p>{{ $contact->user ? $contact->user->email : 'No email' }}</p>
                                </div>
                                @endif

                                @if (!$contact->user and !$contact->note)
                                    <div class="info">
                                        @if (strlen($contact->address1) &&
                                             strlen($contact->city) &&
                                             strlen($contact->state) &&
                                             strlen($contact->zip))
                                            <p class="contact-address">{{ $contact->address1 }} {{ $contact->address2 }}<br/>
                                            {{ $contact->city }}, {{ $contact->state }} {{ $contact->zip }}</p>
                                        @endif
                                    </div>
                                @endif

                                @if ($contact->note)
                                    <div class="info">
                                        <label for="note">Note: </label>
                                        <p>{{ $contact->note }}</p>
                                    </div>
                                @endif

                                @if (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId))
                                    <div class="info">
                                        <p>
                                    <span class="required">
                                        Attendee can not be swapped
                                        @if ($noSwapCancel)
                                            because registration is cancelled.
                                        @elseif ($noSwapYearlySub)
                                            because of Yearly Subscription.
                                        @elseif ($noSwapGSA)
                                            because of GSA Discount.
                                        @endif
                                    </span>
                                        </p>
                                    </div>
                                @endif
                            </div>
                        </div>

                @endforeach
            @endif
            <div class="text-center">
                <button class="btn btn-primary" id="add-contact"
                        data-course_id="{{ $course->id }}"
                        data-class_id="{{ $classId }}"
                        data-user_id="{{ $user->id }}"
                        data-url="{{ route('contact.form.get') }}">
                    <i class="fa fa-user-plus"></i>&nbsp;New Contact
                </button>
            </div>

            @if (!$noContact)
                <div class="alert alert-danger">
                    No contacts for this Course/Class.
                </div>
            @endif

            <div class="transaction-buttons row text-center">

                @if ($orderItemId)
                    <a href="{{ route('user.home') }}" class="btn btn-default btn-lg">
                        Go Back
                    </a>
                @endif
                <button class="btn btn-info btn-lg" id="register"
                        data-course_id="{{ $course->id }}"
                        data-class_id="{{ $classId }}"
                        data-user_id="{{ $user->id }}"
                        data-url="@if(isset($orderItemId)){{ route('attendees.do_swap') }}@else{{ route('contact.add_to_class') }}@endif">

                    @if (isset($orderItemId))
                        Swap
                    @else
                        Register
                    @endif
                </button>
            </div>

            @if ($pairedClassSwapNote)
                  <input type="hidden" id="paired-class" value="{{ $pairedClassSwapNote }}"
                         data-registration-order-id="{{ $registrationOrderId }}"
                         data-url="{{ route('attendees.do_swap.notification') }}">
            @endif
            <!-- Modal for adding / editing contact -->

            <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h5 class="modal-title" id="classModalLabel">Save Contact</h5>
                        </div>
                        <div class="modal-body">
                            <i>Loading...</i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

