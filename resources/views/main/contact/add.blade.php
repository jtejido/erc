<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="classModalLabel">
        Save @if (isset($customer) && $customer) Customer @else Contact @endif
    </h4>
</div>

{!! Form::open([
    'method' => 'POST',
    'url' => isset($customer) ? route('admin.user.add') : route('contact.add'),
    'id' => isset($customer) ? 'customer-book-form' : 'address-book-form'
    ]) !!}
    <div class="modal-body save-contact">

        {!! csrf_field() !!}

        @if(isset($contact))
            {!! Form::input('hidden', 'id', $contact->id) !!}
        @endif

        @if (isset($user))
            {!! Form::input('hidden', 'user_id', $user->id) !!}
        @endif

        @if (isset($class))
            {!! Form::input('hidden', 'class_id', $class->id) !!}
        @endif

        @if (isset($course))
            {!! Form::input('hidden', 'course_id', $course->id) !!}
        @endif

        @if (isset($item))
            {!! Form::input('hidden', 'remember_contact_id', $item) !!}
        @endif

        @if (isset($customer))
            {!! Form::input('hidden', 'role', \App\Utilities\Constant::MEMBER) !!}
        @endif

        @if (isset($email))
            {!! Form::rLabel('email', 'E-Mail: ', ['class' => 'col-sm-3 control-label']) !!}
        @else
            {!! Form::label('email', 'E-Mail: ', ['class' => 'col-sm-3 control-label']) !!}
        @endif

        <div class="form-group col-md-8">
            {!! Form::input('email', 'email', isset($contact) && $contact->user ? $contact->user->email : '', [
            'class' => 'form-control',
            'id'    => 'email',
            'style' => 'display: inline;',
            'placeholder' => 'E-Mail',
            'data-email' => '',
            'data-url'  => route('email.check'),
            isset($email) ? 'required' : null,
            isset($contact) && $contact->user ? 'disabled' : null
            ]) !!}

            @if (isset($contact) && $contact->user)
                <span class="required small">Contact's email cannot be modified.</span>
            @endif
            @if (!isset($customer))
            <p class="help-block">
                <em>
                    If the contact you are adding does not have an email address, leave this blank. And we’ll send the emails for this person to you.
                </em>
            </p>
            @endif
        </div>

        <div class="found-match-container" style="display: none;">
            {!! Form::label('', '', ['class' => 'col-sm-3 control-label']) !!}

            <div class="form-group col-md-8">
                <div class="match-found-text">
                    <span class="required">We have found a match of the email.</span>
                    <p>Is this the person you're looking for?</p>
                    <p>Email : <span class="email">johndoe@erc.com</span></p>
                    <p>First Name : <span class="first_name">John</span></p>
                    <p>Last Name : <span class="last_name">Doe</span></p>

                    <input type="hidden" name="match_user_id" id="match_user_id">

                    <div class="options">
                        <input type="radio" name="matched_user" value="1"> Yes
                    </div>

                    <div class="options">
                        <input type="radio" name="matched_user" value="0"> No
                    </div>
                </div>
            </div>
        </div>


        <div class="match-info">

            @if (!Auth::user()->isMember())

                {!! Form::label('course_   mill_user_id', 'Course Mill User Id: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="form-group col-md-8">
                    {!! Form::input('text', 'course_mill_user_id', isset($contact) ? $contact->course_mill_user_id : '', [
                    'class' => 'form-control',
                    'id'    => 'course_mill_user_id',
                    'style' => 'display: inline;',
                    ]) !!}
                </div>

            @endif

            {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'first_name', isset($contact) ? $contact->first_name : '' , [
                'class' => 'form-control',
                'id'    => 'first_name',
                'style' => 'display: inline;',
                'required' => 'required',
                'placeholder' => 'First Name'
                ]) !!}
            </div>

            {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'last_name', isset($contact) ? $contact->last_name : '', [
                'class' => 'form-control',
                'id'    => 'last_name',
                'style' => 'display: inline;',
                'required' => 'required',
                'placeholder' => 'Last Name'
                ]) !!}
            </div>

            {!! Form::rLabel('company', 'Company: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'company', isset($contact) ? $contact->company : '', [
                'class' => 'form-control',
                'id'    => 'company',
                'style' => 'display: inline;',
                'placeholder' => 'Company',
                'required' => 'required',
                ]) !!}
                @if(!isset($customer))
                <div class="checkbox" data-user-contact="{{ (isset($user) AND count($user) > 0)?collect($user->contact->toArray())->except(['id','course_mill_user_id','course','first_name','last_name','created_at','updated_at','deleted_at']):collect() }}">
                    <label>
                        <input type="checkbox" id="current_address"> Use my current company and address information
                    </label>
                </div>
                @endif
            </div>

            {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'title', isset($contact) ? $contact->title : '', [
                'class' => 'form-control',
                'id'    => 'title',
                'style' => 'display: inline;',
                'placeholder' => 'Title'
                ]) !!}
            </div>

            {!! Form::rLabel('address1', 'Address1: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'address1', isset($contact) ? $contact->address1 : '', [
                'class' => 'form-control',
                'id'    => 'address1',
                'style' => 'display: inline;',
                'required' => 'required',
                'placeholder' => 'Address1'
                ]) !!}
            </div>

            {!! Form::label('address2', 'Address2: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'address2', isset($contact) ? $contact->address2 : '', [
                'class' => 'form-control',
                'id'    => 'address2',
                'style' => 'display: inline;',
                'placeholder' => 'Address2'
                ]) !!}
            </div>

            {!! Form::rLabel('city', 'City: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'city', isset($contact) ? $contact->city : '', [
                'class' => 'form-control',
                'id'    => 'city',
                'style' => 'display: inline;',
                'required' => 'required',
                'placeholder' => 'City'
                ]) !!}
            </div>

            {!! Form::rLabel('state', 'State: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::select('state', ['' => '-- Select State --'] + $states, isset($contact) ? $contact->state : '', [
                    'id'    => 'state',
                    'class' => 'form-control state',
                    'style' => 'display: inline;',
                    'required' => 'required',
                    'placeholder' => 'State'
                ]) !!}
            </div>

            {!! Form::rLabel('zip', 'Zip: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'zip', isset($contact) ? $contact->zip : '', [
                'class' => 'form-control',
                'id'    => 'zip',
                'style' => 'display: inline;',
                'required' => 'required',
                'placeholder' => 'Zip Code',
                 'maxlength'=>'5',
                 'size'=>'5',
                 'pattern'=>"^\d{5}$",
                 'title'=>'The US zipcode must contain 5 digits'
                ]) !!}
            </div>

            {!! Form::rLabel('phone', 'Phone: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'phone',  isset($contact) ? $contact->phone : '', [
                'class' => 'form-control',
                'id'    => 'phone',
                'style' => 'display: inline;',
                'data-inputmask' => '"mask" : "(999) 999-9999"',
                'data-mask',
                'required' => 'required',
                ]) !!}
            </div>
            {!! Form::label('phone_local', 'Extension: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('text', 'phone_local',  isset($contact) ? $contact->phone_local : '', [
                'class' => 'form-control',
                'id'    => 'phone_local',
                'style' => 'display: inline;',
                ]) !!}
            </div>

            {!! Form::label('fax', 'Fax: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::input('tel', 'fax', isset($contact) ? $contact->fax : '', [
                'class' => 'form-control',
                'id'    => 'fax',
                'style' => 'display: inline;',
                'data-inputmask' => '"mask" : "(999) 999-9999"',
                'data-mask',
                'placeholder' => 'Fax'
                ]) !!}
            </div>

        </div>


        @if (! Auth::user()->isMember())
            <div class="row">
                <div class="col-xs-12">
                    <p class="text-center">
                        <label>
                            <input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                        Do Not Email Agent And Attendees</label>
                    </p>
                </div>
            </div>
        @endif


        <div style="text-align: right;">
            <button type="button"
                    class="btn btn-flat @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-default'}}@endif btn-close"
                    data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-flat @if (Auth::user()->isMember()){{'blue-btn'}}@else{{'btn-primary'}}@endif"
                    id="save-contact"
                    value="Save">Save</button>
        </div>
    </div>

{!! Form::close() !!}