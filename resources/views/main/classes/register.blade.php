@extends('main.main_layout')

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1>Course Attendees</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    <p class="lead text-center">
                        You are registering for multiple classes. Kindly verify the attendees below.
                    </p>
                </div>
            </div>
        </div>

    </section>

    {!! Form::open([
        'url'     => route('classes.register.store'),
        'method'  => 'POST'
    ]) !!}

        {!! Form::hidden('registration', null) !!}

    {{-- */ $noContact = 0; $registrable = $class ?: $course; $classId = $class ? $class->id : 0;  /* --}}

    <section class="main-container container-fluid course-attendees-container">

        <section class="attendees-list">

            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @include('errors.form_errors')


                {{-- */
                    $noContact++;

                    $checked = (!count($attendees))
                         ? 'checked'
                         : (in_array($user->contact->id, $attendees)
                         ? 'checked' : '');

                    $noSwapYearlySub = in_array($user->contact->id, $noSwapYearlySubs);
                    $noSwapGSA = in_array($user->contact->id, $noSwapGSAs);

                    $disabled = ($noSwapYearlySub or $noSwapGSA) ? 'disabled' : '';

                /* --}}

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <input type="hidden" id="item-id" value="@if (isset($orderItemId)) {{ $orderItemId }} @else 0 @endif">
                        <input type="hidden" id="reg-count" value="@if (isset($regCount)) {{ $regCount }} @else 0 @endif">

                        <label>
                            {{ $user->contact->name }}
                        </label>
                    </div>
                    <div class="panel-body">
                        <div class="info">
                            Email: {{ $user->email }}
                        </div>
                        <div class="class_list main_list" data-contact="{{ $user->contact->id }}">
                            <ul class="classes">
                            @foreach($classes as $mclass)
                                {{-- */
                                $isContactRegistered = OrderUtility::isContactRegistered($user->contact->id, $mclass);
                                /* --}}
                                <li class="{{ ($isContactRegistered) ? 'disabled-elem' : '' }}">
                                    <label>
                                        @if(!$isContactRegistered)
                                    {!! Form::checkbox('classes[]', $mclass->id, null, ['class' => 'contact_id']) !!}
                                        @else
                                    {!! Form::checkbox('classes[]', $mclass->id, null, ['class' => 'contact_id', 'disabled' => 'disabled']) !!}
                                        @endif
                                    {{ $mclass->title }} | {{$mclass->titleLocation}}
                                    </label>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>


            @if ($contacts->count() > 0)
                @foreach ($contacts as $contact)

                        {{-- */
                            $noContact++;

                            $checked = (is_array($attendees) && in_array($contact->id, $attendees)
                                        ? 'checked'
                                        : Session::has('contact_id') &&
                                          Session::get('contact_id') == $contact->id
                                        ? 'checked' : '');

                            $noSwapYearlySub = in_array($contact->id, $noSwapYearlySubs);
                            $noSwapGSA = in_array($contact->id, $noSwapGSAs);

                            $disabled = ($noSwapYearlySub or $noSwapGSA) ? 'disabled' : '';

                        /* --}}

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <label>
                                    {{ $contact->name }}
                                </label>

                                @if (!$contact->user and !$contact->note)

                                    <a href="javascript:void(0)" class="edit-contact pull-right"
                                       data-course_id="{{ $course->id }}"
                                       data-class_id="{{ $classId }}"
                                       data-id="{{ $contact->id }}"
                                       data-url="{{ route('contact.form.get') }}"><i class="fa fa-edit"></i>
                                    </a>

                                @endif
                            </div>
                            <div class="panel-body">

                                @if (!$contact->user and !$contact->note)
                                    <div class="info">
                                        @if (strlen($contact->address1) &&
                                             strlen($contact->city) &&
                                             strlen($contact->state) &&
                                             strlen($contact->zip))
                                            <p>{{ $contact->address1 }} {{ $contact->address2 }}<br/>
                                            {{ $contact->city }}, {{ $contact->state }} {{ $contact->zip }}</p>
                                        @endif
                                    </div>
                                @endif
                                @if($contact->user)
                                    <div class="info">Email: {{ $contact->user->email or '' }}</div>
                                @endif
                                <div class="class_list" data-contact="{{ $contact->id }}">
                                    <ul class="classes">
                                        @foreach($classes as $mclass)
                                        {{-- */
                                            $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $mclass);
                                        /* --}}
                                        <li class="{{ ($isContactRegistered) ? 'disabled-elem' : '' }}">
                                            <label>
                                                @if(!$isContactRegistered)
                                                {!! Form::checkbox('classes[]', $mclass->id) !!}
                                                @else
                                                    {!! Form::checkbox('classes[]', $mclass->id, null, ['disabled'=>'disabled']) !!}
                                                @endif
                                                {{ $mclass->title }} | {{$mclass->titleLocation}}
                                            </label>
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
@endforeach
@endif

@if (!$noContact)
<div class="alert alert-danger">
No contacts for this Course/Class.
</div>
@endif

<div class="transaction-buttons row">

<button class="btn blue-btn col-md-4 @if($orderItemId){{'col-md-offset-1'}}@endif"
id="add-contact"
type="button"
data-course_id="{{ $course->id }}"
data-class_id="{{ $classId }}"
data-user_id="{{ $user->id }}"
data-url="{{ route('contact.form.get') }}">

Add Contact
</button>
<button class="btn blue-btn pull-right col-md-4"
id="register"
type="button"
data-course_id="{{ $course->id }}"
data-class_id="{{ $classId }}"
data-user_id="{{ $user->id }}">
Register
</button>
</div>

{!! Form::close() !!}

</section>
</section>


<!-- Modal for adding / editing contact -->

<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header panel-heading">
<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h5 class="modal-title" id="classModalLabel">Save Contact</h5>
</div>
<div class="modal-body">
<i>Loading...</i>
</div>
</div>
</div>
</div>

@endsection

@section('javascripts')
@parent
<script type="text/javascript">
(function($) {
$('#register').on('click', function() {

var classes_list = [], registration = [], $form = $(this).closest('form');

// traverse classes
$('.main_list').find('input[type="checkbox"]').each(function() {
// traverse contacts
    var obj = {}, contacts = [], class_id;
    obj.class_id = class_id = $(this).val();
    $('.class_list').each(function() {
        var obj = {}, contact_id = $(this).data('contact'), classes = [];
        $(this).find('input[type="checkbox"]').each(function() {
            if ($(this).val() == class_id && $(this).is(':checked')) {
                contacts.push(contact_id);
            }
        });
    });

    if (contacts.length) {
        obj.contacts = contacts;
        registration.push(obj);
    }
});

if(!registration.length) {
    return false;
}

var reg = JSON.stringify(registration);

$form.find('input[name="registration"]').val(reg);
$form.submit();

return false;
});
})(jQuery);
</script>
@endsection

