@extends('main.main_layout')

@section('meta')
    <?php $title = 'Our Classes | Environmental Resource Center'; $description = 'Do you want to become part of our growing community? Do you want to become successful? Register now.Grab this amazing offer specially for you.'; ?>

    @if ($slug == 'los-angeles')
        <?php $title = 'Classes in Los Angeles | Environmental Resource Center'; ?>
        <?php $description = 'We have locations near you. Visit our Los Angeles Center or give us a call at 800-537-2372 our friendly Customer Service Representative are waiting for you.'; ?>
    @elseif ($slug == 'chicago')
        <?php $title = 'Classes in Chicago | Environmental Resource Center'; ?>
        <?php $description = 'Our Chicago Center has opened. Come, visit us or give us a call at 800-537-2372. Our friendly Customer Service Representative are happy to answer your question.'; ?>
    @elseif ($slug == 'spartanburg')
        <?php $title = 'Classes in Spartanburg | Environmental Resource Center'; ?>
        <?php $description = 'We also have a center at Spartanburg. Come, see the amazing people at our office or give us a call at 800-537-2372 to know more details. We\'re waiting on you.'; ?>
    @endif

    <meta name="title" content="{{ $title }}">
    <meta name="description" content="{{ $description }}">
@endsection

@section('title')
    @if ($slug == 'los-angeles')
        Classes in Los Angeles | Environmental Resource Center
    @elseif ($slug == 'chicago')
        Classes in Chicago | Environmental Resource Center
    @elseif ($slug == 'spartanburg')
        Classes in Spartanburg | Environmental Resource Center
    @else
        Our Classes | Environmental Resource Center
    @endif

@endsection

@section('main_content')
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Classes in {{ $location_name }} </h1>
                    </div>
                </div>
            </div>
        </div>
        <div>

            <div class="content-wrapper-course course-details">
                <div class="container course-container">
                    <div class="course-list-wrapper row col-md-10 col-md-offset-1">

                        <div class="col-md-10 col-md-offset-1">
                            <br/>

                            <form id="select-location" action="{{ route('classes') }}" method="GET">
                                <div class="text-center form-group">
                                    {!! Form::select('slug', $locations, $slug, [
                                        'class' => 'form-control',
                                        'id'    => 'sel-location',
                                        'style' => 'width: 300px; margin: 0 auto; display: inline'
                                    ]) !!}
                                </div>
                            </form>

                            <br/>

                            @if($active->count() == 0)

                                        <div class="text-center">
                                            <p class="alert alert-danger">
                                                No Classes Offered for this Location
                                            </p>
                                        </div>

                                @else

                            <table class="table table-scroll">
                                <thead>
                                    <th width="160">Class Dates</th>
                                    <th>Title</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                @forelse ($active as $class)
                                    <tr>

                                        <td>
                                            {{ $class->dateRangeOnly }}<br/>
                                        </td>

                                        <td>
                                            {{ $class->title }}

                                            <small>
                                                <button class="btn btn-xs btn-default more-info-btn">
                                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                                    &nbsp;<span class="flag">More</span> Info
                                                </button>
                                                <div class="more-info-box">
                                                    <strong>Time: {{ $class->time }}<br/></strong>
                                                        {!! $class->locationNameLink !!}<br/>
                                                        {{ $class->locationAddress }}
                                                </div>
                                            </small>


                                        </td>

                                        <td>
                                            <a class="btn btn-info btn-xs"
                                               href="{{ route('classes.related', [
                                        'class_id'  => $class->id]) }}">
                                                Register
                                            </a>
                                        </td>

                                    </tr>
                                @empty


                                @endforelse

                                </tbody>

                            </table>

                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="course-listings">
            <div class="content-wrapper-course">
                <div class="container">
                    <div class="course-list-wrapper row col-md-12">
                        <div class="col-md-10 col-md-offset-1 subscription-container">
                            <h4 class="col-md-3">Subscriptions</h4>
                            <a href="{{ url('training/subscription') }}" class="col-md-4">Annual Subscription</a>
                            <a href="{{ url('training/subscription') }}" class="btn-corporate-seat col-md-4">Corporate Subscription</a>
                            <img class="live-chat-img" src="/img/erc/live_chat.png" alt="Live Chat" height="36" style="float: right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="live-chat" class="hidden"></div>

    @include('includes.corporate_seat_notification')
@stop

@section('javascripts')
    @parent
    <script>

        (function($) {

            $('.more-info-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    text: function(api){
                        return $(this).next('.more-info-box').clone();
                    }
                },
                show: {
                    event: 'click'
                },
                hide: {
                    event: 'click mouseleave'
                }
            });

            $('.more-info-btn').on('click', function(e) {
                var $that = $(this);
                e.preventDefault();
                if($that.find('.flag').text() == 'More')
                    $that.find('.flag').text('Less')
                else
                    $that.find('.flag').text('More');
            });


            $('.table-scroll').DataTable( {
                scrollY:        '380px',
                scrollCollapse: true,
                paging:         false,
                info:       false,
                searching:     false,
                ordering: false,
                columns: [
                    { "width": "200px" },
                    null,
                    { "width": "50px" },
                ],
                columnDefs: [
                    { "orderable": false, "targets": [0,1,2] }
                ]
            });

            $('#sel-location').change(function () {
                var action = $('#select-location').attr('action'),
                    slug = $(this).val();

                window.location.href = action + '/' + slug;
                return false;

            });

        })(jQuery);
    </script>
@endsection