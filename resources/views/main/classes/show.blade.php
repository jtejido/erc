@extends('main.main_layout')

@section('main_content')

    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="page-title">
                    <div class="border"></div>
                    <div class="inner">
                        <h1> Registration  </h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="class-details">
            <div class="detail-wrap">
                <div class="container">
                    <div class="panel panel-default">

                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <p class="lead text-center">You are registering for the class <br/></p>
                                    <h3 class="text-center">{{ $class->title }}</h3> <br/>
                                <p class="lead text-center">on {{ $class->getDateTimeRange() }}. <br/>
                                </p>
                            </div>
                        </div>
                        @if ($course->course_type_id == \App\Utilities\Constant::SEMINAR)
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <p class="lead text-center">It will be held at<br/>
                                    <strong>{{ $class->completeLocation }}</strong><br/>
                                    Phone: <strong>{{ $class->location_phone }}</strong>
                                </p>
                            </div>
                        </div>
                        @endif
                        {!! Form::open([
                            'url'   => route('contacts.register', [
                                    'course_id' => $class->course->id,
                                    'class_id'  => $class->id]),
                            'method'    => 'GET'
                        ]) !!}
                            {!! Form::hidden('register[]', $class->id) !!}
                        <div class="row">
                            <div class="col-xs-12 col-md-6 col-md-offset-3">
                                <br/>
                                <p class="lead text-center">Other classes that may interest you.</p>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Related Course/Class</th>

                                        @if (!Auth::user() || Auth::user()->hasRole(\App\Utilities\Constant::ROLE_MEMBER))
                                            <th width="10%">Register</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($class->classes as $rel_class)
                                        <tr>

                                            <td>
                                                <strong>{{ $rel_class->title }}</strong>
                                                <br/>
                                                <p>
                                                    <small><strong>{{ $rel_class->price }}</strong> per student</small><br/>
                                                    <small>{{ $rel_class->ShortDateRange }}</small>
                                                </p>
                                                <span class="label label-primary qtip2"
                                                      data-title="{{ $rel_class->title }}">
                                                    <i class="fa fa-info-circle"></i> More Info</span>
                                                <div class="tooltiptext">
                                                    <dl class="class-tooltip">
                                                        <dt>Date</dt>
                                                        <dd>{{ $rel_class->getDateTimeRange() }}</dd>
                                                        <dt>Location</dt>
                                                        <dd>{{ $rel_class->completeLocation }}</dd>
                                                        <dt>Price per Student</dt>
                                                        <dd>{{ $rel_class->price }}</dd>
                                                        <dt>Type</dt>
                                                        <dd><em>{{ $rel_class->course->type }}</em></dd>
                                                        <dt>SKU</dt>
                                                        <dd>{{ $rel_class->course->sku }}</dd>
                                                        <dt>Duration</dt>
                                                        <dd>{{ $rel_class->duration }}</dd>
                                                        <dt>Category</dt>
                                                        <dd>
                                                            @if ($rel_class->course->courseCategories->count())
                                                                @foreach ($rel_class->course->courseCategories as $courseCategory)
                                                                    {{ $courseCategory->category->name }} <br/>
                                                                @endforeach
                                                            @endif
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </td>


                                            <td>
                                                {!! Form::checkbox('register[]', $rel_class->id, null) !!}

                                            </td>
                                        </tr>
                                    @empty
                                        @if ($course->course_type_id != \App\Utilities\Constant::COMPUTER_BASED)
                                            <tr>
                                                <td colspan="5">
                                                    <div class="button-wrap">
                                                        <a class="btn btn-danger pull-right no-class">
                                                            No Classes Offered
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforelse
                                    </tbody>
                                </table>
                                <hr/>
                                <div class="text-center">


                                        <button type="submit"
                                                class="btn btn-lg btn-warning">Continue</button>


                                </div>
                                <br/>
                            </div>
                        </div>
                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('stylesheets')
    @parent
    <style>
        .tooltiptext{
            display: none;
        }
        span.qtip2 {
            cursor: help;
        }
    </style>
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            $('span.qtip2').each(function() {
                $(this).qtip({
                    position: {
                        my: 'bottom left',
                        at: 'top right'
                    },
                    content: {
                        title: { text: $(this).data('title') },
                        text: $(this).next('.tooltiptext')
                    },
                    style: {
                        classes: 'qtip-bootstrap'
                    }
                });
            });
        })(jQuery);
    </script>
@endsection
