@if ($promos->count())
    @foreach ($promos as $promo)
        @if ($promo)
            <div class="promotions-container" data-href="{{ $promo->url }}">
                <?php $path = route('promo.photo', $promo->id); ?>

                <img data-lazy="{{ $path }}" alt="Promotion" class="promotion-link" data-link="{{ $promo->url }}">
            </div>
        @endif
    @endforeach
@else
    <img src="/img/erc/homepage/off.png" alt="Promotion">
@endif