<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="title" content="Environmental Consulting & Training | Environmental Resource Center">
    <meta name="description" content="Environmental Resource Center has been the leading supplier of environmental, safety, & transportation consulting and training since 1981. Call 800-537-2372!">

    <title>@section('title') Environmental Consulting & Training | Environmental Resource Center @show</title>
    <script type="text/javascript">
        window.postInit = [];
    </script>

    @include('main.includes.google_analytics')

    @section('stylesheets')
        {!! HTML::style('vendor/common.css') !!}
        {!! HTML::style(elixir('css/app.css')) !!}
        {!! HTML::style(elixir('css/admin-lte.css')) !!}
    @show

</head>

<body>
    <input type="hidden" id="session-expired" value="{{ route('session.expired') }}">

    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: "{{ $fbApi }}",
                status: true,
                cookie: true,
                xfbml: true
            });
        };

        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>

    @include('main.homepage.mobile')

    <div class="main-container home-container">
        <!--script>
        (function() {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');

            var trident = ua.indexOf('Trident/');
            if (msie > 0 || trident > 0) {
                //document.body.innerHTML = '<div class="alert">Not supported.</div>' +
                //document.body.innerHTML;
                var alertItem = document.createElement('div')
                alertItem.className = 'alert alert-danger';
                var textnode = document.createTextNode('This website is best experienced on the newest browser IE edge or Google Chrome.')
                alertItem.appendChild(textnode);
                var chck = (parseInt(document.getElementsByClassName('main-container').length) - 1);
                var d1 = document.getElementsByClassName('main-container')[chck];
                d1.appendChild(alertItem)
            }
        })()
        </script-->
        <div class="content">
            <div class="header">
                <div class="logo-social-search">
                    <a href="/">
                    <img class="full-width" src="/img/ERC-LOGO-V3-2.png" alt="Environmental Resource Center">
                    </a>

                    <div class="pull-right right">
                        <div class="contact">
                            800-537-2372
                        </div>

                        <div class="social-search">
                            <div class="icons">
                                <a href="https://www.facebook.com/ercweb/" target="_blank" class=""><img src="/img/erc/homepage/facebook.png" alt="Facebook"></a>
                                <a href="https://twitter.com/ercweb" target="_blank" class=""><img src="/img/erc/homepage/twitter.png" alt="Twitter"></a>
                                <a href="https://www.youtube.com/channel/UC4BwgRQIsBi-T_VjOf29vJw" target="_blank" class=""><img src="/img/erc/youtube-1.png" alt="youtube"></a>
                                <a href="https://www.linkedin.com/company/environmental-resource-center" target="_blank" class=""><img src="/img/erc/homepage/linkedin.png" alt="Linkedin"></a>
                            </div>
                            <form id="top-search">
                            <div class="input-group">

                                <input type="search" class="form-control search" placeholder="Search" id="global-search">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" id="global-search-btn"><span class="fa fa-search"></span></button>
                                </span>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="navbar-collapse collapse nav-collapse" id="navbar">

                    <ul class="nav navbar-nav">
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Training
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('courses') }}">Course Listing</a></li>
                                <li><a href="/training/benefits">Benefits</a></li>
                                <li><a href="/training/on-site-and-customized-training">On-site and Customized Training</a></li>
                                <li><a href="/training/subscription">Subscriptions</a></li>
                                <li><a href="/training/gsa">GSA</a></li>
                            </ul>
                        </li>



                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Consulting
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/consulting/consulting">Consulting</a></li>
                                <li><a href="/consulting/audits">Audits</a></li>
                                <li><a href="/consulting/permits">Permits</a></li>
                                <li><a href="/consulting/hazardous-materialsdangerous-goods-classification">Hazardous Materials/Dangerous Goods Classification</a></li>
                                <li><a href="/consulting/sara-title-ii-compliance-and-reporting">SARA Title III Compliance and Reporting</a></li>
                                <li><a href="/consulting/plans-and-procedures">Plans and Procedures</a></li>
                                <li><a href="/consulting/ehs-program-management">EHS Program Management</a></li>
                                <li><a href="/consulting/safety-database-development">Safety Database Development</a></li>
                                <li><a href="/consulting/hazardous-materials-databases">Hazardous Materials Databases</a></li>
                                <li><a href="/consulting/ehs-on-site-staffing">EHS On-site Staffing</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ route('products') }}">Products</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Resources
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/regulations">Reg of the Day</a></li>
                                <li><a href="/tips">Tips of the Week</a></li>
                                <li><a href="{{ route('resource.classcalendar') }}">Schedule of Classes</a></li>
                                <li><a href="/resources/helpful-links">Helpful Links</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                FAQ
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/faq/who-needs-training">Who needs Training?</a></li>
                                <li><a href="/faq/seminars">Seminars</a></li>
                                <li><a href="/faq/webcasts">Webcasts</a></li>
                                <li><a href="/faq/online-training">Online Training</a></li>
                                <li><a href="/faq/purchases">Purchases</a></li>
                                <li><a href="/faq/answerline">Answerline</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Company
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="/about-us">About Us</a></li>
                                <li><a href="/our-team">Our Team</a></li>
                                <li><a href="/contact-us">Contact Us</a></li>
                            </ul>
                        </li>

                        <li class="navbar-menu-auth"><a class="" href="{{ url('/login') }}">Customer Login</a></li>
                    </ul>
                </div>
            </div>

            <div class="categories">
                <a href="{{ url('training/on-site-and-customized-training') }}" class="course-category onsite-training" data-tilt data-tilt-glare>
                    <div class="icon">
                        <img src="/img/erc/homepage/onsite-training.png" alt="Onsite Training">
                        <span>On-site</span>
                        <span>Training</span>
                    </div>
                </a>

                <a href="{{ route('courses') }}?type=3" class="course-category cbt" data-tilt data-tilt-glare>
                    <div class="icon">
                        <img src="/img/erc/homepage/cbt.png" alt="Onsite Training">
                        <span>Online</span>
                        <span>Training</span>
                    </div>
                </a>

                <a href="{{ route('courses') }}?type=1" class="course-category seminar" data-tilt data-tilt-glare>
                    <div class="icon">
                        <img src="/img/erc/homepage/seminar.png" alt="Onsite Training">
                        <span>Seminars</span>
                    </div>
                </a>

                <a href="{{ route('courses') }}?type=2" class="course-category webcast" data-tilt data-tilt-glare>
                    <div class="icon">
                        <img src="/img/erc/homepage/webcast.png" alt="Onsite Training">
                        <span>Webcasts</span>
                    </div>
                </a>

                <a href="{{ url('consulting/consulting') }}" class="course-category consulting" data-tilt data-tilt-glare>
                    <div class="icon">
                        <img src="/img/erc/homepage/consulting.png" alt="Onsite Training">
                        <span>Consulting</span>
                    </div>
                </a>
            </div>

            <div class="mid-content">
                <div class="testimonials">

                    <div class="customer">
                        <div class="cloud">
                            <blockquote>
                                <h3>
                                    Environmental Resource Center seminars are great! There’s no more efficient way to obtain refresher training.
                                </h3>
                            </blockquote>
                        </div>

                        <div class="author-container">
                            <div class="author">
                                <span class="name">- Emil T. Szymczak</span>
                                <span class="company">Exxon Mobil</span>
                            </div>
                        </div>
                    </div>

                    <div class="customer">
                        <div class="cloud">
                            <blockquote>
                                <h3>
                                    Outstanding. Captures attention. Barry was very thorough with the information. He was also very attentive to any and all questions asked. Very informative.
                                </h3>
                            </blockquote>
                        </div>

                        <div class="author-container">
                            <div class="author">
                                <span class="name">- Cory Starrett</span>
                                <span class="company">Stolthaven Houston, Inc.</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="promotion" id="promotion"></div>
            </div>

            <div class="pages-icon">
                <div class="page-icon">
                    <a href="/class_calendar">
                        <img src="/img/erc/homepage/schedule-classes.png" alt="Schedule of Classes">
                    </a>

                    <span class="title">
                        Schedule
                    </span>

                     <span class="title">
                        of Classes
                    </span>
                </div>

                <div class="page-icon">
                    <a href="/docs/compliance_calendar.pdf">
                        <img src="/img/erc/homepage/compliance-calendar.png" alt="Compliance Calendar">
                    </a>

                    <span class="title">
                        Compliance
                    </span>

                     <span class="title">
                        Calendar
                    </span>
                </div>

                <div class="page-icon videos">
                    <a href="https://www.youtube.com/channel/UC4BwgRQIsBi-T_VjOf29vJw" target="_blank">
                        <img src="/img/erc/homepage/videos.png" alt="Videos">
                    </a>

                    <span class="title">
                        Videos
                    </span>
                </div>

                <div class="page-icon">
                    <a href="http://care.ercweb.com/lists/?p=subscribe&id=2" target="_blank">
                        <img src="/img/erc/homepage/sign-up-newsletter.png" alt="Sign Up Newsletter">
                    </a>

                    <span class="title">
                        Sign up for
                    </span>

                     <span class="title">
                        Newsletters
                     </span>
                </div>
                <div class="page-icon">
                    <a href="#" class="live-chat-img" id="comm100-button-20">
                        <img class="live-chat-img" src="/img/erc/homepage/live-chat.png" alt="Live Chat">
                    </a>
                    <div id="live-chat" class="hidden"></div>

                    <span class="title">
                        Live Chat
                    </span>

                    <span class="title">
                        &nbsp;
                     </span>
                </div>
            </div>

            <div class="news" id="news_div"></div>
        </div>

        <div id="footer">
            <div class="row">
                <div class="col-sm-3">
<pre>
Environmental Resource Center &reg;
101 Center Pointe Drive
Cary, North Carolina 27513-5706</pre>
                </div>
                <div class="col-sm-6">
                    <ul>
                        <li><a href="/contact-us" >Contact Us</a></li>
                        <li><a href="" data-toggle="modal" class="ppButtonModal">Privacy Policy</a></li>
                        <li><a href="" data-toggle="modal" class="siteButtonModal">Sitemap</a></li>
                    </ul>
                    <p>
                        © {{ date('Y') }} Environmental Resource Center &reg;. All rights reserved.
                    </p>
                </div>
                <div class="col-sm-3">
                    <h3 class="phone"><a href="tel:+8005372372"><i class="fa fa-phone-square" aria-hidden="true"></i> 800-537-2372</a></h3>
                </div>
            </div>
        </div>
    </div>

    @include('includes.privacy_policy')
    @include('includes.sitemap')

    @section('javascripts-top')
        {!! HTML::script('vendor/common.js') !!}
        <script>
        (function(d, script) {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');

            var trident = ua.indexOf('Trident/');
            if ((msie > 0 || trident > 0) == false) {
                script = d.createElement('script');
                script.type = 'text/javascript';
                script.async = false;
                script.onload = function(){
                    // remote script has loaded
                    VanillaTilt.init(document.querySelector(".course-category"), {
                        scale: 1.05,
                        glare: true,
                        "max-glare": 0.3
                    });
                };
                script.src = window.location.origin + '/js/non-ie.js';
                //d.getElementsByTagName('head')[0].appendChild(script);
                d.body.append(script);
            }
        }(document));
        </script>
        <script type="text/javascript"
                src="https://www.google.com/recaptcha/api.js?hl=en"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    @show

    <script type="text/javascript">

        var Comm100API=Comm100API||{};(function(t){function e(e){var a=document.createElement("script"),c=document.getElementsByTagName("script")[0];a.type="text/javascript",a.async=!0,a.src=e+t.site_id,c.parentNode.insertBefore(a,c)}t.chat_buttons=t.chat_buttons||[],t.chat_buttons.push({code_plan:208,div_id:"live-chat"}),t.site_id=46669,t.main_code_plan=208,e("https://chatserver.comm100.com/livechat.ashx?siteId="),setTimeout(function(){t.loaded||e("https://hostedmax.comm100.com/chatserver/livechat.ashx?siteId=")},5e3)})(Comm100API||{});

        (function($) {

            getarticles();
            getpromos();

            $('.live-chat-img').on('click', function() {
                $('#live-chat').find('a').find('img').click();
            });

            $.get('/cp/session', function(data) {
                var msg = 'The coupon <strong>'+
                        data.coupon_code +
                        '</strong> will be applied on your next transaction.';
                if (data.has_coupon) {
                    $.notify({
                        icon: 'fa fa-info-circle',
                        message: msg}, {
                        placement: {
                            from: "bottom",
                            align: "center"
                        },
                        delay: 0 });
                }
            });

            function getarticles(){
              $.ajax({
                    type: 'get',
                    url: "{{ route('home.articles') }}",
                    success:function(data){
                        $('#news_div').html(data);
                        $('#news_mobile_div').html(data);
                        $('.news').slick({
                            dots: true,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                        });
                        $('.news-mobile').slick({
                            mobileFirst: true,
                            slidesToShow: 1,
                        });
                    }
              }); 
            }

            function getpromos(){
              $.ajax({
                    type: 'get',
                    url: "{{ route('home.promos') }}",
                    success:function(data){
                        $('#promotion').html(data);
                        $('#promotion_mobile').html(data);
                        $('.promotion').slick({
                            dots: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            autoplay: true,
                            autoplaySpeed: 3000,
                            mobileFirst: true,
                            lazyLoad: 'progressive',
                        });
                    }
              }); 
            }

            $('.cat').on('click', function() {
                window.location.href = $(this).data('url');
            });

            $('.read-article').on('click', function() {
                window.location.href = $(this).data('link');
            });

            $('.promotion-link').on('click', function() {
                window.location.href = $(this).data('link');
            });

            $('.ppButtonModal').on('click', function(e) {
                e.preventDefault();
                console.log('test');

                $('#ppModal').modal('show');
            });

            $('.siteButtonModal').on('click', function(e) {
                e.preventDefault();

                $('#siteModal').modal('show');
            });

            $('.fb-share').on('click', function(e) {
                e.preventDefault();

                FB.ui({
                    method: 'share',
                    href: decodeURIComponent($(this).data('href')),
                }, function(response){});
            });

            $('#global-search-btn').on('click', function() {
                searchQ();
            });

            $('#top-search').submit(function(){
                searchQ();
                return false;
            });

            function searchQ() {
                var searchQ = $('#global-search').val();

                if ($.trim(searchQ).length > 0) {
                    window.location.href = "{{ route('courses') . '?searchq='  }}" + searchQ;
                }
            }

            function detectIE() {
                var ua = window.navigator.userAgent;

                var msie = ua.indexOf('MSIE ');
                if (msie > 0) {
                    // IE 10 or older => return version number
                    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                }

                var trident = ua.indexOf('Trident/');
                if (trident > 0) {
                    // IE 11 => return version number
                    var rv = ua.indexOf('rv:');
                    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                }

                var edge = ua.indexOf('Edge/');
                if (edge > 0) {
                    // Edge (IE 12+) => return version number
                    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                }

                // other browser
                return false;
            }

            if(detectIE()) {
                /*
                const tilt = $('.categories > .course-category').tilt({
                    scale: 1.05,
                })
                */
            } else {
                /*
                const tilt = $('.categories > .course-category').tilt({
                    scale: 1.05,
                    glare: true,
                    maxGlare: .3,
                })
                */
            }

            $('#promotion').on('click', '.promotions-container', function() {
                window.location.href = $(this).data('href');
            });
        })(jQuery)
    </script>

    <noscript id="deferred-styles">
        {!! HTML::style('vendor/common_2.css') !!}
        {!! HTML::style(elixir('css/admin-lte-2.css')) !!}
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
    </noscript>
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>

</body>
</html>