

        @if (count($articles))

            @foreach ($articles as $article)
                <?php $link = isset($article['reg'])
                        ? '/regulations/show/' . $article['slug']
                        : '/tips/show/' . $article['slug'];?>

                <?php $default  = isset($article['reg'])
                    ? route('reg.photo', $article['id'])
                    : route('tip.photo', $article['id']); ?>

                <?php $path = isset($article['reg'])
                    ? '/img/erc/homepage/article-1.jpg'
                    : (isset($article['env'])
                        ? '/img/erc/homepage/article-2.jpg'
                        : '/img/erc/homepage/article-3.jpg' );?>

                <div class="article">
                    <a href="{{ $link }}">
                    <div class="header read-article" data-link="{{ $link }}" style="background-image: url({{ ($article['photo'] and $article['photo_mime']) ? $default : $path }})">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">{{ \Carbon\Carbon::parse($article['published_date'])->format('d') }}</span>
                                    <span class="month">{{ \Carbon\Carbon::parse($article['published_date'])->format('M') }}</span>
                                </div>
                                <div class="year">
                                    <span>{{ \Carbon\Carbon::parse($article['published_date'])->format('Y') }}</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    </a>

                    <div class="article-content">
                        <div class="title">
                            <h3 style="font-size: 16px;"><a href="{{ $link }}">{{ str_limit($article['title'], 70) }}</a></h3>
                        </div>

                        <div class="category" style="font-size: 14px;">
                            In <span>{{ $article['category'][0]['name'] }}</span>
                        </div>

                        <div class="story" style="font-size: 14px;">
                            {!! str_limit(strip_tags(html_entity_decode($article['content'])), 300, '...') !!}
                        </div>

                        <div class="footer">
                            <a href="{{ $link }}" class="read-more">Read More</a>

                            <div class="socials">
                                <a href="" class="fb-share" data-href="{{ route('home') . $link }}" data-title="{{ $article['title'] }}" data-desc="{{ strip_tags($article['content']) }}"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href="http://twitter.com/share?text={{ $article['title'] }}&url={{ route('home') . $link }}" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a class="linkedin customer share" href="http://www.linkedin.com/shareArticle?mini=true&url={{ route('home') . $link }}" title="{{ $article['title'] }}" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-1.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-2.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-3.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-1.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-2.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="article">
                <div class="header">
                    <div class="date-container">
                        <div class="date">
                            <div class="month-day">
                                <span class="day">10</span>
                                <span class="month">Nov</span>
                            </div>
                            <div class="year">
                                <span>2017</span>
                            </div>
                        </div>
                    </div>
                    <img src="/img/erc/homepage/article-3.jpg" alt="Article Photo">
                </div>

                <div class="article-content">
                    <div class="title">
                        <h3>This is a title of an article</h3>
                    </div>

                    <div class="category">
                        In <span>Category</span>
                    </div>

                    <div class="story">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                    </div>

                    <div class="footer">
                        <a href="" class="read-more">Read More</a>

                        <div class="socials">
                            <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
         @endif
