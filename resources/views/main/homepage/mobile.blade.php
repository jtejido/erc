<div class="main-container mobile-container">
	<div class="header">
        <nav class="navbar navbar-static-top top-nav">
            <div class="container top-nav-container">
                <div class="main-container col-md-10 col-md-offset-1">
                    <div class="navbar-header">
                        <div class="row">
                            <div class="contact">
                                800-537-2372
                            </div>
                        </div>

                        <div class="row">
                            <button type="button" class="navbar-toggle collapseds col-xs-2 pull-right" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <a class="brand col-xs-10" href="/">
                                <img class="mobile-size" src="/img/ERC-LOGO-V3-2.png" alt="Environmental Resource Center">
                            </a>

                            <div class="col-xs-2">

                            </div>
                        </div>
                    </div>

                    <div class="navbar-collapse collapse nav-collapse" id="navbar">

                        <ul class="nav navbar-nav">

                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Training
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('courses') }}">Course Listing</a></li>
                                    <li><a href="/training/benefits">Benefits</a></li>
                                    <li><a href="/training/on-site-and-customized-training">On-site and Customized Training</a></li>
                                    <li><a href="/training/subscription">Subscriptions</a></li>
                                    <li><a href="/training/gsa">GSA</a></li>
                                </ul>
                            </li>
                          
                         	<li>	
                               <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Consulting
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/consulting/consulting">Consulting</a></li>
                                    <li><a href="/consulting/audits">Audits</a></li>
                                    <li><a href="/consulting/permits">Permits</a></li>
                                    <li><a href="/consulting/hazardous-materialsdangerous-goods-classification">Hazardous Materials/Dangerous Goods Classification</a></li>
                                    <li><a href="/consulting/sara-title-ii-compliance-and-reporting">SARA Title III Compliance and Reporting</a></li>
                                    <li><a href="/consulting/plans-and-procedures">Plans and Procedures</a></li>
                                    <li><a href="/consulting/ehs-program-management">EHS Program Management</a></li>
                                    <li><a href="/consulting/safety-database-development">Safety Database Development</a></li>
                                    <li><a href="/consulting/hazardous-materials-databases">Hazardous Materials Databases</a></li>
                                    <li><a href="/consulting/ehs-on-site-staffing">EHS On-site Staffing</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="/products">Products</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Resources
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/regulations">Reg of the Day</a></li>
                                    <li><a href="/tips">Tips of the Week</a></li>
                                    <li><a href="{{ route('resource.classcalendar') }}">Schedule of Classes</a></li>
                                    <li><a href="/resources/helpful-links ">Helpful Links</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    FAQ
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/faq/who-needs-training">Who needs Training?</a></li>
                                    <li><a href="/faq/seminars">Seminars</a></li>
                                    <li><a href="/faq/webcasts">Webcasts</a></li>
                                    <li><a href="/faq/online-training">Online Training</a></li>
                                    <li><a href="/faq/purchases">Purchases</a></li>
                                    <li><a href="/faq/answerline">Answerline</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Company
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/about-us">About Us</a></li>
                                    <li><a href="/our-team">Our Team</a></li>
                                    <li><a href="/contact-us">Contact Us</a></li>
                                </ul>
                            </li>

                            <li class="navbar-menu-auth"><a class=" active " href="/login">Login/Register</a></li>

                    	</ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <div class="categories">
    	<div class="course-category cat onsite-training" data-url="{{ url('training/on-site-and-customized-training') }}">
    		
        </div>

        <div class="course-category cat cbt" data-url="{{ route('courses') }}?type=3">
			
        </div>

        <div class="course-category cat seminar" data-url="{{ route('courses') }}?type=1">
			
        </div>

        <div class="course-category cat webcast" data-url="{{ route('courses') }}?type=2">
			
        </div>

        <div class="course-category cat consulting" data-url="{{ url('consulting/consulting') }}">
			
        </div>
    </div>

    <div class="mid-content">
        <div class="testimonials">

            <div class="customer">
                <div class="cloud">
                   	<h3>
                        Environmental Resource Center seminars are great! There’s no more efficient way to obtain refresher training.
                    </h3>
                </div>

                <div class="author-container">
                    <div class="author">
                        <span class="name">- Emil T. Szymczak</span>
                        <span class="company">Exxon Mobil</span>
                    </div>
                </div>
            </div>

            <div class="customer">
                <div class="cloud">
                    <h3>
                        Outstanding. Captures attention. Barry was very thorough with the information. He was also very attentive to any and all questions asked. Very informative.
                    </h3>
                </div>

                <div class="author-container">
                    <div class="author">
                        <span class="name">- Cory Starrett</span>
                        <span class="company">Stolthaven Houston, Inc.</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="promotion" id="promotion_mobile"></div>
    </div>


    <div class="pages-icon">
    	<div class="top">
	        <div class="page-icon pull-left">
	            <a href="/class_calendar">
	                <img src="/img/erc/homepage/schedule-classes.png" alt="Schedule of Classes">
	            </a>

	            <span class="title">
	                Schedule
	            </span>

	             <span class="title">
	                of Classes
	            </span>
	        </div>

	        <div class="page-icon pull-right">
	            <a href="/docs/compliance_calendar.pdf">
	                <img src="/img/erc/homepage/compliance-calendar.png" alt="Compliance Calendar">
	            </a>

	            <span class="title">
	                Compliance
	            </span>

	             <span class="title">
	                Calendar
	            </span>
	        </div>
        </div>

        <div class="bottom">
	        <div class="page-icon videos pull-left">
                <a href="https://www.youtube.com/channel/UC4BwgRQIsBi-T_VjOf29vJw" target="_blank">
	                <img src="/img/erc/homepage/videos.png" alt="Videos">
	            </a>

	            <span class="title">
	                Videos
	            </span>
	        </div>

	        <div class="page-icon pull-right">
	            <a href="http://care.ercweb.com/lists/?p=subscribe&id=2" target="_blank">
	                <img src="/img/erc/homepage/sign-up-newsletter.png" alt="Sign Up Newsletter">
	            </a>

	            <span class="title">
	                Sign up for
	            </span>

	             <span class="title">
	                Newsletters
	             </span>
	        </div>

            <div class="page-icon pull-left">
                <a href="#" class="live-chat-img" id="comm100-button-20">
                    <img class="live-chat-img" src="/img/erc/homepage/live-chat.png" alt="Live Chat">
                </a>
                <div id="live-chat" class="hidden"></div>

                <span class="title">
	                Live Chat
	            </span>

                <span class="title">
	                &nbsp;
	             </span>
            </div>
        </div>
    </div>

    <div class="news-mobile" id="news_mobile_div"></div>

    <div id="footer">
        <div class="row">
            <div class="col-sm-4">
<pre>
Environmental Resource Center &reg;
101 Center Pointe Drive
Cary, North Carolina 27513-5706</pre>
            </div>
            <div class="col-sm-4">
                <ul>
                    <li><a href="/contact-us" >Contact Us</a></li>
                    <li><a href="" data-toggle="modal" class="ppButtonModal">Privacy Policy</a></li>
                    <li><a href="" data-toggle="modal" class="siteButtonModal">Sitemap</a></li>
                </ul>
                <p>
                    © {{ date('Y') }} Environmental Resource Center &reg;. All rights reserved.
                </p>
            </div>
            <div class="col-sm-4">
                <h3 class="phone"><a href="tel:+8005372372"><i class="fa fa-phone-square" aria-hidden="true"></i> 800-537-2372</a></h3>
            </div>
        </div>
    </div>
</div>