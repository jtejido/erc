@extends('emails.templates.template')

@section('body')
<p>Hello&nbsp;[name]!</p>
<p>You are up for certification for the following course:</p>
<p><strong>[course]</strong></p>
@include('emails.templates._login')
@endsection