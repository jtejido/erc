@extends('emails.templates.template')

@section('body')
<p>Dear [name],</p>

<p>You are scheduled to participate in the [webcast] Webcast on [dateRange]</p>

<p><strong>Prior to the Webcast</strong></p>
<ul>
    <li>You may test your connection by joining a test meeting at this link: <a href="http://www.webex.com/test-meeting.html">http://www.webex.com/test-meeting.html</a></li>
</ul>

<p><strong>Course Material</strong></p>
<ul>
    <li>Your course materials are now available on your User Portal.</li>
    <li>You may save the materials to your computer or print them out if you would like to refer to them during the training.</li>
</ul>

<p><strong>To Join the Webcast</strong><br/></p>
<ul>
    <li>Please login 10 to 15 minutes prior to the start of the webcast.</li>
    <li>To join the webcast, click here:<br/>Webcast Link: [webcast_link]</li>
</ul>

<p><strong>At the End of the Training</strong></p>
<ul>
    <li>To take your test and complete the course evaluation, click here:<br/>Exam Link - [exam_link]</li>
    <li>Your certificate will be emailed, and will be available on your User Portal, after you complete the steps at the Exam Link above.</li>
</ul>


<p>Looking forward to the training!</p>

@include('emails.templates._login')
@endsection