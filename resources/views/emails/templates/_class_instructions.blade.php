[if hasSeminar]
<p>[if hasMultipleClasses]<strong>Seminar Instruction</strong><br/>[/if]If you need overnight accommodations, please contact the hotel directly.&nbsp;[if hasClassInCary]For classes in Cary, please contact the Hampton Inn Raleigh, Cary at (919) 859-5559.[/if]The hotel offers a limited number of rooms at a discounted rate to Environmental Resource Center attendees. Parking fees may apply.</p>
[/if]

[if hasWebcast]
<p>[if hasMultipleClasses]<strong>Webcast Instruction</strong><br/>[/if]Instructions for accessing the webcast and course materials will be emailed to attendees the week prior to the training.</p>
<p>To participate in this live training attendees must have Internet access. Attendees may test their connection <a href="https://www.webex.com/test-meeting.html">here</a>. To view the technical requirements click on the following link: <a href="https://help.webex.com/docs/DOC-4748">https://help.webex.com/docs/DOC-4748.</a></p>
[/if]

[if hasCbt]
<p>[if hasMultipleClasses]<strong>Online Training Instruction</strong><br/>[/if]In order to access the training, user must first log into Environmental Resource Center&rsquo;s Health and Environmental Learning Program (H.E.L.P.&trade;).</p>
<p>To login to <strong>H.E.L.P.&trade;</strong> and begin your training, follow these steps: </p>
<ol>
    <li>
        <p>In a web browser, go to: https://learning.ercweb.com</p>
    </li>
    <li>
        [if hasCmid]
        <p>Login with your User ID: [courseMillUser]</p>
        [else]
        <p>We are creating a H.E.L.P.™ account for you. Login with your User ID and Password once you have received the account details on a separate email.</p>
        [/if]
    </li>
    [if hasCmid]
    <li>
        <p>
            Enter the password you previously set for your H.E.L.P.™ account. If this is a <strong>NEW</strong> account please use [courseMillUser] (case sensitive) as your temporary password.
        </p>
    </li>
    [/if]
    <li>
        <p>You will be directed to your H.E.L.P.&trade; dashboard where you can start your training, print your course materials, view your transcript, and participate in the course&rsquo;s Discussion Board.</p>
    </li>
</ol>

<p>Your enrollment will remain open until you complete the course or 90 days, whichever comes first. You will receive your course certificate via email when you complete the course.</p>

<p>If you need assistance, please call 800-537-2372 or send an email to <a href="mailto:help@ercweb.com">help@ercweb.com.</a><br /><br/> Thank you for choosing Environmental Resource Center.</p>
[/if]