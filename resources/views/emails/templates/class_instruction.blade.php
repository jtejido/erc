@extends('emails.templates.template')

@section('body')
[user]
<p>[fullname]<br />[company]<br />[address1]<br />[address2]</p>
<p>Dear [name],</p>
[/user]

[if hasMultipleClasses]
<p>Thank you for registering personnel for the following training:</p>
[/if]

[orderItems]
@include('emails.templates._class_seminar')
@include('emails.templates._class_webcast')
@include('emails.templates._class_cbt')

[/orderItems]

@include('emails.templates._class_instructions')

<p>
    <strong>Note</strong><br/>
    [if hasOtherAttendees]An email has been sent to each attendee with detailed information on the training.<br/>[/if]
    If you need training on other topics or information about upgrading to an Annual Training Subscription, please call 800-537-2372. The subscription allows you to attend all Environmental Resource Center seminars and webcasts for one year at one low price. The Annual Subscription does not include online training.
</p>

<p>Thank you for choosing Environmental Resource Center.</p>

<p>Sincerely,</p>
<p>Jenna Lewis<br /> Environmental Resource Center<br /> jlewis@environmentalresourcecenter.com</p>
[if hasSeminar]
<p><hr/><strong>Cancellation Policy: </strong> If you cancel or reschedule less than 7 days prior to a seminar, a fee of $50 per seminar will apply. If you cancel on or after the date of the seminar, no refund will be provided, however, you may reschedule your training during the next 12 months subject to the $50 per seminar fee. We reserve the right to cancel any seminar and refund all registration fees.</p>
[/if]
@endsection