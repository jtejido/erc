@extends('emails.templates.template')

@section('body')
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Hello&nbsp;[name]!</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Details of your On-Site Training on [date] at [address] has been updated.</p>
@include('emails.templates._login')
@endsection