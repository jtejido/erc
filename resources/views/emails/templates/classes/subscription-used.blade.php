@extends('emails.templates.template')

@section('body')
<p>Hi [name]!</p>

[if hasCreditSubscription]
<p>Corporate seat subscriptions were used for the following registrations:</p>
<ul>
    [creditSubscription]
    <li>[title]</li>
    [/creditSubscription]
</ul>
[/if]
<p>Thank you for choosing Environmental Resource Center.</p>
<p>
    Sincerely,<br/>
    <strong>Jenna Lewis</strong><br>
    Environmental Resource Center<br>
    jlewis@environmentalresourcecenter.com<br>
</p>
@endsection