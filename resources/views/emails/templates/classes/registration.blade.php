@extends('emails.templates.template')

@section('body')
[user]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[fullname]<br />[company]<br />[address1]<br />[address2]</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Dear [name]:</p>
[/user] [if isClass] [if isSeminar]
[if isAttendee]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for registering for [name] on [dateRange] in [locationGeneral].</p>
[else]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for registering personnel for [name] on [dateRange] in [locationGeneral].</p>
[/if]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[if isCary] The seminar will be held at Environmental Resource Center, located at 101 Center Pointe Dr. in Cary. The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime].<br/><br/> If you need overnight accommodations, please contact Hampton Inn Raleigh, Cary directly at (919) 859-5559. The hotel offers a limited number of rooms at a discounted rate to Environmental Resource Center attendees. Parking fees may apply.
    [else] The seminar will be held at [locationName], located at [locationAddress]. The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime]. <br/><br/> If you need overnight accommodations, please contact the hotel directly at [locationPhone]. The hotel offers a limited number of rooms at a discounted rate to Environmental Resource Center attendees. Parking fees may apply. [/if]</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">If you need training on other topics or information about upgrading to an Annual Training Subscription, please call 800-537-2372. The subscription allows you to attend all Environmental Resource Center sem for one year at one low price.</p>
[/if] [if isWebinar]
[if isAttendee]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for registering for the [name] on [startDate] from [startTime] to [endTime].</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Instructions for accessing the webcast and course materials will be emailed to you the week prior to the training.</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">To participate in this live training you must have Internet access. You may test your connection <a href="https://www.webex.com/test-meeting.html">here</a>. To view the technical requirements click on the following link: <a href="https://help.webex.com/docs/DOC-4748">https://help.webex.com/docs/DOC-4748.</a></p>
[else]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for registering personnel for the [name] on [startDate] from [startTime] to [endTime].</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Instructions for accessing the webcast and course materials will be emailed to attendees the week prior to the training.</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">To participate in this live training attendees must have Internet access. Attendees may test their connection <a href="https://www.webex.com/test-meeting.html">here</a>. To view the technical requirements click on the following link: <a href="https://help.webex.com/docs/DOC-4748">https://help.webex.com/docs/DOC-4748.</a></p>
[/if]
[/if]
[if hasCourse]

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">You may also be interested in the following course(s):</p>
[courses]
<ul>
    <li>
        <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
         <a href="[courseLink]" target="_blank">[title]</a>
        </p>
    </li>
</ul>
[/courses]
[/if]
[if isAttendee]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">We appreciate your business and look forward to having you in class.</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Click <a href="https://facebook.com/sharer/sharer.php?u=[fbUrl]&amp;title=I'll Be Attending This Class" target="_blank">here</a> to let your Facebook friends know that you will be attending this class.</p>
[else]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">We appreciate your business.</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Click <a href="https://facebook.com/sharer/sharer.php?u=[fbUrl]&amp;title=I'll Be Attending This Class" target="_blank">here</a> to let your Facebook friends know that your co-workers will be attending this class.</p>
[/if]


[else] [if isOnline]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for purchasing [name] online training. In order to access your training, you must first log into Environmental Resource Center&rsquo;s Health and Environmental Learning Program (H.E.L.P.&trade;).</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">To login to <strong>H.E.L.P.&trade;</strong> and begin your training, follow these steps: </p>
<ol>
    <li>
        <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">In a web browser, go to: http://learning.ercweb.com</p>
    </li>
    <li>
        <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Login with your User ID: [courseMillUser]</p>
    </li>
    <li>
        <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
            Enter the password you previously set for your H.E.L.P.™ account. If this is a <strong>NEW</strong> account please use [courseMillUser] (case sensitive) as your temporary password.
        </p>
    </li>
    <li>
        <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">You will be directed to your H.E.L.P.&trade; dashboard where you can start your training, print your course materials, view your transcript, and participate in the course&rsquo;s Discussion Board.</p>
    </li>
</ol>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Your enrollment will remain open until you complete the course or 90 days, whichever comes first. You will receive your course certificate via email when you complete the course.</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">If you need assistance, please call 800-537-2372 or send an email to <a href="mailto:help@ercweb.com">help@ercweb.com.</a><br /><br/> Thank you for choosing Environmental Resource Center.</p>

[/if] [/if]

[if hasProduct]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Here is the list of products you purchased.</p>

<table border="0" cellpadding="10" cellspacing="0" width="100%">
    <tbody>
    [products]
    <tr>
        <td width="165" class="image-container">
            <div style="max-width: 165px; max-height: 180px">[book_whimglink]</div>
        </td>
        <td width="315">
            <div style="max-width: 315px;">
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.6; color: #000000; text-align: start; font-weight: bold;">[name]</p>
            <p style="word-break: break-all;white-space: normal;"><span style="color: #333; font-size: 12px;">[price]</span> <span style="color: #333; font-size: 12px;">[qty]</span></p>
            </div>
        </td>
    </tr>
    [/products]
    </tbody>
</table>
[/if]

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Sincerely,</p>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Jenna Lewis<br /> Environmental Resource Center<br /> jlewis@environmentalresourcecenter.com</p>
[if isClass][if isPolicy] <br /> <br />
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;"><strong>Cancellation Policy: </strong> If you cancel or reschedule less than 7 days prior to a seminar, a fee of $50 per seminar will apply. If you cancel on or after the date of the seminar, no refund will be provided, however, you may reschedule your training during the next 12 months subject to the $50 per seminar fee. We reserve the right to cancel any seminar and refund all registration fees.</p>
[/if][/if]

@include('emails.templates._login')

@endsection