@extends('emails.templates.template')

@section('body')
    [user]
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[fullname]<br />[company]<br />[address1]<br />[address2]</p>
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Dear [name]:</p>
    [/user]

    [if isAttendee]

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for purchasing [name] online training. In order to access your training, you must first log into Environmental Resource Center&rsquo;s Health and Environmental Learning Program (H.E.L.P.&trade;).</p>

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">To login to <strong>H.E.L.P.&trade;</strong> and begin your training, follow these steps: </p>
    <ol>
        <li>
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">In a web browser, go to: http://learning.ercweb.com</p>
        </li>
        <li>
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Login with your User ID: [courseMillUser]</p>
        </li>
        <li>
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
                Enter the password you previously set for your H.E.L.P.™ account. If this is a <strong>NEW</strong> account please use [courseMillUser] (case sensitive) as your temporary password.
            </p>
        </li>
        <li>
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">You will be directed to your H.E.L.P.&trade; dashboard where you can start your training, print your course materials, view your transcript, and participate in the course&rsquo;s Discussion Board.</p>
        </li>
    </ol>

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Your enrollment will remain open until you complete the course or 90 days, whichever comes first. You will receive your course certificate via email when you complete the course.</p>

    [else]

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Thank you for purchasing [name] online training. In order to access your training, you must first log into Environmental Resource Center&rsquo;s Health and Environmental Learning Program (H.E.L.P.&trade;).</p>
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Following this email is a login instructions that will be sent out to all the attendees you have registered.</p>

    [/if]

    [if hasCourse]

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">You may also be interested in the following course(s):</p>
    [courses]
    <ul>
        <li>
            <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
                <a href="[courseLink]" target="_blank">[title]</a>
            </p>
        </li>
    </ul>
    [/courses]
    [/if]

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">If you need assistance, please call 800-537-2372 or send an email to <a href="mailto:help@ercweb.com">help@ercweb.com.</a><br /><br/> Thank you for choosing Environmental Resource Center.</p>

    [if hasProduct]
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Here is the list of products you purchased.</p>

    <table border="0" cellpadding="10" cellspacing="0" width="100%">
        <tbody>
        [products]
        <tr>
            <td width="165" class="image-container">
                <div style="max-width: 165px; max-height: 180px">[book_whimglink]</div>
            </td>
            <td width="315">
                <div style="max-width: 315px;">
                    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.6; color: #000000; text-align: start; font-weight: bold;">[name]</p>
                    <p style="word-break: break-all;white-space: normal;"><span style="color: #333; font-size: 12px;">[price]</span> <span style="color: #333; font-size: 12px;">[qty]</span></p>
                </div>
            </td>
        </tr>
        [/products]
        </tbody>
    </table>
    [/if]

    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Sincerely,</p>
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Jenna Lewis<br /> Environmental Resource Center<br /> jlewis@environmentalresourcecenter.com</p>
    [if isClass][if isPolicy] <br /> <br />
    <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;"><strong>Cancellation Policy: </strong> If you cancel or reschedule less than 7 days prior to a seminar, a fee of $50 per seminar will apply. If you cancel on or after the date of the seminar, no refund will be provided, however, you may reschedule your training during the next 12 months subject to the $50 per seminar fee. We reserve the right to cancel any seminar and refund all registration fees.</p>
    [/if][/if]

    @include('emails.templates._login')

@endsection