@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>
<p>You have been registered for on-site Training with Environmental Resource Center on [date].
    The training will take place at [address].</p>
@include('emails.templates._login')
@endsection