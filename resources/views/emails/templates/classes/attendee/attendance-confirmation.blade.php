@extends('emails.templates.template')

@section('body')
<p>[fullname]<br/>[company]<br/>[address1]<br/>[address2]</p>
<p>Dear [name],</p>
<p>Thank you for attending the following training with Environmental Resource Center:</p>

<ul>
        [class]
        <li>[name]</li>
        [/class]
</ul>
<p>Your certificate is attached.</p>

<p>As part of our ongoing commitment to keep you up to date with the latest regulations, you will receive the following services over the next year:</p>
<ul>
        <li>Free Handbook Updates: As the regulations change, we will update your handbooks with the latest information. Handbook updates may be downloaded from your User Portal.</li>
        <li>Free Answerline: If you have questions that relate to your course topic, call 800-537-2372 or email your questions to <a href="mailto:service@ercweb.com">service@ercweb.com</a>.</li>
</ul>

<p>You can also subscribe to our free email newsletters that cover environmental and safety news and tips at <a href="http://care.ercweb.com">care.ercweb.com</a>.</p>

@include('emails.templates._login')
        
@endsection