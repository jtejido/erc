@extends('emails.templates.template')

@section('body')
<p>[name]<br/>[company]<br/>[address1]<br/>[address2]</p>

<p>
    We'd like to inform you that the handbook for the course below was recently updated, and may be downloaded from your User Portal.
</p>
<ul>
    <li>[course_name]</li>
</ul>

@include('emails.templates._login')
@endsection