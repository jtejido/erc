@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>

[class]

<p>
    This is to inform you that you have been [nominated] as an attendee from the following training course:
</p>

<ul>
    [if type=seminar]
    <li>[name] - [city], [state] on [start]</li>
    [/if]
    [if type=webinar]
    <li>[name] on [start]</li>
    [/if]
    [if type=online]
    <li>[name]</li>
    [/if]
</ul>
[/class]

<p>
    Need Help?<br/>
    Please contact us at 800-537-2372 or <a href="mailto:service@ercweb.com">service@ercweb.com</a> for assistance.
</p>

@include('emails.templates._login')
@endsection