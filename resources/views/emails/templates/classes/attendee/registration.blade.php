@extends('emails.templates.template')

@section('body')
[user]
<p>[fullname]<br />[company]<br />[address1]<br />[address2]</p>
<p>Dear [name],</p>
[/user]

[if isClass]
[if isSeminar]
<p>
    [agent_name] has registered you for [className] course with Environmental Resource Center on [dateRange] in [locationGeneral].
</p>

<p>
    [if isCary]
    The seminar will be held at Environmental Resource Center, located at 101 Center Pointe Dr. in Cary.
    The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime].
    <br/><br/>
    If you need overnight accommodations, please contact Hampton Inn Raleigh, Cary directly at (919) 859-5559. The hotel offers a limited number of rooms at a discounted rate to Environmental Resource Center attendees. Parking fees may apply.
    [else]
    The seminar will be held at [locationName], located at [locationAddress]. The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime].
    <br/><br/>
    If you need overnight accommodations, please contact the hotel directly at [locationPhone]. The hotel offers a limited number of rooms at a discounted rate to Environmental Resource Center attendees.
    Parking fees may apply.
    [/if]
</p>

<p>
    If you need training on other topics or information about upgrading to an Annual Training Subscription, please call 800-537-2372.
    The subscription allows you to attend all Environmental Resource Center seminars and webcasts for one year at one low price. Online training is not included in the subscription rate but can be purchased separately.
</p>

<p>
    We appreciate your business and look forward to seeing you in class.
</p>

<p>
    Click <a href="https://facebook.com/sharer/sharer.php?u=[fbUrl]&title=I'll Be Attending This Class" target="_blank">here</a> to let your Facebook friends know that you will be attending this class.
</p>
[/if]

[if isWebinar]
<p>
    [agent_name] has registered you for [className] course with Environmental Resource Center on [startDate] from [startTime] to [endTime].
</p>

<p>
    Instructions for accessing the webcast and course materials will be emailed to you the week prior to the training.
</p>

<p>
    To participate in this live training you must have Internet access.  You may test your connection <a href="https://www.webex.com/test-meeting.html">here</a>.
    To view the technical requirements click on the following link: <a href="https://help.webex.com/docs/DOC-4748">https://help.webex.com/docs/DOC-4748.</a>
</p>

<p>
    We appreciate your business and look forward to having you in class.
</p>

<p>
    Click <a href="https://web.facebook.com/sharer/sharer.php?u=[fbUrl]&title=I'll Be Attending This Class" target="_blank">here</a> to let your Facebook friends know that you will be attending this class.
</p>
[/if]
[else]
[if isOnline]
<p>
    [agent_name] has registered you for [className] online training. In order to access your training, you must first log into Environmental Resource Center’s Health and Environmental Learning Program (H.E.L.P.™).
</p>

<p>To login to <strong>H.E.L.P.&trade;</strong> and begin your training, follow these steps: </p>

<ol>
    <li>
        <p>In a web browser, go to: https://learning.ercweb.com</p>
    </li>
    <li>
        [if hasCmid]
        <p>Login with your User ID: [courseMillUser]</p>
        [else]
        <p>We are creating a H.E.L.P.™ account for you. Login with your User ID and Password once you have received the account details on a separate email.</p>
        [/if]
    </li>
    [if hasCmid]
    <li>
        <p>
            Enter the password you previously set for your H.E.L.P.™ account. If this is a <strong>NEW</strong> account please use [courseMillUser] (case sensitive) as your temporary password.
        </p>
    </li>
    [/if]
    <li>
        <p>You will be directed to your H.E.L.P.&trade; dashboard where you can start your training, print your course materials, view your transcript, and participate in the course&rsquo;s Discussion Board.</p>
    </li>
</ol>

<p>Your enrollment will remain open until you complete the course or 90 days, whichever comes first. You will receive your course certificate via email when you complete the course.</p>

<p>
    If you need assistance, please call 800-537-2372 or send an email to <a href="mailto:help@ercweb.com">help@ercweb.com.</a><br/><br/>
    Thank you for choosing Environmental Resource Center.
</p>

[/if]
[/if]

<p>Sincerely,</p>
<p>
    Jenna Lewis<br>
    Environmental Resource Center<br>
    jlewis@environmentalresourcecenter.com<br>
</p>

[if isClass][if isPolicy]
<p>
    <strong>Cancellation Policy: </strong>
    If you cancel or reschedule less than 7 days prior to a seminar, a fee of $50 per seminar will apply.  If you cancel on or after the date of the seminar, no refund will be provided, however, you may reschedule your training during the next 12 months subject to the $50 per seminar fee.
    We reserve the right to cancel any seminar and refund all registration fees.
</p>
[/if][/if]
@include('emails.templates._login')
@endsection