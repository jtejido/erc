@extends('emails.templates.template')

@section('body')
<p>Rescheduling was processed and approved by [csrName] (Customer Service Representative).</p>
<p>
The registration for <strong>[name]</strong> to <strong>[courseName]</strong> on <strong>[oldDate]</strong>
    has been rescheduled to <strong>[newDate]</strong>.
</p>
<p>Our accounting department will provide a revised receipt separately, as needed.</p>
@endsection