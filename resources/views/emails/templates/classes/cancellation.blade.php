@extends('emails.templates.template')

@section('body')
<p>Dear [name],</p>
[class]
[if type=seminar]
<p>This is to inform you that your training [name] in [city], [state], on [start_date] has been cancelled. An Environmental Resource Center representative will be in touch to discuss rescheduling options.</p>
[/if]
[if type=webinar]
<p>This is to inform you that your training [name] on [start_date] has been cancelled. An Environmental Resource Center representative will be in touch to discuss rescheduling options.</p>
[/if]
[if type=online]
<p>This is to inform you that your training [name] has been cancelled. An Environmental Resource Center representative will be in touch to discuss rescheduling options.</p>
[/if]
[/class]

@include('emails.templates._login')
@endsection