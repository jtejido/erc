@extends('emails.templates.template')

@section('body')
[user]
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[fullname]<br style="margin: 0px; padding: 0px;" />[company]<br style="margin: 0px; padding: 0px;" />[address1]<br style="margin: 0px; padding: 0px;" />[address2]</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Dear [name]: </p>

[/user]

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    Thank you for registering the following student(s) for [if isOnline]Online Training for [name].[/if][if isClass][cname] on [if isWebinar][startDate] from [startTime] to [endTime].[/if][if isSeminar][dateRange] in [locationGeneral].[/if][/if]
</p>

<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    [attendee]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;">[name]</li>
    [/attendee]
</ul>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    An email has been sent to each attendee with detailed information on the training.  If you have any questions, please contact us at 800-537-2372 or service@environmentalresourcecenter.com.
</p>


<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    Sincerely,
</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    Jenna Lewis<br>
    Environmental Resource Center<br>
    jlewis@environmentalresourcecenter.com<br>
</p>

[if isClass][if isPolicy]
<hr/>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    <strong>Cancellation Policy: </strong>
    If you cancel or reschedule less than 7 days prior to a seminar, a fee of $50 per seminar will apply.  If you cancel on or after the date of the seminar, no refund will be provided, however, you may reschedule your training during the next 12 months subject to the $50 per seminar fee.
    We reserve the right to cancel any seminar and refund all registration fees.
</p>
[/if][/if]

@include('emails.templates._login')
@endsection