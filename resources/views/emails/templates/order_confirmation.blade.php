@extends('emails.templates.template')

@section('body')
[user]
<p>[fullname]<br />[company]<br />[address1]<br />[address2]</p>
<p>Dear [name],</p>
[/user]
<p>Thank you for your order placed on <strong>[orderDate]</strong> via <strong>[paymentMethod]</strong>.</p>
<p>
    <strong>Order Details:</strong>
</p>
<table border="1" style="border: 1px solid #AAA; font-size: 14px">
    <tbody>
    [transactions]
    <tr>
        <td style="padding: 5px" width="70%">[item]</td>
        <td style="padding: 5px" width="15%">[qty]</td>
        <td style="padding: 5px" width="15%">[price]</td>
    </tr>
    [/transactions]
    <tr>
        <td rowspan="5" style="padding: 5px">
            We are incorporated; EIN# 52-1215051 <br/>
            Contact Christie Brooks with questions regarding this document (919-469-1585 x 270 or <br/> cbrooks@ercweb.com).</td>
        <td class="text-center" style="padding: 5px">Shipping</td>
        <td class="text-center" style="padding: 5px" colspan="2">[shippingFee]</td>
    </tr>

    <tr>
        <td class="text-center" style="padding: 5px">Sales Tax</td>
        <td class="text-center" style="padding: 5px" colspan="2">[stateTax]</td>
    </tr>

    <tr>
        <td class="text-center" style="padding: 5px">Payment Via</td>
        <td class="text-center" style="padding: 5px" colspan="2">[paymentType]</td>
    </tr>

    <tr>
        [if isInvoiced]
        <td class="text-center" style="padding: 5px">PO #</td>
        <td class="text-center" style="padding: 5px" colspan="2">[poNum]</td>
        [else]
        [if isCheck]
        <td class="text-center" style="padding: 5px">Check #</td>
        [else]
        <td class="text-center" style="padding: 5px">Credit Card #</td>
        [/if]
        <td class="text-center" style="padding: 5px" colspan="2">[ccNum]</td>
        [/if]
    </tr>

    <tr>
        <td class="text-center" style="padding: 5px"><strong>Total</strong></td>
        <td class="text-center" style="padding: 5px" colspan="2">[totalPaid]</td>
    </tr>
    </tbody>
</table>
<p>
    <strong>Need Help?</strong><br/>
    Please contact us at 800-537-2372 or <a href="mailto:service@ercweb.com">service@ercweb.com</a> for assistance.
</p>
<p>Sincerely,</p>
<p>Jenna Lewis<br /> Environmental Resource Center<br /> jlewis@environmentalresourcecenter.com</p>
@endsection