[if isOnline]
[if hasMultipleClasses]
<p><strong>[name]</strong> online training. You have registered the following attendee(s):</p>
[else]
<p>Thank you for purchasing [name] online training. You have registered the following attendee(s):</p>
[/if]
@include('emails.templates._attendees')
[/if]