@extends('emails.templates.template')

@section('body')
[user]
<p>Dear&nbsp;[name]!</p>
<p>This is just to inform you that&nbsp;[agent]&nbsp;has created an account for you with Environmental Resource Center.</p>
<p>Use the following email address and temporary password to access your account:</p>
<ul>
    <li>Email : [email]</li>
    <li>Temporary Password : [password]</li>
</ul>
[/user]

<p>When you sign in to your account, you will be able to:</p>
<ul>
    <li>Proceed through checkout faster</li>
    <li>Check the status of&nbsp;trainings</li>
    <li>View&nbsp;available and previous&nbsp;classes/orders</li>
    <li>Print certificates</li>
</ul>

<p>Click <a href="{{ env('APP_URL').'/courses' }}">here</a> to see a complete list of our courses. You can register by phone, email or online.</p>
<p>To login to our website:&nbsp;<a href="{{ env('APP_URL').'/login' }}">Click here</a></p>
<p>Thank you for choosing Environmental Resource Center. We look forward to seeing you in class.</p>

@endsection