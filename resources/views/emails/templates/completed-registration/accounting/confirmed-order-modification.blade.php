@extends('emails.templates.template')

@section('body')
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    An order modification was made and approved by [csr][if isCSR]- CSR</span>[/if].
</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">The following are the order modifications.</p>

[adjustments]
<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;">[note]</li>
</ul>
[/adjustments]


[if additional_fee]
<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Additional Fees To Be Paid :</strong> $[additional_fee]</p>
[/if]

[if deduction]
<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Deductions : </strong> $[deduction]</p>
[/if]

[if refund]
<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Amount To Refund : </strong> $[refund]</p>
[/if]


[if hasPayment]

[if isInvoice]
[payment]
<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Purchase Order : </strong>
    [if has_po_image]
</p>
<table border="0" cellspacing="0" width="100%">
    <tr>
        <td></td>
        <td width="300"><div style="max-width:300px; margin:0 auto;">[po_imglink]</div></td>
        <td></td>
    </tr>
</table>

[else]
<p>No Purchase Order</p>
[/if]

<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Purchase Order Number : </strong> [po_number]</p>


<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Cheque : </strong>
    [if has_cheque_image]
</p>
<table border="0" cellspacing="0" width="100%">
    <tr>
        <td></td>
        <td width="300"><div style="max-width:300px; margin:0 auto;">[cheque_imglink]</div></td>
        <td></td>
    </tr>
</table>
[else]
<p>No Cheque</p>
[/if]

<p style="color: #000000; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; text-align: start; line-height: 1.6; font-size: 14px;"><strong>Cheque Number : </strong> [cheque_number]</p>
[/payment]

[/if]

[/if]
@endsection