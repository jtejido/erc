@extends('emails.templates.template')

@section('body')
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Cancellation was done [if is_csr] and approved [/if] by [user] [if is_csr] (CSR) [/if].</p>

<br>
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">The following are the cancelled registrations.</p>
[registrations]
<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;">[attendee] ([class])</li>
</ul>
[/registrations]

<br>

<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Amount Paid For Cancelled Registrations :</strong> $[paid]</li>

    [if is_deferredHalfPriceFee]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Cancelled Half Price Discount Fee :</strong> $[deferredHalfPriceFee]</li>
    [/if]

    [if is_deferredGroupDiscountFee]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Cancelled Group Discount Fee (HWM-DOT) :</strong> $[deferredGroupDiscountFee]</li>
    [/if]

    [if is_additionalFees]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Additional Fees :</strong> $[additional]</li>
    [/if]

    [if is_deductions]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Deductions :</strong> $[deduction]</li>
    [/if]

    [if is_fee]
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Amount Charged By [csr] (CSR) : </strong> $[fee]</li>
    [/if]

    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>Amount To Refund : </strong> $[refund]</li>

    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;"><strong>New Order Total :</strong> $[total]</li>
</ul>

@endsection