@extends('emails.templates.template')

@section('body')
<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">Per request, we have updated the registrations of the following students in [title].</p>

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    These students have been removed:
</p>
[rcontacts]
<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;">[attendee]</li>
</ul>
[/rcontacts]

<p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">
    These students have been added:
</p>

[acontacts]
<ul style="margin: 0px 0px 10px; padding: 0px; font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000;">
    <li style="margin: 0px 0px 0px 25px; padding: 0px; list-style-position: inside;">[attendee]</li>
</ul>
[/acontacts]

@include('emails.templates._login')
@endsection