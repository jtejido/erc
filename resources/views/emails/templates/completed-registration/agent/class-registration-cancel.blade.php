@extends('emails.templates.template')

@section('body')
[if isMember]
<p>Dear [csrName]:</p>
[else]
<p>Cancellation was processed and approved by [csrName] (Customer Service Representative).</p>
[/if]
<p>The following registrations have been cancelled:</p>
<ul>
    [registrations]
    <li>[attendee] ([class])</li>
    [/registrations]
</ul>
<p>Our accounting department will provide a revised receipt separately, as needed.</p>
@endsection