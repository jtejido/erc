@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>
<p>We would like to inform you that [actor] has swapped attendees for [title].</p>

<p>The following attendees have been removed:</p>
[rcontacts]
<ul>
    <li>[attendee]</li>
</ul>
[/rcontacts]

<p>The following attendees have been added:</p>

[acontacts]
<ul>
    <li>[attendee]</li>
</ul>
[/acontacts]

@include('emails.templates._login')
@endsection