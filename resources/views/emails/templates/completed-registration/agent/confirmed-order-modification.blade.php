@extends('emails.templates.template')

@section('body')
<p>
    An order modification was made and approved by [csr][if isCSR]- Customer Service Representative[/if].
</p>

<p>The following order modification(s) have been applied:</p>

<ul>
    [adjustments]
    <li>[note]</li>
    [/adjustments]
</ul>


[if additional_fee]
<p><strong>Additional Fees To Be Paid :</strong> $[additional_fee]</p>
[/if]

[if deduction]
<p><strong>Deductions : </strong> $[deduction]</p>
[/if]

[if refund]
<p><strong>Amount To Refund : </strong> $[refund]</p>
[/if]


[if hasPayment]

[if isInvoice]
[payment]
<p><strong>Purchase Order : </strong></p>
[if has_po_image]
<table border="0" cellspacing="0" width="100%">
    <tr>
        <td></td>
        <td width="300"><div style="max-width:300px; margin:0 auto;">[po_imglink]</div></td>
        <td></td>
    </tr>
</table>
[else]
<p>No Purchase Order</p>
[/if]

<p><strong>Purchase Order Number : </strong> [po_number]</p>


<p><strong>Cheque : </strong></p>
[if has_cheque_image]
<table border="0" cellspacing="0" width="100%">
    <tr>
        <td></td>
        <td width="300"><div style="max-width:300px; margin:0 auto;">[cheque_imglink]</div></td>
        <td></td>
    </tr>
</table>
[else]
<p>No Cheque</p>
[/if]

<p><strong>Cheque Number : </strong> [cheque_number]</p>
[/payment]

[/if]

[/if]
@endsection