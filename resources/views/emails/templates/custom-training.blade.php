<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>{{ $inputs['subject']}} </title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style type="text/css">
        /* /\/\/\/\/\/\/\/\/ CLIENT-SPECIFIC STYLES /\/\/\/\/\/\/\/\/ */
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
        body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

        /* /\/\/\/\/\/\/\/\/ RESET STYLES /\/\/\/\/\/\/\/\/ */
        body{margin:0; padding:0; font-family: "Roboto", Helvetica, Arial, sans-serif;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;-ms-interpolation-mode:bicubic;}
        body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}
        #bodyTable  {
            /*@editable*/
            width: 600px;
            max-width: 100%;
            margin: auto;
            padding: 0;
        }
         table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }
        /* Media Queries */
        @media screen and (max-width: 600px) {
            /* What it does: Adjust typography on small screens to improve readability */
			.email-container p {
				font-size: 17px !important;
				line-height: 22px !important;
			}
		}
        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
	        display: none !important;
	        opacity: 0.01 !important;
        }
        /* /\/\/\/\/\/\/\/\/ TEMPLATE STYLES /\/\/\/\/\/\/\/\/ */

        /* ========== Page Styles ========== */

        #bodyCell{padding:20px;}
        #templateContainer{width:600px;}

        /**
        * @tab Page
				* @sec background style
				* @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
				* @theme page
				*/
        body, #bodyTable{
            /*@editable*/ background-color:#DEE0E2;
        }

        /**
        * @tab Page
				* @sec background style
				* @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
				* @theme page
				*/
        #bodyCell{
            /*@editable*/ border-top:4px solid #BBBBBB;
        }

        /**
        * @tab Page
				* @sec email border
				* @tip Set the border for your email.
				*/
        #templateContainer{
            /*@editable*/ border: 0px solid #BBBBBB;
        }

        /**
        * @tab Page
				* @sec heading 1
				* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
				* @style heading 1
				*/
        h1{
            /*@editable*/ color:#202020 !important;
            display:block;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:26px;
            /*@editable*/ font-style:normal;
            /*@editable*/ font-weight:bold;
            /*@editable*/ line-height:100%;
            /*@editable*/ letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Page
				* @sec heading 2
				* @tip Set the styling for all second-level headings in your emails.
				* @style heading 2
				*/
        h2{
            /*@editable*/ color:#404040 !important;
            display:block;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:20px;
            /*@editable*/ font-style:normal;
            /*@editable*/ font-weight:bold;
            /*@editable*/ line-height:100%;
            /*@editable*/ letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Page
				* @sec heading 3
				* @tip Set the styling for all third-level headings in your emails.
				* @style heading 3
				*/
        h3{
            /*@editable*/ color:#606060 !important;
            display:block;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:16px;
            /*@editable*/ font-style:italic;
            /*@editable*/ font-weight:normal;
            /*@editable*/ line-height:100%;
            /*@editable*/ letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Page
				* @sec heading 4
				* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
				* @style heading 4
				*/
        h4{
            /*@editable*/ color:#808080 !important;
            display:block;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:14px;
            /*@editable*/ font-style:italic;
            /*@editable*/ font-weight:normal;
            /*@editable*/ line-height:100%;
            /*@editable*/ letter-spacing:normal;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            /*@editable*/ text-align:left;
        }

        /* ========== Header Styles ========== */

        /**
        * @tab Header
				* @sec preheader style
				* @tip Set the background color and bottom border for your email's preheader area.
				* @theme header
				*/
        #templatePreheader #erc-logo{
            /*@editable*/ background-color:#00b7ff;
        }

        /**
        * @tab Header
				* @sec preheader text
				* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
				*/
        .preheaderContent{
            /*@editable*/ color:#808080;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:10px;
            /*@editable*/ line-height:125%;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Header
				* @sec preheader link
				* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
				*/
        .preheaderContent a:link, .preheaderContent a:visited, /* Yahoo! Mail Override */ .preheaderContent a .yshortcuts /* Yahoo! Mail Override */{
            /*@editable*/ color:#606060;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        /**
        * @tab Header
				* @sec header style
				* @tip Set the background color and borders for your email's header area.
				* @theme header
				*/
        #templateHeader{
            /*@editable*/ background-color:#F4F4F4;
            /*@editable*/ border-bottom:1px solid #CCCCCC;
        }

        /**
        * @tab Header
				* @sec header text
				* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
				*/
        .headerContent{
            /*@editable*/ color:#505050;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:20px;
            /*@editable*/ font-weight:bold;
            /*@editable*/ line-height:100%;
            /*@editable*/ padding-top:0;
            /*@editable*/ padding-right:0;
            /*@editable*/ padding-bottom:0;
            /*@editable*/ padding-left:0;
            /*@editable*/ text-align:left;
            /*@editable*/ vertical-align:middle;
        }

        /**
        * @tab Header
				* @sec header link
				* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
				*/
        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
            /*@editable*/ color:#EB4102;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        #headerImage{
            height: 60%;
            max-width:600px;
            width: 100%;
            object-fit: cover;
        }

        /* ========== Body Styles ========== */

        /**
        * @tab Body
				* @sec body style
				* @tip Set the background color and borders for your email's body area.
				*/
        #templateBody{
            /*@editable*/ background-color:#F4F4F4;
            /*@editable*/ border-top:1px solid #FFFFFF;
            /*@editable*/ border-bottom:1px solid #CCCCCC;
        }

        /**
        * @tab Body
				* @sec body text
				* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
				* @theme main
				*/
        .bodyContent{
            /*@editable*/ color:#505050;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:16px;
            /*@editable*/ line-height:150%;
            padding-top:20px;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Body
				* @sec body link
				* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
				*/
        .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
            /*@editable*/ color:#EB4102;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        .bodyContent img{
            display:inline;
            height:auto;
            max-width:560px;
        }

        /* ========== Column Styles ========== */

        /**
        * @tab Columns
				* @sec column style
				* @tip Set the background color and borders for your email's column area.
				*/
        #templateColumns{
            /*@editable*/ background-color:#fff;
            /*@editable*/ border-top:1px solid #FFFFFF;
            /*@editable*/ border-bottom:1px solid #CCCCCC;
        }

        /**
        * @tab Columns
				* @sec left column text
				* @tip Set the styling for your email's left column content text. Choose a size and color that is easy to read.
				*/
        .leftColumnContent{
            /*@editable*/ color:#505050;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:14px;
            /*@editable*/ line-height:150%;
            padding-top:0;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Columns
				* @sec left column link
				* @tip Set the styling for your email's left column content links. Choose a color that helps them stand out from your text.
				*/
        .leftColumnContent a:link, .leftColumnContent a:visited, /* Yahoo! Mail Override */ .leftColumnContent a .yshortcuts /* Yahoo! Mail Override */{
            /*@editable*/ color:#EB4102;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        /**
        * @tab Columns
				* @sec right column text
				* @tip Set the styling for your email's right column content text. Choose a size and color that is easy to read.
				*/
        .rightColumnContent{
            /*@editable*/ color:#505050;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:14px;
            /*@editable*/ line-height:150%;
            padding-top:0;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Columns
				* @sec right column link
				* @tip Set the styling for your email's right column content links. Choose a color that helps them stand out from your text.
				*/
        .rightColumnContent a:link, .rightColumnContent a:visited, /* Yahoo! Mail Override */ .rightColumnContent a .yshortcuts /* Yahoo! Mail Override */{
            /*@editable*/ color:#EB4102;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        .leftColumnContent .article-photo, .rightColumnContent img{
            display:inline;
            height: 150px;
            max-width:260px;
            object-fit: cover;
        }

        /* ========== Footer Styles ========== */

        /**
        * @tab Footer
				* @sec footer style
				* @tip Set the background color and borders for your email's footer area.
				* @theme footer
				*/
        #templateFooter{
            /*@editable*/ background-color:#fff;
            /*@editable*/ border-top:1px solid #FFFFFF;
        }

        /**
        * @tab Footer
				* @sec footer text
				* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
				* @theme footer
				*/
        .footerContent{
            /*@editable*/ color:#808080;
            /*@editable*/ font-family:Helvetica;
            /*@editable*/ font-size:10px;
            /*@editable*/ line-height:150%;
            padding-top:20px;
            padding-right:20px;
            padding-bottom:20px;
            padding-left:20px;
            /*@editable*/ text-align:left;
        }

        /**
        * @tab Footer
				* @sec footer link
				* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
				*/
        .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
            /*@editable*/ color:#606060;
            /*@editable*/ font-weight:normal;
            /*@editable*/ text-decoration:underline;
        }

        /**
         * @tab Article Photo
				 * @styling custom
				 */
        .article-photo {
            display: block;
            height: 134px;
            object-fit: cover;
        }

        /**
         * @tab Links
				 * @styling custom
				 */
        .link {
            color: #31a1ff;
            text-decoration: none;
        }

        .social {
            color: #c5c5c5;
        }

        .footer-text {
            text-align: center;
            font-size: 12px;
            padding-bottom: 5px;
        }

        /* /\/\/\/\/\/\/\/\/ MOBILE STYLES /\/\/\/\/\/\/\/\/ */

        @media only screen and (max-width: 480px){
            /* /\/\/\/\/\/\/ CLIENT-SPECIFIC MOBILE STYLES /\/\/\/\/\/\/ */
            body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
            body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */

            /* /\/\/\/\/\/\/ MOBILE RESET STYLES /\/\/\/\/\/\/ */
            #bodyCell{padding:10px !important;}

            /* /\/\/\/\/\/\/ MOBILE TEMPLATE STYLES /\/\/\/\/\/\/ */

            /* ======== Page Styles ======== */

            /**
            * @tab Mobile Styles
					* @sec template width
					* @tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.
					*/
            #templateContainer{
                max-width:600px !important;
                /*@editable*/ width:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec heading 1
					* @tip Make the first-level headings larger in size for better readability on small screens.
					*/
            h1{
                /*@editable*/ font-size:24px !important;
                /*@editable*/ line-height:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec heading 2
					* @tip Make the second-level headings larger in size for better readability on small screens.
					*/
            h2{
                /*@editable*/ font-size:20px !important;
                /*@editable*/ line-height:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec heading 3
					* @tip Make the third-level headings larger in size for better readability on small screens.
					*/
            h3{
                /*@editable*/ font-size:18px !important;
                /*@editable*/ line-height:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec heading 4
					* @tip Make the fourth-level headings larger in size for better readability on small screens.
					*/
            h4{
                /*@editable*/ font-size:16px !important;
                /*@editable*/ line-height:100% !important;
            }

            /* ======== Header Styles ======== */

            /* #templatePreheader{display:none !important;} */ /* Hide the template preheader to save space */

            /**
            * @tab Mobile Styles
					* @sec header image
					* @tip Make the main header image fluid for portrait or landscape view adaptability, and set the image's original width as the max-width. If a fluid setting doesn't work, set the image width to half its original size instead.
					*/
            #headerImage{
                height:auto !important;
                /*@editable*/ max-width:600px !important;
                /*@editable*/ width:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec header text
					* @tip Make the header content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
					*/
            .headerContent{
                /*@editable*/ font-size:20px !important;
                /*@editable*/ line-height:125% !important;
            }

            /* ======== Body Styles ======== */

            /**
            * @tab Mobile Styles
					* @sec body text
					* @tip Make the body content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
					*/
            .bodyContent{
                /*@editable*/ font-size:18px !important;
                /*@editable*/ line-height:125% !important;
            }

            /* ======== Column Styles ======== */

            .templateColumnContainer{display:block !important; width:100% !important;}
            .empty-templateColumnContainer{ margin-top: 20px; }

            /**
            * @tab Mobile Styles
					* @sec column image
					* @tip Make the column image fluid for portrait or landscape view adaptability, and set the image's original width as the max-width. If a fluid setting doesn't work, set the image width to half its original size instead.
					*/
            .columnImage{
                height:auto !important;
                /*@editable*/ max-width:480px !important;
                /*@editable*/ width:100% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec left column text
					* @tip Make the left column content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
					*/
            .leftColumnContent{
                /*@editable*/ font-size:16px !important;
                /*@editable*/ line-height:125% !important;
            }

            /**
            * @tab Mobile Styles
					* @sec right column text
					* @tip Make the right column content text larger in size for better readability on small screens. We recommend a font size of at least 16px.
					*/
            .rightColumnContent{
                /*@editable*/ font-size:16px !important;
                /*@editable*/ line-height:125% !important;
            }

            /* ======== Footer Styles ======== */

            /**
            * @tab Mobile Styles
					* @sec footer text
					* @tip Make the body content text larger in size for better readability on small screens.
					*/
            .footerContent{
                /*@editable*/ font-size:14px !important;
                /*@editable*/ line-height:115% !important;
            }

            .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */

            #top-post {
                padding-left: 50px !important;
            }

            #social-icons {
                padding: 0 !important;
            }
        }

        .image-container img
        {
            max-width: 165px;
            max-height: 180px;
        }

    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="margin: 0; mso-line-height-rule: exactly;">
<div style="width:600px !important;max-width: 600px; margin: auto;" class="email-container">
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;">
        <tr>
            <td align="center" valign="top" id="bodyCell" width="600">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" width="100%" style="max-width: 600px;border-collapse:collapse;">
                    <tr>
                        <td style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;">
                            <!-- BEGIN PREHEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader" style="border-collapse:collapse;">
                                <tr>
                                    <td align="center" bgcolor="#00b7ff" id="erc-logo" style="padding: 20px; padding-bottom: 15px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;background-color:#00b7ff;">
                                        <img src="{{ env('APP_URL').'/img/ERC-LOGO-V3-1.png' }}" alt="Environmental Resource Center" width="350" height="60" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#043767" style="padding:22px; font-size: 24px; line-height: 24px; height: 24px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; text-align:center;color:#fff;background-color:#043767;" height="24">
                                        <img alt="erc phone" src="{{ env('APP_URL').'/img/email/phone.png' }}" style="vertical-align:top; max-height:20px;"  width="20" height="20" />&nbsp;800-537-2372
                                    </td>
                                </tr>
                            </table>
                            <!-- // END PREHEADER -->
                        </td>
                    </tr>

                    <tr>
                        <td bgcolor="#fff" style="padding: 10px 20px 10px 20px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="260" valign="top" class="templateColumnContainer" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f5f5f5">
                                            <tr>
                                                <td colspan="3" class="title"style="padding: 20px 20px 40px 20px;color: #153643; font-size: 16px; line-height: 20px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;text-align:left;">
                                                    <table border="0" cellpadding="5" cellspacing="">
                                                        <tbody>
                                                            <tr><td><strong>On-Site and Customized Training</strong></td></tr>
                                                            <tr><td>Name: {{ $inputs['name'] }}</td></tr>
                                                            <tr><td>Company: {{ $inputs['company'] }}</td></tr>
                                                            <tr><td>Email: {{ $inputs['email'] }}</td></tr>
                                                            <tr><td>Phone Number: {{ $inputs['phone_number'] }}</td></tr>
                                                            <tr><td>Training Requested: {{ $inputs['training_requested'] }}</td></tr>
                                                            <tr><td>Number of students: {{ $inputs['number_students'] }}</td></tr>
                                                            <tr><td>Wants to be called to discuss the training: {{ $inputs['for_call'] }}</td></tr>
                                                            <tr><td>Other Comments: {{ $inputs['comments'] }}</td></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td border="0" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;">
                            <table topmargin="20"  border="0" cellpadding="0" cellspacing="0" bgcolor="#0158a2" style="padding: 20px; text-align: center" width="100%">
                                <tr>
                                    <td bgcolor="#0158a2" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;background-color:#0158a2;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td bgcolor="#0158a2" border="0"  width="160" valign="top" class="templateColumnContainer" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;">
                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%" bgcolor="#0158a2">
                                                        <tr>
                                                            <td colspan="3" border="0" bgcolor="#0158a2" align="center" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;"  cellpadding="10">
                                                                <img class="article-photo" src="{{ env('APP_URL').'/img/email/erc-email-footer-1.jpg' }}"
                                                                     alt=""
                                                                     width="220"
                                                                     height="134"
                                                                     style="display:block; height: 134px;object-fit: cover; object-position: top;background-color:#0158a2;padding-left:20px;width:220px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" border="0" class="title" align="center" bgcolor="#0158a2" style="padding-top: 15px;color: #FFFFFF; font-size: 16px; line-height: 20px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;">
                                                                <a href="{{ env('APP_URL').'/training/on-site-and-customized-training' }}" style="color:#FFFFFF;text-decoration: none">On-Site Training</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td bgcolor="#0158a2"  border="0" width="160" valign="top" class="templateColumnContainer" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;">
                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%" bgcolor="#0158a2">
                                                        <tr>
                                                            <td colspan="3" bgcolor="#0158a2" align="center" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;" cellpadding="10">
                                                                <img class="article-photo" src="{{ env('APP_URL').'/img/email/erc-email-footer-3.jpg' }}"
                                                                     alt=""
                                                                     width="220"
                                                                     height="134"
                                                                     style="display:block; height: 134px;object-fit: cover; object-position: top;background-color:#0158a2;padding-left:20px;width: 220px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="title" align="center" bgcolor="#0158a2" style="padding-top: 15px;color: #FFFFFF; font-size: 16px; line-height: 20px;mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;width:160px;">
                                                                <a href="[coursesPage]" style="color:#FFFFFF;text-decoration: none">Additional Courses</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="footer" bgcolor="#FFFFFF" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:left;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter">
                                <tr>
                                    <td class="footer-text" align="center" style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:center;">
                                    <p style="margin-top:20px;padding:0px;margin-bottom:20px;"> To subscribe to our newsletters, <a href="http://care.ercweb.com/lists/?p=subscribe&id=2" style="color: #0158a2;"> click here.</a> <br>
                                    To visit our website, <a href="{{ env('APP_URL') }}" style="color: #0158a2;"> click here.</a></p>
                                     <hr style=" border-bottom: 1px solid #c5c5c5; width:90%;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="mso-line-height-rule:exactly;-webkit-text-size-adjust:none;-ms-text-size-adjust:none; font-family:Arial, Helvetica, sans-serif;font-size:15px;line-height:18px;text-align:center;">
                                        <a class="social" href="https://www.youtube.com/channel/UC4BwgRQIsBi-T_VjOf29vJw" target="_blank">
                                            <img src="{{ env('APP_URL') . '/img/email/youtube-logo.png' }}" alt="erc youtube"/>
                                        </a>
                                        <a class="social" href="https://www.facebook.com/ercweb" target="_blank">
                                            <img src="{{ env('APP_URL') . '/img/email/fb-logo.png' }}" alt="erc facebook"/>
                                        </a>
                                        <a class="social" href="https://twitter.com/ercweb" target="_blank">
                                            <img src="{{ env('APP_URL') . '/img/email/twitter-logo.png' }}" alt="erc twitter"/>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="footer-text" align="center" style="padding-top: 20px; color: #0158a2; line-height: 1.3em">Environmental Resource Center<br/>101 Center Pointe Drive<br/>Cary, NC 27513</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</div>

</body>
</html>