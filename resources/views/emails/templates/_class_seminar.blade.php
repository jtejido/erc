[if isSeminar]
[if hasMultipleClasses]
<p><strong>[name]</strong> on [dateRange] in [locationGeneral]. You have registered the following attendee(s):</p>
[else]
<p>Thank you for registering personnel for [name] on [dateRange] in [locationGeneral]. You have registered the following attendee(s):</p>
[/if]
@include('emails.templates._attendees')
<p>
    [if isCary]
    The seminar will be held at the Environmental Resource Center, located at 101 Center Pointe Dr. in Cary, North Carolina 27513 with phone no. (919) 469-1585. The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime].
    [else]
    The seminar will be held at [locationName] located at [locationAddress] with phone no. [locationPhone]. The training will begin at [startTime] and conclude at [endTime]. Registration begins at [regTime].
    [/if]
</p>
[/if]