[if isWebinar]
[if hasMultipleClasses]
<p><strong>[name]</strong> on [dateRange] from [startTime] to [endTime]. You have registered the following attendee(s):</p>
[else]
<p>Thank you for registering personnel for the [name] on [dateRange] from [startTime] to [endTime]. You have registered the following attendee(s):</p>
[/if]
@include('emails.templates._attendees')
[/if]