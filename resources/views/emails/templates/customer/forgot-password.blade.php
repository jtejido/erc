@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>
<p>[greeting]</p>
<p><a href="[link]">Reset Password</a></p>
<p>To login to our website:&nbsp;<a href="{{ env('APP_URL').'/login' }}">Click here</a></p>
<p>When you sign in to your account, you will be able to</p>
<ul>
    <li>Proceed through checkout faster</li>
    <li>Check the status of&nbsp;trainings</li>
    <li>View&nbsp;available and previous&nbsp;classes/orders</li>
</ul>
@endsection