@extends('emails.templates.template')

@section('body')
<p>[user]</p>
<p>[name]<br/>[company]<br/>[address1]<br/>[address2]</p>

<p>Dear [name]:</p>
[/user]

<p>
    [if is_webCast]
    Thank you for purchasing an Annual Training Subscription. You can attend all Environmental Resource Center webcasts for one year. The subscription year begins with your first course.
    [else]
    Thank you for purchasing an Annual Training Subscription. You can attend all Environmental Resource Center seminars and webcasts for one year. Online training is not included in the annual subscription rate. Click <a href=[coursesPage]>here</a> to view a list of courses. The subscription year begins with your first course.
    [/if]
</p>

<p>
    To begin using your subscription, use the following activation code: [activationCode]
</p>

<p>
    Thank you for choosing Environmental Resource Center.
</p>


<p>
    Sincerely,<br/>
    <strong>Jenna Lewis</strong><br>
    Environmental Resource Center<br>
    jlewis@environmentalresourcecenter.com<br>
</p>
@endsection