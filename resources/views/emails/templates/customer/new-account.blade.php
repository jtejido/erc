@extends('emails.templates.template')

@section('body')
[user]
<p>
    Thank you for creating an account with Environmental Resource Center. When you sign into your account, from your User Portal, you will be able to:
</p>

<ul>
    <li>Proceed through checkout faster</li>
    <li>Check the status of your training</li>
    <li>View available and previous classes/orders</li>
    <li>Print certificates</li>
</ul>

<p>To sign in to our site, use these credentials:</p>
<ul>
    <li>Email: [email]</li>
    @if (isset($password))
        <li>Password:&nbsp;<em>Your temporary password is {{ $password }}</em></li>
    @else
        <li>Password:&nbsp;<span>[password]</span></li>
    @endif</ul>
[/user]
@include('emails.templates._login')
@endsection