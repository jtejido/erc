@extends('emails.templates.template')

@section('body')
<p>Dear [name],</p>
[if hasMultiple]
<p>We would like to inform you that the following Online Training Accounts were created.</p>
[else]
<p>We would like to inform you that the following Online Training Account was created.</p>
[/if]
[accounts]
<p>Name: [name]</p>
<ul>
    <li>User ID: [userId]</li>
    <li>Password: [password]</li>
</ul>
[/accounts]
@include('emails.templates._login')
@endsection