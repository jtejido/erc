@extends('emails.templates.template')

@section('body')
[user]
<p>[name]<br/>[company]<br/>[address1]<br/>[address2]</p>
<br/>
<p>Dear <strong>[name]: </strong></p>
[/user]

<p>
    Thank you for purchasing a [number]-seat Corporate Subscription. You and your employees may use these seats for any of Environmental Resource Center’s webcasts or seminars. Online training is not included in your subscription but can be purchased separately through our website.
</p>

<p>
    Click <a href=[coursesPage]>here</a> to see a complete list of our courses.  You can register by phone, email, or online.  Just reference your corporate subscription number – [confirmationCode].
</p>

<p>
    If you prefer to register by phone or email, I will be happy to be your point person. You can email me the student names, email addresses, and the courses your team members would like to attend and I will take care of the rest.
</p>

<p>
    Please let me know if you have any questions.
</p>


<p>
    Sincerely,<br/>
    <strong>Jenna Lewis</strong><br>
    Environmental Resource Center<br>
    jlewis@environmentalresourcecenter.com<br>
</p>
@endsection