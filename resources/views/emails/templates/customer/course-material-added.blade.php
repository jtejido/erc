@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>
<p>
    We would like to inform you that new course material has been added to your [course_name] course
    with Environmental Resource Center taking place on [start_date_and_time].
    The new course material is available on your User Portal.
</p>
@include('emails.templates._login')
@endsection