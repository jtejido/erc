@extends('emails.templates.template')

@section('body')
<p>Dear [name]:</p>
[update]
[if type=status]
<p>Your Environmental Resource Center account <strong>[email]</strong> was recently <strong>[status]</strong>.</p>
[/if]
[if type=password]
<p>The password for your Environmental Resource Center account <strong>[email]</strong> was recently changed.</p>
[/if]
[if type=email]
<p>Your email address&nbsp;for your Environmental Resource Center account was recently changed to [email]. Your old email address [email_old] has been emailed for reference.</p>
[/if]
[/update]
<p><strong>Don't recognize this activity?</strong><br/>
    Please call 800-537-2372 to speak with a customer service representative or email <a href="mailto:service@ercweb.com">service@ercweb.com</a> for assistance.</p>
@endsection