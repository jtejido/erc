@extends('emails.templates.template')

@section('body')
   <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[content]</p>
   <br>
   <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[name]</p>
   <p style="font-family: 'Helvetica Neue', Helvetica, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; color: #000000; text-align: start;">[email]</p>
@endsection