<body>
    <div class="container">
        <div class="content">
            <h3>Hello there!,</h3>
            <br />
            @if (isset($password))
                Your temporary password is {{ $password }}
                <br />
            @endif
            To login : <a href="{{ url('/login')}}">Click here</a>
            <br /><br />
            Thanks!,
            <br />
            - Environmental Resource Center Admin
            <div class="quote">
            </div>
        </div>
    </div>
</body>
