@extends('emails.templates.template')

@section('body')
<p>Hello Admin,</p>
<p>An API communication issue occured while checking out the order for the course: {{ $course->title }}.</p>
<p>The coursemill registration for the ff. students may have failed via the API:</p>
@foreach($accounts as $account)
* {{ $account->name }} <br/>
@endforeach
<p>
<strong>Error Message:</strong><br/>
{{ $error_message }}
</p>
<p>Thank You.</p>
@endsection
