<body>
    <div class="container">
        <div class="content">
            <h3>Hello there!,</h3>
            <br />
            Your class registration for {{ $class }} has been cancelled!
            <br />

            <br /><br />
            Thanks!,
            <br />
            - Environmental Resource Center Admin
            <div class="quote">
            </div>
        </div>
    </div>
</body>
