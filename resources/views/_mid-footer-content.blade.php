<div class="news-contact-container">
    <div class="news-wrapper">
    <div class="post-news">
        <div class="news">

            @if (count($articles))

                @foreach ($articles as $article)
                    <?php $link = isset($article['reg'])
                        ? '/regulations/show/' . $article['slug']
                        : '/tips/show/' . $article['slug'];?>

                    <?php $default  = isset($article['reg'])
                        ? route('reg.photo', $article['id'])
                        : route('tip.photo', $article['id']); ?>

                    <?php $path = isset($article['reg'])
                        ? '/img/erc/homepage/article-1.jpg'
                        : (isset($article['env'])
                            ? '/img/erc/homepage/article-2.jpg'
                            : '/img/erc/homepage/article-3.jpg' );?>

                        <a href="{{ $link }}">
                    <div class="article">
                        <div class="header read-article" data-link="{{ $link }}" style="background-image: url({{ ($article['photo'] and $article['photo_mime']) ? $default : $path }})">
                            <div class="date-container">
                                <div class="date">
                                    <div class="month-day">
                                        <span class="day">{{ \Carbon\Carbon::parse($article['published_date'])->format('d') }}</span>
                                        <span class="month">{{ \Carbon\Carbon::parse($article['published_date'])->format('M') }}</span>
                                    </div>
                                    <div class="year">
                                        <span>{{ \Carbon\Carbon::parse($article['published_date'])->format('Y') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="article-content">
                            <div class="title">
                                <h3 style="font-size: 16px;">{{ str_limit($article['title'], 30) }}</h3>
                            </div>

                        </div>
                    </div>
                        </a>
                @endforeach
            @else

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-1.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-2.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-3.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-1.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-2.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="article">
                    <div class="header">
                        <div class="date-container">
                            <div class="date">
                                <div class="month-day">
                                    <span class="day">10</span>
                                    <span class="month">Nov</span>
                                </div>
                                <div class="year">
                                    <span>2017</span>
                                </div>
                            </div>
                        </div>
                        <img src="/img/erc/homepage/article-3.jpg" alt="Article Photo">
                    </div>

                    <div class="article-content">
                        <div class="title">
                            <h3>This is a title of an article</h3>
                        </div>

                        <div class="category">
                            In <span>Category</span>
                        </div>

                        <div class="story">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id officia provident qui laboriosam, sint ducimus, nulla vero corporis vitae quis delectus, temporibus at aliquam! Consectetur dolor beatae nihil repudiandae iure.
                        </div>

                        <div class="footer">
                            <a href="" class="read-more">Read More</a>

                            <div class="socials">
                                <a href=""><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                                <a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>

    </div>

    <div class="contact row">
        <div class="col col-sm-3 col-m1">
            <div class="col-panel m1">
            <div class="text">
                <h5>Reg of the Day&trade;, <br/>Tip of the Week&trade;, Discounts <br/>and Special Offers</h5>
            </div>
            <div class="button">
                <a href="http://care.ercweb.com/lists/?p=subscribe&id=2" class="btn btn-primary">
                    SIGN UP
                </a>
            </div>
            </div>
        </div>
        <div class="col col-sm-6 col-m2">
            <div class="col-panel m2">
            <div class="text">
                <h3 style="margin-bottom: 15px !important;">
                    <strong>Environmental Resource Center <br/> Your Compliance Credentials</strong>
                </h3>
            </div>
            <div class="button">
                <a href="{{ url('/contact-us') }}" class="btn btn-primary">CONTACT US</a>
            </div>
            </div>
        </div>
        <div class="col col-sm-3 col-m3">
            <div class="col-panel m3">
            <div class="text">

            </div>
            <div class="button" id="comm100-button-20">
                <a href="#" class="btn btn-primary live-chat-img" id="comm100-button-20">
                    CLICK HERE
                </a>
                <div id="live-chat" class="hidden"></div>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>