
@extends('main.main_layout')

@section('main_content')

<section class="register">
    <div class="authentication-top"> </div>
    <div class="container">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-inner panel-heading">Register</div>
            <div class="panel-inner panel-body">
                <form id="registration_form" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                      <div class="row">             
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">First Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Last Name: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Title: <span></span></label>
                            <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                          </div>
                        </div>            
                        <div class="col-md-6">    
                          <div class="form-group">
                            <label class=" control-label">E-Mail: <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Password: <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password">
                            <span class="help-text col-md-12">Password should not be less than 6 characters.</span>
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Confirm Password: <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="re_password">                               
                          </div>
                        </div>
                      </div>                  

                      
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class=" control-label">Company, Agency or Organization: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="company" id="company" value="{{ old('company') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">Address: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="address" value="{{ old('address') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label"></label>
                            <input type="text" class="form-control" name="address2" value="{{ old('address2') }}">
                          </div>
                          <div class="form-group">
                            <label class=" control-label">City: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="city" value="{{ old('city') }}">
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">State: <span class="text-danger">*</span></label>
                            <select class="form-control" name="state">
                                @foreach ($state_list as $key => $value)
                                    <option value="{{ $value['state'] }}" <?php if(old('state') == $value['state']){ echo "selected"; } ?> >{{ $value['state'] }}</option>
                                @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class=" control-label">Zip: <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="zip_code" name="zip_code" maxlength="5" value="{{ old('zip_code') }}">
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group phone">
                            <label class=" control-label">Phone: <span class="text-danger">*</span></label>
                            <input type="text"
                                   class="form-control"
                                   id="phone_number"
                                   name="phone_number" value="{{ old('phone_number') }}"
                                   data-inputmask='"mask": "(999) 999-9999"' data-mask>
                          </div>
                        </div>

                          <div class="col-sm-6">
                              <div class="form-group phone">
                                  <label class=" control-label">Extension:</label>
                                  <input type="text"
                                         class="form-control"
                                         id="phone_local"
                                         name="phone_local" value="{{ old('phone_local') }}">
                              </div>
                          </div>


                          @if (!Session::has('TEST-TOKEN'))
                              <div class="col-sm-12">
                                  <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE_KEY') }}"></div>
                              </div>
                          @endif

                    </div>
                </form> 
            </div>     
          
          <div class="panel-inner panel-footer">

                <div class="form-group">
                      <button type="button" class="btn btn-primary btn-register" form="registration_form" id="submit-button">Submit</button>
                </div>
            </div>
          
        </div>
        </div>     
    </div>
  </section>
@endsection

@section('javascripts')
    @parent

    <script>

        $(document).ready(function() {
            $('[data-mask]').inputmask();
            $('#submit-button').click(function() {
                $(this).attr('disabled', true)
                    .text('Submitting');
                $('#registration_form').submit();
            });

            $( "#company" ).autocomplete({
                source: "/api/companies",
                minLength: 2,
            });

        });

    </script>
@endsection
