@extends('main.main_layout')

@section('main_content')
    <section class="login">
        <div class="container-fluid">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                <div class="row authentication-top">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="panel panel-default">

                            <div class="panel-inner panel-heading">Reset Password</div>

                            <div class="panel-inner panel-body">

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger alert-login">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label class="control-label">Email Address</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                </div>

                                <div class="form-group">
                                    <label class=" control-label">New Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>

                                <div class="form-group">
                                    <label class=" control-label">Confirm Password</label>
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row authentication-bottom">
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="panel-inner panel-footer">
                            <button type="submit" class="btn btn-primary">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

