@extends('main.main_layout')

@section('main_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Registration Successful!</div>

                    <div class="panel-body">
                        <p>
                            User account email verified and successfully activated.
                        </p>
                        <a href="{{ url('/auth/login') }}">
                            <button type="button" class="btn btn-success">
                                Click here to Log-in
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
