@extends('main.login_layout')

@section('main_content')
    <section class="login">
        <div class="container-fluid">
            <form id="login-form" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="row" style="margin: 100px 0">

                        <div class="col-md-7">
                            {{--<img src="/img/ERC-LOGO-V3-2.png" style="width: 90%; margin-top: 90px"/>--}}
                        </div>
                        <div class="col-md-5">
                            <div class="panel panel-default">

                                <div class="panel-inner panel-body">

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger alert-login" style="line-height: 1.5em">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{!! $error !!}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    @if (Session::has('session_expired'))
                                        <div class="alert alert-danger alert-login">
                                            {{ Session::get('session_expired') }}
                                        </div>
                                    @endif

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label">Password</label>
                                        <input type="password" class="form-control" name="password">

                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="remember-me" name="remember"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-inner panel-footer">
                                <button type="submit" class="btn btn-primary">Login</button>

                                <div class="form-group">
                                    <a class="btn-link inline btn-reset-password" href="">
                                        <em>Forgot Password</em>
                                    </a>
                                </div>
                                <div class="form-group sign-up-msg" style="color: #FFF">
                                    New attendee? <a class="btn-link inline" href="{{ url('/register') }}">
                                        Sign up here</a> to register for training

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

            </form>
        </div>

        <!-- Modal for reset password -->

        <div class="modal fade reset-password-modal" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="classModalLabel">Forgot Password?</h5>
                    </div>
                    {!! Form::open(['method' => 'POST', 'url' => route('password.post'), 'id' => 'reset-password-form']) !!}

                    <div class="modal-body">
                        <div class="alert hidden"></div>

                        {!! Form::input('email', 'email', '', [
                            'class' => 'form-control',
                            'placeholder' => 'Email address',
                            'required'    => 'required'
                        ]) !!}

                        <p class="note">
                            Enter the email of your ERC account and we will send you a link to reset your password.
                        </p>

                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <!-- Modal for re-activate account -->

        <div class="modal fade reset-password-modal" id="reactivateAccountModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="classModalLabel">Request to re-activate Account?</h5>
                    </div>
                    {!! Form::open(['method' => 'POST', 'url' => route('user.reactivate'), 'id' => 'reactivate_user-form']) !!}

                    <div class="modal-body">
                        <div class="alert hidden"></div>

                        {!! Form::input('email', 'email', '', [
                            'class' => 'form-control',
                            'placeholder' => 'Email address',
                            'required'    => 'required'
                        ]) !!}

                        <p class="note">
                            Enter the email of your ERC account to request for re-activation of account.
                        </p>

                        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section> 
@endsection

@section('javascripts')
    @parent

    <script>
        $(document).ready(function() {
            $('.btn-reset-password').on('click', function(e) {
                e.preventDefault();
                $('#resetPasswordModal').modal('show');
            });

            $('.btn-reactivate-account').on('click', function(e) {
                e.preventDefault();
                $('#reactivateAccountModal').modal('show');
            });

            var hash = window.location.hash.substring(1);

            if (hash) {
                if (hash.indexOf('forgot_password') != -1) {
                    $('.btn-reset-password').click();
                }
            }

            $('#reactivate_user-form').on('submit', function(e) {
                var $this = $(this),
                    email = $this.find('input[type="email"]').val(),
                    $alert = $this.find('.alert'),
                    $button = $this.find('button');

                e.preventDefault();

                $.ajax({
                    type : 'POST',
                    url  : $this.attr('action'),
                    data : { email : email },
                    beforeSend : function (data) {
                        $button.attr('disabled', true);
                    },
                    success : function (response) {
                        if (response.error.length) {
                            $alert.removeClass('hidden alert-success')
                                .addClass('alert-danger')
                                .text(response.error);

                            $button.attr('disabled', false);
                        }

                        else {
                            $alert.removeClass('hidden alert-danger')
                                .addClass('alert-success')
                                .text(response.message);

                            setTimeout(function() {
                                window.location.reload();
                            }, 3000);
                        }
                    }
                });

            });
        });
    </script>
@endsection
