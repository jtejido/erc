<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @section('meta')  @show

    <title>@section('title') Environmental Resource Center @show</title>
    <script type="text/javascript">
        window.postInit = [];
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }
    </script>

    @section('stylesheets')
        {!! HTML::style('vendor/common.css') !!}
        {!! HTML::style('vendor/common_2.css') !!}
    @show


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>

</head>

<body class="{{ $page }} @yield('body-class', '') frontend" ng-app="ercApp">
    <input type="hidden" id="session-expired" value="{{ route('session.expired') }}">

    <header>
    <div class="header">
        <div class="logo-social-search">
            <a href="/">
            <img class="full-width" src="/img/ERC-LOGO-V3-2.png" alt="Environmental Resource Center">
            </a>

            <div class="pull-right right">
                <div class="contact">
                    800-537-2372
                </div>

                <div class="social-search">
                    <div class="icons">
                        <a href="https://www.facebook.com/ercweb/" target="_blank" class=""><img src="/img/erc/homepage/facebook.png" alt="Facebook"></a>
                        <a href="https://twitter.com/ercweb" target="_blank" class=""><img src="/img/erc/homepage/twitter.png" alt="Twitter"></a>
                        <a href="https://www.youtube.com/channel/UC4BwgRQIsBi-T_VjOf29vJw" target="_blank" class=""><img src="/img/erc/youtube-1.png" alt="youtube"></a>
                        <a href="https://www.linkedin.com/company/environmental-resource-center" target="_blank" class=""><img src="/img/erc/homepage/linkedin.png" alt="Linkedin"></a>
                    </div>
                    <form id="top-search">
                    <div class="input-group">
                        <input type="search" id="global-search" class="form-control search" placeholder="Search">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="global-search-btn"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                    </form>

                </div>
            </div>
        </div>

        <nav class="navbar navbar-static-top top-nav">

            <div class="top-nav-container">
                <div class="main-container">
                    <div class="navbar-header">
                        <div class="row">
                            <button type="button" class="navbar-toggle collapsed pull-right col-xs-2" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <a class="brand col-xs-8" href="{{ url('/') }}">
                                <img class="non-full-size" src="/img/ERC-LOGO-V3-2.png" alt="Environmental Resource Center">
                            </a>

                            <div class="col-xs-2">

                            </div>
                        </div>
                    </div>

                    <div class="navbar-collapse collapse nav-collapse" id="navbar">

                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{{ url('/') }}">Home</a>
                            </li>
                            <li class="dropdown">


                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Training
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ route('courses') }}">Course Listing</a></li>
                                    <li><a href="/training/benefits">Benefits</a></li>
                                    <li><a href="/training/on-site-and-customized-training">On-site and Customized Training</a></li>
                                    <li><a href="/training/subscription">Subscriptions</a></li>
                                    <li><a href="/training/gsa">GSA</a></li>
                                </ul>
                            </li>



                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Consulting
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/consulting/consulting">Consulting</a></li>
                                    <li><a href="/consulting/audits">Audits</a></li>
                                    <li><a href="/consulting/permits">Permits</a></li>
                                    <li><a href="/consulting/hazardous-materialsdangerous-goods-classification">Hazardous Materials/Dangerous Goods Classification</a></li>
                                    <li><a href="/consulting/sara-title-ii-compliance-and-reporting">SARA Title III Compliance and Reporting</a></li>
                                    <li><a href="/consulting/plans-and-procedures">Plans and Procedures</a></li>
                                    <li><a href="/consulting/ehs-program-management">EHS Program Management</a></li>
                                    <li><a href="/consulting/safety-database-development">Safety Database Development</a></li>
                                    <li><a href="/consulting/hazardous-materials-databases">Hazardous Materials Databases</a></li>
                                    <li><a href="/consulting/ehs-on-site-staffing">EHS On-site Staffing</a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="{{ route('products') }}">Products</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Resources
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/regulations">Reg of the Day</a></li>
                                    <li><a href="/tips">Tips of the Week</a></li>
                                    <li><a href="{{ route('resource.classcalendar') }}">Schedule of Classes</a></li>
                                    <li><a href="/resources/helpful-links ">Helpful Links</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    FAQ
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/faq/who-needs-training">Who needs Training?</a></li>
                                    <li><a href="/faq/seminars">Seminars</a></li>
                                    <li><a href="/faq/webcasts">Webcasts</a></li>
                                    <li><a href="/faq/online-training">Online Training</a></li>
                                    <li><a href="/faq/purchases">Purchases</a></li>
                                    <li><a href="/faq/answerline">Answerline</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Company
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/about-us">About Us</a></li>
                                    <li><a href="/our-team">Our Team</a></li>
                                    <li><a href="/contact-us">Contact Us</a></li>
                                </ul>
                            </li>


                            @if (Auth::guest())
                                <li class="navbar-menu-auth"><a class="@if ($page == 'login') active @endif" href="{{ url('/login') }}">Login/Register</a></li>
                            @endif

                            @if (Auth::user())
                                {{-- */ $itemsCount = OrderUtility::hasItems(Auth::user()->id) /* --}}

                                @if ($itemsCount)
                                    <li><a href="{{ route('user.shopping_cart') }}">View Cart ({{ $itemsCount }})</a></li>
                                @endif

                                <li class="navbar-menu-auth dropdown">

                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">

                                        @if(has_role([App\Utilities\Constant::ROLE_ADMIN, App\Utilities\Constant::ROLE_CSR]))
                                            {{ explode('@', Auth::user()->email)[0] }}
                                        @else
                                            @if (Auth::user()->contact)
                                                {{ Auth::user()->contact->first_name }}
                                            @else
                                                {{ Auth::user()->email }}
                                            @endif
                                        @endif
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        @if(has_role([App\Utilities\Constant::ROLE_ADMIN, App\Utilities\Constant::ROLE_CSR]))
                                            <li><a href="{{ route('admin.dashboard') }}">Admin Dashboard</a></li>
                                        @else
                                            <li><a href="{{ route('user.home') }}">User Portal</a></li>
                                            <li><a href="{{ route('profile.edit') }}">Edit Profile</a></li>
                                        @endif
                                        <li><a href="{{ url('/logout') }}">Logout</a></li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>

                </div>
            </div>



        </nav>
    </div>
    </header>

    @yield('body')

@section('javascripts-top')
    {!! HTML::script('vendor/common.js') !!}
    <script>
        (function(d, script) {
            var ua = window.navigator.userAgent;

            var msie = ua.indexOf('MSIE ');

            var trident = ua.indexOf('Trident/');
            if ((msie > 0 || trident > 0) == false) {
                script = d.createElement('script');
                script.type = 'text/javascript';
                script.async = true;
                script.onload = function(){
                    // remote script has loaded
                };
                script.src = window.location.origin + '/js/non-ie.js';
                //d.getElementsByTagName('head')[0].appendChild(script);
                d.body.append(script);
            }
        }(document));
    </script>
    <script type="text/javascript"
            src="https://www.google.com/recaptcha/api.js?hl=en"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
@show

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

@section('javascripts')
    <script>

        $('#global-search-btn').on('click', function() {
            searchQ();
        });

        $('#top-search').submit(function(){
            searchQ();
            return false;
        });

        function searchQ() {
            var searchQ = $('#global-search').val(),
                origin = window.location.href.split('?')[0];

            if ($.trim(searchQ).length > 0) {

                if (origin == "{{ route('courses') }}") {
                    $('input[name="searchq"]').val(searchQ);
                    return $('#filters').submit();
                }

                return window.location.href = "{{ route('courses') . '?searchq='  }}" + searchQ;
            }
        }

    </script>
@show
</body>
</html>