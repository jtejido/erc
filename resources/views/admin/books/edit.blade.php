@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Edit Book</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.books.index') }}">Books Management</a></li>
            <li>Edit Product</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @include('admin.includes.form_errors')
                        <div class="pull-right">
                            <a href="{{ route('admin.books.show', [$book->id]) }}"
                               class="btn btn-flat btn-sm btn-info">
                                <i class="fa fa-info-circle"></i>&nbsp;View</a>
                        </div>
                    </div>

                    <div class="box-body">
                        {!! Form::model($book, [
                                        'url'       => route('admin.books.update', [$book->id]),
                                        'method'    => 'put',
                                        'files'     => true,
                                        'class'     => 'status-filter form-horizontal'
                                        ]) !!}
                        @include('admin.books._form')
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>


    </section>
    @include('includes.confirmation_modal')
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){
            $('#delete_btn').on('click', function(e) {
                var $form = $(this).closest('form');
                e.preventDefault();
                Modal.showConfirmationModal(
                        'Are you sure you want to delete this file?',
                        'Delete File',
                        function() {
                            $form.find("input[name='_method']").val('DELETE');
                            $form.submit();
                            return false;
                        });
            });
            $("#thumbnail").fileinput({
                showPreview: false,
                showUpload: false
            });
        })(jQuery);
    </script>
@endsection
