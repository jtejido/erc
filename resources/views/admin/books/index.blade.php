@extends('admin.layout')
@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        @if (isset($user))
            <h1>{!! $user->getLinkedNameForAdmin() !!} Select Book</h1>
        @else
            <h1>Books Management</h1>
        @endif


        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Books Management</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="actions">
                            @include('admin.includes.success_message')

                            {!! Form::open(['method' => 'GET', 'class' => 'status-filter form-inline']) !!}
                                <div class="form-group">
                                    {!! Form::select('sortby', $sort , $sortby, [
                                        'id'    => 'status',
                                        'class' => 'form-control',
                                        'style' => 'width: 200px;'
                                    ]) !!}
                                    {!! Form::text('keyword', $keyword, [
                                        'class' => 'form-control',
                                        'placeholder'   => 'Enter code or keyword'
                                        ]) !!}
                                    <button type="submit" title="Search" class="btn btn-flat btn-sm btn-info"><i class="fa fa-icon fa-search"></i></button>
                                    <button type="button" title="Reset" id="resetFilter" class="btn btn-flat btn-sm btn-default"><i class="fa fa-icon fa-refresh"></i></button>
                                </div>
                            {!! Form::close() !!}
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <a href="{{ route('admin.books.create') }}" class="btn btn-flat btn-success btn-flat pull-right" id="add-class" style="display: inline;">
                                <i class="fa fa-plus"></i> Add Book
                            </a>
                            @endif
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="row no-gutter">
                            @foreach($books as $book)
                                @include('main.books._book')
                            @endforeach
                        </div>

                        @if (!$books->appends(['product_category' => $category])->count())
                            <i>No books Available.</i>
                        @endif
                    </div>
                    <div class="text-center">{!! $books->appends(['sortby' => $sortby, 'keyword' => $keyword])->render() !!}</div>
                </div>
            </div>
        </div>


    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            $("select#status").change(function() {
                $(this).closest('form').submit();
            });

            $('#resetFilter').on('click', function() {
                var $form = $(this).closest('form');
                $form.find("input[name='keyword']").val('');
                $form.find("select[name='sortby']").val('0');
                $form.submit();
            });

        })(jQuery);
    </script>
@endsection


