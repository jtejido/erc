<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('code', 'Code', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('code', null, $attributes = ['class' => 'form-control'])  !!}
                </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', 'Name', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
            {!! Form::text('name', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('slug', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Price', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
            <div class="input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('price', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
                </div>
        </div>
        <div class="form-group">
            {!! Form::label('discounted_price', 'Discounted Price', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                <div class="input-group">
                    <div class="input-group-addon">$</div>
                    {!! Form::text('discounted_price', null, $attributes = ['class' => 'form-control'])  !!}
                </div>
                <p class="help-block">This price will apply to webcast related books.</p>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('weight', 'Weight', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
            <div class="input-group">
                {!! Form::text('weight', null, $attributes = ['class' => 'form-control'])  !!}
                <div class="input-group-addon">lbs</div>
            </div>
            </div>

        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
            {!! Form::textarea('description', null, $attributes = ['class' => 'form-control'])  !!}
                </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('thumbnail', 'Thumbnail', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
            {!! Form::file('thumbnail', null, $attributes = ['class' => 'form-control'])  !!}
                </div>
        </div>
        @if (isset($book->id) && isset($book->thumbnail))
            <div class="thumbnail btn-flat">
                <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
            </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-flat btn-primary">Save</button>
        @if (isset($book->id) && $book->canBeDeleted())
            <button type="button" class="btn btn-flat btn-danger" id="delete_btn" data-book-id="{{ $book->id }}">Delete</button>
        @endif
    </div>
</div>