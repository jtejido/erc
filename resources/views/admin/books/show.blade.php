@extends('admin.layout')

@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
        }
        .file-preview-frame-custom {
            position: relative;
            display: inline-block;
            margin: 8px;
            border: 1px solid #ddd;
            box-shadow: 1px 1px 5px 0 #a2958a;
            padding: 6px;
        }
        .file-footer-caption {
            width: 130px !important;
        }
        .file-preview {
            border-radius: 0 !important;
        }
    </style>
@endsection

@section('content')
    <section class="content-header">
        <h1>View Book</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.books.index') }}">Books Management</a></li>
            <li>View book</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.form_errors')
                        @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                        <div class="pull-right">
                            <a href="{{ route('admin.books.edit', [$book->id]) }}"
                               class="btn btn-flat btn-sm btn-warning">
                                <i class="fa fa-edit"></i>&nbsp;Edit</a>
                        </div>
                        @endif
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="thumbnail btn-flat">
                                    @if($book->thumbnail)
                                        <img src="{{route('book.thumbnail', $book->thumbnail)}}" alt="ALT NAME" class="img-responsive" />
                                    @else
                                        <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                                    @endif

                                </div>
                            </div>
                            <div class="col-md-7">
                                <h2>{{ $book->name }}</h2>
                                <h3 class="price">{{ $book->getPrice() }}</h3>
                                <strong>SKU:</strong>&nbsp;<code>{{ $book->code }}</code>&nbsp;&nbsp;&nbsp;
                                <strong>Weight:</strong>&nbsp;<code>{{ $book->getWeight() }}</code>&nbsp;
                                <strong>Discounted:</strong>&nbsp;<code>{{ $book->getDiscountedPrice() }}</code>
                                <hr/>
                                <p>{!! $book->description  !!}</p>
                                <hr/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <h4>Associated File(s)
                                    @if(!$book->ebooks()->count())
                                    <button type="button"
                                            data-file_id="0"
                                            data-type="EBOOK_UPDATE"
                                            data-title=""
                                            data-label="Upload an eBook"
                                            class="btn-sm btn btn-flat btn-info pull-right"
                                            data-toggle="modal"
                                            data-target="#addFileModal">
                                        <i class="fa fa-plus"></i>&nbsp;Add New eBook
                                    </button>
                                    @else
                                    <button type="button"
                                            data-file_id="{{ $book->ebook()->id }}"
                                            data-type="EBOOK_UPDATE"
                                            data-title=""
                                            data-label="Update eBook"
                                            class="btn-sm btn btn-flat btn-info pull-right"
                                            data-toggle="modal"
                                            data-target="#addFileModal">
                                        <i class="fa fa-upload"></i>&nbsp;Update eBook
                                    </button>
                                    @endif
                                </h4>

                                <table class="table" id="bookEntryTable">
                                    <thead>
                                        <tr>
                                            <th width="1%">&nbsp;</th>
                                            <th width="25%">Title</th>
                                            <th width="20%">Last Updated</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(!$book->files->count())
                                        <tr class="no-books"><td colspan="4"><p class="text-center">No Files Associated with this book</p></td></tr>
                                    @else
                                        @foreach($book->ebooks() as $book_entry)
                                            <tr>
                                                <td><span class="fa fa-file-pdf-o" aria-hidden="true" title="Full eBook"></span></td>
                                                <td>{{ $book_entry->title }}</td>
                                                <td>{{ $book_entry->updated_at->toDateString() }}</td>
                                                <td>
                                                    {!! Form::open([
                                                            'route'     => ['admin.course.deletematerial', $book_entry->id],
                                                            'method'    => 'delete',
                                                            'class'     => 'form-inline deleteFileForm',
                                                    ]) !!}
                                                    <button type="button"
                                                            data-msg="Are you sure you want to delete this eBook and all the updated pages?"
                                                            class="btn btn-xs btn-flat btn-danger delete-file">
                                                        <i class="fa fa-trash"></i>
                                                    </button>&nbsp;
                                                    <a href="{{ route('admin.course.getmaterial', [$book_entry->id]) }}"
                                                       title="Download File {{ $book_entry->size }}"
                                                       class="btn btn-primary btn-xs btn-flat">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        @foreach($book->ebookUpdates()->sortByDesc('updated_at') as $book_entry)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>{{ $book_entry->title }}</td>
                                                <td>{{ $book_entry->updated_at->toDateString() }}</td>
                                                <td>
                                                    {!! Form::open([
                                                            'route'     => ['admin.course.deletematerial', $book_entry->id],
                                                            'method'    => 'delete',
                                                            'class'     => 'form-inline deleteFileForm',
                                                    ]) !!}
                                                    <button type="button"
                                                            data-msg="Are you sure you want to delete this file?"
                                                            class="btn btn-xs btn-flat btn-danger delete-file">
                                                        <i class="fa fa-trash"></i>
                                                    </button>&nbsp;
                                                    <a href="{{ route('admin.course.getmaterial', [$book_entry->id]) }}"
                                                       title="Download File {{ $book_entry->size }}"
                                                       class="btn btn-primary btn-xs btn-flat">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                <br/>
                                <div class="progress" style="display:none">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-lg-offset-3">
                                <h4>Associated Course(s)
                                    <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#courseModal">
                                        <i class="fa fa-link"></i>&nbsp;&nbsp;Link a Course
                                    </button>
                                </h4>
                                <table class="table">
                                    <tbody>
                                    @foreach($book->courses as $c)
                                        <tr>
                                            <td>{!! $c->getLinkedCourseTitleAdmin() !!}</td>
                                            <td>{{ $c->title }}</td>
                                        </tr>
                                    @endforeach
                                    @if(!$book->courses->count())
                                        <tr class="no-books"><td colspan="2"><p class="text-center">No Course Associated with this book</p></td></tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <!-- Modal -->
        @include('includes.confirmation_modal')
        @include('admin.courses._course-modal')
        {!! Form::open(['route'         => 'admin.books.associate',
                    'method'        => 'post',
                    'id'            => 'associateForm']) !!}
        {!! Form::hidden('course_id', '0') !!}
        {!! Form::hidden('item_id', $book->id) !!}
        {!! Form::hidden('item_type', 'Book') !!}
        {!! Form::close() !!}

        {!! Form::open(['route'         => 'admin.course.unlink.item',
                        'method'        => 'post',
                        'id'            => 'unlinkForm']) !!}
        {!! Form::hidden('course_id', '0') !!}
        {!! Form::hidden('item_id', $book->id) !!}
        {!! Form::hidden('item_type', 'Book') !!}
        {!! Form::close() !!}
        <div class="modal fade" id="addFileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.bookentry.store'),
                                    'id'        => 'addFileForm',
                                    'method'    => 'post',
                                    'files'     => true]) !!}
                    <input type="hidden" name="file_id" value="0" />
                    <input type="hidden" name="item_id" value="{{$book->id}}" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload an eBook</h4>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            {!! Form::label('title', 'Full eBook') !!}
                            {!! Form::text('title', null, [
                                    'class' => 'form-control',
                                    'placeholder'=>'Title (optional)'])  !!}
                        </div>
                        <div class="form-group">
                            <input type="file" id="file" name="file" class="form-control fileinput" placeholder="File"/>
                        </div>
                        <hr/>
                        {!! Form::label('otherfiles', 'Specific Updated Pages') !!}
                        <input type="file" multiple id="otherfiles" name="otherfiles[]" class="form-control" />
                        <p class="help-block">To select multiple files, click CTRL + (choose files)</p>
                        <hr/>
                        <div class="callout callout-info" style="margin: 0">
                            <h4>Notice!</h4>
                            On save, the system will send out an email notification to active subscribers for this book containing the links to these files.
                        </div>
                    </div>

                    <div class="modal-footer">
                        <label><input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;Do Not Send Email Notification</label>&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>


    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

            var $file = $('#file'), $addFileModal = $('#addFileModal');

            $file.fileinput({
                showPreview: false,
                showUpload: false,
            });

            $file.on('fileloaded', function(event, file, previewId, index, reader) {
                if ($addFileModal.find("input[name='title']").val().trim() == '')
                    $addFileModal.find("input[name='title']").val(file.name);
            });

            $("#otherfiles").fileinput({
                showPreview: true,
                showUpload: false,
                layoutTemplates: {
                    actions: '<div class="file-actions">\n' +
                    '    <div class="file-footer-buttons">\n' +
                    '        {upload} {delete} {other}' +
                    '    </div>\n' +
                    '    {drag}\n' +
                    '    <div class="clearfix"></div>\n' +
                    '</div>',
                },
                previewTemplates: {
                    pdf: '<div class="file-preview-frame-custom" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}">\n' +
                        '   {footer}\n' +
                        '<input type="text" name="otherFilesNames[]" value="{caption}" class="form-control"/>' +
                    '</div>',
                    image: '<div class="file-preview-frame-custom" id="{previewId}" data-fileindex="{fileindex}" data-template="{template}">\n' +
                    '   {footer}\n' +
                    '<input type="text" name="otherFilesNames[]" value="{caption}" class="form-control"/>' +
                    '</div>',
                },

            });

            $('.deleteFileForm').on('click', '.delete-file', function(e){
                var     $that = $(this),
                        msg = $that.data('msg');
//                console.log($that.data('msg'));
                Modal.showConfirmationModal(
                        msg,
                        'Delete File',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
                return false;
            });

            $addFileModal.on('hidden.bs.modal', function () {
                $("#addFileForm").trigger("reset");
                $('.fileinput').fileinput('reset');
            });
            $addFileModal.on('show.bs.modal', function (event) {
                var     modal = $(this),
                        button = $(event.relatedTarget),
                        label = button.data('label'),
                        title = button.data('title'),
                        file_id = button.data('file_id'),
                        type = button.data('type');
                modal.find('.modal-title').text(label);
                if (type != '') modal.find('.modal-body input[name=type]').val(type);
                modal.find('.modal-body input[name=title]').val(title);
                modal.find('.modal-content input[name=file_id]').val(file_id);
            });
            $('#courseList').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 2 }
                ]
            });
            var     $associateForm = $("#associateForm"),
                    $unlinkForm = $("#unlinkForm");
            var linkItem = function($that) {
                $associateForm.find("input[name='course_id']").val($that.data('id'));
                $associateForm.submit();
            }

            var unLinkItem = function($that) {
                $unlinkForm.find("input[name='course_id']").val($that.data('id'));
                $unlinkForm.submit();
            }

            $('#courseList').on('click', '.associateBook', function(e) {
                e.preventDefault();
                var $that = $(this), msg;

                msg = "Are you sure you want to link this item with this course?";
                msg += "<br/>";
                msg += '"' + $that.data('sku') + " - " + $that.data('title') + '"';

                $('#courseModal').modal('hide').one('hidden.bs.modal', function () {
                    Modal.showConfirmationModal(
                            msg,
                            'Associate Item With Course',
                            function() {
                                linkItem($that);
                                return false;
                            });
                });

                return false;
            });
        })(jQuery);
    </script>
@endsection
