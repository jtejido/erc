@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Add Book</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.books.index') }}">Books Management</a></li>
            <li>Add Book</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.form_errors')
                    </div>

                    <div class="box-body">
                        {!! Form::open(['url'       => route('admin.books.store'),
                                        'method'    => 'post',
                                        'files'     => true,
                                        'class'     => 'status-filter form-horizontal']) !!}
                        @include('admin.books._form')
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>


    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){
            $("#thumbnail").fileinput({
                showPreview: false,
                showUpload: false
            });
        })(jQuery);
    </script>
@endsection
