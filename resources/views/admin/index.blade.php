@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Customers</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header box-header-users">
                <button class="btn btn-flat btn-success pull-right" id="add-customer">

                    <i class="fa fa-plus"></i> Add Customer
                </button>
            </div>

            <div class="box-body">

                @include('admin.includes.success_message')
                @include('errors.form_errors')

                @if (Session::has('user'))
                    <input type="hidden" id="searchkey" value="{{ Session::get('user')->email }}" data-column="4">
                @endif

                @if ($customers->count())

                    <table id="table-clients-ajax" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>CourseMill User Id</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                    </table>

                    <button class="btn btn-flat btn-primary" id="merge-customers">
                       Merge Customers
                    </button>
                @else
                    <i>No Clients.</i>
                @endif
            </div>
        </div>

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

        <!-- Modal for adding / editing contact -->

        <div class="modal fade" id="contactModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('javascripts')
    @parent

    <script>
        $(document).ready(function() {
            $('#add-customer').on('click', function() {
                var $this    = $(this),
                    modal    = $('#contactModal');

                $.ajax({
                    type : 'GET',
                    url  : "{{ route('contact.form.get') }}",
                    data : {
                        email : true,
                        customer : true
                    },
                    success : function(response) {
                        modal.find('.modal-content').html(response);
                        $('#phone, #zip').NumericOnly();
                    }
                }).done(function() {
                    $("[data-mask]").inputmask();

                    $('.state').select2({
                        placeholder: "Select State"
                    });
                    $( "input#company" ).autocomplete({
                        source: "/api/companies",
                        minLength: 2,
                    });
                    $( "input#company" ).autocomplete("option", "appendTo", "#contactModal");

                });

                modal.find('.modal-content').removeClass('panel-danger');
                modal.modal('show');
            });

            $('#merge-customers').on('click', function() {
                var customers = [],
                    $checkBoxes = $('.customer-checkbox'),
                    $modal = $('#contactModal'),
                    $modalContent = $modal.find('.modal-content');

                $checkBoxes.each(function() {
                    if ($(this).is(':checked')) {
                        customers.push($(this).val());
                    }
                });

                if (customers.length > 1) {
                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('customers.merge.get')  }}",
                        data : { customer_ids : customers },
                        success : function (response) {
                            if (response) {
                                $modalContent.html(response);
                                $modal.modal('show');
                            }
                        }
                    })
                }
            });

            $('body').on('submit', 'form#merge-customers', function(e) {
                var selected = [],
                    $checkBoxes = $(this).find('.customer_id'),
                    $alert = $(this).find('.alert');

                $checkBoxes.each(function() {
                    if ($(this).is(':checked')) {
                        selected.push($(this).val());
                    }
                });

                if (!selected.length) {
                    e.preventDefault();

                    $alert.text('Please select a customer.')
                        .show();
                }
            });

            $('body').on('change', '.customer_id', function() {
                if ($(this).prop('checked')) {
                    $(this).parents('.modal-body').find('.alert').hide();
                }
            });
        });

        $('#table-clients-ajax').DataTable({
            "stateSave": true,
            "pageLength": 20,
            "processing": true,
            "serverSide": false,
            "order": [[ 2 , "desc"]],
            "ajax": "/admin/index_data",
            "columns": [
                { "data": "customer_cbox" },
                { "data": "first_name" },
                { "data": "last_name" },
                { "data": "company" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "course_mill_user_id" },
                { "data": "action" }
            ],
            "columnDefs": [
                { "searchable": false,
                    "orderable": false,
                    "targets": [0],
                    "sortable": false
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [1],
                    "sortable": true
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [2],
                    "sortable": true
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [3],
                    "sortable": true
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [4],
                    "sortable": true
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [5],
                    "sortable": true
                },
                { "searchable": true,
                    "orderable": true,
                    "targets": [6],
                    "sortable": true
                },
                { "searchable": false,
                    "orderable": false,
                    "targets": [7],
                    "sortable": false
                }

            ],
        });

    </script>
@endsection
