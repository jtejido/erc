@extends('admin.layout')

@section('body-class', 'index')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _tips = {!! $tips->toJson() !!};
    data.tips = _tips;
</script>
<div class="container-fluid" ng-controller="AdminTips">
    <section class="content-header">
        <h1>Tips of the Week <a href="/admin/tips/add"><button class="btn btn-success">New</button></a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header box-header-users">
                        <div class="filters pull-right">
                            <form action="" method="GET">
                                <input type="text" class="form-control" name="title" value="{{ isset($title) ? $title : '' }}" style="display: inline; height: 35px !important;" placeholder="Tip of the Week Title">
                                <button type="submit" class="btn btn-flat btn-primary"><span class="fa fa-search"></span></button>
                                <a href="{{ route('admin.tips.index') }}" class="btn btn-success btn-flat">Clear</a>
                            </form>
                        </div>
                    </div>

                    <div class="box-body">

                        @if ($tips->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Publish Date</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                @foreach ($tips as $tip)
                                    <tr>
                                        <td data-title="'Title'" sortable="'title'">
                                            <strong>{{ $tip->title }}</strong>
                                            @if ($tip->cat)
                                                <p><em>{{$tip->cat->name}}</em></p>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $tip->published_date->format('M d, Y') }}
                                            @if (!$tip->is_published)
                                            <span class="label label-primary">Draft</span>
                                            @endif
                                        </td>
                                        <td>{{ $tip->created_at }}</td>
                                        <td>
                                            <a href="/admin/tips/edit/{{$tip->id}}">
                                                <button type="button" class="btn btn-primary">View</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                            <div class="pull-right">
                                {!! $tips->appends([$page, $title])->render() !!}
                            </div>
                        @else
                            <p class="text-center">Currently, there are no tips.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
