@extends('admin.layout')

@section('body-class', 'add')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }

    var _categories = {!! $categories->toJson() !!};
    data.categories = _categories;
    @if (@$tip)
    var tip = {!! $tip->toJson() !!};
    data.tip = tip;
    @endif
</script>
<div class="container-fluid" ng-controller="AdminTipsAdd">
    <section class="content-header">
        <h1>
            Tips of the Week
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.tips.index') }}">Tips of the Week</a></li>
            <li class="active">New</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Entry</h3>
                </div>
                <form name="tipsAdd" ng-submit="save()" novalidate role="form" enctype="multipart/form-data">
                    <input ng-if="data.tip.id" type="hidden" id="id" value="@{{ data.tip.id }}">
                    <div class="box-body">
                        <div class="callout callout-danger" ng-show="formSubmitError" ng-cloak>
                            <h4>Issues encountered:</h4>
                            <p>@{{ formSubmitErrorMessage }}</p>
                        </div>
                        <div class="callout callout-success" ng-show="formSubmitSuccess" ng-cloak>
                            <h4>Tip of the week saved!</h4>
                        </div>
                        <div class="callout callout-success" ng-show="formDestroySuccess" ng-cloak>
                            <h4>Deleting Tip successful!</h4>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="photo">
                                    Photo <span ng-if="data.tip.id && !data.tip.is_published"><span class="label label-primary">Draft</span></span>
                                </label>
                                <input name="photo" type="file" class="input-lg" id="photo"
                                       accept="image/*">
                            </div>
                            <div class="form-group col-md-6">
                                @if (isset($tip) and $tip->photo and $tip->photo_mime)
                                    <label for="">Current Photo</label>
                                    <img src="{{ route('tip.photo', $tip->id) }}" alt="Tip Photo" style="width: 100%; height: 100%;">
                                @endif
                            </div>
                            <div class="form-group col-md-8 col-sm-8"
                                ng-class="{
                                    'has-error' : tipsAdd.title.$invalid && formSubmitInvalid,
                                    'has-success': tipsAdd.title.$valid
                                }">
                                <label for="title">
                                    Title <span class="required">*</span> <span ng-if="data.tip.id && !data.tip.is_published"><span class="label label-primary">Draft</span></span>
                                </label>
                                <input type="text" class="form-control input-lg" id="title" placeholder="Title"
                                    name="title"
                                    ng-model="data.tip.title"
                                    tabindex="1"
                                    required>
                                <p ng-show="tipsAdd.title.$invalid && formSubmitInvalid" class="help-block">A title is required.</p>
                            </div>
                              <div class="form-group col-md-4 col-sm-4">
                                <label for="category_id">Category <span class="required">*</span></label>
                                <select name="category_id" class="form-control" ng-model="data.tip.category"
                                    ng-options="category.name for category in categories track by category.id">
                                </select>
                              </div>
                          </div>
                        <div class="form-group"
                            ng-class="{
                                    'has-error' : tipsAdd.content.$invalid && formSubmitInvalid,
                                    'has-success': tipsAdd.content.$valid
                            }">
                            <label>Content <span class="required">*</span></label>
                            <textarea class="form-control" rows="3" name="content" placeholder="Content" ui-tinymce="getTinymceOptions()" ng-model="data.tip.content" required></textarea>
                            <p ng-show="tipsAdd.content.$invalid && formSubmitInvalid" class="help-block">Content is needed.</p>
                        </div>
                        <h4>Published Date</h4>
                        <div class="row">
                            <div class="form-group col-md-2 col-sm-2">
                                  <label for="day">Month</label>
                                  <!--
                                  <input type="text" name="month" class="form-control" ng-model="data.tip.published_date.month">
                                  -->
                                <select name="month" class="form-control" ng-model="data.tip.published_date.month"
                                    ng-options="month as month for month in months">
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-sm-2"
                                ng-class="{
                                    'has-error' : tipsAdd.day.$invalid && formSubmitInvalid,
                                    'has-success': tipsAdd.day.$valid
                                }">
                                <label for="day">Day</label>
                                <input type="number" min="1" max="31" step="1" name="day" class="form-control" placeholder="@{{ data.tip.published_date.day }}"
                                    ng-model="data.tip.published_date.day"
                                    ng-pattern="/^([1|2][0-9])|[1-9]|(3(0|1))$/"
                                    required>
                                <p ng-show="tipsAdd.title.$invalid && formSubmitInvalid" class="help-block">Date of publish required.</p>
                            </div>
                            <div class="form-group col-md-2 col-sm-2">
                                <label for="year">Year</label>
                                <input type="number" name="year" class="form-control" ng-model="data.tip.published_date.year">
                            </div>
                          </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" ng-class="{
                                    'has-error' : tipsAdd.title.$invalid && formSubmitInvalid,
                                    'has-success': tipsAdd.title.$valid
                                }">
                                    <label for="slug">
                                        Slug
                                    </label>
                                    <input type="text" class="form-control" id="slug" placeholder="Slug"
                                           name="slug"
                                           ng-model="data.tip.slug"
                                           tabindex="1"
                                           required>
                                    <p ng-show="tipsAdd.slug.$invalid && formSubmitInvalid" class="help-block">A slug is required.</p>
                                </div>
                            </div>
                        </div>
                          <div ng-if="!data.tip.id">
                                <button type="button" class="btn btn-success" ng-click="publish()">Save and Publish</button>
                                <button type="button" class="btn btn-primary"
                                    ng-disabled="formSubmitInvalid && tipsAdd.$invalid"
                                    ng-click="save()">Save Draft</button>
                          </div>
                          <div ng-if="data.tip.id">
                            <span ng-if="data.tip.is_published">
                                <button type="button" class="btn btn-success" ng-click="publish()">Update</button>
                                <button type="button" class="btn btn-warning"
                                    ng-disabled="formSubmitInvalid && tipsAdd.$invalid"
                                    ng-click="save({is_published: false})">Save and Unpublish</button>
                            </span>
                            <span ng-if="!data.tip.is_published">
                                <button type="button" class="btn btn-success" ng-click="publish()">Publish</button>
                                <button type="button" class="btn btn-warning"
                                    ng-disabled="formSubmitInvalid && tipsAdd.$invalid"
                                    ng-click="save()">Update</button>
                            </span>

                            <button type="button" class="btn btn-danger" ng-click="delete()">Delete</button>
                          </div>
                    </div><!-- .box-body -->
                </form>
            </div><!-- .box -->
        </div>
    </section>
</div>
@include('includes.confirmation_modal')
@endsection
