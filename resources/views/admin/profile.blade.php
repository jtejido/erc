@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1 class="cart-header">
            Edit My Profile
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', ['user_id' => $user->id]) }}">User</a></li>
            <li><a href="">Transaction</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row payment-details">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Email
                            </h3>
                        </div>
                    </div>

                    {!! Form::open([
                        'method' => 'POST',
                        'url' => route('email.edit_profile'),
                        'class' => 'edit-profile-form',
                        ]) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_email'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                        {!! Form::rLabel('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('email', 'email', isset($user) ? $user->email : '', [
                            'class' => 'form-control',
                            'id'    => 'email',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('email'))
                                {!! $errors->first('email', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('password', 'Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'password', '', [
                            'class' => 'form-control',
                            'id'    => 'password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}
                            <span class="required block small text-right">Confirm password to change email.</span>

                            @if ($errors->has('password'))
                                {!! $errors->first('password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Password
                            </h3>
                        </div>
                    </div>

                    {!! Form::open([
                        'method' => 'POST',
                        'url' => route('password.edit_profile'),
                        'class' => 'edit-profile-form',
                        ]) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_password'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                        {!! Form::rLabel('old_password', 'Old Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'old_password', '', [
                            'class' => 'form-control',
                            'id'    => 'old_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('old_password'))
                                {!! $errors->first('old_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('new_password', 'New Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'new_password', '', [
                            'class' => 'form-control',
                            'id'    => 'new_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('new_password'))
                                {!! $errors->first('new_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('re_new_password', 'Confirm New Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 're_new_password', '', [
                            'class' => 'form-control',
                            'id'    => 're_new_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('re_new_password'))
                                {!! $errors->first('re_new_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Personal Information
                            </h3>
                        </div>
                    </div>

                    {!! Form::open(['method' => 'POST', 'url' => route('personal_info.edit_profile'),  'class' => 'edit-profile-form']) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_personal_info'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                        {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'first_name', isset($userDetails) ? $userDetails->first_name : '', [
                            'class' => 'form-control',
                            'id'    => 'first_name',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('first_name'))
                                {!! $errors->first('first_name', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'last_name', isset($userDetails) ? $userDetails->last_name : '', [
                            'class' => 'form-control',
                            'id'    => 'last_name',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('last_name'))
                                {!! $errors->first('last_name', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('company', 'Company: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'company', isset($userDetails) ? $userDetails->company : '', [
                            'class' => 'form-control',
                            'id'    => 'company',
                            'style' => 'display: inline;',
                            'required' => 'required',
                            ]) !!}

                            @if ($errors->has('company'))
                                {!! $errors->first('company', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'title', isset($userDetails) ? $userDetails->title : '', [
                            'class' => 'form-control',
                            'id'    => 'company',
                            'style' => 'display: inline;',
                            ]) !!}

                            @if ($errors->has('title'))
                                {!! $errors->first('title', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('address1', 'Address1: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'address1', isset($userDetails) ? $userDetails->address1 : '', [
                            'class' => 'form-control',
                            'id'    => 'address1',
                            'style' => 'display: inline;',
                            'required' => 'required',
                            ]) !!}

                            @if ($errors->has('address1'))
                                {!! $errors->first('address1', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('address2', 'Address2: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'address2', isset($userDetails) ? $userDetails->address2 : '', [
                            'class' => 'form-control',
                            'id'    => 'address2',
                            'style' => 'display: inline;'
                            ]) !!}

                            @if ($errors->has('address2'))
                                {!! $errors->first('address2', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('city', 'City: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'city', isset($userDetails) ? $userDetails->city : '', [
                            'class' => 'form-control',
                            'id'    => 'city',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('city'))
                                {!! $errors->first('city', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('state', 'State: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::select('state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->state : '', [
                                'id'    => 'state',
                                'class' => 'form-control',
                                'style' => 'display: inline;',
                                'required' => 'required'
                            ]) !!}

                            @if ($errors->has('state'))
                                {!! $errors->first('state', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('zip', 'Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'zip', isset($userDetails) ? $userDetails->zip : '', [
                            'class' => 'form-control',
                            'id'    => 'zip',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('zip'))
                                {!! $errors->first('zip', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('phone', 'Phone: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'phone',  isset($userDetails) ? $userDetails->phone : '', [
                                'class' => 'form-control',
                                'id'    => 'phone',
                                'style' => 'display: inline;',
                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                'data-mask',
                                'required' => 'required',
                            ]) !!}

                            @if ($errors->has('phone'))
                                {!! $errors->first('phone', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('fax', 'Fax: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('tel', 'fax', isset($userDetails) ? $userDetails->fax : '', [
                                'class' => 'form-control',
                                'id'    => 'fax',
                                'style' => 'display: inline;',
                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                'data-mask',
                                'placeholder' => 'Fax'
                            ]) !!}

                            @if ($errors->has('fax'))
                                {!! $errors->first('fax', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary">Save</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

@endsection
