@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Edit Location</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.locations.index') }}">Locations</a></li>
            <li>Edit Location</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @include('admin.includes.form_errors')
                    </div>

                    <div class="box-body">
                        {!! Form::model($location, [
                                        'url'       => route('admin.locations.update', [$location->id]),
                                        'method'    => 'put',
                                        'class'     => 'status-filter form-horizontal'
                                        ]) !!}
                        @include('admin.locations._form')
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>


    </section>
    @include('includes.confirmation_modal')
@endsection
