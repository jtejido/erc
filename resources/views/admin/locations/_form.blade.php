<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::rLabel('location_name', 'Name    ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('location_name', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('address', 'Address', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('address', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('city', 'City', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('city', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('state', 'State', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('state', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('zip', 'Zip', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('zip', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('general_location_id', 'General City', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::select('general_location_id', $general_locations, null, ['class' => 'form-control'])  !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::rLabel('phone_number', 'Phone Number', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('phone_number', null, $attributes = ['class' => 'form-control', 'data-inputmask' => '"mask" : "(999) 999-9999"',
                'data-mask'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('url', 'Website', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('url', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::textarea('description', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <p class="pull-right">
            <a href="{{ route('admin.locations.index') }}" class="btn btn-flat btn-default">Back</a>
            <button type="submit" class="btn btn-flat btn-primary">Save</button>
        </p>
    </div>
    <div class="col-md-6">
    </div>
</div>