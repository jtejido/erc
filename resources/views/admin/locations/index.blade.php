@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Locations</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Locations</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')

                        <a href="{{ route('admin.locations.create') }}" class="pull-right btn btn-success btn-flat">Add Location</a>

                    </div>

                    <div class="box-body">


                            <table id="table-classes-cert" class="table table-bordered table-hover table-classes">
                                <thead>
                                <tr>
                                    <th width="1%">ID</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Url</th>
                                    <th>Description</th>
                                    <th>Phone Number</th>
                                    <th width="1%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($locations as $location)
                                        <tr>
                                            <td>{{ $location->id }}</td>
                                            <td>{{ $location->location_name }}</td>
                                            <td>{{ $location->location }}</td>
                                            <td>{{ $location->url }}</td>
                                            <td>{{ $location->description }}</td>
                                            <td>{{ $location->phone_number }}</td>
                                            <td>
                                                {!! Form::open([
                                                    'url'       => route('admin.locations.delete', [$location->id]),
                                                    'method'    => 'delete'
                                                ]) !!}
                                                @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                                                <a href="{{ route('admin.locations.edit', [$location->id]) }}"
                                                   class="btn btn-primary btn-flat btn-block">Edit</a>
                                                @if(!$location->hasClass)
                                                <button type="button"
                                                        class="submitButton btn btn-danger btn-flat btn-block">Delete</button>
                                                @endif
                                                @endif
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </div>


    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#table-classes-cert').on('click', '.submitButton', function() {
                var $that = $(this);
                Modal.showConfirmationModal(
                        'Are you sure you want to delete this location?',
                        'Delete Location',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
            });
            $('#table-classes-cert').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 5 }
                ],
                "order": [[ 0, "desc" ]]
            });
        });
    </script>
@endsection

