<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::rLabel('slug', 'Slug', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('slug', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('location', 'Location', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('location', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <p class="pull-right">
            <a href="{{ route('admin.general_locations.index') }}" class="btn btn-flat btn-default">Back</a>
            <button type="submit" class="btn btn-flat btn-primary">Save</button>
        </p>
    </div>
    <div class="col-md-6">
    </div>
</div>