@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>On-Site Training</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">On-Site Training</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <div class="box">
                    <div class="box-header box-header-users">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                    </div>
                    <div class="box-body box-onsite-training">

                        {!! Form::open(['id'     => 'training-form',
                                        'files'     => true]) !!}

                        {!! Form::hidden('type', 'MATERIAL') !!}

                        @if (Session::has('message'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::label('agent', 'Agent: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            <p>{{ $user->contact->first_name }} {{ $user->contact->last_name }}</p>
                            <input type="hidden" name="originating_agent_user_id" value="{{ $user->id }}">
                            <input type="hidden" name="price" value="0">
                        </div>

                        {!! Form::rLabel('date', 'Training Date: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'training_date', '', [
                            'class' => 'form-control',
                            'id'    => 'trainingDate',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('date'))
                                {!! $errors->first('date', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>
                        {!! Form::label('files', 'Files: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            <input type="file" multiple name="files[]" class="form-control" id="files" />
                        </div>


                        {!! Form::rLabel('address', 'Address: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::textarea('address', '', [
                            'class' => 'form-control',
                            'id'    => 'address',
                            'style' => 'display: inline;',
                            'rows'  => 5,
                            'required'
                            ]) !!}

                            @if ($errors->has('date'))
                                {!! $errors->first('date', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            {{-- */
                                $params = [
                                       'contact_id' => $user ? $user->contact->id : null,
                                       'user_id' => $user ? $user->id : null,
                                       ];
                            /* --}}

                            <div>
                                <label><input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                                    Do Not Email Agent And Attendees</label><br/>
                            </div>

                            <a href="{{ route('admin.user_details', array_filter($params)) }}"
                               class="btn btn-flat btn-primary">Back</a>
                            <button id="onsite-training-save-btn" class="btn btn-flat btn-success">
                                Save
                            </button>&nbsp;

                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            @include('includes.notification_modal')
        </div>
    </section>

@endsection

@section('javascripts')
    @parent

    <script>
        (function($) {
            var submit = false,
                $form  = $('#training-form');

            $form.submit(function(e) {
                Modal.showConfirmationModal(
                    'Are you sure you want to create this onsite training?',
                    'Notification',
                    function () {
                        $form.unbind().submit();
                        return false;
                    }
                );
                return false;
            });

            $("#files").fileinput({
                showPreview: false,
                showUpload: false,
            });

        })(jQuery);
    </script>
@endsection
