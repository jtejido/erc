@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>@if ($orderItemId) Swap @endif Attendees (With Related Classes)</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">@if ($orderItemId) Swap @endif Attendees</a></li>
        </ol>
    </section>

    {{-- */ $registrable = $class ?: $course; $classId = $class ? $class->id : 0; /* --}}

    <section class="content contacts-related">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        <div class="pull-right">
                        <button
                           id="hide-related"
                           class="btn btn-default btn-flat">Hide Related Classes</button>
                        <button class="btn btn-flat btn-success" id="add-contact"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-user_id="{{ $user->id }}"
                                data-url="{{ route('contact.form.get') }}">

                            <i class="fa fa-plus"></i> Add Contact
                        </button>

                        </div>

                        <label>
                            <h4 style="display: inline">{{ $class->title }}</h4>
                        </label>
                        <br/>
                        <small>{!!$class->titleLocationLink!!}</small>
                        <br/>



                    </div>

                    <div class="box-body">
                        {!! Form::open([
                            'url'     => route('classes.register.store'),
                            'method'  => 'POST'
                        ]) !!}

                        {!! Form::hidden('registration', null) !!}
                        {!! Form::hidden('user_id', $user->id) !!}


                        @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @include('errors.form_errors')

                        <table class="table table-bordered table-hover" id="table-contacts-reg">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="check-all">&nbsp;Name</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            @if (!OrderUtility::isRegistered($user->contact->id, $registrable) || $includeAll)

                                <tr>
                                    <td>
                                        <label for="contact_{{ $user->contact->id }}" title="{{ $user->contact->id }}">
                                            {{ $user->contact->name }}
                                        </label>
                                        <small>{{ $user->email }}</small>
                                        <ul class="classes main_list class_list" data-contact="{{ $user->contact->id }}">
                                            <li>
                                                <label>
                                                    {!! Form::checkbox('classes[]', $class->id, false, ['class' => 'check-single']) !!}
                                                    &nbsp;{{ $class->title }}
                                                </label><br/>
                                                <small>{!!$class->titleLocationLink!!}</small>
                                            </li>
                                            <li class="related-head">
                                                <hr/>
                                                <strong>Related Classes</strong></li>
                                            @foreach($class->classes as $mclass)
                                                <li class="related">
                                                    <label>
                                                        {!! Form::checkbox('classes[]', $mclass->id, false, ['class' => 'check-single']) !!}
                                                        &nbsp;{{ $mclass->title }}
                                                    </label><br/>
                                                    <small>{!!$mclass->titleLocationLink!!}</small>
                                                    <small>@ {{ $mclass->price }} per person</small>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>

                                    <td>{{ $user->contact->phone }}</td>
                                    <td>
                                        <p>{{ $user->contact->address1 }} {{ $user->contact->address2 }}</p>
                                        <p class="block">{{ $user->contact->city }} {{ $user->contact->state }} {{ $user->contact->zip }}</p>
                                    </td>
                                    <td>
                                        @if ($user->contact->generalNote)
                                            {!!  $user->contact->generalNote->content  !!}
                                        @else
                                            --
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.profile.edit', ['contact_id' => $user->contact->id]) }}"
                                           class="btn btn-primary btn-block btn-flat">
                                            Edit Profile
                                        </a>
                                    </td>
                                </tr>

                            @endif

                            @if ($contacts->count())
                                @foreach ($contacts as $contact)

                                    @if (!OrderUtility::isRegistered($contact->id, $registrable) || $includeAll)

                                        <tr>
                                            <td>
                                                <label for="contact_{{ $contact->id }}" title="{{ $contact->id }}">
                                                    {{ $contact->name }}
                                                </label>
                                                @if ($contact->user)
                                                    <small>{{ $contact->user->email }}</small>

                                                    @if ($contact->user->hasSubscription and !count($noSwapYearlySubs))
                                                        <p class="block label label-info btn-flat btn-block">
                                                            Has Yearly Subscription
                                                        </p>
                                                    @endif
                                                @endif

                                                <ul class="classes class_list" data-contact="{{ $contact->id }}">
                                                    <li>
                                                        <label>
                                                            {!! Form::checkbox('classes[]', $class->id, false, ['class' => 'check-single']) !!}
                                                            &nbsp;{{ $class->title }}
                                                        </label><br/>
                                                        <small>{!!$class->titleLocationLink!!}</small>
                                                    </li>
                                                    <li class="related-head">
                                                        <hr/>
                                                        <strong>Related Classes</strong></li>
                                                    @foreach($class->classes as $mclass)
                                                        <li class="related">
                                                            <label>
                                                                {!! Form::checkbox('classes[]', $mclass->id, false, ['class' => 'check-single']) !!}
                                                                &nbsp;{{ $mclass->title }}
                                                            </label><br/>
                                                            <small>{!!$mclass->titleLocationLink!!}</small>
                                                            <small>@ {{ $mclass->price }} per person</small>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>{{ $contact->phone }}</td>
                                            <td>
                                                <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                                <p class="block">{{ $contact->city }} {{ $contact->state }} {{ $contact->zip }}</p>
                                            </td>
                                            <td>
                                                @if ($contact->generalNote)
                                                    {!!  $contact->generalNote->content  !!}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.profile.edit', ['contact_id' => $contact->id]) }}"
                                                   class="btn btn-primary btn-block btn-flat">
                                                    Edit Profile
                                                </a>
                                            </td>
                                        </tr>

                                    @endif

                                @endforeach
                            @endif

                        </table>

                        <button class="btn btn-flat btn-primary"
                                id="register-classes"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-agent="{{ $user->id }}"
                                data-url="{{ route('contact.add_to_class') }}">
                                Add To Registration
                        </button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>


    <!-- Confirmation modal -->

    @include('includes.alert_modal')

    <!-- Modal for adding / editing class -->

        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {
            var oTable = $("#table-contacts-reg").DataTable({
                "columnDefs" : [{
                    "targets": 0,
                    "orderable": false,
                    "searchable": false
                }],
                "order": []
            });
            oSettings = oTable.settings();
            $('#register-classes').on('click', function() {

                // display all rows
                oSettings[0]._iDisplayLength = oSettings[0].fnRecordsTotal();
                oTable.search( '' ).draw();

                var registration = [], $form = $(this).closest('form'), reg_count = 0;

                // traverse classes
                $('.main_list').find('input[type="checkbox"]:not(.hide)').each(function() {
                    // traverse contacts

                    var obj = {}, contacts = [], class_id;
                    obj.class_id = class_id = $(this).val();
                    $('.class_list').each(function () {
                        var obj = {}, contact_id = $(this).data('contact'), classes = [];
                        $(this).find('input[type="checkbox"]').each(function () {
                            if ($(this).val() == class_id && $(this).is(':checked')) {
                                contacts.push(contact_id);
                                reg_count++;
                            }
                        });
                    });

                    if (contacts.length) {
                        obj.contacts = contacts;
                        registration.push(obj);
                    }
                });


                var reg = JSON.stringify(registration);

                $form.find('input[name="registration"]').val(reg);

                if(reg_count == 0) {
                    Modal.showAlertModal('Please select at least one contact to register.', 'Registration');
                    return false;
                }

                $form.submit();

                return false;
            });

            $('#hide-related').on('click', function(e) {
                e.preventDefault();
                if($(this).text() == "Hide Related Classes") {
                    $(this).text("Show Related Classes")
                    $('li.related').find(".check-single").addClass('hide');
                } else {
                    $(this).text("Hide Related Classes")
                    $('li.related').find(".check-single").removeClass('hide');
                }
                $('li.related-head').toggle();
                $('li.related').toggle();

                $('li.related').find(".check-single").attr('checked', false);
            });
        })(jQuery);
    </script>
@endsection

