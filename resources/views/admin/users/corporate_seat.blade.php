@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1>
            <a href="{{ route('admin.user_details', $params) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Corporate Seats
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">Corporate Seats</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="box-corp-seat-container">
                <div class="box">
                    <div class="box-header box-header-users">
                        <h4>Corporate Seats List</h4>
                    </div>
                    <div class="box-body box-corp-seats">

                        @include('includes.success_message')

                        @foreach($corpSeats as $corpSeat)
                            <div class="seat-container">
                                <input type="radio" name="corporate_seat" class="seat-options" value="{{ $corpSeat->id }}">
                                <span class="credit_number">{{ $corpSeat->credit_number }} Seats</span>
                                <span class="price pull-right">
                                    $ <input type="text" class="corp-seat" value="{{ $corpSeat->price }}">
                                    <button class="btn btn-flat btn-sm btn-success btn-save-seat" data-id="{{ $corpSeat->id }}">Save</button>
                                    <button class="btn btn-flat btn-sm btn-danger btn-delete-seat" data-id="{{ $corpSeat->id }}">Delete</button>
                                </span>
                            </div>
                        @endforeach

                        <div class="transaction-buttons">
                            <p class="required">* For Custom Number of Seats create a new one and save.</p>
                        </div>

                        <div class="seat-container">
                            <span class="credit_number"> <input type="text" id="new-corp-seat-number" style="width: 50px;"> Seats</span>
                            <span class="price pull-right">
                                $ <input type="text" id="new-corp-seat-price">
                                <button class="btn btn-flat btn-sm btn-success btn-save-new-seat" data-user="{{ $user->id }}">Add To Cart</button>
                            </span>
                        </div>

                        <div class="transaction-buttons">
                            <p class="required">
                                Any saved changes in the price will be permanent.
                            </p>

                            <a class="btn btn-flat btn-primary"
                               href="{{ route('admin.user_details', $params) }}">
                                <i class="fa fa-arrow-circle-o-left"></i>
                                Return
                            </a>
                            <button class="btn btn-flat btn-success" id="btn-add"
                                    href="javascript:void(0)"
                                    data-user="{{ $user->id }}">
                                <i class="fa fa-shopping-cart"></i>
                                Add to Cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.notification_modal')
    </section>

@endsection

@section('javascripts')
    @parent

    <script>
        (function($) {
            $('#btn-add').on('click', function() {
                var userId = $(this).data('user'),
                    id     = $('input[name="corporate_seat"]:checked').val();

                if (typeof id != 'undefined') {
                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('client.add_to_credit') }}",
                        data : {
                            is_admin: true,
                            corp_seat_id : id,
                            user_id      : userId
                        },
                        success : function (response) {
                            window.location.href = response;
                        }
                    });
                }
            });

            $('.btn-save-new-seat').on('click', function () {
                var $this = $(this),
                    userId = $this.data('user'),
                    number = $('#new-corp-seat-number').val(),
                    price = $('#new-corp-seat-price').val();

                if ($.trim(number).length && $.trim(price).length)  {
                    $.ajax({
                        type: 'POST',
                        url : "{{ route('client.add_to_credit') }}",
                        data: {
                            corp_seat_id: 0,
                            is_admin: true,
                            credit_number: number,
                            price: price,
                            user_id : userId
                        },
                        beforeSend: function() {
                            $this.attr('disabled', true).text('Saving')
                        },
                        success: function(response) {
                            setTimeout(function() {
                                $this.attr('disabled', false).text('Save');

                                window.location.href = response;
                            }, 1000);
                        }
                    })
                }
            });

            $('.btn-save-seat').on('click', function() {
                var $this = $(this),
                    corpSeatId = $this.data('id'),
                    value = $this.siblings('.corp-seat').val();

                if ($.trim(value).length && value != 0) {

                    $.ajax({
                        type : 'POST',
                        url : "{{ route('admin.save_corporate_seat') }}",
                        data : {
                            corp_seat_id : corpSeatId,
                            value : value
                        },
                        beforeSend : function(data) {
                            $this.text('Saving...');
                        },
                        success : function(response) {
                            if (response) {
                                setTimeout(function(){
                                    $this.text('Save');
                                }, 1500);
                            }
                        }
                    })
                }
            });

            $('.btn-delete-seat').on('click', function() {
                var $this = $(this),
                    id = $this.data('id');

                Modal.showConfirmationModal(
                    'Are you sure you want to delete this?',
                    'Deletion',
                    function() {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('admin.delete_corporate_seat') }}",
                            data: {
                                id: id
                            },
                            success: function(response) {
                                window.location.reload();
                            }
                        });
                    }
                );
            });
        })(jQuery);
    </script>
@endsection