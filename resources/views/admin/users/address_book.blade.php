@extends('admin.layout')

@section('content')
<div>
    
    <section class="content-header">
        <h1>{{ $user->contact->name }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.users') }}">Users</a></li>
            <li><a href="{{ route('admin.user_details', ['contact_id' => $user->contact_id, 'user_id' => $user->id]) }}">Profile</a></li>
            <li class="active">Address Book</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Address Book</h3>
                        <button class="btn btn-flat btn-success pull-right" id="add-contact"
                                data-user_id="{{ $user->id }}"
                                data-url="{{ route('contact.form.get') }}">

                            <i class="fa fa-plus"></i> Add Contact
                        </button>
                    </div>

                    <div class="box-body">

                        @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @include('errors.form_errors')

                        @include('includes.success_message')

                        <table class="table table-bordered table-hover" id="table-contacts">
                            <thead>
                            <tr>
                                <th>CourseMill User Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            @if ($contacts->count())
                                @foreach ($contacts as $contact)

                                    <tr>
                                        <td>
                                            @if ($contact->course_mill_user_id)
                                                {{ $contact->course_mill_user_id }}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td>{{ $contact->first_name }} {{ $contact->last_name }}</td>
                                        <td>{{ $contact->user ? $contact->user->email : 'No email' }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>
                                            <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                            <p class="block">{{ $contact->city }} {{ $contact->state }} {{ $contact->zip }}</p>
                                        </td>
                                        <td>
                                            @if ($contact->note)
                                                {{ $contact->note }}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.profile.edit', ['contact_id' => $contact->id]) }}"
                                               class="btn btn-primary btn-block btn-flat">
                                                Edit Profile
                                            </a>

                                            <a href="javascript:void(0)"
                                               data-id="{{ $contact->id }}"
                                               data-user_id="{{ $user->id }}"
                                               data-url="{{ route('contact.delete') }}"
                                               class="btn btn-danger btn-block btn-flat delete-contact">
                                                Remove Contact
                                            </a>
                                        </td>
                                    </tr>

                                @endforeach
                            @endif

                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

        <!-- Modal for adding / editing class -->

        <div class="modal fade" id="contactModal" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection