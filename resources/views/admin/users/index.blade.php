@extends('admin.layout')

@section('content')
<div>
    {{-- */
        $params = [
               'contact_id' => $userDetails->id,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1>
            {{ $userDetails->first_name }} {{ $userDetails->last_name }}
            <a href="{{ route('admin.profile.edit', ['contact_id' => $userDetails->id]) }}"><span class="fa fa-edit"></span></a>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', array_filter($params)) }}">User</a></li>

            @if ($user)
                {{-- */ $itemsCount = OrderUtility::hasItems($user->id, false); /* --}}
                <li>
                    <a href="{{ route('admin.user.shopping_cart', ['user_id' => $user->id]) }}">
                        <i class="fa fa-shopping-cart"> Cart ({{ $itemsCount }})</i>
                    </a>
                </li>
            @endif
        </ol>

        <div class="other-details">
            <div class="address pull-left">
                <p>{{ $userDetails->company }}</p>
                <p>{{ $userDetails->address1 }}</p>
                <p class="block">{{ $userDetails->city }}, {{ $userDetails->state }} {{ $userDetails->zip }}</p>
            </div>

            <div class="user-options-container pull-right">

                <div class="col-md-5">
                    <div class="checkbox form-group">
                        <label for="same_as_billing">
                            <input type="checkbox" class="user-options" name="address_same_as_billing"
                                   data-url="{{ route('admin.attribute.edit', ['contact_id' => $userDetails->id]) }}"
                                   @if ($userDetails->address_same_as_billing) checked @endif>

                            Same as billing
                        </label>
                    </div>

                    <div class="checkbox form-group">
                        <label for="same_as_shipping">
                            <input type="checkbox" class="user-options" name="address_same_as_shipping"
                                   data-url="{{ route('admin.attribute.edit', ['contact_id' => $userDetails->id]) }}"
                                   @if ($userDetails->address_same_as_shipping) checked @endif>
                            Same as shipping
                        </label>
                    </div>

                    @if ($user)
                        <div class="checkbox form-group">
                            <label for="military">
                                <input type="checkbox" class="user-options" name="has_military_discount"
                                       data-url="{{ route('admin.attribute.edit', [
                                        'user_id' => $user->id,
                                        'contact_id'=> $user->contact->id
                                        ]) }}"
                                       @if ($user->has_military_discount) checked @endif>
                                GSA Customer
                            </label>
                        </div>
                    @endif

                    @if (isset($hasSubscription))

                        <div class="checkbox form-group">
                            <label for="yearly_subscription">
                                <input type="checkbox" class="subscription-options" name="has_subscription"
                                       checked readonly disabled>
                                Yearly Subscription
                            </label>
                             <span>
                                 {{-- */ $subText = isset($expDate) ? ' - Expires on ' . $expDate . ')' : ')';  /* --}}
                                 ({{ $subscriptionType }}{{ $subText }}
                            </span>
                            @if (!$subscription->activated)
                            | <a href="javascript:void(0)" class="activate-subscription" data-url="{{ route('activation.get') }}"
                                 data-id="{{ $subscription->id }}"><span class="label label-danger">Activate</span></a>
                            @endif
                        </div>
                    @endif

                    @if ($user && $user->getCorporateSeatsCredit())
                        <div class="checkbox form-group">
                            <label for="military">
                                <input type="checkbox" class="subscription-options" name="has_subscription" checked readonly disabled>
                                {{ $user->getCorporateSeatsCredit() }} Corporate Seats
                            </label>
                        </div>
                    @endif
                </div>

                <div class="col-md-7">
                    <textarea id="general-note" class="hidden" cols="30" rows="10">
                        @if ($userDetails->generalNote)
                            {{ $userDetails->generalNote->content }}
                        @endif
                    </textarea>
                    <input type="hidden"
                           id="customer-id" value="{{ $userDetails->id }}">
                    <input type="hidden" id="save-note" value="{{ route('customer.note.save') }}">
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-users">
                    <div class="box-header">
                        <h4>Order History</h4>
                    </div>

                    <div class="box-body">

                        @include('includes.success_message')
                        @include('includes.error_message')

                            <table id="table-clients-admin" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Transaction Date</th>
                                    <th>Title</th>
                                    <th>Location</th>
                                    <th>Agent</th>
                                    <th>Status</th>
                                    <th width="135px">Details</th>
                                </tr>
                                </thead>

                            </table>

                    </div>
                </div>

                @if($user && $userHasNoCompany)
                <div class="alert alert-warning" role="alert">
                    <strong>Profile Update!</strong> Before you can purchase a corporate seat for this user, please update the profile and make sure to indicate the <strong>company</strong>.
                </div>
                @endif

                <div class="transaction-buttons">
                    @if ($user)
                        <a href="{{ route('admin.user.select_course', ['user_id' => $user->id]) }}" class="btn btn-primary btn-flat">Register Course</a>
                        @if($userHasNoCompany)
                        <a href="#" class="btn btn-primary btn-flat disabled">Corporate Seat</a>
                        @else
                        <a href="{{ route('admin.corporate_seat', ['user_id' => $user->id]) }}" class="btn btn-primary btn-flat">Corporate Seat</a>
                        @endif
                        <a href="{{ route('admin.books.buy', ['user_id' => $user->id]) }}" class="btn btn-primary btn-flat">Buy a Book</a>

                        @if (!$user->hasSubscription)
                            <a href="{{ route('admin.yearly_subscription', ['user_id' => $user->id]) }}" class="btn btn-primary btn-flat">Yearly Subscription</a>
                        @endif

                        <a href="{{ route('admin.onsite_training', ['user_id' => $user->id]) }}" class="btn btn-flat btn-primary">
                            Onsite Training
                        </a>

                        @if ($itemsCount)
                            <a href="{{ route('admin.user.shopping_cart', ['user_id' => $user->id]) }}" class="btn btn-primary btn-flat">View Cart</a>
                        @endif
                        <a href="{{ route('admin.address_book.edit', ['user_id' => $user->id]) }}" class="btn btn-flat btn-primary">
                            Address Book
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('includes.confirmation_modal')
</div>
@endsection

@section('javascripts')
    @parent

    <script>
        generalNoteHandler.init();

        (function($) {
            $(document).ready(function(){

                var userID = "{{ $user_Id }}",
                contactID = "{{ $contact_Id }}",
                baseURL = "/admin/user/user_orders/",
                url = userID ? baseURL + contactID + '/' + userID : baseURL + contactID;

                $('#table-clients-admin').dataTable({
                    "order": [[ 0, "desc" ]],
                    "stateSave": true,
                    "pageLength": 10,
                    "processing": true,
                    "serverSide": false,
                    "ajax": url,
                    "columns": [
                        { "data": "trans_date" },
                        { "data": "title" },
                        { "data": "location" },
                        { "data": "agent" },
                        { "data": "status" },
                        { "data": "actions" },
                    ],
                    "language": {
                            "emptyTable": "No Transactions."
                        }
                });

                $('#table-clients-admin').on('click', '.resend-email', function() {
                    var $that = $(this),
                        href = $that.data('href'),
                        order_id = $that.data('order-id');


                    Modal.showConfirmationModal(
                        'Are you sure you want to resend the confirmation email?',
                        'Resend Confirmation',
                        function() {
                            $('#notificationModal').modal('toggle');
                            window.location = href;
                            return false;
                        });
                    return false;

                });
 
            });
        })(jQuery);

    </script>
@endsection
