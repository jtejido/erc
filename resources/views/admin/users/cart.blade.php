@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];

       $itemsCount = OrderUtility::hasItems($user->id);
    /* --}}

    <section class="content-header">
        <h1 class="cart-header">
            <a href="{{ route('admin.user_details', $params) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Shopping Cart
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', $params) }}">User</a></li>
            <li><a href="">Cart @if ($itemsCount) ({{ $itemsCount }}) @endif</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row row-order-items">

            <input type="hidden" id="order" value="@if ($order) {{ $order->id }} @else 0 @endif">

            @if ($items)

                @include('admin.includes.success_message')
                @include('admin.includes.error_message')

                @if (Session::has('coupon_applied') && Session::get('coupon_applied'))
                    {{-- */ $label = 'success' /* --}}
                @else
                    {{-- */ $label = 'danger' /* --}}
                @endif

                @if (Session::has('coupon_message'))

                    <div class="box-container col-md-12">

                        <div class="alert alert-{{ $label }} alert-dismissible btn-flat" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ Session::get('coupon_message') }}
                        </div>

                    </div>

                @endif

                @foreach ($items as $item)
                    {!! OrderUtility::extractItems($item, $user) !!}
                @endforeach

                @if ($order && $corpSeatCredits)
                    {!! OrderUtility::extractCredits($order, $user, $corpSeatCredits) !!}
                @endif

                @if ($order && (($combinationDiscounts && $combinationDiscounts->count()) ||
                            ($halfPriceDiscounts && $halfPriceDiscounts->count()) ||
                            ($couponDiscounts && $couponDiscounts->count())) ||
                            ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count()) ||
                            ($militaryDiscounts && $militaryDiscounts->count()) ||
                            ($subscriptionDiscounts && $subscriptionDiscounts->count()) ||
                            ($creditDiscounts && $creditDiscounts->count()))

                    <div class="col-md-12 box-container">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Discounts Summary</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">

                                @if ($subscriptionDiscounts && $subscriptionDiscounts->count())
                                    <div class="attendees">
                                        <h5>Yearly Subscription Discounts</h5>

                                        <ul class="list">
                                            @foreach ($subscriptionDiscounts as $subscriptionDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractSubscriptionDiscount($subscriptionDiscount) }}
                                                        </span>
                                                        <span class="price col-xs-2">${{ $subscriptionDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($subscriptionDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif


                                @if ($militaryDiscounts && $militaryDiscounts->count())
                                    <div class="attendees">
                                        <h5>GSA Pricing Discounts</h5>

                                        <ul class="list">
                                            @foreach ($militaryDiscounts as $militaryDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractMilitaryDiscount($militaryDiscount) }}
                                                        </span>
                                                        <span class="price col-xs-2">${{ $militaryDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($militaryDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($halfPriceDiscounts && $halfPriceDiscounts->count())
                                    <div class="attendees">
                                        <h5>Group Discounts</h5>

                                        <ul class="list">
                                            @foreach ($halfPriceDiscounts as $halfPriceDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractHalfPriceInfo($halfPriceDiscount) }}
                                                        </span>
                                                        <span class="price col-xs-2">${{ $halfPriceDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($halfPriceDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($combinationDiscounts && $combinationDiscounts->count())
                                    <div class="attendees">
                                        <h5>HWM-DOT Discounts</h5>

                                        <ul class="list">
                                            @foreach ($combinationDiscounts as $combinationDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractClassCombinationInfo($combinationDiscount) }}
                                                        </span>
                                                        <span class="price col-xs-2">${{ $combinationDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($combinationDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($couponDiscounts && $couponDiscounts->count())
                                    <div class="attendees">
                                        <h5>Coupon Discounts</h5>

                                        <ul class="list">
                                            @foreach ($couponDiscounts as $couponDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractCouponDiscountInfo($couponDiscount) }}
                                                        </span>
                                                        <span class="price col-xs-2">${{ $couponDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($couponDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($orderAdjustmentDiscounts && $orderAdjustmentDiscounts->count())
                                    <div class="attendees">
                                        <h5>Order Adjustment Discounts</h5>

                                        <ul class="list">
                                            @foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-xs-10">
                                                            {{ DiscountUtility::extractOrderAdjustment($orderAdjustmentDiscount) }}

                                                            @if ($orderAdjustmentDiscount->adjustment_notes)
                                                                <i class="fa fa-info-circle" data-container="body"
                                                                   data-toggle="popover"
                                                                   data-placement="top" data-content="{{ $orderAdjustmentDiscount->adjustment_notes }}">
                                                                </i>
                                                            @endif
                                                        </span>
                                                        <span class="price pull-right col-md-2">${{ $orderAdjustmentDiscount->adjusted_deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($orderAdjustmentDiscounts->sum('computed_deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($creditDiscounts && $creditDiscounts->count())
                                    <div class="attendees">
                                        <h5>Corporate Seat Discounts</h5>

                                        <ul class="list">
                                            @foreach ($creditDiscounts as $creditDiscount)
                                                <li>
                                                    <p>
                                                    <span class="discount-title col-xs-10">
                                                        {{ DiscountUtility::extractCreditDiscount($creditDiscount) }}
                                                    </span>
                                                        <span class="price pull-right col-md-2">${{ $creditDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($creditDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                @endif

                @if (!is_null($total))
                    <div class="col-md-12 total-container">
                        <div class="info-box bg-light-blue">
                            <div class="info-box-content">
                                <span class="info-box-text">TOTAL COST:</span>
                                <span class="info-box-number pull-right">${{ number_format($total, 2) }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 pull-right total-container coupon-navigation">
                        <div class="col-md-6 col-md-offset-3 coupon-form">
                            <input type="text" id="coupon-code" class="coupon-text" value="{{ Session::get('coupon_code') }}">
                            <input type="hidden" id="order" value="@if ($order){{ $order->id }}@endif">
                            <button class="btn btn-flat btn-info btn-sm" id="apply-coupon"
                                    data-href="{{ route('coupon.apply') }}">
                                Apply Coupon
                            </button>
                        </div>
                    </div>

                    <div class="actions col-md-12">
                        <a href="{{ route('admin.user.select_course', ['user_id' => $user->id]) }}" class="btn btn-flat btn-info col-md-3">Browse For Courses</a>
                        <a href="{{ route('admin.books.buy', ['user_id' => $user->id]) }}" class="btn btn-flat btn-info col-md-4 col-md-offset-1">Browse For Books</a>

                        @if ($total > 0)
                            <a href="{{ route('admin.checkout.get', ['user_id' => $user->id]) }}" class="btn btn-flat btn-info pull-right col-md-3">Proceed To Checkout</a>
                        @else
                            <button data-user_id="{{ $user->id }}" data-url="{{ route('order.complete') }}" class="btn btn-flat btn-info pull-right col-md-3 cart-complete-order">Complete Order</button>
                        @endif
                    </div>
                @else
                    <i>No Items.</i>
                @endif
            @else
                <i>No Items.</i>
            @endif

        </div>

        @include('includes.notification_modal')
    </section>

@endsection

@section('javascripts')
    @parent

    <script>
        (function() {
            confirmationHandler.init();
            $('.price').AllowDecimal();
            $('#notificationModal').on('hidden.bs.modal', function () {
                $('#doNotEmailContainer').hide();
            })
        })(jQuery);
    </script>
@endsection