@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1>
            <a href="{{ route('admin.user_details', $params) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Yearly Subscription
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">Attendees</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="subs-options-container pull-left">
                            @foreach($types as $type)
                                <div class="checkbox form-group">
                                    <label for="subscription-type">
                                        <input type="radio" class="subscription-options" name="subscription-type"
                                               value="{{ $type->id }}"
                                               id="sub-{{ $type->id }}"
                                               @if (isset($stype) && $stype == $type->id) checked @endif>
                                        {{ $type->name }}
                                    </label>

                                    <div class="price pull-right">
                                        $<input type="text" name="type" class="type" value="{{ $type->price }}" style="width: 100px">
                                        <button class="btn btn-flat btn-xs btn-success btn-save-subs" data-id="{{ $type->id }}">Save</button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <button class="btn btn-flat btn-success pull-right" id="add-contact"
                                data-user_id="{{ $user->id }}"
                                data-url="{{ route('contact.form.get') }}">

                            <i class="fa fa-plus"></i> Create New Contact
                        </button>
                    </div>

                    <div class="box-body">
                        @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classMessage') }}">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @include('errors.form_errors')

                        <table class="table table-bordered table-hover" id="table-contacts">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="check-all"></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tr>
                                <td width="1%">
                                    {!!
                                    Form::checkbox( 'contact[]', $user->contact->id, (in_array($user->id, $attendees) ? true : false),
                                    array('class'=>'contact check-single'))
                                    !!}
                                </td>
                                <td>{{ $user->contact->first_name }} {{ $user->contact->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->contact->phone }}</td>
                                <td>{{ $user->contact->address1 }} {{ $user->contact->address2 }}, {{ $user->contact->city }}, {{ $user->contact->state }}</td>
                                <td>--</td>
                                <td>
                                    <a href="{{ route('admin.profile.edit', ['contact_id' => $user->contact->id]) }}"
                                       class="edit-contact btn btn-primary btn-block btn-flat">
                                        Edit Profile
                                    </a>
                                </td>
                            </tr>

                            @if ($contacts->count())
                                @foreach ($contacts as $contact)

                                    @if ($contact->user)
                                        <tr>
                                            <td width="1%">
                                                {!!
                                                Form::checkbox( 'contact[]', $contact->id, (in_array($contact->id, $attendees) ? true : false),
                                                array('id'=>'contact_'.$contact->id,
                                                'class'=>'contact check-single',
                                                'data-contact_id'=> $contact->id))
                                                !!}
                                            </td>
                                            <td>{{ $contact->first_name }} {{ $contact->last_name }}</td>
                                            <td>@if ($contact->user){{ $contact->user->email }}@endif</td>
                                            <td>{{ $contact->phone }}</td>
                                            <td>{{ $contact->address1 }} {{ $contact->address2 }}, {{ $contact->city }}, {{ $contact->state }}</td>
                                            <td>
                                                @if ($contact->note)
                                                    {{ $contact->note }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.profile.edit', ['contact_id' => $contact->id]) }}"
                                                   class="edit-contact btn btn-primary btn-block btn-flat">
                                                    Edit Profile
                                                </a>
                                            </td>
                                        </tr>
                                    @endif

                                @endforeach
                            @endif

                        </table>

                        <button class="btn btn-flat btn-primary" id="add-contact-to-subscription"
                                data-agent="{{ $user->id }}">
                            Add To Subscription
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

        <!-- Modal for adding / editing class -->

        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        (function($){
            $('#add-contact').on('click', function() {
                var $this    = $(this),
                    userId   = $this.data('user_id'),
                    modal    = $('#contactModal');

                $.ajax({
                    type : 'GET',
                    url  : "{{ route('contact.form.get') }}",
                    data : {
                        user_id   : userId,
                        email : true
                    },
                    success : function(response) {
                        modal.find('.modal-content').html(response);
                        $('#phone, #zip').NumericOnly();

                        searchEmail.init();
                    }
                }).done(function() {
                    $("[data-mask]").inputmask();

                    $('.state').select2({
                        placeholder: "Select State"
                    });
                    $( "input#company" ).autocomplete({
                        source: "/api/companies",
                        minLength: 2,
                    });
                    $( "input#company" ).autocomplete("option", "appendTo", "#contactModal");
                });;

                modal.modal('show');
            });

            $('.btn-save-subs').on('click', function() {
                var $this = $(this),
                    typeId = $this.data('id'),
                    value = $this.siblings('.type').val();

                if ($.trim(value).length && value != 0) {

                    $.ajax({
                        type : 'POST',
                        url : "{{ route('admin.save_yearly_subscription') }}",
                        data : {
                            type_id : typeId,
                            value : value
                        },
                        beforeSend : function(data) {
                            $this.text('Saving...');
                        },
                        success : function(response) {
                            if (response) {
                                setTimeout(function(){
                                    $this.text('Save');
                                }, 1500);
                            }
                        }
                    })
                }
            });

            $('#add-contact-to-subscription').on('click', function() {
                var $this = $(this),
                    contacts = [],
                    $checked = $('input[name="subscription-type"]:checked'),
                    id = $checked.val(),
                    userId = $this.data('agent');

                $('.contact').each(function() {
                    if ($(this).prop('checked')) {
                        contacts.push($(this).val());
                    }
                });

                if (typeof id != 'undefined' && contacts.length) {
                    
                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('contact.add_to_subscription') }}",
                        data : {
                            is_admin: true,
                            user_id : userId,
                            contacts : contacts,
                            subscription_id : id
                        },
                        beforeSend : function (data) {
                            $this.attr('disabled', true);
                        },
                        success : function (response) {
                            if (response) {
                                window.location.href = response;
                            }
                        }
                    });
                }


            });

            @if (isset($stype))
                var $parent = $('#sub-' + {{ $stype }}).parent();
                $parent.addClass('checked');
                $parent.attr('aria-checked', 'true');
            @endif

            $('.type').NumericOnly()

            setTimeout(function() {
                $('.alert').fadeOut('slow');
            }, 3000);
        })(jQuery);
    </script>
@endsection