@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];

    /* --}}

    <section class="content-header">
        <h1 class="cart-header">
            <a href="{{ route('admin.user_details', $params) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Payment History
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', $params) }}">User</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row payment-details">

            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default order-items">
                    <div class="panel-heading display-table" >
                        <div class="row display-tr" >
                            <h3 class="panel-title display-td" >Payment Summary</h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row summary-info">
                            <div class="col-xs-12 attendees-info">
                                @foreach ($order->paymentTransactions as $paymentTransaction)

                                    @include('includes.payment_summary.payment_summary')

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a class="btn btn-warning"
                       href="{{ route('admin.receipt', ['user_id' => $user->id, 'order_id' => $order->id]) }}">
                        <i class="fa fa-print" aria-hidden="true"></i> Download Receipt</a>
                </div>
            </div>
        </div>

        @include('includes.notification_modal')

    </section>

@endsection
