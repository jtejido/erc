@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1 class="cart-header">
            <a href="{{ route('admin.user_details', $params) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Order Details
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', $params) }}">User</a></li>
        </ol>
    </section>

    <section class="content">

        <div class="row row-order-items">

            <input type="hidden" id="order" value="@if ($order){{ $order->id }}@else 0 @endif">
            <input type="hidden" id="agent-id" value="@if ($order){{ $order->user->id }}@else 0 @endif">
            <input type="hidden" id="status" value="@if ($order){{ $order->status }}@else 0 @endif">

            @if ($items)

                @include('admin.includes.success_message')
                @include('admin.includes.error_message')

                @if (Session::has('coupon_applied') && Session::get('coupon_applied'))
                    {{-- */ $label = 'success' /* --}}
                @else
                    {{-- */ $label = 'danger' /* --}}
                @endif

                @if (Session::has('coupon_message'))

                    <div class="box-container col-md-12">
                        <div class="alert alert-{{ $label }} alert-dismissible btn-flat" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{ Session::get('coupon_message') }}
                        </div>
                    </div>

                @endif

                @foreach ($items as $item)
                    {!! OrderUtility::extractItems($item, $user) !!}
                @endforeach

                @if ($order && $corpSeatCredits)
                    {!! OrderUtility::extractCredits($order, $user, $corpSeatCredits) !!}
                @endif

                @if ($order and (($combinationDiscounts and $combinationDiscounts->count()) or
                            ($halfPriceDiscounts and $halfPriceDiscounts->count()) or
                            ($couponDiscounts and $couponDiscounts->count())) or
                            ($orderAdjustmentDiscounts and $orderAdjustmentDiscounts->count()) or
                            ($militaryDiscounts and $militaryDiscounts->count()) or
                            ($subscriptionDiscounts and $subscriptionDiscounts->count()) or
                            ($creditDiscounts and $creditDiscounts->count()))

                    <div class="col-md-12 box-container">
                        <div class="box box-primary box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Discounts Summary</h3>
                                <div class="box-tools pull-right">
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body">

                                @if ($subscriptionDiscounts and $subscriptionDiscounts->count())
                                    <div class="attendees">
                                        <h5>Yearly Subscription Discounts</h5>

                                        <ul class="list">
                                            @foreach ($subscriptionDiscounts as $subscriptionDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractSubscriptionDiscount($subscriptionDiscount) }}

                                                            @if ($subscriptionDiscount->classRegistration->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif
                                                        </span>
                                                        <span class="price col-md-2">${{ $subscriptionDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($subscriptionDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif


                                @if ($militaryDiscounts and $militaryDiscounts->count())
                                    <div class="attendees">
                                        <h5>GSA Pricing Discounts</h5>

                                        <ul class="list">
                                            @foreach ($militaryDiscounts as $militaryDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractMilitaryDiscount($militaryDiscount) }}

                                                            @if ($militaryDiscount->classRegistration->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif
                                                        </span>
                                                        <span class="price col-md-2">${{ $militaryDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($militaryDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($halfPriceDiscounts and $halfPriceDiscounts->count())
                                    <div class="attendees">
                                        <h5>Group Discounts</h5>

                                        <ul class="list">
                                            @foreach ($halfPriceDiscounts as $halfPriceDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractHalfPriceInfo($halfPriceDiscount) }}

                                                            @if ($halfPriceDiscount->classRegistration->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif
                                                        </span>
                                                        <span class="price col-md-2">${{ $halfPriceDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($halfPriceDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($combinationDiscounts and $combinationDiscounts->count())
                                    <div class="attendees">
                                        <h5>HWM-DOT Discounts</h5>

                                        <ul class="list">
                                            @foreach ($combinationDiscounts as $combinationDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractClassCombinationInfo($combinationDiscount) }}

                                                            @if ($combinationDiscount->classOne->is_new or $combinationDiscount->classTwo->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif
                                                        </span>
                                                        <span class="price col-md-2">${{ $combinationDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($combinationDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($couponDiscounts and $couponDiscounts->count())
                                    <div class="attendees">
                                        <h5>Coupon Discounts</h5>

                                        <ul class="list">
                                            @foreach ($couponDiscounts as $couponDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractCouponDiscountInfo($couponDiscount) }}

                                                            @if ($couponDiscount->classRegistration->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif
                                                        </span>
                                                        <span class="price col-md-2">${{ $couponDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($couponDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($orderAdjustmentDiscounts and $orderAdjustmentDiscounts->count())
                                    <div class="attendees">
                                        <h5>Order Adjustment Discounts</h5>

                                        <ul class="list">
                                            @foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount)
                                                <li>
                                                    <p>
                                                        <span class="discount-title col-md-10">
                                                            {{ DiscountUtility::extractOrderAdjustment($orderAdjustmentDiscount) }}

                                                            @if ($orderAdjustmentDiscount->adjustable_type == \App\Utilities\Constant::CLASS_REGISTRATION and
                                                                $orderAdjustmentDiscount->adjustable->is_new)
                                                                <span class="label label-warning">New</span>
                                                            @endif

                                                            @if ($orderAdjustmentDiscount->adjustment_notes)
                                                                <i class="fa fa-info-circle" data-container="body"
                                                                   data-toggle="popover"
                                                                   data-placement="top"
                                                                   data-content="{!! $orderAdjustmentDiscount->adjustment_notes . ' Created: ' . $orderAdjustmentDiscount->created_at->format('F d, Y') !!}">
                                                                </i>
                                                            @endif
                                                        </span>
                                                        <span class="price pull-right col-md-2">
                                                            ${{ $orderAdjustmentDiscount->adjusted_deduction }}

                                                        </span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($orderAdjustmentDiscounts->sum('computed_deduction'), 2)}}</span>
                                    </div>
                                @endif

                                @if ($creditDiscounts and $creditDiscounts->count())
                                    <div class="attendees">
                                        <h5>Corporate Seat Discounts</h5>

                                        <ul class="list">
                                            @foreach ($creditDiscounts as $creditDiscount)
                                                <li>
                                                    <p>
                                                    <span class="discount-title col-md-10">
                                                        {{ DiscountUtility::extractCreditDiscount($creditDiscount) }}
                                                    </span>
                                                        <span class="price pull-right col-md-2">${{ $creditDiscount->deduction }}</span>
                                                    </p>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="total">
                                        <hr>
                                        <b>Discount Sub-total : </b>
                                        <span class="total-cost pull-right">${{ number_format($creditDiscounts->sum('deduction'), 2)}}</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                @endif

                @if (!is_null($total))
                    @if(!is_null($shipping_rates) AND $shipping_rates >0)
                    <div class="col-md-12 total-container">
                        <div class="info-box bg-light-blue">
                            <div class="info-box-content">
                                <span class="info-box-text">SHIPPING TOTAL:</span>
                                <span class="info-box-number pull-right">${{ (!is_null($shipping_rates))? number_format($shipping_rates, 2):'0.00' }}</span>
                            </div>
                        </div>
                    </div>
                    @endif
                        @if(!is_null($order_tax) AND $order_tax >0)
                            <div class="col-md-12 total-container">
                                <div class="info-box bg-light-blue">
                                    <div class="info-box-content">
                                        <span class="info-box-text">SALES TAX:</span>
                                        <span class="info-box-number pull-right">${{ (!is_null($order_tax))? number_format($order_tax, 2):'0.00' }}</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    <div class="col-md-12 total-container">
                        <div class="info-box bg-light-blue">
                            <div class="info-box-content">
                                <span class="info-box-text">CURRENT TOTAL:</span>
                                <span class="info-box-number pull-right">${{ number_format($total, 2) }}</span>
                            </div>
                        </div>
                    </div>

                    @if ($order and $orderAdjustments->count())
                        {!! OrderModificationUtility::extractOrderAdjustment($orderAdjustments, $order->paid_total) !!}
                    @endif

                    <div class="col-md-12 pull-right total-container coupon-navigation margin-bottom">
                        <div class="col-md-6 col-md-offset-3 coupon-form">
                            <input type="text" id="coupon-code" class="coupon-text" value="{{ Session::get('coupon_code') }}">
                            <input type="hidden" id="order" value="@if ($order){{ $order->id }}@endif">
                            <button class="btn btn-flat btn-info btn-sm" id="apply-coupon"
                                    data-href="{{ route('coupon.apply') }}">
                                Apply Coupon
                            </button>
                        </div>
                    </div>

                    @if ($order and $orderAdjustmentHistories->count())
                        {!! OrderModificationUtility::extractOrderAdjustmentHistory($orderAdjustmentHistories) !!}
                    @endif

                    <div class="row">

                        <div class="alert alert-danger col-md-6 col-md-offset-3">
                            <h4 class="alert-heading alert-danger"><i class="fa fa-check-square-o"></i> Warning!</h4>
                            <p>Deleting this order will not undo the payment transaction and email notifications that has been sent already.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="text-center">
                            {!! Form::open([
                                'url'   => route('admin.order.delete', $order->id),
                                'id'    => 'delete-order-form'
                            ]) !!}
                            {!! method_field('delete') !!}
                            <button class="btn btn-danger" id="delete-entire-order">
                                <i class="fa fa-trash" aria-hidden="true"></i> Delete Order</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                @else
                    <i>No Items.</i>
                @endif
            @else
                <i>No Items.</i>
            @endif

        </div>

        @include('includes.notification_modal')
        @include('includes.cancellation_modal')
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notes</h3>
                    </div>
                    <div class="panel-body">
                        <textarea name="note" class="notes"></textarea>
                    </div>
                    <div class="panel-footer text-right">
                        <button type="reset" class="btn btn-default" id="resetNote">Reset</button>
                        <button type="button" class="btn btn-primary" id="saveNote">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="container">
                <div class="col-md-9">

                    <ul class="media-list" id="note-list">
                     @if(count($notes)>0)
                        @foreach($notes as $note)
                        <li class="media panel">
                            <div class="media-body panel-body">
                                <h4 class="media-heading">{{ $note->user->email }} <br><small>{{ $note->created_at_string }}</small></h4>
                                {!! $note->notes !!}
                            </div>
                        </li>
                        @endforeach
                    @endif
                    </ul>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        var InvoiceInfo = {
        init : function() {
            this.writer();
            this.storeNote();
            this.resetNote();
        },
        resetNote: function(){
            $('#resetNote').click(function(e){
                tinyMCE.activeEditor.setContent('');
            })
        },
        storeNote: function(){
            $('#saveNote').click(function(e){
                var note = tinyMCE.activeEditor.getContent();
                var order_id = $('#order').val();
                var data = {
                        'note': note,
                        'order_id': order_id
                };

                $(this).attr('disabled','disabled');
                if( note.length > 0 ){
                     $.ajax({
                        url: '{{route('admin.order.notes.add')}}',
                        type: 'post',
                        data : data,
                        success: function (res) {
                            var tem = InvoiceInfo.noteTemplate(res);
                            $('#note-list').prepend($(tem));
                            tinyMCE.activeEditor.setContent('');
                            $('#saveNote').removeAttr('disabled');
                        },
                        error: function (res) {

                        }
                    });
                }

                e.preventDefault();
                e.stopImmediatePropagation();
            });
        },
        noteTemplate: function(data){
            return `<li class="media panel">
                        <div class="media-body panel-body">
                            <h4 class="media-heading">${data.email}<br><small>${data.created_at_string}</small></h4>
                            ${data.notes}
                        </div>
                    </li>
            `;
        },
        writer: function() {
            tinymce.init({
                selector: '.notes',
                height: 150,
                menubar: false,
                toolbar: [
                    'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link'
                ],
                theme_advanced_buttons1: "bold,italic,underline"
            });
        }
    };
        $(function(){
            InvoiceInfo.init();
            $('.price').AllowDecimal();
            cancelRegistration.init();
            cancellationAmountHandler.init();
            orderModificationHandler.init();
            confirmationHandler.init();
            if($('input[name="book_id"]').length > 0){
                $('input[name="book_id"]').parent().find('button[type="submit"]').hide();
                $('input[name="book_id"]').parent().find('input[name="quantity"]').attr('disabled','disabled');
            }
            $('#delete-entire-order').click(function() {

                Modal.showConfirmationModal(
                    'Are you sure you want to delete this order?',
                    'Deletion',
                    function() {
                        var $okBtn = $('#notification-ok');

                        $('#delete-order-form').submit();
                        return false;
                    }
                );
                return false;
            });

        });
    </script>
@endsection