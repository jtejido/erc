@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1 class="cart-header">
            Edit Account
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', ['user_id' => $user->id]) }}">User</a></li>
            <li><a href="">Transaction</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row payment-details">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Email And Role
                            </h3>
                        </div>
                    </div>

                    {!! Form::open([
                        'method' => 'POST',
                        'url' => route('admin_user.edit_email'),
                        'class' => 'edit-profile-form'
                        ]) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_email'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif

                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                        {!! Form::rLabel('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('email', 'email', isset($user) ? $user->email : '', [
                            'class' => 'form-control',
                            'id'    => 'email',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('email'))
                                {!! $errors->first('email', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('role', 'Role: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::select('role', $roles, isset($user) ? $user->role : '', [
                                'class' => 'form-control',
                                'style' => 'display: inline;',
                                'required' => 'required'
                            ]) !!}
                        </div>

                        {!! Form::rLabel('password', 'Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'password', '', [
                            'class' => 'form-control',
                            'id'    => 'password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}
                            <em>Confirm password to change email/role.</em><br/>

                            @if ($errors->has('password'))
                                {!! $errors->first('password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Password
                            </h3>
                        </div>
                    </div>

                    {!! Form::open([
                        'method' => 'POST',
                        'url' => route('admin_user.edit_password'),
                        'class' => 'edit-profile-form',
                        ]) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_password'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success" role="alert">
                                    {{ Session::get('message') }}
                                </div>
                            </div>
                        @endif

                        {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                        {!! Form::rLabel('old_password', 'Old Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'old_password', '', [
                            'class' => 'form-control',
                            'id'    => 'old_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('old_password'))
                                {!! $errors->first('old_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('new_password', 'New Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 'new_password', '', [
                            'class' => 'form-control',
                            'id'    => 'new_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('new_password'))
                                {!! $errors->first('new_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>

                        {!! Form::rLabel('re_new_password', 'Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('password', 're_new_password', '', [
                            'class' => 'form-control',
                            'id'    => 're_new_password',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('re_new_password'))
                                {!! $errors->first('re_new_password', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary">
                                Save
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>

@endsection
