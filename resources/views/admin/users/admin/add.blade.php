@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Add New User</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.users') }}">Users</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">

                @include('admin.includes.form_errors')

                @if (Session::has('message'))
                    <div class="box-container col-md-12">
                        <div class="alert alert-success alert-dismissible box box-info box-solid" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ Session::get('message') }}
                        </div>
                    </div>
                @endif

                {!! Form::open(['method' => 'POST']) !!}

                    @if (isset($user))
                        {!! Form::input('hidden', 'id', $user->id) !!}
                    @endif

                    {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="form-group col-md-9">
                        {!! Form::input('text', 'first_name', isset($user) ? $user->contact->first_name : '', [
                            'class' => 'form-control col-md-9',
                            'style' => 'width: 300px; display: inline;',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="form-group col-md-9">
                        {!! Form::input('text', 'last_name', isset($user) ? $user->contact->last_name : '', [
                            'class' => 'form-control col-md-9',
                            'style' => 'width: 300px; display: inline;',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('email', 'Email Address: ', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="form-group col-md-9">
                        {!! Form::input('text', 'email', isset($user) ? $user->email : '', [
                            'class' => 'form-control col-md-9',
                            'style' => 'width: 300px; display: inline;',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('password', 'Password: ', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="form-group col-md-9">
                        {!! Form::password('password', [
                            'class' => 'form-control col-md-9',
                            'style' => 'width: 300px; display: inline;',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('role', 'Role: ', ['class' => 'col-sm-2 control-label']) !!}
                    <div class="form-group col-md-9">
                        {!! Form::select('role', $roles, isset($user) ? $user->role : '', [
                            'class' => 'form-control col-md-9 user-role',
                            'style' => 'width: 300px; display: inline;',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    <div class="form-group col-md-9">
                        {!! Form::input('submit', 'Save', 'Save', ['class' => 'btn btn-flat btn-success btn-submit']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        (function($) {
            $('form').on('submit', function() {
                var $btn = $('.btn-submit');

                $btn.attr('disabled', true)
                    .val('Saving..')
            });
        })(jQuery);
    </script>
@endsection
