@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Users</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header box-header-users"></div>

            <div class="box-body">

                @include('admin.includes.success_message')

                @if ($users->count())

                    <table id="table-clients" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Name</th>
                                <th>Registered Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        @foreach ($users as $user)
                            <tr>
                                <td><input type="checkbox" name="user[]" data-id="{{ $user->id }}"></td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->roleName }}</td>
                                <td>{{ $user->contact->name }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                                    <a href="{{ route('admin_user.edit', ['user_id' => $user->id]) }}" class="btn btn-primary btn-block btn-flat">Edit</a>
                                    <a href="javascript:void(0)"
                                       class="btn btn-danger btn-block btn-flat btn-delete"
                                       data-id="{{ $user->id }}">Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @else
                    <i>No Users.</i>
                @endif
            </div>
        </div>

        @include('includes.confirmation_modal')

    </section>
@endsection

@section('javascripts')
    @parent

    <script>
        (function($) {
            $('.btn-delete').on('click', function() {
                var $this = $(this),
                    userId = $this.data('id');

                Modal.showConfirmationModal(
                    'Are you sure you want to delete this user?',
                    'Delete',
                    function() {
                        $.ajax({
                            type: 'POST',
                            url: "{{ route('admin_user.delete') }}",
                            data: { user_id : userId },
                            success: function(response) {
                                if (response) {
                                    window.location.reload();
                                }
                            }
                        })
                    }
                );
            });
        })(jQuery);
    </script>
@endsection
