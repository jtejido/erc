@extends('admin.layout')

@section('content')

    {{-- */

        $params = [
               'contact_id' => $userDetails->id,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1 class="cart-header">
            Edit
            <a href="{{ route('admin.user_details', array_filter($params)) }}">
                {{ $userDetails->first_name }} {{ $userDetails->last_name }}'s
            </a>
            Account Profile
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', array_filter($params)) }}">User</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row payment-details">
            <div class="col-md-6">

                @if ($user)
                    <div class="panel panel-default">
                        <div class="panel-heading display-table">
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">
                                    User Email Account
                                </h3>
                            </div>
                        </div>

                        {!! Form::open([
                            'method' => 'POST',
                            'url' => route('email.edit_profile'),
                            'class' => 'edit-profile-form',
                            'id'    => 'edit-email-form'
                            ]) !!}

                        <div class="panel-body">

                            @if (Session::has('edit_email'))
                                <div class="form-group col-md-12">
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                </div>
                            @endif

                            {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                            {!! Form::rLabel('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('email', 'email', isset($user) ? $user->email : '', [
                                'class' => 'form-control',
                                'id'    => 'email',
                                'style' => 'display: inline;',
                                ]) !!}

                                @if ($errors->has('email'))
                                    {!! $errors->first('email', '<div class="alert alert-danger">:message</div>') !!}
                                @endif

                            </div>

                            {!! Form::rLabel('password', 'Confirm Password: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('password', 'password',  '', [
                                'class' => 'form-control',
                                'id'    => 'password',
                                'style' => 'display: inline;',
                                ]) !!}

                                @if ($errors->has('password'))
                                    {!! $errors->first('password', '<div class="alert alert-danger">:message</div>') !!}
                                @endif

                                @if ($userDetails->course_mill_user_id)
                                    <p class="required course-mill-alert">
                                        Updating this email will also update the email in CourseMill.
                                    </p>
                                @endif

                                <button type="submit" class="btn btn-flat btn-primary btn-save">
                                    Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading display-table">
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">
                                    Reset Password
                                </h3>
                            </div>
                        </div>

                        {!! Form::open([
                            'method' => 'POST',
                            'url' => route('password.post'),
                            'class' => 'edit-profile-form',
                            'id'    => 'reset-password-form'
                            ]) !!}

                        <div class="panel-body">

                            <div class="form-group col-md-12">
                                <div class="alert alert-success hidden"></div>
                            </div>

                            {!! Form::label('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('email', 'email', isset($user) ? $user->email : '', [
                                'class' => 'form-control',
                                'id'    => 'email',
                                'style' => 'display: inline;',
                                'readonly'
                                ]) !!}


                                <button type="submit" class="btn btn-flat btn-primary">
                                    Send Reset Password Link
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading display-table">
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">
                                    Activate / Deactivate Account
                                </h3>
                            </div>
                        </div>

                        {!! Form::open([
                            'id'    => 'toggleUserStatus',
                            'method' => 'POST',
                            'url' => route('user.toggleUserStatus'),
                            ]) !!}

                        <div class="panel-body">

                            @if (Session::has('toggle_account'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                            @endif

                            <label class="col-md-3 control-label">Status:</label>
                            <div class="form-group col-md-9">
                                Account is currently
                                @if ($user->isActive())
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Deactivated</span>
                                @endif
                            </div>
                                @inject('userService', 'App\Services\UserService')


                                <div>
                                {!! Form::input('hidden', 'user_id', isset($user) ? $user->id : '' ) !!}

                                @if ($userDetails->course_mill_user_id)
                                    <p class="required course-mill-alert text-center">
                                        Updating the status will also update the status in CourseMill.
                                    </p>
                                @endif
                                <p class="text-center">
                                @if ($user->isActive())
                                    <button type="button" data-text="deactivate" class="btn btn-flat btn-danger submitButton">
                                        Deactivate Account
                                    </button>
                                @else
                                    <button t   ype="button" data-text="activate" class="btn btn-flat btn-success submitButton">
                                        Activate Account
                                    </button>
                                @endif
                                </p>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                @else

                    <div class="panel panel-default">
                        <div class="panel-heading display-table">
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">
                                    Add Email
                                </h3>
                            </div>
                        </div>

                        {!! Form::open([
                            'method' => 'POST',
                            'url' => route('email.add'),
                            'class' => 'edit-profile-form'
                            ]) !!}

                        <div class="panel-body">

                            <div class="form-group col-md-12">
                                <div class="alert alert-success hidden"></div>
                            </div>

                            {!! Form::label('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('email', 'email', '', [
                                'class' => 'form-control',
                                'id'    => 'email',
                                'style' => 'display: inline;',
                                ]) !!}

                                <input type="hidden" name="contact_id" value="{{ $userDetails->id }}">

                                @if ($userDetails->course_mill_user_id)
                                    <p class="required course-mill-alert">
                                        Updating this email will also update the email in CourseMill.
                                    </p>
                                @endif


                                <button type="submit" class="btn btn-flat btn-primary btn-save">
                                    Save
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>

                @endif

                @if (Auth::user()->hasRole(\App\Utilities\Constant::ROLE_ADMIN))
                    @if ((!$userDetails->user and $userDetails->classes->count() == 0) or
                        ($userDetails->classes->count() == 0 and $userDetails->user and $userDetails->user->corporateSeatCredits->count() == 0 and
                         $userDetails->user->clientYearlySubscriptions->count() == 0 and $userDetails->user->orders->count() == 0 and
                         $userDetails->user->registrationOrders->count() == 0 and $userDetails->user->yearlySubscriptions->count() == 0))

                        <div class="panel panel-default">
                            <div class="panel-heading display-table">
                                <div class="row display-tr">
                                    <h3 class="panel-title display-td">
                                        Delete User
                                    </h3>
                                </div>
                            </div>


                            <div class="panel-body">
                                <a href="" data-id="{{ $userDetails->id }}" class="btn btn-danger btn-flat delete-customer col-md-6 col-md-offset-3">Delete</a>
                            </div>
                        </div>
                    @endif
                @endif
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">
                                Personal Information
                            </h3>
                        </div>
                    </div>

                    {!! Form::open(['method' => 'POST', 'url' => route('personal_info.edit_profile'),  'class' => 'edit-profile-form']) !!}

                    <div class="panel-body">

                        @if (Session::has('edit_personal_info'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::label('course_mill_user_id', 'CourseMill User Id: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'course_mill_user_id', isset($userDetails) ? $userDetails->course_mill_user_id : '', [
                            'class' => 'form-control',
                            'id'    => 'course_mill_user_id',
                            'style' => 'display: inline;'
                            ]) !!}

                            @if ($errors->has('first_name'))
                                {!! $errors->first('first_name', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>


                        {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                        {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'first_name', isset($userDetails) ? $userDetails->first_name : '', [
                            'class' => 'form-control',
                            'id'    => 'first_name',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('first_name'))
                                {!! $errors->first('first_name', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'last_name', isset($userDetails) ? $userDetails->last_name : '', [
                            'class' => 'form-control',
                            'id'    => 'last_name',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('last_name'))
                                {!! $errors->first('last_name', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('company', 'Company: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'company', isset($userDetails) ? $userDetails->company : '', [
                            'class' => 'form-control',
                            'id'    => 'company',
                            'style' => 'display: inline;',
                            'required' => 'required',
                            ]) !!}

                            @if ($errors->has('company'))
                                {!! $errors->first('company', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'title', isset($userDetails) ? $userDetails->title : '', [
                            'class' => 'form-control',
                            'id'    => 'company',
                            'style' => 'display: inline;',
                            ]) !!}

                            @if ($errors->has('title'))
                                {!! $errors->first('title', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('address1', 'Address1: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'address1', isset($userDetails) ? $userDetails->address1 : '', [
                            'class' => 'form-control',
                            'id'    => 'address1',
                            'style' => 'display: inline;',
                            'required' => 'required',
                            ]) !!}

                            @if ($errors->has('address1'))
                                {!! $errors->first('address1', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('address2', 'Address2: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'address2', isset($userDetails) ? $userDetails->address2 : '', [
                            'class' => 'form-control',
                            'id'    => 'address2',
                            'style' => 'display: inline;'
                            ]) !!}

                            @if ($errors->has('address2'))
                                {!! $errors->first('address2', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('city', 'City: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'city', isset($userDetails) ? $userDetails->city : '', [
                            'class' => 'form-control',
                            'id'    => 'city',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('city'))
                                {!! $errors->first('city', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('state', 'State: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::select('state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->state : '', [
                                'id'    => 'state',
                                'class' => 'form-control',
                                'style' => 'display: inline;',
                                'required' => 'required'
                            ]) !!}

                            @if ($errors->has('state'))
                                {!! $errors->first('state', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('zip', 'Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'zip', isset($userDetails) ? $userDetails->zip : '', [
                            'class' => 'form-control',
                            'id'    => 'zip',
                            'style' => 'display: inline;',
                            'required'  => 'required'
                            ]) !!}

                            @if ($errors->has('zip'))
                                {!! $errors->first('zip', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::rLabel('phone', 'Phone: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'phone',  isset($userDetails) ? $userDetails->phone : '', [
                                'class' => 'form-control',
                                'id'    => 'phone',
                                'style' => 'display: inline;',
                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                'data-mask',
                                'required' => 'required',
                            ]) !!}

                            @if ($errors->has('phone'))
                                {!! $errors->first('phone', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('phone_local', 'Extension: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'phone_local',  isset($userDetails) ? $userDetails->phone_local : '', [
                                'class' => 'form-control',
                                'id'    => 'phone_local',
                                'style' => 'display: inline;',
                            ]) !!}

                            @if ($errors->has('phone'))
                                {!! $errors->first('phone', '<div class="alert alert-danger">:message</div>') !!}
                            @endif
                        </div>

                        {!! Form::label('fax', 'Fax: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('tel', 'fax', isset($userDetails) ? $userDetails->fax : '', [
                                'class' => 'form-control',
                                'id'    => 'fax',
                                'style' => 'display: inline;',
                                'data-inputmask' => '"mask" : "(999) 999-9999"',
                                'data-mask',
                                'placeholder' => 'Fax'
                            ]) !!}

                            @if ($errors->has('fax'))
                                {!! $errors->first('fax', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            <button type="submit" class="btn btn-flat btn-primary btn-save">Save</button>
                        </div>
                    </div>

                    {!! Form::close() !!}

                    <hr>

                    {!! Form::open(['method' => 'POST', 'url' => route('same_as_billing_or_shipping.edit_profile'),  'class' => 'edit-profile-form']) !!}

                    {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                    {!! Form::input('hidden', 'address_same_as_billing', 0) !!}

                    <div class="panel-inner panel-body">

                        @if (Session::has('edit_billing_info'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::label('same_as_billing', 'Same As Billing: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::checkbox('address_same_as_billing', '', isset($userDetails) && $userDetails->address_same_as_billing ? true : false, [
                                'class' => 'same-as',
                                'id'    => 'address_same_as_billing',
                                'style' => 'display: inline;'
                            ]) !!}
                        </div>

                        <div class="address_same_as_billing" style="@if ($userDetails->address_same_as_billing) display:none @endif">

                            {!! Form::rLabel('billing_address', 'Billing Address: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'billing_address', isset($userDetails) ? $userDetails->billing_address : '', [
                                'class' => 'form-control',
                                'id'    => 'billing_address',
                                'style' => 'display: inline;',
                                'required' => 'required',
                                ]) !!}
                            </div>


                            {!! Form::rLabel('billing_city', 'Billing City: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'billing_city', isset($userDetails) ? $userDetails->billing_city : '', [
                                'class' => 'form-control',
                                'id'    => 'billing_city',
                                'style' => 'display: inline;',
                                'required'  => 'required'
                                ]) !!}
                            </div>


                            {!! Form::rLabel('billing_state', 'Billing State: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::select('billing_state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->billing_state : '', [
                                    'id'    => 'billing_state',
                                    'class' => 'form-control',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('billing_zip', 'Billing Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'billing_zip', isset($userDetails) ? $userDetails->billing_zip : '', [
                                'class' => 'form-control',
                                'id'    => 'billing_zip',
                                'style' => 'display: inline;',
                                'required'  => 'required'
                                ]) !!}

                                <button type="submit" class="btn btn-flat btn-primary btn-save">Save</button>
                            </div>

                        </div>

                    </div>

                    {!! Form::close() !!}

                    <hr>

                    {!! Form::open(['method' => 'POST', 'url' => route('same_as_billing_or_shipping.edit_profile'),  'class' => 'edit-profile-form']) !!}

                    {!! Form::input('hidden', 'contact_id', isset($userDetails) ? $userDetails->id : '' ) !!}

                    {!! Form::input('hidden', 'address_same_as_shipping', 0) !!}

                    <div class="panel-inner panel-body">

                        @if (Session::has('edit_shipping_info'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::label('address_same_as_shipping', 'Same As Shipping: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::checkbox('same_as_shipping', '', isset($userDetails) && $userDetails->address_same_as_shipping? true : false, [
                                'class' => 'same-as',
                                'id'    => 'address_same_as_shipping',
                                'style' => 'display: inline;'
                            ]) !!}
                        </div>

                        <div class="address_same_as_shipping" style="@if ($userDetails->address_same_as_shipping) display:none @endif">

                            {!! Form::rLabel('shipping_address', 'Shipping Address: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'shipping_address', isset($userDetails) ? $userDetails->shipping_address : '', [
                                'class' => 'form-control',
                                'id'    => 'shipping_address',
                                'style' => 'display: inline;',
                                'required' => 'required',
                                ]) !!}
                            </div>

                            {!! Form::rLabel('shipping_city', 'Shipping City: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'shipping_city', isset($userDetails) ? $userDetails->shipping_city : '', [
                                'class' => 'form-control',
                                'id'    => 'shipping_city',
                                'style' => 'display: inline;',
                                'required'  => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('shipping_state', 'Shipping State: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::select('shipping_state', ['' => '-- Select State --'] + $states, isset($userDetails) ? $userDetails->shipping_state : '', [
                                    'id'    => 'billing_state',
                                    'class' => 'form-control',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('shipping_zip', 'Shipping Zip: ', ['class' => 'col-sm-4 control-label']) !!}
                            <div class="form-group col-md-8">
                                {!! Form::input('text', 'shipping_zip', isset($userDetails) ? $userDetails->shipping_zip : '', [
                                'class' => 'form-control',
                                'id'    => 'shipping_zip',
                                'style' => 'display: inline;',
                                'required'  => 'required'
                                ]) !!}

                                <button type="submit" class="btn btn-flat btn-primary btn-save">Save</button>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent

    <script>
        (function ($){
            var $form = $('#toggleUserStatus');
            $('.same-as').on('change', function() {
                var $this = $(this),
                    id    = $this.attr('id'),
                    $container = $('div.' + id),
                    contact_id    = "{{ $userDetails->id }}";

                if ($this.prop('checked')) {
                    $container.hide();

                    $.ajax({
                        type : 'POST',
                        url  : "{{ route('same_as_address') }}",
                        data : {
                            contact_id : contact_id,
                            column : id
                        },
                        beforeSend : function(data) {
                            $this.after(" <label><i>Saving...</i></label>");
                        },
                        success : function(response) {
                            setTimeout(function() {
                                $this.siblings('label').remove();
                            }, 1500)
                        }
                    });

                    return;
                }

                $container.show();
            });

            $('.submitButton').on('click', function() {
                var text = $(this).data('text');
                Modal.showConfirmationModal(
                        "Are you sure you want to "+text+" this user?",
                        'User Account',
                        function() {
                            $form.submit();
                            return false;
                        });
                return false;
            });
            $( "#company" ).autocomplete({
                source: "/api/companies",
                minLength: 2,
            });

            $('.delete-customer').on('click', function(e) {
                var id = $(this).data('id');

                e.preventDefault();

                Modal.showConfirmationModal(
                    'Are you sure you want to delete this customer?',
                    'Delete',
                    function() {
                        var $okBtn = $('#notification-ok');

                        $.ajax({
                            type: 'POST',
                            url: "{{ route('admin.customer.delete') }}",
                            data: { id: id },
                            success: function() {
                                window.location.href = "{{ route('admin.dashboard') }}";
                            }
                        });
                    }
                );


            });
        })(jQuery)
    </script>
@endsection