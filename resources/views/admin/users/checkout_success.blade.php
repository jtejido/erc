@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1>Successful Payment</h1>
    </section>

    <section class="content">
        <div class="row payment-success">
            <div class="alert alert-success alert-dismissable success-payment">
                <h4><i class="icon fa fa-check"></i> Your order has been processed!</h4>
                An email is coming to confirm your purchase.
                <a href="{{ route('admin.user_details', $params) }}">Click here</a> to view transaction
            </div>
        </div>
    </section>

@endsection