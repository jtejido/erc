@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1 class="cart-header">
            <a href="{{ route('admin.user_details', ['user_id' => $user->id]) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Shopping Cart
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', ['user_id' => $user->id]) }}">User</a></li>
            <li><a href="">Transaction</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row row-order-items">
            {!! OrderUtility::extractItems($item, $user) !!}
        </div>
    <section>

@endsection
