@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>@if ($orderItemId) Swap @endif Attendees</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">@if ($orderItemId) Swap @endif Attendees</a></li>
        </ol>
    </section>

    {{-- */ $registrable = $class ?: $course; $classId = $class ? $class->id : 0; /* --}}

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="inline">
                            {{ $course->title }}
                        </h4>

                        @if ($class)
                            <br>
                            <p class="small inline">
                                {{ $class->date_range }}
                            </p>
                        @endif

                        <button class="btn btn-flat btn-success pull-right" id="add-contact"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-user_id="{{ $user->id }}"
                                data-url="{{ route('contact.form.get') }}">

                            <i class="fa fa-plus"></i> Create New Contact
                        </button>
                    </div>

                    <div class="box-body">

                        <input type="hidden" id="item-id" value="@if (isset($orderItemId)) {{ $orderItemId }} @else 0 @endif">
                        <input type="hidden" id="reg-count" value="@if (isset($regCount)) {{ $regCount }} @else 0 @endif">
                        <input type="hidden" id="is-swap" value="@if (isset($orderItemId)) 1 @else 0 @endif">

                        @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @include('errors.form_errors')

                        <table class="table table-bordered table-hover" id="table-contacts-reg">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="check-all"></th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            @if (!in_array($user->contact->id, $notIn))

                                {{-- */

                                $checked = (!count($attendees) and !isset($orderItemId))
                                     ? 'checked'
                                     : (in_array($user->contact->id, $attendees)
                                     ? 'checked' : '');

                                $noSwapYearlySub = in_array($user->contact->id, $noSwapYearlySubs);
                                $noSwapGSA = in_array($user->contact->id, $noSwapGSAs);
                                $noSwapCancel = in_array($user->contact->id, $noSwapCancelled);

                                $disabled = (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId)) ? 'disabled' : '';

                                $isContactRegistered = OrderUtility::isContactRegistered($user->contact->id, $registrable);

                                /* --}}

                                <tr class="{{ ($isContactRegistered and !isset($orderItemId)) ? 'registered' : '' }}">
                                    <td width="1%">
                                        @if(!$isContactRegistered or isset($orderItemId))
                                            <input type="checkbox" name="contact[]" value="{{ $user->contact->id }}"
                                                class="contact check-single" {{ $checked }} {{ $disabled }}>
                                        @endif
                                    </td>
                                    <td>
                                        <label for="contact_{{ $user->contact->id }}" title="{{ $user->contact->id }}">
                                            {{ $user->contact->name }}
                                        </label>
                                        @if($isContactRegistered and !isset($orderItemId))
                                            <p class="text-info">
                                                <small>Already Registered</small>
                                            </p>
                                        @endif
                                    </td>
                                    <td>
                                        <p>{{ $user->email }}</p>

                                        @if ($user->hasSubscription and $user->hasSubscription->activated and !$noSwapYearlySub)
                                            <p class="block label label-info btn-flat btn-block">
                                                Has Yearly Subscription
                                            </p>
                                        @elseif ($noSwapCancel and isset($orderItemId))
                                            <p class="required">
                                                Attendee can not be swapped because of registration is cancelled.
                                            </p>
                                        @elseif ($noSwapYearlySub and isset($orderItemId))
                                            <p class="required">
                                                Attendee can not be swapped because of Yearly Subscription.
                                            </p>

                                        @elseif ($noSwapGSA and isset($orderItemId))
                                            <p class="required">
                                                Attendee can not be swapped because of GSA Discount.
                                            </p>
                                        @endif

                                    </td>
                                    <td>{{ $user->contact->phone }}</td>
                                    <td>
                                        <p>{{ $user->contact->address1 }} {{ $user->contact->address2 }}</p>
                                        <p class="block">{{ $user->contact->city }} {{ $user->contact->state }} {{ $user->contact->zip }}</p>
                                    </td>
                                    <td>--</td>
                                    <td>
                                        <a href="{{ route('admin.profile.edit', ['contact_id' => $user->contact->id]) }}"
                                           class="btn btn-primary btn-block btn-flat">
                                            Edit Profile
                                        </a>
                                    </td>
                                </tr>

                            @endif

                            @if ($contacts->count())
                                @foreach ($contacts as $contact)

                                        {{-- */

                                            $checked = (is_array($attendees) && in_array($contact->id, $attendees)
                                                ? 'checked'
                                                : Session::has('contact_id') &&
                                                  Session::get('contact_id') == $contact->id
                                                ? 'checked' : '');

                                            $noSwapYearlySub = in_array($contact->id, $noSwapYearlySubs);
                                            $noSwapGSA = in_array($contact->id, $noSwapGSAs);
                                            $noSwapCancel = in_array($contact->id, $noSwapCancelled);

                                            $disabled = (($noSwapYearlySub or $noSwapGSA or $noSwapCancel) and isset($orderItemId)) ? 'disabled' : '';

                                            $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $registrable);

                                        /* --}}
                                        <tr class="{{ ($isContactRegistered and !isset($orderItemId)) ? 'registered' : '' }}">
                                            <td width="1%">
                                                @if(! $isContactRegistered or isset($orderItemId))
                                                <input type="checkbox" name="contact[]" value="{{ $contact->id }}"
                                                        id="contact_{{ $contact->id }}" class="contact check-single"
                                                        data-contact_id="{{ $contact->id }}" {{ $checked }} {{ $disabled }}>
                                                @endif
                                            </td>
                                            <td>
                                                <label for="contact_{{ $contact->id }}" title="{{ $contact->id }}">
                                                {{ $contact->name }}
                                                </label>
                                                @if($isContactRegistered and !isset($orderItemId))
                                                    <p class="text-info">
                                                        <small>Already Registered</small>
                                                    </p>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($contact->user)
                                                    <p>{{ $contact->user->email }}</p>

                                                    @if ($contact->user->hasSubscription and $contact->user->hasSubscription->activated and !$noSwapYearlySub)
                                                        <p class="block label label-info btn-flat btn-block">
                                                            Has Yearly Subscription
                                                        </p>
                                                    @endif
                                                @endif

                                                @if ($noSwapCancel and isset($orderItemId))
                                                    <p class="required">
                                                        Attendee can not be swapped because of registration is cancelled.
                                                    </p>

                                                @elseif ($noSwapYearlySub and isset($orderItemId))
                                                    <p class="required">
                                                        Attendee can not be swapped because of Yearly Subscription.
                                                    </p>

                                                @elseif ($noSwapGSA and isset($orderItemId))
                                                    <p class="required">
                                                        Attendee can not be swapped because of GSA Discount.
                                                    </p>

                                                @elseif ($pairedClassSwapNote and isset($orderItemId))
                                                    <p class="required">
                                                        This will also swap attendee's {{ $pairedClassSwapNote }} registration.
                                                    </p>
                                                @endif


                                            </td>
                                            <td>{{ $contact->phone }}</td>
                                            <td>
                                                <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                                <p class="block">{{ $contact->city }} {{ $contact->state }} {{ $contact->zip }}</p>
                                            </td>
                                            <td>
                                                @if ($contact->note)
                                                    {{ $contact->note }}
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.profile.edit', ['contact_id' => $contact->id]) }}"
                                                   class="btn btn-primary btn-block btn-flat">
                                                    Edit Profile
                                                </a>
                                            </td>
                                        </tr>

                                @endforeach
                            @endif

                        </table>
                        <hr/>

                        @if (isset($orderItemId))
                            <a href="{{ route('admin.user_details', ['contact_id' => $user->contact_id, 'user_id' => $user->id]) }}" class="btn btn-flat btn-default">
                                Return to User Portal
                            </a>
                        @endif

                        <button class="btn btn-flat btn-primary" id="register-class"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-agent="{{ $user->id }}"
                                data-url="@if(isset($orderItemId)){{ route('attendees.do_swap') }}@else{{ route('contact.add_to_class') }}@endif">
                            @if (isset($orderItemId))
                                Save
                            @else
                                Add To Registration
                            @endif
                        </button>

                        @if (isset($orderItemId))
                            &nbsp;&nbsp;&nbsp;<label><input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                                Do Not Email Agent And Attendees</label>
                        @endif

                    </div>
                </div>
            </div>
        </div>

        @if ($pairedClassSwapNote)
            <input type="hidden" id="paired-class" value="{{ $pairedClassSwapNote }}"
               data-registration-order-id="{{ $registrationOrderId }}"
               data-url="{{ route('attendees.do_swap.notification') }}">
        @endif

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

        <!-- Modal for adding / editing class -->

        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

            var oTable = $("#table-contacts-reg").DataTable({
                "columnDefs" : [{
                    "targets": 0,
                    "orderable": false,
                    "searchable": false
                }],
                "order": []
            });

            oSettings = oTable.settings();

            $('#register-class').on('click', function() {

                oSettings[0]._iDisplayLength = oSettings[0].fnRecordsTotal();
                oTable.search( '' ).draw();

                var $this = $(this),
                    contacts = [],
                    url = $this.data('url'),
                    class_id = $this.data('class_id'),
                    course_id = $this.data('course_id'),
                    item = $.trim($('#item-id').val()),
                    regCount = $.trim($('#reg-count').val()),
                    $container = $('.attendees-list').length ? $('.attendees-list') : $('.box-body'),
                    $alert = $container.find('.alert-danger'),
                    $modal = $('#contactModal'),
                    userId = $this.data('agent') ? $this.data('agent') : 0,
                    isSwap = $('#is-swap').val(),
                    doNotEmail = $('#doNotEmail').is(':checked');

                $('.contact').each(function() {
                    if ($(this).prop('checked')) {
                        contacts.push($(this).val());
                    }
                });

                if (contacts.length) {

                    function doRegistration() {
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {
                                class_id: class_id,
                                course_id: course_id,
                                contacts: contacts,
                                item: item,
                                user_id: userId,
                                doNotEmail: doNotEmail

                            },
                            beforeSend : function(data) {
                                $this.attr('disabled', true);
                            },
                            success: function (response) {
                                try {
                                    if (typeof response !== 'object') {
                                        response = jQuery.parseJSON(response);
                                    }

                                    window.location.href = response.data.route;
                                } catch (e) {
                                    $modal.find('.modal-content').addClass('panel-danger');
                                    $modal.find('.modal-body').html('There was an issue with your request.');
                                    $modal.modal('show');
                                    $this.attr('disabled', false);
                                }

                            }
                        });
                    }

                    if (isSwap != 0 && item != 0 && regCount != contacts.length){
                        var text = 'Number of attendees (' + regCount + ') should be the same.';

                        if ($alert.length == 0) {
                            $container.prepend("<div class='alert alert-danger'>" + text + "</div>");
                        }

                        else if ($alert.length == 1) {
                            $alert.html(text);
                        }

                        $('body, html').animate({
                            scrollTop : $container.offset().top - 10
                        }, 1000);

                        return false;
                    }

                    var $pairedClass = $('#paired-class');

                    if ($pairedClass.length) {

                        $.ajax({
                            type: 'POST',
                            url: $pairedClass.data('url'),
                            data: {
                                registration_order_id: $pairedClass.data('registration-order-id'),
                                contacts: contacts
                            },
                            success: function(response) {
                                if (response) {
                                    $container.append(response);

                                    var $swapModal = $('#swapNotificationModal'),
                                        $notificationOk = $swapModal.find('#notification-ok'),
                                        $notificationCancel = $swapModal.find('#notification-cancel');

                                    $swapModal.modal('show');

                                    $notificationOk.on('click', function () {
                                        var $this = $(this);
                                        $swapModal.attr('data-backdrop', 'static')
                                            .attr('data-keyboard', false);

                                        $this.text('Swapping')
                                            .attr('disabled', true);

                                        $notificationCancel.attr('disabled', true);

                                        doRegistration();
                                    });
                                }

                                else {
                                    doRegistration();
                                }
                            }
                        });
                    }

                    else {
                        doRegistration();
                    }

                }
            });

        })(jQuery);
    </script>
@endsection