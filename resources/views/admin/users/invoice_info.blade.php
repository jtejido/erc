@extends('admin.layout')

@section('content')

    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1 class="cart-header">
            Payment Details
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.user_details', $params) }}">User</a></li>
            <li><a href="">Transaction</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row payment-details">
            <div class="inner col-md-7">

                @include('admin.includes.success_message')
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-12">
                    <div class="panel panel-default credit-card-box">
                        <div class="panel-heading display-table">
                            <div class="row display-tr">
                                <h3 class="panel-title display-td">Order Details</h3>
                            </div>
                        </div>
                        <div class="panel-body">
                            <input type="hidden" name="order_tax" value="{{$order->order_tax}}">
                            <input type="hidden" name="shipping_fee" value="{{$order->shipping_rates}}">
                            <div>
                                <dl class="dl-horizontal">
                                    <dt>Agent</dt>
                                    <dd>{{ $order->user->name }} ({{ $order->user->email }})</dd>
                                    <dt>Shipping</dt>
                                    <dd>{!! $order->complete_shipping_address  !!}</dd>
                                    <dt>Billing</dt>
                                    <dd>{!! $order->complete_billing_address  !!}</dd>
                                </dl>
                                @if($order->paymentTransactions->count())
                                <p class="text-center">
                                    <a class="btn btn-default"
                                       href="{{ route("admin.payment.summary", [
                        "user_id" => $order->user_id,
                        "order_id" => $order->id
                    ]) }}">View Payment Transactions</a>
                                </p>
                                @endif
                            </div>
                            <hr/>

                            @if(! $order->isCompleted)
                            <label>Pay via</label>
                            {!! Form::open(['route' => ['admin.invoice.update-status', $user->id],
                                'id'    => 'choose-payment-form']) !!}
                                <input type="hidden" name="payment_type" id="payment_type" value="CASH" />
                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                <a href="{{route('admin.order.invoice_info.checkout',[$user->id,
                                    $order->id])}}" class="btn btn-warning">Credit Card</a>&nbsp;
                                <a href="#" class="btn btn-default" id="cash_button">Cash</a>&nbsp;
                                <a href="#" class="btn btn-default" id="check_button">Check</a>

                            <div id="payment_form_fields">
                                <hr/>
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                    <input type="number" step="0.01" name="amount" placeholder="Amount *" class="form-control" required/>
                                    </div>
                                </div>

                                <div class="col-xs-5">
                                    <div class="form-group">
                                    <input name="check_num" placeholder="Check Number" class="form-control" />
                                    </div>
                                </div>

                                <div class="col-xs-3">
                                    &nbsp;
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="invoice_status" value="1" checked="checked"/>&nbsp;
                                        Change Invoice Status to Complete
                                    </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <button class="form-control btn btn-warning btn-inline">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                            </div>

                            {!! Form::close() !!}
                            <hr/>
                            @endif
                            <label>Manually Update Status</label>
                            {!! Form::open(['route' => ['admin.invoice.update-status', $user->id], 'class' => 'form-inline']) !!}
                                <input type="hidden" name="order_id" value="{{$order->id}}">
                                <input type="hidden" name="update_status_only" value="1">
                                <div class="form-group">
                                    <select name="status" id="" class="form-control" style="width: 300px">
                                        <option value="{{($order->status != 'COMPLETE')?$order->status:'INVOICED'}}" {{($order->status != 'COMPLETE')?'selected="selected"':''}}>Invoiced</option>
                                        <option value="COMPLETE" {{($order->status == 'COMPLETE')?'selected="selected"':''}}>Paid</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Update</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div><!-- panel -->
                </div>
                <input type="hidden"
                       name="validate_url"
                       value="{{ route('ajax.file.validate') }}"
                       id="path-to-validate-file">

                @foreach ($invoices as $invoice)

                {!! Form::open([
                      'route' => ['admin.invoice_checkout', $user->id],
                      'id' => 'edit-payment-details-form',
                      'enctype' => 'multipart/form-data',
                      'class' => 'col-md-12'
                    ])
                !!}

                <div class="panel panel-default credit-card-box">
                    <div class="panel-heading display-table">
                        <div class="row display-tr">
                            <h3 class="panel-title display-td">Purchase Order Payment Details</h3>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="error-container"></div>
                        <div class="row">
                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                            <input type="hidden" name="invoice_payment" value="{{ $invoice->id }}">

                            <div class="col-md-6">
                                <div class="form-group change-purchase-order">
                                    <button class="btn btn-flat btn-primary btn-md btn-block btn-change-invoice"
                                            data-target="select-purchase-order"
                                            data-hidden="change-purchase-order">
                                        Upload Purchase Order
                                    </button>
                                </div>

                                <div class="form-group select-purchase-order hidden">
                                    {!! Form::label('po_file', 'Purchase Order File') !!}
                                    <a href="#" class="btn-cancel-change"
                                            data-target="change-purchase-order"
                                            data-hidden="select-purchase-order">Cancel</a>
                                    <button class="btn btn-flat btn-success btn-md btn-block btn-upload">
                                        Select Purchase Order File/Image
                                    </button>

                                    {!! Form::file('po_file', [
                                        'accept'    => 'application/pdf, image/*',
                                        'id'        => 'po_file',
                                        'class'     => 'upload-file hidden'])
                                    !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- */ $hiddenPoClass = ''; /* --}}

                                @if ($invoice->poImage)
                                    <div class="form-group change-purchase-order">
                                        {!! HTML::image(route('invoice_image.preview',
                                            ['type' => \App\Utilities\Constant::PO_IMAGE,
                                            'order_id' => $order->id,
                                            'invoice_id' => $invoice->id,
                                            'file_name' => $invoice->poImage]),
                                            'Payment Order Image', [
                                            'class' => 'payment-image img-responsive center-block',
                                        ]) !!}

                                        <div class="download-payment">
                                            @if (isset($invoice->poFile))
                                                <a href="{{ route('invoice_image.preview',
                                                    ['type' =>  \App\Utilities\Constant::PO_FILE,
                                                     'order_id' => $order->id,
                                                     'invoice_id' => $invoice->id,
                                                     'file_name' => $invoice->poFile]) }}"
                                                   class="text-center" download="{{ $invoice->poFile }}">
                                                    Download Payment Order
                                                </a>

                                            @else
                                                <a href="javascript:void(0)"
                                                   data-order_id="{{ $order->id }}"
                                                   data-invoice_id="{{ $invoice->id }}"
                                                   data-type="{{ \App\Utilities\Constant::PO_IMAGE }}"
                                                   data-file_name="{{ $invoice->poImage  }}"
                                                   class="text-center download-invoice">
                                                    Download Payment Order
                                                </a>
                                            @endif
                                        </div>
                                    {{-- */ $hiddenPoClass = 'hidden'; /* --}}
                                    </div>
                                @endif

                                <div class="form-group select-purchase-order {{ $hiddenPoClass }}">
                                    {!! HTML::image('img/placeholder-image.jpg', 'PO Image/File', [
                                        'class' => 'po_file payment-image img-responsive center-block'
                                    ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row invoice-details">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('po_number', 'Purchase Order Number') !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::input(
                                        'text',
                                        'po_number',
                                        $invoice->po_number, [
                                        'placeholder' => 'Purchase Order Number',
                                        'class' => 'form-control invoice-text center-block'
                                        ]
                                    ) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row hidden">
                            <hr>
                        </div>

                        <div class="row hidden">
                            <div class="col-md-6">
                                <div class="form-group change-check">
                                    <button class="btn btn-flat btn-primary btn-md btn-block btn-change-invoice"
                                            data-target="select-check"
                                            data-hidden="change-check">
                                        Change Check
                                    </button>
                                </div>

                                <div class="form-group select-check hidden">
                                    {!! Form::label('check_file', 'Check Image or File') !!}
                                    <a href="#" class="btn-cancel-change"
                                            data-target="change-check"
                                            data-hidden="select-check">Cancel</a>
                                    <button class="btn btn-flat btn-success btn-md btn-block btn-upload">
                                        Select Check
                                    </button>
                                    {!! Form::file('check_file', [
                                        'accept'   => 'application/pdf, image/*',
                                        'id'       => 'check_file',
                                        'class'    => 'upload-file hidden'])
                                    !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                {{-- */ $hiddenCheckClass = ''; /* --}}

                                @if ($invoice->checkImage)
                                    <div class="form-group change-check">
                                        {!! HTML::image(route('invoice_image.preview',
                                        ['type' => \App\Utilities\Constant::CHECK_IMAGE,
                                        'order_id' => $order->id,
                                        'invoice_id' => $invoice->id,
                                        'file_name' => $invoice->checkImage]), 'Check Image', [
                                            'class' => 'payment-image img-responsive center-block',
                                        ]) !!}


                                        <div class="download-payment">
                                            @if (isset($invoice->checkFile))
                                                <a href="{{ route('invoice_image.preview',
                                                    ['type' =>  \App\Utilities\Constant::CHECK_FILE,
                                                     'order_id' => $order->id,
                                                     'invoice_id' => $invoice->id,
                                                     'file_name' => $invoice->checkFile]) }}"
                                                   class="text-center" download="{{ $invoice->checkFile }}">Download Check</a>

                                            @else
                                                <a href="javascript:void(0)"
                                                   data-order_id="{{ $order->id }}"
                                                   data-invoice_id="{{ $invoice->id }}"
                                                   data-type="{{ \App\Utilities\Constant::CHECK_IMAGE }}"
                                                   data-file_name="{{ $invoice->checkImage  }}"
                                                   class="text-center download-invoice">
                                                    Download Check
                                                </a>
                                            @endif
                                        </div>

                                    {{-- */ $hiddenCheckClass = 'hidden'; /* --}}
                                    </div>
                                @endif

                                <div class="form-group select-check {{ $hiddenCheckClass }}">
                                    {!! HTML::image('img/placeholder-image.jpg', 'Check Image/File', [
                                        'class' => 'check_file payment-image img-responsive center-block'
                                        ]) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row invoice-details hidden">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('check_number', 'Check Number') !!}
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::input(
                                      'text',
                                      'check_number',
                                      $invoice->check_number, [
                                      'placeholder' => 'Check Number',
                                      'class' => 'form-control invoice-text center-block'
                                      ]
                                  ) !!}
                                </div>
                            </div>
                        </div>

                        @if ($invoice->recipients->count())
                            <div class="row">
                                <hr>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label>Other Recipients : </label>

                                    <ul class="list">
                                        @foreach ($invoice->recipients as $recipient)
                                            <li>{{ $recipient->email }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>

                <div class="row margin-bottom">
                    <div class="col-md-6">
                        <a href="{{ route('admin.user_details', $params) }}"
                           class="btn btn-flat btn-primary btn-lg btn-block">
                            Cancel
                        </a>
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-flat btn-primary btn-lg btn-pay btn-block"
                                type="submit"
                                @if (!isset($order)) disabled @endif>
                            Save Payment Details
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}

                @endforeach
            </div>
            @include('main.checkout.payment_details')
        </div>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notes</h3>
                    </div>
                    <div class="panel-body">
                        <textarea name="note" class="notes"></textarea>
                    </div>
                    <div class="panel-footer text-right">
                        <button type="reset" class="btn btn-default" id="resetNote">Reset</button>
                        <button type="button" class="btn btn-primary" id="saveNote">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="container">
                <div class="col-md-9">

                    <ul class="media-list" id="note-list">
                     @if(count($notes)>0)
                        @foreach($notes as $note)
                        <li class="media panel">
                            <div class="media-body panel-body">
                                <h4 class="media-heading">{{ $note->user->email }} <br><small>{{ $note->created_at_string }}</small></h4>
                                {!! $note->notes !!}
                            </div>
                        </li>
                        @endforeach
                    @endif
                    </ul>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent

    <script type="text/javascript">
    var InvoiceInfo = {
        init : function() {
            this.writer();
            this.storeNote();
            this.resetNote();
        },
        resetNote: function(){
            $('#resetNote').click(function(e){
                tinyMCE.activeEditor.setContent('');
            })
        },
        storeNote: function(){
            $('#saveNote').click(function(e){
                var note = tinyMCE.activeEditor.getContent();
                var order_id = $('[name="order_id"]').val();
                var data = {
                        'note': note,
                        'order_id': order_id
                }

                $(this).attr('disabled','disabled');

                     $.ajax({
                        url: window.location.origin + '/admin/user/store-note',
                        type: 'post',
                        data : data,
                        success: function (res) {
                            var tem = InvoiceInfo.noteTemplate(res);
                            $('#note-list').prepend($(tem));
                            tinyMCE.activeEditor.setContent('');
                            $('#saveNote').removeAttr('disabled');
                        },
                        error: function (res) {

                        }
                    });

                e.preventDefault();
                e.stopImmediatePropagation();
            });
        },
        noteTemplate: function(data){
            return `<li class="media panel">
                        <div class="media-body panel-body">
                            <h4 class="media-heading">${data.email}<br><small>${data.created_at_string}</small></h4>
                            ${data.notes}
                        </div>
                    </li>
            `;
        },
        writer: function() {
            tinymce.init({
                selector: '.notes',
                height: 150,
                menubar: false,
                toolbar: [
                    'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link'
                ],
                theme_advanced_buttons1: "bold,italic,underline"
            });
        }
    };
    $(function() {
        InvoiceInfo.init();
        var amount = $('[name="shipping_fee"]').val();
        var order_tax = $('[name="order_tax"]').val();
        var fee_template = function(amount, title) {
            var ie_bff = ' <div class="row">';
            ie_bff += '<div class="col-md-8">';
            ie_bff +=  '<span class="text-left">'+title+'</span></div>';
            ie_bff +=  '<div class="col-md-4 text-right">'+amount+'</div>';
            ie_bff += '</div>';
            return ie_bff;
        };

        if(amount != 0)
            $('.sub-total-box').last().parent().before($(fee_template(amount, 'Shipping Fee:')));
        if(order_tax != 0)
            $('.sub-total-box').last().parent().before($(fee_template(order_tax, 'Sales Tax:')));

        $paymentForm = $('#choose-payment-form')
        $paymentFormFields = $('#payment_form_fields')
        $paymentType = $('#payment_type');

        $paymentFormFields.hide();

        $('#cash_button').click(function (e) {
            e.preventDefault();
            $paymentType.val('CASH');
            $paymentFormFields.show();
            $paymentForm.find("input[name='check_num']").hide();
        });

        $('#check_button').click(function (e) {
            e.preventDefault();
            $paymentType.val('CHECK');
            $paymentFormFields.show();
            $paymentForm.find("input[name='check_num']").show();
        });
    });


    </script>
@stop