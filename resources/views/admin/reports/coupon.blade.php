@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Coupon Reports</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('reports.coupon') }}">Coupon Reports</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="filters">
                    <form action="" method="GET">
                        <input type="text" class="form-control" name="title" value="{{ isset($title) ? $title : '' }}" style="display: inline; height: 35px !important;" placeholder="Class Title">
                        <button type="submit" class="btn btn-flat btn-primary"><span class="fa fa-search"></span></button>
                    </form>
                </div>
            </div>
            <div class="box-body">

                @if ($coupons->count() > 0)
                    <table id="table-reports-class" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Coupon Code</th>
                            <th>Amount</th>
                            <th>Discount Type</th>
                            <th>Classes</th>
                            <th>Count</th>
                        </tr>
                        </thead>

                        @foreach ($coupons as $coupon)
                            <tr>
                                <td>{{ $coupon->coupon_code  }}</td>
                                <td>{{ $coupon->coupon_amount }}</td>
                                <td>{{ $coupon->coupon_type }}</td>
                                <td>
                                    @if ($coupon->couponCourses->count())
                                        @foreach ($coupon->couponCourses as $couponCourse)
                                            @if($couponCourse->discountable)
                                            <span class="small">{{ $couponCourse->discountable->title }}</span> <br/>
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{ \App\Facades\DiscountUtility::countRegistrationsOfCoupon($coupon->id) }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <form action="{{ route('reports.coupon.generate') }}" method="GET" style="position: relative; top: 5px; display: inline">
                        <input type="hidden" name="coupon_ids" value="{{ $couponIds }}">
                        <button type="submit" class="btn btn-flat btn-success">Generate Report</button>
                    </form>

                    <div class="pull-right">
                        {!! $coupons->render() !!}
                    </div>
                @else
                    <i>No Coupons.</i>
                @endif
            </div>
        </div>
    </section>

    @include('includes.notification_modal')
@endsection
