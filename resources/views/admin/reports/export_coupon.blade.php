<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- 3rd Party CSS -->
    <link href="{{ url('css/print.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Playfair+Display+SC" rel="stylesheet">
    <style>
        .text-center {
            text-align: center;
        }

        h1.title {
            margin-top: 25px;
            margin-bottom: 25px;
            font-family: 'Playfair Display SC', serif;
            font-size: 4em;
        }

        html, body {
            height: 203mm;
            width: 297mm;
            font-size: 1.1em;
        }

        td {
            padding: 5px !important;
        }

        .required {
            color: #c23321;
        }

        .td-billing {
            width: 200px;
        }

        .small {
            font-size: 13px;
        }

        .no-border {
            border: 0 !important;
        }

        .keeptogether {page-break-inside:avoid;}

    </style>

</head>

<body>
<div class="container keeptogether">
    <h1>Coupon Count Report</h1>
    <table id="table-reports-coupon" class="table table-bordered keeptogether">
        <thead>
        <tr>
            <th class="text-center">Code</th>
            <th class="text-center">Amount</th>
            <th class="text-center">Discount Type</th>
            <th class="text-center">Classes</th>
            <th class="text-center">Count</th>
        </tr>
        </thead>

        @foreach ($coupons as $coupon)
            <tr class="keeptogether">
                <td>{{ $coupon->coupon_code  }}</td>
                <td>{{ $coupon->coupon_amount }}</td>
                <td>{{ $coupon->coupon_type }}</td>
                <td>
                    @if ($coupon->couponCourses->count())
                        @foreach ($coupon->couponCourses as $couponCourse)
                            @if($couponCourse->discountable)
                            <span class="small">{{ $couponCourse->discountable->title }}</span> <br>
                            @endif
                        @endforeach
                    @endif
                </td>
                <td>{{ \App\Facades\DiscountUtility::countRegistrationsOfCoupon($coupon->id) }}</td>
            </tr>
        @endforeach
    </table>
</div>
</body>
</html>