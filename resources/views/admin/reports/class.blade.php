@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Class Reports</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('reports.class') }}">Class Reports</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <div class="filters">
                    <form action="" method="GET">
                        <input type="text" class="form-control report-date" name="start_date" value="{{ isset($startDate) ? $startDate : '' }}" style="display: inline; height: 35px !important;" placeholder="Start Date">
                        <input type="text" class="form-control report-date" name="end_date" value="{{ isset($endDate) ? $endDate : '' }}" style="display: inline; height: 35px !important;" placeholder="End Date">
                        <input type="text" class="form-control" name="city" value="{{ isset($city) ? $city : '' }}" style="display: inline; height: 35px !important;" placeholder="City">
                        <select name="state" id="state" class="form-control" style="display: inline; height: 35px !important;">
                            <option value="">Select State</option>
                            @foreach ($states as $state)
                                <option value="{{ $state->state }}" @if (isset($sstate) and $sstate == $state->state) selected @endif>{{ $state->state }}</option>
                            @endforeach
                        </select>
                        <input type="text" class="form-control" name="title" value="{{ isset($title) ? $title : '' }}" style="display: inline; height: 35px !important;" placeholder="Class Title">
                        <button type="submit" class="btn btn-flat btn-primary"><span class="fa fa-search"></span></button>
                        <a href="{{ route('reports.class') }}" class="btn btn-success btn-flat">Clear</a>
                    </form>
                </div>
            </div>
            <div class="box-body">

                @if ($classes->count() > 0)
                    <table id="table-reports-class" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>City</th>
                            <th>Class</th>
                            <th>Count</th>
                        </tr>
                        </thead>

                        @foreach ($classes as $class)
                            <tr>
                                <td>{{ $class->dates }}</td>
                                <td>@if ($class->location) {{ $class->location->short_location }} @else -- @endif</td>
                                <td>{{ $class->title }}</td>
                                <td>{{ \App\Facades\CourseUtility::countAttendeesOfClass($class->id) }}</td>
                            </tr>
                        @endforeach
                    </table>

                    <form action="{{ route('reports.class.generate') }}" method="GET" style="position: relative; top: 5px; display: inline">
                        <input type="hidden" name="class_ids" value="{{ $classIds }}">
                        <button type="submit" class="btn btn-flat btn-success">Generate Report</button>
                    </form>

                    <div class="pull-right">
                        {!! $classes->render() !!}
                    </div>
                @else
                    <i>No Classes.</i>
                @endif

            </div>
        </div>
    </section>

    @include('includes.notification_modal')
@endsection
