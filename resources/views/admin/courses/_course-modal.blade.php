<div class="modal fade" id="courseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Courses</h4>
            </div>
            <div class="modal-body">
                <table class="table" id="courseList">
                    <thead>
                    <tr>
                        <th>SKU</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($courses as $c)
                        <tr>
                            <td>{{ $c->sku }}</td>
                            <td>{{ $c->title }}</td>
                            <td><a href="#"
                                   data-sku="{{ $c->sku }}"
                                   data-title="{{ $c->title }}"
                                   data-type="COURSE"
                                   data-id="{{ $c->id }}"
                                   class="associateBook">Link</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>