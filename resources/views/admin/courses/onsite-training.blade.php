@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>On-site Training Details</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>On-site Training</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                @include('admin.includes.success_message')
                @include('admin.includes.form_errors')
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">

                        {!! Form::model($course, [
                                'method'    => 'PUT',
                                'class'     => 'training-form',
                                'files'     => true
                                ]) !!}

                        {!! Form::hidden('type', 'OnsiteTraining') !!}
                        {!! Form::hidden('id', $course->id) !!}

                        @if (Session::has('message'))
                            <div class="form-group col-md-12">
                                <div class="alert alert-success">{{ Session::get('message') }}</div>
                            </div>
                        @endif

                        {!! Form::label('agent', 'Agent: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            <p>{{ $user->contact->first_name }} {{ $user->contact->last_name }}</p>
                            <input type="hidden" name="originating_agent_user_id" value="{{ $user->id }}">
                            <input type="hidden" name="price" value="0">
                        </div>

                        {!! Form::rLabel('date', 'Training Date: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::input('text', 'training_date', $course->training_date->format('F d, Y'), [
                            'class' => 'form-control',
                            'id'    => 'trainingDate',
                            'style' => 'display: inline;',
                            'required'
                            ]) !!}

                            @if ($errors->has('date'))
                                {!! $errors->first('date', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                        </div>


                        {!! Form::rLabel('address', 'Address: ', ['class' => 'col-sm-4 control-label']) !!}
                        <div class="form-group col-md-8">
                            {!! Form::textarea('address', null, [
                            'class' => 'form-control',
                            'id'    => 'address',
                            'style' => 'display: inline;',
                            'rows'  => 5,
                            'required'
                            ]) !!}

                            @if ($errors->has('date'))
                                {!! $errors->first('date', '<div class="alert alert-danger">:message</div>') !!}
                            @endif

                            {{-- */
                                $params = [
                                       'contact_id' => $user ? $user->contact->id : null,
                                       'user_id' => $user ? $user->id : null,
                                       ];
                            /* --}}

                            <a href="{{ route('admin.user_details', array_filter($params)) }}"
                               class="btn btn-flat btn-primary">Back</a>
                            <button type="submit" class="btn btn-flat btn-success">
                                Save
                            </button>
                        </div>

                        {!! Form::close() !!}


                    </div>
                    @if(isset($course))
                        <div class="col-md-6 col-xs-12" id="relatedItems">
                            <h4>Related Files <small>You may also upload the certificates here.</small>
                                <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#addFileModal">
                                    <i class="fa fa-file"></i>&nbsp;&nbsp;Add File
                                </button></h4>
                            <table class="table" id="fileEntryTable">
                                <thead>
                                <tr>
                                    <th width="25%">Title</th>
                                    <th>Filename</th>
                                    <th width="10%">Type</th>
                                    <th width="21%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!$course->files->count())
                                    <tr class="no-files"><td colspan="4"><p class="text-center">No Files Associated with this Course</p></td></tr>
                                @else
                                    @foreach($course->files as $file)
                                        <tr>
                                            <td>{{ $file->title }}</td>
                                            <td>{{ $file->original_filename }}</td>
                                            <td>{{ $file->type }}</td>
                                            <td>
                                                {!! Form::open([
                                                        'route'     => ['admin.course.deletematerial', $file->id],
                                                        'method'    => 'delete',
                                                        'class'     => 'form-inline deleteFileForm',
                                                ]) !!}
                                                <button type="button"
                                                        class="btn btn-xs btn-flat btn-danger delete-file">
                                                    <i class="fa fa-trash"></i>
                                                </button>&nbsp;
                                                <a href="{{ route('admin.course.getmaterial', [$file->id]) }}"
                                                   class="btn btn-primary btn-xs btn-flat">
                                                    <i class="fa fa-download"></i>
                                                </a>
                                                {!! Form::close() !!}

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>


                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <!-- Add File Modal -->
    @include('includes.fileupload_modal', [
                        'route'     => 'admin.course.addOnsitematerial',
                        'route_back'    => route('admin.course.onsite-training', [$order_item->id, $user->id]),
                        'item_id'   => isset($course) ? $course->id : '0',
                        'type'      => ['MATERIAL' => 'Material', 'CERTIFICATE' => 'Certificate']
                        ]);

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

            var     $associateForm = $("#associateForm"),
                    $unlinkForm = $("#unlinkForm");

            $('#addFileModal').on('hidden.bs.modal', function () {
                $('#addFileForm').trigger("reset");
            });

            $('.deleteFileForm').on('click', '.delete-file', function(e){
                var     $that = $(this);
                Modal.showConfirmationModal(
                        'Are you sure you want to delete this file?',
                        'Delete File',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
                return false;
            });

            $('#bookList, #courseList').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 2 }
                ]
            });

            var linkItem = function($that) {
                $associateForm.find("input[name='item_id']").val($that.data('id'));
                $associateForm.find("input[name='item_type']").val($that.data('type'));
                $associateForm.submit();
            }

            var unLinkItem = function($that) {
                $unlinkForm.find("input[name='item_id']").val($that.data('id'));
                $unlinkForm.find("input[name='item_type']").val($that.data('type'));
                $unlinkForm.submit();
            }

            $('#relatedItems').on('click', '.unlinkItem', function(e) {
                e.preventDefault();
                var $that = $(this), msg;
                msg = "Are you sure you want to unlink this item from this course?";
                Modal.showConfirmationModal(
                        msg,
                        'Unlink Item',
                        function() {
                            unLinkItem($that);
                            return false;
                        });
            });

            $('#bookList, #courseList').on('click', '.associateBook', function(e) {
                e.preventDefault();
                var $that = $(this), msg;

                msg = "Are you sure you want to link this item with this course?";
                msg += "<br/>";
                msg += '"' + $that.data('sku') + " - " + $that.data('title') + '"';

                $('#bookModal, #courseModal').modal('hide').one('hidden.bs.modal', function () {
                    Modal.showConfirmationModal(
                            msg,
                            'Associate Item With Course',
                            function() {
                                linkItem($that);
                                return false;
                            });
                });

                return false;
            });

            $("#file").fileinput({
                showPreview: false,
                showUpload: false
            });

            $('.test-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                }
            });
        })(jQuery);
    </script>
@endsection