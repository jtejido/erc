@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>@if (isset($course)) Edit Course @else Add New Course @endif</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.courses') }}">Courses</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                @include('admin.includes.success_message')
                @include('admin.includes.form_errors')
            </div>
            <div class="box-body">
                <div class="row">
            <div class="col-xs-12 col-md-7">

                {!! Form::open(['method' => 'POST', 'class' => 'form-horizontal']) !!}

                    @if (isset($course))
                        {!! method_field('put') !!}
                        {!! Form::input('hidden', 'id', $course->id) !!}
                    @endif

                {!! Form::label('topic', 'Topic: ', ['class' => 'col-sm-4']) !!}
                <div class="form-group col-sm-8">
                    {!! Form::select('topic_id', $topics, (isset($course) AND $course->topic) ? $course->topic->id : '', [
                        'id'    => 'type',
                        'class' => 'form-control ',
                    ]) !!}
                </div>

                    {!! Form::rLabel('title', 'Title: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::input('text', 'title', isset($course) ? $course->title : '', [
                            'class' => 'form-control ',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('slug', 'SEO Link: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        <div class="input-group">
                            {!! Form::input('text', 'slug', isset($course) ? $course->slug : '', [
                                'class' => 'form-control ',
                                'required' => 'required'
                            ]) !!}
                            <span class="input-group-btn">
                                <button class="btn btn-warning" id="verify_seo" type="button">Verify</button>
                            </span>
                        </div>
                        <p class="text-muted"><i>Verify first if link is available or taken</i>  <span id="seo_message" class="text-bold"></span> <br>
                            {{url('/')}}/courses/<span class="slug_link"></span>
                        </p>
                    </div>

                    {!! Form::label('description', 'Description: ', ['class' => 'col-sm-4', 'id' => 'desc-label']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::textarea('description',  isset($course) ? $course->description : '', [
                            'class' => 'form-control '
                        ]) !!}
                    </div>

                    {!! Form::rLabel('sku', 'SKU: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::input('text', 'sku', isset($course) ? $course->sku : '', [
                            'class' => 'form-control',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {!! Form::rLabel('category', 'Category: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {{-- */ $courseCatIds = isset($course) ? $course->courseCategories->pluck('category_id')->toArray() : [];  /* --}}

                        @foreach ($categories as $category)
                            <div class="categories-container">
                                {!! Form::checkbox('categories[]',
                                    $category->id,
                                    isset($course) && in_array($category->id, $courseCatIds)
                                    ? true
                                    : false
                                    ) !!}
                                <span>{{ $category->name }}</span>
                            </div>
                        @endforeach
                    </div>

                    {!! Form::rLabel('type', 'Type: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::select('course_type_id', $courseTypes, isset($course) ? $course->course_type_id : '', [
                            'id'    => 'type',
                            'class' => 'form-control ',
                            'required' => 'required'
                        ]) !!}
                    </div>

                    {{-- */ $isCourseMill = (isset($course) && $course->course_type_id == Constant::COMPUTER_BASED); /* --}}

                    <div class="course-mill-text @if(!$isCourseMill) hidden @endif">
                        {!! Form::label('course_mill_course_id', 'CourseMill Course Id: ', ['class' => 'col-sm-4']) !!}
                        <div class="form-group col-sm-8">
                            {!! Form::input('text', 'course_mill_course_id',
                                (isset($course) && $isCourseMill) ? $course->course_mill_course_id : '', [
                                'class' => 'form-control ',
                            ]) !!}
                        </div>
                    </div>

                    <div class="course-mill-text @if(!$isCourseMill) hidden @endif">
                        {!! Form::label('course_mill_session_id', 'CourseMill Course Session Id: ', ['class' => 'col-sm-4']) !!}
                        <div class="form-group col-sm-8">
                            {!! Form::input('text', 'course_mill_session_id',
                                (isset($course) && $isCourseMill) ? $course->course_mill_session_id : '', [
                                'class' => 'form-control ',
                            ]) !!}
                        </div>
                    </div>

                    {{-- */ $isWebinar = (isset($course) && $course->course_type_id == Constant::WEBCAST); /* --}}

                    <div class="webinar-text @if(!$isWebinar) hidden @endif">
                        {!! Form::label('meeting_id', 'Meeting URL: ', ['class' => 'col-sm-4']) !!}
                        <div class="form-group col-sm-8">
                            <div class="input-group">
                                <input type="text"
                                       id="meeting_id"
                                       name="meeting_id"
                                       style="height: 40px;"
                                       class="form-control"
                                       value="{{ (isset($course) && $isWebinar) ? $course->meeting_id : '' }}" />

                                <div class="input-group-addon">
                                    <a class="test-btn btn btn-default btn-xs"
                                       id="test-exam-btn"
                                       title="Click to test"
                                       target="_blank"
                                       href="{{ isset($course) ? $course->meetingUrl : '#'}}"><i class="fa fa-external-link-square"></i>
                                    </a>
                                </div>

                            </div>
                            @if (isset($course))
                                <p class="help-block">Paste the Meeting URL here.<br/><em>E.g. <code>https://aufreetrial.webex.com/join/greg.hermogmail.com</code></em></p>
                            @endif
                        </div>
                    </div>

                    <div class="webinar-text @if(!$isWebinar) hidden @endif">
                        {!! Form::label('test_link', 'Class Marker Test ID: ', ['class' => 'col-sm-4']) !!}
                        <div class="form-group col-sm-8">
                            <div class="input-group">
                                <input type="text"
                                       id="test_link"
                                       name="test_link"
                                       style="height: 40px;"
                                       class="form-control"
                                       value="{{ (isset($course) && $isWebinar) ? $course->test_link : '' }}" />

                                <div class="input-group-addon">
                                    <a class="test-btn btn btn-default btn-xs"
                                            id="test-exam-btn"
                                            title="Click to test"
                                            target="_blank"
                                            href="{{ isset($course) ? $course->testUrl : '#' }}"><i class="fa fa-external-link-square"></i>
                                    </a>
                                </div>

                            </div>
                            @if (isset($course))
                                <p class="help-block">Indicated in classmarker as "Email test link to users ".<br/>
                                    <em>E.g. https://www.classmarker.com/online-test/start/?quiz=<code>x3p57b5c0ff2e68c</code></em></p>
                            @endif
                        </div>
                    </div>

                    {!! Form::rLabel('price', 'Price: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::input('number', 'price', isset($course) ? $course->price : '', [
                            'class' => 'form-control ',
                            'step'  => 'any',
                            'min'       => '0',
                        ]) !!}
                    </div>

                    {!! Form::rLabel('duration', 'Duration: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::input('text', 'duration', isset($course) ? $course->duration : '', [
                            'class' => 'form-control ',
                        ]) !!}
                    </div>

                {!! Form::label('time', 'Time: ', ['class' => 'col-sm-4']) !!}
                <div class="form-group col-sm-8">
                    {!! Form::input('text', 'time', isset($course) ? $course->time : '', [
                        'class' => 'form-control ',
                    ]) !!}
                </div>

                    {!! Form::label('citation', 'Citation: ', ['class' => 'col-sm-4']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::textarea('citation', isset($course) ? $course->citation : '', [
                            'class' => 'form-control ',
                        ]) !!}
                    </div>

                {!! Form::label('additional_info', 'Additional Info: ', ['class' => 'col-sm-4']) !!}
                <div class="form-group col-sm-8">
                    {!! Form::textarea('additional_info',  isset($course) ? $course->additional_info : '', [
                        'class' => 'form-control '
                    ]) !!}
                </div>

                {!! Form::label('reviews', 'Reviews: ', ['class' => 'col-sm-4']) !!}
                <div class="form-group col-sm-8">
                    {!! Form::textarea('reviews',  isset($course) ? $course->reviews : '', [
                        'class' => 'form-control ',
                        'id'    => 'reviews'
                    ]) !!}
                </div>

                @if (isset($course) and $course->orders->count())
                    {!! Form::label('course_order', 'Select Course Order In: ', ['class' => 'col-sm-4']) !!}

                    <div class="form-group col-sm-8">
                        @foreach ($course->orders as $courseOrder)

                            <div class="course-order" style="overflow: auto">
                                <label style="width: 50%; float: left;">{{ $courseOrder->category->name }} - {{ $courseOrder->type->name }}</label>
                                {!! Form::select("course_order[$courseOrder->id]", \App\Facades\CourseUtility::getCourseOrderCount($courseOrder), $courseOrder->order, [
                                    'id'    => 'course_order_' . $courseOrder->id,
                                    'class' => 'form-control',
                                    'style' => 'margin-bottom: 10px; width: 50%; float: right;'
                                ]) !!}
                            </div>

                        @endforeach
                    </div>
                @endif


                    {!! Form::label('submit', '&nbsp;', ['class' => 'col-sm-4 control-label']) !!}
                    <div class="form-group col-sm-8">
                        {!! Form::input('submit', 'Save', 'Save', ['class' => 'btn btn-flat btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            @if(isset($course))
            <div class="col-md-5 col-xs-12" id="relatedItems">
                <h4>Course Materials
                <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#addFileModal">
                    <i class="fa fa-file"></i>&nbsp;&nbsp;Add File
                </button></h4>
                <table class="table" id="fileEntryTable">
                    <thead>
                    <tr>
                        <th width="25%">Title</th>
                        <th>Filename</th>
                        <th width="10%">Size</th>
                        <th width="21%">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!$course->materials()->count())
                        <tr class="no-files"><td colspan="4"><p class="text-center">No Files Associated with this Course</p></td></tr>
                    @else
                        @foreach($course->materials()->get() as $file)
                            <tr>
                                <td><a target="_blank" href=""{{ $file->path }}>{{ $file->title }}</a></td>
                                <td>{{ $file->original_filename }}</td>
                                <td>{{ $file->is_file ? $file->size : '' }}</td>
                                <td>
                                    {!! Form::open([
                                            'route'     => ['admin.course.deletematerial', $file->id],
                                            'method'    => 'delete',
                                            'class'     => 'form-inline deleteFileForm',
                                    ]) !!}
                                    <button type="button"
                                            class="btn btn-xs btn-flat btn-danger delete-file">
                                        <i class="fa fa-trash"></i>
                                    </button>&nbsp;

                                    @if ($file->is_file)
                                        <a href="{{ route('admin.course.getmaterial', [$file->id]) }}"
                                           class="btn btn-primary btn-xs btn-flat">
                                           <i class="fa fa-download"></i>
                                        </a>
                                    @endif

                                    {!! Form::close() !!}

                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

                <h4>Related Books
                    <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#bookModal">
                        <i class="fa fa-link"></i>&nbsp;&nbsp;Link a Book
                    </button>
                </h4>
                <table class="table">
                    <tbody>
                    @foreach($course->books as $book)
                        <tr>
                            <td><a href="{{ route('admin.books.show',[$book->id]) }}">{{ $book->code }}</a></td>
                            <td>{{ $book->name }}</td>
                            <td><a href="#"
                                   title="Unlink"
                                   data-id="{{ $book->id }}"
                                   data-type="BOOK"
                                   class="unlinkItem btn btn-xs btn-warning"><i class="fa fa-unlink"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($course->books->count() == 0)
                    <p class="text-center">No book associated with this course.</p>
                @endif
                <h4>Related Courses
                    <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#courseModal">
                        <i class="fa fa-link"></i>&nbsp;&nbsp;Link a Course
                    </button>
                </h4>
                <table class="table">

                    <tbody>
                    @foreach($course->courses as $c)
                        <tr>
                            <td>{!! $c->getLinkedCourseTitleAdmin() !!}</td>
                            <td>{{ $c->title }}</td>
                            <td><a href="#"
                                   title="Unlink"
                                   data-id="{{ $c->id }}"
                                   data-type="COURSE"
                                   class="unlinkItem btn btn-xs btn-warning"><i class="fa fa-unlink"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($course->courses->count() == 0)
                    <p class="text-center">No course associated with this course.</p>
                @endif

            </div>
            @endif
        </div>
            </div>
        </div>
    </section>

    <!-- Add File Modal -->
    @include('includes.fileupload_modal', [
                        'route'     => 'admin.course.addmaterial',
                        'item_id'   => isset($course) ? $course->id : '0',
                        'type'      => ['MATERIAL' => 'MATERIAL'],
                        'file_type' => ['FILE', 'LINK']
                        ]);

    @include('includes.confirmation_modal')
    @if( isset($course))
    <!-- Modal -->
    <div class="modal fade" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Books</h4>
                </div>
                <div class="modal-body">
                    <table class="table" id="bookList">
                        <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <td>{{ $book->code }}</td>
                                <td>{{ $book->name }}</td>
                                <td><a href="#"
                                       data-sku="{{ $book->code }}"
                                       data-title="{{ $book->name }}"
                                       data-type="BOOK"
                                       data-id="{{ $book->id }}"
                                       class="associateBook">Link</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('admin.courses._course-modal')
    {!! Form::open(['route'         => 'admin.course.associate.item',
                    'method'        => 'post',
                    'id'            => 'associateForm']) !!}
    {!! Form::hidden('course_id', $course->id) !!}
    {!! Form::hidden('item_id', 0) !!}
    {!! Form::hidden('item_type', '') !!}
    {!! Form::close() !!}

    {!! Form::open(['route'         => 'admin.course.unlink.item',
                    'method'        => 'post',
                    'id'            => 'unlinkForm']) !!}
    {!! Form::hidden('course_id', $course->id) !!}
    {!! Form::hidden('item_id', 0) !!}
    {!! Form::hidden('item_type', '') !!}
    {!! Form::close() !!}
    @endif
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

            $(document).ready(function () {

                function slugify(text)
                {
                    return text.toString().toLowerCase()
                        .replace(/\s+/g, '-')           // Replace spaces with -
                        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                        .replace(/^-+/, '')             // Trim - from start of text
                        .replace(/-+$/, '');            // Trim - from end of text
                }

                $('[name="title"]').on('blur',function(){
                    var title = $(this).val();
                    $('[name="slug"]').val(slugify(title));
                    $('.slug_link').text(slugify(title));
                });

                $('#verify_seo').click(function(){
                    var slug = $('[name="slug"]').val();
                    var id = $('[name="id"]').val();
                    $('#seo_message').removeClass('text-success');
                    $('#seo_message').removeClass('text-danger');
                    $('#seo_message').text('');
                    $.ajax({
                        url: '{{route('admin.course.verifyseo')}}',
                        type: 'post',
                        data: {
                            'slug' : slug,
                            'id' : id
                        },
                        success : function(res){
                            if(res.has_duplicate == false){
                                $('#seo_message').addClass('text-success');
                                $('#seo_message').text('Available');
                                $('.slug_link').text(slug);
                            }
                            if(res.has_duplicate == true){
                                $('#seo_message').addClass('text-danger');
                                $('#seo_message').text('Taken');
                            }
                        },
                        error: function(res){

                        }
                    });
                });

                tinymce.init({
                    selector: '#description',
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link charmap',
                        'fullscreen',
                        'paste'
                    ],
                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                });

                tinymce.init({
                    selector: '#reviews',
                    menubar: false,
                    plugins: [
                        'advlist autolink lists link charmap',
                        'fullscreen',
                        'paste'
                    ],
                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                });

                var     $associateForm = $("#associateForm"),
                    $unlinkForm = $("#unlinkForm");

                $('#addFileModal').on('hidden.bs.modal', function () {
                    $('#addFileForm').trigger("reset");
                });

                $('.deleteFileForm').on('click', '.delete-file', function(e){
                    var     $that = $(this);
                    Modal.showConfirmationModal(
                        'Are you sure you want to delete this file?',
                        'Delete File',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
                    return false;
                });

                $('#bookList, #courseList').DataTable({
                    "columnDefs": [
                        { "searchable": false, "orderable": false, "targets": 2 }
                    ]
                });

                var linkItem = function($that) {
                    $associateForm.find("input[name='item_id']").val($that.data('id'));
                    $associateForm.find("input[name='item_type']").val($that.data('type'));
                    $associateForm.submit();
                }

                var unLinkItem = function($that) {
                    $unlinkForm.find("input[name='item_id']").val($that.data('id'));
                    $unlinkForm.find("input[name='item_type']").val($that.data('type'));
                    $unlinkForm.submit();
                }

                $('#relatedItems').on('click', '.unlinkItem', function(e) {
                    e.preventDefault();
                    var $that = $(this), msg;
                    msg = "Are you sure you want to unlink this item from this course?";
                    Modal.showConfirmationModal(
                        msg,
                        'Unlink Item',
                        function() {
                            unLinkItem($that);
                            return false;
                        });
                });

                $('#bookList, #courseList').on('click', '.associateBook', function(e) {
                    e.preventDefault();
                    var $that = $(this), msg;

                    msg = "Are you sure you want to link this item with this course?";
                    msg += "<br/>";
                    msg += '"' + $that.data('sku') + " - " + $that.data('title') + '"';

                    $('#bookModal, #courseModal').modal('hide').one('hidden.bs.modal', function () {
                        Modal.showConfirmationModal(
                            msg,
                            'Associate Item With Course',
                            function() {
                                linkItem($that);
                                return false;
                            });
                    });

                    return false;
                });

                $("#file").fileinput({
                    showPreview: false,
                    showUpload: false
                });

                $('.test-btn').qtip({
                    style: {
                        classes: 'qtip-bootstrap'
                    }
                });

                $('#material-type').on('change', function() {
                    var value = $(this).val();

                    if (value == 0) {
                        $('.file-caption-main').removeClass('hidden');
                        $('.link').addClass('hidden');
                    }

                    if (value == 1) {
                        $('.file-caption-main').addClass('hidden');
                        $('.link').removeClass('hidden');
                    }
                });

                $('#desc-label').append("<span class=required>*</span>")

            });

        })(jQuery);
    </script>
@endsection