@extends('admin.layout')

@section('content')
    {{-- */
        $params = [
               'contact_id' => $user ? $user->contact->id : null,
               'user_id' => $user ? $user->id : null,
               ];
    /* --}}

    <section class="content-header">
        <h1>
            <a href="{{ route('admin.user_details', array_filter($params)) }}">
                {{ $user->contact->first_name }} {{ $user->contact->last_name }}'s
            </a>
            Select Course
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">Select Course</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header box-select">

                    </div>

                    <div class="box-body">

                        @if ($courses->count() > 0)
                            <table id="table-select-course" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>SKU</th>
                                    <th>Title</th>
                                    <th>Date</th>
                                    <th>Location</th>
                                    <th>Category</th>
                                    <th>Price ($)</th>
                                </tr>
                                </thead>

                                @foreach ($courses as $course)

                                    @if ($course->courseType->id == \App\Utilities\Constant::COMPUTER_BASED)

                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.contacts.register', [
                                                             'user_id'   => $user->id,
                                                             'course_id' => $course->id]) }}" class="btn btn-flat btn-info btn-s">
                                                    <span class="fa fa-plus"></span>
                                                </a>
                                            </td>
                                            <td>{{ $course->sku }}</td>
                                            <td>{{ ucfirst($course->title) }}</td>
                                            <td>--</td>
                                            <td>{{ $course->courseType->name }}</td>
                                            <td>
                                                @if ($course->courseCategories->count())
                                                    @foreach($course->courseCategories as $courseCategory)
                                                        <span class="label label-success">
                                                            {{ $courseCategory->category->name }}
                                                        </span>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td>{{ $course->price }}</td>
                                        </tr>

                                    @elseif ($course->classes)
                                        @foreach($course->classes as $class)

                                                @if ($now <= date('Y-m-d', strtotime($class->end_date)) or env('SHOW_FINISHED','0'))

                                                <tr>
                                                    <td>
                                                        <a href="{{ route('admin.contacts.register', [
                                                                 'user_id'   => $user->id,
                                                                 'course_id' => $course->id,
                                                                 'class_id'  => $class->id] ) }}" class="btn btn-flat btn-info btn-s">
                                                            <span class="fa fa-plus"></span>
                                                        </a>
                                                    </td>
                                                    <td>{{ $course->sku }}</td>
                                                    <td>{{ ucfirst($course->title) }}</td>
                                                    <td>
                                                        {{ date('F d, Y', strtotime($class->start_date)) }} -
                                                        {{ date('F d, Y', strtotime($class->end_date)) }}
                                                    </td>
                                                    <td>
                                                        <p>{{ $class->shortLocation }}</p>
                                                    </td>
                                                    <td>
                                                        @if ($course->courseCategories->count())
                                                            @foreach($course->courseCategories as $courseCategory)
                                                                <span class="label label-success">
                                                                    {{ $courseCategory->category->name }}
                                                                </span>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>{{ $course->price }}</td>
                                                </tr>

                                            @endif

                                        @endforeach
                                    @endif
                                @endforeach
                            </table>

                        @else
                            <i>No Courses.</i>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection