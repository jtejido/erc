@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>{{ ucfirst($course->title) }}</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.courses') }}">Courses</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="actions">
                            {!! Form::open(['method' => 'GET', 'class' => 'status-filter']) !!}
                            {!! Form::select('status', ['' => '-- All Status --'] + $statusTypes , $status, [
                                'id'    => 'status',
                                'class' => 'form-control',
                                'style' => 'width: 200px;'
                            ]) !!}
                            {!! Form::close() !!}

                            <a href="{{ route('admin.classes.create', [$course->id]) }}"
                               class="btn btn-flat btn-success pull-right">
                                <i class="fa fa-plus"></i> Add Class
                            </a>
                        </div>
                    </div>

                    <div class="box-body">
                        @if ($classes->count())

                            @include('admin.includes.form_errors')
                            @include('admin.includes.success_message')

                            <table id="table-classes" class="table table-bordered table-hover table-classes">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Class Dates</th>
                                        <th>Time</th>

                                        @if ($course->isSeminar())
                                            <th>Address</th>
                                        @endif

                                        <th>Instructor</th>
                                        <th>Action
                                            <span id="actions-tip" class="fa-stack fa-lg" title="For active classes only">
                                              <i class="fa fa-square-o fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                        </th>
                                    </tr>
                                </thead>

                                @foreach ($classes as $class)
                                    <tr>
                                        <td>{{ $class->id }}</td>
                                        <td>
                                            {{ $class->start_date->format('F d, Y') }} - {{ $class->end_date->format('F d, Y') }}
                                            <br>
                                            @if (!$class->active)
                                                <span class="label label-danger">Finished</span>
                                            @else
                                                @if ($class->is_ongoing)
                                                <span class="label label-warning">In Progress</span>
                                                @else
                                                <span class="label label-success">Not Started</span>
                                                @endif
                                            @endif
                                        </td>
                                        <td>{{ $class->start->format('g:ia') }} - {{ $class->end->format('g:ia') }}</td>

                                        @if ($course->isSeminar())
                                            <td>
                                                @if($class->location)
                                                    <p>
                                                        {{ $class->location->location_name }}<br/>
                                                        {{ $class->location->location }}
                                                    </p>
                                                @else
                                                    <p>--</p>
                                                @endif
                                            </td>
                                        @endif
                                        <td>
                                            @if ($class->instructor)
                                                {{ $class->instructorName() }}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td class="action">
                                            @if ($class->active)
                                                {{-- */ $hasAttendees = CourseUtility::hasAttendees($class->id); /* --}}
                                                @if ($hasAttendees && !$class->end->isPast())
                                                    @if (!$class->is_cancelled)
                                                        <button class="btn btn-flat btn-success status-change btn-block"
                                                                data-id="{{ $class->id }}"
                                                                data-href="{{ route('admin.class.cancel') }}">

                                                            Cancel Entire Class
                                                        </button>
                                                    @endif

                                                @endif

                                                @if ($class->is_cancelled)
                                                    <button class="btn btn-flat btn-info btn-block">
                                                        Entire Class Cancelled
                                                    </button>
                                                @else
                                                    <button class="btn btn-flat btn-block @if ($class->is_deactivated) btn-success @else btn-warning @endif status-change"
                                                            data-href="{{ route('admin.class.status') }}"
                                                            data-id="{{ $class->id }}">
                                                        @if ($class->is_deactivated)
                                                            Activate
                                                        @else
                                                            Deactivate
                                                        @endif
                                                    </button>
                                                @endif

                                            @endif

                                            @if (!$class->is_deactivated)
                                                <a class="btn btn-primary btn-flat btn-block"
                                                   href="{{ route('admin.classes.show', [ $class->id]) }}">Edit</a>
                                            @endif

                                            @inject('classService', 'App\Services\ClassService')

                                            @if($classService->isClassDeletable($class->id))
                                                <button class="btn btn-danger btn-flat delete btn-block"
                                                        data-href="{{ route('admin.class.delete') }}"
                                                        data-id="{{ $class->id }}"> Delete
                                                </button>
                                            @endif
                                            @if (!$class->is_deactivated)
                                                <a class="btn btn-info btn-flat btn-block"
                                                   href="{{ route('admin.certificates.show', [$class->id]) }}">Students</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </table>
                        @else
                            <i>No Classes.</i>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#actions-tip').tooltip();
        })
    </script>
@endsection

@section('stylesheets')
    @parent
    <style media="screen">
    #actions-tip {
        font-size: 10px;
    }
        .action .btn {
            margin-bottom: 3px;
        }
    </style>
@endsection
