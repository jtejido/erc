@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Courses</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.courses') }}">Courses</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                @include('admin.includes.success_message')

                @if (Session::has('searchkey'))
                    <input type="hidden" id="searchkey" value="{{ ucfirst(Session::get('searchkey')) }}" data-column="2">
                @endif

                @if ($courses->count() > 0)
                    <table id="table-courses" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>SKU</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        @foreach ($courses as $course)
                            <tr>
                                <td>
                                    @if ($course->course_type_id != \App\Utilities\Constant::COMPUTER_BASED)
                                        <a href="{{ route('admin.classes', ['id' => $course->id]) }}" class="btn btn-block btn-info btn-sm btn-flat" style="font-size: 12px; padding: 5px;">
                                            Classes
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $course->sku }}</td>
                                <td>
                                    {{ ucfirst($course->title) }}

                                </td>
                                <td>
                                    @if ($course->courseCategories->count())
                                        @foreach ($course->courseCategories as $courseCategory)
                                            <span class="label label-info">
                                                {{ $courseCategory->category->name }}
                                            </span>
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{ $course->courseType->name }}</td>
                                <td>
                                    @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                                    <a class="btn btn-block btn-primary btn-xs btn-flat"
                                       href="{{ route('admin.course.edit', ['id' => $course->id]) }}">
                                        Edit
                                    </a>
                                    <button class="btn btn-block btn-xs btn-flat @if ($course->is_deactivated) btn-success @else btn-warning @endif status-change" data-href="{{ route('admin.course.status') }}" data-id="{{ $course->id }}">
                                        @if ($course->is_deactivated)
                                            Activate
                                        @else
                                            Deactivate
                                        @endif
                                    </button>
                                        @inject('courseService', 'App\Services\CourseService')

                                        @if($courseService->isCourseDeletable($course->id))
                                        <button class="btn btn-block btn-danger btn-xs btn-flat delete" data-href="{{ route('admin.course.delete') }}" data-id="{{ $course->id }}">Delete</button>
                                        @endif
                                    @endif
                                        @if ($course->course_type_id == \App\Utilities\Constant::COMPUTER_BASED)
                                    <a class="btn btn-block btn-info btn-flat" href="{{ route('admin.certificates.showCourse', [$course->id]) }}">Students</a>
                                        @endif
                                </td>
                            </tr>
                        @endforeach
                    </table>

                @else
                    <i>No Courses.</i>
                @endif
            </div>
        </div>
    </section>

@include('includes.notification_modal')
@endsection
