<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="articleModalTitle">
        Order Active Promos
    </h4>
</div>

{!! Form::open([
    'method' => 'POST',
    'url' => route('promo.ordersave'),
    'id' => 'promo-form'
    ]) !!}
<div class="modal-body">

    {!! csrf_field() !!}
<div class="row">
<div class="col-md-5 col-md-offset-3">
    
                <table id="table-promo-active" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Link</th>
                        <th>Order</th>
                    </tr>
                    </thead>
                    @if ($promos->count() > 0)
                        @foreach ($promos as $promo)
                            <tr>
                                <td>
                                    <strong>{{ $promo->url }}</strong>
                                </td>
                                <td>
                                <input type="text" name="ids[{{ $promo->id }}]" class="form-control" value="{{ $promo->order }}">
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                <i>No Promos.</i>
                            </td>
                        </tr>
                    @endif
                </table>
    </div>
</div>

    <div style="text-align: right;">
        <button type="button"
                class="btn btn-flat btn-default"
                data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-flat btn-primary">Save</button>
    </div>
    
</div>

{!! Form::close() !!}