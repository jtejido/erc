<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="articleModalTitle">
        Save Article
    </h4>
</div>

{!! Form::open([
    'method' => 'POST',
    'url' => route('promo.save'),
    'id' => 'promo-form',
    'enctype' => 'multipart/form-data'
    ]) !!}
<div class="modal-body">

    {!! csrf_field() !!}

    @if(isset($promo))
        {!! Form::input('hidden', 'id', $promo->id) !!}
    @endif

    @if (isset($promo) and $promo->photo and $promo->photo_mime)
        {!! Form::label('current_photo', 'Current Photo: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-8">
            <?php $path = route('promo.photo', $promo->id); ?>
            <img src="{{ $path }}" alt="Featured Photo" style="width: 100%; height: 100%;">
        </div>
    @endif

    <div class="row">
    {!! Form::rLabel('photo', 'Photo: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="form-group col-md-8">
        {!! Form::input('file', 'photo', '', ['class' => 'photo', 'accept' => '.png,.jpg,.jpeg']) !!}
    </div>
    </div>
    <div class="row">
    {!! Form::rLabel('url', 'Link: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="form-group col-md-8">
        {!! Form::input('text', 'url', isset($promo) ? $promo->url : '' , [
        'class' => 'form-control',
        'id'    => 'author',
        'style' => 'display: inline;',
        'required' => 'required',
        'placeholder' => 'http://ercweb.com/23dw34'
        ]) !!}
    </div>
    </div>

    <div class="row">
    {!! Form::rLabel('active', 'Activate: ', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-md-8">
        {!! Form::checkbox('active', true, (isset($promo) and $promo->active) ? true : false) !!}
    </div>
    </div>
    <hr/>
    <div class="row">
    <div class="col-xs-12">
        <div class="alert alert-info">
            <strong>Reference:</strong>
            <ul>
                <li><a href="{{ url('courses') }}" target="_blank">Course List</a></li>
                <li><a href="{{ url('classes') }}" target="_blank">Class List By Location</a></li>
            </ul>
        </div>
    </div>


    </div>

    <div style="text-align: right;">
        <button type="button"
                class="btn btn-flat btn-default"
                data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-flat btn-primary">Save</button>
    </div>
</div>

{!! Form::close() !!}