@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Promos</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('promos.index') }}">Promos</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                <button class="btn btn-flat btn-primary" id="add">Add Promo</button>
                <button class="btn btn-flat btn-warning" id="activate">Activate</button>
                <button class="btn btn-flat btn-default" id="deactivate">Deactivate</button>
                <button class="btn btn-flat btn-danger" id="delete">Delete</button>
                <button class="btn btn-flat btn-primary" id="order">Order Active Promos</button>
            </div>
            <div class="box-body">

                @include('includes.success_message')

                <table id="table-promo" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Link</th>
                        <th>Order</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    @if ($promos->count() > 0)
                        @foreach ($promos as $promo)
                            <tr>
                                <td><input type="checkbox" name="promo" class="promo-checkbox" data-id="{{ $promo->id }}"></td>
                                <td>
                                    <strong>{{ $promo->url }}</strong>

                                    @if ($promo->active)
                                        <span class="label label-success">ACTIVE</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $promo->order }}
                                </td>
                                <td>
                                    <button class="btn btn-flat btn-primary edit" data-id="{{ $promo->id }}">Edit</button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                <i>No Promos.</i>
                            </td>
                        </tr>
                    @endif
                </table>

                @if ($promos->count() > 0)
                    <div class="pull-right">
                        {!! $promos->appends($page)->render() !!}
                    </div>
                @endif
            </div>
        </div>
    </section>

    @include('includes.notification_modal')

    <div class="modal fade" id="promoModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="classModalLabel">Save Promo</h4>
                </div>
                <div class="modal-body">
                    <i>Loading...</i>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascripts')
    @parent

    <script>
        (function($){
            $('#add, .edit').on('click', function() {
                var $modal = $('#promoModal'),
                        id = $(this).data('id') ? $(this).data('id') : 0;

                $.ajax({
                    type: 'POST',
                    url: "{{ route('promo.form') }}",
                    data: { id: id },
                    success: function(response) {
                        $modal.find('.modal-content').html(response);
                        $modal.modal('show');
                    }
                }).done(function () {
                    $modal.find('.discount').AllowDecimal();
                })
            });

            $('#order').on('click', function() {
                var $modal = $('#promoModal');

                $.ajax({
                    type: 'POST',
                    url: "{{ route('promo.order') }}",
                    success: function(response) {
                        $modal.find('.modal-content').html(response);
                        $modal.modal('show');
                    }
                })
            });

            $('#activate').on('click', function() {
                var $checkboxes = $('.promo-checkbox'),
                    ids = [];

                $checkboxes.each(function() {
                    if ($(this).prop('checked')) {
                        ids.push($(this).data('id'))
                    }
                });

                if (ids.length) {
                    Modal.showConfirmationModal(
                        'Are you sure you want to activate?',
                        'Restoration',
                        function() {

                            $.ajax({
                                type : 'POST',
                                url  : "{{ route('promo.activate')  }}",
                                data : { ids: ids },
                                success : function() {
                                    window.location.reload();
                                }
                            });
                        }
                    );
                }
            });

            $('#deactivate').on('click', function() {
                var $checkboxes = $('.promo-checkbox'),
                    ids = [];

                $checkboxes.each(function() {
                    if ($(this).prop('checked')) {
                        ids.push($(this).data('id'))
                    }
                });

                if (ids.length) {
                    Modal.showConfirmationModal(
                        'Are you sure you want to deactivate?',
                        'Restoration',
                        function() {

                            $.ajax({
                                type : 'POST',
                                url  : "{{ route('promo.deactivate')  }}",
                                data : { ids: ids },
                                success : function() {
                                    window.location.reload();
                                }
                            });
                        }
                    );
                }
            });

            $('#delete').on('click', function() {
                var $checkboxes = $('.promo-checkbox'),
                    ids = [];

                $checkboxes.each(function() {
                    if ($(this).prop('checked')) {
                        ids.push($(this).data('id'))
                    }
                });

                if (ids.length) {
                    Modal.showConfirmationModal(
                        'Are you sure you want to delete?',
                        'Restoration',
                        function() {

                            $.ajax({
                                type : 'POST',
                                url  : "{{ route('promo.delete')  }}",
                                data : { ids: ids },
                                success : function() {
                                    window.location.reload();
                                }
                            });
                        }
                    );
                }
            });

            $('.promo-checkbox').on('click', function() {
                var ctr = 0;

                $('.promo-checkbox').each(function() {
                    if ($(this).prop('checked')) {
                        ctr++;
                    }
                });

                if (ctr > 3) {
                    alert('Maximum of 3 promo codes can be selected.');
                    return false;
                }
            });
        })(jQuery);
    </script>
@endsection
