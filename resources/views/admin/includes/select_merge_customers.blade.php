<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="classModalLabel">
        Which user should all the training history and address book entries be moved to?
    </h4>
</div>

{!! Form::open([
    'method' => 'POST',
    'url' => route('customers.merge.post'),
    'id' => 'merge-customers'
    ]) !!}
<div class="modal-body">

    <div class="alert alert-danger" style="display: none"></div>

    @foreach($customers as $customer)

        {!! Form::label('', '', [
            'class' => 'control-label']) !!}

        <div class="form-group col-md-12">
            {!! Form::radio('customer_id', $customer->id, '', [
            'class'    => 'customer_id',
            ]) !!}

            <input type="hidden" name="contacts[]" value="{{ $customer->id }}">

            <span>
                {{ $customer->first_name }} {{ $customer->last_name }}
            </span>
            <p>Email : {{ $customer->user ? $customer->user->email : 'No email.' }}</p>
        </div>
    @endforeach

    <div style="text-align: right;">
        <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-flat btn-primary">Merge</button>
    </div>
</div>

{!! Form::close() !!}