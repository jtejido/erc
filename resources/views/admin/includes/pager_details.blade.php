@if (isset($pager))
    <?php
    $start = ($pager->currentPage() - 1) * $pager->perPage() + 1;
    $total = $pager->total();

    if ($pager->currentPage() == $pager->lastPage()) {
        $end = $total;
    } else {
        $end = $start + $pager->perPage() - 1;
    }
    ?>

    <div class="pager-details">Result {{ $start }} - {{ $end }} of {{ $total }}</div>
@endif