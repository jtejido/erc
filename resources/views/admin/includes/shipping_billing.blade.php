<hr/>
<div class="shipping_billing row">

            <div class="col col-xs-12 col-sm-6">
                <div class="billing_container">
                    <label><strong>Billing Address</strong></label>
                    <div class="panel panel-address panel-option panel-selected">
                        <div class="panel-body">
                            <a href="{{ route('admin.profile.edit', [ $user->contact->id ]) }}" class="edit" title="Edit" style="color: #333"><i class="fa fa-pencil pull-right" aria-hidden="true"></i></a>
                            <i class="fa fa-check-circle pull-left" aria-hidden="true" style="line-height: 3.6em; color:green;"></i>
                            {!! $user->contact->complete_billing_address  !!}
                            <input type="hidden" name="billing_company_default" value="{{ $user->contact->company }}" />
                            <input type="hidden" name="billing_address_default" value="{{ $user->contact->user_billing_address }}" />
                            <input type="hidden" name="billing_state_default" value="{{ $user->contact->user_billing_state }}" />
                            <input type="hidden" name="billing_city_default" value="{{ $user->contact->user_billing_city }}" />
                            <input type="hidden" name="billing_zip_default" value="{{ $user->contact->user_billing_zip }}" />
                        </div>
                    </div>
                    <div class="panel panel-option panel-form-button">
                        <div class="panel-body">
                            <strong><i class="fa fa-plus-circle" aria-hidden="true"></i> Use another address</strong>
                        </div>
                    </div>
                    <div class="panel panel-form-address" style="display: none">
                        <input type="hidden" name="billing_custom" class="custom_flag" value="0"/>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="billing_company">Company/Name:</label>
                                <input name="billing_company" type="text" class="form-control" id="billing_company">
                            </div>
                            <div class="form-group">
                                <label for="billing_address1">Address:</label>
                                <input name="billing_address1" type="text" class="form-control" id="billing_address1">
                            </div>
                            <div class="form-group">
                                <label for="billing_city">City:</label>
                                <input name="billing_city" type="text" class="form-control" id="billing_city">
                            </div>
                            <div class="form-group">
                                <label for="billing_state">State:</label>
                                {!! Form::select('billing_state', $states, '', [
                                        'class' => 'form-control'
                                    ]) !!}
                            </div>
                            <div class="form-group">
                                <label for="billing_zip">Zip:</label>
                                <input name="billing_zip" type="number" class="form-control" id="billing_zip" maxlength="5" size="5" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col col-xs-12 col-sm-6">
                <div class="shipment_container">
                    <label><strong>Shipping Address</strong></label>
                    <div class="panel panel-address panel-option panel-selected">
                        <div class="panel-body">
                            <input type="hidden" name="po_shipping_zip" value="{{ $user->contact->zip }}"/>
                            <input type="hidden" name="po_shipping_state" value="{{ $user->contact->user_shipping_state }}"/>
                            <input type="hidden" name="tax_rate" value="0"/>
                            <input type="hidden" name="shipping_rates" value=""/>
                            <a href="{{ route('admin.profile.edit', [ $user->contact->id ]) }}" class="edit" title="Edit" style="color: #333"><i class="fa fa-pencil pull-right" aria-hidden="true"></i></a>
                            <i class="fa fa-check-circle pull-left" aria-hidden="true" style="line-height: 3.6em; color:green;"></i>
                            {!! $user->contact->complete_shipping_address  !!}
                            <div style="display: block; margin-top: 5px; display: none">
                                {!! Form::select('shipment_county_default', $counties, '', [
                                    'class' => 'form-control',
                                ]) !!}
                            </div>
                            <input type="hidden" name="shipment_company_default" value="{{ $user->contact->company }}" />
                            <input type="hidden" name="shipment_address_default" value="{{ $user->contact->user_shipping_address }}" />
                            <input type="hidden" name="shipment_state_default" value="{{ $user->contact->user_shipping_state }}" />
                            <input type="hidden" name="shipment_city_default" value="{{ $user->contact->user_shipping_city }}" />
                            <input type="hidden" name="shipment_zip_default" value="{{ $user->contact->user_shipping_zip }}" />
                        </div>
                    </div>
                    <div class="panel panel-option panel-form-button">
                        <div class="panel-body">
                            <strong><i class="fa fa-plus-circle" aria-hidden="true"></i> Use another address</strong>
                        </div>
                    </div>
                    <div class="panel panel-form-address" style="display: none">
                        <input type="hidden" name="shipping_custom" class="custom_flag" value="0"/>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="shipment_company">Company/Name:</label>
                                <input name="shipment_company" type="text" class="form-control" id="shipment_company">
                            </div>
                            <div class="form-group">
                                <label for="shipment_address1">Address:</label>
                                <input name="shipment_address1" type="text" class="form-control" id="shipment_address1">
                            </div>
                            <div class="form-group">
                                <label for="shipment_city">City:</label>
                                <input name="shipment_city" type="text" class="form-control" id="shipment_city">
                            </div>
                            <div class="form-group">
                                <label for="shipment_state">State:</label>
                                {!! Form::select('shipment_state', $states, '', [
                                    'class' => 'form-control'
                                ]) !!}
                            </div>
                            <div class="form-group" style="display: none">
                                <label for="shipment_county">County:</label>
                                {!! Form::select('shipment_county', $counties, '', [
                                    'class' => 'form-control',
                                ]) !!}
                            </div>
                            <div class="form-group">
                                <label for="shipment_zip">Zip:</label>
                                <input name="shipment_zip" type="number" class="form-control" id="shipment_zip" maxlength="5" size="5" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

<style>
    .shipping_billing .panel {
        border: none;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .shipping_billing .panel-address .panel-body {
        line-height: 1.2em;
    }
    .shipping_billing .panel:hover {
        background: #f8f8f8;
    }
    .shipping_billing .panel-option {
        cursor: pointer;
        border: none;
    }
    .shipping_billing .panel-selected {
        border: 1px solid #9ca643 !important;
        box-shadow: 0 2px 4px 0 rgba(0,0,0,.09);
    }
</style>

