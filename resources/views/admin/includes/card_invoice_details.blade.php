<div class="col-md-7">

    <div class="panel panel-default credit-card-box">
        <div class="panel-heading display-table" >
            <div class="row display-tr" >
                <h3 class="panel-title display-td" >Payment Options</h3>
            </div>
        </div>
        <div class="panel-body">
            <select name="payment-option" class="form-control">
                <option value="card">Credit / Debit Card</option>
                <option value="invoice">Purchase Order</option>
            </select>
        </div>
    </div>

    <div class="panel panel-default credit-card-box" id="card-payment-form">
        <div class="panel-heading display-table" >
            <div class="row display-tr" >
                <h3 class="panel-title display-td" >Card Payment Details</h3>
                <div class="display-td">
                    <img class="img-responsive" src="/img/accepted_cc.png">
                </div>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open([
                'route' => ['admin.card_checkout', 'user_id' => $user->id]])
            !!}

            @if (isset($order))
                {!! Form::input('hidden', 'order_id', $order->id) !!}
            @endif

            @if (isset($total))
                {!! Form::input('hidden', 'amount', $total) !!}
                {!! Form::input('hidden', 'shipping_fee', 0, [ 'class' => 'shipping_fee' ]) !!}
                {!! Form::input('hidden', 'state_tax', 0, [ 'class' => 'state_tax' ]) !!}
                {!! Form::input('hidden', 'current_total', $total) !!}
                {!! Form::input('hidden', 'user_id', $user->id) !!}
                {!! Form::input('hidden', 'description', 'Payment of Order') !!}
            @endif

            @if (isset($items))
                @foreach ($items as $item)
                    <input type="hidden" name="items[]" value="{{ $item->id }}" />
                @endforeach
            @endif

            <div class="row">
                <div class="col-xs-4 col-md-3">
                    <div class="form-group">
                        {!! Form::rLabel('first_name', 'First Name') !!}
                        {!! Form::input(
                            'text',
                            'first_name',
                            $user->contact->first_name, [
                                'placeholder' => 'John',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
                <div class="col-xs-4 col-md-3">
                    <div class="form-group">
                        {!! Form::rLabel('last_name', 'Last Name') !!}
                        {!! Form::input(
                            'text',
                            'last_name',
                            $user->contact->last_name, [
                                'placeholder' => 'Doe',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
                <div class="col-xs-4 col-md-6">
                    <div class="form-group">
                        {!! Form::rLabel('email', 'Email') !!}
                        {!! Form::input(
                            'text',
                            'email',
                            $user->email, [
                                'placeholder' => 'john@example.com',
                                'class' => 'form-control',
                                'required' => 'required'
                            ]
                        ) !!}
                    </div>
                </div>
            </div>

            <hr>

            <div class="row card-info">
                <div class="col-xs-7 col-md-7">
                    <div class="form-group">
                        {!! Form::rLabel('card_number', 'Credit Card Number') !!}
                        <div class="input-group">
                            {!! Form::input(
                                'text',
                                'card_number',
                                '', [
                                    'placeholder' => 'Credit Card Number',
                                    'class' => 'form-control numeric-only',
                                    'required' => 'required'
                                 ]
                            ) !!}
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-3 col-md-5">
                    <div class="form-group">
                        {!! Form::rLabel('cvv', 'CVV') !!}
                        {!! Form::input(
                            'text',
                            'cvv',
                            '', [
                                'placeholder' => 'Security',
                                'class' => 'form-control numeric-only',
                                'required' => 'required'
                             ]
                        ) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-7 col-md-7">
                    <div class="form-group">
                        {!! Form::rLabel('exp_month', 'Expiration Month') !!}
                        {!! Form::select('exp_month', $months, '', [
                            'class' => 'form-control',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>

                <div class="col-xs-3 col-md-5">
                    <div class="form-group">
                        {!! Form::rLabel('exp_year', 'Expiration Year') !!}
                        {!! Form::select('exp_year', $years, '', [
                            'class' => 'form-control',
                            'required' => 'required'
                        ]) !!}
                    </div>
                </div>
            </div>

            @include('admin.includes.shipping_billing')

            <div class="row">
                <div class="col-xs-12">
                    <p class="text-center">
                        <input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                        <label for="doNotEmail">Do Not Email Agent And Attendees</label>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-flat btn-warning btn-lg btn-block btn-pay"
                            type="submit"
                            @if (!isset($order)) disabled @endif>
                        Place Order</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="panel panel-default credit-card-box hidden" id="invoice-payment-form">
        <div class="panel-heading display-table" >
            <div class="row display-tr" >
                <h3 class="panel-title display-td" >Purchase Order Payment Details</h3>
            </div>
        </div>
        <div class="panel-body">
            {!! Form::open([
                'route' => ['admin.invoice_checkout', 'user_id' => $user->id],
                'enctype' => 'multipart/form-data'])
            !!}

            @if (isset($order))
                {!! Form::input('hidden', 'order_id', $order->id) !!}
            @endif

            @if (isset($total))
                {!! Form::input('hidden', 'amount', $total) !!}
                {!! Form::input('hidden', 'shipping_fee', 0, [ 'class' => 'shipping_fee' ]) !!}
                {!! Form::input('hidden', 'state_tax', 0, [ 'class' => 'state_tax' ]) !!}
                {!! Form::input('hidden', 'current_total', $total) !!}
                {!! Form::input('hidden', 'user_id', $user->id) !!}
                {!! Form::input('hidden', 'description', 'Payment of Order') !!}
                {!! Form::input('hidden', 'validate_url', route('ajax.file.validate'), ['id' => 'path-to-validate-file']) !!}
            @endif

            @if (isset($items))
                @foreach ($items as $item)
                    <input type="hidden" name="items[]" value="{{ $item->id }}" />
                @endforeach
            @endif

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('po_file', 'Purchase Order File') !!}
                        <button class="btn btn-flat btn-success btn-md btn-block btn-upload">
                            Select Purchase Order
                        </button>

                        {!! Form::file('po_file', [
                            'accept'    => 'application/pdf, image/*',
                            'id'        => 'po_file',
                            'class'     => 'upload-file hidden'])
                        !!}
                    </div>
                </div>

                <div class="col-md-6">
                    {!! HTML::image('img/placeholder-image.jpg', 'Purchase Order Image/File',
                    ['class' => 'po_file payment-image img-responsive center-block']) !!}
                </div>
            </div>

            <div class="row invoice-details">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('po_number', 'Purchase Order Number') !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::input(
                        'text',
                        'po_number',
                        '', [
                        'placeholder' => 'Purchase Order Number',
                        'class' => 'form-control invoice-text center-block'
                        ]
                    ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <hr>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('check_file', 'Check Image or File') !!}
                        <button class="btn btn-flat btn-success btn-md btn-block btn-upload">
                            Select Check
                        </button>
                        {!! Form::file('check_file', [
                            'accept'   => 'application/pdf, image/*',
                            'id'       => 'check_file',
                            'class'    => 'upload-file hidden'])
                        !!}
                    </div>
                </div>

                <div class="col-md-6">
                    {!! HTML::image('img/placeholder-image.jpg', 'Check Image/File',
                    ['class' => 'check_file payment-image img-responsive center-block']) !!}
                </div>
            </div>

            <div class="row invoice-details">
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('check_number', 'Check Number') !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::input(
                            'text',
                            'check_number',
                            '', [
                            'placeholder' => 'Check Number',
                            'class' => 'form-control invoice-text center-block'
                            ]
                        ) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('other_recipients', 'Other Email Recipients') !!}
                        {!! Form::input(
                            'text',
                            'other_recipients',
                            '', [
                            'placeholder' => 'Other Email Recipients',
                            'class' => 'form-control ei btn-flat'
                            ]
                        ) !!}
                    </div>
                </div>
            </div>

            @include('admin.includes.shipping_billing')

            <div class="row">
                <div class="col-xs-12">
                    <p class="text-center">
                        <input type="checkbox" name="doNotEmailInvoice" id="doNotEmailInvoice" />&nbsp;
                        <label for="doNotEmailInvoice">Do Not Email Agent And Attendees</label>
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-flat btn-warning btn-lg btn-block btn-pay"
                            type="submit"
                            @if (!isset($order)) disabled @endif>
                        Place Order</button>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>