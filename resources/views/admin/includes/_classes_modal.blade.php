<div class="modal fade" id="classesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Hazardous Waste Management: The Complete Course (RCRA)</h4>
            </div>
            <div class="modal-body">
                <table class="table" id="classList">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>City/State</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>Nov 1, 2017</td>
                            <td>Webcast</td>
                            <td><a href="#"
                                   data-type="CLASS"
                                   class="selectClass">Select</a></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>