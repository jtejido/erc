@if (Session::has('error_message'))
    <div class="box-container col-md-12">
        <div class="alert alert-danger alert-dismissible box box-success box-solid" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('error_message') }}
        </div>
    </div>
@endif