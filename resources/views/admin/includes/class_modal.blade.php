<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="classModalLabel">Save Class</h4>
</div>

{!! Form::open(['method' => 'POST', 'url' => route('admin.class.add'), 'id' => 'classForm']) !!}
    {!! Form::hidden('location_id', 1) !!}
    <div class="modal-body">
        @if (isset($class))
            {!! Form::input('hidden', 'id', $class->id) !!}
        @endif

        {!! Form::input('hidden', 'course_id', $course->id) !!}
        {!! Form::input('hidden', 'course_type', $course->courseType->id) !!}

        {!! Form::rLabel('start_date', 'Start Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-9 input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::input('text', 'start_date', isset($class) ? date('F d, Y', strtotime($class->start_date)) : '', [
                'class' => 'form-control',
                'id'    => 'startDate',
                'style' => 'display: inline;',
                'placeholder' => 'MM dd, yyyy',
                'required' => 'required',
            ]) !!}
        </div>

        {!! Form::rLabel('start_time', 'Start Time: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="bootstrap-timepicker form-group col-md-9 input-group">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
            {!! Form::input('text', 'start_time', isset($class) ? $class->start_time : '', [
                'class' => 'form-control',
                'id'    => 'startTime',
                'style' => 'display: inline;',
                'required' => 'required'
            ]) !!}
        </div>

        {!! Form::rLabel('end_date', 'End Date: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-9 input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            {!! Form::input('text', 'end_date', isset($class) ? date('F d, Y', strtotime($class->end_date)) : '', [
                'class' => 'form-control',
                'id'    => 'endDate',
                'style' => 'display: inline;',
                'placeholder' => 'MM dd, yyyy',
                'required' => 'required'
            ]) !!}
        </div>

        {!! Form::rLabel('end_time', 'End Time: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="bootstrap-timepicker form-group col-md-9 input-group">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
            {!! Form::input('text', 'end_time', isset($class) ? $class->end_time : '', [
                'class' => 'form-control',
                'id'    => 'endTime',
                'style' => 'display: inline;',
                'required' => 'required'
            ]) !!}
        </div>

        {!! Form::label('instructor_name', 'Instructor: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-9 input-group">
            <select name="instructor_id" class="form-control col-md-9 instructor">
                <option value="">-- Select Instructor --</option>
                @foreach ($instructors as $instructor)
                    <option value="{{ $instructor->id }}"
                            @if (isset($class) && $class->instructor && $class->instructor->id == $instructor->id) selected @endif>
                        {{ $instructor->name }}
                    </option>
                @endforeach
            </select>
        </div>

        @if ($isSeminar)

                {!! Form::label('location', 'Location: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="form-group col-md-9 input-group">
                    <select name="location_id" class="form-control col-md-9 location">
                        <option value="">-- Select Location --</option>
                        @foreach ($locations as $location)
                            <option value="{{ $location->id }}"
                                    @if (isset($class) && $class->location && $class->location->id == $location->id) selected @endif>
                                {{ $location->location }}
                            </option>
                        @endforeach
                    </select>
                </div>
            
        @endif

        {!! Form::label('website', 'Venue Info (link): ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-9 input-group">
            {!! Form::input('text', 'website', isset($class) ? $class->website : '', [
                'class' => 'form-control',
            ]) !!}
        </div>

        {!! Form::label('instructions', 'Instructions: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="form-group col-md-9 input-group">
            {!! Form::textarea('instructions',  isset($class) ? $class->instructions : '', [
                'class' => 'form-control col-md-9',
                'style' => 'display: inline;'
            ]) !!}
        </div>

        <div style="text-align: right;">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-flat">Save</button>
        </div>
    </div>

{!! Form::close() !!}