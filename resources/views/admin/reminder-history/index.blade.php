@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Reminder History</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Reminder History</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                    </div>

                    <div class="box-body">


                        <table id="table-classes-cert" class="table table-bordered table-hover table-classes">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>To</th>
                                <th>Subject</th>
                                <th>Course / Class Info</th>
                                <th width="1%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reminders as $reminder)
                                <tr>
                                    <td>{{ $reminder->updated_at->toDateString() }}</td>
                                    <td>{{ $reminder->username }}</td>
                                    <td>{{ $reminder->subject }}</td>
                                    <td>
                                        @if($reminder->isCourse())
                                        @else
                                            <strong>{{ $reminder->remindable->title }}</strong><br/>
                                            <em>{{ $reminder->remindable->titleLocation }}</em>
                                        @endif
                                    </td>
                                    <td>
                                        {!! Form::open([
                                            'url'       => route('admin.reminder-history.resend', [$reminder->id]),
                                            'method'    => 'post'
                                        ]) !!}

                                            <button type="button"
                                               class="btn btn-primary btn-flat btn-block submitButton">Resend</button>

                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>


    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#table-classes-cert').on('click', '.submitButton', function() {
                var $that = $(this);
                Modal.showConfirmationModal(
                        'Are you sure you want to resend this reminder?',
                        'Reminder',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
                return false;
            });
            $('#table-classes-cert').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 2 }
                ],
                "order": [[ 0, "desc" ]]
            });
        });
    </script>
@endsection

