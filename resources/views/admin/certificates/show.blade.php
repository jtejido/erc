@extends('admin.layout')
@section('stylesheets')
    @parent
    <style media="screen">
        .label { font-weight: normal }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Class Attendees</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.certificates.index') }}">Certificate Management</a></li>
            <li>Class - Attendees</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @include('admin.includes.form_errors')
                        @include('admin.certificates._info')
                    </div>

                    <div class="box-body">
                        @inject('classRegService', 'App\Services\ClassRegistrationService')
                        @if ($students->count())

                            <table id="table-classes-cert-students"
                                   class="table table-striped table-hover table-classes">
                                <thead>
                                <tr>
                                    <th width="20%">Name</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Certificate
                                        <i class="fa fa-file-pdf-o"
                                           data-toggle="tooltip" data-placement="top"
                                           aria-hidden="true"
                                           title="Certificate Actions"></i></th>
                                    <th width="10%">Notification
                                        <i class="fa fa-bell-o"
                                           data-toggle="tooltip" data-placement="top"
                                           aria-hidden="true"
                                           title="Notification Actions"></i></th>
                                    <th>Attendance
                                        <i class="fa fa-calendar-check-o"
                                           data-toggle="tooltip" data-placement="top"
                                           aria-hidden="true"
                                           title="Attendance Actions"></i></th>
                                </tr>
                                </thead>
                                <tbody>
                                {{--*/ $count = 0 /*--}}
                                @foreach ($students as $student)
                                    {{--*/ $attended = $classRegService->getAttendanceStatusOfAttendeeFlag($student->regOrderId, $student->id) /*--}}
                                    {{--*/ $meeting_flag = $classRegService->getAttendanceMeetingStatusOfAttendeeFlag($student->regOrderId, $student->id) /*--}}
                                    {{--*/ $exam_flag = $classRegService->getAttendanceExamStatusOfAttendeeFlag($student->regOrderId, $student->id) /*--}}
                                    <tr>
                                        <td>{{ $student->name }}</td>
                                        <td>
                                            @if (!$course->isCBT())
                                                {!! $classRegService->getAttendanceStatusOfAttendee($student->regOrderId, $student->id) !!}
                                            @else
                                                <em>--</em>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($course->isCBT())
                                                Your certificate is generated in
                                                <a href="{{ env('CM_SITE') }}" target="_blank" style="font-size: 14px;padding: 0;">CourseMill.</a>
                                            @else
                                                @include('admin.certificates._cert-action')
                                            @endif
                                        </td>


                                        <td>
                                            @if($course->isWebcast() && $student->user)
                                                {!! Form::open([ 'url' => route('reminders.webcast'), 'class'=>'form-inline']) !!}
                                                {!! Form::hidden('classId', $class->id) !!}
                                                {!! Form::hidden('userId', $student->user->id) !!}
                                                <button type="submit"
                                                        title="Send Reminder Instruction"
                                                        data-msg="Are you sure you want to email the class instruction to this user?"
                                                        class="btn btn-flat btn-default submitButton">
                                                    Email Instruction
                                                </button>
                                                {!! Form::close() !!}
                                            @else
                                                --
                                            @endif

                                        </td>


                                        <td>
                                            @if (!$course->isCBT())
                                                @include($classRegService->getActionView($class ? $class->course->typeId : $course->typeId))
                                            @else
                                                <em>--</em>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2"></td>
                                    <td>
                                        @if (!$course->isCBT())
                                            @if($class)
                                                <a class='btn btn-flat btn-primary btn-block'
                                                    href="{{ route('download.certs', [$class->id]) }}">Download All</a>
                                                @else
                                                <a class='btn btn-flat btn-primary btn-block'
                                                   href="{{ route('download.course.certs', [$course->id]) }}">Download All</a>
                                            @endif
                                        @endif
                                    </td>
                                    <td></td>
                                    <td>
                                        @if (!$course->isCBT())
                                            @include($classRegService->getActionView($class ? $class->course->typeId : $course->typeId, 0))
                                        @endif
                                    </td>
                                </tr>

                                </tfoot>
                            </table>

                            @if($course->isSeminar)
                                <hr/>
                                        <span>Class Material</span>

                                        <a class='btn btn-flat btn-primary'
                                           href="{{ route('download.roster', [$class->id]) }}">Class Roster</a>
                                        <a class='btn btn-flat btn-primary'
                                           href="{{ route('download.nameplate', [$class->id]) }}">Name Plates</a>
                                        <a class='btn btn-flat btn-primary'
                                           href="{{ route('download.signsheet', [$class->id]) }}">Sign-in Sheet</a>
                                        <a class='btn btn-flat btn-info'
                                           href="{{ route('download.all', [$class->id]) }}">Download all in Zip File</a>


                            @endif

                        @else
                            <i>No Attendees.</i>
                        @endif

                    </div>
                </div>
            </div>
        </div>


    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {


            $('.submitButton').on('click', function() {
                var     $that = $(this),
                        msg = $that.data('msg');
                Modal.showConfirmationModal(
                        msg,
                        'Class Completion',
                        function() {
                            $('#notificationModal').find('.btn').attr('disabled', 'disabled');
                            $('#notificationModal').find('.notification-ok').text('Loading...');
                            $that.closest('form').find("input[name='status']").val($that.data('flag'));
                            $that.closest('form').find("input[name='key']").val($that.data('key'));
                            $that.closest('form').find("input[name='flag']").val($that.data('flag'));
                            $that.closest('form').submit();
                            return false;
                        });
                return false;
            });

            $('.btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                }
            });


            var clipboard = new Clipboard('.copy-btn');
            $('.copy-btn, .go-btn').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                }
            });
            clipboard.on('success', function(e) {
                $('.copy-btn').attr('title', 'Copy');
                $(e.trigger).attr('title', 'Copied!');
            });

        });
    </script>
@endsection
