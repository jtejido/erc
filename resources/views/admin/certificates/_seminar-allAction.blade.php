@if($classRegService->getAttendanceWithFlag($class->id, 0, 'attended') != 0)
<form action="{{route('statusAll.cert')}}"
      data-student-id="{{ $student->id }}"
      data-student-order="{{ $student->regOrderId }}"
      data-student-status="{{ $attended }}"
      id="generateCertForm"
      class="generateCertForm"
      method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="class_id" value="{{(!$class) ? 0 : $class->id}}"/>
    <input type="hidden" name="status" value="" />
    <input type="hidden" name="type" value="{{ \App\Utilities\Constant::SEMINAR }}" />
    <input type="hidden" name="key" value="" />
    <input type="hidden" name="flag" value="" />
    <input type="hidden" name="course_id" value="{{ $course->id }}" />
    <button type="button"
            data-class="{{ (!$class) ? 0 : $class->id }}"
            data-course="{{ $course->id }}"
            data-msg="Are you sure you want to mark all other attendee(s) as PRESENT?"
            data-flag="1"
            data-key="attended"
            class="submitButton btn btn-flat btn-success">
        Mark all TBC as Present</button>
    <button type="button"
            data-class="{{ (!$class) ? 0 : $class->id }}"
            data-course="{{ $course->id }}"
            data-msg="Are you sure you want to mark all other attendee(s) as ABSENT?"
            data-flag="2"
            data-key="attended"
            class="submitButton btn btn-flat btn-danger">
        Mark all TBC as Absent</button>

</form>
    @endif