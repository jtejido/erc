<form action="{{route('statusAll.cert')}}"
      data-student-id="{{ $student->id }}"
      data-student-order="{{ $student->regOrderId }}"
      data-student-status="{{ $attended }}"
      id="generateCertForm"
      class="generateCertForm"
      method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="class_id" value="{{$class->id}}"/>
    <input type="hidden" name="status" value="" />
    <input type="hidden" name="type" value="{{ \App\Utilities\Constant::SEMINAR }}" />
    <input type="hidden" name="key" value="" />
    <input type="hidden" name="flag" value="" />

<dl class="dl-horizontal webcast">
    @if($classRegService->getAttendanceWithFlag($class->id, 0, 'meeting_flag') != 0)
    <dt>Webcast</dt>
    <dd>
        <button type="button"
                data-class="{{ (!$class) ? 0 : $class->id }}"
                data-course="{{ $course->id }}"
                data-msg="Are you sure you want to mark all other attendee(s) as PRESENT?"
                data-flag="1"
                data-key="meeting_flag"
                class="submitButton btn btn-flat btn-success">
            Mark unconfirmed as Watched</button>
        <button type="button"
                data-class="{{ (!$class) ? 0 : $class->id }}"
                data-course="{{ $course->id }}"
                data-msg="Are you sure you want to mark all other attendee(s) as ABSENT?"
                data-flag="2"
                data-key="meeting_flag"
                class="submitButton btn btn-flat btn-danger">
            Mark unconfirmed as Absent</button>

    </dd>
    @endif
    @if($classRegService->getAttendanceWithFlag($class->id, 0, 'exam_flag') != 0)
    <dt>Exam</dt>
    <dd>
        <button type="button"
                data-class="{{ (!$class) ? 0 : $class->id }}"
                data-course="{{ $course->id }}"
                data-msg="Are you sure you want to mark all other attendee(s) as PRESENT?"
                data-flag="1"
                data-key="exam_flag"
                class="submitButton btn btn-flat btn-success">
            Mark unconfirmed as Taken</button>
        <button type="button"
                data-class="{{ (!$class) ? 0 : $class->id }}"
                data-course="{{ $course->id }}"
                data-msg="Are you sure you want to mark all other attendee(s) as ABSENT?"
                data-flag="2"
                data-key="exam_flag"
                class="submitButton btn btn-flat btn-danger">
            Mark unconfirmed as Not Taken</button>
    </dd>
        @endif
</dl>
</form>