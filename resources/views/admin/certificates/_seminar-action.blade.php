<form action="{{route('status.cert')}}"
      data-student-id="{{ $student->id }}"
      data-student-order="{{ $student->regOrderId }}"
      data-student-status="{{ $attended }}"
      id="generateCertForm"
      class="generateCertForm"
      method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="user_id" value="{{$student->id}}" />
    <input type="hidden" name="class_id" value="{{$student->regOrderId}}"/>
    <input type="hidden" name="status" value="{{ $attended }}" />
    <input type="hidden" name="type" value="{{ \App\Utilities\Constant::SEMINAR }}" />
    <input type="hidden" name="key" value="" />
    <div class="seminar action">
    @if($attended != 0)
        {{--*/ $count++ /*--}}
        @if($attended == '2')
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as PRESENT?"
                    data-flag="1"
                    data-key="attended"
                    class="btn btn-flat btn-success submitButton">Change to Present</button>
        @else
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as ABSENT?"
                    data-flag="2"
                    data-key="attended"
                    class="btn btn-flat btn-danger submitButton">Change to Absent</button>
        @endif
    @else
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as PRESENT?"
                data-flag="1"
                data-key="attended"
                class="btn btn-flat btn-success submitButton">Mark as Present</button>
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as ABSENT?"
                data-flag="2"
                data-key="attended"
                class="btn btn-flat btn-danger submitButton">Mark as Absent</button>
    @endif
    </div>
</form>