@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Class Completions</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Class Completions</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <label>
                            <input type="checkbox" id="showOnlyUpcoming" checked="checked" /> Only Show Upcoming Classes
                        </label>
                    </div>
                    <div class="box-body">
                        <table id="table-classes-cert" class="table table-striped table-classes">
                            <thead>
                            <tr>
                                <th>SKU</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Address / Type</th>
                                <th>No. of Participants</th>
                                <th>To Be Confirmed</th>
                                <th>Action
                                    <span class="fa-stack fa-lg tooltip-sm"
                                          data-toggle="tooltip" data-placement="top"
                                          title="For classes with students only.">
                                              <i class="fa fa-square-o fa-stack-2x"></i>
                                              <i class="fa fa-question fa-stack-1x"></i>
                                            </span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {

            $table = $('#table-classes-cert').DataTable({
                "columnDefs": [
                    { "searchable": true,
                        "orderable": true,
                        "targets": [0],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [1],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [2],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": false,
                        "targets": [3],
                        "sortable": false
                    },
                    { "searchable": false,
                        "orderable": false,
                        "targets": [4],
                        "sortable": false
                    },
                    { "searchable": false,
                        "orderable": false,
                        "targets": [5],
                        "sortable": false
                    },
                    { "searchable": false,
                        "orderable": false,
                        "targets": [6],
                        "sortable": false
                    }

                ],
                //"order": [[ 3, "desc" ]],
                "columns": [
                    { "data": "course.sku" },
                    { "data": "course.title" },
                    {
                        "data": "dateLabel",
                    },
                    { "data": "addressType" },
                    { "data": "TotalParticipants"},
                    { "data": "ToBeConfirmed"},
                    { "data": "action" }
                ],
                "processing":true,
                "serverSide":true,
                "stateSave":true,
                "ajax":{
                    url: window.location.origin + '/admin/certificates/api',
                    data: function ( d ) {
                        d.showOnlyUpcoming = ($('#showOnlyUpcoming').is(':checked')) ? 1 : 0;
                    },
                    type: "get",
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    error: function(){
                        $(".table-classes-cert-error").html("");
                        $("#table-classes-cert").append('<tbody class="table-classes-cert-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                        $("#table-classes-cert-grid_processing").css("display","none");
                    }
                }
            });

            $('#showOnlyUpcoming').change(function () {
                $table.ajax.reload();
                console.log($(this).is(':checked'));
            });
        })
    </script>
@endsection

