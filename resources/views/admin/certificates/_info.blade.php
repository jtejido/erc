@if (!$class)
    <h3 style="padding-left: 15px">
        {{ $course->title }}
        <small class="pull-right">
            <a href="{{route('admin.course.edit', [$course->id])}}"
               class="btn btn-xs btn-primary btn-flat">View Course</a>
        </small>
        {{--&nbsp;<small class="pull-right">( Edit {!! $course->getLinkedCourseTitleAdmin('course') !!} | {!! $class->getLinkedClassAdmin('class') !!} )</small>--}}
    </h3>
    <hr/>
    <dl class="dl-horizontal">
        <dt>SKU</dt>
        <dd>{{ $course->sku }}</dd>
        <dt>Category</dt>
        <dd>
            @if ($course->courseCategories->count())
                @foreach ($course->courseCategories as $courseCategory)
                    {{ $courseCategory->category->name }} <br/>
                @endforeach
            @endif
        </dd>
        <dt>Type</dt>
        <dd>{{ $course->courseType->name }}</dd>
    </dl>
@else
    <h3 style="padding-left: 15px">
        {{ $course->title }}
        <small></small>
        <small class="pull-right">
            <a href="{{route('admin.classes.show', [$class->id])}}"
               class="btn btn-xs btn-primary btn-flat">Edit Class</a>
            <a href="{{route('admin.course.edit', [$course->id])}}"
               class="btn btn-xs btn-default btn-flat">View Course</a>
        </small>
        {{--&nbsp;<small class="pull-right">( Edit {!! $course->getLinkedCourseTitleAdmin('course') !!} | {!! $class->getLinkedClassAdmin('class') !!} )</small>--}}
    </h3>
    <hr/>
    <dl class="dl-horizontal">
        <dt>SKU</dt>
        <dd>{{ $course->sku }}</dd>
        <dt>Category</dt>
        <dd>
            @if ($course->courseCategories->count())
                @foreach ($course->courseCategories as $courseCategory)
                    {{ $courseCategory->category->name }} <br/>
                @endforeach
            @endif
        </dd>
        @if($course->isSeminar)
        <dt>Location</dt>
        <dd>{{ $class->completeLocation }}</dd>
        @endif
        <dt>Date</dt>
        <dd>
            {{ $class->getDateTimeRange() }}
        </dd>
        <dt>Type</dt>
        <dd><em>{{ $class->course->type }}</em></dd>
    </dl>
    @if ($class->course->isWebcast())
        <dl class="dl-horizontal" style="width:80%">
            <dt>Webcast Link
            <dd>
                <div class="input-group">
                    <input type="text"
                           id="meetingId"
                           style="height: 39px !important; width: 100% !important; font-size: 0.9em"
                           class="form-control" value="{{ $course->meetingUrl }}" />

                    <div class="input-group-addon">
                        <button class="copy-btn btn btn-default btn-xs"
                                id="copy-btn-meeting"
                                title="Copy"
                                data-clipboard-target="#meetingId"><i class="fa fa-copy"></i>
                        </button>
                        <a class="btn btn-xs btn-default go-btn"
                           href="{{ $course->meetingUrl }}"
                           title="Open Link"
                           target="_blank"><i class="fa fa-arrow-circle-o-right"></i></a>
                    </div>
                </div>
            </dd>
        </dl>
        <dl class="dl-horizontal" style="width:80%">
            <dt>Online Test Link</dt>
            <dd>
                <div class="input-group">
                    <input type="text"
                           id="examId"
                           style="height: 39px !important; width: 100% !important; font-size: 0.9em"
                           class="form-control" value="{{ $course->testUrl }}" />

                    <div class="input-group-addon">
                        <button class="copy-btn btn btn-default btn-xs"
                                id="copy-btn-exam"
                                title="Copy"
                                data-clipboard-target="#examId"><i class="fa fa-copy"></i>
                        </button>
                        <a class="btn btn-xs btn-default go-btn"
                           href="{{ $course->testUrl }}"
                           title="Open Link"
                           target="_blank"><i class="fa fa-arrow-circle-o-right"></i></a>
                    </div>
                </div>
            </dd>
        </dl>
    @endif
@endif