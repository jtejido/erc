<form action="{{route('status.cert')}}"
      method="POST" class="form-inline  ">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="user_id" value="{{$student->id}}" />
    <input type="hidden" name="class_id" value="{{$student->regOrderId}}"/>
    <input type="hidden" name="key" value=""/>
    <a class="btn btn-flat btn-default btn-block"
       title="Preview Certificate"
       href="{{ route('preview.cert', [$student->id, $student->regOrderId]) }}"
       target="_blank">Preview Certificate</a>
    <a class="btn btn-flat btn-default btn-block"
       title="Download Certificate"
       href="{{ route('download.cert', [$student->id, $student->regOrderId]) }}">
        Download Certificate</a>
    @if ($attended == '1' && $student->user || ($meeting_flag == '1' && $exam_flag == '1'  && $student->user))
        <button type="button"
                title="Resend Certificate"
                data-msg="Resend certificate to this user?"
                data-key="certificate_sent"
                class="btn btn-flat btn-default submitButton btn-block"
                data-flag="1">Resend Certificate
        </button>
    @endif
    <a  href="{{ route('regenerate.cert', [$student->id, $student->regOrderId]) }}"
        class="btn btn-block"
        title="Regenerate Certificate"
        data-msg="Are you sure you want to re-generate this student's certificate?">
        Regenerate Certificate</a>
</form>