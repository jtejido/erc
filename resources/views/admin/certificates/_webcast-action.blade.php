<form action="{{route('status.cert')}}"
      data-student-id="{{ $student->id }}"
      data-student-order="{{ $student->regOrderId }}"
      data-student-status="{{ $attended }}"
      id="generateCertForm"
      class="generateCertForm"
      method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="user_id" value="{{$student->id}}" />
    <input type="hidden" name="class_id" value="{{$student->regOrderId}}"/>
    <input type="hidden" name="status" value="{{ $meeting_flag }}" />
    <input type="hidden" name="status_exam" value="{{ $exam_flag }}" />
    <input type="hidden" name="type" value="{{ \App\Utilities\Constant::WEBCAST }}" />
    <input type="hidden" name="key" value="" />
    <dl class="webcast action dl-horizontal">
        <dt>Webcast</dt>
        <dd>
    @if($meeting_flag)
        {{--*/ $count++ /*--}}
        @if($meeting_flag == '2')
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as PRESENT?"
                    data-flag="1"
                    data-key="meeting_flag"
                    class="btn btn-flat btn-success submitButton">Change to Watched</button>
        @else
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as ABSENT?"
                    data-flag="2"
                    data-key="meeting_flag"
                    class="btn btn-flat btn-danger submitButton">Change to Absent</button>
        @endif
    @else
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as PRESENT?"
                data-flag="1"
                data-key="meeting_flag"
                class="btn btn-flat btn-success submitButton">Mark as Watched</button>
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as PRESENT?"
                data-flag="2"
                data-key="meeting_flag"
                class="btn btn-flat btn-danger submitButton">Mark as Absent</button>
    @endif
        </dd>
            <dt>Exam</dt>

        <dd>
    @if($exam_flag)
        {{--*/ $count++ /*--}}
        @if($exam_flag == '2')
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as PRESENT?"
                    data-flag="1"
                    data-key="exam_flag"
                    class="btn btn-flat btn-success submitButton">Change to Taken</button>
        @else
            <button type="button"
                    data-msg="Are you sure you want to mark this attendee as ABSENT?"
                    data-flag="2"
                    data-key="exam_flag"
                    class="btn btn-flat btn-danger submitButton">Change to Not Taken</button>
        @endif
    @else
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as PRESENT?"
                data-flag="1"
                data-key="exam_flag"
                class="btn btn-flat btn-success submitButton">Mark as Taken</button>
        <button type="button"
                data-msg="Are you sure you want to mark this attendee as PRESENT?"
                data-flag="2"
                data-key="exam_flag"
                class="btn btn-flat btn-danger submitButton">Mark as Not Taken</button>
    @endif
        </dd>
    </dl>

</form>