@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>Add Attendees</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="">Add Attendees</a></li>
        </ol>
    </section>

    {{-- */ $registrable = $class ?: $course; $classId = $class ? $class->id : 0; /* --}}

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h4 class="inline">
                            {{ $course->title }}
                        </h4>

                        @if ($class)
                            <br>
                            <p class="small inline">
                                {{ $class->date_range }}
                            </p>
                        @endif

                        <button class="btn btn-flat btn-success pull-right" id="add-contact"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-user_id="{{ $user->id }}"
                                data-url="{{ route('contact.form.get') }}">

                            <i class="fa fa-plus"></i> Add Contact
                        </button>
                    </div>

                    <div class="box-body">

                        @if (Session::has('message'))
                            <div class="alert alert-{{ Session::get('classMessage') }} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @include('errors.form_errors')

                        <table class="table table-bordered table-hover" id="table-contacts">
                            <thead>
                            <tr>
                                <th><input type="checkbox" id="check-all"></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            @if ($agent)

                                {{-- */

                                $isContactRegistered = OrderUtility::isContactRegistered($user->contact->id, $registrable);

                                /* --}}

                                <tr class="{{ ($isContactRegistered) ? 'registered' : '' }}">
                                    <td width="1%">
                                        @if(!$isContactRegistered)
                                        <input type="checkbox" name="contact[]" value="{{ $agent->contact->id }}"
                                               class="contact check-single">
                                        @endif
                                    </td>
                                    <td>
                                        <label for="contact_{{ $agent->contact->id }}" title="{{ $agent->contact->id }}">
                                            {{ $agent->contact->name }}
                                        </label>
                                    </td>
                                    <td>
                                        <p>{{ $agent->email }}</p>

                                        @if ($agent->hasSubscription)
                                            <p class="block label label-info btn-flat btn-block">
                                                Has Yearly Subscription
                                            </p>
                                        @endif
                                    </td>
                                    <td>{{ $agent->contact->phone }}</td>
                                    <td>
                                        <p>{{ $agent->contact->address1 }} {{ $agent->contact->address2 }}</p>
                                        <p class="block">{{ $agent->contact->city }} {{ $agent->contact->state }} {{ $agent->contact->zip }}</p>
                                    </td>
                                    <td>--</td>
                                    <td>
                                        <a href="{{ route('admin.profile.edit', ['contact_id' => $agent->contact->id]) }}"
                                           class="edit-contact btn btn-primary btn-block btn-flat">
                                            Edit Profile
                                        </a>
                                    </td>
                                </tr>

                            @endif

                            @if ($contacts->count())
                                @foreach ($contacts as $contact)

                                    {{-- */

                                    $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $registrable);

                                    /* --}}

                                    <tr class="{{ ($isContactRegistered) ? 'registered' : '' }}">
                                        <td width="1%">
                                            @if(! $isContactRegistered)
                                            <input type="checkbox" name="contact[]" value="{{ $contact->id }}"
                                                   id="contact_{{ $contact->id }}" class="contact check-single"
                                                   data-contact_id="{{ $contact->id }}">
                                            @endif
                                        </td>
                                        <td>
                                            <label for="contact_{{ $contact->id }}" title="{{ $contact->id }}">
                                                {{ $contact->name }}
                                            </label>
                                        </td>
                                        <td>
                                            @if ($contact->user)
                                                <p>{{ $contact->user->email }}</p>

                                                @if ($contact->user->hasSubscription)
                                                    <p class="block label label-info btn-flat btn-block">
                                                        Has Yearly Subscription
                                                    </p>
                                                @endif
                                            @endif
                                        </td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>
                                            <p>{{ $contact->address1 }} {{ $contact->address2 }}</p>
                                            <p class="block">{{ $contact->city }} {{ $contact->state }} {{ $contact->zip }}</p>
                                        </td>
                                        <td>
                                            @if ($contact->note)
                                                {{ $contact->note }}
                                            @else
                                                --
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.profile.edit', ['contact_id' => $contact->id]) }}"
                                               class="btn btn-primary btn-block btn-flat">
                                                Edit Profile
                                            </a>
                                        </td>
                                    </tr>

                                @endforeach
                            @endif

                        </table>

                        <button class="btn btn-flat btn-primary" id="register"
                                data-course_id="{{ $course->id }}"
                                data-class_id="{{ $classId }}"
                                data-agent="{{ $user->id }}"
                                data-url="{{ route('admin.order.attendees.add.post') }}">
                            Add To Registration
                        </button>

                        <a href="{{ route('admin.order.info', [$user->id, $order->id]) }}" class="btn btn-flat btn-default">
                            Return to Order Info
                        </a>

                        <input type="hidden" id="item-id" value="{{ $orderItem->id }}">
                        <input type="hidden" id="is-swap" value="0">
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirmation modal -->

        @include('includes.notification_modal')

        <!-- Modal for adding / editing class -->

        <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="classModalLabel">Save Contact</h4>
                    </div>
                    <div class="modal-body">
                        <i>Loading...</i>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection