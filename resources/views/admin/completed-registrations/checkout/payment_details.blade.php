<div class="col-md-5">
    <div class="panel panel-default order-items">
        <div class="panel-heading display-table" >
            <div class="row display-tr" >
                <h3 class="panel-title display-td" >Payable Summary</h3>
            </div>
        </div>
        <div class="panel-body">
            @if ($items->count())
                {!! OrderModificationUtility::extractOrderAdjustment($items, $total, true) !!}

                @if ($balance > 0)
                    <div class="row">
                        <div class="col-md-12 sub-total-box">
                            <span class="info-box-text">Balance Due :</span>
                            <span class="info-box-number">${{ number_format($balance, 2) }}</span>
                        </div>
                    </div>
                @endif
                @if($order->has_tax)
                <div class="row">
                    <div class="col-xs-12">
                        <div class="text-left">
                            <strong>NOTE: </strong><em>Balance due includes adjustment in sales tax if applicable.</em>
                        </div>
                    </div>
                </div>
                @endif
            @endif
        </div>
    </div>
</div>