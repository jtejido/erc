<div class="modal-header panel-heading">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="classModalLabel">
        Cancellation
    </h4>
</div>

@if (count($customers))

    {!! Form::open([
            'method' => 'POST',
            'url' => route('admin.registration.cancel')
            ]) !!}
    <div class="modal-body">

        <div class="form-group text-center">
            <h4>
                Add reason of cancellation on each registration.
            </h4>
        </div>

        {!! Form::input('hidden', 'registration_order_id', $registrationOrderId) !!}
        {!! Form::input('hidden', 'agent_id', $agentId) !!}

        @foreach ($customers as $customer)

            {!! Form::input('hidden', 'class_registration_ids[]', $customer['id']) !!}

            {!! Form::label('name', $customer['name'] . ':', ['class' => 'col-sm-3 control-label']) !!}
            <div class="form-group col-md-8">
                {!! Form::textarea('notes['. $customer['id'] .']', null, [
                    'size' => '30x5',
                    'class' => 'form-control',
                    'style' => 'display: inline;',
                    'required' => 'required',
                    ])  !!}
            </div>

        @endforeach

        <div class="form-group col-md-12">
            <hr>
        </div>

        @if (($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count()) or
             ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
                     $deferredGroupDiscount['class_combination_discount_ids']->count()))

            <div class="well well-sm col-md-12 ">

                @if ($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count())
                    @foreach ($deferredHalfDiscount['class_registration_ids'] as $classRegIds)
                        {!! Form::input('hidden', 'deferred_half_price_class_registration_ids[]', $classRegIds) !!}
                    @endforeach

                    <div class="text-center">
                        <span class="required">
                            There are half price discounts that will be considered void once cancellation is done.
                        </span>
                    </div>
                @endif

                @if ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
                     $deferredGroupDiscount['class_combination_discount_ids']->count())
                    @foreach ($deferredGroupDiscount['class_registration_ids'] as $classRegIds)
                        {!! Form::input('hidden', 'deferred_group_discounts_class_registration_ids[]', $classRegIds) !!}
                    @endforeach

                    @foreach ($deferredGroupDiscount['class_combination_discount_ids'] as $classCombinationIds)
                        {!! Form::input('hidden', 'deferred_class_combination_discount_ids[]', $classCombinationIds) !!}
                    @endforeach

                    <div class="text-center">
                        <span class="required">
                            There are group discounts that will be considered void once cancellation is done.
                        </span>
                    </div>
                @endif

            </div>

        @endif

        {!! Form::label('paid', 'Amount Paid for Cancelled Registrations :', ['class' => 'col-sm-4 control-label']) !!}
        <div class="form-group col-md-8">
            <span class="form-control no-border" id="amount-paid" data-amount="{{ $paid }}">
                ${{ number_format($paid, 2) }}
            </span>

            <input type="hidden" name="amount_paid" value="{{ $paid }}">
        </div>

        @if ($deferredHalfDiscount['amount'] and $deferredHalfDiscount['class_registration_ids']->count())
            {!! Form::label('deferred_half_price_discounts', 'Cancelled Half Price Discount Fees :', ['class' => 'col-sm-4 control-label']) !!}

            <div class="form-group col-md-8">
                <span class="form-control no-border" id='deferred-half-price-fees' data-amount="{{ $deferredHalfDiscount['amount'] }}">
                    ${{ number_format($deferredHalfDiscount['amount'], 2) }}
                </span>

                <input type="hidden" name="deferred_half_price_fees" value="{{ $deferredHalfDiscount['amount'] }}">
            </div>
        @endif

        @if ($deferredGroupDiscount['amount'] and $deferredGroupDiscount['class_registration_ids']->count() and
             $deferredGroupDiscount['class_combination_discount_ids']->count())
            {!! Form::label('deferred_half_price_discounts', 'Cancelled Group Discount Fees :', ['class' => 'col-sm-4 control-label']) !!}

            <div class="form-group col-md-8">
                <span class="form-control no-border" id='deferred-group-discount-fees' data-amount="{{ $deferredGroupDiscount['amount'] }}">
                    ${{ number_format($deferredGroupDiscount['amount'], 2) }}
                </span>

                <input type="hidden" name="deferred_goup_discount_fees" value="{{ $deferredGroupDiscount['amount'] }}">
            </div>

        @endif

        @if ($additionalFees)

            {!! Form::label('additional_fees', 'Additional Fees :', ['class' => 'col-sm-4 control-label']) !!}

            <div class="form-group col-md-8">
                <span class="form-control no-border" id='additional-fees' data-amount="{{ $additionalFees }}">
                    ${{ number_format($additionalFees, 2) }}
                </span>

                <input type="hidden" name="additional_fees" value="{{ $additionalFees }}">
            </div>

        @endif

        @if ($deductions)

            {!! Form::label('deductions', 'Deductions :', ['class' => 'col-sm-4 control-label']) !!}

            <div class="form-group col-md-8">
                <span class="form-control no-border" id='deductions' data-amount="{{ $deductions }}">
                    ${{ number_format($deductions, 2) }}
                </span>

                <input type="hidden" name="deductions" value="{{ $deductions }}" data-amount="{{ $deductions }}">
            </div>

        @endif

        @if ($withinRestriction)

            {!! Form::input('hidden', 'agent_contact_id', $agentContactId) !!}

            {!! Form::label('name', 'Total Cost of Fee :', ['class' => 'col-sm-4 control-label']) !!}
            <div class="form-group col-md-8">
                <span>$</span>
                {!! Form::input('text', 'total_fee', $totalFee, [
                   'class' => 'form-control',
                   'id'    => 'cancellation-fee',
                   'style' => 'display: inline; width: 120px',
                   ]) !!}

                <span class="required small block cancellation-error-msg"></span>
            </div>
        @endif

        {!! Form::label('subtotal_refund', 'Total Amount of Refund :', ['class' => 'col-sm-4 control-label']) !!}

        <div class="form-group col-md-8">
            <span class="form-control no-border" id='subtotal-refund' data-amount="{{ $subTotalRefund }}">
                ${{ number_format($subTotalRefund, 2) }}
            </span>

            <input type="hidden" name="refund" value="{{ $subTotalRefund }}">
        </div>

        {!! Form::label('new_order_total', 'New Order Total :', ['class' => 'col-sm-4 control-label']) !!}

        <div class="form-group col-md-8">
            <span class="form-control no-border" id='order-total' data-amount="{{ $originalTotal }}">
                ${{ number_format($newOrderTotal, 2) }}
            </span>

            <input type="hidden" name="order_total" value="{{ $newOrderTotal }}">
        </div>

        <div style="text-align: right">
            <label><input type="checkbox" name="doNotEmail" id="doNotEmail" />&nbsp;
                Do Not Email Agent And Attendees</label>&nbsp;&nbsp;&nbsp;
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-flat proceed-cancellation">Proceed With Cancellation</button>
        </div>
    </div>
    {!! Form::close() !!}
@endif

