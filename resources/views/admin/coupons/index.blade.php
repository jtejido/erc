@extends('admin.layout')

@section('content')
<div class="container-fluid">
    <section class="content-header">
        <h1>Coupons</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.coupons') }}">Coupons</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <div class="box-body">

                        @include('admin.includes.success_message')

                        @if (Session::has('searchkey'))
                            <input type="hidden" id="searchkey" value="{{ Session::get('searchkey') }}" data-column="0">
                        @endif


                        @if ($coupons->count() > 0)
                            <table id="table-coupons" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th style="width: 100px !important;">Action</th>
                                </tr>
                                </thead>

                                @foreach ($coupons as $coupon)
                                    <tr>
                                        <td>
                                            {{ $coupon->coupon_code }}
                                        </td>
                                        <td>{{ $coupon->coupon_amount }}</td>
                                        <td>{{ ucwords(str_replace('_',' ',$coupon->coupon_type)) }}</td>
                                        <td>
                                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                                            <a class="btn btn-primary btn-md btn-flat"
                                               href="{{ route('coupons.edit', ['id' => $coupon->id]) }}">
                                                Edit
                                            </a>
                                            <button class="btn btn-danger btn-md btn-flat delete" data-href="{{ route('coupons.delete') }}" data-id="{{ $coupon->id }}">Delete</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>

                        @else
                            <i>No Coupons.</i>
                        @endif
                    </div>
                </div>
            </div>

            @include('includes.notification_modal')

        </div>
    </section>
</div>
@endsection



