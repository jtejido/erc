@extends('admin.layout')

@section('content')

    <section class="content-header">
        <h1>@if (isset($coupon)) Edit Coupon @else Add New Coupon @endif</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.coupons') }}">Coupons</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header"></div>
            <div class="box-body">
                @include('admin.includes.success_message')
                @include('admin.includes.form_errors')

                {!! Form::open(['method' => 'POST', 'url' => route('coupons.save'), 'id' => 'coupons_form', 'style' => 'width: 80%', 'class' => 'row']) !!}

                @if (isset($coupon))
                    {!! Form::input('hidden', 'id', $coupon->id) !!}
                @endif

                {!! Form::rLabel('coupon_code', 'Coupon Code: ', ['class' => 'col-sm-2 control-label']) !!}
                <div class="form-group col-md-9">
                    {!! Form::input('text', 'coupon_code', isset($coupon) ? $coupon->coupon_code : $coupon_code, [
                        'class' => 'form-control col-md-9',
                        'placeholder' => 'Coupon Code',
                        'style' => 'width: 300px; display: inline;',
                        'required' => 'required'
                    ]) !!}
                </div>

                {!! Form::rLabel('coupon_amount', 'Amount: ', ['class' => 'col-sm-2 control-label']) !!}
                <div class="form-group col-md-9">
                    {!! Form::input('number', 'coupon_amount', isset($coupon) ? $coupon->coupon_amount : '', [
                        'id'    => 'coupon_amount',
                        'class' => 'form-control col-md-9',
                        'placeholder' => 'Amount',
                        'step'  => 'any',
                        'min'       => '0',
                        'style' => 'width: 300px; display: inline;',
                        'required' => 'required'
                    ]) !!}
                </div>

                {!! Form::rLabel('Coupon', 'Coupon Type: ', ['class' => 'col-sm-2 control-label']) !!}
                <div class="form-group col-md-9">
                    {!! Form::radio('coupon_type', \App\Utilities\Constant::PERCENT,
                    (isset($coupon) && ($coupon->coupon_type == \App\Utilities\Constant::PERCENT))
                    ? true
                    : !isset($coupon)
                    ? true
                    : false,
                    [
                            'class' => 'pull-left change_coupon_type',
                            'id' => 'percent',
                            'style' => 'display: inline;',
                                'required' => 'required'
                        ]) !!}
                    {!! Form::label('percent', 'Percent: ', ['class' => 'col-sm-2 control-label']) !!}

                    {!! Form::radio('coupon_type', \App\Utilities\Constant::FIXED, (isset($coupon) && ($coupon->coupon_type == \App\Utilities\Constant::FIXED)) ? true : false, [
                            'class' => 'pull-left change_coupon_type',
                            'id' => 'fixed_amount',
                            'style' => 'display: inline;',
                            'required' => 'required'
                        ]) !!}
                    {!! Form::label('fixed_amount', 'Fixed Amount: ', ['class' => 'col-sm-3 control-label']) !!}
                </div>

                {{-- */
                    if (isset($discountableType)) {
                        $courseAttr = $discountableType == \App\Utilities\Constant::COURSE_OBJECT;
                        $classAttr  = $discountableType == \App\Utilities\Constant::COURSE_CLASS_OBJECT;
                    }
                 /* --}}

                {!! Form::rLabel('apply', 'Apply To: ', ['class' => 'col-sm-2 control-label']) !!}
                <div class="form-group col-md-9">
                    {!! Form::radio('discountable_type', \App\Utilities\Constant::COURSE_OBJECT,
                    (isset($coupon) && ($discountableType == \App\Utilities\Constant::COURSE_OBJECT))
                    ? true
                    : !isset($coupon)
                    ? true
                    : false,
                    [
                            'class' => 'pull-left coupon-apply',
                            'id' => 'courses',
                            'style' => 'display: inline;',
                            'required' => 'required'
                        ]) !!}
                    {!! Form::label('courses', 'Courses ', ['class' => 'col-sm-2 control-label']) !!}

                    {!! Form::radio('discountable_type', \App\Utilities\Constant::COURSE_CLASS_OBJECT, (isset($coupon) && ($discountableType == \App\Utilities\Constant::COURSE_CLASS_OBJECT)) ? true : false, [
                            'class' => 'pull-left coupon-apply',
                            'id' => 'classes',
                            'style' => 'display: inline;',
                            'required' => 'required'
                        ]) !!}
                    {!! Form::label('classes', 'Classes ', ['class' => 'col-sm-3 control-label']) !!}
                </div>

                @if ($courses)

                    <div class="courses-container" style="@if (isset($classAttr) && $classAttr) display:none @endif">
                        {!! Form::rLabel('courses', 'Courses: ', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="form-group col-md-10 course-list">
                            @foreach ($courses as $k => $course)
                                <div class="form-group">
                                    {!! Form::checkbox('discountable_ids[]', $course->id,
                                    ((isset($courseAttr) && $courseAttr) && in_array($course->id, $discountableIds))
                                    ? true
                                    : false,

                                    [
                                        'class' => 'pull-left',
                                        'id' => 'course_'.$k,
                                        'style' => 'display: inline;',
                                        (isset($classAttr) && $classAttr) ? 'disabled' : null
                                    ]) !!}
                                    {!! Form::label('course_'.$k, $course->title, ['class' => 'control-label', 'style'=>'display:inline-block;font-weight:normal; margin-left:5px;']) !!}
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div class="classes-container" style="@if ((isset($courseAttr) && $courseAttr) || !isset($coupon)) display:none @endif">
                        {!! Form::rLabel('classes', 'Classes: ', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="form-group col-md-10 class-list">
                            @foreach ($courses as $course)

                                @if ($course->classes)

                                    @foreach($course->classes as $k => $class)
                                        <div class="form-group">
                                            {!! Form::checkbox('discountable_ids[]', $class->id,
                                            ((isset($classAttr) && $classAttr) && in_array($class->id, $discountableIds))
                                            ? true
                                            : false,

                                            [
                                                'class' => 'pull-left',
                                                'id' => 'class_'.$k,
                                                'style' => 'display: inline;',
                                                (isset($courseAttr) && $courseAttr) || !isset($coupon) ? 'disabled' : null
                                            ]) !!}

                                            <label for="class_{{ $k }}" style="display:inline-block;font-weight:normal; margin-left:5px;">
                                                <p>{{ $course->title }}</p>
                                                <p>{{ $class->completeLocation }}</p>
                                                <p>{{ date('F d, Y', strtotime($class->start_date)) }} - {{ date('F d, Y', strtotime($class->end_date)) }}</p>
                                            </label>
                                        </div>
                                    @endforeach

                                @endif

                            @endforeach
                        </div>
                    </div>
                @endif


                <div class="form-group col-md-9">
                    {!! Form::input('submit', 'Save', 'Save', ['class' => 'btn btn-success btn-flat btn-save']) !!}
                </div>
                {!! Form::close() !!}

                        <!-- Confirmation modal -->
                @include('includes.notification_modal')
            </div>
        </div>
    </section>
@endsection

@section('javascripts')
    @parent
    <script>
        (function($) {
            $('input[name="discountable_type"]').on('change', function() {
                var id = $(this).attr('id'),
                    $courseContainer = $('.courses-container'),
                    $courseCheckBoxes = $courseContainer.find('input[type="checkbox"]'),
                    $classContainer  = $('.classes-container'),
                    $classCheckBoxes = $classContainer.find('input[type="checkbox"]');

                if (id == 'courses') {
                    $courseContainer.show();
                    $classContainer.hide();

                    $courseCheckBoxes.attr('disabled', false);
                    $classCheckBoxes.attr('disabled', true);

                    return true;
                }

                $courseContainer.hide();
                $classContainer.show();

                $courseCheckBoxes.attr('disabled', true);
                $classCheckBoxes.attr('disabled', false);
            });

            $('.btn-save').on('click', function(e) {
                var selected = $('input[name="discountable_type"]:checked').attr('id'),
                    $courseCheckboxes = $('.courses-container').find('input[type="checkbox"]:checked'),
                    $classCheckboxes  = $('.classes-container').find('input[type="checkbox"]:checked'),
                    length;

                length = selected == 'courses'
                        ? $courseCheckboxes.length
                        : $classCheckboxes.length;

                if (!length) {
                    e.preventDefault();
                }

                return true;
            });
        })(jQuery);
    </script>
@endsection