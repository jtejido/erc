@extends('admin.layout')

@section('body-class', 'add')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }

    var _categories = {!! $categories->toJson() !!};
    data.categories = _categories;
    @if (@$link)
    var link = {!! $link->toJson() !!};
    data.link = link;
    @endif
</script>
<div class="container-fluid" ng-controller="AdminLinksAdd">
    <section class="content-header">
        <h1>Helpful Links <a href="/admin/links"><button class="btn btn-default">Back</button></a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
    <section>
        <form name="linksAdd" ng-submit="save()" novalidate>
            <div class="bs-callout bs-callout-danger" ng-show="formSubmitError">
                <h4>Issues encountered:</h4>
                <p>@{{ formSubmitErrorMessage }}</p>
            </div>
            <div class="bs-callout bs-callout-success" ng-show="formSubmitSuccess">
                <h4>Link of the week saved!</h4>
            </div>

                <div class="row">
                    <div class="form-group col-md-8 col-sm-8"
                        ng-class="{
                            'has-error' : linksAdd.name.$invalid && formSubmitInvalid,
                            'has-success': linksAdd.name.$valid
                        }">
                        <label for="name">
                            Name
                        </label>
                        <input type="text" class="form-control" id="name" placeholder="Name"
                            name="name"
                            ng-model="data.link.name"
                            tabindex="1" 
                            required>
                        <p ng-show="linksAdd.name.$invalid && formSubmitInvalid" class="help-block">A name is required.</p>
                    </div>
                      <div class="form-group col-md-4 col-sm-4">
                        <label for="category_id">Category</label>
                        <select name="category_id" class="form-control" ng-model="data.link.category"
                            ng-options="category.name for category in categories track by category.id">
                        </select>
                      </div>
                  </div>
                <div class="form-group"
                    ng-class="{
                            'has-error' : linksAdd.url.$invalid && formSubmitInvalid,
                            'has-success': linksAdd.url.$valid
                    }">
                    <label>URL</label>
                    <input type="text" class="form-control" name="url" placeholder="URL" ng-model="data.link.url" required></textarea>
                    <p ng-show="linksAdd.url.$invalid && formSubmitInvalid" class="help-block">URL is needed.</p>
                </div>
                  <div ng-if="!data.link.id">
                        <button type="button" class="btn btn-primary"
                            ng-disabled="formSubmitInvalid && linksAdd.$invalid"
                            ng-click="save()">Save</button>
                  </div>
                  <div ng-if="data.link.id">
                    <button type="button" class="btn btn-success" ng-click="save()" ng-disabled="linksAdd.$pristine">Update</button>
                    <button type="button" class="btn btn-danger" ng-click="delete()">Delete</button>
                  </div>
        </form>
    </section>
</div>
@endsection
