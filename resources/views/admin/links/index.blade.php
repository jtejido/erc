@extends('admin.layout')

@section('body-class', 'index')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _links = {!! $links->toJson() !!};
    data.links = _links;
</script>
<div class="container-fluid" ng-controller="AdminLinks">
    <section class="content-header">
        <h1>Helpful Links <a href="/admin/links/add"><button class="btn btn-success">New Link</button></a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header box-header-users"></div>

                    <div class="box-body">

                        @if ($links->count())
                            <table id="table-clients" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                @foreach ($links as $link)
                                    <tr>
                                        <td>
                                            <strong>{{ $link->name }}</strong>
                                            <p>{{ $link->url }}</p>
                                            <p><em>{{ $link->category[0]->name }}</em></p>
                                        </td>
                                        <td>{{ $link->created_at->format('M d, Y') }}</td>
                                        <td>
                                            <a href="/admin/links/edit/{{$link->id}}">
                                                <button type="button" class="btn btn-default">Show</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
