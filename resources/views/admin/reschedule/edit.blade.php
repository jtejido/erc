@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Reschedule Attendee</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Reschedule Attendee</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @include('admin.includes.form_errors')
                    </div>
                    <div class="box-body">
                        {!! Form::open([
                            'url'       => route('reschedule.store', [ $orderItemId ]),
                            'id'        => 'rescheduleForm',
                            'method'    => 'post']) !!}
                        <input type="hidden" name="class_id" value="0" />
                        <input type="hidden" name="contact_id" value="{{ $contactId }}" />
                        <input type="hidden" name="doNotEmailHidden" value="0" />
                        <div class="row">
                            <div class="col col-md-8 col-md-offset-2">
                                <dl class="dl-horizontal">
                                    <dt>Course</dt>
                                    <dd>{{ $course->title }}</dd>
                                    <dt>Attendee</dt>
                                    <dd>{{ $contact->name }} - {{ $contact->company }}</dd>
                                    <dt>Current Date</dt>
                                    <dd>{{ $regOrder->date() }} <em>[Code: {{ $regOrder->registrable_id }}]</em></dd>
                                </dl>

                                <p>
                                    <em>Select a New Date Below:</em>
                                </p>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="20px">Code</th>
                                        <th width="170px">Date</th>
                                        <th>City/State</th>
                                        <th width="120px">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($classes as $class)
                                        {{-- */
                                        $isContactRegistered = OrderUtility::isContactRegistered($contact->id, $class);
                                        /* --}}
                                    <tr>
                                        <td>{{ $class->id }}</td>
                                        <td>{{ $class->short_date_range }}</td>
                                        <td>{{ $class->location_name }}</td>
                                        <td class="text-center">
                                            @if($isContactRegistered)
                                                <small>Already Registered</small>
                                                @else
                                            <a href="#" class="btn btn-warning select-class"
                                               data-class_id = "{{ $class->id }}"
                                            >Select</a>
                                                @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @if($classes->count() == 0)
                                        <tr>
                                            <td colspan="4" class="text-center">There are no other schedules for this class.</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {!! Form::close() !!}
                        <a href="{{ route('admin.order.info', [
                            'user_id'   => $order->user_id,
                            'order_id'   => $order->id,
                        ]) }}" class="btn btn-default">
                            <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back To Order Details</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('includes.notification_modal')
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            var $doNotEmail = $('#doNotEmailContainer');
            $doNotEmail.show();

            $('.select-class').click(function(e) {
                e.preventDefault();
                var class_id = $(this).data('class_id');
                $("input[name='class_id']").val(class_id);
                var $form = $('#rescheduleForm');

                Modal.showConfirmationModal(
                    'Are you sure you want to proceed with the reschedule?',
                    'Confirmation',
                    function() {
                        if($('#doNotEmail').is(':checked')) {
                            $("input[name='doNotEmailHidden']").val(1);
                        } else {
                            $("input[name='doNotEmailHidden']").val(0);
                        }

                        $form.submit();
                        return false;
                    });

                return false;
            });
        });
    </script>
@endsection
