@extends('admin.layout')

@section('body-class', 'add')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }

    var _categories = {!! $categories->toJson() !!};
    data.categories = _categories;
    @if (@$regulation)
    var regulation = {!! $regulation->toJson() !!};
    data.regulation = regulation;
    @endif
</script>
<div class="container-fluid" ng-controller="AdminRegulationsAdd">
    <section class="content-header">
        <h1>Regulations</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.regulations.index') }}">Regulations</a></li>
            <li class="active">New</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">New Entry</h3>
                </div>
                <form name="regsAdd" ng-submit="save()" novalidate  enctype="multipart/form-data">
                    <input ng-if="data.tip.id" type="hidden" id="id" value="@{{ data.tip.id }}">
                    <div class="box-body">
                        <div class="callout callout-danger" ng-show="formSubmitError" ng-cloak>
                            <h4>Issues encountered:</h4>
                            <p>@{{ formSubmitErrorMessage }}</p>
                        </div>
                        <div class="callout callout-success" ng-show="formSubmitSuccess" ng-cloak>
                            <h4>Reg of the Day saved!</h4>
                        </div>
                        <div class="callout callout-success" ng-show="formDestroySuccess" ng-cloak>
                            <h4>Deleting regulation successful!</h4>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label for="photo">
                                    Photo <span ng-if="data.reg.id && !data.reg.is_published"><span class="label label-primary">Draft</span></span>
                                </label>
                                <input name="photo" type="file" class="input-lg" id="photo"
                                       accept="image/*">
                            </div>
                            <div class="form-group col-md-6">
                                @if (isset($regulation) and $regulation->photo and $regulation->photo_mime)
                                    <label for="">Current Photo</label>
                                    <img src="{{ route('reg.photo', $regulation->id) }}" alt="Reg Photo" style="width: 100%; height: 100%;">
                                @endif
                            </div>
                            <div class="form-group col-md-8 col-sm-8"
                                ng-class="{
                                    'has-error' : regsAdd.title.$invalid && formSubmitInvalid,
                                    'has-success': regsAdd.title.$valid
                                }">
                                <label for="title">
                                    Title <span ng-if="data.reg.id && !data.reg.is_published"><span class="label label-primary">Draft</span></span>
                                </label>
                                <input type="text" class="form-control" id="title" placeholder="Title"
                                    name="title"
                                    ng-model="data.reg.title"
                                    tabindex="1" 
                                    required>
                                <p ng-show="regsAdd.title.$invalid && formSubmitInvalid" class="help-block">A title is required.</p>
                            </div>
                              <div class="form-group col-md-4 col-sm-4">
                                <label for="category_id">Category</label>
                                <select name="category_id" class="form-control" ng-model="data.reg.category"
                                    ng-options="category.name for category in categories track by category.id">
                                </select>
                              </div>
                          </div>

                        <div class="form-group"
                            ng-class="{
                                    'has-error' : regsAdd.content.$invalid && formSubmitInvalid,
                                    'has-success': regsAdd.content.$valid
                            }">
                            <label>Content</label>
                            <textarea class="form-control" rows="3" name="content" placeholder="Content" ui-tinymce="getTinymceOptions()" ng-model="data.reg.content" required></textarea>
                            <p ng-show="regsAdd.content.$invalid && formSubmitInvalid" class="help-block">Content is needed.</p>
                        </div>
                        <h5>Published Date</h5>
                        <div class="row">
                            <div class="form-group col-md-2 col-sm-2"
                                ng-class="{
                                    'has-error' : regsAdd.day.$invalid && formSubmitInvalid,
                                    'has-success': regsAdd.day.$valid
                                }">
                                <label for="day">Day</label>
                                <input type="number" min="1" max="31" step="1" name="day" class="form-control" placeholder="@{{ data.reg.published_date.day }}"
                                    ng-model="data.reg.published_date.day"
                                    ng-pattern="/^([1|2][0-9])|[1-9]|(3(0|1))$/"
                                    required>
                                <p ng-show="regsAdd.title.$invalid && formSubmitInvalid" class="help-block">Date of publish required.</p>
                            </div>
                            <div class="form-group col-md-2 col-sm-2">
                                <label for="day">Month</label>
                                <select name="month" class="form-control" ng-model="data.reg.published_date.month"
                                    ng-options="month as month for month in months">
                                </select>
                            </div>
                            <div class="form-group col-md-2 col-sm-2">
                                <label for="year">Year</label>
                                <input type="number" name="year" class="form-control" ng-model="data.reg.published_date.year">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" ng-class="{
                                    'has-error' : regsAdd.title.$invalid && formSubmitInvalid,
                                    'has-success': regsAdd.title.$valid
                                }">
                                <label for="slug">
                                    Slug
                                </label>
                                <input type="text" class="form-control" id="slug" placeholder="Slug"
                                       name="slug"
                                       ng-model="data.reg.slug"
                                       tabindex="1"
                                       required>
                                <p ng-show="regsAdd.slug.$invalid && formSubmitInvalid" class="help-block">A slug is required.</p>
                                </div>
                            </div>
                        </div>

                            <div ng-if="!data.reg.id">
                                    <button type="button" class="btn btn-success" ng-click="publish()">Save and Publish</button>
                                    <button type="button" class="btn btn-primary"
                                        ng-disabled="formSubmitInvalid && regsAdd.$invalid"
                                        ng-click="save()">Save Draft</button>
                            </div>
                            <div ng-if="data.reg.id">
                                <span ng-if="data.reg.is_published">
                                    <button type="button" class="btn btn-success" ng-click="publish()">Update</button>
                                    <button type="button" class="btn btn-warning"
                                        ng-disabled="formSubmitInvalid && regsAdd.$invalid"
                                        ng-click="save({is_published: false})">Save and Unpublish</button>
                                </span>
                                <span ng-if="!data.reg.is_published">
                                    <button type="button" class="btn btn-success" ng-click="publish()">Publish</button>
                                    <button type="button" class="btn btn-warning"
                                        ng-disabled="formSubmitInvalid && regsAdd.$invalid"
                                        ng-click="save()">Update</button>
                                </span>

                                <button type="button" class="btn btn-danger" ng-click="delete()">Delete</button>
                            </div>
                        </div><!-- .box-body -->
                    </form>
            </div><!-- .box -->
        </div><!-- .row -->
    </section><!-- .content -->
</div>
@include('includes.confirmation_modal')
@endsection
