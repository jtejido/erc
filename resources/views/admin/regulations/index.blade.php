@extends('admin.layout')

@section('body-class', 'index')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _regs = {!! $regulations->toJson() !!};
    data.regs = _regs;
</script>
<div class="container-fluid" ng-controller="AdminRegulations">
    <section class="content-header">
        <h1>Regulations <a href="/admin/regulations/add"><button class="btn btn-success">New</button></a></h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header box-header-users">
                        <div class="filters pull-right">
                            <form action="" method="GET">
                                <input type="text" class="form-control" name="title" value="{{ isset($title) ? $title : '' }}" style="display: inline; height: 35px !important;" placeholder="Regulation Title">
                                <button type="submit" class="btn btn-flat btn-primary"><span class="fa fa-search"></span></button>
                                <a href="{{ route('admin.regulations.index') }}" class="btn btn-success btn-flat">Clear</a>
                            </form>
                        </div>
                    </div>

                    <div class="box-body">

                        @if ($regulations->count())
                            <table id="table-clients" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Publish Date</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                @foreach ($regulations as $reg)
                                    <tr>
                                        <td data-title="'Title'" sortable="'title'">
                                            <strong>{{ $reg->title }}</strong>
                                            <p><em>{{$reg->category[0]->name}}</em></p>
                                            
                                        </td>
                                        <td>
                                            {{ $reg->published_date->format('M d, Y') }}
                                            @if (!$reg->is_published)
                                            <span class="label label-primary">Draft</span>
                                            @endif
                                        </td>
                                        <td>{{ $reg->created_at }}</td>
                                        <td>
                                            <a href="/admin/regulations/edit/{{$reg->id}}">
                                                <button type="button" class="btn btn-primary">View</button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                            </table>
                        @else
                            <p class="text-center">Currently, there are no regulations.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
