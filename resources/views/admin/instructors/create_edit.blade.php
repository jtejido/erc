@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>@if(isset($instructor)) Edit @else Add @endif Instructor</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('instructor.index') }}">Instructors Management</a></li>
            <li>@if(isset($instructor)) Edit @else Add @endif Instructor</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.form_errors')
                        @include('admin.includes.success_message')
                    </div>

                    <div class="box-body">
                        {{-- */ $route = isset($instructor) ? route('instructor.update', $instructor->id) : route('instructor.store') ; /* --}}
                        {!! Form::open(['url'       => $route,
                                        'method'    => 'post',
                                        'files'     => true,
                                        'class'     => 'status-filter']) !!}
                        <div class="row">
                            <div class="col-md-6">

                                @if (isset($instructor))
                                    {!! Form::input('hidden', 'id', $instructor->id)  !!}
                                @endif

                                {!! Form::rLabel('first_name', 'First Name: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="form-group col-md-8">
                                    {!! Form::input('text', 'first_name', isset($instructor) ? $instructor->first_name : '', [
                                        'class' => 'form-control',
                                        'style' => 'width: 300px; display: inline;',
                                        'required' => 'required'
                                    ]) !!}
                                </div>

                                {!! Form::rLabel('last_name', 'Last Name: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="form-group col-md-8">
                                    {!! Form::input('text', 'last_name', isset($instructor) ? $instructor->last_name : '', [
                                        'class' => 'form-control',
                                        'style' => 'width: 300px; display: inline;',
                                        'required' => 'required'
                                    ]) !!}
                                </div>

                                {!! Form::label('signature', 'Signature: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="form-group col-md-8">
                                    {!! Form::file('signature', null, [
                                    'accept' =>  'image/*',
                                    'class' => 'form-control'])  !!}

                                    <br>
                                    <p class="required">
                                        Image dimension should be 300x150 px. <br>
                                        Image should have a transparent background.
                                    </p>
                                </div>

                                <div class="form-group col-md-8">
                                    <button type="submit" class="btn btn-success btn-flat btn-save">Save</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if (isset($instructor))
                                    {{-- */ $path = route('signature.preview', $instructor->signature);  /*--}}

                                    @if ($instructor->signature && $path)
                                        {!! Form::label('signature', 'Current Signature: ', ['class' => 'col-sm-3 control-label']) !!}
                                        <div class="thumbnail btn-flat col-md-8">
                                            <img src="{{ $path }}" alt="ALT NAME" class="img-responsive" />
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

