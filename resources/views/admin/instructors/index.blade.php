@extends('admin.layout')
@section('stylesheets')
    @parent
    <style media="screen">
        h3.price {
            color: red;
            font-size: 2em;
        }
    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>Instructors Management</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Instructors Management</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="actions">
                            @include('admin.includes.success_message')
                        </div>
                    </div>

                    <div class="box-body">
                        @if ($instructors->count())
                            <div class="row no-gutter">
                                @foreach($instructors as $instructor)
                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                        <div class="thumbnail btn-flat">
                                            {{-- */ $path = route('signature.preview', $instructor->signature);  /*--}}
                                            @if($instructor->signature && $path)
                                                <img src="{{ $path }}" alt="ALT NAME" class="img-responsive" />
                                            @else
                                                <img src="{{ asset('img/no-img-avail.png') }}" alt="ALT NAME" class="img-responsive" />
                                            @endif
                                            <div class="caption">
                                                <h3 class="instructor-name">{{ $instructor->name }}</h3>
                                                {{-- */ $classes = CourseUtility::getInstructorClasses($instructor->id); /* --}}
                                                <div class="info-container">
                                                    <p>
                                                        <strong>Classes:</strong>&nbsp;<code>{{ $classes->count() }}</code>&nbsp;&nbsp;&nbsp;
                                                    </p>
                                                </div>
                                                <hr/>
                                                <p>
                                                    <a href="{{ route('instructor.edit', $instructor->id) }}"
                                                       class="btn btn-flat btn-sm btn-primary"><i class="fa fa-edit"></i>&nbsp;Edit</a>&nbsp;

                                                    @if (!$instructor->classes->count())
                                                        <a href="javascript:void(0)" data-href="{{ route('instructor.remove', $instructor->id) }}" data-id="{{ $instructor->id }}"
                                                           class="btn btn-flat btn-sm btn-danger delete"><i class="fa fa-remove"></i>&nbsp;Delete</a>&nbsp;
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <i>No Instructors Available.</i>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @include('includes.confirmation_modal')
    </section>

@endsection


