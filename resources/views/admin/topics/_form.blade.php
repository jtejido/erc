<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::rLabel('name', 'Name    ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::text('name', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::rLabel('duration', 'Duration    ', ['class' => 'col-sm-3']) !!}

            <div class="col-sm-9">
                <div class="input-group">
                    {!! Form::number('duration', null,  [
                        'class' => 'form-control',
                        'min'   => 0
                        ])  !!}
                    <div class="input-group-addon">months</div>
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description    ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-9">
                {!! Form::textarea('description', null, $attributes = ['class' => 'form-control'])  !!}
            </div>
        </div>


        <p class="pull-right">
            <a href="{{ route('admin.topics.index') }}" class="btn btn-flat btn-default">Back</a>
            <button type="submit" class="btn btn-flat btn-primary">Save</button>
        </p>
    </div>
    <div class="col-md-6">
    </div>
</div>