@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Add New Topic</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.topics.index') }}">Topics</a></li>
            <li>Add New Topic</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @include('admin.includes.form_errors')
                    </div>

                    <div class="box-body">
                        {!! Form::open([
                                        'url'       => route('admin.topics.store'),
                                        'class'     => 'status-filter form-horizontal'
                                        ]) !!}
                        @include('admin.topics._form')
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>


    </section>
    @include('includes.confirmation_modal')
@endsection