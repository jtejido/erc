@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Topics</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Topics</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.success_message')
                        @include('admin.includes.error_message')
                        @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                        <a href="{{ route('admin.topics.create') }}" class="pull-right btn btn-success btn-flat">Add Topic</a>
                        @endif
                    </div>

                    <div class="box-body">


                            <table id="table-classes-cert" class="table table-bordered table-hover table-classes">
                                <thead>
                                <tr>
                                    <th width="1%">ID</th>
                                    <th>Name</th>
                                    <th>Reminder Duration</th>
                                    <th width="1%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($topics as $topic)
                                        <tr>
                                            <td>{{ $topic->id }}</td>
                                            <td>{{ $topic->name }}</td>
                                            <td>{{ $topic->duration }} months</td>
                                            <td>
                                                {!! Form::open([
                                                    'url'       => route('admin.topics.destroy', [$topic->id]),
                                                    'method'    => 'delete'
                                                ]) !!}
                                                @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                                                <a href="{{ route('admin.topics.edit', [$topic->id]) }}"
                                                   class="btn btn-primary btn-flat btn-block">Edit</a>
                                                @if(!$topic->isAssigned)
                                                <button type="button"
                                                        class="submitButton btn btn-danger btn-flat btn-block">Delete</button>
                                                @endif
                                                @endif
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </div>


    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {
            $('#table-classes-cert').on('click', '.submitButton', function() {
                var $that = $(this);
                Modal.showConfirmationModal(
                        'Are you sure you want to delete this topic?',
                        'Delete Topic',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
            });
            $('#table-classes-cert').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 2 }
                ],
                "order": [[ 0, "asc" ]]
            });
        });
    </script>
@endsection

