@extends('admin.layout')
@section('stylesheets')
    @parent
    <style>

    </style>

@endsection
@section('content')
    <section class="content-header">
        <h1>Coupon Landing Pages</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Coupon Landing</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        @include('admin.includes.form_errors')

                    </div>

                    <div class="box-body">

                        {!! Form::open(['url'       => route('admin.coupon_landing.store'),
                                        'method'    => 'post',]) !!}

                        @include('admin.coupon_landing._form')

                        {!! Form::close() !!}

                    </div>

                </div>
            </div>
        </div>


    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">


        (function($) {


        })(jQuery);
    </script>
@endsection


