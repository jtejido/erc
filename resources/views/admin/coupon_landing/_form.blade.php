<div class="form-group"  style="overflow: hidden">
    {!! Form::label('title', 'Title', ['class' => 'col-sm-3']) !!}
    <div class="col-sm-9">
        {!! Form::text('title', null, $attributes = ['class' => 'form-control', 'maxlength' => 35])  !!}
    </div>
</div>
<div class="form-group" style="overflow: hidden">
    {!! Form::label('content', 'Content', ['class' => 'col-sm-3']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('content') !!}
    </div>

</div>

<div class="form-group" style="overflow: hidden">
    {!! Form::label('coupon_id', 'Coupon', ['class' => 'col-sm-3']) !!}
    <div class="col-sm-9">
        {!! Form::select('coupon_id', $coupons, null, ['class' => 'form-control', 'style' => 'width: 300px'])  !!}
    </div>
</div>

<div class="form-group" style="overflow: hidden">
    {!! Form::label('published', 'Published', ['class' => 'col-sm-3']) !!}
    <div class="col-sm-9">
        {!! Form::checkbox('published', '1', false, ['id' => 'published'])  !!}
    </div>
</div>

<div class="form-group"  style="overflow: hidden">
    {!! Form::label('slug', 'URL (slug)', ['class' => 'col-sm-3']) !!}
    <div class="col-sm-9">
        {{ url(\App\Models\CouponLanding::CP_URL_PREFIX) }}/
        {!! Form::text('slug', $slug, [
            'class' => 'form-control',
            'pattern'   => '^\S+$',
            'style' => 'display: inline; width: 200px'])  !!}
    </div>
</div>

<div>
    <a href="{{ route('admin.coupon_landing.index') }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Back to List</a>

    @if(isset($coupon_landing))
    <a href="{{ route('cp.landing.preview', [$coupon_landing->slug]) }}"
       class="btn btn-flat btn-default"
       target="_blank">Preview</a>
    @endif
    <button type="button" class="btn btn-primary btn-flat" id="saveButton">Save</button>

</div>



{!! Form::hidden('published', '0', ['id' => 'published_hidden']) !!}

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

            tinymce.init({
                selector:'textarea#content',
                plugins: 'link image code',
                menubar: false,
                toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | link image | code',
                file_browser_callback: RoxyFileBrowser
            });

            function RoxyFileBrowser(field_name, url, type, win) {
                var roxyFileman = '/fileman/index.html';
                if (roxyFileman.indexOf("?") < 0) {
                    roxyFileman += "?type=" + type;
                }
                else {
                    roxyFileman += "&type=" + type;
                }
                roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
                if(tinyMCE.activeEditor.settings.language){
                    roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
                }
                tinyMCE.activeEditor.windowManager.open({
                    file: roxyFileman,
                    title: 'Gallery',
                    width: 850,
                    height: 650,
                    resizable: "yes",
                    plugins: "media",
                    inline: "yes",
                    close_previous: "no"
                }, {     window: win,     input: field_name    });
                return false;
            }

            $('#saveButton').click(function() {
                if(document.getElementById("published").checked) {
                    document.getElementById('published_hidden').disabled = true;
                }
                $(this).closest('form').submit();
            });
        })(jQuery);
    </script>
@endsection