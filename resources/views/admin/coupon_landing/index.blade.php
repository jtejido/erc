@extends('admin.layout')
@section('stylesheets')
    @parent
    <style>

    </style>

@endsection
@section('content')
    <section class="content-header">
        <h1>Coupon Landing Pages</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Coupon Landing</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        @include('includes.success_message')
                        <div class="pull-right">
                                <a href="{{ route('admin.coupon_landing.create') }}"
                                   class="btn btn-success btn-flat"><i class="fa fa-plus"></i> New Landing Page</a>
                        </div>

                    </div>

                    <div class="box-body">

                        <table id="coupon_landing_table" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Coupon Code</th>
                                    <th>URL</th>
                                    <th>Published</th>
                                    <th>Last Updated</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($coupon_landings as $coupon_landing)
                            <tr>
                                <td>{{ $coupon_landing->title }}</td>
                                <td>{!! $coupon_landing->getCouponCodeLink() !!}</td>
                                <td>{{ $coupon_landing->url }}</td>
                                <td>{!! $coupon_landing->getPublishedLabel() !!}</td>
                                <td>{{ $coupon_landing->updated }}</td>
                                <td>
                                    {!! Form::open([
                                        'url'=>route('admin.coupon_landing.destroy', [$coupon_landing->id]),
                                        'method'=>'DELETE' ]) !!}
                                    <a href="{{ route('admin.coupon_landing.edit', [$coupon_landing->id]) }}"
                                       class="btn btn-flat btn-primary">Edit</a>
                                    <a href="{{ route('cp.landing.preview', [$coupon_landing->slug]) }}"
                                       class="btn btn-flat btn-default"
                                       target="_blank">Preview</a>
                                    <a href="javascript:void(0)"
                                       class="btn btn-flat btn-danger delete">Delete</a>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>


    </section>
    @include('includes.confirmation_modal')
@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($) {

            $('#coupon_landing_table').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 1 }
                ],
                "order": [[ 0, "desc" ]]
            });

            $('#coupon_landing_table').on('click', '.delete', function() {
                var     $that = $(this);
                Modal.showConfirmationModal(
                        "Are you sure you want to delete this coupon landing page?",
                        'Coupon Landing Page',
                        function() {
                            $that.closest('form').submit();
                            return false;
                        });
                return false;
            });

        })(jQuery);
    </script>
@endsection


