@extends('admin.layout')

@section('body-class', 'index')

@section('content')
    <div class="container-fluid">
        <section class="content-header">
            <h1>Order History</h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header box-header-users">

                            @include('includes.success_message')

                        </div>

                        <div class="box-body">

                            <table id="table-orders" class="table table-bordered table-hover">
                                <thead>
                                    <th width="1%"><i class="fa fa-archive" aria-hidden="true"></i></th>
                                    <th width="110">Transaction Date</th>
                                    <th width="110">Transaction ID / Check Number</th>
                                    <th>Name</th>
                                    <th width="120">Company</th>
                                    <th>Email</th>
                                    <th width="80">Total Price</th>
                                    <th width="80">Status</th>
                                    <th>Action</th>
                                </thead>

                                <tbody>

                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @include('includes.confirmation_modal')
    {!! Form::open(['
        method'     => 'POST',
        'style'     => 'display:none',
        'id'        =>'resend_confirmation_form']) !!}
        <input type="hidden" name="order_id" />
    {!! Form::close() !!}
@endsection

@section('javascripts')
    @parent

    <script>

        (function($) {

            $('#table-orders').DataTable({
                "stateSave": true,
                "pageLength": 50,
                "processing": true,
                "serverSide": true,
                "order": [[ 1 , "desc"]],
                "ajax": "/admin/orders/index_data",
                "columns": [
                    { "data": "is_archived" },
                    { data: 'completed_formatted', name: 'completed_at' },
                    { data: 'trans_id', name: 'paymentTransactions.transaction_id' },
                    { data: 'name', name: 'user.contact.first_name' },
                    { "data": "user.contact.company" },
                    { "data": "user.email" },
                    { "data": "total" },
                    { "data": "status" },
                    { "data": "action" }
                ],
                "columnDefs": [
                    { "searchable": false,
                        "orderable": false,
                        "targets": [0],
                        "sortable": false
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [1],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": false,
                        "targets": [2],
                        "sortable": false
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [3],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [4],
                        "sortable": true
                    },
                    { "searchable": true,
                        "orderable": true,
                        "targets": [5],
                        "sortable": true
                    },
                    { "searchable": false,
                        "orderable": true,
                        "targets": [6],
                        "sortable": true
                    },
                    { "searchable": false,
                        "orderable": true,
                        "targets": [7],
                        "sortable": true
                    },
                    { "searchable": false,
                        "orderable": false,
                        "targets": [8],
                        "sortable": false
                    }

                ],
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
                    if ( aData['is_archived_value'] == "1" )
                    {
                        $(nRow).addClass('archived');
                    }
                }
            });

            $('#table-orders').on('change', '.is_archived_box', function (e) {
                e.preventDefault();
                var $that = $(this),
                    archive = $(this).is(':checked'),
                    order_id = $(this).data('order_id');
                $.post('/admin/orders/'+order_id, {
                    "archive" : archive
                }, function (data) {
                    if(data.result == 'archived') {
                        $that.closest('tr').addClass('archived');
                    } else {
                        $that.closest('tr').removeClass('archived');
                    }
                });
            });

            $('#table-orders').on('click', '.resend_confirmation', function (e) {

                var url = $(this).data('url'),
                    $form = $('#resend_confirmation_form');

                e.preventDefault();

                Modal.showConfirmationModal(
                    'Are you sure you want to resend the confirmation email?',
                    'Resend Confirmation Email',
                    function() {
                        $form.attr('action', url);
                        $form.submit();
                        return false;
                    });

                return false;

            });

            $(document).ready(function() {
                $('.dataTables_filter').find('input.form-control').qtip({
                    show: 'focus',
                    hide: 'blur',
                    style: {
                        classes: 'qtip-bootstrap'
                    },
                    position: {
                        my: 'top right',
                        at: 'bottom right'
                    },
                    content: {
                        title: {
                            text: 'Search Filter Examples'
                        },
                        text: "<em>* transactions for Sept 2017</em> - <strong>\"2017-09\"</strong>"
                    }
                });
            });

        })(jQuery);

    </script>
@endsection

@section('stylesheets')
    @parent

    <style>
        #table-orders tr.archived {
            color: #BBB;
        }
    </style>
@endsection
