@extends('admin.layout')

@section('body-class', 'index')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _posts = {!! $posts !!};
    data.posts = _posts;
</script>

@if (Session::get('success_message'))
    <div class="alert alert-success alert-dismissible box box-success box-solid" role="alert"
         style=" width: 95%; margin: 0 auto; position: relative; top: 5px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        {{ Session::get('success_message') }}
    </div>
@endif

<div class="container-fluid">
    <div ng-controller="AdminPosts">
        <section class="content-header">
            <h1>Static Pages</h1>
            
            <ol class="breadcrumb">
                <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            </ol>
        </section>

        <section>
            <h2>Main Pages</h2>
            <section ng-repeat="(order, _post) in posts[null]"
                     ng-include src="'admin/posts/index/post.html'" 
                     ng-controller="AdminPostsPost" ng-init="initPost(order, _post)">
            </section>
            <hr>
            <h2>Training</h2>
            <section ng-repeat="(order, _post) in posts[1]"
                     ng-include src="'admin/posts/index/post.html'" 
                     ng-controller="AdminPostsPost" ng-init="initPost(order, _post)">
            </section>
            <hr>
            <h2>Consulting</h2>
            <section ng-repeat="(order, _post) in posts[2]"
                     ng-include src="'admin/posts/index/post.html'" 
                     ng-controller="AdminPostsPost" ng-init="initPost(order, _post)">
            </section>
            <hr>
            <h2>FAQ</h2>
            <section ng-repeat="(order, _post) in posts[4]"
                     ng-include src="'admin/posts/index/post.html'"
                     ng-controller="AdminPostsPost" ng-init="initPost(order, _post)">
            </section>
            <hr/>

            <h2>Resources</h2>
            <section ng-repeat="(order, _post) in posts[3]"
                     ng-include src="'admin/posts/index/post.html'" 
                     ng-controller="AdminPostsPost" ng-init="initPost(order, _post)">
            </section>

        </section>
        <script type="text/ng-template" id="admin/posts/index/post/save-success.html">
            <div class="ui-notification custom-template">
                @{{ post.name }} <em>successfully saved</em>.
            </div>
        </script>
        <script type="text/ng-template" id="admin/posts/index/post/save-error.html">
            <div class="ui-notification custom-template">
                @{{ post.name }} <em>was not saved</em>.
            </div>
        </script>
        <script type="text/ng-template" id="admin/posts/index/post.html">
        <div>

            <div class="pull-right">
                <button
                    class="btn btn-primary"
                    ng-disabled="!dirty"
                    ng-click="savePost()">
                    Save</button>
            </div>
            <h2>@{{ post.name }}</h2>
            <h3>/@{{ post.url }} <button class="btn btn-default" ng-click="preview()">
                View Post
            </button></h3>
            <textarea ui-tinymce="getTinymceOptions()" ng-model="post.content"></textarea>
        </div>
        </script>

    </div>
</div>
@endsection


