@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>@if (isset($class)) Edit Class @else Add New Class @endif</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.courses') }}">Courses</a></li>
            <li><a href="{{ route('admin.classes', [ $class->course->id]) }}">{{ $class->course->title }}</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                @include('admin.includes.success_message')
                @include('admin.includes.error_message')
                @include('admin.includes.form_errors')
                <h3 style="padding-left: 15px">
                    {{ $course->title }}
                    <small class="pull-right">
                        <a href="{{route('admin.certificates.show', ['class_id' => $class->id])}}"
                           class="btn btn-xs btn-primary btn-flat">View Class</a>
                        <a href="{{route('admin.course.edit', [$course->id])}}"
                           class="btn btn-xs btn-default btn-flat">View Course</a>
                    </small>
                </h3>
                <hr/>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-md-7">

                        {!! Form::open(['method' => 'POST', 'url' => route('admin.class.add'), 'id' => 'classForm']) !!}
                        <div class="modal-body">
                            @if (isset($class))
                                {!! Form::input('hidden', 'id', $class->id) !!}
                            @endif

                            {!! Form::input('hidden', 'course_id', $course->id) !!}
                            {!! Form::input('hidden', 'course_type', $course->courseType->id) !!}


                            {!! Form::rLabel('start_date', 'Start Date: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::input('text', 'start_date', isset($class) ? date('F d, Y', strtotime($class->start_date)) : '', [
                                    'class' => 'form-control',
                                    'id'    => 'startDate',
                                    'style' => 'display: inline;',
                                    'placeholder' => 'MM dd, yyyy',
                                    'required' => 'required',
                                ]) !!}
                            </div>

                            {!! Form::rLabel('start_time', 'Start Time: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="bootstrap-timepicker form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {!! Form::input('text', 'start_time', isset($class) ? $class->start->format('g:i a') : '', [
                                    'class' => 'form-control',
                                    'id'    => 'startTime',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('end_date', 'End Date: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::input('text', 'end_date', isset($class) ? date('F d, Y', strtotime($class->end_date)) : '', [
                                    'class' => 'form-control',
                                    'id'    => 'endDate',
                                    'style' => 'display: inline;',
                                    'placeholder' => 'MM dd, yyyy',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('end_time', 'End Time: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="bootstrap-timepicker form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {!! Form::input('text', 'end_time', isset($class) ? $class->end->format('g:i a') : '', [
                                    'class' => 'form-control',
                                    'id'    => 'endTime',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::label('instructor_name', 'Instructor: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <select name="instructor_id" class="form-control col-md-9 instructor">
                                    <option value="">-- Select Instructor --</option>
                                    @foreach ($instructors as $instructor)
                                        <option value="{{ $instructor->id }}"
                                                @if (isset($class) && $class->instructor && $class->instructor->id == $instructor->id) selected @endif>
                                            {{ $instructor->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($isSeminar)

                                {!! Form::label('location', 'Location: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="form-group col-md-9 input-group">
                                    <select name="location_id" class="form-control col-md-9 location">
                                        <option value="">-- Select Location --</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}"
                                                    @if (isset($class) && $class->location && $class->location->id == $location->id) selected @endif>
                                                {{ $location->location_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            @endif

                            {!! Form::label('instructions', 'Instructions: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                {!! Form::textarea('instructions',  isset($class) ? $class->instructions : '', [
                                    'class' => 'form-control col-md-9',
                                    'style' => 'display: inline;'
                                ]) !!}
                            </div>

                            {!! Form::label('is_private', 'Is Private: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                {!! Form::select('is_private', array('1' => 'Yes', '0' => 'No'), $class->is_private,                     ['class' => 'form-control',
                                ]) !!}
                            </div>

                            <div style="text-align: right;">
                                <a href="{{ route('admin.classes', [$class->course->id]) }}" class="btn btn-default btn-flat">Back</a>
                                <button type="submit" class="btn btn-primary btn-flat">Save</button>
                            </div>
                        </div>

                        {!! Form::close() !!}


                    </div>
                    @if(isset($class))
                        <div class="col-md-5 col-xs-12" id="relatedItems">
                            <h4>Related Classes&nbsp;
                                <i class="fa fa-info-circle"
                                   id="related-class-info"
                                   data-tooltip="Classes with same location and close start date are automatically linked."></i>
                                <button type="button" class="pull-right btn btn-info btn-flat btn-xs" data-toggle="modal" data-target="#classModal">
                                    <i class="fa fa-file"></i>&nbsp;&nbsp;Associate Class
                                </button></h4>
                            <table class="table">

                                <tbody>
                                @foreach($class->classes()->withPivot('auto')->get() as $c)
                                    <tr>
                                        <td>
                                            {!! ($c->pivot->auto) ? "<i class='fa fa-bolt' title='Auto Associated'></i>" : '' !!}
                                            &nbsp;
                                            {!! $c->titleCodeLink !!}
                                        </td>
                                        <td><a href="#"
                                               title="Unlink"
                                               data-id="{{ $c->id }}"
                                               data-type="COURSE"
                                               class="unlinkItem btn btn-xs btn-warning"><i class="fa fa-unlink"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        @if($class->course->isWebcast())
                        <div class="col-md-5 col-xs-12">
                            <h4>Webcast Instruction</h4>
                            <div class="well">
                                @if($class->webcast_notification_flag)
                                    Webcast Instructions will be sent automatically on
                                    <strong>{{ $class->start->copy()->subDays(7)->format('F d, Y') }}</strong>
                                @else
                                    Webcast Instructions is set to <strong>Manual</strong> Sending. Click
                                    <strong><a href="{{ route('admin.certificates.show', ['class_id' => $class->id]) }}">HERE</a></strong>
                                    to send out the instructions manually.
                                @endif
                            </div>
                            {!! Form::open(['method' => 'POST',
                                            'url' => route('admin.classes.update', [
                                                'id'    => $class->id
                                            ]),
                                            'id' => 'modify-notification-flag']) !!}
                            {!! method_field('patch') !!}
                            @if($class->webcast_notification_flag)
                                <a class="btn btn-primary"
                                   id="manual-flag-btn"
                                   data-msg="Are you sure you want to send notifications for this webcast manually?"
                                   href="#"
                                   data-flag="0">Switch to Manual</a>
                            @else
                                <a class="btn btn-primary"
                                   id="auto-flag-btn"
                                   data-msg="Are you sure you want to send notifications for this webcast automatically 7 days before the class starts?"
                                   href="#"
                                   data-flag="1">Switch to Automatic</a>
                            @endif
                            {!! Form::close() !!}
                        </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include('includes.confirmation_modal')
    @include('admin.classes._classes')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

            $('#related-class-info').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    attr: 'data-tooltip'
                }
            });

            $('#startTime').timepicker({
                showInputs : false
            });

            $('#endTime').timepicker({
                showInputs : false
            });

            $("#startDate").datepicker({
                format: 'MM dd, yyyy',
                startDate: 'today',
                autoclose: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#endDate').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#endDate').datepicker('setStartDate', null);
            });

            $("#endDate").datepicker({
                format: 'MM dd, yyyy',
                startDate: 'today',
                autoclose: true,
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#startDate').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#startDate').datepicker('setEndDate', null);
            });

            $('#classForm .instructor').select2({
                placeholder: "Select Instructor"
            });

            $('#classForm .location').select2({
                placeholder: "Select Location"
            });

            $('#classForm .state').select2({
                placeholder: "Select State"
            });

            $('#classList').DataTable({
                "columnDefs": [
                    { "searchable": false, "orderable": false, "targets": 2 }
                ]
            });

            var     $associateForm = $("#associateForm"),
                    $unlinkForm = $("#unlinkForm");

            var linkItem = function($that) {
                $associateForm.find("input[name='item_id']").val($that.data('id'));
                $associateForm.find("input[name='item_type']").val($that.data('type'));
                $associateForm.submit();
            }

            var unLinkItem = function($that) {
                $unlinkForm.find("input[name='item_id']").val($that.data('id'));
                $unlinkForm.find("input[name='item_type']").val($that.data('type'));
                $unlinkForm.submit();
            }

            $('#relatedItems').on('click', '.unlinkItem', function(e) {
                e.preventDefault();
                var $that = $(this), msg;
                msg = "Are you sure you want to unlink this class?";
                Modal.showConfirmationModal(
                        msg,
                        'Unlink Item',
                        function() {
                            unLinkItem($that);
                            return false;
                        });
            });

            $('#classList').on('click', '.associateItem', function(e) {
                e.preventDefault();
                var $that = $(this),
                        modalTitle = 'Associate Item With Class',
                        msg = '';

                msg = "Are you sure you want to link this item with this class?";
                msg += "<br/>";
                msg += '"' + $that.data('title') + '"';

                $('#classModal').modal('hide').one('hidden.bs.modal', function () {
                    Modal.showConfirmationModal(
                            msg,
                            modalTitle,
                            function() {
                                linkItem($that);
                                return false;
                            });
                });

                return false;
            });

            $('#manual-flag-btn, #auto-flag-btn').click(function (e) {
                e.preventDefault();
                var msg = $(this).data('msg'),
                    flag = $(this).data('flag');
                Modal.showConfirmationModal(
                    msg,
                    'Toggle Webcast Notification Schedule',
                    function() {

                        var $modify = $('#modify-notification-flag');
                        $modify.find("input[name='flag']").val(flag);
                        $modify.submit();

                        return false;
                    });
            });

        })(jQuery);
    </script>
@endsection