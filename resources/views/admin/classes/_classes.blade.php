<div class="modal fade" id="classModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Classes</h4>
            </div>
            <div class="modal-body">
                <table class="table" id="classList">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($classes as $c)
                        <tr>
                            <td>{{$c->startFormatted}}</td>
                            <td>{{ $c->titleCode }}</td>
                            <td><a href="#"
                                   data-id="{{ $c->id }}"
                                   data-title="{{ $c->titleCode }}"
                                   class="associateItem">Link</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{!! Form::open(['route'         => 'admin.classes.link',
                    'method'        => 'post',
                    'id'            => 'associateForm']) !!}
{!! Form::hidden('class_id', $class->id) !!}
{!! Form::hidden('item_id', 0) !!}
{!! Form::hidden('item_type', '') !!}
{!! Form::close() !!}

{!! Form::open(['route'         => 'admin.classes.unlink',
                'method'        => 'post',
                'id'            => 'unlinkForm']) !!}
{!! Form::hidden('class_id', $class->id) !!}
{!! Form::hidden('item_id', 0) !!}
{!! Form::hidden('item_type', '') !!}
{!! Form::close() !!}