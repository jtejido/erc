@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>@if (isset($class)) Edit Class @else Add New Class @endif</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.courses') }}">Courses</a></li>
            <li>Add Class</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-header">
                @include('admin.includes.success_message')
                @include('admin.includes.form_errors')
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12 col-md-7">

                        {!! Form::open(['method' => 'POST', 'url' => route('admin.classes.store'), 'id' => 'classForm']) !!}
                        <div class="modal-body">
                            @if (isset($class))
                                {!! Form::input('hidden', 'id', $class->id) !!}
                            @endif
                            @if (isset($course))
                                {!! Form::input('hidden', 'course_id', $course->id) !!}
                                {!! Form::input('hidden', 'course_type', $course->courseType->id) !!}
                            @endif


                            {!! Form::rLabel('start_date', 'Start Date: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::input('text', 'start_date', isset($class) ? date('F d, Y', strtotime($class->start_date)) : '', [
                                    'class' => 'form-control',
                                    'id'    => 'startDate',
                                    'style' => 'display: inline;',
                                    'placeholder' => 'MM dd, yyyy',
                                    'required' => 'required',
                                ]) !!}
                            </div>

                            {!! Form::rLabel('start_time', 'Start Time: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="bootstrap-timepicker form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {!! Form::input('text', 'start_time', isset($class) ? $class->start->format('g:i a') : '', [
                                    'class' => 'form-control',
                                    'id'    => 'startTime',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('end_date', 'End Date: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::input('text', 'end_date', isset($class) ? date('F d, Y', strtotime($class->end_date)) : '', [
                                    'class' => 'form-control',
                                    'id'    => 'endDate',
                                    'style' => 'display: inline;',
                                    'placeholder' => 'MM dd, yyyy',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::rLabel('end_time', 'End Time: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="bootstrap-timepicker form-group col-md-9 input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                                {!! Form::input('text', 'end_time', isset($class) ? $class->end->format('g:i a') : '', [
                                    'class' => 'form-control',
                                    'id'    => 'endTime',
                                    'style' => 'display: inline;',
                                    'required' => 'required'
                                ]) !!}
                            </div>

                            {!! Form::label('instructor_name', 'Instructor: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                <select name="instructor_id" class="form-control col-md-9 instructor">
                                    <option value="">-- Select Instructor --</option>
                                    @foreach ($instructors as $instructor)
                                        <option value="{{ $instructor->id }}"
                                                @if (isset($class) && $class->instructor && $class->instructor->id == $instructor->id) selected @endif>
                                            {{ $instructor->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            @if ($course->isSeminar())

                                {!! Form::label('location', 'Location: ', ['class' => 'col-sm-3 control-label']) !!}
                                <div class="form-group col-md-9 input-group">
                                    <select name="location_id" class="form-control col-md-9 location">
                                        <option value="">-- Select Location --</option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}"
                                                    @if (isset($class) && $class->location && $class->location->id == $location->id) selected @endif>
                                                {{ $location->location_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            @endif

                            {!! Form::label('instructions', 'Instructions: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                {!! Form::textarea('instructions',  isset($class) ? $class->instructions : '', [
                                    'class' => 'form-control col-md-9',
                                    'style' => 'display: inline;'
                                ]) !!}
                            </div>

                            {!! Form::label('is_private', 'Is Private: ', ['class' => 'col-sm-3 control-label']) !!}
                            <div class="form-group col-md-9 input-group">
                                {!! Form::select('is_private', array('1' => 'Yes', '0' => 'No'), 0,                     ['class' => 'form-control',
                                ]) !!}
                            </div>

                            <div style="text-align: right;">
                                <a href="{{ route('admin.classes', [$course->id]) }}" class="btn btn-default btn-flat">Back</a>
                                <button type="submit" class="btn btn-primary btn-flat">Save</button>
                            </div>
                        </div>

                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

            $('#related-class-info').qtip({
                style: {
                    classes: 'qtip-bootstrap'
                },
                content: {
                    attr: 'data-tooltip'
                }
            });

            $('#startTime').timepicker({
                showInputs : false
            });

            $('#endTime').timepicker({
                showInputs : false
            });

            $("#startDate").datepicker({
                format: 'MM dd, yyyy',
                autoclose: true,
            }).on('changeDate', function (selected) {
                var startDate = new Date(selected.date.valueOf());
                $('#endDate').datepicker('setStartDate', startDate);
            }).on('clearDate', function (selected) {
                $('#endDate').datepicker('setStartDate', null);
            });

            $("#endDate").datepicker({
                format: 'MM dd, yyyy',
                autoclose: true,
                orientation: 'bottom',
            }).on('changeDate', function (selected) {
                var endDate = new Date(selected.date.valueOf());
                $('#startDate').datepicker('setEndDate', endDate);
            }).on('clearDate', function (selected) {
                $('#startDate').datepicker('setEndDate', null);
            });

            $('#classForm .instructor').select2({
                placeholder: "Select Instructor"
            });

            $('#classForm .location').select2({
                placeholder: "Select Location"
            });

            $('#classForm .state').select2({
                placeholder: "Select State"
            });



        })(jQuery);
    </script>
@endsection