@extends('admin_layout')

@section('stylesheets')
    @parent

    {!! HTML::style(elixir('css/admin.css')) !!}
    {!! HTML::style(elixir('css/admin-lte.css')) !!}
    {!! HTML::style(elixir('css/admin-lte-2.css')) !!}
    {!! HTML::script(elixir('js/admin/tinymce/tinymce.js')) !!}
    {!! HTML::script(elixir('js/admin/angular.js')) !!}
    {!! HTML::script(elixir('js/admin/angular/app.js')) !!}
@endsection

@section('body')

    <div class="wrapper">

        <header class="main-header">
            <a href="" class="logo">
                <span class="logo-lg"><b>ERCWeb</b></span>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">

                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a>{{ Auth::user()->email }}</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">


                <ul class="sidebar-menu">
                    <li class="treeview @if ($page == 'dashboard') active @endif">
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="fa fa-users"></i> <span>Customers</span>
                        </a>
                    </li>

                    <li class="treeview @if ($page == 'orders') active @endif">
                        <a href="{{ route('admin.orders') }}">
                            <i class="fa fa-history"></i> <span>Order History</span>
                        </a>
                    </li>

                    <li class="treeview @if ($page == 'courses' || $page == 'add_course' || $page == 'edit_course' || $page == 'classes' || $page == 'topics' || $page == 'history') active @endif">
                        <a href="#">
                            <i class="fa fa-files-o"></i>
                            <span>Courses</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'courses') active @endif">
                                <a href="{{ route('admin.courses') }}"><i class="fa fa-list"></i> List</a>
                            </li>
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <li class="@if ($page == 'add_course') active @endif">
                                <a href="{{ route('admin.course.add') }}"><i class="fa fa-plus-circle"></i> Add Course</a>
                            </li>
                            <li class="@if ($page == 'topics') active @endif">
                                <a href="{{ route('admin.topics.index') }}"><i class="fa fa-circle-o"></i> Topics</a>
                            </li>
                            <li class="@if ($page == 'history') active @endif">
                                <a href="{{ route('admin.reminder-history.index') }}"><i class="fa fa-envelope-o"></i> Reminder History</a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    @inject('classService', 'App\Services\ClassService')
                    <li class="treeview @if ($page == 'certificates' || $page == 'certificates_show') active @endif">
                        <a href="{{ route('admin.certificates.index') }}">
                            <i class="fa fa-graduation-cap"></i> <span>Class Completions</span>
                        </a>
                    </li>

                    <li class="treeview @if ($page == 'coupons' || $page == 'add_coupon' || $page == 'coupon_landing') active @endif">
                        <a href="#">
                            <i class="fa fa-th"></i>
                            <span>Coupons</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'coupons') active @endif">
                                <a href="{{ route('admin.coupons') }}"><i class="fa fa-circle-o"></i> List Coupons</a>
                            </li>
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <li class="@if ($page == 'add_coupon') active @endif">
                                <a href="{{ route('admin.coupon.add') }}"><i class="fa fa-circle-o"></i> Add Coupon</a>
                            </li>
                            @endif
                            <li class="@if ($page == 'coupon_landing') active @endif">
                                <a href="{{ route('admin.coupon_landing.index') }}"><i class="fa fa-circle-o"></i> Landing Pages</a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview @if ($page == 'books_index' ||
                                            $page == 'books_edit' ||
                                            $page == 'books_create' ||
                                            $page == 'books_show') active @endif">
                        <a href="#">
                            <i class="fa fa-book"></i>
                            <span>Books</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="treeview @if ($page == 'books_index' ||
                                            $page == 'books_edit' ||
                                            $page == 'books_show') active @endif">
                                <a href="{{ route('admin.books.index') }}"><i class="fa fa-circle-o"></i> List</a>
                            </li>
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <li class="@if ($page == 'books_create') active @endif">
                                <a href="{{ route('admin.books.create') }}"><i class="fa fa-circle-o"></i> Add Book</a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="treeview @if ($page == 'instructors' or $page == 'add_instructor' or $page == 'edit_instructor')
                            active @endif">
                        <a href="#">
                            <i class="fa fa-group"></i>
                            <span>Instructors</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'instructors') active @endif">
                                <a href="{{ route('instructor.index') }}"><i class="fa fa-circle-o"></i> List</a>
                            </li>
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <li class="@if ($page == 'add_instructor') active @endif">
                                <a href="{{ route('instructor.create') }}"><i class="fa fa-circle-o"></i> Add Instructor</a>
                            </li>
                            @endif
                        </ul>
                    </li>



                    <li class="treeview @if ($page == 'locations' or $page == 'general_locations') active @endif">
                        <a href="#">
                            <i class="fa fa-map"></i>
                            <span>Locations</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'locations') active @endif">
                                <a href="{{ route('admin.locations.index') }}">
                                    <i class="fa fa-circle-o"></i> <span>Specific Locations</span>
                                </a>
                            </li>
                            <li class="@if ($page == 'general_locations') active @endif">
                                <a href="{{ route('admin.general_locations.index') }}">
                                    <i class="fa fa-circle-o"></i> <span>General City</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))

                    <li class="treeview @if ($page == 'tips' or $page == 'add_tip' or $page == 'edit_tip'
                                    or $page == 'regulations' || $page == 'add_regulation' || $page == 'edit_regulation'
                                    or $page == 'posts' || $page == 'add_post' || $page == 'edit_post'
                                    or $page == 'state_handouts') active @endif">
                        <a href="#">
                            <i class="fa fa-pencil-square-o"></i>
                            <span>Resources</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="treeview @if ($page == 'tips' || $page == 'add_tip' || $page == 'edit_tip') active @endif">
                                <a href="{{ route('admin.tips.index') }}">
                                    <i class="fa fa-calendar-plus-o"></i> <span>Tips of the Week</span>
                                </a>
                            </li>
                            <li class="treeview @if ($page == 'regulations' || $page == 'add_regulation' || $page == 'edit_regulation') active @endif">
                                <a href="{{ route('admin.regulations.index') }}">
                                    <i class="fa fa-bullhorn"></i> <span>Reg of the Day</span>
                                </a>
                            </li>
                            <li class="treeview @if ($page == 'posts' || $page == 'add_post' || $page == 'edit_post') active @endif">
                                <a href="{{ route('admin.posts.index') }}">
                                    <i class="fa fa-file-text-o"></i> <span>Static Pages</span>
                                </a>
                            </li>
                            <li class="treeview @if ($page == 'state_handouts') active @endif">
                                <a href="{{ route('admin.state_handouts.index') }}">
                                    <i class="fa fa-file-pdf-o"></i> <span>State Handouts</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="treeview @if ($page == 'emails' || $page == 'add_email' || $page == 'edit_email') active @endif">
                        <a href="{{ route('admin.email.index') }}">
                            <i class="fa fa-envelope-o"></i> <span>Email Management</span>
                        </a>
                    </li>


                    @endif

                    <li class="treeview @if ($page == 'reports_class' or $page == 'reports_coupon')
                            active @endif">
                        <a href="#">
                            <i class="fa fa-list-alt"></i>
                            <span>Reports</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'reports_class') active @endif">
                                <a href="{{ route('reports.class') }}"><i class="fa fa-circle-o"></i> Class Reports</a>
                            </li>
                            <li class="@if ($page == 'reports_coupon') active @endif">
                                <a href="{{ route('reports.coupon') }}"><i class="fa fa-circle-o"></i> Coupon Reports</a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview @if ($page == 'promos') active @endif">
                        <a href="{{ route('promos.index') }}">
                            <i class="fa fa-money"></i> <span>Registration Promo</span>
                        </a>
                    </li>

                    <li class="treeview @if ($page == 'my_profile') active @endif">
                        <a href="{{ route('admin.profile') }}">
                            <i class="fa fa-bars"></i> <span>My Profile</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="/">
                            <i class="fa fa-share"></i> <span>Frontend</span>
                        </a>
                    </li>

                    <li class="treeview @if ($page == 'users' || $page == 'add_user' || $page == 'edit_user') active @endif">
                        <a href="#">
                            <i class="fa fa-user-secret"></i>
                            <span>Users</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="@if ($page == 'users') active @endif">
                                <a href="{{ route('admin.users') }}"><i class="fa fa-circle-o"></i> List</a>
                            </li>
                            @if (has_role([\App\Utilities\Constant::ROLE_ADMIN]))
                            <li class="@if ($page == 'add_user') active @endif">
                                <a href="{{ route('admin.user.add') }}"><i class="fa fa-circle-o"></i> Add User</a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="{{ url('logout') }}">
                            <i class="fa fa-sign-out"></i> <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>

        <div class="content-wrapper">
            @yield('content')
        </div>

        <div class="control-sidebar-bg"></div>

        <footer class="main-footer no-print main-footer-blend"></footer>
    </div>
@endsection

@section('javascripts')
    @parent
    {!! HTML::script(elixir('js/admin/admin-lte.js')) !!}
    {!! HTML::script(elixir('js/admin/admin.js')) !!}
@endsection