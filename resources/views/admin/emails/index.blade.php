@extends('admin.layout')

@section('body-class', 'index')

@section('content')
<script type="text/javascript">
    if (typeof data == 'undefined') {
        window.data = {};
    }
    var _emails = {!! $emails->toJson() !!};
    data.emails = _emails;
</script>
<div class="container-fluid" ng-controller="AdminEmails">
    <section class="content-header">
        <h1>Email Management</h1>
        <form class="@if(env('EMAIL_SIMULATE_ENABLED', 0)) ng-show @else ng-hide @endif">
            <div class="checkbox">
                <label><input type="checkbox" ng-model="simulation.enabled">Simulation Mode</label>
            </div>
            <div class="form-group">
                <label ng-show="simulation.enabled">Email Address: <input type="email" ng-model="simulation.email" class="form-control"></label>
            </div>
        </form>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>

    <section class="content">

            <section ng-if="emails['ua-register']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['ua-register'])"
                     onload="email_template = emails['ua-register']">
            </section>

            <section ng-if="emails['ua-forgot-password']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['ua-forgot-password'])"
                     onload="email_template = emails['ua-forgot-password']">
            </section>

            <section ng-if="emails['ua-profile-update']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['ua-profile-update'])"
                     onload="email_template = emails['ua-profile-update']">
            </section>

            <section ng-if="emails['reg-seminars-confirmation-attendee']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['reg-seminars-confirmation-attendee'])"
                     onload="email_template = emails['reg-seminars-confirmation-attendee']">
            </section>

        <section ng-if="emails['order-confirmation']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['order-confirmation'])"
                 onload="email_template = emails['order-confirmation']">
        </section>

        <section ng-if="emails['class-instruction']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['class-instruction'])"
                 onload="email_template = emails['class-instruction']">
        </section>

            <section ng-if="emails['reg-training-swap-user']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['reg-training-swap-user'])"
                     onload="email_template = emails['reg-training-swap-user']">
            </section>

            <section ng-if="emails['ua-contact-add']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['ua-contact-add'])"
                     onload="email_template = emails['ua-contact-add']">
            </section>

            <section ng-if="emails['reg-onsite-confirmation']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['reg-onsite-confirmation'])"
                     onload="email_template = emails['reg-onsite-confirmation']">
            </section>

        <section ng-if="emails['reg-onsite-update']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['reg-onsite-update'])"
                 onload="email_template = emails['reg-onsite-update']">
        </section>

            <section ng-if="emails['reg-cancellation']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['reg-cancellation'])"
                     onload="email_template = emails['reg-cancellation']">
            </section>

            <section ng-if="emails['course-update']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['course-update'])"
                     onload="email_template = emails['course-update']">
            </section>

            <section ng-if="emails['course-mill-account-create']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['course-mill-account-create'])"
                     onload="email_template = emails['course-mill-account-create']">
            </section>

            <section ng-if="emails['order-cancel-agent']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['order-cancel-agent'])"
                     onload="email_template = emails['order-cancel-agent']">
            </section>

            <section ng-if="emails['yearly-subscription-purchase']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['yearly-subscription-purchase'])"
                     onload="email_template = emails['yearly-subscription-purchase']">
            </section>

            <section ng-if="emails['corp-seat-subscription-purchase']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['corp-seat-subscription-purchase'])"
                     onload="email_template = emails['corp-seat-subscription-purchase']">
            </section>

            <section ng-if="emails['notify-swap-agent']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['notify-swap-agent'])"
                     onload="email_template = emails['notify-swap-agent']">
            </section>

            <section ng-if="emails['webcast-instruction']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['webcast-instruction'])"
                     onload="email_template = emails['webcast-instruction']">
            </section>

            <section ng-if="emails['attendance-confirmation']"
                     ng-include src="'admin/emails/index/email.html'"
                     ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['attendance-confirmation'])"
                     onload="email_template = emails['attendance-confirmation']">
            </section>

        <section ng-if="emails['course-material-added']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['course-material-added'])"
                 onload="email_template = emails['course-material-added']">
        </section>

        <section ng-if="emails['subscription-used']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['subscription-used'])"
                 onload="email_template = emails['subscription-used']">
        </section>

        <section ng-if="emails['order-modification-confirmed-agent']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['order-modification-confirmed-agent'])"
                 onload="email_template = emails['order-modification-confirmed-agent']">
        </section>

        <section ng-if="emails['class-registration-cancel-agent']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['class-registration-cancel-agent'])"
                 onload="email_template = emails['class-registration-cancel-agent']">
        </section>

        <section ng-if="emails['cert-reminder']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['cert-reminder'])"
                 onload="email_template = emails['cert-reminder']">
        </section>

        <section ng-if="emails['ua-activation-request']"
                 ng-include src="'admin/emails/index/email.html'"
                 ng-controller="AdminEmailsTemplate" ng-init="initEmailTemplate(emails['ua-activation-request'])"
                 onload="email_template = emails['ua-activation-request']">
        </section>

    </section>

    <script type="text/ng-template" id="admin/emails/index/email.html">
        <div class="box" ng-class="{ 'box-primary': visible }">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ template.description }}</h3>
                <div class="box-tools pull-right">
                    <button
                            class="btn btn-warning"
                            ng-show="visible && simulation.enabled"
                            ng-click="send()">
                            Send</button>
                    <button type="button" class="btn btn-box-tool" 
                        ng-click="show()"
                        ng-hide="template.contents.length == 0">
                        <span ng-if="!visible"><i class="fa fa-plus"></i></span>
                        <span ng-if="visible"><i class="fa fa-minus"></i></span>
                    </button>
                  </div>
            </div>
            <div class="box-body" ng-if="visible">
                <div>
                    
                      <uib-tabset active="active" ng-show="visible">
                        <uib-tab index="$index + 1" ng-repeat="contents in template.contents" active="tab.active" select="select($index)">
                            <uib-tab-heading>
                                Content @{{ $index + 1 }}
                            </uib-tab-heading>
                            <div ng-controller="AdminEmailsTemplateContent"
                                 ng-init="initAdminEmailsTemplateContent($index)">
                                <textarea ui-tinymce="getTinymceOptions()" ng-model="contents.content"></textarea>
                            </div>
                            
                        </uib-tab>
                      </uib-tabset>

                </div>
            </div>
            <div class="box-footer clearfix" ng-if="visible">
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-sm btn-primary btn-flat pull-right"
                            ng-click="addContent()"
                            ng-show="visible || template.contents.length == 0">Add Content</button>
                </div>
            </div>
        </div>
    </script>
    
</div>
@endsection
