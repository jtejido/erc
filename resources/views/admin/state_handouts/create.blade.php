@extends('admin.layout')

@section('content')
    <section class="content-header">
        <h1>Add State Handout</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.state_handouts.index') }}">State Handouts</a></li>
            <li>Add State Handout</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @include('admin.includes.form_errors')
                    </div>

                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-6">
                        {!! Form::open(['url'       => route('admin.state_handouts.store'),
                                        'method'    => 'post',
                                        'files'     => true,
                                        'class'     => 'status-filter']) !!}

                                <div class="form-group">
                                    <label for="state">Type:</label>
                                    {!! Form::select('type', $types, '', [
                                            'class' => 'form-control'
                                        ]) !!}
                                </div>

                                <div class="form-group">
                                    <label for="state">State:</label>
                                    {!! Form::select('state', $states, '', [
                                            'class' => 'form-control'
                                        ]) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('file', 'File', ['class' => '']) !!}
                                    {!! Form::file('file', ['class' => 'form-control']) !!}

                                </div>

                                <a href="{{ route('admin.state_handouts.index') }}" class="btn btn-default"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Back</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                                @if (isset($book->id) && $book->canBeDeleted())
                                    <button type="button" class="btn btn-danger" id="delete_btn" data-book-id="{{ $book->id }}">Delete</button>
                                @endif

                        {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </section>

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        (function($){

        })(jQuery);
    </script>
@endsection
