@extends('admin.layout')
@section('stylesheets')
    @parent
    <style>

    </style>
@endsection
@section('content')
    <section class="content-header">
        <h1>State Handouts</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>State Handouts</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">

                        @include('admin.includes.success_message')

                    </div>

                    <div class="box-body">

                        <div class="row">
                            <div class="col col-xs-8 col-xs-offset-2">
                                <a href="{{ route('admin.state_handouts.create') }}" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> New Handout</a>
                                <hr/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col col-xs-8 col-xs-offset-2">

                                <table class="table table-bordered" id="handout-list">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Type</th>
                                        <th>State</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($handouts as $handout)

                                            <tr>
                                                <td>{{ $handout->id }}</td>
                                                <td>{{ $handout->type_value }}</td>
                                                <td>{{ $handout->state }}</td>
                                                <td>
                                                    <a href="{{ route('admin.course.getmaterial', [$handout->files()->first()->id]) }}" class="btn btn-default" title="Download"><i class="fa fa-download" aria-hidden="true"></i> </a>
                                                    <a href="{{ route('admin.state_handouts.edit', [$handout->id]) }}" class="btn btn-warning" title="Replace"><i class="fa fa-refresh" aria-hidden="true"></i> </a>
                                                    <a href="#" class="btn btn-danger delete" data-handout_id="{{ $handout->id }}" title="Delete"><i class="fa fa-trash" aria-hidden="true"></i> </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                                <br/>
                            </div>


                        </div>

                        {!! Form::open(['method' => 'DELETE', 'id' => 'delete-state-handout', 'url' => route('admin.state_handouts.destroy')]) !!}
                        <input type="hidden" name="state_handout_id" id="state_handout_id" value="0" />
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>


    </section>
    @include('includes.confirmation_modal')

@endsection

@section('javascripts')
    @parent
    <script type="text/javascript">
        $(document).ready(function() {

            $('#handout-list').DataTable({
                paging: false,
                "order": [[ 1, 'asc' ], [ 2, 'asc' ]],
                "columnDefs": [ {
                    "targets": [ 0, 3 ],
                    "orderable": false
                } ]
            });

            $('#handout-list').on('click', '.delete', function(e) {
                e.preventDefault();
                var id = $(this).data('handout_id');
                $('#state_handout_id').val(id);
                Modal.showConfirmationModal(
                    'Are you sure you want to delete this state handout?',
                    'Delete State Handout',
                    function() {
                        $('#delete-state-handout').submit();
                        return false;
                    });
            });
        });
    </script>
@endsection


