(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.controller('AdminLinksAdd',
    function ($scope, $http, $window) {
        _window.scope = $scope;
        if ('link' in $window.data) {
            $scope.link = $window.data.link;
        }

        $scope.categories = $window.data.categories;
        $scope.testdata   = '';
        $scope.data = {};

        $scope.initialize = function(name, data) {
            $scope.data[name] = data;
        };

        $scope.init = function()
        {
            console.log($scope.categories);

            var now = moment();
            $scope.initialize('link', {
                'name': '',
                'url': '',
                'category': $scope.categories[0]
            });
            
            if ($scope.link) {
                $scope.link.category = _.findWhere($scope.categories, {
                    id: $scope.link.category_id
                });

                delete $scope.link.category_id;
                $scope.initialize('link', $scope.link);
            }
            
            console.log($scope.data['link']);
        };

        $scope.save = function(replace) {
            if (!$scope.linksAdd.$valid) {
                $scope.formSubmitInvalid = true;
                return;
            }

            var _data = angular.copy($scope.data['link']);
            if (replace) {
                _data = _.extend(_data, replace);
            }
            _data['category_id'] = _data.category.id;
            delete _data.category;
            
            var errorSave = function(data) {
                jQuery('.bs-callout.bs-callout-danger:hidden').show(100);
                if ($('body').scrollTop() != 0) {
                    $('body').animate({ scrollTop: 0 }, 800);
                }

                $scope.formSubmitError = true;
                $scope.formSubmitErrorMessage = "Error "+data.status + ": " + data.statusText;
                setTimeout(function(){
                    $scope.formSubmitError = false;
                    jQuery('.bs-callout.bs-callout-danger:visible').hide(600);
                }, 1500);
            };

            $http.post('/admin/links/save', _data).then(function(data){
                if (data.status != 200) {
                    errorSave(data);
                    return;
                }

                $scope.formSubmitSuccess = true;
                jQuery('.bs-callout.bs-callout-success').show(100);
                $('body').animate({ scrollTop: 0 }, 800);
                jQuery('.bs-callout.bs-callout-danger:visible').hide(600);
                setTimeout(function(){
                    $window.location.href = $window.location.origin + '/admin/links';
                }, 1500);
            }, errorSave);
        };

        $scope.init();
    });

})(window.ercApp, window);