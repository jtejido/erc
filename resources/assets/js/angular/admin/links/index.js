(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.controller('AdminLinks',
    function ($scope, $http, $window) {
        _window.scope = $scope;
        $scope.tips = $window.data.tips;

        $scope.init = function()
        {
            
        };

        $scope.init();
    });

})(window.ercApp, window);