(function($erc, _window) {
    "use strict";

    $erc.controller('AdminPostsPost',
    function ($scope, $window, $sce, Notification) {

        $scope.post = null;
        $scope.saved = null;
        $scope.dirty = null;

        $scope.initPost = function(order, post)
        {
            $scope.post = post;
            console.log('post',post);
            $scope.saved = angular.copy(post);
        };

        $scope.getTinymceOptions = function()
        {
            var toolbars = [
                // 'newdocument',
                // 'bold',
                // 'italic',
                // 'underline',
                // 'strikethrough',
                'alignleft',
                'aligncenter',
                'alignright',
                'alignjustify',
                // 'styleselect',
                // 'formatselect',
                'fontselect',
                'fontsizeselect',
                // 'cut',
                // 'copy',
                // 'paste',
                'bullist',
                'numlist',
                'outdent',
                'indent',
                'blockquote',
                // 'undo',
                // 'redo',
                'removeformat',
                'subscript',
                'superscript',
                'image',
                'fullscreen',
                'template',
            ];

            toolbars = toolbars.join(', ');

            return angular.copy({
                toolbar: toolbars,
                height: 150,
                plugins: "fullscreen code image",
                automatic_uploads: true,
                file_browser_callback: $scope.RoxyFileBrowser,
                // relative_urls: false,
                // remove_script_host: false,
                setup: function(editor) {
                    editor.on('init', function() {
                        
                    });
                }
            });
        };

        $scope.RoxyFileBrowser = function(field_name, url, type, _win) {
        var roxyFileman = '/fileman/index.html';

            if (roxyFileman.indexOf("?") < 0) {     
                roxyFileman += "?type=" + type;   
            } else {
                roxyFileman += "&type=" + type;
            }
            roxyFileman += '&input=' + field_name + '&value=' + _win.document.getElementById(field_name).value;

            if(tinyMCE.activeEditor.settings.language){
                roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
            }

            tinyMCE.activeEditor.windowManager.open({
                file           : roxyFileman,
                title          : 'Gallery',
                width          : 850, 
                height         : 650,
                resizable      : "yes",
                plugins        : "media",
                inline         : "yes",
                close_previous : "no"  
            }, {
                window: _win,
                input: field_name
            });
            return false; 
        };

        $scope.preview = function() {
            var location = $window.location;
            console.log(location['origin']+'/'+$scope.post.url);
            $window.open(location['origin']+'/'+$scope.post.url);
        };


        $scope.$watch('post', function(changed, original) {
            
            var changed = angular.copy(changed);
            console.log('original', $scope.saved.content);
            console.log('changed', changed.content);
            
            if (changed.content !== $scope.saved.content) {
                $scope.dirty = changed.content;
            } else {
                $scope.dirty = null;
                console.log('back to original')
            }
        }, true);

        $scope.savePost = function() {
            var _message = $scope.post.name+' successfully saved';
            $scope.$emit('save-post', {
                data: $scope.post,
                callback: {
                    success: function(data) {
                        $scope.saved = $scope.dirty;
                        $scope.dirty = null;

                        window.location.reload();
                    },
                    error: function(data) {
                        window.location.reload();
                    }
                }
            });
        };

    });

})(window.ercApp, window);