(function($erc, _window) {

    "use strict";

    /**
     * @controller AccountController
     * @author Adam Timberlake
     * @module Moa
     */
    $erc.controller('AdminPosts',
    function ($scope, $http, $window) {

        _window.scope = $scope;
        $scope.posts = _.groupBy($window.data.posts, 'category_id');
        $scope.init = function()
        {
            _.each($scope.posts, function(posts, category_id) {
                $scope.posts[category_id] = _.indexBy(posts, 'order');
            });
        };

        $scope.$on('save-post', function(event, payload) {
            var post = payload.data;
            var callback = payload.callback;

            $http.post('/admin/posts/save', post).then(callback.success, callback.error);
        });

        $scope.init();
    });

})(window.ercApp, window);