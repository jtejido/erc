(function($erc, _window) {

    "use strict";

    $erc.controller('AdminEmails',
    function ($scope, $http, $window) {

        _window.scope = $scope;
        $scope.emails = _.indexBy($window.data.emails, 'slug');
        $scope.simulation = {
            enabled: false,
            email: 'greg@cspreston.com'
        };

        $scope.init = function()
        {
            //console.log($scope.emails)
        };

        $scope.$on('simulate-email', function(event, data) {
            console.log('simulation request received!', event);
            console.log(data);


            $http.post('/admin/email/simulate', data).then(function(data) {
                console.log('yes!', data);
            }, function(data) {
                console.log('no', data);
            });
        });


        $scope.$on('save-template-content', function(event, payload) {
            var post = payload.data;
            var callback = payload.callback;

            $http.post('/admin/email/content/save', post).then(callback.success, callback.error);
        });


        $scope.init();
    });

})(window.ercApp, window);