(function($erc, _window) {
    "use strict";

    $erc.controller('AdminEmailsTemplateContent',
    function ($scope, $window, Notification) {
        $scope.contents = null;
        $scope.saved = null;
        $scope.dirty = null;

        $scope.initAdminEmailsTemplateContent = function(index) {
            $scope.contents = $scope.$parent.template.contents[index];
            $scope.saved = angular.copy($scope.contents.content);
        };

        $scope.tinymceSaveButton = null;
        $scope.getTinymceOptions = function()
        {
            var toolbars = [
                'SaveButton',
                // 'newdocument',
                // 'bold',
                // 'italic',
                // 'underline',
                // 'strikethrough',
                'alignleft',
                'aligncenter',
                'alignright',
                'alignjustify',
                // 'styleselect',
                // 'formatselect',
                'fontselect',
                'fontsizeselect',
                // 'cut',
                // 'copy',
                // 'paste',
                'bullist',
                'numlist',
                'outdent',
                'indent',
                'blockquote',
                // 'undo',
                // 'redo',
                'removeformat',
                'subscript',
                'superscript',
                'image',
                'fullscreen',
                'template'
            ];

            toolbars = toolbars.join(', ');

            return angular.copy({
                toolbar: toolbars,
                height: 200,
                plugins: "fullscreen code fullpage image template",
                automatic_uploads: true,
                file_browser_callback: $scope.RoxyFileBrowser,
                // relative_urls: false,
                // remove_script_host: false,
                setup: function(editor) {
                    editor.on('init', function() {
                        
                    });
                    editor.addButton('SaveButton', {
                        text: 'Save',
                        icon: false,
                        onPostRender : function() {
                            $scope.tinymceSaveButton = this;
                        },
                        onclick: function () {
                            $scope.saveTemplateContent();

                        }});
                    editor.on('blur', function() {
                        $scope.fixContent(editor.getContent());
                        $scope.tinymceValidate(editor.getContent());
                    });
                    //editor.on('NodeChange', function() {
                    //    $scope.fixContent(editor.getContent());
                    //    $scope.tinymceValidate(editor.getContent());
                    //});
                }
            });
        };

        $scope.fixContent = function(content) {
            var _content = angular.copy(content);
            var _base = _window.location.origin+"/fileman/Uploads/Email/";
            _content = _content.replace(/(url ?\(["']?|src=["'])images\/?(.+)(["']?\)?)/g, "$1"+_base+"$2$3");
            
            $scope.contents.content = _content;
        }


        $scope.tinymceValidate = function(content)
        {
            var _dirty = ($scope.saved !== content);
            $scope.tinymceSaveButton.disabled(!_dirty);

            if (_dirty) {
                $scope.dirty = content;
            } else {
                $scope.dirty = null;
            }
        };


        $scope.saveTemplateContent = function() {
            var content = angular.copy($scope.contents);
            content.content = btoa(content.content);
            $scope.$emit('save-template-content', {
                data: {
                    template_id: $scope.$parent.template.id,
                    content: content
                },
                callback: {
                    success: function(data) {
                        $scope.saved = $scope.dirty;
                        $scope.dirty = null;
                        
                        Notification.success({
                            templateUrl: "save-success.html",
                            message: $scope.$parent.template.description
                        });
                        var _contents = data.data.data;
                        _contents.content = $scope.contents.content;
                        $scope.contents = _contents;
                        $scope.tinymceValidate();
                    },
                    error: function(data) {
                        Notification.error({
                            templateUrl: "save-error.html",
                            message: $scope.$parent.template.description
                        });
                    }
                }
            });
        };

    });

})(window.ercApp, window);