(function($erc, _window) {
    "use strict";

    $erc.controller('AdminEmailsTemplate',
    function ($scope, $window) {
        $scope.template = null;
        $scope.initialized = [];
        $scope.visible = false;
        $scope.active_content_index = 1;
        $scope.saved = null;
        $scope.dirty = null;

        $scope.initEmailTemplate = function(email_template)
        {
            $scope.template = email_template;
            $scope.saved = angular.copy(email_template);
        };

        $scope.select = function($index)
        {
            $scope.active_content_index = $index;
        };

        $scope.send = function()
        {
            $scope.$emit('simulate-email', {
                id      : $scope.template.id,
                content : $scope.template.contents[$scope.active_content_index].content,
                email   : $scope.simulation.email
            });
        };

        $scope.addContent = function()
        {
            if ($scope.template.contents.length == 0) {
                $scope.visible = true;
            }

            $scope.template.contents.push({content: ""});
        };

        $scope.show = function ()
        {
            $scope.visible = !$scope.visible;
            $scope.select(0);
        };

        $scope.RoxyFileBrowser = function(field_name, url, type, _win) {
        var roxyFileman = '/fileman/index.html';

            if (roxyFileman.indexOf("?") < 0) {     
                roxyFileman += "?type=" + type;   
            } else {
                roxyFileman += "&type=" + type;
            }
            roxyFileman += '&input=' + field_name + '&value=' + _win.document.getElementById(field_name).value;

            if(tinyMCE.activeEditor.settings.language){
                roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
            }

            tinyMCE.activeEditor.windowManager.open({
                file           : roxyFileman,
                title          : 'Gallery',
                width          : 850, 
                height         : 650,
                resizable      : "yes",
                plugins        : "media",
                inline         : "yes",
                close_previous : "no"  
            }, {
                window: _win,
                input: field_name
            });
            return false; 
        };

        $scope.saveTemplate = function() {
            $scope.$emit('save-template', {
                data: $scope.template,
                callback: {
                    success: function(data) {
                        $scope.saved = $scope.dirty;
                        $scope.dirty = null;
                        
                        Notification.success({
                            templateUrl: "save-success.html",
                            message: $scope.post.name
                        });
                    },
                    error: function(data) {
                        Notification.error({
                            templateUrl: "save-error.html",
                            message: $scope.post.name
                        });
                    }
                }
            });
        };

    });

})(window.ercApp, window);