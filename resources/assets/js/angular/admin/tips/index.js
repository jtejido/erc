(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.controller('AdminTips',
    function ($scope, $http, $window, NgTableParams) {

        _window.scope = $scope;
        $scope.tips = $window.data.tips;

        $scope.init = function()
        {
            console.log($scope.tips)
        };
        
        $scope.cols = [
            { field: "title", title: "Title", sortable: "title", show: true },
            { field: "published_date", title: "Published Date", sortable: "published_date", show: true },
            { field: "created_at", title: "Created", sortable: "created_at", show: true },
        ];

        $scope.tableParams = new NgTableParams({
            // initial sort order
            sorting: { created_at: "desc" } 
        }, {
            getData: function(params){
                    /* code to fetch data that matches the params values EG: */
                    // return executeQuery(params).then(function(data){
                    //     params.total(data.inlineCount);
                    //     return data.results;
                    // });
                return $scope.tips;
            }
        });
        /*
        this.tableParams = new NgTableParams({}, {
            getData: function(params) {
                return IssueService.query({
                            page: params.page(),
                            per_page: params.count(),

                            state: 'all',
                            username: 'esvit',
                            repo: 'ng-table'
                        }, function(data, headersGetter) {
                            var headers = headersGetter(),
                            pages = headers['link'].split(', '),
                            totalPages = 1;

                            // get total pages count
                            angular.forEach(pages, function(page) {
                            var parts = page.split(' rel=');
                            if (parts[1] == '"last"') {
                                totalPages = parseInt(parts[0].match(/page=(\d+)/)[1], 10);
                            }
                            if (totalPages == 1 && parts[1] == '"prev"') { // if last page
                                totalPages = parseInt(parts[0].match(/page=(\d+)/)[1], 10) + 1;
                            }
                        });
                    params.total(totalPages * params.count());
                    return data;
                }).$promise;
            }
        });
        */

        $scope.init();
    });

})(window.ercApp, window);