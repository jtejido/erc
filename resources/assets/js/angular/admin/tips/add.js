(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.controller('AdminTipsAdd',
    function ($scope, $http, $window) {
        _window.scope = $scope;
        if ('tip' in $window.data) {
            $scope.tip = $window.data.tip;
        }
        $scope.categories = $window.data.categories;
        $scope.months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];
        $scope.testdata = '';
        $scope.data = {};

        $scope.initialize = function(name, data) {
            $scope.data[name] = data;
        };

        $scope.init = function()
        {
            var now = moment();
            $scope.initialize('tip', {
                'title': '',
                'content': '',
                'category': $scope.categories[0],
                'published_date': {
                    'year': now.year(),
                    'month': now.format('MMMM'),
                    'day': now.date()
                },
                'is_published': false
            });
            
            if ($scope.tip) {
                var published_date = moment($scope.tip.published_date.date);
                var published_date = {
                    'year': published_date.year(),
                    'month': published_date.format('MMMM'),
                    'day': published_date.date()
                };
                
                $scope.tip.category = _.findWhere($scope.categories, {
                    id: $scope.tip.category_id
                });
                $scope.tip.published_date = published_date;
                delete $scope.tip.category_id;
                $scope.initialize('tip', $scope.tip);
            }
        };

        $scope.publish = function() {
            $scope.save({is_published: true});
        };

        $scope.delete = function() {

            Modal.showConfirmationModal(
                'Are you sure you want to delete this tip?',
                'Delete Tip',
                function() {
                    $scope.destroy();
                    $('#notificationModal').modal('toggle');
                });
            return false;
        };

        $scope.destroy = function() {
            var _data = angular.copy($scope.data['tip']);
            var errorSave = function(data) {
                jQuery('.callout.callout-danger:hidden').show(100);
                if ($('body').scrollTop() != 0) {
                    $('body').animate({ scrollTop: 0 }, 800);
                }

                $scope.formSubmitError = true;
                $scope.formSubmitErrorMessage = "Error "+data.status + ": " + data.statusText;
                setTimeout(function(){
                    $scope.formSubmitError = false;
                    //jQuery('.callout.callout-danger:visible').hide(600);
                }, 1500);
            };

            $http.post('/admin/tips/destroy', _data).then(function(data){
                if (data.status != 200) {
                    errorSave(data);
                    return;
                }


                $scope.formDestroySuccess = true;
                jQuery('.callout.callout-success').show(100);
                $('body').animate({ scrollTop: 0 }, 800);
                jQuery('.callout.callout-danger:visible').hide(600);
                setTimeout(function(){
                    $window.location.href = $window.location.origin + '/admin/tips';
                }, 1500);
            }, errorSave);
        };

        $scope.save = function(replace) {
            var formData = new FormData();

            if (!$scope.tipsAdd.$valid) {
                $scope.formSubmitInvalid = true;
                return;
            }

            var _data = angular.copy($scope.data['tip']);
            if (replace) {
                _data = _.extend(_data, replace);
            }
            _data['category_id'] = _data.category.id;

            if (_data.id) {
                formData.append('id', _data.id)
            }

            formData.append('title', _data.title);
            formData.append('slug', _data.slug);
            formData.append('content', _data.content);

            if ($('#photo')[0].files[0]) {
                formData.append('photo', $('#photo')[0].files[0]);
            }

            formData.append('category_id', _data.category_id);
            formData.append('is_published', _data.is_published ? 1 : 0);
            formData.append('month', _data.published_date.month);
            formData.append('day', _data.published_date.day);
            formData.append('year', _data.published_date.year);

            delete _data.category;
            
            var errorSave = function(data) {

                var msg = "";
                jQuery('.callout.callout-danger:hidden').show(100);
                if ($('body').scrollTop() != 0) {
                    $('body').animate({ scrollTop: 0 }, 800);
                }

                $scope.formSubmitError = true;

                angular.forEach(data.data, function(value, key) {
                    msg += value[0];
                    msg += "\n";
                }, msg);

                $scope.formSubmitErrorMessage = msg;

                setTimeout(function(){
                    $scope.formSubmitError = false;
                    jQuery('.callout.callout-danger:visible').hide(1200);
                }, 1500);
            };

            $http.post('/admin/tips/save', formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).then(function(data){
                if (data.status != 200) {
                    errorSave(data);
                    return;
                }

                $scope.formSubmitSuccess = true;
                jQuery('.callout.callout-success').show(100);
                $('body').animate({ scrollTop: 0 }, 800);
                jQuery('.callout.callout-danger:visible').hide(600);
                setTimeout(function(){
                    $window.location.href = $window.location.origin + '/admin/tips';
                }, 1500);
            }, errorSave);
        };


        $scope.getTinymceOptions = function()
        {
            var toolbars = [
                'SaveButton',
                // 'newdocument',
                // 'bold',
                // 'italic',
                // 'underline',
                // 'strikethrough',
                'alignleft',
                'aligncenter',
                'alignright',
                'alignjustify',
                // 'styleselect',
                // 'formatselect',
                'fontselect',
                'fontsizeselect',
                // 'cut',
                // 'copy',
                // 'paste',
                'bullist',
                'numlist',
                'outdent',
                'indent',
                'blockquote',
                // 'undo',
                // 'redo',
                'removeformat',
                'subscript',
                'superscript',
                'image',
                'fullscreen',
                'template'
            ];

            toolbars = toolbars.join(', ');

            return angular.copy({
                toolbar: toolbars,
                height: 200,
                plugins: "fullscreen code image",
                automatic_uploads: true,
                // file_browser_callback: $scope.RoxyFileBrowser,
                // relative_urls: false,
                // remove_script_host: false,
                setup: function(editor) {
                    editor.on('init', function() {
                        
                    });
                    // editor.addButton('SaveButton', {
                    //     text: 'Save',
                    //     icon: false,
                    //     onPostRender : function() {
                    //         $scope.tinymceSaveButton = this;
                    //     },
                    //     onclick: function () {
                    //         $scope.saveTemplateContent();

                    //     }});
                    // editor.on('change', function() {
                    //     $scope.fixContent(editor.getContent());
                    //     $scope.tinymceValidate(editor.getContent());
                    // });
                    // editor.on('NodeChange', function() {
                    //     $scope.fixContent(editor.getContent());
                    //     $scope.tinymceValidate(editor.getContent());
                    // });
                }
            });
        };


        $scope.init();
    });

})(window.ercApp, window);