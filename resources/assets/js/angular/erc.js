(function($window, $angular) {

    "use strict";

    /**
     * @property window.moaApp
     * @type {Object}
     */
    $window.ercApp = $angular.module('ercApp', [
        'ngAnimate',
        'ngRoute',
        'ui.bootstrap',
        'ui.tinymce',
        'ui-notification',
        'angularMoment',
    ])
    .config(function(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionY: 'top',
            positionX: 'right',
        });
    })
    .filter('moment', function() {
        return function(string, format) {
            return moment(string).format(format);
        };
    });

})(window, window.angular);