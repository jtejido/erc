(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.filter('current_view_regulations', function() {
        return function(input, selection) {
            var current_view = moment(new Date(selection.month+' 1, '+selection.year));
            return _.map(input, function(v,i,l) {
                
                var date_match = current_view.isSame(v.published_date.date,'month');
                var text_match = false;
                if (selection.text.length > 0) {
                    var pattern = new RegExp(selection.text,'gi');
                    if (pattern.test(v.title)) {
                        text_match = true;
                        console.log(v.title, 'matches', selection.text)
                    }
                } else {
                    text_match = true;
                }

                if (text_match && date_match) {
                    return v;
                }

                return;
                
            });
        };
    });

    $erc.controller('MainRegulations',
    function ($scope, $http, $window, $filter) {
        _window.scope = $scope;
        $scope.regs = $window.data.regs;
        $scope.months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        $scope.years = _.range(2002, new Date().getFullYear() + 1);
        $scope.data = {};

        $scope.active_regs = $scope.regs;

        $scope.refresh_regs = function() {
            $scope.active_regs = $filter('current_view_regulations')(
                $scope.regs,
                $scope.data.current_view
            );
        }

        $scope.initialize = function(name, data) {
            $scope.data[name] = data;
        };

        $scope.init = function()
        {
            if ('regs' in $window.data) {
                $scope.regs = $window.data.regs;
            }

            var now = moment();
            var current_view = {
                year: now.year(),
                month: now.format('MMMM'),
                text: '',
            }
            $scope.initialize('current_view', current_view);

            $scope.refresh_regs();
        };

        $scope.init();
    });

})(window.ercApp, window);