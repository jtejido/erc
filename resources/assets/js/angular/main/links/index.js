(function($erc, _window) {

    "use strict";

    /**
     * @controller
     */
    $erc.filter('current_view_links', function() {
        return function(input, selection) {
            return _.map(input, function(v,i,l) {
                var category_match = (v.category_id == selection.category.id);
                var text_match = false;

                if (selection.category.id === 0) {
                    // All is selected
                    category_match = true;
                }

                if (selection.text.length > 0) {
                    var pattern = new RegExp(selection.text,'gi');
                    if (pattern.test(v.name)) {
                        text_match = true;
                        console.log(v.name, 'matches', selection.text)
                    }
                } else {
                    text_match = true;
                }

                if (text_match && category_match) {
                    return v;
                }

                return;
            });
        };
    });

    $erc.filter('category_view', function() {
        return function(input, category_id) {
            return _.map(input, function(v,i,l) {
                if (v) {
                    var category_match = (v.category_id == category_id);

                    if (category_id === 0) {
                        // All is selected
                        category_match = true;
                    }

                    if (category_match) {
                        return v;
                    }

                    return;
                }
            });
        };
    });

    $erc.controller('MainLinks',
    function ($scope, $http, $window, $filter) {
        _window.scope = $scope;
        $scope.links = $window.data.links;
        $scope.data = {};

        $scope.active_links = $scope.links;

        $scope.refresh_links = function() {
            $scope.active_links = $filter('current_view_links')(
                $scope.links,
                $scope.data.current_view
            );
        }

        $scope.initialize = function(name, data) {
            $scope.data[name] = data;
        };

        $scope.init = function()
        {
            if ('links' in $window.data) {
                $scope.links = $window.data.links;
            }
            if ('categories' in $window.data) {
                var _all = [{id: 0, name: 'All'}];

                $scope.categories = _.union(_all, $window.data.categories);
            }

            var now = moment();
            var current_view = {
                category: $scope.categories[0],
                text: '',
            }
            $scope.initialize('current_view', current_view);

            $scope.refresh_links();
        };

        $scope.init();
    });

})(window.ercApp, window);