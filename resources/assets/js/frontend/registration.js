
$('.remove-item').on('click', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    $('#notificationModal').modal('show');

    $('.notification-ok').on('click', function() {
        $.ajax({
            type : 'POST',
            url  : href,
            data : { id : id },
            success : function(response) {
                if (response) {
                    window.location.reload();
                }
            }
        })
    });
});