(function() {
    var http = location.protocol;
    var slashes = http.concat("//");
    var host = slashes.concat(window.location.hostname);

    window.Casper = {
        initialized: false,
        at: function(path, data) {
            
            if (window.location.hostname === 'ercweb.com') {
                throw new Error('Production testing not allowed');
            }

            if (data) {
                return $.post(host+'/'+path, data);
            }

            return $.get(host+'/'+path);
        }
    };

    _.each(window.postInit, function(postInit) {
        postInit();
    });

    /** Accepts numeric only **/

    $.fn.NumericOnly = function(options) {
        var options = $.extend({
            length : 11
        }, options);

        $(this).keypress(function(e) {
            if ($.trim($(this).val()).length == options.length && (e.which != 8 && e.which != 0)) {

                return false;
            }

            return !((e.which < 48 || e.which > 57) && e.which != 8 && e.which != 0);
        });
    }

    /** Accepts decimal **/

    $.fn.AllowDecimal = function() {

        $(this).keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            if (charCode == 46 && $(this).val().split('.').length > 1) {
                return false;
            }

            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }

            return true;
        });
    }

    $.fn.emailinput = function(options) {

        var settings = $.extend({
            id: 'eai_input', // overlay input text id
            onlyValidValue: true, // set input text valid email address only
            delim: ',' // input text value delimiter
        }, options);

        options = $.extend(settings, options);

        // Is email address validate?
        var getValidation = function(address) {
            var reg = /^([A-Za-z0-9_\-\.\+])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            return reg.test(address);
        };

        // div to real data. If you want email value finally should call this method.
        var setAddresses = function(obj) {
            var addresses = [];
            obj.find('span' + (options.onlyValidValue ? '.ei_valid' : '')).each(function() { addresses.push($(this).data('address')); });
            obj.data('ref').val(addresses.join(options.delim));
        };

        // Add email address box, wrapper is div generated by code
        var addAddress = function(wrapper, val) {

            val = $.trim(val);
            if(val !== '') {
                var validate = getValidation(val);
                wrapper.append(
                    $('<span></span>')
                    .data('address', val)
                    .attr({ 'title': val })
                    .html(val)
                    .addClass('ei_box')
                    .addClass(validate ? 'ei_valid' : 'ei_invalid')
                    .bind('click', function(e) { // click to delete
                        e.stopPropagation();

                        // $(this).remove();
                        setAddresses(wrapper);
                        edit($(this));
                    })
                );

                setAddresses(wrapper);
            }
        };

        var updateAddress = function(wrapper, input) {

            val = $.trim(input.val());
            if (val !== '') {
                var validate = getValidation(val);
                input.before(
                    $('<span></span>')
                    .data('address', val)
                    .attr({ 'title': val })
                    .html(val)
                    .addClass('ei_box')
                    .addClass(validate ? 'ei_valid' : 'ei_invalid')
                    .bind('click', function(e) { // click to delete
                        e.stopPropagation();

                        // $(this).remove();
                        setAddresses(wrapper);
                        edit($(this));
                    })
                );
            }

            setAddresses(wrapper);
        }

        // backspace to delete last address
        var removeLastAddress = function(obj) {
            var wrapper = obj.parent();
            wrapper.find('span:last').last().remove();
            setAddresses(wrapper);
        };

        var edit = function(el) {
            inputOverlay = $('<input />')
                .attr({ 'id': options.id, 'attr': 'text' })
                .addClass('ei_box')
                .css('border', 'none')
                .bind('keydown', function(e) {
                    switch (e.keyCode) {
                        case 13: // enter
                            updateAddress($(this).parent(), $(this));
                            $(this).remove();
                            break;

                        case 27: // esc
                            updateAddress($(this).parent(), $(this).val(el.text()));
                            $(this).remove();
                            break;
                    }
                })
                .bind('blur', function() {
                    updateAddress($(this).parent(), $(this));
                    $(this).remove();
                })
                .val(el.text());

            el.before(inputOverlay);
            el.remove();
            inputOverlay.focus();
        }

        this.each(function() {
            var input = $('<div></div>')
                .addClass($(this).attr('class'))
                .data('ref', $(this));

            input.bind('click', function() {
                var inputOverlay = $('#' + options.id);
                if (inputOverlay.length === 0) {
                    inputOverlay = $('<input />')
                        .attr({ 'id': options.id, 'attr': 'text' })
                        .addClass('ei_box')
                        .css('border', 'none')
                        .bind('keydown', function(e) {
                            switch (e.keyCode) {
                                case 13: // enter
                                    addAddress($(this).parent(), $(this).val());
                                    $(this).remove();
                                    input.click();
                                    break;

                                case 27: // esc
                                    addAddress($(this).parent(), '');
                                    $(this).remove();
                                    break;
                            }
                        })
                        .bind('blur', function() {
                            addAddress($(this).parent(), $(this).val());
                            $(this).remove();
                        });

                    $(this).append(inputOverlay);
                }
                setTimeout(function() { inputOverlay.focus(); }, 5); // for IE bugs
            });

            $(this).hide();
            $(this).parent().after().append(input);

            // initialize value of input box
            var tokens = $(this).val().split(options.delim);
            for (var i = 0; i < tokens.length; ++i) {
                if (tokens[i] === '') { continue; }
                addAddress(input, tokens[i]);
            }
        });

        return this;
    };

    /**
     * Checkout Form Handler
     */

    var CheckoutFormHandler = {

        removeErrorMessage: function(element) {
            return element.find('.error').remove();
        },

        displayErrorMessage: function(element, message) {
            this.removeErrorMessage(element);

            return element.append('<p class="error alert alert-danger">' + message + '</p>');
        }
    }

    $('#po_file, #check_file').on('change', function(e) {
        var $this = $(this),
            type = $this.attr('name'),
            file = e.target.files[0],
            data = new FormData()
        $img = $('img.' + type);

        data.append('type', type);
        data.append('file', file);

        if (typeof file !== 'undefined') {
            $.ajax({
                type: 'POST',
                url: $('#path-to-validate-file').val(),
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    if (response.error) {
                        CheckoutFormHandler.displayErrorMessage($('.error-container'), response.error)
                    } else {
                        $img.attr('src', response.src);
                    }
                }
            });
        }

    });

    $('.btn-upload').on('click', function(e) {
        e.preventDefault();
        var $file = $(this).siblings('input[type="file"]');

        $file.click();
    });

    $('.btn-change-invoice').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            $parent = $this.parent(),
            $target = $('.' + $this.data('target')),
            $hidden = $('.' + $this.data('hidden'));

        $parent.addClass('hidden');
        $hidden.addClass('hidden')
        $target.removeClass('hidden');
    });

    $('.btn-cancel-change').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            $parent = $this.parent(),
            $target = $('.' + $this.data('target'));

        $parent.addClass('hidden');
        $target.removeClass('hidden');
    });

    $('.download-invoice').on('click', function() {
        var $this = $(this);

        if ($this.attr('href') == 'javascript:void(0)') {
            $.ajax({
                type: 'POST',
                url: '/invoice-download',
                data: {
                    type: $this.data('type'),
                    file_name: $this.data('file_name'),
                    order_id: $this.data('order_id'),
                    invoice_id: $this.data('invoice_id')
                },
                success: function(response) {
                    if (response) {
                        var downloadBtn = document.createElement('a');
                        downloadBtn.setAttribute('href', '/' + response);
                        downloadBtn.setAttribute('download', response);
                        downloadBtn.setAttribute('class', 'hidden');
                        document.body.appendChild(downloadBtn);
                        downloadBtn.click();
                        downloadBtn.remove();

                        $.ajax({
                            type: 'POST',
                            url: '/remove-download',
                            data: {
                                file: response
                            },
                            success: function(response) {
                                //
                            }
                        });
                    }
                }
            });
        }
    })
})(jQuery);

// Admin-LTE Checkboxes
$('.user-options, .subscription-options, .seat-options').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
});

//setInterval(function() {
//    $.ajax({
//        type: 'POST',
//        url: $('#session-expired').val(),
//        data: {
//            current_location: window.location.pathname
//        },
//        success: function(response) {
//            if (response.authenticated == false) {
//                window.location.href = response.route;
//            }
//        }
//    });
//}, 300 * 1000);

$("[data-mask]").inputmask();

$('input[name="card_number"]').NumericOnly({ length: 16 });

$('input[name="cvv"]').NumericOnly({ length: 4 });

$('input[name="zip"]').NumericOnly({ length: 5 });

$('[data-toggle="popover"]').popover();

$('body').on('click', '#save-contact', function() {
    var empty = [];

    $('#address-book-form').find('input').each(function() {
        if ($(this).prop('required')) {
            if (!$.trim($(this).val()).length) {
                empty.push(1);
            }
        }
    });

});

$('body').on('submit', '#customer-book-form, #address-book-form', function() {
    $(this).find('#save-contact')
        .attr('disabled', true)
        .text('Saving');
    $(this).find('.btn-close').attr('disabled', true);
});

$('#apply-coupon').on('click', function() {
    var $this = $(this),
        coupon_code = $('#coupon-code').val(),
        order_id = $('#order').val();

    if ($.trim(coupon_code).length) {

        $.ajax({
            type: 'POST',
            url: $this.data('href'),
            data: {
                coupon_code: coupon_code,
                order_id: order_id
            },
            beforeSend: function(data) {
                $this.attr('disabled', true)
                    .text('Applying...');
            },
            success: function(response) {

                setTimeout(function() {
                    $this.removeAttr('disabled')
                         .text('Apply');
                    window.location.reload();
                }, 1500);

            }
        });
    }
});

$('#card-payment-form, #invoice-payment-form').on('submit', function() {
    $(this).find('button[type="submit"]').attr('disabled', true)
        .text('Sending Payment');

});

$('#edit-payment-details-form').on('submit', function() {
    $(this).find('button[type="submit"]').attr('disabled', true)
        .text('Saving Payment Details');
});

$('select[name="payment-option"]').on('change', function() {
    var $cardForm = $('#card-payment-form'),
        $invoiceForm = $('#invoice-payment-form');

    if ($(this).val() == 'card') {
        $cardForm.removeClass('hidden');
        $invoiceForm.addClass('hidden');

        return true;
    }

    $cardForm.addClass('hidden');
    $invoiceForm.removeClass('hidden');
});

$('input[name="other_recipients"]').emailinput();

$('.btn-save').on('click', function() {
    $(this).text('Saving')
});

var $alert = $('.alert'),
    $alertDeactivate = $('.alert-deactivate');

if ($alert.length && $alertDeactivate.length == 0) {

    $('body, html').animate({
        scrollTop: $alert.offset().top - 10
    }, 1000);
}

$('#reset-password-form').on('submit', function(e) {
    var $this = $(this),
        email = $this.find('input[type="email"]').val(),
        $alert = $this.find('.alert'),
        $button = $this.find('button');

    e.preventDefault();

    $.ajax({
        type: 'POST',
        url: $this.attr('action'),
        data: { email: email },
        beforeSend: function(data) {
            $button.attr('disabled', true);
        },
        success: function(response) {
            if (response.error.length) {
                $alert.removeClass('hidden alert-success')
                    .addClass('alert-danger')
                    .text(response.error);

                $button.attr('disabled', false);
            } else {
                $alert.removeClass('hidden alert-danger')
                    .addClass('alert-success')
                    .text(response.message);

                setTimeout(function() {
                    window.location.reload();
                }, 3000);
            }
        }
    });

});

var timer;

$('.iCheck-helper').on('click', function() {
    var $option = $(this).siblings('.user-options'),
        url = $option.data('url'),
        checked = $option.prop('checked') ? 1 : 0,
        $container = $(this).parents('label');

    if ($option.length && !$option.prop('readonly')) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                attrib: $option.attr('name'),
                value: checked
            },
            beforeSend: function(data) {
                if (!$container.find('i').length) {
                    clearTimeout(timer);
                    $container.append('<i>Saving...</i>');
                }
            },
            success: function(response) {
                timer = setTimeout(function() {
                    $container.find('i').remove();
                }, 1000);
            }
        })
    }

    return false;
});

/**
 * Class Credit Discount JS
 */

$('.class-credit').on('click', function() {
    var $giveCreditButton = $('#btn-give-credit');

    if ($('.class-credit:checked').length) {
        $giveCreditButton.removeAttr('disabled');
    } else {
        $giveCreditButton.attr('disabled', true);
    }
});

var sendCCDForm = false;

$('#class-credit-discount-form').on('submit', function(e) {
    var $this = $(this);

    Modal.showConfirmationModal(
        'Are you sure you want to use your credits?',
        'Modification',
        function() {
            sendCCDForm = true;
            $this.submit();
        }
    );

    if (!sendCCDForm) {
        e.preventDefault();
    }
});

$('.btn-remove-credit').on('click', function() {
    var $this = $(this),
        classRegId = $this.data('id'),
        url = $this.data('url');

    Modal.showConfirmationModal(
        'Are you sure you want to remove the credit from this class?',
        'Deletion',
        function() {
            $.ajax({
                type: 'POST',
                url: url,
                data: { class_reg_id: classRegId },
                success: function(response) {
                    if (response) {
                        window.location.reload();
                    }
                }
            });
        }
    );
});

$('.cancel-order').on('click', function() {
    var $this = $(this),
        userId = $this.data('user_id');
    id = $this.data('id'),
        url = $this.data('url'),
        $btnCancel = $('#notification-cancel'),
        $btnOk = $('#notification-ok');

    Modal.showConfirmationModal(
        'Are you sure you want to cancel this entire order?',
        'Cancellation',
        function() {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    user_id: userId,
                    id: id
                },
                beforeSend: function() {
                    $btnOk.text('Cancelling')
                        .attr('disabled', true);

                    $btnCancel.attr('disabled', true);
                },
                success: function(response) {
                    if (response) {
                        window.location.href = response;
                    }
                }
            });
        }
    );
});

var searchEmail = {

    foundMatchText: "We have found a match of the email. Is this the person you're looking?",

    notFoundMatchText: "We haven't found a match of that email",

    $addressBookForm: "",

    $emailTextBox: "",

    $firstNameTextBox: "",

    $lastNameTextBox: "",

    $matchContainer: "",

    $email: "",

    $first_name: "",

    $last_name: "",

    $option: "",

    $saveButton: "",

    init: function() {
        var timer;
        this.$addressBookForm = $('#address-book-form');
        this.$emailTextBox = this.$addressBookForm.find('#email');
        this.$firstNameTextBox = this.$addressBookForm.find('#first_name');
        this.$lastNameTextBox = this.$addressBookForm.find('#last_name');
        this.$matchContainer = this.$addressBookForm.find('.found-match-container');
        this.$saveButton = this.$addressBookForm.find('#save-contact');

        this.$email = this.$matchContainer.find('.email');
        this.$first_name = this.$matchContainer.find('.first_name');
        this.$last_name = this.$matchContainer.find('.last_name');

        this.$addressBookForm.find('#match_user_id')
            .attr('disabled', true);

        this.$emailTextBox.on('input', function() {
            var $this = $(this),
                email = $this.val(),
                url = $this.data('url'),
                emailData = $this.data('email')
            ms = 1000;

            clearTimeout(timer);

            timer = setTimeout(function() {

                $this.data('email', email);

                if ($.trim(email).length && email !== emailData) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: { email: email },
                        success: function(response) {
                            if (response && response.email) {
                                searchEmail.showMatchField(response);
                            } else {
                                searchEmail.hideMatchField();
                            }
                        },

                        error: function() {
                            //handle error here
                        }
                    })
                }

            }, ms);
        });

        this.sameAsMatchOrNot();
    },

    sameAsMatchOrNot: function() {
        this.$option = this.$addressBookForm.find('input[name="matched_user"]');

        this.$option.on('click', function() {

            if ($(this).val() == 1) {
                searchEmail.hideMatchInfo();
            } else {
                searchEmail.showMatchInfo();
            }

        });
    },

    showMatchField: function(response) {
        this.$email.text(response.email);
        this.$first_name.text(response.first_name);
        this.$last_name.text(response.last_name);
        this.$addressBookForm.find('#match_user_id')
            .attr('disabled', false)
            .val(response.id);

        $('.match-info').hide()
            .find('.form-control').attr('disabled', true);

        searchEmail.$matchContainer.show();
        this.$saveButton.attr('disabled', true);

        this.$option.removeAttr('checked');

        return this;
    },

    hideMatchField: function() {
        this.$addressBookForm.find('#match_user_id')
            .attr('disabled', true);

        searchEmail.$matchContainer.hide();
        this.$saveButton.attr('disabled', false);

        $('.match-info').show()
            .find('.form-control').attr('disabled', false);
    },

    showMatchInfo: function() {
        $('.match-info').show();

        this.$addressBookForm.find('.form-control')
            .attr('disabled', false)
            .val('');
        $('#match_user_id').attr('disabled', true);

        this.$matchContainer.hide();
        this.$saveButton.attr('disabled', false);
    },

    hideMatchInfo: function() {

        $('.match-info').hide()
            .find('.form-control').attr('disabled', true);
        $('#match_user_id').attr('disabled', false);

        this.$matchContainer.hide();
        this.$saveButton.attr('disabled', false);
    }
}

/**
 * Contact Module JS
 */

$('#add-contact, .edit-contact').on('click', function() {
    var $this = $(this),
        url = $this.data('url'),
        contactId = $this.data('id') ? $this.data('id') : 0,
        classId = $this.data('class_id') ? $this.data('class_id') : 0,
        courseId = $this.data('course_id') ? $this.data('course_id') : 0,
        modal = $('#contactModal'),
        item = $('#item-id') ? $.trim($('#item-id').val()) : 0,
        userId = $this.data('user_id') ? $this.data('user_id') : 0;
    $.ajax({
        type: 'GET',
        url: url,
        data: {
            contact_id: contactId,
            class_id: classId,
            course_id: courseId,
            item: item,
            user_id: userId
        },
        success: function(response) {
            modal.find('.modal-content').html(response);
            $('#phone, #zip').NumericOnly();
            if (modal.find('[name="id"]').length < 1 && $('#add-contact').length > 0) {
                $('#current_address').change(function(e) {
                    var com = $(this).parents().closest('label').parent().data('user-contact');
                    if ($(this).is(':checked')) {
                        $.each(com, function(k, v) {
                            if (k == 'state') {
                                $('.state').val(v).trigger('change');
                            } else {
                                $('[name="' + k + '"]').val(v);
                            }
                        });
                    } else {
                        $.each(com, function(k, v) {
                            if (k == 'state') {
                                $('.state').val('').trigger('change');
                            } else {
                                $('[name="' + k + '"]').val('');
                            }
                        });
                    }
                    e.preventDefault();
                });
            } else {
                $('#current_address').parent().hide()
            }

            searchEmail.init();
        }
    }).done(function() {
        $('[data-mask]').inputmask();

        $('.state').select2({
            placeholder: "Select State"
        });
        $("input#company").autocomplete({
            source: "/api/companies",
            minLength: 2,
        });
        $("input#company").autocomplete("option", "appendTo", "#contactModal");
    });

    modal.find('.modal-content').removeClass('panel-danger');
    modal.modal('show');
});

$('.delete-contact').on('click', function(e) {
    var $this = $(this),
        userId = $this.data('user_id')
    contactId = $this.data('id'),
        url = $this.data('url')
    $okBtn = $('#notification-ok');

    e.preventDefault();

    Modal.showConfirmationModal(
        'Are you sure you want to delete this contact?',
        'Deletion',
        function() {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    user_id: userId,
                    contact_id: contactId
                },
                beforeSend: function() {
                    $okBtn.attr('disabled', true)
                        .text('Deleting');
                },
                success: function(response) {

                    if (response == 'deleted') {

                        setTimeout(function() {
                            $okBtn.attr('disabled', false)
                                .text('Ok');

                            window.location.reload();
                        }, 1000);
                    }
                }
            })
        }
    );
});

/**
 * Registration Module JS
 */

$('#register').on('click', function() {
    var $this = $(this),
        contacts = [],
        url = $this.data('url'),
        class_id = $this.data('class_id'),
        course_id = $this.data('course_id'),
        item = $.trim($('#item-id').val()),
        regCount = $.trim($('#reg-count').val()),
        $container = $('.attendees-list').length ? $('.attendees-list') : $('.box-body'),
        $alert = $container.find('.alert-danger'),
        $modal = $('#contactModal'),
        userId = $this.data('agent') ? $this.data('agent') : 0,
        isSwap = $('#is-swap').val();

    $('.contact').each(function() {
        if ($(this).prop('checked')) {
            contacts.push($(this).val());
        }
    });

    if (contacts.length) {

        function doRegistration() {
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    class_id: class_id,
                    course_id: course_id,
                    contacts: contacts,
                    item: item,
                    user_id: userId,

                },
                beforeSend: function(data) {
                    $this.attr('disabled', true);
                },
                success: function(response) {
                    try {
                        if (typeof response !== 'object') {
                            response = jQuery.parseJSON(response);
                        }

                        window.location.href = response.data.route;
                    } catch (e) {
                        $modal.find('.modal-content').addClass('panel-danger');
                        $modal.find('.modal-body').html('There was an issue with your request.');
                        $modal.modal('show');
                        $this.attr('disabled', false);
                    }

                }
            });
        }

        if (isSwap != 0 && item != 0 && regCount != contacts.length) {
            var text = 'Number of attendees (' + regCount + ') should be the same.';

            if ($alert.length == 0) {
                $container.prepend("<div class='alert alert-danger'>" + text + "</div>");
            } else if ($alert.length == 1) {
                $alert.html(text);
            }

            $('body, html').animate({
                scrollTop: $container.offset().top - 10
            }, 1000);

            return false;
        }

        var $pairedClass = $('#paired-class');

        if ($pairedClass.length) {

            $.ajax({
                type: 'POST',
                url: $pairedClass.data('url'),
                data: {
                    registration_order_id: $pairedClass.data('registration-order-id'),
                    contacts: contacts
                },
                success: function(response) {
                    if (typeof response == 'string') {
                        $container.append(response);

                        var $swapModal = $('#swapNotificationModal'),
                            $notificationOk = $swapModal.find('#notification-ok'),
                            $notificationCancel = $swapModal.find('#notification-cancel');

                        $swapModal.modal('show');

                        $notificationOk.on('click', function() {
                            var $this = $(this);
                            $swapModal.attr('data-backdrop', 'static')
                                .attr('data-keyboard', false);

                            $this.text('Swapping')
                                .attr('disabled', true);

                            $notificationCancel.attr('disabled', true);

                            doRegistration();
                        });
                    } else {
                        doRegistration();
                    }
                }
            });
        } else {
            doRegistration();
        }

    }
});

$('.activate-subscription').on('click', function() {
    var $this = $(this),
        id = $this.data('id'),
        url = $this.data('url');

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            id: id
        },
        success: function(response) {
            $('body').prepend(response);
            $('#activationModal').modal('show');
        }
    })
});

var cancelRegistration = {

    $cancelRegistrationBtn: $('.cancel-registration'),

    $cancelRegistrationCheckbox: $('.checkbox-cancel-registration'),

    url: $('#cancel-url').val(),

    init: function() {
        this.$cancelRegistrationBtn.on('click', function() {
            var $this = $(this),
                agentId = $this.data('agent_id'),
                orderId = $this.data('order_id'),
                registrationOrderId = $this.data('registration_order_id'),
                $checkBoxes = $this.parents('.list-container').find('input[name=class-registrations]'),
                newRegistration = 0,
                classRegistrationIds = [];

            $checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    classRegistrationIds.push($(this).val());

                    if ($(this).data('is_new')) {
                        newRegistration = 1;
                    }
                }
            });

            if (classRegistrationIds.length) {
                cancelRegistration.queryCancelStatus($this, agentId, classRegistrationIds, registrationOrderId, orderId, newRegistration);
            }
        });

        $('body').on('click', '#cancel-new-registration-btn', function() {
            var $this = $(this);

            $this.attr('disabled');
        });

        this.$cancelRegistrationCheckbox.on('click', function() {
            var checked = [];

            cancelRegistration.$cancelRegistrationCheckbox.each(function() {
                if ($(this).prop('checked')) {
                    checked.push($(this));
                }
            });

            if (checked.length > 0) {
                cancelRegistration.$cancelRegistrationBtn.attr('disabled', false);
            } else {
                cancelRegistration.$cancelRegistrationBtn.attr('disabled', true);
            }
        });
    },

    queryCancelStatus: function($btn, agentId, classRegistrationIds, registrationOrderId, orderId, newRegistration) {

        $.ajax({
            type: 'POST',
            url: cancelRegistration.url,
            data: {
                agent_id: agentId,
                order_id: orderId,
                class_registration_ids: classRegistrationIds,
                registration_order_id: registrationOrderId,
                new_registration: newRegistration
            },
            beforeSend: function() {
                $btn.attr('disabled', true);
            },
            success: function(content) {
                if (content) {
                    var $modal = $('#cancellationModal');

                    $modal.find('.modal-content').html(content);
                    $modal.modal('show');

                    $('body').on('submit', '#cancel-complete-registration', function() {
                        var $this = $(this);

                        $('.proceed-cancellation').text('Cancelling')
                            .css('opacity', 0.7);

                        $('.btn-close').css('opacity', 0.7);

                        setTimeout(function() {
                            $modal.modal('hide');
                        }, 2000);
                    });
                }

                $btn.removeAttr('disabled');
            }
        });
    }
}

var confirmationHandler = {

    $orderConfirmBtn: $('#contact-csr-modification'),

    $cartOrderCompleteBtn: $('.cart-complete-order'),

    $notifyAccntg: $('#notify-accounting-modification'),

    $orderAdjustmentsIds: $('.order_adjustment_ids'),

    init: function() {
        var $okBtn = $('#notification-ok');
        var $doNotEmail = $('#doNotEmailContainer');

        this.$orderConfirmBtn.on('click', function() {
            $okBtn.hide();
            Modal.showConfirmationModal(
                'Please contact a Customer Service Representative to Confirm Modification.',
                'Notification',
                function() {
                    return this;
                }
            );
        });

        this.$cartOrderCompleteBtn.on('click', function() {
            var url = $(this).data('url'),
                userId = $(this).data('user_id');


            $okBtn.show();
            $doNotEmail.show();

            Modal.showConfirmationModal(
                'Are you sure you want to complete this order?',
                'Complete Order',
                function() {

                    if ($("input#doNotEmail:checked").length > 0) {
                        data = {
                            order_id: $('#order').val(),
                            doNotEmail: 1,
                            user_id: userId
                        };
                    } else {
                        data = {
                            order_id: $('#order').val(),
                            user_id: userId
                        };
                    }

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        beforeSend: function() {
                            $okBtn.text('Saving..')
                                .attr('disabled', true);
                        },
                        success: function(response) {
                            setTimeout(function() {
                                $okBtn.text('Ok')
                                    .attr('disabled', false);
                                window.location.href = response;
                            }, 1500)
                        }
                    });
                }
            );
        });

        this.$notifyAccntg.on('click', function() {
            var url = $(this).data('url');
            $doNotEmail.show();

            Modal.showConfirmationModal(
                'Changes on the prices will be notified to the accounting. ' +
                'Do you want to proceed?',
                'Notify Accounting',
                function() {
                    var orderAdjustmentIds = [];

                    confirmationHandler.$orderAdjustmentsIds.each(function() {
                        orderAdjustmentIds.push($(this).val());
                    });

                    if ($("input#doNotEmail:checked").length > 0) {
                        data = {
                            doNotEmail: 1,
                            order_adjustment_ids: orderAdjustmentIds,
                            status: $('#status').val()
                        };
                    } else {
                        data = {
                            order_adjustment_ids: orderAdjustmentIds,
                            status: $('#status').val()
                        };
                    }

                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        beforeSend: function() {
                            $('.notification-ok').text('Sending...')
                        },
                        success: function(response) {
                            setTimeout(function() {
                                $('.notification-ok').text('Ok')
                                window.location.reload();
                            }, 1500);
                        }
                    });
                }
            );
        });
    }
}