/**
 * Admin JS
 *
 * @author Cyril Yu <cyril@cspreston.com>
 * @since Nov 20115
 */

//Register JumpTo Plugin - Datatables

jQuery.fn.dataTable.Api.register( 'page.jumpToData()', function ( data, column ) {

    if (typeof this.column(column, {order:'current'}).data() != 'undefined') {
        var pos = this.column(column, {order: 'current'}).data().indexOf(data);

        if (pos >= 0) {
            var page = Math.floor(pos / this.page.info().length);
            this.page(page).draw(false);
        }
    }
    return this;
} );

// End
var $table = $("#table-select-course, #table-coupons, #table-classes, #table-notes");
var table = $table.DataTable();

$("#table-contacts").DataTable({
   "columnDefs" : [{
       "targets": 0,
       "orderable": false,
       "searchable": false
   }],
   "order": []
});

$('#table-clients').DataTable({
    "order": [[ 2 , "desc"]],
    "stateSave":true,
});

$tableCourses = $('#table-courses').DataTable({
    "order": [[ 2 , "desc"]],
    "stateSave":true,
});

if ($('#searchkey').length) {
    table.page.jumpToData($('#searchkey').val(), $('#searchkey').data('column'));
}

$('#check-all').on('click', function() {
    var $this = $(this);

    if ($this.prop('checked')) {
        $('.check-single').prop('checked', true);
    }

    else {
        $('.check-single').prop('checked', false);
    }
});

$('.check-single').on('click', function() {
    var $this = $(this);

    if (!$this.prop('checked')) {
        $('#check-all').prop('checked', false);
    }
});

/**
 * DatePicker JS
 */

$('#filter-start-date')
    .datepicker({ format: "yyyy-mm-dd", autoclose: true })
    .on('change',function(){
        $('#search-start_date-filter').submit();
    }
);

$("#trainingDate").datepicker({
    format: 'MM dd, yyyy',
    startDate: 'today',
    autoclose: true,
});

$(".report-date").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
});

$('#category, #type, #status').on('change', function() {
    $(this).parent('form').submit();
});

$('#add-class, #edit-class').on('click', function() {
    var $this = $(this),
        id = $this.data('id'),
        courseId = $this.data('course'),
        modal = $('#classModal');

    $.ajax({
        type: 'GET',
        url: '/admin/class/form',
        data: { id : id, course_id : courseId },
        success : function(response) {
            modal.find('.modal-content').html(response);
        }
    }).done(function() {
        $('#startTime').timepicker({
            showInputs : false
        });

        $('#endTime').timepicker({
            showInputs : false
        });

        $("#startDate").datepicker({
            format: 'MM dd, yyyy',
            startDate: 'today',
            autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('#endDate').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('#endDate').datepicker('setStartDate', null);
        });

        $("#endDate").datepicker({
            format: 'MM dd, yyyy',
            startDate: 'today',
            autoclose: true,
        }).on('changeDate', function (selected) {
            var endDate = new Date(selected.date.valueOf());
            $('#startDate').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
            $('#startDate').datepicker('setEndDate', null);
        });

        $('#classForm .instructor').select2({
            placeholder: "Select Instructor"
        });

        $('#classForm .state').select2({
            placeholder: "Select State"
        });
    });

    modal.modal('show');
});

$table.on('click', '.delete, .remove-item', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you want to delete this?',
        'Delete',
        function() {
            var $okBtn = $('#notification-ok');

            $.ajax({
                type: 'POST',
                url: href,
                data: { id : id },
                beforeSend: function(data) {
                    $okBtn.text('Modifying...')
                        .attr('disabled', true);
                },
                success: function(response) {
                    setTimeout(function() {
                        $okBtn.text('Ok').attr('disabled', false);
                        window.location.reload();
                    }, 1500);
                }
            })
        }
    );
});

$table.on('click', '.status-change', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you want to modify this?',
        'Modification',
        function() {
            var $okBtn = $('#notification-ok');

            $.ajax({
                type: 'POST',
                url: href,
                data: { id : id },
                beforeSend: function(data) {
                    $okBtn.text('Modifying...')
                        .attr('disabled', true);
                },
                success: function(response) {
                    setTimeout(function() {
                        $okBtn.text('Ok').attr('disabled', false);
                        window.location.reload();
                    }, 1500);
                }
            })
        }
    );
});

$tableCourses.on('click', '.status-change', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you want to modify this?',
        'Modification',
        function() {
            var $okBtn = $('#notification-ok');

            $.ajax({
                type: 'POST',
                url: href,
                data: { id : id },
                beforeSend: function(data) {
                    $okBtn.text('Modifying...')
                        .attr('disabled', true);
                },
                success: function(response) {
                    setTimeout(function() {
                        $okBtn.text('Ok').attr('disabled', false);
                        window.location.reload();
                    }, 1500);
                }
            })
        }
    );
});

$tableCourses.on('click', '.delete', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you want to delete this?',
        'Delete',
        function() {
            var $okBtn = $('#notification-ok');

            $.ajax({
                type: 'POST',
                url: href,
                data: { id : id },
                beforeSend: function(data) {
                    $okBtn.text('Modifying...')
                        .attr('disabled', true);
                },
                success: function(response) {
                    setTimeout(function() {
                        $okBtn.text('Ok').attr('disabled', false);
                        window.location.reload();
                    }, 1500);
                }
            })
        }
    );
})

$('[data-toggle="tooltip"]').tooltip()

if ($('.subscription-options').length) {
    $('.subscription-options').siblings('.iCheck-helper').attr('readonly');
}

$('select[name="course_type_id"]').on('change', function() {
    var $this = $(this),
        courseTypeId = $this.val(),
        $webinarFieldContainer = $('.webinar-text'),
        $courseMillFieldContainer = $('.course-mill-text');

    if (courseTypeId == 3) {
        $webinarFieldContainer.addClass('hidden');
        $courseMillFieldContainer.removeClass('hidden');
        return false;
    } else if (courseTypeId == 2) {
        $courseMillFieldContainer.addClass('hidden');
        $webinarFieldContainer.removeClass('hidden');
        return false;
    }
    $webinarFieldContainer.addClass('hidden');
    $courseMillFieldContainer.addClass('hidden');
    return false;
});

$('.change-price').on('click', function() {
    var $this = $(this),
        price = $this.siblings('.price').val(),
        originalPrice = $this.siblings('.price').data('original-price'),
        order = $('#order').val(),
        orderableId = $this.data('orderable-id'),
        orderableType = $this.data('orderable-type');

    if (price.length && parseFloat(price) != parseFloat(originalPrice)) {

        Modal.showConfirmationModal(
            'Are you sure you want to change the price?',
            'Modification',
            function() {
                $.ajax({
                    type : 'POST',
                    url  : "/admin/user/change-item-price",
                    data : {
                        price : price,
                        order : order,
                        orderable_id : orderableId,
                        orderable_type : orderableType
                    },
                    beforeSend : function() {
                        $('.notification-ok').text('Saving...')
                            .attr('disabled', 'disabled')
                    },
                    success : function(response) {
                        if (response) {

                            setTimeout(function() {
                                $('.notification-ok').text('Ok')
                                window.location.reload();
                            }, 1500);
                        }
                    }
                });
            }
        );
    }
});

$('.remove-item, .delete').on('click', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you wanna delete this?',
        'Deletion',
        function() {
            var $okBtn = $('#notification-ok');

            $.ajax({
                type: 'POST',
                url: href,
                data: { id : id },
                beforeSend: function(data) {
                    $okBtn.text('Modifying...')
                        .attr('disabled', true);
                },
                success: function(response) {
                    setTimeout(function() {
                        $okBtn.text('Ok').attr('disabled', false);
                        window.location.reload();
                    }, 1500);
                }
            })
        }
    );
});


var generalNoteHandler = {

    init: function() {

        /**
         * TinyMCE
         */

        var $textarea = $('#general-note'),
            timer;

        tinymce.init({
            selector: '#general-note',
            height: 150,
            menubar: false,
            toolbar: [
                'bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link'
            ],
            theme_advanced_buttons1: "bold,italic,underline",
            setup: function (editor) {
                editor.on('change', function() {
                    var ms = 1000,
                        content = editor.getContent();

                    clearTimeout(timer);

                    timer = setTimeout(function() {

                        if ($.trim(content.length)) {
                            $.ajax({
                                type : 'POST',
                                url  : $('#save-note').val(),
                                data : {
                                    customer_id: $('#customer-id').val(),
                                    content: content
                                },
                                success : function(response) {
                                    //console.log(response);
                                },

                                error : function() {
                                    //handle error here
                                }
                            });
                        }

                    }, ms);
                });
            }
        });


        $textarea.removeClass('hidden');
    }
}

var cancellationAmountHandler = {

    init: function() {
        $('body').on('input', '#cancellation-fee', function() {
            var $this = $(this),
                totalFee = $this.val(),
                amountPaid = $('#amount-paid').data('amount'),
                deferredHalfPriceDiscount = $('#deferred-half-price-fees').length
                                    ? $('#deferred-half-price-fees').data('amount')
                                    : 0,
                deferredGroupDiscount = $('#deferred-group-discount-fees').length
                                    ? $('#deferred-group-discount-fees').data('amount')
                                    : 0,
                additionalFees = $('#additional-fees').length
                                 ? ('#additional-fees').data('amount')
                                 : 0,
                deductions = $('#deductions').length
                            ? $('#deductions').data('amount')
                            : 0,
                $subTotalRefund = $('#subtotal-refund'),
                $errorMsg = $('.cancellation-error-msg'),
                totalRefund;

            if ($.isNumeric(totalFee)) {
                $errorMsg.text('');
                totalRefund = (parseFloat(amountPaid) -
                             (parseFloat(additionalFees) + parseFloat(deferredHalfPriceDiscount) + parseFloat(deferredGroupDiscount)  + parseFloat(totalFee)  - parseFloat(deductions)));
                $subTotalRefund.html('$' + parseFloat(totalRefund).toFixed(2));
            }

            else {
                $errorMsg.text('Value entered is not a number.');
            }
        });
    }
}

var orderModificationHandler = {

    $modifyPriceBtn: $('.order-modification-change-price'),

    $adjustmentPriceChangeBtn: $('.change-order-adjustment-price'),

    $removeAdjustmentBtn: $('.remove-order-adjustment'),

    $restoreCancelledBtn: $('.restore-cancelled'),

    init: function() {

        this.$modifyPriceBtn.on('click', function() {
            var $this = $(this)
                adjustableId = $this.data('adjustable-id'),
                adjustableType = $this.data('adjustable-type')
                newPrice = $this.siblings('.price').val(),
                originalPrice = $this.siblings('.price').data('original-price'),
                orderId = $('#order').val(),
                orderItemId = $('#order-item-id').val(),
                agentId = $('#agent-id').val();

            if ($.isNumeric(newPrice)) {

                Modal.showConfirmationModal(
                    'Are you sure you want to change the price?',
                    'Modification',
                    function() {
                        $.ajax({
                            type : 'POST',
                            url  : '/admin/order-modification/change-price',
                            data : {
                                original_price: originalPrice,
                                new_price: newPrice,
                                order_id: orderId,
                                order_item_id: orderItemId,
                                agent_id: agentId,
                                adjustable_id: adjustableId,
                                adjustable_type: adjustableType
                            },
                            beforeSend : function() {
                                $('.notification-ok').text('Saving...')
                            },
                            success : function(response) {
                                if (response) {

                                    setTimeout(function() {
                                        $('.notification-ok').text('Ok')
                                        window.location.reload();
                                    }, 1500);
                                }
                            }
                        });
                    }
                );
            }
        });

        this.$adjustmentPriceChangeBtn.on('click', function() {
            var $this = $(this),
                $priceInput = $('.' + $this.attr('id')),
                newPrice = $priceInput.val(),
                originalPrice = $priceInput.data('original-price'),
                adjustmentId = $this.data('id')

            if ($.isNumeric(newPrice) && (parseFloat(newPrice) != parseFloat(originalPrice))) {
                Modal.showConfirmationModal(
                    'Are you sure you want to change this price?',
                    'Modification',
                    function() {

                        $.ajax({
                            type : 'POST',
                            url  : '/admin/order-modification/adjustment-change-price',
                            data : {
                                new_price: newPrice,
                                adjustment_id: adjustmentId
                            },
                            beforeSend : function() {
                                $('.notification-ok').text('Saving...')
                            },
                            success : function(response) {
                                setTimeout(function() {
                                    $('.notification-ok').text('Ok')
                                    window.location.reload();
                                }, 1500);
                            }
                        });
                    }
                );
            }
        });

        this.$removeAdjustmentBtn.on('click', function () {
            var $this = $(this),
                adjustmentId = $this.data('id');

            Modal.showConfirmationModal(
                'Are you sure you want to remove this adjustment?',
                'Deletion',
                function() {

                    $.ajax({
                        type : 'POST',
                        url  : '/admin/order-modification/adjustment-remove',
                        data : {
                            adjustment_id: adjustmentId
                        },
                        beforeSend : function() {
                            $('.notification-ok').text('Deleting...')
                        },
                        success : function(response) {
                            // console.log(response)
                            setTimeout(function() {
                                $('.notification-ok').text('Ok')
                                window.location.reload();
                            }, 1500);
                        }
                    });
                }
            );
        });

        this.$restoreCancelledBtn.on('click', function() {
            var $this = $(this),
                classRegistrationId = $this.data('id'),
                userId = $this.data('user_id');

            Modal.showConfirmationModal(
                'Are you sure you want to restore this registration?',
                'Restoration',
                function() {

                    $.ajax({
                        type : 'POST',
                        url  : '/admin/order-modification/restore-cancelled',
                        data : {
                            class_registration_id: classRegistrationId,
                            user_id: userId
                        },
                        beforeSend : function() {
                            $('.notification-ok').text('Restoring...')
                        },
                        success : function(response) {
                            setTimeout(function() {
                                $('.notification-ok').text('Ok')
                                window.location.reload();
                            }, 1500);
                        }
                    });
                }
            );
        });
    }
}


