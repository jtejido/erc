
$('#course_filter, #coupon_type_filter').on('change', function() {
    $(this).parent('form').submit();
});

$('.delete-coupon').on('click', function() {
    var $this = $(this),
        href = $this.data('href'),
        id = $this.data('id');

    Modal.showConfirmationModal(
        'Are you sure you wanna remove this coupon?',
        'Deletion',
        function() {
            $.ajax({
                type : 'POST',
                url  : href,
                data : { id : id },
                success : function(response) {
                    if (response) {
                        window.location.reload();
                    }
                }
            });
        }
    );
});

$('.change_coupon_type').on('click', function() {
    var type = $(this).val(),
        amount = parseFloat($('#coupon_amount').val());

    if(amount <= 0 || isNaN(amount)) {
        Modal.showAlertModal('The amount you have entered is not valid for this coupon type.', 'Invalid Amount');
        $('#coupon_amount').val(0);
    }

    if(type === 'percent' && amount > 100) {
        Modal.showAlertModal('The amount you have entered is not valid for this coupon type.', 'Invalid Amount');
        $('#coupon_amount').val(0);
    }
});