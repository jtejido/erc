var Modal = (function($) {
    return {
        showConfirmationModal : function(modalMsg, modalHeader, onOkFunction, onCancelFunction) {
            $('div.modal-body p:first-child').html(modalMsg);
            $('#myModalLabel').html(modalHeader);
            $('#notificationModal').modal('show');

            this.attachConfirmationEventListener(onOkFunction, onCancelFunction);
        },

        hideConfirmationModal : function() {
            $('#notification-ok').modal('hide');
        },

        attachConfirmationEventListener : function(onOkFunction, onCancelFunction) {
            $('#notification-ok').unbind('click').click(function () {
                if (onOkFunction) {
                    onOkFunction.call();
                }

                Modal.hideConfirmationModal();
            });

            $('#notification-cancel').unbind('click').click(function () {
                if (onCancelFunction) {
                    onCancelFunction.call();
                }

                Modal.hideConfirmationModal();
            });
        },

        showAlertModal : function(modalMsg, modalHeader, onOkFunction) {
            $('#notificationModal').find('div.modal-body p:first-child').html(modalMsg);
            $('#myModalLabel').html(modalHeader);
            $('#notificationModal').modal('show');

            $('#notification-ok').click(function () {
                if (onOkFunction) {
                    onOkFunction.call();
                }

                $('#notificationModal').modal('hide');
            });
        }
    }
})(jQuery);