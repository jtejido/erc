<?php

return [

    'cert-success-generateByBatch'      => 'Students successfully mark as attended.',
    'cert-success-generate'             => 'Student successfully mark as attended.',
    'cert-success-regenerate'             => 'Successfully re-generated certificate',

];
