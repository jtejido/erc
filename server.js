var app = require('http').createServer(handler);
var io = require('socket.io')(app);

var Redis = require('ioredis');
var redis = new Redis();
// var sub = Redis.createClient();

app.listen(3001, function() {
    console.log('Server is running!');
});

function handler(req, res) {
    res.writeHead(200);
    res.end('');
}

io.on('connection', function(socket) {
    console.log('xxxxx '+socket.id);
    var redisClient = Redis.createClient();

    // sub.on('message', function(ch, msg) {
    //     console.log('message', ch, msg);
    // });
    // console.log("new client connected");
    
    redisClient.subscribe('message');

    redisClient.on("message", function(channel, message) {
        console.log("mew message in queue "+ message + "channel");
        socket.emit(channel, message);
    });

    socket.on('disconnect', function() {
        redisClient.quit();
    });

});


redis.psubscribe('*', function(err, count) {
    //
        console.log('subscribing to *');
});

redis.on('pmessage', function(subscribed, channel, message) {
    console.log('initial: '+message);
    message = JSON.parse(message);

    console.log(channel + ':' + message.event, message.data);
    io.emit(channel + ':' + message.event, message.data);
});