#!/bin/sh
FILE="Operations Manual.md"

pandoc \
    --standalone \
    --normalize \
    --smart \
    --self-contained \
    --from=markdown \
    --output="storage/Operations Manual.pdf" \
    "$FILE"
echo "Created 'storage/Operations Manual.pdf'"