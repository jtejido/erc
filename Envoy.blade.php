@servers(['web' => 'erc@45.33.70.35'])

<?php
    $repo = 'git@bitbucket.org:cspreston/erc.git';
    $location = '/home/erc';
    $release_dir = $location.'/releases';
    $app_dir = $location.'/current';
    $release = 'release_' . date('YmdHis');
    $environment = isset($env) ? $env : "production";
    $gulp_flags = '';
    $bower_flags = '';
    if ($environment === 'production') {
        //$gulp_flags = '--production';
        $bower_flags = '--allow-root';
    } 
    $branch = isset($branch) ? $branch : "develop"; 
?>
@macro('deploy_web', ['on' => 'web'])
    fetch_repo
    run_composer
    run_cache_clear
    update_permissions
    update_symlinks
@endmacro

@macro('deploy', ['on' => 'web'])
    fetch_repo
    run_composer
    update_symlinks
    update_permissions
    elixir
    run_cache_clear
@endmacro

@macro('elixir', ['on' => 'web'])
    elixir
@endmacro

@macro('daemon', ['on' => 'web'])
    kill_daemon
    run_cache_clear
    run_daemon
@endmacro

@task('fetch_repo')
	cd {{ $location }};
    mkdir -p {{ $release_dir }};
    cd {{ $release_dir }};

    echo "Cloning {{ $branch}}..";
    git clone -b {{ $branch }} {{ $repo }} {{ $release }};
@endtask


@task('run_composer')

    <?php /*
	[ -d {{ $location }}/vendor ] || mkdir -p {{ $location }}/vendor;
	cp -rf {{ $release_dir }}/{{ $release }}/vendor {{ $location }}/vendor;
    */ ?>

    cd {{ $release_dir }}/{{ $release }};
    rm -rf {{ $release_dir }}/{{ $release }}/vendor;
    ln -nsf {{ $location }}/vendor {{ $release_dir }}/{{ $release }};

    composer install --no-dev --no-scripts;
    php artisan clear-compiled --env=production;
    php artisan optimize --env=production;
@endtask

@task('update_permissions')
    cd {{ $release_dir }};
    chgrp -R www-data {{ $release }};
    <?php /* chown -R erc:www-data {{ $location }}/storage; */ ?>
    chmod -R ug+rwx {{ $release }};
@endtask

@task('update_symlinks')
    ln -nfs {{ $release_dir }}/{{ $release }} {{ $app_dir }};
    chgrp -h www-data {{ $release_dir }}/{{ $release }};

    cd {{ $release_dir }}/{{ $release }};
    ln -nfs {{ $location }}/.env .env;
    chgrp -h www-data .env;

    [ -d {{ $location }}/storage ] || mkdir -p {{ $location }}/storage;
    [ -d {{ $location }}/node_modules ] || mkdir -p {{ $location }}/node_modules;

    {{--rm -rf {{ $release_dir }}/{{ $release }}/storage;--}}
    {{--ln -nsf {{ $location }}/storage {{ $release_dir }}/{{ $release }}/;--}}

    rm -rf {{ $release_dir }}/{{ $release }}/node_modules;
    ln -nsf {{ $location }}/node_modules {{ $release_dir }}/{{ $release }}/;

    rm -rf {{ $release_dir }}/{{ $release }}/public/fileman/Uploads;
    ln -nsf {{ $location }}/Uploads {{ $release_dir }}/{{ $release }}/public/fileman;

    {{--mkdir {{ $release_dir }}/{{ $release }}/public/uploads;--}}
    {{--ln -nsf {{ $location }}/invoice_photos {{ $release_dir }}/{{ $release }}/public/uploads/;--}}

    cd {{ $release_dir }}/{{ $release }};
    git config core.fileMode false; {{-- this ignores chmod done in the directory --}}
@endtask

@task('kill_daemon')
    cd {{ $release_dir }}/{{ $release }};
    kill -9 `cat {{ $app_dir }}/run.pid`;
@endtask

@task('run_daemon')
    cd {{ $release_dir }}/{{ $release }};
    nohup php artisan queue:work --daemon > /dev/null 2>&1 & echo $! > run.pid
@endtask

@task('run_cache_clear')
    cd {{ $release_dir }}/{{ $release }};
    php artisan cache:clear
@endtask

@task('elixir')
    cd {{ $release_dir }}/{{ $release }};
    cp {bower.json,composer.json,composer.lock,package.json} {{ $location }};
	
	cd {{ $location }};
    bower install {{ $bower_flags }};
    npm install;
    cd {{ $release_dir }}/{{ $release }};
    gulp {{ $gulp_flags }};
@endtask