<?php

namespace App\Jobs;

use App\Events\CourseMillAccountCreateFailedEvent;
use App\Facades\CourseMillService;
use function foo\func;
use Illuminate\Support\Facades\Log;
use Mail;
use App\Models\RegistrationOrder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mockery\Exception;

class CreateStudentFromRegistration extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var RegistrationOrder
     */
    public $registrationOrder;

    /**
     * Create a new job instance.
     *
     * @param Mailer $mailer
     * @param RegistrationOrder $registrationOrder
     */
    public function __construct(RegistrationOrder $registrationOrder)
    {
        $this->registrationOrder = $registrationOrder;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            CourseMillService::createStudentFromRegistration($this->registrationOrder);
        } catch (\Exception $e) {
            Log::alert('CreateStudentFromRegistration: ' . $e->getMessage());
            if($this->attempts() >= 3) {
                event(new CourseMillAccountCreateFailedEvent($this->registrationOrder, $e));
            } else {
                $this->release(1);
            }
        }

    }

}
