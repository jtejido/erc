<?php

    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all of the routes for an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the controller to call when that URI is requested.
    |
    */

    Route::get('/api/companies', 'ApiController@listCompanies');

    require_once('Routes/main_routes.php');
    require_once('Routes/client_routes.php');
    require_once('Routes/admin_routes.php');

    Route::get('/{post}', [
        'as'    => 'post.show',
        'uses'  => 'Main\PostController@show'
    ]);

    Route::get('/faq/computer-based-training', function(){ 
        return Redirect::to('/faq/online-training', 301); 
    });

    Route::get('/{category}/{post}', [
        'as'    => 'post.show',
        'uses'  => 'Main\PostController@showWithCategory'
    ]);

    Route::post('/onsite-and-customized-training',[
        'as'    => 'post.custom-training',
        'uses'  => 'Main\PostController@sendEmailCustom'
    ]);

    Route::get('/design', [
        'as'    => 'design',
        'uses'  => 'DesignController@index'
    ]);



