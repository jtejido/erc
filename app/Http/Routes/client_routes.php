<?php

    /*
    |--------------------------------------------------------------------------
    | Member Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['middleware' => 'member'], function()
    {

        Route::get('home', [
            'as'=> 'user.home',
            'uses'  => 'Main\MainController@userDetails'
        ]);

        Route::get('order_history', [
            'as'    => 'user.order_history',
            'uses'  => 'Main\OrderHistoryController@index_api'
        ]);

        Route::get('classes/{class_id}/related', [
            'as'        => 'classes.related',
            'uses'      => 'Main\ClassesController@showRelated'
        ]);

        Route::get('/register/{course_id}/{class_id?}', [
            'as'    => 'contacts.register',
            'uses'  => 'Main\ContactController@contacts'
        ]);

        Route::get('classes/register', [
            'as'    => 'classes.register',
            'uses'  => 'Main\ClassesController@register'
        ]);


        /******************************************************/
        /*                    Cart Routes                     */
        /******************************************************/

        Route::get('shopping-cart', [
            'as'    => 'user.shopping_cart',
            'uses'  => 'Main\RegistrationController@cart'
        ]);

        Route::post('remove-item-shopping-cart', [
            'as'    => 'user.shopping_cart_item.remove',
            'uses'  => 'Main\RegistrationController@removeItem'
        ]);

        Route::get('/yearly-subscription', [
            'as'    => 'yearly_subscription',
            'uses'  => 'Admin\SubscriptionController@contacts'
        ])->where('user_id', '[0-9]+');

        /******************************************************/
        /*                  Checkout Routes                   */
        /******************************************************/

        Route::get('checkout', [
            'as'    => 'user.checkout.get',
            'uses'  => 'Main\CheckoutController@checkout'
        ]);

        Route::post('card-checkout', [
            'as'    => 'user.card_checkout',
            'uses'  => 'Main\CheckoutController@createCardPayment'
        ]);

        Route::post('invoice-checkout', [
           'as'     => 'user.invoice_checkout',
           'uses'   => 'Main\CheckoutController@createInvoicePayment'
        ]);

        Route::get('success-checkout', [
            'as'    => 'checkout.success',
            'uses'  => 'Main\CheckoutController@success'
        ]);

        /******************************************************/
        /*               Completed Order Routes               */
        /******************************************************/

        Route::group(['middleware' => 'trans_owner'], function()
        {
            Route::get('user-training-details/{user_id}/{transaction_id}', [
                'as'=> 'user.training-details',
                'uses'  => 'Main\MainController@userTrainingDetail'
            ]);

            Route::get('onsite-training-details/{transaction_id}', [
                'as'=> 'onsite.training-details',
                'uses'  => 'Main\MainController@onsiteTrainingDetails'
            ]);

            Route::get('transaction/{order_item_id}/edit-attendees', [
                'as'        => 'order.attendees.swap',
                'uses'      => 'Main\ContactController@contacts'
            ]);

            Route::get('transaction/{order_item_id}/add-attendees', [
                'as'        => 'order.attendees.add.get',
                'uses'      => 'Main\OrderModificationController@addAttendees'
            ]);

            Route::post('add-attendees', [
                'as'        => 'order.attendees.add.post',
                'uses'      => 'Main\OrderModificationController@doAddAttendees'
            ]);

            Route::get('invoice-info/{order_id}', [
                'as'        => 'order.invoice_info',
                'uses'      => 'Main\OrderController@invoiceInfo'
            ]);

            Route::get('order/{order_id}', [
                'as'        => 'order.info',
                'uses'      => 'Main\OrderController@entireOrderInfo'
            ]);
        });

        /******************************************************/
        /*           Profile, Address Book Routes             */
        /******************************************************/

        Route::get('edit-profile', [
            'as'        => 'profile.edit',
            'uses'      => 'Main\ProfileController@editProfile'
        ]);

        Route::get('address-book', [
            'as'        => 'address_book.edit',
            'uses'      => 'Main\ContactController@edit'
        ]);

        Route::post('edit-attribute', [
            'as'        => 'attribute.edit',
            'uses'      => 'Main\ProfileController@saveEditOtherAttributes'
        ]);

        Route::post('query-cancellable', [
            'as'        => 'registration.query.cancellable',
            'uses'      => 'Main\OrderModificationController@queryIfCancellable'
        ]);

        Route::post('cancel-registration', [
            'as'        => 'registration.cancel',
            'uses'      => 'Main\OrderModificationController@cancelClassRegistrations'
        ]);

        Route::group(['prefix' => 'checkout'], function()
        {

            Route::get('order-modification/{order_id}', [
                'as'    => 'order_mod.checkout',
                'uses'  => 'Main\OrderModificationCheckoutController@checkout'
            ]);

            Route::post('order-modification/credit-card-payment', [
                'as'    => 'order_mod.checkout.credit_card',
                'uses'  => 'Main\OrderModificationCheckoutController@createCreditCardPayment'
            ]);

            Route::post('order-modification/invoice-payment', [
                'as'    => 'order_mod.checkout.invoice',
                'uses'  => 'Main\OrderModificationCheckoutController@createInvoicePayment'
            ]);
        });
    });


