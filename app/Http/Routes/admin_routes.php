<?php

    /*
    |--------------------------------------------------------------------------
    | CSR Routes
    |--------------------------------------------------------------------------
    */

    Route::get('promo-photo/{id}', [
        'as'    => 'promo.photo',
        'uses'  => 'Admin\PromoController@previewPhoto'
    ]);

    Route::group(['middleware' => 'admin'], function()
    {
        Route::group(['prefix' => 'admin'], function()
        {
            Route::get('/', [
                'as'    => 'admin.dashboard',
                'uses'  => 'Admin\DashboardController@index'
            ]);

            Route::get('/index_data', [
                'as'    => 'admin.dashboard.data',
                'uses'  => 'Admin\DashboardController@index_data'
            ]);

            Route::group(['prefix' => 'profile'], function()
            {
                Route::get('/' ,[
                    'uses'  => 'Admin\UserController@getProfile',
                    'as'    => 'admin.profile',
                ]);
            });


            Route::group(['prefix' => 'orders'], function ()
            {
                Route::get('/', [
                    'as'    => 'admin.orders',
                    'uses'  => 'Admin\OrdersController@index'
                ]);

                Route::get('/index_data', [
                    'as'    => 'admin.orders.index_data',
                    'uses'  => 'Admin\OrdersController@index_data'
                ]);

                Route::post('{id}', [
                   'as'     => 'admin.order.update',
                   'uses'   => 'Admin\OrdersController@update'
                ]);

                Route::post('{id}/resend_confirmation', [
                    'as'     => 'admin.order.resend_confirmation',
                    'uses'   => 'Admin\OrdersController@resendConfirmation'
                ]);

            });

        /*
       |--------------------------------------------------------------------------
       | CSR Routes - Users
       |--------------------------------------------------------------------------
       */

            Route::group(['prefix' => 'user'], function()
            {
                // User List
                Route::get('/', [
                    'as'    => 'admin.users',
                    'uses'  => 'Admin\UserController@index'
                ]);
                // User Add - Form
                Route::get('add', [
                    'as'    => 'admin.user.add',
                    'uses'  => 'Admin\UserController@create'
                ]);
                // User Add - Action
                Route::post('add', [
                    'as' => 'admin.user.add',
                    'uses' => 'Admin\UserController@save'
                ]);

                Route::get('{contact_id}/{user_id?}', [
                    'as'    => 'admin.user_details',
                    'uses'  => 'Main\MainController@userDetails'
                ])->where([
                    'contact_id' => '[0-9]+',
                    'user_id'   => '[0-9]+'
                ]);

                Route::get('user_orders/{contact_id}/{user_id?}', [
                    'as'    => 'admin.user_orders',
                    'uses'  => 'Main\MainController@transactions_api'
                ])->where([
                    'contact_id' => '[0-9]+',
                    'user_id'   => '[0-9]+'
                ]);

                Route::get('{order_id}/remind', [
                    'as'    => 'admin.user_order.remind',
                    'uses'  => 'Admin\UserController@remind'
                ]);

                Route::post('delete-customer', [
                    'as'    => 'admin.customer.delete',
                    'uses'  => 'Admin\UserController@deleteCustomer'
                ]);

                /******************************************************/
                /*                 Registration Routes                */
                /******************************************************/

                Route::get('{user_id}/select-course', [
                    'as'    => 'admin.user.select_course',
                    'uses'  => 'Admin\CourseController@index'
                ])->where('user_id', '[0-9]+');

                Route::get('{user_id}/select-book', [
                    'as'    => 'admin.books.buy',
                    'uses'  => 'Main\BookController@buyBook',
                ]);

                Route::get('{user_id}/register/{course_id}/{class_id?}',[
                    'as'    => 'admin.contacts.register',
                    'uses'  => 'Main\ContactController@contacts'
                ]);

                /******************************************************/
                /*                 Subscription Routes                */
                /******************************************************/

                Route::get('{user_id}/yearly-subscription', [
                    'as'    => 'admin.yearly_subscription',
                    'uses'  => 'Admin\SubscriptionController@contacts'
                ])->where('user_id', '[0-9]+');

                Route::post('save-yearly-subscription', [
                    'as'    => 'admin.save_yearly_subscription',
                    'uses'  => 'Admin\SubscriptionController@save'
                ]);

                /******************************************************/
                /*                 Corporate Seat Routes              */
                /******************************************************/

                Route::get('{user_id}/corporate-seat', [
                    'as'    => 'admin.corporate_seat',
                    'uses'  => 'Admin\SubscriptionController@corporateSeats'
                ])->where('user_id', '[0-9]+');

                Route::post('save-corporate-seat', [
                    'as'    => 'admin.save_corporate_seat',
                    'uses'  => 'Admin\SubscriptionController@saveCorporateSeat'
                ]);

                Route::post('save-new-corporate-seat', [
                    'as'    => 'admin.save_new_corporate_seat',
                    'uses'  => 'Admin\SubscriptionController@saveNewCorporateSeat'
                ]);

                Route::post('delete-corporate-seat', [
                    'as'    => 'admin.delete_corporate_seat',
                    'uses'  => 'Admin\SubscriptionController@deleteCorporateSeat'
                ]);


                /******************************************************/
                /*                 Change Item/Cart Routes           */
                /******************************************************/

                Route::post('change-item-price', [
                    'as'    => 'admin.shopping_cart.item_change',
                    'uses'  => 'Main\RegistrationController@itemChange'
                ]);

                Route::get('{user_id}/shopping-cart', [
                    'as'    => 'admin.user.shopping_cart',
                    'uses'  => 'Main\RegistrationController@cart'
                ])->where('user_id', '[0-9]+');

                /******************************************************/
                /*                   Checkout Routes                  */
                /******************************************************/

                Route::get('{user_id}/checkout', [
                    'as'    => 'admin.checkout.get',
                    'uses'  => 'Main\CheckoutController@checkout'
                ])->where('user_id', '[0-9]+');

                Route::post('{user_id}/card-checkout', [
                    'as'    => 'admin.card_checkout',
                    'uses'  => 'Main\CheckoutController@createCardPayment'
                ])->where('user_id', '[0-9]+');

                Route::post('{user_id}/invoice-checkout', [
                    'as'     => 'admin.invoice_checkout',
                    'uses'   => 'Main\CheckoutController@createInvoicePayment'
                ])->where('user_id', '[0-9]+');

                Route::get('{user_id}/success-checkout', [
                    'as'    => 'admin.checkout.success',
                    'uses'  => 'Main\CheckoutController@success'
                ])->where('user_id', '[0-9]+');

                /******************************************************/
                /*               Completed Order Routes               */
                /******************************************************/

                Route::get('{user_id}/transaction/{order_item_id}', [
                    'as'        => 'admin.order.transaction',
                    'uses'      => 'Main\OrderModificationController@swapAttendees'
                ])->where('user_id', '[0-9]+');

                Route::get('{user_id}/transaction/{order_item_id}/edit-attendees', [
                    'as'        => 'admin.order.attendees.swap',
                    'uses'      => 'Main\ContactController@contacts'
                ])->where('user_id', '[0-9]+');

                Route::get('{user_id}/invoice-info/{order_id}', [
                    'as'        => 'admin.order.invoice_info',
                    'uses'      => 'Main\OrderController@invoiceInfo'
                ])->where('user_id', '[0-9]+');

                Route::get('{user_id}/invoice-info/{order_id}/checkout', [
                    'as'        => 'admin.order.invoice_info.checkout',
                    'uses'      => 'Main\OrderController@invoiceInfoCheckout'
                ])->where('user_id', '[0-9]+');

                Route::post('update-status/{user_id}',[
                    'as'        => 'admin.invoice.update-status',
                    'uses'      => 'Main\OrderController@updateOrderStatus'
                ]);

                Route::post('store-note', [
                    'as'        => 'admin.order.notes.add',
                    'uses'      => 'Main\OrderController@addNotes'
                ]);

                Route::get('{user_id}/order/{order_id}', [
                    'as'        => 'admin.order.info',
                    'uses'      => 'Main\OrderController@entireOrderInfo'
                ]);

                Route::post('query-cancellable', [
                    'as'        => 'admin.registration.query.cancellable',
                    'uses'      => 'Admin\OrderModificationController@queryIfCancellable'
                ]);

                Route::post('cancel-registration', [
                    'as'        => 'admin.registration.cancel',
                    'uses'      => 'Admin\OrderModificationController@cancelClassRegistrations'
                ]);

                Route::get('{user_id}/transaction/{order_item_id}/add-attendees', [
                    'as'        => 'admin.order.attendees.add.get',
                    'uses'      => 'Main\OrderModificationController@addAttendees'
                ]);

                Route::post('add-attendees', [
                    'as'        => 'admin.order.attendees.add.post',
                    'uses'      => 'Main\OrderModificationController@doAddAttendees'
                ]);

                Route::get('{user_id}/receipt/{order_id}', [
                    'as'        => 'admin.receipt',
                    'uses'      => 'Main\OrderController@downloadReceipt'
                ]);

                Route::get('{user_id}/payment/{order_id}', [
                    'as'        => 'admin.payment.summary',
                    'uses'      => 'Main\OrderController@paymentSummary'
                ]);

                Route::delete('delete-entire-order/{id}', [
                    'as'        => 'admin.order.delete',
                    'uses'      => 'Main\OrderController@deleteEntireOrder'
                ]);

                /******************************************************/
                /*               Profile/Account Routes               */
                /******************************************************/

                Route::get('{contact_id}/edit-profile', [
                    'as'        => 'admin.profile.edit',
                    'uses'      => 'Main\ProfileController@editProfile'
                ])->where('contact_id', '[0-9]+');

                Route::get('{user_id}/address-book', [
                    'as'        => 'admin.address_book.edit',
                    'uses'      => 'Main\ContactController@edit'
                ])->where('user_id', '[0-9]+');

                Route::post('edit-attribute/{contact_id}/{user_id?}', [
                    'as'        => 'admin.attribute.edit',
                    'uses'      => 'Main\ProfileController@saveEditOtherAttributes'
                ])->where([
                    'contact_id' => '[0-9]+',
                    'user_id' => '[0-9]+'
                ]);

                Route::post('add-email', [
                    'as'        => 'email.add',
                    'uses'      => 'Admin\UserController@addEmail'
                ]);

                Route::get('{user_id}/edit', [
                    'as'        => 'admin_user.edit',
                    'uses'      => 'Admin\UserController@showAdmin'
                ])->where('user_id', '[0-9]+');

                Route::post('/edit-email', [
                    'as'        => 'admin_user.edit_email',
                    'uses'      => 'Admin\UserController@updateAdminEmail'
                ]);

                Route::post('/edit-password', [
                    'as'        => 'admin_user.edit_password',
                    'uses'      => 'Admin\UserController@updateAdminPassword'
                ]);

                Route::post('/delete', [
                    'as'        => 'admin_user.delete',
                    'uses'      => 'Admin\UserController@deleteAdmin'
                ]);

                /******************************************************/
                /*                 OnSite Training Routes             */
                /******************************************************/

                Route::get('{user_id}/onsite-training', [
                    'as'        => 'admin.onsite_training',
                    'uses'      => 'Admin\SubscriptionController@onSiteTraining'
                ])->where('user_id', '[0-9]+');

                Route::post('{user_id}/onsite-training',
                    'Admin\SubscriptionController@addClientToOnsiteTraining'
                )->where('user_id', '[0-9]+');

                /******************************************************/
                /*                 Merge Customer Routes              */
                /******************************************************/

                Route::post('get-merge-customers', [
                    'as'        => 'customers.merge.get',
                    'uses'      => 'Admin\UserController@getCustomersToBeMerged'
                ]);

                Route::post('merge-customers', [
                    'as'        => 'customers.merge.post',
                    'uses'      => 'Admin\UserController@mergeCustomers'
                ]);
            });

            Route::group(['prefix' => 'classes'], function()
            {
                Route::post('/',[
                    'as'    => 'admin.classes.store',
                    'uses'  => 'Admin\ClassController@store'
                ]);

                Route::patch('{id}',[
                    'as'    => 'admin.classes.update',
                    'uses'  => 'Admin\ClassController@update'
                ]);

                Route::get('{id}',[
                    'as'    => 'admin.classes.show',
                    'uses'  => 'Admin\ClassController@show'
                ]);

                Route::post('associateItemToClass', [
                    'as' => 'admin.classes.link',
                    'uses' => 'Admin\ClassController@linkItem'
                ]);

                Route::post('unlinkItemToClass', [
                    'as' => 'admin.classes.unlink',
                    'uses' => 'Admin\ClassController@unlinkItem'
                ]);
            });

            /*
            |--------------------------------------------------------------------------
            | CSR Routes - Courses
            |--------------------------------------------------------------------------
            */

            Route::group(['prefix' => 'courses'], function()
            {

                Route::get('onsite-training/{transaction_id}/{user_id}', [
                    'as' => 'admin.course.onsite-training',
                    'uses' => 'Main\MainController@onsiteTrainingDetails'
                ]);

                Route::put('onsite-training/{transaction_id}/{user_id}', [
                    'as' => 'admin.course.onsite-training.update',
                    'uses' => 'Admin\SubscriptionController@updateOnsiteTraining'
                ]);

                Route::get('/', [
                    'as' => 'admin.courses',
                    'uses' => 'Admin\CourseController@index'
                ]);

                Route::get('add', [
                    'as' => 'admin.course.add',
                    'uses' => 'Admin\CourseController@create'
                ]);

                Route::post('add', [
                    'as' => 'admin.course.add',
                    'uses' => 'Admin\CourseController@save'
                ]);

                Route::post('verify-seo-link', [
                    'as' => 'admin.course.verifyseo',
                    'uses' => 'Admin\CourseController@verifySeo'
                ]);

                Route::get('{id}/edit', [
                    'as' => 'admin.course.edit',
                    'uses' => 'Admin\CourseController@edit'
                ]);

                Route::put('{id}/edit', [
                    'as' => 'admin.course.edit',
                    'uses' => 'Admin\CourseController@save'
                ]);

                Route::post('status', [
                    'as' => 'admin.course.status',
                    'uses' => 'Admin\CourseController@activateDeactivateCourse'
                ]);

                Route::post('delete', [
                    'as' => 'admin.course.delete',
                    'uses' => 'Admin\CourseController@deleteCourse'
                ]);

                Route::get('{id}/classes/',[
                    'as'    => 'admin.classes',
                    'uses'  => 'Admin\CourseController@classes'
                ]);

                Route::post('addmaterial', [
                    'as' => 'admin.course.addmaterial',
                    'uses' => 'Admin\CourseController@addMaterial'
                ]);

                Route::post('addOnsitematerial', [
                    'as' => 'admin.course.addOnsitematerial',
                    'uses' => 'Admin\CourseController@addOnsiteMaterial'
                ]);

                Route::delete('deletematerial/{id}', [
                    'as' => 'admin.course.deletematerial',
                    'uses' => 'Main\FileEntryController@destroy'
                ]);

                Route::get('getmaterial/{id}', [
                    'as' => 'admin.course.getmaterial',
                    'uses' => 'Main\FileEntryController@show'
                ]);

                Route::post('associateItemToCourse', [
                    'as' => 'admin.course.associate.item',
                    'uses' => 'Admin\CourseController@associateItemToCourse'
                ]);

                Route::post('unlinkItemToCourse', [
                    'as' => 'admin.course.unlink.item',
                    'uses' => 'Admin\CourseController@unLinkItemFromCourse'
                ]);

                Route::get('{id}/create',[
                    'as'    => 'admin.classes.create',
                    'uses'  => 'Admin\ClassController@create'
                ]);

            });

            /*
            |--------------------------------------------------------------------------
            | CSR Routes - Classes
            |--------------------------------------------------------------------------
            */

            Route::get('/class/form',[
                'as'    => 'admin.class.form',
                'uses'  => 'Admin\CourseController@getClassForm'
            ]);

            Route::post('/class/save',[
                'as'    => 'admin.class.add',
                'uses'  => 'Admin\CourseController@saveClass'
            ]);

            Route::post('/class/status',[
                'as'    => 'admin.class.status',
                'uses'  => 'Admin\CourseController@activateDeactivateClass'
            ]);

            Route::post('/class/cancel', [
                'as'    => 'admin.class.cancel',
                'uses'  => 'Admin\CourseController@cancelClass'
            ]);

            Route::post('/class/delete',[
                'as'    => 'admin.class.delete',
                'uses'  => 'Admin\CourseController@deleteClass'
            ]);

            /*
            |--------------------------------------------------------------------------
            | CSR Routes - Coupons
            |--------------------------------------------------------------------------
            */

            Route::group(['prefix' => 'coupons'], function()
            {
                Route::get('', [
                    'as'    => 'admin.coupons',
                    'uses'  => 'Admin\CouponController@index'
                ]);

                Route::get('/form', [
                    'as'    => 'admin.coupon.add',
                    'uses'  => 'Admin\CouponController@form'
                ]);

                Route::post('/save', [
                    'as'    => 'coupons.save',
                    'uses'  => 'Admin\CouponController@save'
                ]);

                Route::get('/details/{id}',[
                    'as'    => 'coupon.details',
                    'uses'  => 'Admin\CouponController@details'

                ]);

                Route::get('/edit/{id}',[
                    'as'    => 'coupons.edit',
                    'uses'  => 'Admin\CouponController@form'
                ]);

                Route::post('/delete',[
                    'as'    => 'coupons.delete',
                    'uses'  => 'Admin\CouponController@delete'
                ]);
            });

            Route::group(['prefix' => 'certificates'], function() {
                Route::get('/', [
                    'uses'  => 'Main\CertificateController@index',
                    'as'    => 'admin.certificates.index'
                ]);
                Route:;get('/api', [
                    'uses'  => 'Main\CertificateController@api',
                    'as'    => 'admin.certificates.api'
                 ]);
                Route::get('/{course_id}/course', [
                    'uses'  => 'Main\CertificateController@showCourse',
                    'as'    => 'admin.certificates.showCourse'
                ]);
                Route::get('/{class_id}', [
                    'uses'  => 'Main\CertificateController@showClass',
                    'as'    => 'admin.certificates.show'
                ]);
            });

            Route::group(['prefix' => 'reminders'], function() {
                Route::post('webcast', [
                    'as'  => 'reminders.webcast',
                    'uses'    => 'Main\ReminderController@webcast'
                ]);
            });


            Route::resource('books', 'Main\BookController');
            Route::post('books/associate', [
                'uses'  => 'Main\BookController@associateBookToCourse',
                'as'    => 'admin.books.associate'
            ]);
            Route::post('books/{id}/upload', [
                'uses'  => 'Main\BookEntryController@upload',
                'as'    => 'admin.books.upload'
            ]);
            Route::post('bookentry', [
                'uses'  => 'Main\BookController@addFileToBook',
                'as'    => 'admin.bookentry.store'
            ]);
            Route::get('bookentry/{filename}', [
                'uses'  => 'Main\BookEntryController@get',
                'as'    => 'admin.bookentry.get'
            ]);
            Route::delete('bookentry/{id}', [
                'uses'  => 'Main\BookEntryController@destroy',
                'as'    => 'admin.bookentry.delete'
            ]);


            Route::group(['prefix' => 'email'], function() {

                Route::get('/', [
                    'uses'  => 'Admin\EmailController@index',
                    'as'    => 'admin.email.index'
                ]);

                Route::post('/simulate', [
                    'uses'  => 'Admin\EmailController@simulate',
                    'as'    => 'admin.email.simulate'
                ]);

                Route::group(['prefix' => 'content'], function() {
                    Route::post('save', [
                        'uses'  => 'Admin\EmailController@contentSave',
                        'as'    => 'admin.email.content.save'
                    ]);
                });

            });


            Route::group(['prefix' => 'tips'], function() {

                Route::get('/', [
                    'uses'  => 'Admin\TipController@index',
                    'as'    => 'admin.tips.index'
                ]);

                Route::get('add', [
                    'uses'  => 'Admin\TipController@add',
                    'as'    => 'admin.tips.add'
                ]);

                Route::get('edit/{tip}', [
                    'uses'  => 'Admin\TipController@edit',
                    'as'    => 'admin.tips.edit'
                ]);

                Route::post('save', [
                    'uses'  => 'Admin\TipController@save',
                    'as'    => 'admin.tips.save'
                ]);

                Route::post('destroy', [
                    'uses'  => 'Admin\TipController@destroy',
                    'as'    => 'admin.tips.destroy'
                ]);
            });


            Route::group(['prefix' => 'regulations'], function() {

                Route::get('/', [
                    'uses'  => 'Admin\RegulationController@index',
                    'as'    => 'admin.regulations.index'
                ]);

                Route::get('add', [
                    'uses'  => 'Admin\RegulationController@add',
                    'as'    => 'admin.regulations.add'
                ]);

                Route::get('edit/{regulation}', [
                    'uses'  => 'Admin\RegulationController@edit',
                    'as'    => 'admin.regulations.edit'
                ]);

                Route::post('save', [
                    'uses'  => 'Admin\RegulationController@save',
                    'as'    => 'admin.regulations.save'
                ]);
                Route::post('destroy', [
                    'uses'  => 'Admin\RegulationController@destroy',
                    'as'    => 'admin.regulations.destroy'
                ]);
            });


            Route::group(['prefix' => 'links'], function() {

                Route::get('/', [
                    'uses'  => 'Admin\LinkController@index',
                    'as'    => 'admin.links.index'
                ]);

                Route::get('add', [
                    'uses'  => 'Admin\LinkController@add',
                    'as'    => 'admin.links.add'
                ]);

                Route::get('edit/{link}', [
                    'uses'  => 'Admin\LinkController@edit',
                    'as'    => 'admin.links.edit'
                ]);

                Route::post('save', [
                    'uses'  => 'Admin\LinkController@save',
                    'as'    => 'admin.links.save'
                ]);
            });

            Route::group(['prefix' => 'posts'], function() {

                Route::get('/', [
                    'as'    => 'admin.posts.index',
                    'uses'  => 'Admin\PostController@index'
                ]);

                Route::post('/save', [
                    'as'    => 'admin.posts.save',
                    'uses'  => 'Admin\PostController@save'
                ]);

            });

            Route::group(['prefix' => 'instructors'], function() {

                Route::get('/', [
                    'as'    => 'instructor.index',
                    'uses'  => 'Admin\InstructorController@index'
                ]);

                Route::get('/create', [
                    'as'    => 'instructor.create',
                    'uses'  => 'Admin\InstructorController@create'
                ]);

                Route::post('/store', [
                    'as'    => 'instructor.store',
                    'uses'  => 'Admin\InstructorController@store'
                ]);

                Route::get('{id}/edit', [
                    'as'    => 'instructor.edit',
                    'uses'  => 'Admin\InstructorController@edit'
                ]);

                Route::post('{id}', [
                    'as'    => 'instructor.update',
                    'uses'  => 'Admin\InstructorController@update'
                ]);

                Route::post('{id}/delete', [
                    'as'    => 'instructor.remove',
                    'uses'  => 'Admin\InstructorController@remove'
                ]);
            });

            Route::group(['prefix' => 'locations'], function() {
                Route::post('/', [
                    'as'    => 'admin.locations.store',
                    'uses'  => 'Admin\LocationsController@store'
                ]);
                Route::get('/', [
                    'as'    => 'admin.locations.index',
                    'uses'  => 'Admin\LocationsController@index'
                ]);
                Route::get('{id}/edit', [
                    'as'    => 'admin.locations.edit',
                    'uses'  => 'Admin\LocationsController@edit'
                ]);
                Route::put('{id}', [
                    'as'    => 'admin.locations.update',
                    'uses'  => 'Admin\LocationsController@update'
                ]);
                Route::get('create', [
                    'as'    => 'admin.locations.create',
                    'uses'  => 'Admin\LocationsController@create'
                ]);
                Route::delete('{id}/delete', [
                    'as'    => 'admin.locations.delete',
                    'uses'  => 'Admin\LocationsController@destroy'
                ]);
            });

            Route::resource('general_locations', 'Admin\GeneralLocationController');

            /******************************************************/
            /*                    Notes Routes                    */
            /******************************************************/


            Route::post('note/save', [
                'as'        => 'customer.note.save',
                'uses'      => 'Admin\UserController@saveNote'
            ]);


            /******************************************************/
            /*      Order Modification Checkout Routes            */
            /******************************************************/

            Route::group(['prefix' => 'checkout'], function()
            {

                Route::get('order-modification/{order_id}', [
                    'as'    => 'admin.order_mod.checkout',
                    'uses'  => 'Main\OrderModificationCheckoutController@checkout'
                ]);

                Route::post('order-modification/credit-card-payment', [
                    'as'    => 'admin.order_mod.checkout.credit_card',
                    'uses'  => 'Main\OrderModificationCheckoutController@createCreditCardPayment'
                ]);

                Route::post('order-modification/invoice-payment', [
                    'as'    => 'admin.order_mod.checkout.invoice',
                    'uses'  => 'Main\OrderModificationCheckoutController@createInvoicePayment'
                ]);
            });

            Route::group(['prefix' => 'order-modification'], function()
            {
                Route::post('change-price', [
                    'as'    => 'order_mod.change_price',
                    'uses'  => 'Admin\OrderModificationController@changePrice'
                ]);

                Route::post('adjustment-change-price', [
                    'as'    => 'order_mod.adjustment.change_price',
                    'uses'  => 'Admin\OrderModificationController@changePriceAdjustment'
                ]);

                Route::post('adjustment-remove', [
                    'as'    => 'order_mod.adjustment.remove',
                    'uses'  => 'Admin\OrderModificationController@removeAdjustment'
                ]);

                Route::post('restore-cancelled', [
                    'as'    => 'order_mod.cancelled.restore',
                    'uses'  => 'Admin\OrderModificationController@restoreCancelled'
                ]);
            });

            Route::group(['prefix' => 'reports'], function()
            {
                Route::get('class', [
                    'as'    => 'reports.class',
                    'uses'  => 'Admin\ReportController@classes'
                ]);

                Route::get('coupon', [
                    'as'    => 'reports.coupon',
                    'uses'  => 'Admin\ReportController@coupons'
                ]);

                Route::get('generate-class', [
                    'as'    => 'reports.class.generate',
                    'uses'  => 'Admin\ReportController@generateClass'
                ]);

                Route::get('generate-coupon', [
                    'as'    => 'reports.coupon.generate',
                    'uses'  => 'Admin\ReportController@generateCoupon'
                ]);

            });

            Route::get('reschedule/{orderItemId}/{contactId}', [
                'as'    => 'reschedule.edit',
                'uses'  => 'Admin\RescheduleController@edit'
            ]);

            Route::post('reschedule/{orderItemId}', [
                'as'    => 'reschedule.store',
                'uses'  => 'Admin\RescheduleController@store'
            ]);

            Route::get('state_handouts', [
                'as'    => 'admin.state_handouts.index',
                'uses'  => 'Admin\StateHandoutController@index'
            ]);

            Route::get('state_handouts/create', [
                'as'    => 'admin.state_handouts.create',
                'uses'  => 'Admin\StateHandoutController@create'
            ]);

            Route::post('state_handouts/store', [
                'as'    => 'admin.state_handouts.store',
                'uses'  => 'Admin\StateHandoutController@store'
            ]);

            Route::delete('state_handouts/destroy', [
                'as'    => 'admin.state_handouts.destroy',
                'uses'  => 'Admin\StateHandoutController@destroy'
            ]);

            Route::get('state_handouts/{id}/edit', [
                'as'    => 'admin.state_handouts.edit',
                'uses'  => 'Admin\StateHandoutController@edit'
            ]);

            Route::patch('state_handouts/{id}', [
                'as'    => 'admin.state_handouts.update',
                'uses'  => 'Admin\StateHandoutController@update'
            ]);

        });

        Route::resource('admin/coupon_landing', 'Main\CouponLandingController');
        Route::resource('admin/topics', 'Admin\TopicsController');
        Route::get('admin/reminder-history', [
            'as'    => 'admin.reminder-history.index',
            'uses'  => 'Admin\ReminderHistoryController@index'
        ]);
        Route::post('admin/reminder-history/resend/{id}', [
            'as'    => 'admin.reminder-history.resend',
            'uses'  => 'Admin\ReminderHistoryController@resend'
        ]);
        Route::get('admin/reminder-history/trigger', [
            'as'    => 'admin.reminder-history.trigger',
            'uses'  => 'Admin\ReminderHistoryController@trigger'
        ]);

        /******************************************************/
        /*                  Promos Routes                     */
        /******************************************************/

        Route::get('promos', [
            'as'    => 'promos.index',
            'uses'  => 'Admin\PromoController@index'
        ]);

        Route::post('promo-form', [
            'as'    => 'promo.form',
            'uses'  => 'Admin\PromoController@form'
        ]);

        Route::post('promo-save', [
            'as'    => 'promo.save',
            'uses'  => 'Admin\PromoController@save'
        ]);

        Route::post('promo-activate', [
            'as'    => 'promo.activate',
            'uses'  => 'Admin\PromoController@activate'
        ]);

        Route::post('promo-deactivate', [
            'as'    => 'promo.deactivate',
            'uses'  => 'Admin\PromoController@deactivate'
        ]);

        Route::post('promo-delete', [
            'as'    => 'promo.delete',
            'uses'  => 'Admin\PromoController@delete'
        ]);

        Route::post('promo-order', [
            'as'    => 'promo.order',
            'uses'  => 'Admin\PromoController@order'
        ]);

        Route::post('promo-ordersave', [
            'as'    => 'promo.ordersave',
            'uses'  => 'Admin\PromoController@ordersave'
        ]);
    });