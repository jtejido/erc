<?php

    Route::get('/', [
        'as'     => 'home',
        'uses'   => 'Main\MainController@index'
    ]);

    Route::get('/articles', [
        'as'    => 'home.articles',
        'uses'  => 'Main\MainController@articles'
    ]);

    Route::get('/promotions', [
        'as'    => 'home.promos',
        'uses'  => 'Main\MainController@promos'
    ]);

    Route::group(['middleware' => 'session.expired'], function()
    {
        Route::post('session-expired', [
            'as'     => 'session.expired',
            'uses'   => 'Auth\AuthController@sessionExpired'
        ]);
    });

    Route::group(['prefix' => 'test', 'namespace' => 'Test'], function()
    {
        Route::get('', [
            'as'     => 'test.index',
            'uses'   => 'Controller@index'
        ]);

        Route::get('forget', [
            'as'     => 'test.forget',
            'uses'   => 'Controller@forget'
        ]);

        Route::group(['prefix' => 'emails'], function()
        {
            Route::get('', [
                'as'     => 'test.emails.index',
                'uses'   => 'EmailController@index'
            ]);
        });
    });

    Route::post('reactivate-user-status', [
        'as'        => 'user.reactivate',
        'uses'      => 'Main\ProfileController@requestReactivateAccount'
    ]);

    Route::group(['middleware' => 'guest'], function()
    {
        Route::get('login', [
            'as'     => 'get.login',
            'uses'   => 'Auth\AuthController@getLogin'
        ]);

        Route::get('register', [
            'as'        => 'user.register',
            'uses'      => 'Auth\AuthController@getRegister'
        ]);

        Route::get('verify', 'Auth\AuthController@verifyMessage');
        Route::get('verify/{verification_id}', [
            'as'    => 'user.verifier',
            'uses'  => 'Auth\AuthController@getVerify'
        ]);
    });

    /*
    |--------------------------------------------------------------------------
    | Login/logout Routes
    |--------------------------------------------------------------------------
    */



    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout');

    /*
    |--------------------------------------------------------------------------
    | Registration Routes
    |--------------------------------------------------------------------------
    */

    Route::post('register', 'Auth\AuthController@postRegister');


    /*
    |--------------------------------------------------------------------------
    | Rest Password Routes
    |--------------------------------------------------------------------------
    */

    Route::post('password/email', [
        'as'        => 'password.post',
        'uses'      => 'Auth\PasswordController@postEmail'
    ]);

    Route::get('password/reset/{token}', [
        'as'        => 'password_reset_token.get',
        'uses'      => 'Auth\PasswordController@getReset'
    ]);

    Route::post('password/reset', [
        'as'        => 'password_reset.post',
        'uses'      => 'Auth\PasswordController@postReset'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Courses Routes
    |--------------------------------------------------------------------------
    */

    Route::any('courses', [
        'as'    => 'courses',
        'uses'  => 'Main\CourseController@index'
    ]);

    Route::get('courses/{id}', [
        'as'    => 'course.show',
        'uses'  => 'Main\CourseController@getDetails'
    ]);

    Route::get('downloadmaterial/{filename}', [
        'as' => 'main.course.getmaterial',
        'uses' => 'Main\FileEntryController@showFile'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Classes Routes
    |--------------------------------------------------------------------------
    */

    Route::get('classes', [
        'as'    => 'classes',
        'uses'  => 'Main\ClassesController@index'
    ]);

    Route::get('classes/{slug}', [
        'as'    => 'classes.slug.get',
        'uses'  => 'Main\ClassesController@index'
    ]);


    /******************************************************/
    /*                Products Routes                     */
    /******************************************************/

    Route::get('products', [
        'as'        => 'products',
        'uses'      => 'Main\BookController@index'
    ]);

    Route::get('products/{bookId}', [
        'as'        => 'products.show',
        'uses'      => 'Main\BookController@show'
    ]);

    Route::get('books/thumbnail/{filename}', [
        'as'    => 'book.thumbnail',
        'uses'  => 'Main\BookController@getThumbnail'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Tips of the Week Routes
    |--------------------------------------------------------------------------
    */


    Route::group(['prefix' => 'tips'], function() {

        Route::get('/', [
            'uses'  => 'Main\TipController@index',
            'as'    => 'main.tips.index'
        ]);

        Route::get('show/{slug}', [
            'uses'  => 'Main\TipController@show',
            'as'    => 'main.tips.edit'
        ]);
    });

    /*
    |--------------------------------------------------------------------------
    | Reg of the Day Routes
    |--------------------------------------------------------------------------
    */


    Route::group(['prefix' => 'regulations'], function() {

        Route::get('/', [
            'uses'  => 'Main\RegulationController@index',
            'as'    => 'main.regulations.index'
        ]);

        Route::get('show/{slug}', [
            'uses'  => 'Main\RegulationController@show',
            'as'    => 'main.regulations.edit'
        ]);
    });


    /*
    |--------------------------------------------------------------------------
    | Helpful Links
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'links'], function() {

        Route::get('/', [
            'uses'  => 'Main\LinkController@index',
            'as'    => 'main.links.index'
        ]);
    });


    /*
    |--------------------------------------------------------------------------
    | Helpful Links
    |--------------------------------------------------------------------------
    */
    Route::group(['prefix' => 'class_calendar'], function() {

        Route::get('/', [
            'uses'  => 'Main\ClassCalendarController@index',
            'as'    => 'resource.classcalendar'
        ]);

        Route::get('/data', [
            'uses'  => 'Main\ClassCalendarController@index_data',
            'as'    => 'resource.classcalendar.data'
        ]);

    });


    Route::get('cp/{uuid}/preview', [
        'as' => 'cp.landing.preview',
        'uses' => 'Main\CouponLandingController@preview'
    ]);

    Route::get('cp/{uuid}/{to}', [
        'as' => 'cp.redirect',
        'uses' => 'Main\CouponLandingController@redirect'
    ]);

    Route::get('cp/{uuid}/{to}/{class_id}', [
        'as' => 'cp.redirect.class',
        'uses' => 'Main\CouponLandingController@redirect'
    ]);

    Route::get('cp/session', [
        'as' => 'cp.session',
        'uses' => 'Main\CouponLandingController@getCoupon'
    ]);

    Route::get('cp/{uuid}', [
        'as' => 'cp.landing',
        'uses' => 'Main\CouponLandingController@view'
    ]);



    Route::group(['prefix' => 'r'], function() {

        Route::get('m/{uuid}', [
            'as' => 'redirect.meeting',
            'uses' => 'Main\RedirectController@meeting'
        ]);

        Route::post('m/{uuid}', [
            'as' => 'redirect.meeting.update',
            'uses' => 'Main\RedirectController@updateMeeting'
        ]);

        Route::get('e/{uuid}', [
            'as' => 'redirect.exam',
            'uses' => 'Main\RedirectController@exam'
        ]);

        Route::post('e/{uuid}', [
            'as' => 'redirect.exam',
            'uses' => 'Main\RedirectController@updateExam'
        ]);

        Route::get('hook', [
            'as' => 'redirect.hook',
            'uses' => 'Main\RedirectController@hook'
        ]);

        Route::post('hook', [
            'as' => 'redirect.hook',
            'uses' => 'Main\RedirectController@hook'
        ]);

    });

    /*
    |--------------------------------------------------------------------------
    | Global authenticated Routes
    |--------------------------------------------------------------------------
    */

    Route::group(['middleware' => 'auth'], function()
    {

        /******************************************************/
        /*                 Contact Routes                     */
        /******************************************************/

        Route::group(['prefix' => 'contact'], function()
        {

            Route::get('/form', [
                'as'    => 'contact.form.get',
                'uses'  => 'Main\ContactController@form'
            ]);

            Route::post('/add', [
                'as'    => 'contact.add',
                'uses'  => 'Main\ContactController@store'
            ]);

            Route::post('delete', [
                'as'    => 'contact.delete',
                'uses'  => 'Main\ContactController@delete'
            ]);

            Route::post('/add-to-class', [
                'as'    => 'contact.add_to_class',
                'uses'  => 'Main\RegistrationController@addContactsToClass'
            ]);

            Route::post('check-email', [
                'as'    => 'email.check',
                'uses'  => 'Main\ContactController@checkEmail'
            ]);
        });

        Route::post('remove-item', [
            'as'    => 'cart.remove_item',
            'uses'  => 'Main\RegistrationController@removeItem'
        ]);

        Route::post('apply-coupon', [
            'as'    => 'coupon.apply',
            'uses'  => 'Admin\CouponController@applyCoupon'
        ]);

        Route::post('ajax-validate-file', [
            'as'    => 'ajax.file.validate',
            'uses'  => 'Main\FileController@ajaxValidateInvoiceFile'
        ]);

        Route::post('complete-order', [
            'as'    => 'order.complete',
            'uses'  => 'Main\RegistrationController@completeOrder'
        ]);

        Route::get('payment/{order_id}', [
            'as'        => 'payment.summary',
            'uses'      => 'Main\OrderController@paymentSummary'
        ]);

        Route::get('receipt/{order_id}', [
            'as'        => 'receipt',
            'uses'      => 'Main\OrderController@downloadReceipt'
        ]);

        /******************************************************/
        /*                 Profile Routes                     */
        /******************************************************/

        Route::post('email-edit-profile', [
            'as'        => 'email.edit_profile',
            'uses'      => 'Main\ProfileController@saveEditEmail'
        ]);

        Route::post('password-edit-profile', [
            'as'        => 'password.edit_profile',
            'uses'      => 'Main\ProfileController@saveEditPassword'
        ]);

        Route::post('personal-info-edit-profile', [
            'as'        => 'personal_info.edit_profile',
            'uses'      => 'Main\ProfileController@saveEditPersonalInfo'
        ]);

        Route::post('same-as-billing-or-shipping-edit-profile', [
            'as'        => 'same_as_billing_or_shipping.edit_profile',
            'uses'      => 'Main\ProfileController@saveEditSameAsBillingOrShipping'
        ]);

        Route::post('same-as-address', [
            'as'       => 'same_as_address',
            'uses'     => 'Main\ProfileController@saveEditSameAsAddress'
        ]);

        Route::post('toggle-user-status', [
            'as'        => 'user.toggleUserStatus',
            'uses'      => 'Main\ProfileController@toggleUserStatus'
        ]);

        /******************************************************/
        /*                 Certificate Routes                 */
        /******************************************************/

        Route::post('printcert', [
            'as'        => 'generate.cert',
            'uses'      => 'Main\CertificateController@saveCertificate'
        ]);

        Route::get('printcert/{user_id}/{regOrderId}/regenerate', [
            'as'        => 'regenerate.cert',
            'uses'      => 'Main\CertificateController@regenerateCertificate'
        ]);

        Route::post('printcert/status', [
            'as'        => 'status.cert',
            'uses'      => 'Main\ClassRegistrationController@update'
        ]);

        Route::post('printcert/statusAll', [
            'as'        => 'statusAll.cert',
            'uses'      => 'Main\ClassRegistrationController@updateAll'
        ]);

        Route::get('printcert/{user_id}/{regOrderId}/preview', [
            'as'        => 'preview.cert',
            'uses'      => 'Main\CertificateController@previewCertificate'
        ]);

        Route::get('printcert/{user_id}/{regOrderId}/download', [
            'as'        => 'download.cert',
            'uses'      => 'Main\CertificateController@downloadCertificate'
        ]);

        Route::get('printcert/{class_id}/download', [
            'as'        => 'download.certs',
            'uses'      => 'Main\CertificateController@downloadCertificates'
        ]);

        Route::get('printcert/{class_id}/roster', [
            'as'        => 'download.roster',
            'uses'      => 'Main\CertificateController@downloadRoster'
        ]);

        Route::get('printcert/{course_id}/roster-course', [
            'as'        => 'download.course.roster',
            'uses'      => 'Main\CertificateController@downloadCourseRoster'
        ]);

        Route::get('printcert/{class_id}/signsheet', [
            'as'        => 'download.signsheet',
            'uses'      => 'Main\CertificateController@downloadSignSheet'
        ]);

        Route::get('printcert/{class_id}/nameplate', [
            'as'        => 'download.nameplate',
            'uses'      => 'Main\CertificateController@downloadNamePlate'
        ]);

        Route::get('printcert/{class_id}/all', [
            'as'        => 'download.all',
            'uses'      => 'Main\CertificateController@downloadAll'
        ]);

        Route::get('printcert/{course_id}/downloadcourse', [
            'as'        => 'download.course.certs',
            'uses'      => 'Main\CertificateController@downloadCertificatesOfCourse'
        ]);

        Route::get('printcert/{regOrderId}/downloads', [
            'as'        => 'download.users.certs',
            'uses'      => 'Main\CertificateController@downloadCertificatesPerRegistration'
        ]);

        Route::post('printcert/batch', [
            'as'        => 'generate_batch.cert',
            'uses'      => 'Main\CertificateController@generateByBatchCertificate'
        ]);

        /******************************************************/
        /*                   Products Routes                  */
        /******************************************************/

        Route::post('products/cart', [
            'as'        => 'products.cart.add',
            'uses'      => 'Main\BookController@addBookToCart'
        ]);

        Route::post('products/cart/update', [
            'as'        => 'products.cart.update',
            'uses'      => 'Main\BookController@updateBookQuantityOnCart'
        ]);

        /******************************************************/
        /*               Corporate Credit Routes              */
        /******************************************************/

        Route::post('give-credit', [
            'as'        => 'credit.give',
            'uses'      => 'Admin\SubscriptionController@giveCredit'
        ]);

        Route::post('remove-credit', [
            'as'        => 'credit.remove',
            'uses'      => 'Admin\SubscriptionController@removeCredit'
        ]);

        /******************************************************/
        /*                  Invoice Routes                    */
        /******************************************************/

        Route::get('invoice-temp-preview/{file_name}', [
            'as'        => 'invoice_image_temp.preview',
            'uses'      => 'Main\FileController@invoiceTempPreview'
        ]);



        Route::post('invoice-download', [
            'as'        => 'invoice_image.download',
            'uses'      => 'Main\FileController@downloadInvoice'
        ]);

        Route::post('remove-download', [
            'as'        => 'download.remove',
            'uses'      => 'Main\FileController@deleteDownloadedInvoice'
        ]);

        /******************************************************/
        /*                  Signature Route                   */
        /******************************************************/

        Route::get('signature/{name}', [
            'as'        => 'signature.preview',
            'uses'      => 'Main\FileController@signaturePreview'
        ]);

        /******************************************************/
        /*                  Swap Attendees Route              */
        /******************************************************/

        Route::post('swap-attendees', [
            'as'        => 'attendees.do_swap',
            'uses'      => 'Main\OrderModificationController@doSwapAttendees'
        ]);

        Route::post('swap-notification', [
            'as'        => 'attendees.do_swap.notification',
            'uses'      => 'Main\OrderModificationController@swapNotification'
        ]);

        /******************************************************/
        /*                  Order Route                       */
        /******************************************************/

        Route::post('cancel-new-class-registrations', [
            'as'        => 'new_registrations.cancel',
            'uses'      => 'Main\OrderModificationController@cancelNewRegistrations'
        ]);

        Route::post('classes/register', [
            'as'    => 'classes.register.store',
            'uses'  => 'Main\RegistrationController@addContactsToClasses'
        ]);

        Route::post('confirm-order-modification', [
            'as'    => 'order_mod.confirm',
            'uses'  => 'Main\OrderModificationController@confirmOrderModification'
        ]);

        Route::post('add-to-subscription', [
            'as'    => 'contact.add_to_subscription',
            'uses'  => 'Admin\SubscriptionController@addContactsToSubscription'
        ]);

        Route::post('add-to-credit', [
            'as'    => 'client.add_to_credit',
            'uses'  => 'Admin\SubscriptionController@addClientToCredit'
        ]);

        Route::post('get-activation', [
            'as'    => 'activation.get',
            'uses'  => 'Admin\SubscriptionController@getActivation'
        ]);

        Route::post('post-activation', [
            'as'    => 'activation.post',
            'uses'  => 'Admin\SubscriptionController@doActivation'
        ]);
    });

    Route::get('tip-photo/{id}', [
        'as'  => 'tip.photo',
        'uses'  => 'Admin\TipController@tipPhoto'
    ]);


    Route::get('reg-photo/{id}', [
        'as'  => 'reg.photo',
        'uses'  => 'Admin\RegulationController@regPhoto'
    ]);

    Route::get('invoice-preview/{type}/{order_id}/{invoice_id}/{file_name}', [
        'as'        => 'invoice_image.preview',
        'uses'      => 'Main\FileController@invoiceCompletedPreview'
    ]);

    Route::post('ups-shipping-rate', [
        'as'        => 'shipping.ups',
        'uses'      => 'ApiController@ups'
    ]);

    Route::post('contact-request', [
        'as'        => 'contact.us.send',
        'uses'      => 'Main\PostController@send'
    ]);

    Route::get('state-handouts', [
        'as'        => 'main.state_handouts',
        'uses'      => 'Main\StateHandoutController@index'
    ]);