<?php

namespace App\Http\Middleware;

use Session;
use Closure;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;

class SessionTimeout
{

    protected $session;
    protected $timeout = 6000;

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bag = Session::getMetadataBag();

        $max = ((config('session.lifetime') * 60) - 1200); // min to hours conversion

        if (($bag && $max < (time() - $bag->getLastUsed()))) {

            $this->session->flash('session_expired', 'Your session has expired. Please login again to do further transactions.');
            $this->session->forget('lastActivityTime');
        }

        else {

            if (Auth::check()) {
                $this->session->put('lastActivityTime', time());
                Session::forget('last_activity_url');
            }
        }

        return $next($request);
    }
}
