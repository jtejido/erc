<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Facades\OrderUtility;
use App\Repositories\OrderItemRepository;
use App\Utilities\Constant;

class TransactionAgent
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * @var OrderItemRepository
     */
    protected $orderItemRepo;

    /**
     * Create a new filter instance.
     *
     * @param Guard $auth
     * @return void
     */
    public function __construct(Guard $auth,
                                OrderItemRepository $orderItemRepo)
    {
        $this->auth = $auth;
        $this->orderItemRepo = $orderItemRepo;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $orderId = $request->route('order_id');
        $orderItemId = $request->route('order_item_id');
        $transactionId = $request->route('transaction_id');

        if ($this->auth->guest()) {
            return redirect()->route('get.login');
        }

        if (!$this->auth->user()->hasRole(Constant::ROLE_MEMBER)) {
            if (isset($transactionId)) {
                return $next($request);
            }

            return redirect()->route('admin.dashboard');
        }

        if (isset($orderId)) {
            $isAgent = OrderUtility::isOriginatingAgent($orderId, $this->auth->user()->id);

            if (!$isAgent) {
                return redirect()->route('user.home');
            }
        }

        if (isset($orderItemId) or isset($transactionId)) {
            $itemId = isset($orderItemId) ? $orderItemId : (isset($transactionId) ? $transactionId : null);
            $orderItem = $this->orderItemRepo->getById($itemId);

            if (!$orderItem) {
                return redirect()->route('user.home');
            }

            $order = $orderItem->order;
            $isAgent = OrderUtility::isOriginatingAgent($order->id, $this->auth->user()->id);
            $isActingAgent = OrderUtility::isActingAgent([
                'orderable_id' => $orderItem->orderable_id,
                'orderable_type' => $orderItem->orderable_type,
                'acting_agent_id' => $this->auth->user()->id,
            ]);

            if (!$isAgent and !$isActingAgent) {
                return redirect()->route('user.home');
            }
        }

        return $next($request);
    }
}
