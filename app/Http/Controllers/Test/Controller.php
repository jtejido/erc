<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller as BaseController;
use Carbon\Carbon;
use Session;
use DB;

class Controller extends BaseController
{
    public function __construct()
    {
        $this->beforeFilter(function() {
            if (!Session::has('TEST-TOKEN')) {
                $token = md5(Carbon::now()->toDateTimeString());
                Session::put('TEST-TOKEN', $token);
                \Log::info('Test token init: '.Session::get('TEST-TOKEN'));
                DB::table('email_logs')->truncate();
            }
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        \Log::info('Test token: '.Session::get('TEST-TOKEN'));

        return $this->json(true, [], 'Success');
    }

    public function forget() {
        Session::forget('TEST-TOKEN');
        return $this->json(true, [], 'Success');
    }
}
