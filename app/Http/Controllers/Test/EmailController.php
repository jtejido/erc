<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller as BaseController;
use App\Http\Requests;
use App\Models\EmailLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class EmailController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->json(true, EmailLog::all(), 'Success');
    }
}
