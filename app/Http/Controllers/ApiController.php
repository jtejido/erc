<?php
/**
 * Created by PhpStorm.
 * User: greghermo
 * Date: 31/05/2017
 * Time: 2:55 PM
 */

namespace App\Http\Controllers;

use App\Models\State;
use App\Models\StateTax;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Contact;
use App\Models\Order;
use App\Traits\UpsShippingTrait;

class ApiController extends Controller
{
    use UpsShippingTrait;

    public function listCompanies()
    {

        $term = request('term', '');

        $contacts = Contact::select('company')
            ->where('company', 'like', $term.'%')
            ->groupBy('company')
            ->get();
        $companies = $contacts->map(function ($contact) {
            return $contact->company;
        })->toArray();
        return $companies;
    }

    public function ups(Request $request)
    {
        if($request->ajax()){

            $data                   = [];
            $zip                    = $request->get('zip');
            $state_val              = $request->input('state', 'Alabama');
            $county                 = $request->input('county', 0);
            $county                 = empty(trim($county)) ? 0 : $county;
            $order_id               = $request->get('order_id');
            $order                  = Order::findOrFail($order_id);
            $current_total          = $order->current_total;
            $current_product_total  = $order->current_product_total;
            $is_pr                  = ($state_val == 'Puerto Rico') ? true : false;
            $total                  = $current_total;

            if(strlen($zip) < 5) {
                $shipping_rate      = 0;
                $order->update(['shipping_zip'      =>  $request->get('zip'),
                                'shipping_rates'    => 0.00,
                                'total'             => (float) $current_total]);
            } else {
                $items_weight       = $request->has('weight') ? (float) $request->get('weight') : 10;
                $items              = $this->packages_chunk($items_weight);
                $shipping_rate      = $this->ship_rate($items, $zip, $is_pr);
            }

            $current_product_total += $shipping_rate;
            $total += $shipping_rate;

            $data['shipping_rates'] = $shipping_rate;

            $data['hasTax'] = ($county != 0) ? 1 : 0;

            if($data['hasTax']) {
                $state_tax                  = StateTax::find($county);
                $data['state_tax_percent']  = $state_tax->total_tax;
                $data['state_tax']          = getPercentageAmountFromTotal($state_tax->total_tax,
                                                $current_product_total);
                $total += $data['state_tax'];
            } else {
                $data['state_tax_percent']  = 0;
                $data['state_tax']          = 0;
            }

            $data['total']          = $total;

            return response()->json($data, 200);
        }
    }

}