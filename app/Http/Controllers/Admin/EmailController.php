<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\EmailSimulateRequest;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Services\Mailer\SimulationMailer;
use App\Utilities\Constant;
use Illuminate\Http\Request;
use Session;
use Log;

class EmailController extends BaseController
{

    public function index()
    {
        $this->data['page']    = 'emails';
        $this->data['emails'] = EmailTemplate::all();
         
        return view('admin.emails.index', $this->data);
    }

    public function simulate(EmailSimulateRequest $request,
                             SimulationMailer $mailer)
    {
        $data = $request->all();
        $mailer->send($data);
        exit;
        error_log(print_r($request->all(), true));
    }

    public function contentSave(Request $request)
    {
        $data = $request->all();
        
        /*
         * Base64 is required to minimize the size of transmitted data
         */
        $_content = base64_decode($data['content']['content']);
        $template_id = $data['template_id'];

        $message = 'Successfully updated';
        if (array_key_exists('id', $data['content'])) {
            $content = EmailTemplateContent::find($data['content']['id']);
            $content->content = $_content;
            $content->save();
        } else {
            $content = new EmailTemplateContent([
                'content'           => $_content,
                'email_template_id' => $template_id
            ]);
            EmailTemplate::find($template_id)->contents()->save($content);
            $message = 'Successfully saved';
        }

        return $this->json(true, array_except($content->toArray(), 'content'), $message);
    }



}
