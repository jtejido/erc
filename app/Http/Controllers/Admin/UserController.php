<?php namespace App\Http\Controllers\Admin;

use App\Facades\ReminderHistoryService;
use App\Models\Order;
use App\Utilities\Constant;
use Auth;
use Session;
use App\Models\User;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Repositories\RoleRepository;
use App\Repositories\StateRepository;
use App\Repositories\UserRepository;
use App\Repositories\ContactRepository;
use App\Services\UserService;
use App\Services\ContactService;
use Illuminate\Http\Request;
use App\Http\Requests\AddEmailRequest;
use App\Events\CustomerRegisteredEvent;
use App\Facades\CourseMillService;
use App\Http\Requests\EditEmailRequest;
use App\Http\Requests\EditPasswordRequest;
use App\Events\AssociateContactAccountEvent;
use App\Models\CustomerNote;

class UserController extends BaseController
{
    /**
     * @var StateRepository
     */
    private $stateRepo;

    /**
     * @param StateRepository $stateRepo
     */
    public function __construct(StateRepository $stateRepo)
    {
        $this->stateRepo = $stateRepo;
    }

    /**
     * @param UserRepository $userRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(UserRepository $userRepo)
    {
        $this->data['page']    = 'add_user';
        $this->data['users'] = $userRepo->management();
        return view('admin.users.admin.index', $this->data);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(RoleRepository $rolesRepository)
    {
        $roles = [];

        $rolesRepository->all(true)->each(function($role) use (&$roles) {
           $roles[$role->id] = $role->label;
        });

        $this->data['page']   = 'users';
        $this->data['roles']  = $roles;
        return view('admin.users.admin.add', $this->data);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id, RoleRepository $rolesRepository)
    {
        $this->data['page']   = 'edit_user';
        $this->data['user']   = $this->userRepo->getById($id, true);
        $this->data['states'] = $this->stateRepo->getByList('state', 'state')->toArray();

        return view('admin.users.add_edit', $this->data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function save(UserRequest $request, UserService $userService)
    {
        try {
            if ($request->isMethod('POST')) {

                $role = $request->get('role');
                $user = $userService->save($request->all());

                $route = ($role == Constant::ADMIN or $role == Constant::SUPER_ADMIN)
                         ? 'admin.users'
                         : 'admin.dashboard';

                return redirect()
                        ->route($route)
                        ->with([
                            'success_message' => 'User successfully added.',
                            'user'  => $user
                        ]);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Get customers to be merged
     *
     * @param Request $request
     * @param ContactRepository $contactRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getCustomersToBeMerged(Request $request,
                                           ContactRepository $contactRepo)
    {
        try {

            $ids = $request->get('customer_ids');
            $this->data['customers'] = $contactRepo->getByIds($ids);

            return view('admin.includes.select_merge_customers', $this->data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Do merge customers
     *
     * @param Request $request
     * @param ContactService $contactService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function mergeCustomers(Request $request,
                                   ContactService $contactService)
    {
        try {
            $masterId = $request->get('customer_id');
            $slaveIds = $request->get('contacts');

            $response = $contactService->merge($masterId, $slaveIds);

            if ($response) {

                return redirect()->back()
                                 ->with('success_message', 'Training history and address book entries moved.');
            }


        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Associate and email to an existing user
     *
     * @param AddEmailRequest $request
     * @param RoleRepository $roleRepo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function addEmail(AddEmailRequest $request,
                             RoleRepository $roleRepo)
    {
        try {
            $email = $request->get('email');
            $contactId = $request->get('contact_id');
            $password = mt_rand(10000000, 99999999);//str_random(10);

            $user = User::create([
                'email' => $email,
                'password'  => bcrypt($password),
                'contact_id' => $contactId,
                'state' => 1
            ]);

            $role = $roleRepo->findOne(['role' => Constant::ROLE_MEMBER]);

            $user->assignRole($role);

            if ($user) {
                event(new CustomerRegisteredEvent($user, $password));
                event(new AssociateContactAccountEvent($user->id));

                if (!is_null($user->contact->course_mill_user_id)) {
                    CourseMillService::createUpdateStudentFromCustomer($user->contact);
                }
            }

            return redirect()->back()
                             ->with([
                                 'edit_email' => true,
                                 'message' => 'Email successfully added.'
                             ]);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /***************************************************************************************/
    /*                                 Admin Profile                                       */
    /***************************************************************************************/

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function getProfile(Request $request)
    {
        try {
            $this->data['page'] = 'my_profile';
            $this->data['user'] = Auth::user();
            $this->data['userDetails'] = Auth::user()->contact;
            $this->data['states'] = $this->stateRepo->getByList('state', 'state')->toArray();

            return view('admin.profile', $this->data);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepo
     * @param RoleRepository $roleRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAdmin(Request $request,
                              UserRepository $userRepo,
                              RoleRepository $roleRepository)
    {
        try {

            $page = 'edit_user';
            $user = $userRepo->getById($request->route('user_id'));
            $roleRepository->all(true)->each(function($role) use (&$roles) {
                $roles[$role->id] = $role->label;
            });

            if ($user && !$user->isMember()) {
                return view('admin.users.admin.show', compact('page', 'user', 'roles'));
            }

            return redirect()->back();

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditEmailRequest $request
     * @param UserRepository $userRepo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateAdminEmail(EditEmailRequest $request,
                                     UserRepository $userRepo)
    {
        try {

            $user = $userRepo->getById($request->get('user_id'));

            if ($user) {
                $user->email = $request->get('email');
                $user->save();

                // remove existing role
                $user->removeRole($user->roles->first());

                $user->assignRole($request->get('role'));

                return redirect()->back()->with([
                    'edit_email' => true,
                    'message' => 'Email and Role successfully edited.'
                ]);
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditPasswordRequest $request
     * @param UserRepository $userRepo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateAdminPassword(EditPasswordRequest $request,
                                        UserRepository $userRepo)
    {
        try {

            $user = $userRepo->getById($request->get('user_id'));

            if ($user) {
                $user->password = bcrypt($request->get('new_password'));
                $user->save();

                return redirect()->back()->with([
                    'edit_password' => true,
                    'message' => 'Password successfully edited.'
                ]);
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param UserRepository $userRepo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteAdmin(Request $request,
                                UserRepository $userRepo)
    {
        try {

            $user = $userRepo->getById($request->get('user_id'));

            if ($user) {
                $user->delete();

                $request->session()->flash('success_message', 'User successfully deleted.');

                return $user;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function saveNote(Request $request,
                             ContactRepository $contactRepo)
    {
        $contactId = $request->get('customer_id');
        $customer = $contactRepo->getById($contactId);
        $note = CustomerNote::where('customer_id', $customer->id)
                            ->first()
                ?: new CustomerNote();

        $note->fill($request->all());
        $note->save();

        return $note;
    }

    public function remind($order_id) {
        $order = Order::findOrFail($order_id);
        if($order->reminder->first()) {
            ReminderHistoryService::send($order->reminder->first());
            return redirect()->back()
                ->with(['success_message' => 'Reminder has been resent']);
        } else {
            return redirect()->back()
                ->with(['error_message' => 'Something went wrong, please try again later or contact the administrator.']);
        }

    }


    /**
     * @param Request $request
     * @param ContactRepository $contactRepo
     * @return string
     */
    public function deleteCustomer(Request $request,
                                   ContactRepository $contactRepo)
    {
        if ($request->ajax()) {
            $contact = $contactRepo->getById($request->get('id'));

            if ($contact) {
                $user = $contact->user;

                if ($user) {

                    if ($user->corporateSeatCredits->count()) {
                        $user->corporateSeatCredits->each(function($corporateSeatCredit) {
                            $corporateSeatCredit->delete();
                        });
                    }

                    if ($user->clientYearlySubscriptions->count()) {
                        $user->clientYearlySubscriptions->each(function($clientYearlySubscription) {
                            $clientYearlySubscription->delete();
                        });
                    }

                    if ($user->yearlySubscriptions->count()) {
                        $user->yearlySubscriptions->each(function($yearlySubscription) {
                            $yearlySubscription->delete();
                        });
                    }

                    if ($user->registrationOrders->count()) {
                        $user->registrationOrders->each(function($registrationOrder) {
                            $registrationOrder->delete();
                        });
                    }

                    if ($user->orders->count()) {
                        $user->orders->each(function($order) {
                            $order->delete();
                        });
                    }

                    $user->forceDelete();
                }

                $contact->forceDelete();

                request()->session()->flash('success_message', 'Customer deleted.');

                return '';
            }
        }
    }
}
