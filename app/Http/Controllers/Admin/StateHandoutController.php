<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StateHandoutRequest;
use App\Models\State;
use App\Models\StateHandout;
use App\Models\StateHandoutTypes;
use App\Repositories\FileEntryRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StateHandoutController extends Controller
{

    /**
     * @var FileEntryRepository
     */
    private $fileEntryRepository;

    public function __construct(FileEntryRepository $fileEntryRepository)
    {
        $this->data['page'] = 'state_handouts';
        $this->fileEntryRepository = $fileEntryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['handouts'] = StateHandout::has('files')->get();
        return view('admin.state_handouts.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $this->data['states'] = State::orderBy('state')->lists('state', 'abbrev');
        $this->data['types'] = StateHandoutTypes::getTypes()->lists('type', 'id');

        return view('admin.state_handouts.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateHandoutRequest $request)
    {
        $handout = new StateHandout();

        $abbr = $request->input('state');

        $state = State::where('abbrev', $abbr)->firstOrFail();

        $handout->state = $state->state;
        $handout->state_abbr = $state->abbrev;
        $handout->type = $request->input('type', 1);
        $handout->save();
        $file = $request->file('file');
        $fields = [];
        $fields['title'] = $request->get('title', null)
            ? $request->get('title')
            : $file->getClientOriginalName();
        $fileEntryFields = $this->fileEntryRepository->storeFileEntryFile($fields, $handout, $file);
        $handout->files()->create($fileEntryFields);

        return redirect()->route('admin.state_handouts.index')
            ->with(['success_message' => 'State handout has been added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $handout = StateHandout::findOrFail($id);

        $this->data['handout'] = $handout;
        $state = State::where('abbrev', $handout->state_abbr)->firstOrFail();
        $this->data['state'] = $state->state;

        return view('admin.state_handouts.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StateHandoutRequest $request, $id)
    {
        $handout = StateHandout::findOrFail($id);

        $this->fileEntryRepository->deleteFileAndRelatedFiles($handout->files()->first());

        $file = $request->file('file');

        $fields = [];
        $fields['title'] = $request->get('title', null)
            ? $request->get('title')
            : $file->getClientOriginalName();
        $fileEntryFields = $this->fileEntryRepository->storeFileEntryFile($fields, $handout, $file);
        $handout->files()->create($fileEntryFields);

        return redirect()->route('admin.state_handouts.index')
            ->with(['success_message' => 'State handout has been updated.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('state_handout_id');

        $handout = StateHandout::findOrFail($id);

        $this->fileEntryRepository->deleteFileAndRelatedFiles($handout->files()->first());

        $handout->delete();

        return redirect()->route('admin.state_handouts.index')
            ->with(['success_message' => 'State handout has been deleted.']);

    }
}
