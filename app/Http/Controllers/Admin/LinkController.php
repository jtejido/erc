<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\LinkRequest;
use App\Models\Link\Status;
use App\Models\Link\Category;
use App\Models\Link;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;

class LinkController extends BaseController
{

    public function index()
    {

        $this->data['page'] = 'links';
        $this->data['links'] = Link::all();

        return view('admin.links.index', $this->data);
    }

    public function add()
    {
        $this->data['page'] = 'links';
        $this->data['categories'] =  Category::all();
        $this->data['statuses']   =  Status::all();

        return view('admin.links.add', $this->data);
    }


    public function edit(Link $link)
    {
        $this->data['page'] = 'links';
        $this->data['categories'] =  Category::all();
        $this->data['statuses']   =  Category::all();
        $this->data['link'] =  $link;

        return view('admin.links.add', $this->data);
    }


    public function save(LinkRequest $request)
    {
        $data = $request->all();

        $link = null;
        if (array_key_exists('id', $data)) {
            \Log::info(array_except($data, ['id']));
            Link::where('id', $data['id'])
                ->update(array_except($data, ['id']));
        } else {
            $link = Link::create($data);
        }
        
        \Log::info($link);
        
        return $this->json(true, [], 'Successfully saved');
    }
}
