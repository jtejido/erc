<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests\TipRequest;
use App\Models\Category;
use App\Models\Tip;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Image;

class TipController extends BaseController
{

    public function index(Request $request)
    {

        $this->data['page'] = 'tips';
        $this->data['tips'] = Tip::orderBy('created_at', 'DESC')->paginate(20);
        $this->data['title'] = $request->get('title');

        if (isset($this->data['title'])) {
            $this->data['tips'] = Tip::where('title', 'LIKE', '%' . $this->data['title'] . '%')
                ->orderBy('created_at', 'DESC')
                ->paginate(20);
        }
         
        return view('admin.tips.index', $this->data);
    }

    public function add()
    {
        $this->data['page'] = 'tips';
        $this->data['categories'] =  Category::all();

        return view('admin.tips.add', $this->data);
    }

    public function destroy(Request $request)
    {
        Tip::findOrFail($request->input('id'))->delete();
        return response('Successfully deleted.', 200);
    }


    public function edit(Tip $tip)
    {
        $this->data['page'] = 'tips';
        $this->data['categories'] =  Category::all();
        $this->data['tip'] =  $tip;

        return view('admin.tips.add', $this->data);
    }


    /**
     * @param TipRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(TipRequest $request)
    {
        try {
            $data = $request->all();

            \Log::info($data);
            $date =  $data['month']
                .' '.$data['day']
                .' '.$data['year']
            ;

            $published_date = Carbon::parse($date);
            $data['published_date'] = $published_date;

            unset($data['month']);
            unset($data['day']);
            unset($data['year']);
            unset($data['photo']);

            $tip = null;
            if (array_key_exists('id', $data)) {
                \Log::info(array_except($data, ['id']));
                $tip = Tip::find($data['id']);
                $tip->update(array_except($data, ['id']));
            } else {
                $tip = Tip::create($data);
            }

            if ($request->hasFile('photo')) {
                $photo = $request->file('photo');
                Storage::deleteDirectory('tip_photos/' . $tip->id);

                $img_name = pathinfo($photo, PATHINFO_FILENAME);
                $img_ext = 'png';
                $mimeType = 'image/png';
                $width = Image::make($photo)->width();
                $newWidth = ($width > 285) ? 285 : $width;

                $resized_desktop = Image::make($photo)->resize($newWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $resized_desktop->encode('png', 30);

                if (!Storage::disk('local')->exists('tip_photos/' . $tip->id)) {
                    Storage::disk('local')->makeDirectory('tip_photos/' . $tip->id, 0777, true);
                }

                $directory = 'tip_photos/' . $tip->id . '/' . $img_name . '.' . $img_ext;

                Storage::put($directory, $resized_desktop);

                $tip->photo = $img_name . '.' . $img_ext;
                $tip->photo_mime = $mimeType;
                $tip->save();
            }

            \Log::info($tip);
        
            return $this->json(true, [], 'Successfully saved');
        }

        catch (\Exception $e) {
            return $this->json(true, [], $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return $this|bool
     */
    public function tipPhoto(Request $request)
    {
        $tip = Tip::find($request->route('id'));

        if ($tip and $tip->photo and $tip->photo_mime) {
            $file = Storage::disk('local')->get('tip_photos/' . $tip->id . '/' . $tip->photo);;

            return (new Response($file, 200))->header('Content-Type', $tip->photo_mime);
        }

        return false;
    }


}
