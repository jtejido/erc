<?php

namespace App\Http\Controllers\Admin;

use App\Models\GeneralLocation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GeneralLocationController extends BaseController
{

    public function __construct()
    {
        $this->data['page'] = 'general_locations';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['locations'] = GeneralLocation::all();
        return view('admin.general_locations.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.general_locations.add', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = GeneralLocation::create($request->all());
        return redirect()->route('admin.general_locations.index')
            ->with(['success_message' => 'Location has been added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['location'] = $location = GeneralLocation::findOrFail($id);
        return view('admin.general_locations.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = GeneralLocation::findOrFail($id);
        $location->fill($request->all());
        $location->save();
        return back()
            ->with(['success_message' => 'Location successfully saved']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = GeneralLocation::findOrFail($id);
        $location->delete();
        return redirect()->route('admin.general_locations.index')
            ->with(['success_message' => 'Location deleted']);
    }
}
