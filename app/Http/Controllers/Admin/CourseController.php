<?php namespace App\Http\Controllers\Admin;

use App\Events\OnsiteRegistrationConfirmationEvent;
use App\Events\TrainingCancellationEvent;
use App\Http\Requests\FileEntryRequest;
use App\Models\Book;
use App\Models\CourseOrder;
use App\Models\Instructor;
use App\Models\OnsiteTraining;
use App\Models\Topic;
use App\Repositories\FileEntryRepository;
use App\Repositories\BookRepository;
use App\Repositories\InstructorRepository;
use Carbon\Carbon;
use App\Models\Course;
use App\Models\CourseClass;
use App\Repositories\CourseRepository;
use App\Repositories\ClassRepository;
use App\Repositories\CourseTypeRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\StateRepository;
use App\Repositories\UserRepository;
use App\Repositories\ZipCodeRepository;
use App\Services\CourseService;
use App\Http\Requests\CourseRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ClassRequest;

class CourseController extends BaseController
{

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    /**
     * @var CourseTypeRepository
     */
    private $courseTypeRepo;

    /**
     * @var CourseService
     */
    private $courseService;

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @var StateRepository
     */
    private $stateRepo;

    /**
     * @var ZipCodeRepository
     */
    private $zipCodeRepo;
    /**
     * @var FileEntryRepository
     */
    private $fileEntryRepository;

    /**
     * @var BookRepository
     */
    private $bookRepo;

    /**
     * @param CourseRepository $courseRepo
     * @param CategoryRepository $categoryRepo
     * @param CourseTypeRepository $courseTypeRepo
     * @param CourseService $courseService
     * @param ClassRepository $classRepo
     * @param StateRepository $stateRepo
     * @param ZipCodeRepository $zipCodeRepo
     * @param BookRepository $bookRepo
     * @param ZipCodeRepository $zipCodeRepo
     * @param FileEntryRepository $fileEntryRepository
     */
    public function __construct(CourseRepository $courseRepo,
                                CategoryRepository $categoryRepo,
                                CourseTypeRepository $courseTypeRepo,
                                CourseService $courseService,
                                ClassRepository $classRepo,
                                StateRepository $stateRepo,
                                ZipCodeRepository $zipCodeRepo,
                                BookRepository $bookRepo,
                                FileEntryRepository $fileEntryRepository)
    {
        $this->courseRepo = $courseRepo;
        $this->categoryRepo = $categoryRepo;
        $this->courseTypeRepo = $courseTypeRepo;
        $this->courseService = $courseService;
        $this->classRepo = $classRepo;
        $this->stateRepo = $stateRepo;
        $this->zipCodeRepo = $zipCodeRepo;
        $this->bookRepo = $bookRepo;

        $this->data['statusTypes'] = [1 => 'Active', 0 => 'Deactivated'];
        $this->data['categories']  = $this->categoryRepo->getAll();
        $this->data['courseTypes'] = $this->courseTypeRepo->getByList('name', 'id')->toArray();
        $topics = Topic::all();
        $this->data['topics'][''] = 'None';
        foreach($topics as $topic) {
            $this->data['topics'][$topic->id] = $topic->name . ' (reminder sent in ' . $topic->duration . ' months)';
        }
        $this->fileEntryRepository = $fileEntryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param UserRepository $userRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request,
                          UserRepository $userRepo)
    {
        $userId = $request->route('user_id');

        $this->data['courses'] = isset($userId)
                                ? Course::where('is_deactivated', false)->get()
                                : Course::all();

        $this->data['page'] = isset($userId)
                              ? 'select_course'
                              : 'courses';

        $this->data['user'] = isset($userId)
                              ? $userRepo->getById($userId)
                              : null;

        $this->data['now']  = Carbon::now()->format('Y-m-d');

        $view = isset($userId)
                ? view('admin.courses.select', $this->data)
                : view('admin.courses.index', $this->data);

        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->data['page'] = 'add_course';
        return view('admin.courses.add_edit', $this->data);
    }

    public function verifySeo(Request $request)
    {
        if($request->ajax())
        {
            if($request->has('id')){
                // $has = Course::where('id','<>',$request->get('id'))
                //             ->where('slug','=',$request->get('slug'))->count();
                $v = \Validator::make($request->all(),[
                    'slug' => 'unique:courses,slug,'.$request->get('id')
                ]);
            }else{
                // $has = Course::where('slug','=',$request->get('slug'))->count();
                $v = \Validator::make($request->all(),[
                    'slug' => 'unique:courses,slug'
                ]);
            }

            return response()->json(['has_duplicate' =>$v->fails()],200);
        }
    }

    public function associateItemToCourse(Request $request)
    {
        $msg = "%s was linked to this course";
        $courseId = $request->get('course_id');
        $item_id = $request->get('item_id');
        $type = $request->get('item_type');
        $course = Course::findOrFail($courseId);
        switch ($type) {
            case 'BOOK':
                $course->books()->save(Book::findOrFail($item_id));
                $msg = sprintf($msg, 'Book');
                break;
            case 'COURSE':
                $linkCourse = Course::findOrFail($item_id);
                $course->courses()->save($linkCourse);
                $linkCourse->courses()->save($course);
                $msg = sprintf($msg, 'Course');
                break;
        }

        return back()->with(['success_message' => $msg]);
    }

    public function unLinkItemFromCourse(Request $request)
    {
        $msg = "%s was unlinked to this course";
        $courseId = $request->get('course_id');
        $course = Course::findOrFail($courseId);
        $type = $request->get('item_type');
        $item_id = $request->get('item_id');
        switch ($type) {
            case 'BOOK':
                $course->books()->detach($item_id);
                $msg = sprintf($msg, 'Book');
                break;
            case 'COURSE':
                $linkCourse = Course::findOrFail($item_id);
                $course->courses()->detach($item_id);
                $linkCourse->courses()->detach($courseId);
                $msg = sprintf($msg, 'Course');
                break;
        }
        return back()->with(['success_message' => $msg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $books = $this->bookRepo->all()->keyBy('id');
        $courses = Course::all()->keyBy('id');
        foreach ($course->books as $book) {
            $books->forget($book->id);
        }
        $courses->forget($course->id);
        foreach ($course->courses as $c) {
            $courses->forget($c->id);
        }
        $this->data['course'] = $course;
        $this->data['page']   = 'edit_course';
        $this->data['books'] = $books;
        $this->data['course'] = $course;
        $this->data['courses'] = $courses;

        return view('admin.courses.add_edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CourseRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function save(CourseRequest $request)
    {
        try {

            $successMessage = $request->get('id' )
                              ? 'Course has been edited.'
                              : 'Course has been added.';

            $course = $this->courseService->save($request->all());

            if ($course) {

                return redirect()->route('admin.course.edit', ['id' => $course->id])
                    ->with([
                        'success_message' => $successMessage
                    ]);
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Activate or Deactivate course
     *
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function activateDeactivateCourse(Request $request)
    {
        try {

            if ($request->isMethod('POST')) {
                $course = $this->courseRepo->getById($request->get('id'));

                if ($course->is_deactivated) {
                    $course->is_deactivated = false;
                } else {
                    $course->is_deactivated = true;
                }

                $course->save();

                return $course;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove a course
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function deleteCourse(Request $request)
    {
        try {

            if ($request->isMethod('POST')) {
                $course = $this->courseRepo->getById($request->get('id'));

                $courseOrders = CourseOrder::where('course_id', $course->id)->get();

                if ($courseOrders->count()) {
                    $courseOrders->each(function($courseOrder) {
                        $courseOrder->delete();
                    });
                }

                $course->delete();

                return $course;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * List of classes
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function classes(Request $request)
    {
        $this->data['page']    = 'classes';
        $this->data['course']  = $course = $this->courseRepo->getById($request->route('id'));
        $this->data['status']  = $request->get('status');
        $this->data['classes'] = CourseClass::where('course_id', $course->id)->get();

        $this->data['now']     = date('Y-m-d');

        if (!is_null($this->data['status']) && strlen($this->data['status'])) {
            $params = ['status' => $this->data['status'], 'course' => $request->route('id')];

            $this->data['classes'] = $this->classRepo->searchFilter($params);
        }

        return view('admin.courses.classes', $this->data);
    }

    /**
     * Get class form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClassForm(Request $request,
                                 InstructorRepository $instructorRepo)
    {
        if ($request->get('id')) {
            $this->data['class'] = $this->classRepo->getById($request->get('id'));
        }

        $this->data['states'] = $this->stateRepo->getByList('state', 'state')->toArray();
        $this->data['course'] = $this->courseRepo->getById($request->get('course_id'));
        $this->data['isSeminar'] = $this->data['course']->isSeminar();
        $this->data['instructors'] = Instructor::orderBy('first_name')->get();

        return view('admin.includes.class_modal', $this->data);
    }

    /**
     * Save class
     *
     * @param ClassRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function saveClass(ClassRequest $request)
    {
        try {
            if ($request->isMethod('POST')) {
                $this->courseService->saveClass($request->all());

                return redirect()->back()
                                 ->with('success_message', 'Class has been saved.');
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Activate or Deactivate class
     *
     * @param Request $request
     * @return bool
     * @throws \Exception
     */
    public function activateDeactivateClass(Request $request)
    {
        try {

            if ($request->isMethod('POST')) {
                $class = $this->classRepo->getById($request->get('id'));

                if ($class->is_deactivated) {
                    $class->is_deactivated = false;
                } else {
                    $class->is_deactivated = true;
                }

                $class->save();

                return $class;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Delete a class
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function deleteClass(Request $request)
    {
        try {

            if ($request->isMethod('POST')) {
                $class = $this->classRepo->getById($request->get('id'));
                $class->delete();

                return $class;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Cancel a class and send notification
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function cancelClass(Request $request)
    {
        try {
            $class = $this->classRepo->getById($request->get('id'));
            $class->is_cancelled = true;
            $class->save();
            event(new TrainingCancellationEvent(get_class($class), $class->id));

            return $class;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function addMaterial(FileEntryRequest $request)
    {
        $path = $request->get('path');
        $course = $this->courseRepo->getById($request->get('item_id'));

        if ((!$course || !$request->hasFile('file')) and !isset($path)) {
            abort(404);
        }
        $this->fileEntryRepository->addFile($request, $course);

        $this->data['page']   = 'edit_course';
        $this->data['course'] = $course;

        return redirect()->route('admin.course.edit', [$course->id])
            ->with(['success_message' => 'File has been added.']);
    }

    public function addOnsiteMaterial(FileEntryRequest $request)
    {
        $course = OnsiteTraining::find($request->input('item_id'));
        if (!$course || !$request->hasFile('file')) {
            abort(404);
        }
        $files = $this->fileEntryRepository->addFile($request, $course);

        $this->data['page']   = 'edit_course';
        $this->data['course'] = $course;

        event(new OnsiteRegistrationConfirmationEvent(
            $course->originating_agent_user_id,
            $course->training_date->format('F j, Y'),
            $course->address,
            $files, false));

        return redirect()->to($request->input('route_back'))
            ->with(['success_message' => 'File has been added.']);
    }

}
