<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $data;

    /**
     * BaseController constructor
     */
    public function __construct() {}
}
