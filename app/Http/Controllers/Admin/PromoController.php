<?php

namespace App\Http\Controllers\Admin;

use App\Models\Promo;
use App\Utilities\Constant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Image;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page'] = 'promos';
        $data['promos'] = Promo::paginate(10);

        return view('admin.promos.index', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        $id = $request->get('id');

        if (isset($id)) {
            $data['promo'] = Promo::find($id);
        }

        return view('admin.promos.form', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $id = $request->get('id');
        $photo = $request->file('photo');
        $activate = $request->get('active');

        $promo = isset($id)
            ? Promo::find($id)
            : new Promo();

        $promo->fill($request->all());
        $promo->save();

        if (isset($photo)) {
            $this->removePhoto($promo);
            $this->savePhoto($promo, $photo);
        }

        if (!isset($activate)) {
            $promo->active = false;
            $promo->order = null;
            $promo->save();
        }

        return redirect()->back()->with('success_message', 'Promo saved.');
    }

    /**
     * @param Promo $promo
     * @param UploadedFile $file
     */
    private function savePhoto(Promo $promo, UploadedFile $file)
    {
        
        $img_name = pathinfo($file, PATHINFO_FILENAME);
        $img_ext = 'png';
        $mimeType = 'image/png';
        $image = Image::make($file);
        $width = Image::make($file)->width();
        $newWidth = ($width > 285) ? 285 : $width;

        $resized_desktop = Image::make($image)->resize($newWidth, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

        $resized_desktop->encode('png', 30);


        $promo->photo = $img_name . '.' . $img_ext;
        $promo->photo_mime = $mimeType;
        $promo->save();

        $directory = 'promos/' . $promo->id . '/' . $img_name . '.' . $img_ext;

        Storage::put($directory, $resized_desktop);

    }

    /**
     * @param Promo $promo
     */
    private function removePhoto(Promo $promo)
    {
        Storage::deleteDirectory('promos/' . $promo->id);
    }

    /**
     * @param Request $request
     * @return $this|bool
     */
    public function previewPhoto(Request $request)
    {
        $file = false;
        $promo = Promo::find($request->route('id'));

        $path = 'promos/' . $promo->id . '/' . $promo->photo;

        if (Storage::disk('local')->exists($path)) {
            $file = Storage::disk('local')->get($path);
        }

        if ($file) {
            return (new Response($file, 200))->header('Content-Type', $promo->photo_mime);
        }

        return $file;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate(Request $request)
    {
        $ids = $request->get('ids');
        $promos = Promo::whereIn('id', $ids)->get();

        if ($promos->count()) {
            $promos->each(function ($p) {
                $p->active = true;
                $p->save();
            });
        }

        request()->session()->flash('success_message', 'Activate success.');

        return $promos;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deactivate(Request $request)
    {
        $ids = $request->get('ids');
        $promos = Promo::whereIn('id', $ids)->get();

        if ($promos->count()) {
            $promos->each(function($p) {
                $p->active = false;
                $p->save();
            });
        }

        request()->session()->flash('success_message', 'Unpublish success.');

        return $promos;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = $request->get('ids');
        $promos = Promo::whereIn('id', $ids)->get();

        if ($promos->count()) {
            $promos->each(function($p) {
                $p->delete();
            });
        }

        request()->session()->flash('success_message', 'Delete success.');

        return $promos;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function order(Request $request)
    {

        $data['promos'] = Promo::where('active', true)->orderBy('order')->get();

        return view('admin.promos.order', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ordersave(Request $request)
    {

        foreach ($request->ids AS $key => $value) {

                $promo = Promo::find($key);

                $promo->order = $value;

                $promo->save();

            }

        return redirect()->back()->with('success_message', 'Promo Order saved.');

    }
}
