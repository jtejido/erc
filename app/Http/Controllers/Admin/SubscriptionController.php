<?php namespace App\Http\Controllers\Admin;

use App\Models\CorporateSeat;
use App\Models\OnsiteTraining;
use App\Repositories\UserYearlySubscriptionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Http\Requests;
use App\Repositories\ContactRepository;
use App\Repositories\CorporateSeatRepository;
use App\Repositories\UserRepository;
use App\Repositories\YearlySubscriptionRepository;
use App\Repositories\YearlySubscriptionTypesRepository;
use App\Services\RegistrationService;
use App\Facades\DiscountService;
use App\Facades\UserService;
use App\Http\Requests\FilesEntryRequest;
use App\Repositories\FileEntryRepository;
use App\Repositories\OrderRepository;
use App\Events\OnsiteRegistrationConfirmationEvent;
use Illuminate\Http\Request;

class SubscriptionController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var YearlySubscriptionTypesRepository
     */
    private $subscriptionTypesRepo;

    /**
     * @var YearlySubscriptionRepository
     */
    private $subscriptionRepo;

    /**
     * @var RegistrationService
     */
    private $registrationService;

    /**
     * @var CorporateSeatRepository
     */
    private $corporateSeatRepo;
    /**
     * @var FileEntryRepository
     */
    private $fileEntryRepository;

    /**
     * @param UserRepository $userRepo
     * @param ContactRepository $contactRepo
     * @param YearlySubscriptionTypesRepository $subscriptionTypesRepo
     * @param YearlySubscriptionRepository $subscriptionRepo
     * @param RegistrationService $registrationService
     * @param CorporateSeatRepository $corporateSeatRepo
     * @param FileEntryRepository $fileEntryRepository
     */
    public function __construct(UserRepository $userRepo,
                                ContactRepository $contactRepo,
                                YearlySubscriptionTypesRepository $subscriptionTypesRepo,
                                YearlySubscriptionRepository $subscriptionRepo,
                                RegistrationService $registrationService,
                                CorporateSeatRepository $corporateSeatRepo,
                                FileEntryRepository $fileEntryRepository)
    {
        $this->userRepo = $userRepo;
        $this->contactRepo = $contactRepo;
        $this->subscriptionTypesRepo = $subscriptionTypesRepo;
        $this->subscriptionRepo = $subscriptionRepo;
        $this->registrationService = $registrationService;
        $this->corporateSeatRepo = $corporateSeatRepo;
        $this->fileEntryRepository = $fileEntryRepository;
    }

    /**
     * List of address book
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function contacts(Request $request)
    {
        try {
            $userId = $request->route('user_id');
            $user = isset($userId)
                     ? $this->userRepo->getById($userId)
                     : Auth::user();

            $this->data['page']        = 'subscription';
            $this->data['user']        = $user;
            $this->data['attendees'][] = '';
            $this->data['types']       = $this->subscriptionTypesRepo->getAll();

            $this->data['contacts'] = $this->contactRepo
                                           ->getByUserWithContact($user->id);

            $this->data['stype'] = 1; // set default stype

            $subscriptions = $this->subscriptionRepo->getByAgent($user->id);

            if ($subscriptions) {
                foreach ($subscriptions->clientSubscriptions as $key => $subscription) {
                    $this->data['attendees'][$key] = $subscription->user_id;
                }

                $this->data['stype'] = $subscriptions->type_id;
            }

            return isset($userId)
                    ? view('admin.users.contacts_yearly_subscription', $this->data)
                    : view('main.users.contacts_yearly_subscription', $this->data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Save subscription price
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function save(Request $request)
    {
        try {
            $id      = $request->get('type_id');
            $price   = $request->get('value');
            $subType = $this->subscriptionTypesRepo->getById($id);

            if ($subType) {
                $subType->price = $price;
                $subType->save();
            }

            return $subType;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Add a contact into cart after a valid request
     *
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function addContactsToSubscription(Request $request)
    {
        try {

            $userId         = $request->get('user_id');
            $subscriptionId = $request->get('subscription_id');
            $contacts       = $request->get('contacts');
            $isAdmin        = $request->get('is_admin');
            $user           = $this->userRepo->getById($userId);

            $this->registrationService->createSubscription([
                'originating_agent_user_id'     => $user->id,
                'subscription_id'               => $subscriptionId,
                'contacts'                      => $contacts,
            ]);

            return isset($isAdmin)
                    ? route('admin.user.shopping_cart', ['user_id' => $userId])
                    : route('user.shopping_cart');

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /***************************************************************************************/
    /*                                  Corporate Seat                                     */
    /***************************************************************************************/

    /**
     * List of corp seats
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function corporateSeats(Request $request)
    {
        try {
            $userId = $request->route('user_id');
            $user = isset($userId)
                    ? $this->userRepo->getById($userId)
                    : Auth::user();

            $this->data['page']      = 'corporate_seats';
            $this->data['user']      = $user;
            $this->data['corpSeats'] = $this->corporateSeatRepo->getAll();

            return isset($userId)
                    ? view('admin.users.corporate_seat', $this->data)
                    : view('main.users.corporate_seat', $this->data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Save corporate seat permanently
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function saveCorporateSeat(Request $request)
    {
        try {

            $id = $request->get('corp_seat_id');
            $price = $request->get('value');
            $corpSeat = $this->corporateSeatRepo->getById($id);

            if ($corpSeat) {
                $corpSeat->price = $price;
                $corpSeat->save();

                $request->session()->flash('success_message', 'Corporate seat saved.');
            }

            return $corpSeat;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return CorporateSeat
     */
    public function saveNewCorporateSeat(Request $request)
    {
        $corporatSeat = new CorporateSeat();
        $corporatSeat->fill($request->all());
        $corporatSeat->save();

        $request->session()->flash('success_message', 'Corporate seat saved.');

        return $corporatSeat;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function deleteCorporateSeat(Request $request)
    {
        $corporateSeat = $this->corporateSeatRepo->getById($request->get('id'));

        if ($corporateSeat) {
            $corporateSeat->delete();

            $request->session()->flash('success_message', 'Corporate seat deleted.');
        }

        return $corporateSeat;
    }

    /**
     * Add client to Corporate Seat
     *
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function addClientToCredit(Request $request)
    {
        try {
            $params     = $request->all();
            $isAdmin    = $request->get('is_admin');
            $userId     = $request->get('user_id');

            $this->registrationService->createCredit($params);

            return isset($isAdmin)
                    ? route('admin.user.shopping_cart', ['user_id' => $userId])
                    : route('user.shopping_cart');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Give credit to each class registration
     *
     * @param Request $request
     * @param OrderRepository $orderRepo
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function giveCredit(Request $request,
                               OrderRepository $orderRepo)
    {
        try {
            $user = $this->userRepo->getById($request->get('user_id'));
            $order = $orderRepo->getById($request->get('order_id'));
            $classIds = $request->get('class_ids');

            if($user->getCorporateSeatsCredit() < count($classIds)) {
                return redirect()->back()
                    ->with('error_message', "You don't have enough credits for the number of users chosen.");
            }

            DiscountService::giveCredit($user, $order, $classIds);
            UserService::updateCredits($user, count($classIds));

            return redirect()->back()
                             ->with('success_message', 'Corporate Seat Credits Applied.');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the credit that has been applied
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function removeCredit(Request $request)
    {
        try {

            $classCreditDiscount = DiscountService::removeCredit($request->get('class_reg_id'));

            Session::flash('success_message', 'Corporate Seat Credit Removed.');

            return $this->json($classCreditDiscount);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /***************************************************************************************/
    /*                                  OnSite Training                                    */
    /***************************************************************************************/

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function onSiteTraining(Request $request)
    {
        try {

            $this->data['page'] = 'onsite_training';
            $this->data['user'] = $this->userRepo->getById($request->route('user_id'));

            return view('admin.users.onsite_training', $this->data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function addClientToOnsiteTraining(FilesEntryRequest $request)
    {
        try {
            $data = $request->all();

            $notify = $request->has('doNotEmail') ? false: true;

            $user_id = $data['originating_agent_user_id'];
            $address = $data['address'];
            $date    = $data['training_date'];

            $training = $this->registrationService->createTraining($data);

            $files = $this->fileEntryRepository->addFiles($request, $training);

            if ($training) {
                event(new OnsiteRegistrationConfirmationEvent($user_id, $date, $address, $files, true, $notify));
                return redirect()->back()
                    ->with('message', 'Onsite Training created.');
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateOnsiteTraining(Request $request)
    {
        try {

            $training = OnsiteTraining::find($request->input('id'));
            $data = $request->only('originating_agent_user_id', 'address','training_date');
            $user_id = $data['originating_agent_user_id'];
            $address = $data['address'];
            $date    = $data['training_date'];

            $data['training_date'] = Carbon::parse($data['training_date'])->format('Y-m-d');

            $training->fill($data);
            $training->save();

            event(new OnsiteRegistrationConfirmationEvent($user_id, $date, $address, [], false));

            return redirect()->back()
                ->with('message', 'Onsite Training updated.');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param UserYearlySubscriptionRepository $userYearlySubscriptionRepo
     * @return string
     * @throws \Exception
     * @throws \Throwable
     */
    public function getActivation(Request $request,
                                  UserYearlySubscriptionRepository $userYearlySubscriptionRepo)
    {
        try {
            $id = $request->get('id');
            $yearlySub = $userYearlySubscriptionRepo->getById($id);

            if ($yearlySub) {

                $view = (auth()->user()->id == $yearlySub->user_id)
                    ? 'includes.activation_modal'
                    : 'includes.activation_modal_admin';

                return view($view, compact('yearlySub'))->render();
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param UserYearlySubscriptionRepository $userYearlySubscriptionRepo
     * @return $this
     */
    public function doActivation(Request $request,
                                 UserYearlySubscriptionRepository $userYearlySubscriptionRepo)
    {
        $id = $request->get('subscription_id');
        $code = $request->get('activation_code');

        $userSubscription = $userYearlySubscriptionRepo->getByIdAndActivationCode($id, $code);

        if ($userSubscription) {
            $userSubscription->activated = true;
            $userSubscription->save();

            return redirect()->back()->with('success_message', 'Yearly Subscription activated.');
        }

        return redirect()->back()->with('error_message', 'Activation code has been used or does not exist.');
    }
}
