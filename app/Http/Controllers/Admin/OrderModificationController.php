<?php namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\OrderAdjustment;
use App\Models\ProductOrders;
use App\Models\Book;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Facades\OrderModificationUtility;
use App\Facades\OrderModificationService;
use App\Facades\OrderModificationDiscountService;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utilities\Constant;
use App\Repositories\OrderAdjustmentRepository;
use App\Traits\UpsShippingTrait;

class OrderModificationController extends BaseController
{
    use UpsShippingTrait;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo)
    {
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepo;
    }

    /**
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function queryIfCancellable(Request $request,
                                       ClassRegistrationRepository $classRegistrationRepo,
                                       RegistrationOrderRepository $registrationOrderRepo,
                                       OrderAdjustmentRepository $orderAdjustmentRepo)
    {
        try {

            $pairedCtr = 0;
            $data['paid'] = 0;
            $data['totalFee'] = 0;
            $data['customers'] = [];
            $data['withinRestriction'] = false;

            $pairClassRegistration = null;
            $pairedRegistrationOrder = null;
            $cancelledClassRegistrationIds = [];

            $data['agentId'] = $request->get('agent_id');
            $newRegistration = $request->get('new_registration');
            $classRegistrationIds = $request->get('class_registration_ids');
            $registrationOrderId = $request->get('registration_order_id');
            $order = $this->orderRepo->getById($request->get('order_id'));

            $registrationOrder = $registrationOrderRepo->getById($registrationOrderId);
            $restriction = OrderModificationUtility::cancellationWithinRestriction($registrationOrder);

            $data['registrationOrderId'] = $registrationOrder->id;

            foreach ($classRegistrationIds as $classRegistrationId) {
                $classRegistration = $classRegistrationRepo->getById($classRegistrationId);

                $data['paid'] += $classRegistration->paid_charge;

                array_push($cancelledClassRegistrationIds, $classRegistrationId);

                $params = [
                    'id' => $classRegistration->id,
                    'name' => $classRegistration->contact->name,
                    'is_new' => $classRegistration->is_new
                ];

                array_push($data['customers'], $params);
            }

            if ($restriction and $registrationOrder->courseTypeId() != Constant::COMPUTER_BASED) {
                $classRegistrations = $classRegistrationRepo->getByIds($classRegistrationIds);

                $data['withinRestriction'] = true;
                $data['agentContactId'] = $registrationOrder->orderItem->order->user->contact->id;
                $data['numOfClassRegistrations'] = ($classRegistrations->count() + $pairedCtr);
                $data['totalFee'] = 50 * (int) $data['numOfClassRegistrations'];
            }

            $data['deferredHalfDiscount'] = OrderModificationUtility::extractDeferredHalfPriceDiscounts($registrationOrder, $cancelledClassRegistrationIds);
            $data['deferredGroupDiscount'] = OrderModificationUtility::extractDeferredGroupDiscounts($registrationOrder, $cancelledClassRegistrationIds);

            $addtionalFeesFromAdjustments = $orderAdjustmentRepo->getByOrderIdReturnTotal($order->id, Constant::ADDED);

            $data['additionalFees'] = $addtionalFeesFromAdjustments;

            $data['deductions'] = $orderAdjustmentRepo->getByOrderIdReturnTotal($order->id, Constant::DEDUCTED);

            $data['subTotalRefund'] = OrderModificationUtility::extractCancellationSubTotal(
                $data['paid'],
                $data['deferredHalfDiscount']['amount'],
                $data['deferredGroupDiscount']['amount'],
                $data['additionalFees'],
                $data['deductions'],
                $data['totalFee']
            );

            $data['originalTotal'] = $order->total;

            $data['newOrderTotal'] = OrderModificationUtility::extractNewOrderTotal(
                $order->total,
                $data['paid'],
                $data['deferredHalfDiscount']['amount'],
                $data['deferredGroupDiscount']['amount'],
                $data['additionalFees'],
                $data['deductions']
            );

            $view = $newRegistration
                     ? view('admin.completed-registrations.cancel_new_registration_confirmation', $data)
                     : view('admin.completed-registrations.registration_cancellation_modal', $data);

            return $view;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function cancelClassRegistrations(Request $request)
    {
        try {
            OrderModificationService::cancelClassRegistration($request->all());

            $s = count($request->get('class_registration_ids')) > 1 ? 's' : '';

            return redirect()->back()
                ->with('success_message', 'Registration' . $s .' cancelled.');


        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param  Request $request
     * @return mixed
     */
    public function changePrice(Request $request)
    {
        $response = OrderModificationService::changePrice($request->all());
        $request->session()->flash('success_message', 'Price Changed.');
        return $response;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changePriceAdjustment(Request $request)
    {
        $response = OrderModificationService::changeAdjustmentPrice($request->all());

        $request->session()->flash('success_message', 'Price Changed.');

        return $response;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function removeAdjustment(Request $request)
    {
        $response = OrderModificationService::removeAdjustment($request->all());
        $request->session()->flash('success_message', 'Order Modification Removed.');

        return $response;
    }

    /**
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function restoreCancelled(Request $request,
                                     ClassRegistrationRepository $classRegistrationRepo)
    {
        $classRegistration = $classRegistrationRepo->getById($request->get('class_registration_id'));
        $user = $this->userRepo->getById($request->get('user_id'));

        if ($classRegistration) {
            $classRegistration->is_cancelled = false;
            $classRegistration->save();

            $registrationOrder = $classRegistration->registrationOrder;
            $classRegistrations = $classRegistrationRepo->getByRegistrationIdNoSwapNoCancel($registrationOrder->id);

            OrderModificationDiscountService::restoreDiscounts($classRegistration);
            OrderModificationDiscountService::reComputeOrderWhenCancelled($classRegistrations, $user);
            OrderModificationService::createAdjustmentFromRestoration($user, $classRegistration, $registrationOrder);
        }

        $request->session()->flash('success_message', 'Class Registration Restored.');

        return $classRegistration;
    }
}
