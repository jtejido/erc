<?php

namespace App\Http\Controllers\Admin;

use App\Facades\ReminderHistoryService;
use App\Models\ReminderHistory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ReminderHistoryController extends Controller
{

    public function __construct()
    {
        $this->data['page'] = 'history';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['reminders'] = ReminderHistory::reminders()->get();
        return view('admin.reminder-history.index', $this->data);
    }

    public function resend($id)
    {
        $reminder = ReminderHistory::findOrFail($id);
        ReminderHistoryService::send($reminder);
        return redirect()->route('admin.reminder-history.index')
            ->with(['success_message' => 'Reminder has been resent']);
    }

    public function trigger()
    {
        return Artisan::call('reminder:send');
    }


}
