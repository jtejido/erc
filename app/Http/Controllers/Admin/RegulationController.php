<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\RegulationRequest;
use App\Models\Category;
use App\Models\Regulation;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Image;

class RegulationController extends BaseController
{

    public function index(Request $request)
    {

        $this->data['page'] = 'regulations';
        $this->data['regulations'] = Regulation::orderBy('created_at', 'DESC')->get();
        $this->data['title'] = $request->get('title');

        if (isset($this->data['title'])) {
            $this->data['regulations'] = Regulation::where('title', 'LIKE', '%' . $this->data['title'] .'%')
                ->orderBy('created_at', 'DESC')
                ->get();
        }
         
        return view('admin.regulations.index', $this->data);
    }

    public function add()
    {
        $this->data['page'] = 'regulations';
        $this->data['categories'] =  Category::all();

        return view('admin.regulations.add', $this->data);
    }


    public function edit(Regulation $regulation)
    {
        $this->data['page'] = 'regulations';
        $this->data['categories'] =  Category::all();
        $this->data['regulation'] =  $regulation;

        return view('admin.regulations.add', $this->data);
    }

    public function destroy(Request $request)
    {
        Regulation::findOrFail($request->input('id'))->delete();
        return response('Successfully deleted.', 200);
    }


    public function save(RegulationRequest $request)
    {
        $data = $request->all();

        \Log::info($data);
        $date =  $data['month']
            .' '.$data['day']
            .' '.$data['year']
        ;

        $published_date = Carbon::parse($date);
        $data['published_date'] = $published_date;

        unset($data['month']);
        unset($data['day']);
        unset($data['year']);
        unset($data['photo']);

        $reg = null;
        if (array_key_exists('id', $data)) {
            \Log::info(array_except($data, ['id']));
            $reg = Regulation::find($data['id']);
            $reg->update(array_except($data, ['id']));

        } else {
            $reg = Regulation::create($data);
        }

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            Storage::deleteDirectory('reg_photos/' . $reg->id);

            $img_name = pathinfo($photo, PATHINFO_FILENAME);
            $img_ext = 'png';
            $mimeType = 'image/png';
            $width = Image::make($photo)->width();
            $newWidth = ($width > 285) ? 285 : $width;
            $resized_desktop = Image::make($photo)->resize($newWidth, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $resized_desktop->encode('png', 30);

            if (!Storage::disk('local')->exists('reg_photos/' . $reg->id)) {
                Storage::disk('local')->makeDirectory('reg_photos/' . $reg->id, 0777, true);
            }

            $directory = 'reg_photos/' . $reg->id . '/' . $img_name . '.' . $img_ext;

            Storage::put($directory, $resized_desktop);

            $reg->photo = $img_name . '.' . $img_ext;
            $reg->photo_mime = $mimeType;
            $reg->save();
        }

        \Log::info($reg);

        return $this->json(true, [], 'Successfully saved');
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function regPhoto(Request $request)
    {
        $reg = Regulation::find($request->route('id'));

        if ($reg and $reg->photo and $reg->photo_mime) {
            $file = Storage::disk('local')->get('reg_photos/' . $reg->id . '/' . $reg->photo);;

            return (new Response($file, 200))->header('Content-Type', $reg->photo_mime);
        }

        return false;
    }


}
