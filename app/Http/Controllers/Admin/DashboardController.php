<?php namespace App\Http\Controllers\Admin;

use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ContactRepository;
use App\Repositories\OrderItemRepository;
use App\Utilities\Constant;
use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends BaseController
{

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $registrationRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @param ContactRepository $contactRepo
     * @param ClassRegistrationRepository $registrationRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(ContactRepository $contactRepo,
                                ClassRegistrationRepository $registrationRepo,
                                OrderItemRepository $orderItemRepo)
    {
        $this->contactRepo = $contactRepo;
        $this->registrationRepo = $registrationRepo;
        $this->orderItemRepo = $orderItemRepo;

        $this->data['page'] = 'dashboard';
    }

    /**
     * Display list of users
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        try {

            $this->data['searchq']     = $request->get('searchq');
            $this->data['status']      = $request->get('status');

            $this->data['customers'] = $this->contactRepo->getAllCustomers();

            return view('admin.index', $this->data);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function index_data(Request $request)
    {
        $customers = $this->contactRepo->getAllCustomers();

        $customers->load('user');

        $data = [];

        foreach ($customers as $customer) {
            $d = [];

            $d['customer_cbox'] = "<input type=\"checkbox\" name=\"customer[]\" class=\"customer-checkbox\" value=\"$customer->id\">";
            $d['first_name'] = $customer->first_name;
            $d['last_name'] = $customer->last_name;
            $d['company'] = $customer->company;
            $d['phone'] = $customer->phone;
            $d['course_mill_user_id'] = ($customer->course_mill_user_id)
                ? $customer->course_mill_user_id
                : '';
            $d['email'] = ($customer->user)
                ? $customer->user->email
                : '';


            $action = "<a class='btn btn-info btn-flat btn-block' ";
            $action .= "href='" . route('admin.user_details', [
                                'contact_id'    => $customer->id,
                                'user_id'       => $customer->user ?: null]) . "'>";
            $action .= "View Transactions";
            $action .= "</a>";

            $d['action'] = $action;

            $data[] = $d;
        }


        $r_data['draw'] = intval($request->input('draw'));
        $r_data['recordsTotal'] = $customers->count();
        $r_data['recordsFiltered'] = $customers->count();
        $r_data['data'] = $data;

        return response()->json($r_data);
    }
}
