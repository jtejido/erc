<?php
/**
 * Created by PhpStorm.
 * User: greghermo
 * Date: 01/08/2017
 * Time: 4:29 PM
 */

namespace App\Http\Controllers\Admin;


use App\Events\OnsiteRegistrationConfirmationEvent;
use App\Events\RegistrationConfirmationEvent;
use App\Models\Order;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

class OrdersController extends BaseController
{

    public function __construct()
    {
        $this->data['page'] = 'orders';
    }

    public function index()
    {
        return view('admin.orders.index', $this->data);
    }

    public function index_data(Request $request)
    {

        return Datatables::of(Order::completed()
            ->with(['user.contact', 'paymentTransactions']))
            ->editColumn('is_archived', function($order) {
                if($order->is_archived) {
                    $archived_cbox = sprintf("<input type='checkbox' 
                    name='archived_cbox' 
                    checked='%s' 
                    class='is_archived_box' 
                    data-order_id='%s'/>", $order->is_archived, $order->id);
                } else {
                    $archived_cbox = sprintf("<input type='checkbox' 
                    name='archived_cbox' 
                    class='is_archived_box'
                    data-order_id='%s'/>", $order->id);
                }
                return $archived_cbox;
            })
            ->editColumn('is_archived_value', function($order) {
                return $order->is_archived;
            })
            ->addColumn('name', function($order) {

                $name = $order->user->contact->first_name . ' ' . $order->user->contact->last_name;

                $link_to_customer = sprintf("<a href='%s'>$name</a>",
                    route('admin.user_details', [
                        'contact_id'    => $order->user->contact->id,
                        'user_id'       => $order->user->id
                    ]));

                return $link_to_customer;
            })
            ->editColumn('completed_formatted', function($order) {
                return $order->completed_at->format('M j, Y g:i a');
            })
            ->addColumn('trans_id', function ($order) {
                return $order->paymentTransactions->map(function($transaction) {
                    return $transaction->transaction_id;
                })->implode('<br>');
            })
            ->editColumn('total', function($order) {
                return $order->total_in_dollars;
            })
            ->editColumn('status', function($order) {
                return $order->status_formatted;
            })
            ->addColumn('action', function($order) {
                $action = '';
                if ($order->paymentTransactions->count()) {
                    $action .= '<a class="btn btn-primary btn-flat btn-block"';
                    $action .= 'href="' . route("admin.payment.summary", [
                            "user_id" => $order->user_id,
                            "order_id" => $order->id
                        ]) . '""';
                    $action .= ' target="_blank">View Payment</a>';
                }

                $action .= '<a class="btn btn-default btn-flat btn-block"';
                $action .= 'href="' . route("admin.order.info", [
                        "user_id" => $order->user_id,
                        "order_id" => $order->id
                    ]) . '""';
                $action .= '>View Order</a>';

                if($order->invoicePayments->count()) {
                    $action .= '<a class="btn btn-default btn-flat btn-block"';
                    $action .= 'href="' . route("admin.order.invoice_info", [
                            "user_id" => $order->user_id,
                            "order_id" => $order->id
                        ]) . '""';
                    $action .= ' target="_blank">View Invoice</a>';
                }

                $action .= sprintf("<a href='#' 
                    class='btn btn-default btn-flat btn-block resend_confirmation'
                    data-url='%s'>
                    Resend Confirmation</a>",
                    route('admin.order.resend_confirmation', ['order_id' => $order->id]));

                if($order->isOnsite()) {
                    $action = '';
                }
                return $action;
            })
            ->make(true);

    }

    public function update(Request $request, $id) {

        try {
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        if($request->input('archive', false) == 'true') {
            $order->archiveNow();
            return response()->json(['code' => 'success', 'result' => 'archived'], 200);
        } else {
            $order->UnArchiveNow();
            return response()->json(['code' => 'success', 'result' => 'unarchived'], 200);
        }

    }

    public function resendConfirmation(Request $request, $id)
    {

        try {
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Order ID Not Found'], 404);
        }

        if($order->isOnsite()) {
//            event(new OnsiteRegistrationConfirmationEvent($user_id, $date, $address, $files));
        } else {
            event(new RegistrationConfirmationEvent($order->id, null, null, [], true, false, false));
        }


        return redirect()->route('admin.orders')->with(['success_message' => 'Registration confirmation sent!']);

    }

}