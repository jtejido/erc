<?php namespace App\Http\Controllers\Admin;

use App\Facades\ClassService;
use App\Http\Requests\ClassRequest;
use App\Models\Course;
use App\Models\CourseClass;
use App\Models\GeneralLocation;
use App\Models\Instructor;
use App\Models\Location;
use App\Repositories\StateRepository;
use App\Services\CourseService;
use Illuminate\Http\Request;

class ClassController extends BaseController
{
    /**
     * @var StateRepository
     */
    private $stateRepo;
    /**
     * @var CourseService
     */
    private $courseService;


    /**
     * ClassController constructor.
     * @param StateRepository $stateRepo
     * @param CourseService $courseService
     */
    public function __construct(StateRepository $stateRepo,
                                CourseService $courseService)
    {
        $this->stateRepo = $stateRepo;
        $this->data['page'] = 'classes';
        $this->courseService = $courseService;
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $this->data['class'] = $class = CourseClass::findOrFail($id);
        $this->data['locations'] = Location::all();
        $this->data['course'] = $course = $class->course;
        $this->data['isSeminar'] = $course->isSeminar();
        $this->data['instructors'] = Instructor::orderBy('first_name')->get();
        $this->data['states'] = $this->stateRepo->getByList('state', 'state')->toArray();
        $classes = CourseClass::whereHas('course', function ($query) use ($course) {
            $query->where('course_type_id', $course->course_type_id);
        })->get()->keyBy('id');
        $classes->forget($class->id);
        foreach ($class->classes as $c) {
            $classes->forget($c->id);
        }
        $this->data['classes'] = $classes;
        return view('admin.classes.show', $this->data);
    }

    public function create($id)
    {
        $this->data['course'] = Course::findOrFail($id);
        $this->data['instructors'] = Instructor::orderBy('first_name')->get();
        $this->data['locations'] = Location::all();
        return view('admin.classes.add', $this->data);
    }
    /**
     * Save class
     *
     * @param ClassRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(ClassRequest $request)
    {
        try {
            $data = $request->all();
            if($request->has('instructor_id'))
                $data['instructor_id'] = ($data['instructor_id'] != '') ? $data['instructor_id'] : null;
            if($request->has('location_id'))
                $data['location_id'] = ($data['location_id'] != '') ? $data['location_id'] : null;

            $this->courseService->saveClass($data);

            return redirect()->route('admin.classes', [$request->input('course_id')])
                ->with('success_message', 'Class has been saved.');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function linkItem(Request $request)
    {

        ClassService::linkItem($request);

        return back()->with(['success_message' => 'Successfully linked class!']);
    }

    public function unlinkItem(Request $request)
    {
        ClassService::unLinkItem($request);
        return back()->with(['success_message' => 'Successfully unlinked class!']);
    }

    public function update(Request $request, $id)
    {

        $class = CourseClass::find($id);

        if(! $class) {
            return back()->with(['error_message'    => 'Class Not Found']);
        }

        if($class->webcast_notification_flag) {
            $class->update([
                'webcast_notification_flag' => 0
            ]);
        } else {
            $class->update([
                'webcast_notification_flag' => 1
            ]);
        }

        return back()->with(['success_message' => 'Successfully updated class notification!']);
    }

}
