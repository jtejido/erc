<?php

namespace App\Http\Controllers\Admin;

use App\Events\ClassAttendeeWasRescheduled;
use App\Facades\OrderService;
use App\Models\ClassCombinationDiscount;
use App\Models\ClassRegistration;
use App\Models\Contact;
use App\Models\CourseClass;
use App\Models\OrderItem;
use App\Models\RegistrationOrder;
use App\Utilities\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Vinkla\Hashids\Facades\Hashids;


class RescheduleController extends BaseController
{
    /**
     * RescheduleController constructor.
     */
    public function __construct()
    {
        $this->data['page'] = 'reschedule';
    }


    /**
     * @param $orderItemId
     * @param $contactId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($orderItemId, $contactId)
    {
        try {
            $orderItem = OrderItem::findOrFail($orderItemId);
            $contact = Contact::findOrFail($contactId);
        } catch (ModelNotFoundException $e) {
            return back()->with([
                'error_message' => 'Order Item Not Found'
            ]);
        }

        // check if contact id is part of order item

        $order = $orderItem->order;
        $regOrder = $orderItem->orderable;
        $course = $regOrder->course();

        $this->data['order'] = $order;
        $this->data['orderItemId'] = $orderItemId;
        $this->data['contactId'] = $contactId;
        $this->data['contact'] = $contact;
        $this->data['course'] = $course;
        $this->data['regOrder'] = $regOrder;
        $this->data['classes'] = $course->classes()
            ->active()
            ->get()
            ->reject(function($class) use ($regOrder) {
                return $class->id == $regOrder->registrable_id;
            });

        return view('admin.reschedule.edit', $this->data);

    }

    public function store(Request $request, $orderItemId)
    {

        try {
            $current_orderItem = OrderItem::findOrFail($orderItemId);
            $course_class = CourseClass::findOrFail($request->input('class_id'));
            $contactId = $request->input('contact_id');
        } catch (ModelNotFoundException $e) {
            return back()->with([
                'error_message' => 'Order Item or Class Not Found'
            ]);
        }

        // TODO check if user is already registered on the class
        $contact_already_in_class = DB::select('select b.id from registration_orders a, class_registrations b 
            where a.id = b.registration_order_id and 
            a.registrable_id = ? and
            a.registrable_type = ? and
            b.attendee_contact_id = ? and b.deleted_at = null', [$course_class->id, 'CourseClass', $contactId]);

        if(count($contact_already_in_class)) {
            return back()->with([
                'error_message' => 'Contact Already In That Class'
            ]);
        }

        $current_registrationOrder = $current_orderItem->orderable;

        $order = $current_orderItem->order;

        DB::beginTransaction();

        try {

            $registrationOrder = $this->getRegOrder($order, $course_class->id);

            if(!$registrationOrder) {
                $params = array_except($current_registrationOrder->toArray(),
                    ['created_at', 'updated_at', 'id', 'slug']);
                $params['registrable_id'] = $course_class->id;
                $registrationOrder = new RegistrationOrder();
                $registrationOrder::$FIRE_EVENTS = false;
                $registrationOrder->fill($params);
                $registrationOrder->save();
                $registrationOrder->update([
                    'slug' => Hashids::encode($registrationOrder->id, $registrationOrder->originating_agent_user_id)
                ]);

                $params = array_except($current_orderItem->fresh()->toArray(),
                    ['id', 'created_at', 'updated_at', 'transaction_id']);
                $params['orderable_id'] = $registrationOrder->id;
                $orderItem = new OrderItem();
                $orderItem->fill($params);
                $orderItem->save();
            }

            $current_classRegistration = $current_registrationOrder->classRegistrations()
                ->where('attendee_contact_id', $contactId)->first();

            if (!$current_classRegistration) {
                throw new \Exception('Class Registration Not Found');
            }

            $params = array_except($current_classRegistration->toArray(),
                ['updated_at','created_at', 'id']);
            $params['registration_order_id'] = $registrationOrder->id;
            $classRegistration = new ClassRegistration();
            $classRegistration::$FIRE_EVENTS = false;
            $classRegistration->fill($params);
            $classRegistration->save();

            $this->updateClassRegDiscounts($current_classRegistration, $classRegistration);
            $this->updateClassCombinationDiscounts($current_classRegistration, $classRegistration, $course_class->id);
            $this->updateTransactionItems($registrationOrder, $current_registrationOrder, $classRegistration, $current_classRegistration);

            $current_classRegistration::$FIRE_EVENTS = false;
            $current_registrationOrder::$FIRE_EVENTS = false;
            $current_orderItem::$FIRE_EVENTS = false;

            $current_classRegistration_id = $current_classRegistration->id;

            $current_classRegistration->delete();

            if($current_registrationOrder->classRegistrations()->where('is_swapped', 0)->count() == 0)
            {
                $current_registrationOrder->delete();
                $current_orderItem->forceDelete();
            }

            $this->updateOrderItemPriceCharged($registrationOrder->orderItem->order);


        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with([
                'error_message' => $e->getMessage()
            ]);
        }

        DB::commit();

        $notify = ($request->input('doNotEmailHidden', '0') == '1') ? false : true;

        event(new ClassAttendeeWasRescheduled($current_classRegistration_id, $classRegistration->id, $notify));

        return redirect()->route('admin.order.info', [
            'user_id'   => $order->user_id,
            'order_id'   => $order->id,
        ])->with([
            'success_message' => 'Successfully Rescheduled.'
        ]);

    }

    public function getRegOrder($order, $class_id)
    {
        foreach ($order->items as $item) {
            $regOrder = $item->orderable;
            if($regOrder->registrable_type == 'CourseClass' and
                $regOrder->registrable_id == $class_id) {
                return $regOrder;
            }
        }

        return null;
    }

    /**
     * @param $current_registrationOrder
     * @param $classRegistration
     */
    protected function updateClassRegDiscounts($current_classRegistration, $classRegistration)
    {

        $subscriptionDiscount = $current_classRegistration->subscriptionDiscount;
        $cmDiscount = $current_classRegistration->classMilitaryDiscount;
        $chDiscount = $current_classRegistration->classHalfPriceDiscount;
        $ccDiscount = $current_classRegistration->classCouponDiscounts()->first();
        $ccreditDiscount = $current_classRegistration->classCreditDiscount;
        $orderAdjustmentDiscounts = $current_classRegistration->orderAdjustmentDiscounts;

        if($orderAdjustmentDiscounts->count() != 0) {
            foreach ($orderAdjustmentDiscounts as $orderAdjustmentDiscount) {
                $orderAdjustmentDiscount->update([
                    'adjustable_id' => $classRegistration->id
                ]);
            }
        }

        if ($subscriptionDiscount != null) {
            $subscriptionDiscount->update([
                'class_reg_id' => $classRegistration->id
            ]);
        }

        if ($cmDiscount != null) {
            $cmDiscount->update([
                'class_reg_id' => $classRegistration->id
            ]);
        }

        if ($chDiscount != null) {
            $chDiscount->update([
                'class_reg_id' => $classRegistration->id
            ]);
        }

        if ($ccDiscount != null) {
            $ccDiscount->update([
                'class_reg_id' => $classRegistration->id
            ]);
        }
        if ($ccreditDiscount != null) {
            $ccreditDiscount->update([
                'class_reg_id' => $classRegistration->id
            ]);
        }

    }

    protected function updateClassCombinationDiscounts($current_classRegistration, $classRegistration, $class_id)
    {
        $classCombinationDiscount =
            ClassCombinationDiscount::where(function($query) use ($current_classRegistration) {
                $query->where('class_reg_one', $current_classRegistration->id)
                    ->orWhere('class_reg_two', $current_classRegistration->id);
            })->first();

        if ($classCombinationDiscount != null) {
            if($classCombinationDiscount->class_reg_one == $current_classRegistration->id) {
                $classCombinationDiscount->update([
                    'class_reg_one' => $classRegistration->id
                ]);
            } else {
                $classCombinationDiscount->update([
                    'class_reg_two' => $classRegistration->id
                ]);
            }
            if($classCombinationDiscount->hwm_class_id == $current_classRegistration->registrationOrder->registrable_id) {
                $classCombinationDiscount->update([
                    'hwm_class_id' => $class_id
                ]);
            } else {
                $classCombinationDiscount->update([
                    'dot_class_id' => $class_id
                ]);
            }
        }
    }

    protected function updateOrderItemPriceCharged($order)
    {
        foreach ($order->items as $item) {
            if($item->orderable_type == 'RegistrationOrder' and $item->orderable->isClass()) {
                OrderService::setPriceCharged($item->orderable);
            }
        }

    }

    protected function updateTransactionItems($registrationOrder,
                                              $current_registrationOrder,
                                              $classRegistration,
                                              $current_classRegistration)
    {
        $order = $registrationOrder->orderItem->order;
        $invoicedPaymentTrans = $order->invoicePayments;
        $paymentTrans = $order->paymentTransactions;

        if ($paymentTrans->count()) {
            // find the payment trans to edit
            // edit the items

            $this->updatePaymentTransactionItems($registrationOrder,
                $current_registrationOrder,
                $classRegistration,
                $current_classRegistration,
                $paymentTrans);

        }

        if($invoicedPaymentTrans->count()) {

            $this->updatePaymentTransactionItems($registrationOrder,
                $current_registrationOrder,
                $classRegistration,
                $current_classRegistration,
                $invoicedPaymentTrans);

        }
    }

    protected function transContainsRegistration($trans, $regOrderId)
    {
        if(empty($trans->items)) {
            return false;
        }

        $items = json_decode($trans->items);

        foreach ($items->items as $item) {
            if($item->object == 'RegistrationOrder' and
                $item->id == $regOrderId) {
                return true;
            }
        }

    }

    protected function transContainsOrderMod($trans, $classRegId)
    {
        if(empty($trans->items)) {
            return false;
        }

        $items = json_decode($trans->items);

        foreach ($items->items as $item) {
            if($item->object == 'OrderAdjustment' and
                $item->adjustment_object == 'ClassRegistration' and
                $item->adjustment_id == $classRegId) {
                return true;
            }
        }

    }

    protected function getOriginalPriceCharged($trans, $regOrderId, $classRegId)
    {
        if(empty($trans->items)) {
            return false;
        }

        $items = json_decode($trans->items);

        foreach ($items->items as $item) {
            if($item->object == 'RegistrationOrder' and
                $item->id == $regOrderId) {
                foreach ($item->contents as $content) {
                    if($content->id == $classRegId) {
                        return $content->price_paid;
                    }
                }
            }
        }
    }

    /**
     * @param $registrationOrder
     * @param $current_registrationOrder
     * @param $classRegistration
     * @param $current_classRegistration
     * @param $paymentTrans
     * @return array
     */
    protected function updatePaymentTransactionItems($registrationOrder, $current_registrationOrder, $classRegistration, $current_classRegistration, $paymentTrans)
    {
        foreach ($paymentTrans as $trans) {

            if($this->transContainsOrderMod($trans, $current_classRegistration->id)) {

                $transItems = json_decode($trans->items);

                foreach ($transItems->items as $key => $item) {
                    if ($item->adjustment_object == 'ClassRegistration' and
                        $item->adjustment_id == $current_classRegistration->id) {

                        $note = adjustment_registration_added(
                            $classRegistration,
                            $classRegistration->registrationOrder,
                            $classRegistration->item_charge
                        );

                        $pos = strpos($note, '$');
                        $desc = substr($note, 0, $pos - 1);

                        $transItems->items[$key]->adjustment_id = $classRegistration->id;
                        $transItems->items[$key]->contents[0]->description = $desc;
                    }
                }

                $transItems = json_encode($transItems);

                $trans->update([
                    'items' => $transItems
                ]);

                break;

            } elseif ($this->transContainsRegistration($trans, $current_registrationOrder->id)) {

                $price_charge = $this->getOriginalPriceCharged($trans,
                    $current_registrationOrder->id,
                    $current_classRegistration->id);
                $transItems = json_decode($trans->items);


                if ($this->transContainsRegistration($trans, $registrationOrder->id)) {
                    foreach ($transItems->items as $key => $item) {
                        if ($item->object == 'RegistrationOrder' and
                            $item->id == $registrationOrder->id) {

                            $contents = $transItems->items[$key]->contents;

                            $contents[] = (object)[
                                'id' => $classRegistration->id,
                                'price_paid' => $price_charge,
                                'name' => $classRegistration->contact->name
                            ];

                            $transItems->items[$key]->contents = $contents;
                        }
                    }
                } else {
                    $transItems->items[] = (object)[
                        "id" => $registrationOrder->id,
                        "object" => 'RegistrationOrder',
                        "contents" => [(object)[
                            'id' => $classRegistration->id,
                            'price_paid' => $price_charge,
                            'name' => $classRegistration->contact->name
                        ]]
                    ];
                }
                // remove old class
                foreach ($transItems->items as $key => $item) {
                    if ($item->object == 'RegistrationOrder' and
                        $item->id == $current_registrationOrder->id) {
                        foreach ($item->contents as $j => $content) {
                            if ($content->id == $current_classRegistration->id) {
                                unset($item->contents[$j]);
                                $transItems->items[$key]->contents = array_values($item->contents);
                            }
                        }
                    }
                }

                // delete items with no content
                foreach ($transItems->items as $key => $item) {
                    if ($item->object == 'RegistrationOrder') {
                        if (count($item->contents) == 0) {
                            unset($transItems->items[$key]);
                            $transItems->items = array_values($transItems->items);
                        }
                    }
                }

                $transItems = json_encode($transItems);

                $trans->update([
                    'items' => $transItems
                ]);

                break;

            } else {
                continue;
            }
        }
    }

    protected function updateOrderOrderAdjustmentHistory($current_classRegistration, $classRegistration, $orderItemId)
    {

        $orderAdjustmentHistory = $current_classRegistration->orderAdjustmentHistory();
        $orderAdjustmentHistory->update([
            'note'              => adjustment_registration_added(
                $classRegistration,
                $classRegistration->registrationOrder,
                $classRegistration->item_charge
            )
        ]);

    }

}
