<?php namespace App\Http\Controllers\Admin;

use Session;
use App\Http\Requests;
use App\Models\Coupon;
use App\Repositories\CourseRepository;
use App\Repositories\CouponRepository;
use App\Repositories\CouponCourseRepository;
use App\Facades\DiscountService;
use App\Services\CouponCourseService;
use App\Services\RegistrationService;
use App\Http\Requests\CouponFormRequest;
use App\Utilities\Constant;
use Illuminate\Http\Request;

class CouponController extends BaseController
{
    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @var CouponRepository
     */
    private $couponRepo;

    /**
     * @var CouponCourseRepository
     */
    private $couponCourseRepo;

    /**
     * @var CouponCourseService
     */
    private $couponCourseService;

    /**
     * @var RegistrationService
     */
    private $registrationService;

    /**
     * @param CourseRepository $courseRepo
     * @param CouponRepository $couponRepo
     * @param CouponCourseRepository $couponCourseRepo
     * @param CouponCourseService $couponCourseService
     * @param RegistrationService $registrationService
     */
    public function __construct(CourseRepository $courseRepo,
                                CouponRepository $couponRepo,
                                CouponCourseRepository $couponCourseRepo,
                                CouponCourseService $couponCourseService,
                                RegistrationService $registrationService)
    {
        $this->courseRepo = $courseRepo;
        $this->couponRepo = $couponRepo;
        $this->couponCourseRepo = $couponCourseRepo;
        $this->couponCourseService = $couponCourseService;
        $this->registrationService = $registrationService;

        $this->data['courses']  = $this->courseRepo->getAll();
        $this->data['coupon_types']  = [Constant::PERCENT => Constant::PERCENT,
                                        Constant::FIXED   => Constant::FIXED];
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->data['page'] = 'coupons';
        $this->data['coupons'] = Coupon::withTrashed()->orderBy('coupon_type')->get();

        return view('admin.coupons.index', $this->data);
    }

    /**
     * Delete coupon
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        try {
            if ($request->isMethod('POST')) {
                $coupon = $this->couponRepo->getById($request->get('id'), true);
                $coupon->forceDelete();

                Session::flash('success_message', 'Coupon successfully deleted.');

                return $coupon;

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        $this->data['page'] = 'add_coupon';

        $this->data['coupon_code'] = str_random(6);
        $couponId = $request->route('id');

        if(isset($couponId)) {
            $this->data['coupon'] = $this->couponRepo->getById($couponId);
            $this->data['discountableIds']  = $this->data['coupon']
                                                   ->couponCourses
                                                   ->pluck('discountable_id')
                                                   ->toArray();

            $this->data['discountableType'] = $this->data['coupon']
                                                   ->couponCourses
                                                   ->pluck('discountable_type')
                                                   ->first();

        }

        return view('admin.coupons.add_edit', $this->data);
    }

    /**
     * Save resource in storage.
     *
     * @param CouponFormRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function save(CouponFormRequest $request)
    {
        try {
            $result = $this->couponCourseService->save($request->all());

            if($result) {

                return redirect()->back()
                                 ->with([
                                     'success_message' => 'Coupon successfully saved.',
                                     'searchkey' => $result->coupon_code
                                 ]);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Apply Coupon
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function applyCoupon(Request $request)
    {
        try {

            return DiscountService::applyCoupon($request->all());

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
