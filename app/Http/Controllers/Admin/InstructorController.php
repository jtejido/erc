<?php

namespace App\Http\Controllers\Admin;

use Session;
use Storage;
use App\Models\Instructor;
use App\Repositories\InstructorRepository;
use App\Services\InstructorService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\InstructorRequest;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 'instructors';
        $instructors = Instructor::orderBy('first_name')->get();

        return view('admin.instructors.index', compact('page', 'instructors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page = 'add_instructor';

        return view('admin.instructors.create_edit', compact('page'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InstructorRequest $request
     * @param InstructorService $instructorService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(InstructorRequest $request,
                          InstructorService $instructorService)
    {
        try {

            $instructor = $instructorService->save($request->all());

            if ($instructor) {
                return redirect()->route('instructor.edit', $instructor->id)
                                 ->with('success_message', 'Instructor successfully created.');

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param InstructorRepository $instructorRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request,
                         InstructorRepository $instructorRepo)
    {
        $page = 'edit_instructor';
        $instructor = $instructorRepo->getById($request->route('id'));

        return view('admin.instructors.create_edit', compact('page', 'instructor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param InstructorRequest $request
     * @param InstructorService $instructorService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(InstructorRequest $request,
                           InstructorService $instructorService)
    {
        try {
            $instructor = $instructorService->save($request->all());

            if ($instructor) {
                return redirect()->route('instructor.edit', $instructor->id)
                                 ->with('success_message', 'Instructor successfully edited.');

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id, InstructorRepository $instructorRepo)
    {
        $instructor = $instructorRepo->getById($id);

        if ($instructor) {

            if ($instructor->signature) {
                if (Storage::disk('local')->exists('signatures/'.$instructor->signature)) {
                    Storage::disk('local')->delete('signatures/'.$instructor->signature);
                }
            }

            $instructor->delete();

            Session::flash('success_message', 'Instructor successfully deleted.');
        }

        return '';
    }
}
