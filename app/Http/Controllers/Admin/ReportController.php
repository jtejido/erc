<?php namespace App\Http\Controllers\Admin;

use App\Repositories\ClassRepository;
use App\Repositories\CouponRepository;
use App\Repositories\StateRepository;
use App\Services\ReportService;
use Illuminate\Http\Request;

use App\Http\Requests;

class ReportController extends BaseController
{

    /**
     * @param Request $request
     * @param ClassRepository $classRepo
     * @param StateRepository $stateRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function classes(Request $request,
                            ClassRepository $classRepo,
                            StateRepository $stateRepo)
    {
        $this->data['page'] = 'reports_class';
        $this->data['classes'] = $classRepo->searchFilterReports($request->all())->paginate(20);
        $classIds = $classRepo->searchFilterReports($request->all())->get()->pluck('id')->toArray();
        $this->data['classIds'] = implode($classIds, ',');
        $this->data['states'] = $stateRepo->getAll();
        $this->data['startDate'] = $request->get('start_date');
        $this->data['endDate'] = $request->get('end_date');
        $this->data['city'] = $request->get('city');
        $this->data['sstate'] = $request->get('state');
        $this->data['title'] = $request->get('title');

        return view('admin.reports.class', $this->data);
    }

    /**
     * @param Request $request
     * @param CouponRepository $couponRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function coupons(Request $request,
                            CouponRepository $couponRepo)
    {
        $this->data['page'] = 'reports_coupon';
        $this->data['coupons'] = $couponRepo->searchFilterReports($request->all())->paginate(20);
        $couponIds = $couponRepo->searchFilterReports($request->all())->get()->pluck('id')->toArray();
        $this->data['couponIds'] = implode($couponIds, ',');
        $this->data['title'] = $request->get('title');

        return view('admin.reports.coupon', $this->data);
    }

    /**
     * @param Request $request
     * @param ReportService $reportService
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function generateClass(Request $request,
                                  ReportService $reportService)
    {
        $fileName = 'Class Report.pdf';
        $file = $reportService->generateClassReport($request->all());
        $file->save($fileName, true);

        return response()->download($fileName);
    }

    /**
     * @param Request $request
     * @param ReportService $reportService
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function generateCoupon(Request $request,
                                   ReportService $reportService)
    {
        $fileName = 'Coupon Report.pdf';
        $file = $reportService->generateCouponReport($request->all());
        $file->save($fileName, true);

        return response()->download($fileName);
    }

}
