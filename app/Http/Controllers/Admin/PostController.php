<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Models\Post;
use App\Utilities\Constant;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Session;

class PostController extends BaseController
{

    public function index()
    {
        $this->data['page']    = 'posts';
        $this->data['posts'] = Post::all();
         
        return view('admin.posts.index', $this->data);
    }

    public function save(Request $request) {
        $data = $request->all();
        $id = $data['id'];
        $post = Post::find($id);
        $post->content = $data['content'];
        $post->save();

        request()->session()->flash('success_message', 'Content successfully saved.');

        return new JsonResponse();
    }

}
