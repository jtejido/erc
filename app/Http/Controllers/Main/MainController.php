<?php namespace App\Http\Controllers\Main;

use App\Facades\CourseUtility;
use App\Models\Promo;
use App\Models\Regulation;
use App\Models\Tip;
use App\Repositories\ClassRegistrationRepository;
use App\Utilities\Constant;
use Auth;
use App\Http\Requests;
use App\Models\OrderItem;
use App\Models\OnsiteTraining;
use App\Repositories\InvoicedPaymentRecipientRepository;
use App\Repositories\InvoicedPaymentRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\ContactRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Facades\OrderUtility;

class MainController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var InvoicedPaymentRepository
     */
    private $invoicedPaymentRepository;

    /**
     * @var InvoicedPaymentRecipientRepository
     */
    private $invoicedPaymentRecipientRepo;
    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param InvoicedPaymentRepository $invoicedPaymentRepository
     * @param InvoicedPaymentRecipientRepository $invoicedPaymentRecipientRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                InvoicedPaymentRepository $invoicedPaymentRepository,
                                InvoicedPaymentRecipientRepository $invoicedPaymentRecipientRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->userRepo   = $userRepo;
        $this->orderRepo  = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->invoicedPaymentRepository    = $invoicedPaymentRepository;
        $this->invoicedPaymentRecipientRepo = $invoicedPaymentRecipientRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $this->data['page'] = 'home';

        $this->data['fbApi'] = env('APP_ENV') == 'local'
                                ? (env('APP_URL') == 'https://ww2.ercweb.com'
                                    ? '1540563629321237'
                                    : '1913969722221372')
                                : '1883436765212589';

        return view('main.homepage.index', $this->data);
    }

    public function articles()
    {

        $this->data['articles'] = $this->getArticles();

        return view('main.homepage._articles', $this->data);

    }

    public function promos()
    {

        $this->data['promos'] = $this->getPromos();

        return view('main.homepage._promotion', $this->data);

    }

    /**
     * Get user details page
     *
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function userDetails(Request $request,
                                ContactRepository $contactRepository)
    {
        try {
            $userId = $request->route('user_id');
            $contactId = $request->route('contact_id');

            $user = isset($userId)
                      ? $this->userRepo->getById($userId)
                      : (isset($contactId)
                        ? null
                        : auth()->user());

            $this->data['page'] = 'dashboard';
            $this->data['user'] = $user;
            $this->data['userDetails'] = $contact = $contactRepository->getById($contactId) ?: $user->contact;
            $this->data['userHasNoCompany'] = $contact->hasNoCompany();
            $this->data['user_Id'] = $userId;
            $this->data['contact_Id'] = $contactId;

            if ($user && $user->hasSubscription) {

                $this->data['hasSubscription'] = true;
                $this->data['subscription'] = $user->hasSubscription;
                $this->data['subscriptionType'] = $user->hasSubscription
                                                       ->clientSubscription
                                                       ->yearlySubscription
                                                       ->type
                                                       ->name;

                $expDate = $user->hasSubscription
                                ->clientSubscription
                                ->subscription_exp_date;

                if (!is_null($expDate)) {
                    $this->data['expDate'] = Carbon::parse($expDate)->format('F d, Y');
                }
            }

            $view = isset($userId) || isset($contactId)
                ? view('admin.users.index', $this->data)
                : view('main.users.index', $this->data);

            return $view;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Get user transactions list
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */

    public function transactions_api(Request $request)
    {

        $data = [];

        $userId = $request->route('user_id');
        $contactId = $request->route('contact_id');

        $user = isset($userId)
                      ? $this->userRepo->getById($userId)
                      : (isset($contactId)
                        ? null
                        : auth()->user());
        
        $transactions = $this->orderItemRepo->getAllItems($user ? $user->id : null, $contactId, false);

        foreach ($transactions as $orderItem) {

            $order = $orderItem->order()->withTrashed()->first();
            $isActingAgent = null;
            $completedOrInvoiced = OrderUtility::isCompleteOrInvoiced($orderItem);
            $isClassRegistration = $orderItem->orderable_type == Constant::REGISTRATION_ORDER;

            if($isClassRegistration and is_null($orderItem->orderable()->withTrashed()->first())) {
                continue;
            }

            $_data['trans_date'] = $order->trans_date;

            $_data['title'] = '';

            if (!($orderItem->orderable()->withTrashed()->first() instanceof OnsiteTraining) 
                and (class_basename($orderItem->orderable) == Constant::REGISTRATION_ORDER)
                and ($orderItem->orderable->registrable->course_type_id != Constant::COMPUTER_BASED)) {
                $training_id = $orderItem->orderable()->withTrashed()->first()->registrable->id;
                if ($orderItem->orderable()->withTrashed()->first()->registrable->course) {
                    $training_id = $orderItem->orderable()->withTrashed()->first()->registrable->course->id;
                }

                $_data['title'] .= '<a href="'
                    .route('admin.classes', ['id' => $training_id])
                    .'">'
                    .OrderUtility::getTitle($orderItem)
                    .'</a>';


                $_data['title'] .= '<p><em><strong>Class Date:</strong>'
                 .CourseUtility::getClassDate($orderItem)
                 .'</em></p>';
                $_data['title'] .= '<p><em><strong>Class Location:</strong>'
                 .CourseUtility::getClassLocation($orderItem)
                 .'</em></p>';


                if (!($orderItem->orderable()->withTrashed()->first() instanceof OnsiteTraining)
                 and (class_basename($orderItem->orderable()->withTrashed()->first()) == Constant::REGISTRATION_ORDER)
                 and ($orderItem->orderable()->withTrashed()->first()->registrable->course_type_id != Constant::COMPUTER_BASED)) {
                    $class_id = $orderItem->orderable()->withTrashed()->first()->id; 
                    if ($orderItem->orderable()->withTrashed()->first()->registrable->course) {
                        $class_id = $orderItem->orderable()->withTrashed()->first()->registrable_id;
                    }

                    $_data['title'] .= '<p><small>[Code:' .$class_id . ']</small></p>';
                }
                if($notes = OrderUtility::getNotes($orderItem)) {
                    $_data['title'] .= '<p style="display:none;">';
                        foreach($notes as $note) {
                            $_data['title'] .= strip_tags($note->notes);
                        }
                    $_data['title'] .= '</p>';
                }


            } else {
                $_data['title'] .= OrderUtility::getTitle($orderItem);
            }

            $_data['location'] = OrderUtility::getLocation($orderItem);

            $_data['agent'] = OrderUtility::getAgent($orderItem);


            $notPending = OrderUtility::getStatus($orderItem) != Constant::PENDING;
            $notCancelled = OrderUtility::getStatus($orderItem) != Constant::CANCELLED;
            $isRegistration = $orderItem->orderable_type == Constant::REGISTRATION_ORDER;
            $isOrder = OrderUtility::isInvoiced($order);
            $isOriginatingAgent = ($user)
                                  ? OrderUtility::isOriginatingAgent($order->id, $user->id)
                                  : false;

            $_data['status'] = '';

            if ($order->trashed()) {
                $_data['status'] = '<span class="label label-danger">DELETED</span>';
            } else {
                if (OrderUtility::getStatus($order) == Constant::COMPLETE) {

                            $label = 'label-success';
                            $withLink = false;

                } elseif (OrderUtility::getStatus($order) == Constant::PENDING) {
                    
                            $label = 'label-danger';
                            $withLink = true;
                    
                } else {

                            $label = 'label-warning';
                            $withLink = false;

                }
                if ($withLink && $user) {
                    $_data['status'] = '<a href="'
                    .route('admin.user.shopping_cart', ['user_id' =>  $user->id])
                    .'"><span class="label '
                    .$label
                    .'">'
                    .OrderUtility::getConvertedStatus($orderItem)
                    .'</span></a>';
                } else {
                    $_data['status'] = '<span class="label '
                    .$label
                    .'">'
                    .OrderUtility::getConvertedStatus($orderItem)
                    .'</span>';
                }
            }

            $_data['actions']   = '';

            if ($isClassRegistration and $user) {
                $isActingAgent = OrderUtility::isActingAgent([
                    'orderable_id' => $orderItem->orderable_id,
                    'orderable_type' => $orderItem->orderable_type,
                    'acting_agent_id' => $user->id
                ]);
            }

            $hideIfRemovedActingAgent = ($isActingAgent and $isActingAgent->trashed()) ? 'hidden' : '';
            $notActiveClass = ($isClassRegistration and
                           $orderItem->orderable()->withTrashed()->first()->registrable_type == Constant::COURSE_CLASS_OBJECT and
                           is_null(CourseUtility::scopeActive($orderItem->orderable()->withTrashed()->first()->registrable)))
                           ? 'hidden'
                           : '';

           $hideIfOnsiteTraining = $orderItem->orderable_type ==  Constant::ONSITE_TRAINING
                                    ? 'hidden'
                                    : '';


            if (!$order->trashed()) {
                if ($user and $completedOrInvoiced and $isClassRegistration) {
                    if ($orderItem->orderable()->withTrashed()->first()->isCourse()) {

                        $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfRemovedActingAgent
                        .'" href="'
                        .route('admin.certificates.showCourse', [$orderItem->orderable()->withTrashed()->first()->registrable->id])
                        .'">View Details</a>';

                    } else {

                        $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfRemovedActingAgent
                        .'" href="'
                        .route('admin.certificates.show', [$orderItem->orderable()->withTrashed()->first()->registrable->id])
                        .'">View Details</a>';

                    }
                } elseif($user and $orderItem->orderable_type == Constant::ONSITE_TRAINING) {

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfRemovedActingAgent
                        .'" href="'
                        .route('admin.course.onsite-training', [$orderItem->id, $user->id])
                        .'">View Details</a>';

                }

                if ($isOrder && $isOriginatingAgent && $user) {

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info'
                        .'" href="'
                        .route('admin.order.invoice_info', [
                        'user_id'  => $user->id,
                        'order_id' => $order->id
                        ])
                        .'" target="_blank">View Invoice</a>';

                }

                if ($user and $completedOrInvoiced and $isOriginatingAgent) {

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfOnsiteTraining
                        .'" href="'
                        .route('admin.order.info', [$user->id, $order->id])
                        .'">View Entire Order</a>';


                    if ($isRegistration) {

                        $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfRemovedActingAgent
                        .' '
                        .$notActiveClass
                        .'" href="'
                        .route('admin.order.attendees.add.get', [$user->id, $orderItem->id])
                        .'">Add Attendees</a>';

                    }
                }

                if ($notPending && $notCancelled && $isRegistration && $user) {

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary action-info '
                        .$hideIfRemovedActingAgent
                        .' '
                        .$notActiveClass
                        .'" href="'
                        .route('admin.order.attendees.swap', [
                            'user_id' => $user->id,
                            'order_item_id' => $orderItem->id
                            ])
                        .'">Swap Attendees</a>';

                }

                if ($user and $order->status == Constant::COMPLETE AND $orderItem->order->total > 0) {

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary "'
                        .'href="'
                        .route('admin.payment.summary', [
                            'user_id' => $user->id,
                            'order_id' => $order->id
                            ])
                        .'">View Payment</a>';

                    $_data['actions'] .= '<a class="btn btn-block btn-flat btn-primary "'
                        .'href="'
                        .route('admin.receipt', [
                            'user_id' => $user->id,
                            'order_id' => $order->id
                            ])
                        .'">Download Receipt</a>';

                }
            }

            $data[] = $_data;

        }

        $r_data['draw'] = intval($request->input('draw'));
        $r_data['recordsTotal'] = count($data);
        $r_data['recordsFiltered'] = count($data);
        $r_data['data'] = $data;


        return response()->json($r_data);
    }

    /**
     * Show details of a class
     * @param $user_id
     * @param $transaction_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userTrainingDetail($user_id, $transaction_id)
    {
        $user = isset($user_id)
            ? $this->userRepo->getById($user_id)
            : (isset($contactId)
                ? null
                : Auth::user());

        $orderitem = $this->orderItemRepo->getById($transaction_id);

        $this->data['title'] = OrderUtility::getTitle($orderitem);
        $this->data['description'] = OrderUtility::getDescription($orderitem);
        $this->data['students'] = $this->classRegistrationRepo->getStudentsOfRegistrationOrder($orderitem->orderable, false);
        $this->data['available'] = $available = OrderUtility::isMaterialAvailable($orderitem);
        $this->data['available_date'] = OrderUtility::getMaterialAvailabiltyDate($orderitem);
        $this->data['page'] = 'user_training_details';
        $this->data['user'] = $user;
        $this->data['regOrder'] = $orderitem->orderable;
        $this->data['classId'] = $orderitem->orderable->registrable->id;
        $this->data['regOrderId'] = $orderitem->orderable->id;
        $this->data['materials'] = ($available) ? OrderUtility::getFiles($orderitem) : collect();
        $this->data['ebooks'] = ($available) ? OrderUtility::getEbooks($orderitem) : collect();
        $this->data['rel_courses'] = OrderUtility::getCourses($orderitem);
        $this->data['course'] = OrderUtility::getCourseInstance($orderitem);
        $this->data['class'] = $orderitem->orderable->registrable;

        if (class_basename($this->data['class']) == Constant::COURSE_CLASS_OBJECT) {
            $this->data['materials_not_expired'] = CourseUtility::isClassStartedNotExpired($this->data['class']);
        }

        $view = isset($userId) || isset($contactId)
            ? view('admin.users.details', $this->data)
            : view('main.users.details', $this->data);
        return $view;
    }

    public function onsiteTrainingDetails($transaction_id, $user_id = null)
    {
        $user = ($user_id)
            ? $this->userRepo->getById($user_id)
            : (isset($contactId)
                ? null
                : auth()->user());

        $orderitem = $this->orderItemRepo->getById($transaction_id);
        $this->data['page'] = 'user_training_details';
        $this->data['description'] = OrderUtility::getDescription($orderitem);
        $this->data['course'] = $orderitem->orderable;
        $this->data['user'] = $user;
        $this->data['order_item'] = $orderitem;

        $view = ($user_id) || isset($contactId)
            ? view('admin.courses.onsite-training', $this->data)
            : view('main.users.onsite-training', $this->data);

        return $view;
    }

    /**
     * @param \App\Models\OrderItem $orderitem
     * @return array $data
     */
    public function getStudentsList(OrderItem $orderitem)
    {
        $data = [];
        $classRegistrations = $orderitem->orderable->trashedRegistrations;

        if ($classRegistrations) {
            foreach ($classRegistrations as $classRegistration) {
                $trashed = $classRegistration->trashed() ? ' <span style="color:#d9534f">removed</span>' : '';
                $contact = $classRegistration->contact;
                $name = '';

                if ($contact) {
                    $name = $contact->first_name . ' ' . $contact->last_name . $trashed;
                }
                $id = $classRegistration->attendee_contact_id;
                $regOrderId = $classRegistration->registration_order_id;
                $certAvailable = $classRegistration->isCertificateAvailable();
                $attended = $classRegistration->isConfirmed();

                array_push($data, compact('id', 'name', 'trashed', 'regOrderId', 'certAvailable', 'attended'));
            }
        }
        return $data;
    }
}
