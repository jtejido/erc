<?php

namespace App\Http\Controllers\Main;

use App\Models\CourseClass;
use App\Models\CourseType;
use App\Repositories\ClassRepository;
use App\Utilities\Constant;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClassCalendarController extends BaseController
{
    /**
     * @var ClassRepository
     */
    private $classRepository;

    /**
     * ClassCalendarController constructor.
     * @param ClassRepository $classRepository
     */
    public function __construct(ClassRepository $classRepository)
    {
        $this->classRepository = $classRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['course_types'] = $ctypes = CourseType::all();
        foreach ($ctypes as $ctype) {
            switch ($ctype->id) {
                case 1: $ctype->color = Constant::SEMINAR_COLOR; break;
                case 2: $ctype->color = Constant::WEBCAST_COLOR; break;
                case 3: continue;
                default: $ctype->color = Constant::SEMINAR_COLOR;
            }
        }
        $this->data['page'] = 'class_calendar';
        return view('main.calendar.index', $this->data);
    }

    public function index_data(Request $request)
    {

        $classes = $this->classRepository->NotPrivatebetween_and_activated($request->input('start'),$request->input('end'));

        $events = collect();

        $dt = "<dt style='font-weight:bold'>%s</dt><dd>%s</dd>";

        foreach ($classes as $class) {
            $desc = "<dl>";
            $desc .= sprintf($dt, 'Code', $class->sku);
            $desc .= sprintf($dt, 'Price', $class->price);
            $desc .= sprintf($dt, 'Length', $class->duration);
            $desc .= "</dl><code>Click the event for more details</code>";

            switch ($class->type) {
                case 1:
                    $color = Constant::SEMINAR_COLOR;
                    $title = $class->title . ' (' . $class->shortLocation . ')';
                    break;
                case 2:
                    $color = Constant::WEBCAST_COLOR;
                    $title = $class->title;
                    break;
                case 3: $color = Constant::COMPUTER_BASED_COLOR; break;
                default:
                    $color = Constant::SEMINAR_COLOR;
                    $title = $class->title . ' (' . $class->shortLocation . ')';
            }



            $events->push((object) [
                        'id'    => $class->id,
                        'title' => $title,
                        'start' => $class->start_date->toDateString(),
                        'end'   => $class->end_date->addDay()->toDateString(),
                        'desc'   => $class->description,
                        'description'   => $desc,
                        'color'         => $color,
                        'url'           => route('course.show', $class->course->slug)
                ]);
        }

        return $events->toJson();
    }
}
