<?php

namespace App\Http\Controllers\Main;

use App\Events\CourseEbookUpdateEvent;
use App\Events\EditOrderEvent;
use App\Exceptions\BookNotFoundException;
use App\Http\Requests\AddBookToCartRequest;
use App\Http\Requests\BookEntryRequest;
use App\Models\Book;
use App\Models\Course;
use App\Models\User;
use App\Repositories\BookEntryRepository;
use App\Repositories\Criteria\SortByCriteria;
use App\Repositories\FileEntryRepository;
use App\Repositories\ProductOrdersRepository;
use App\Repositories\ContactRepository;
use App\Utilities\Constant;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repositories\BookRepository;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use App\Traits\UpsShippingTrait;
use App\Models\Order;
class BookController extends BaseController
{
    use UpsShippingTrait;
    /**
     * @var BookRepository
     */
    protected $bookRepo;

    /**
     * @var BookEntryRepository
     */
    protected $bookEntryRepo;

    protected $productOrdersRepo;
    /**
     * @var FileEntryRepository
     */
    private $fileEntryRepository;

    /**
     * bookController constructor.
     * @param BookRepository $bookRepo
     * @param BookEntryRepository $bookEntryRepo
     * @param ProductOrdersRepository $productOrdersRepository
     * @param FileEntryRepository $fileEntryRepository
     */
    public function __construct(BookRepository $bookRepo,
                                BookEntryRepository $bookEntryRepo,
                                ProductOrdersRepository $productOrdersRepository, FileEntryRepository $fileEntryRepository)
    {
        $this->bookRepo = $bookRepo;
        $this->bookEntryRepo = $bookEntryRepo;
        $this->productOrdersRepo = $productOrdersRepository;
        $this->fileEntryRepository = $fileEntryRepository;
    }

    public function buyBook(Request $request, $user_id)
    {
        $uri = $request->path();
        $this->data['category']  = $request->get('category');
        $this->data['page_flag'] = ($uri == 'products') ? 1 : 0;
        $this->data['page'] = 'books_index';
        $this->data['sort'] = collect([
            '1' => 'Sort by Name (A-Z)',
            '2' => 'Sort by Name (Z-A)',
            '3' => 'Sort by Price (Low-High)',
            '4' => 'Sort by Price (High-Low)',
        ]);


        $this->data['user'] = User::findOrFail($user_id);


        $this->data['sortby'] = ($request->has('sortby')) ? $request->get('sortby') : '1';
        $this->data['keyword'] = ($request->has('keyword')) ? trim($request->get('keyword')) : '';

        $this->bookRepo->sortBooks($this->data['sortby']);

        $this->bookRepo->searchByKeyword($this->data['keyword']);

        $this->data['books'] = $this->bookRepo->paginate($this->page_limit);
        $this->data['products_show'] = ($this->data['page_flag'])
            ? 'products.show'
            : 'admin.books.show';

        return ($this->data['page_flag'])
            ? view('main.books.index', $this->data)
            : view('admin.books.index', $this->data);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $uri = $request->path();
        $this->data['category']  = $request->get('category');
        $this->data['page_flag'] = ($uri == 'products') ? 1 : 0;
        $this->data['page'] = 'books_index';
        $this->data['sort'] = collect([
            '1' => 'Sort by Name (A-Z)',
            '2' => 'Sort by Name (Z-A)',
            '3' => 'Sort by Price (Low-High)',
            '4' => 'Sort by Price (High-Low)',
        ]);

        $this->data['sortby'] = ($request->has('sortby')) ? $request->get('sortby') : '1';
        $this->data['keyword'] = ($request->has('keyword')) ? trim($request->get('keyword')) : '';

        $this->bookRepo->sortBooks($this->data['sortby']);

        $this->bookRepo->searchByKeyword($this->data['keyword']);

        $this->data['books'] = $this->bookRepo->paginate($this->page_limit);
        $this->data['products_show'] = ($this->data['page_flag'])
                                        ? 'products.show'
                                        : 'admin.books.show';

        return ($this->data['page_flag'])
            ? view('main.books.index', $this->data)
            : view('admin.books.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['page'] = 'books_create';
        return view('admin.books.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookRequest|Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function store(BookRequest $request)
    {

        $fields = $request->only('code','name','description','weight','price','discounted_price', 'slug');
        $book = $this->bookRepo->createBook($request, $fields);
        if (!$book) {
            abort(404);
        }
        return redirect()->route('admin.books.index')
                        ->with(['success_message' => 'Book has been added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $uri = $request->segment(1);
        $this->data['page_flag'] = ($uri == 'products') ? 1 : 0;

        $book = Book::find($id);

        if($book and $this->data['page_flag']) {
            return redirect()->route('products.show', [ 'slug' => $book->slug ]);
        }

        if(!$book) {
            try {
                $book = Book::where('slug', $id)->firstOrFail();
            } catch (ModelNotFoundException $e) {
                return abort(404);
            }
        }

        $this->data['page'] = 'books_show';
        $this->data['book'] = $book;

        $this->data['book_entries'] = $book->bookEntries;
        $courses = Course::all()->keyBy('id');
        foreach ($book->courses as $c) {
            $courses->forget($c->id);
        }
        $this->data['courses'] = $courses;
        return ($this->data['page_flag'])
            ? view('main.books.show', $this->data)
            : view('admin.books.show', $this->data);
    }

    public function associateBookToCourse(Request $request)
    {
        $msg = "%s was linked to this book";
        $courseId = $request->get('course_id');
        $item_id = $request->get('item_id');
        $course = Course::findOrFail($courseId);
        $course->books()->save(Book::findOrFail($item_id));
        $msg = sprintf($msg, 'Course');

        return back()->with(['success_message' => $msg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['page'] = 'books_edit';
        $this->data['book'] = $book = $this->bookRepo->find($id);
        if (!$book ) {
            throw new BookNotFoundException;
        }
        return view('admin.books.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BookRequest|Request $request
     * @param  int $id
     * @return Response
     */
    public function update(BookRequest $request, $id)
    {
        $book = $this->bookRepo->find($id);
        if (!$book ) {
            abort(404);
        }
        $fields = $request->only('name','description','weight','price','discounted_price', 'code', 'slug');
        $this->data['book'] = $this->bookRepo->updateBook($request, $book, $fields);
        return redirect()->route('admin.books.show', [ $book->fresh()->slug ])
                         ->with(['success_message' => 'Book has been saved.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        if ($book->courses->count()) {
            return back()
                    ->with(['error_message' => 'Book cannot be deleted because of existing course association(s)']);
        }
        $this->bookRepo->deleteBook($book, $this->fileEntryRepository);
        return redirect()->route('admin.books.index')
            ->with(['success_message' => 'Book has been deleted.']);
    }

    /**
     * @param $filename
     * @return Response
     */
    public function getThumbnail($filename)
    {
        $entry = Book::where('thumbnail', $filename)->firstOrFail();
        $file = $this->bookRepo->getFile(Book::FOLDERPATH_THUMBS,$filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->thumbnail_mime);
    }

    public function addBookToCart(AddBookToCartRequest $request)
    {
        $userId = ($request->has('user_id'))
            ? $request->input('user_id')
            : auth()->user()->id;
        $book = $this->bookRepo->find($request->get('book_id'));
        $discounted = $request->input('discounted', false);
        $this->productOrdersRepo->addBookToCart($book, $request->get('quantity'), $userId, $discounted);

        if($request->has('user_id')) {
            $param = [
                'user_id'  => $userId,
                'open_flag' => 0,
                'save_flag' => 1
            ];

            event(new EditOrderEvent($param));
        }

        return ($request->has('user_id'))
            ? redirect()->route('admin.user.shopping_cart', [$userId])
            : redirect()->route('user.shopping_cart');
    }

    public function updateBookQuantityOnCart(AddBookToCartRequest $request, ContactRepository $contactRepo)
    {
        $client_id = $request->get('client_id');
        $uri = $request->segment(1);
        $userId = ($client_id) ? $client_id : auth()->user()->id;
        $contact = $contactRepo->getById($userId);
        $state = $contact->shipping_state ? $contact->shipping_state : $contact->state;
        $is_pr = ($state == 'Puerto Rico') ? true : false;
        $book = $this->bookRepo->find($request->get('book_id'));
        $status = $request->get('status');
        $up = $this->productOrdersRepo->updateBookToCart($book, $request->get('quantity'), $userId, $status);
        $weight = ((float) $request->get('weight') * (int)$request->get('quantity'));
        $item = (object) ['weight' => $weight];
        $items[] = $item;

        if(count($items)> 0 AND $up->modified_order_id > 0 AND $up->shipping_zip > 0){
            $shipping_rate = $this->ship_rate($items,$up->shipping_zip,$is_pr);
            $this->ship_order_update($up->modified_order_id,$shipping_rate);
        }

        if($client_id) {
            $param = [
                'user_id'  => $userId,
                'open_flag' => 0,
                'save_flag' => 1
            ];

            event(new EditOrderEvent($param));
        }

        return $status == Constant::PENDING
                ? ($client_id
                    ? redirect()->route('admin.user.shopping_cart', [$client_id])
                    : redirect()->route('user.shopping_cart'))
                : redirect()->back();
    }

    /**
     * @param BookEntryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addFileToBook(BookEntryRequest $request)
    {

        $notify = $request->has('doNotEmail') ? false : true;
        $book = Book::findOrFail($request->get('item_id'));

        if($request->hasFile('file') or $request->hasFile('otherfiles')) {
            $updatedFiles = $this->fileEntryRepository->addBookFiles($request, $book);
            $this->data['page']   = 'edit_course';
            $this->data['book'] = $book;

            event(new CourseEbookUpdateEvent($book->id, $updatedFiles, $notify));
        }


        return redirect()->route('admin.books.show', [$book->fresh()->slug]);
    }

}
