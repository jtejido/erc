<?php namespace App\Http\Controllers\Main;


use App\Http\Requests\CouponLandingRequest;
use App\Models\Coupon;
use App\Models\CouponLanding;
use App\Models\Course;
use App\Models\CourseClass;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CouponLandingController extends BaseController
{

    /**
     * CouponLandingController constructor.
     */
    public function __construct()
    {
        $this->data['page'] = 'coupon_landing';
    }

    public function index()
    {
        $this->data['coupon_landings'] = CouponLanding::all();
        return view('admin.coupon_landing.index', $this->data);
    }

    public function create()
    {
        $this->data['coupons'] = Coupon::orderBy('coupon_code')->lists('coupon_code','id');
        $this->data['slug'] = generate_slug(auth()->user()->id);
        return view('admin.coupon_landing.create', $this->data);
    }

    public function store(CouponLandingRequest $request)
    {
        $params = $request->only('title','content','published', 'coupon_id', 'slug');
        $coupon_landing = CouponLanding::create($params);
        if (!$coupon_landing) {
            abort(404);
        }
        return redirect()->route('admin.coupon_landing.index')
            ->with(['success_message' => 'Coupon landing page has been created.']);
    }

    public function edit($id)
    {
        $this->data['coupons'] = Coupon::orderBy('coupon_code')->lists('coupon_code','id');
        $this->data['coupon_landing'] = $coupon_landing = CouponLanding::findOrFail($id);
        $this->data['slug'] = $coupon_landing->slug;
        return view('admin.coupon_landing.edit', $this->data);
    }

    public function update(CouponLandingRequest $request, $id)
    {
        $slug = $request->input('slug');
        $coupon_landing = CouponLanding::findOrFail($id);

        if($coupon_landing->slug != $slug && CouponLanding::where('slug', $slug)->count()) {
            return back()->with([
                'error_message' => 'URL is already used.'
            ]);
        }

        $coupon_landing->fill($request->only('title','content','published', 'coupon_id', 'slug'));
        $coupon_landing->save();

        return back()->with([
            'success_message' => 'Coupon landing page has been updated.'
        ]);
    }

    public function destroy($id)
    {
        $coupon_landing = CouponLanding::findOrFail($id);
        $coupon_landing->delete();
        return back()->with([
            'success_message' => 'Coupon landing page has been deleted.'
        ]);
    }

    public function getCoupon(Request $request)
    {
        $this->data['has_coupon'] = $request->session()->has('coupon_code');
        $this->data['coupon_code'] = $request->session()->get('coupon_code');
        return response()->json($this->data);
    }

    public function preview($uuid)
    {
        return $this->view($uuid, true);
    }

    public function view($uuid, $preview = false)
    {
        $this->data['uuid'] = $uuid;
        $this->data['preview'] = $preview;
        $today = Carbon::now();

        $landing_page = CouponLanding::where('slug', $uuid)->first();

        if (!$landing_page || (!$landing_page->published && !$preview)) abort(404);

        $this->data['landing_page'] = $landing_page;
        $this->data['coupon'] = $coupon = $landing_page->coupon;
        $this->data['code'] = $landing_page->coupon->coupon_code;
        $this->data['discountableIds'] = $discountableIds = $this->data['coupon']
            ->couponCourses
            ->pluck('discountable_id');

        $this->data['coupon_course_type'] = $coupon_course_type = $coupon->couponCourses->first()->discountable_type;

        $seminars = CourseClass::whereHas('course', function ($query) use ($today) {
            $query->where('course_type_id', Constant::SEMINAR)
                  ->where('start_date', '>=', $today);
        });

        $this->data['seminars'] = ($coupon_course_type == 'Course') ?
                            $seminars->get() :
                            $seminars->whereIn('id', $discountableIds->toArray())->get();


        $webcasts = CourseClass::whereHas('course', function ($query) use ($today) {
            $query->where('course_type_id', Constant::WEBCAST)
                  ->where('start_date', '>=', $today);
        });

        $this->data['webcasts'] = ($coupon_course_type == 'Course') ?
            $webcasts->get() :
            $webcasts->whereIn('id', $discountableIds->toArray())->get();

        $this->data['cbts'] = Course::where('course_type_id', Constant::COMPUTER_BASED)
            ->whereIn('id', $discountableIds->toArray())
            ->get();

        $webcasts_count = 0;
        $seminars_count = 0;
        $cbts_count = 0;
        if ($coupon_course_type == 'Course') {
            foreach ($this->data['seminars']->groupBy('course_id') as $key => $seminar_group) {
                if ($discountableIds->contains(function ($v, $k) use ($key) {
                    return $k == $key;
                })
                ) {
                    $seminars_count++;
                }
            }
            foreach ($this->data['webcasts']->groupBy('course_id') as $key => $seminar_group) {
                if ($discountableIds->contains(function ($v, $k) use ($key) {
                        return $k == $key;
                    })
                ) {
                    $webcasts_count++;
                }
            }
            foreach ($this->data['cbts'] as $key => $cbt_group) {
                if ($discountableIds->contains(function ($v, $k) use ($key, $cbt_group) {
                    return $k == $cbt_group->id;
                })
                ) {
                    $cbts_count++;
                }
            }

        } else {
            $seminars_count = $seminars->count();
            $webcasts_count = $webcasts->count();
            $cbts_count = 0;
        }
        $this->data['webcasts_count'] = $webcasts_count;
        $this->data['seminars_count'] = $seminars_count;
        $this->data['cbts_count'] = $cbts_count;

        return view('main.coupon_landing.index', $this->data);
    }

    public function redirect(Request $request, $uuid, $to = 'get.login', $class_id = null)
    {

        $this->data['uuid'] = $uuid;
        $landing_page = CouponLanding::where('slug', $uuid)->first();

        if(!$landing_page) abort(404);

        $this->data['code'] = $code = $landing_page->coupon->coupon_code;

        $request->session()->put('coupon_code', $code);

        if($class_id) {
            return redirect()->route($to, [$class_id]);
        }
        return redirect()->route($to);

    }

}