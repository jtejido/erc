<?php namespace App\Http\Controllers\Main;

use App\Http\Requests\CreateCardRequest;
use App\Models\Order;
use App\Models\StateTax;
use Auth;
use Session;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassSubscriptionDiscountRepository;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\ClassMilitaryDiscountRepository;
use App\Repositories\CouponDiscountRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Services\PaymentService;
use App\Services\InvoiceService;
use App\Facades\PhotoUtility;
use App\Utilities\Constant;
use Illuminate\Http\Response;
use App\Repositories\StateRepository;

class CheckoutController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepository
     * @param InvoiceService $invoiceService
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepository,
                                InvoiceService $invoiceService)
    {
        $this->userRepo          = $userRepo;
        $this->orderRepo         = $orderRepo;
        $this->orderItemRepo     = $orderItemRepository;
        $this->invoiceService    = $invoiceService;
    }

    /**
     * Get checkout form
     *
     * @param Request $request
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassMilitaryDiscountRepository $militaryDiscountRepo
     * @param ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function checkout(Request $request,
                             ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                             ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                             CouponDiscountRepository $couponDiscountRepo,
                             OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                             ClassMilitaryDiscountRepository $militaryDiscountRepo,
                             ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo,
                             ClassCreditDiscountRepository $classCreditDiscountRepo,
                              StateRepository $stateRepo)
    {
        try {

            $userId = $request->route('user_id');

            $user = isset($userId)
                ? $this->userRepo->getById($userId)
                : Auth::user();

            $this->data['page']  = 'checkout';

            $order = $this->orderRepo->getByUserId($user->id);

            $this->data['user']  = $user;
            $this->data['userDetails'] = $user->contact;
            $this->data['states']   = $stateRepo->getByList('state', 'state')->toArray();
            $this->data['counties']   = StateTax::lists('county', 'id');
            $this->data['counties']->prepend('Please choose county:', '');
            $this->data['order'] = $order;
            $this->data['shipping_rates'] = isset($order->shipping_rates)?$order->shipping_rates:0.00;
            $s_address = isset($order->shipping_adress)?$order->shipping_adress:'';
            $s_zip = isset($order->shipping_zip)?$order->shipping_zip:'';
            $this->data['shipping_to'] = $s_address.' - '.$s_zip;
            $this->data['items'] = $order ? $order->items : null;

            $this->data['total'] = $order ? $order->current_total : 0;

            if (!Auth::user()->isMember() && ($order && $order->csr && $order->csr->count() === 0)) {
                // Transaction is now being handled by a CSR/Admin
                $order->csr()->associate(Auth::user()->id);
                $order->save();
            }

            $this->data['subscriptionDiscounts'] = $order
                                                    ? $classSubscriptionDiscountRepo->getByOrderId($this->data['order']->id)
                                                    : null;

            $this->data['combinationDiscounts'] = $order
                                                    ? $classCombinationDiscountRepo
                                                        ->getByOrderId($order->id)
                                                    : null;

            $this->data['halfPriceDiscounts'] = $order
                                                ? $classHalfPriceDiscountRepo
                                                    ->getByOrderId($order->id)
                                                : null;

            $this->data['couponDiscounts'] = $order
                                                ? ($couponDiscountRepo
                                                    ->getByOrderId($order->id)
                                                    ? $couponDiscountRepo
                                                        ->getByOrderId($order->id)
                                                        ->classDiscounts
                                                    : null)
                                                : null;

            $this->data['creditDiscounts'] = $order
                                                ? $classCreditDiscountRepo->getByOrderId($order->id)
                                                : null;

            $this->data['orderAdjustmentDiscounts'] = $order
                                                        ? $orderAdjustmentDiscountRepo
                                                            ->getByOrderId($order->id)
                                                        : null;

            $this->data['militaryDiscounts']  = $order
                                                ? $militaryDiscountRepo->getByOrderId($order->id)
                                                : null;


            $currentYear = date('Y');
            $length = $currentYear + 29;
            $this->data['years'] = [];

            for ($ctr = 1; $ctr <= 12; $ctr++) {
                $months[$ctr] = date('F', mktime(0,0,0,$ctr, 1, date('Y')));
            }

            $this->data['months'] = $months;

            for ($currentYear; $length >= $currentYear; $currentYear++) {
                $this->data['years'][$currentYear] = $currentYear;
            }

            $this->data['ccOnly'] = ($order and $order->items)
                ? ($this->orderItemRepo->checkFromOrderItemsIfThereIsItemWithType(
                    $order->items,
                    Constant::COMPUTER_BASED))
                    ? true
                    : false
                : false;

            $view = isset($userId)
                    ? view('admin.users.checkout', $this->data)
                    : view('main.users.checkout', $this->data);
            // return $this->data['userDetails'];
            return $view;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create payment via card
     *
     * @param Request $request
     * @param PaymentService $paymentService
     * @return string
     * @throws \Exception
     */
    public function createCardPayment(CreateCardRequest $request,
                                      PaymentService $paymentService)
    {
        try {

            $data = $request->all();
            $redirect = null;
            $userId = $request->route('user_id');
            $year   = $request->get('exp_year');
            $month  = $request->get('exp_month');

            $data['card_exp'] = $year . '-' . $month;

            $data = $this->extractBillingAndShippingAddressFromRequest($request, $data);

            $transaction = $paymentService->doPayment($data);

            if ($transaction) {

                $redirect = isset($userId)
                            ? redirect()->route('admin.checkout.success', [
                                'user_id' => $userId
                            ])
                            : redirect()->route('checkout.success');


                $request->session()->put('checkout_order_id', $data['order_id']);

            }

            if (Session::has('error_message')) {
                $redirect = redirect()->back()->with('error_message', Session::get('error_message'));
            }

            return $redirect;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Create payment via invoice
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function createInvoicePayment(Request $request)
    {
        try {

            $data = $request->all();

            $userId = $request->route('user_id');
            $data['recipients'] = strlen(trim($request->get('other_recipients')))
                                        ? array_map('trim',
                                                explode(',',
                                                $request->get('other_recipients')
                                               ))
                                        : null;

            $data = $this->extractBillingAndShippingAddressFromRequest($request, $data);

            $result = $this->invoiceService->save($data);

            if ($result) {

                $redirect = isset($userId)
                            ? ($request->get('invoice_payment')
                               ? redirect()->back()->with('success_message', 'Saved Payment Details.')
                               : redirect()->route('admin.checkout.success', [
                                'user_id'   => $userId]))
                            : ($request->get('invoice_payment')
                                ? redirect()->back()->with('success_message', 'Saved Payment Details.')
                                : redirect()->route('checkout.success'));

                $request->session()->put('checkout_order_id', $data['order_id']);

                return $redirect;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Show success page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function success(Request $request)
    {
        try {

            $userId = $request->route('user_id');
            $this->data['page'] = 'checkout_success';
            $this->data['user'] = isset($userId)
                                  ? $this->userRepo->getById($userId)
                                  : Auth::user();

            if (Session::has('PAYMENT_SUCCESS') && Session::get('PAYMENT_SUCCESS')) {
                if (Session::has('checkout_order_id')) {
                    $order = Order::findOrFail(Session::get('checkout_order_id'));
                    $this->data['transaction_id'] = $order->id;
                    $this->data['value'] = $order->paid_total;
                    $this->data['tax'] = is_null($order->order_tax) ? 0 : $order->order_tax;
                    $this->data['shipping'] = is_null($order->shipping_rates) ? 0 : $order->shipping_rates;
                    $this->data['items'] = [];

                    foreach ($order->items as $item) {
                        switch ($item->orderable_type) {
                            case Constant::REGISTRATION_ORDER:
                                $registrationOrder = $item->orderable;
                                $id = $registrationOrder->course()->sku;
                                $name = $registrationOrder->title();
                                $quantity = $registrationOrder->classRegistrations->count();
                                $category = 'Course';
                                break;

                            case Constant::PRODUCT_ORDERS:
                                $productOrder = $item->orderable;
                                $quantity = $productOrder->paid_quantity;
                                $id = $productOrder->product->code;
                                $category = 'Books';
                                $name = $productOrder->product->name;
                                break;

                            case Constant::YEARLY_SUBSCRIPTION:
                                $category = 'Annual Subscription';
                                $yearlySubscription = $item->orderable;
                                $quantity = $yearlySubscription->clientSubscriptions->count();
                                $id = 'yearly_sub';
                                $name = 'Annual Subscription';
                                break;

                            case Constant::CORPORATE_SEAT:
                                $category = 'Corporate Seat Discount';
                                $corporateSeat = $item->orderable;
                                $quantity = $corporateSeat->credits;
                                $id = 'corp_seat';
                                $name = 'Corporate Seat Discount';
                                break;
                        }

                        $item = (object) [
                            'id' => $id,
                            'name' => $name,
                            'category' => $category,
                            'quantity' => $quantity,
                            'price' =>  $item->price_charged
                        ];

                        array_push($this->data['items'], $item);
                    }

                    $request->session()->put('purchase', true);
                }

                $this->data['userId'] = $userId;
                $view =  isset($userId)
                        ? view('admin.users.checkout_success', $this->data)
                        : view('main.checkout.checkout_success', $this->data);

                Session::forget('checkout_order_id');

                return $view;
            }

            $redirect = isset($userId)
                        ? redirect()->route('admin.user_details', [
                            'user_id' => $userId,
                            'contact_id' => $this->data['user']->contact->id
                           ])
                        : redirect()->route('user.home');

            Session::forget('checkout_order_id');

            return $redirect;

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param $data
     * @return mixed
     */
    protected function extractBillingAndShippingAddressFromRequest(Request $request, $data)
    {
        if ($request->input('billing_custom', 0) == "0") {
            $data['billing_company'] = $request->input('billing_company_default');
            $data['billing_address1'] = $request->input('billing_address_default');
            $data['billing_city'] = $request->input('billing_city_default');
            $data['billing_state'] = $request->input('billing_state_default');
            $data['billing_zip'] = $request->input('billing_zip_default');
        }

        if ($request->input('shipping_custom', 0) == "0") {
            $data['shipment_company'] = $request->input('shipment_company_default');
            $data['shipment_address1'] = $request->input('shipment_address_default');
            $data['shipment_city'] = $request->input('shipment_city_default');
            $data['shipment_state'] = $request->input('shipment_state_default');
            $data['shipment_zip'] = $request->input('shipment_zip_default');
            $data['shipment_county'] = $request->input('shipment_county_default');
        }
        return $data;
    }
}