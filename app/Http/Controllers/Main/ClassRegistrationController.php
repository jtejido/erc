<?php

namespace App\Http\Controllers\Main;

use App\Facades\CertificateService;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClassRegistrationController extends Controller
{


    /**
     * ClassRegistrationController constructor.
     */
    public function __construct()
    {
    }

    public function update(Request $request)
    {
        try {
            CertificateService::updateStatus($request);
        } catch (\Exception $e) {
            return back()->with(['error_message' => 'Something went wrong, please try again.']);
        }
        return back()->with(['success_message' => trans('system_message.cert-success-generate')]);
    }

    public function updateAll(Request $request)
    {
        try {
            CertificateService::updateStatusAll($request);
        } catch (\Exception $e) {
            return back()->with(['error_message' => 'Something went wrong, please try again.']);
        }
        return back()->with(['success_message' => trans('system_message.cert-success-generate')]);
    }

}
