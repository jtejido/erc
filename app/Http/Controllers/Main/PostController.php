<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Models\ContactMessage;
use App\Models\Post;
use App\Models\PostCategory;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use App\Models\Regulation;
use App\Models\Tip;
use App\Http\Requests\CustomTrainingRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mailer;
use App\Mail\Message;

class PostController extends BaseController
{

    public function show(Post $post)
    {
        $this->data['page'] = 'posts';
        $this->data['post'] = $post;

        $this->data['articles'] = $this->getArticles();

        return view('main.posts.show', $this->data);
    }

    public function showWithCategory(PostCategory $category, Post $post)
    {
        $this->data['page'] = 'posts';
        $this->data['post'] = $post;

        $this->data['articles'] = $this->getArticles();

        return view('main.posts.show', $this->data);
    }

    public function sendEmailCustom(CustomTrainingRequest $request)
    {
        $inputs = $request->only('name',
                                'company',
                                'email',
                                'phone_number',
                                'training_requested',
                                'number_students',
                                'for_call',
                                'comments');
        $inputs['subject'] = 'Request For Training';
        $inputs['for_call'] = ($request->has('for_call') AND $request->get('for_call') == 'Yes')?'Yes':'No';
        Mail::later(2,'emails.templates.custom-training', compact('inputs'), function ($m) use ($inputs){
            $to = (env('APP_ENV') == 'local')?'flor@cspreston.com':'service@ercweb.com';
            $to_name = (env('APP_ENV') == 'local')?'Flor':'ERC Service';
            $cc = (env('APP_ENV') == 'local')?'mar.tuico@gmail.com':'aknight@environmentalresourcecenter.com';
            $cc_name = (env('APP_ENV') == 'local')?'mar.dev':'Environmental Resource Center';
            $m->from($inputs['email'], $inputs['name']);
            $m->to($to, $to_name);
            $m->cc($cc, $cc_name)
              ->subject('Request For Training');
        });

        if(Mail::failures()){
            return back()
                    ->withErrors(['message' => 'Unable to request for training']);
        }

        return back()
                ->with(['successMessage' => 'Successfully requested for training']);
    }

    /**
     * @param Request $request
     * @param EmailTemplateMailer $mailer
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(Request $request, EmailTemplateMailer $mailer)
    {
        try {

            $contactMessage = new ContactMessage();
            $contactMessage->fill($request->all());
            $contactMessage->save();

            $data = [
                'to' => env('CATCH_ALL_EMAIL'),
                'subject' => 'Message from ' . $contactMessage->name,
                'data' => [
                    'content' => $contactMessage->message,
                    'name' => $contactMessage->name,
                    'email' => $contactMessage->email,
                    'cbtsPage' => route('courses') . '?type=3',
                    'coursesPage' => route('courses'),
                    'homePage' => route('home'),
                    'fbUrl' => route('post.show', 4),
                ]
            ];

            $mailer->send('contact-request', $data);

        } catch (\Exception $e) {
            throw $e;
            return redirect()->back()->with('error_message', $e->getMessage());
        }

        return redirect()->back()->with('success_message', 'Message sent.');
    }

}
