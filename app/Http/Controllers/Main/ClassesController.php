<?php namespace App\Http\Controllers\Main;

use App\Models\Course;
use App\Models\GeneralLocation;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use App\Repositories\CourseTypeRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\CourseClass;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ClassesController extends MainController
{
    /**
     * @var CourseTypeRepository
     */
    private $courseTypeRepo;
    /**
     * @var CategoryRepository
     */
    private $categoryRepo;
    /**
     * @var CourseRepository
     */
    private $courseRepo;


    /**
     * ClassesController constructor.
     */
    public function __construct(CourseTypeRepository $courseTypeRepo,
                                CategoryRepository $categoryRepo,
                                CourseRepository $courseRepo)
    {
        $this->data['page'] = 'classes';
        $this->courseTypeRepo = $courseTypeRepo;
        $this->categoryRepo = $categoryRepo;
        $this->courseRepo = $courseRepo;
    }

    public function register()
    {
        return view('main.classes.register', $this->data);
    }

    public function showRelated($class_id)
    {
        $this->data['class'] = $class = CourseClass::findOrFail($class_id);
        if ($class->classes->count() == 0) {
            return redirect()->route('contacts.register', [
                'course_id' => $class->course->id,
                'class_id'  => $class->id]);
        }

        $this->data['course'] = $class->course;

        return view('main.classes.show', $this->data);

    }

    public function index(Request $request)
    {

        $slug = $request->route('slug');

        $this->data['articles'] = $this->getArticles();

        try {
            if($slug) {
                $gl = GeneralLocation::where('slug', '=', $slug)->firstOrFail();
                $this->data['location_name'] = $gl->location;
                $this->data['location'] = $gl->id;
                $this->data['slug'] = $gl->slug;
            } else {
                $gl = GeneralLocation::findOrFail($request->input('location_id', 1));
                $this->data['location_name'] = $gl->location;
                $this->data['location'] = $gl->id;
                $this->data['slug'] = $gl->slug;
            }
        } catch (ModelNotFoundException $e) {
            throw $e;
        }


        $this->data['locations'] = GeneralLocation::orderBy('location')->lists('location','slug');

         $query = CourseClass::
                    join('courses', 'classes.course_id', '=', 'courses.id')
                    ->join('locations', 'classes.location_id', '=', 'locations.id')
                    ->where('locations.general_location_id', '=', $this->data['location'])
                    ->where('classes.start_date', '>=', Carbon::today()->toDateString())
                    ->orderBy('classes.start_date');

        $classes = $query->get();
        $this->data['active'] = $classes;
        $this->data['now']    = Carbon::now();

        if ($this->data['active']) {
            return view('main.classes.index', $this->data);
        }


    }

}
