<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Models\Category;
use App\Models\Regulation;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;
use App\Models\Tip;

class RegulationController extends BaseController
{

    public function index()
    {

        $this->data['page'] = 'regulations';
        $this->data['regulations'] = Regulation::published()->get();

        return view('main.regulations.index', $this->data);
    }

    public function show($id)
    {

        $reg = Regulation::find($id);

        if($reg) {
            return redirect()->route('main.regulations.edit', [ 'slug' => $reg->slug ]);
        }

        $reg = Regulation::where('slug', $id)->first();

        if(!$reg) {
            return abort(404);
        }

        $this->data['page'] = 'regulations';
        $this->data['categories'] =  Category::all();
        $this->data['reg'] =  $reg;

        $this->data['articles'] = $this->getArticles();

        return view('main.regulations.show', $this->data);
    }
}
