<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClassRepository;
use App\Repositories\ContactRepository;
use App\Repositories\CourseRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\UserRepository;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use App\Models\Course;
use App\Models\CourseClass;
use App\Facades\CertificateService;
use Carbon\Carbon;
use App\Services\ClassService;
use App\Models\RegistrationOrder;

class CertificateController extends Controller
{

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /*
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * CertificateController constructor.
     * @param UserRepository $userRepo
     * @param ClassRepository $classRepo
     */
    public function __construct(UserRepository $userRepo,
                                ClassRepository $classRepo,
                                CourseRepository $courseRepo,
                                ContactRepository $contactRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ClassService $classService)
    {
        $this->userRepo = $userRepo;
        $this->classRepo = $classRepo;
        $this->courseRepo = $courseRepo;
        $this->contactRepo = $contactRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->classService = $classService;
    }

    public function api(Request $request)
    {

        $today = Carbon::today();
        $orderby = $request->input('order.0.column');
        $sort['col'] = $request->input('columns.' . $orderby . '.data');
        $sort['dir'] = $request->input('order.0.dir');

        $query = CourseClass::leftJoin('courses as course','course.id','=','classes.course_id')
                            ->select('classes.*','course.sku','course.title','course.course_type_id','course.price','course.description')
                            ->with('course','registrations.countClassRegistrations','registrations.countClassRegistrationsTBC')
                            ->whereRaw('(course.sku like ? OR course.title like  ?)', ['%'. $request->input('search.value') .'%','%'. $request->input('search.value') .'%']);

        if($request->input('showOnlyUpcoming', 0)) {
            $query->whereDate('classes.start_date', '>=', $today->toDateString());
        }

        if($sort['col'] == 'TotalParticipants'){
            $ids_arr = RegistrationOrder::where('registrable_type','=','CourseClass')->get()->pluck('registrable_id');
            $query->whereIn('classes.id',$ids_arr);
        }

        if($sort['col'] == 'dateLabel'){
           $sort['col'] = 'start_date';
        }
        if($sort['col'] == 'addressType'){
           $sort['col'] = 'course_type_id';
        }

        $col = ['dateLabel','ToBeConfirmed','course_type_id','TotalParticipants',''];
        if(!in_array($sort['col'],$col)){
            $query->orderBy($sort['col'], $sort['dir']);
        }

        $output['recordsTotal'] = $query->count();
        $output['draw'] = intval($request->input('draw'));
        $output['recordsFiltered'] = $output['recordsTotal'];
        $output['data'] = $query
                ->skip($request->input('start',0))
                ->take($request->input('length',10))
                ->get();

        $output['data']->each(function($item, $key) {
            $item->action = '';
            $item->dateLabel = $item->dates.'<br>'.$item->getClassDateLabel();
            $regidsArr = $item->registrations->pluck('id');
            $item->ToBeConfirmed = $this->classService->getNumberOfParticipantsPerClassTBC($item->id);
            $item->TotalParticipants = $this->classRegistrationRepo->countActiveRegistrationsByIds($regidsArr);
            $item->addressType = ($item->course->type == 'Seminar')?$item->locationName:$item->course->type;

            if(!is_null($item->TotalParticipants) AND $item->TotalParticipants > 0){
                $item->action = '<a href="'.route('admin.certificates.show', [$item->id]).'"
                                class="btn btn-flat btn-primary btn-flat btn-block">View</a>';
            }
            if(is_null($item->TotalParticipants)){
                $item->TotalParticipants = '0';
            }
            if(is_null($item->ToBeConfirmed)){
                $item->ToBeConfirmed = '0';
            }
            return $item;
        });

        return response()->json($output);
    }

    public function index()
    {
        $this->data['page'] = 'certificates';
        $this->data['now'] = date('Y-m-d');
        return view('admin.certificates.index', $this->data);
    }

    /**
     * Show list of students given a class
     * @param $class_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showClass($class_id)
    {
        $this->data['page'] = 'certificates_show';
        $this->data['class'] = $class = $this->classRepo->getById($class_id);
        $this->data['course'] = $class->course;
        $this->data['students'] = $this->classRegistrationRepo->getStudentsOfClass($class, false);
        return view('admin.certificates.show', $this->data);
    }

    /**
     * @param $course_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showCourse($course_id)
    {
        $this->data['page'] = 'certificates_show';
        $this->data['course'] = $class = $this->courseRepo->getById($course_id);
        $this->data['class'] = null;
        $this->data['students'] = $this->classRegistrationRepo->getStudentsOfClass($class, false);
        return view('admin.certificates.show', $this->data);
    }


    /**
     * Generate preview of the certificate
     * @param  $user_id
     * @param  $regOrderId
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function previewCertificate($user_id, $regOrderId)
    {
        $tmpFile = 'Certificate Preview.pdf';
        $pdf = CertificateService::generateCertificate($user_id, $regOrderId);
        $pdf->save($tmpFile, true);
        session()->flash('download.in.the.next.request', url($tmpFile));
        return view('filedl');
    }

    /**
     * Save Certificate to PDF and DB
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function saveCertificate(Request $request)
    {
        CertificateService::saveCertToDBandStorage($request);
        return back()->with(['success_message' => trans('system_message.cert-success-generate')]);
    }

    public function regenerateCertificate($user_id, $regOrderId)
    {
        CertificateService::saveCertToDBandStorage($user_id, $regOrderId);
        return back()->with(['success_message' => trans('system_message.cert-success-regenerate')]);
    }


    /**
     * Download Certificate PDF file
     * @param  $user_id
     * @param  $regOrderId
     * @return \Illuminate\Http\Response
     */
    public function downloadCertificate($user_id, $regOrderId)
    {
        $reg_order = $this->classRegistrationRepo
                        ->getByRegistrationAndAttendee($regOrderId, $user_id);
        if(!$reg_order) abort(404);
        if(!$reg_order->certificate_file_path) {
            try {
                $path = CertificateService::saveCertToDBandStorage($user_id, $regOrderId);
            } catch (\Exception $e) {
                return back()->with(['error_message' => 'Something went wrong, please try again.']);
            }
        } else {
            $path = $reg_order->certificate_file_path;
        }
        $file = Storage::get($path);
        return (new Response($file, 200))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="Certificate.pdf"');
    }

    /**
     * @param $class_id
     * @return Response
     */
    public function downloadCertificates($class_id)
    {
        $pdf = CertificateService::generateCertificates($class_id);
        return (new Response($pdf, 200))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="Certificates.pdf"');
    }

    /**
     * @param $course_id
     * @return Response
     */
    public function downloadCertificatesOfCourse($course_id)
    {
        $class = $this->courseRepo->getById($course_id);
        $students = $this->classRegistrationRepo->getStudentsOfClass($class, false);
        $pdf = CertificateService::mergeCertificates($students);
        return (new Response($pdf, 200))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="Certificates.pdf"');
    }

    /**
     * Download certificates -- FRONTEND
     * @param $regOrderId
     * @return Response
     */
    public function downloadCertificatesPerRegistration($regOrderId)
    {
        $regOrder = $this->registrationOrderRepo->getById($regOrderId);
        $students = $this->classRegistrationRepo->getStudentsOfRegistrationOrder($regOrder, false);
        $pdf = CertificateService::mergeCertificates($students, true);
        return (new Response($pdf, 200))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="Certificates.pdf"');
    }

    /**
     * Generate Certificate for Multiple Contacts
     * @param Request $request
     * @return mixed
     */
    public function generateByBatchCertificate(Request $request)
    {
        $class_id = $request->input('class_id');
        $course_id = $request->input('course_id');
        $contacts = $request->input('contacts');
        $status = $request->input('status');
        foreach ($contacts as $contact) {
            CertificateService::saveCertToDBandStorage($contact[0], $contact[1], intval($status));
        }
        $this->data['route'] = ($class_id) ?
                                route('admin.certificates.show', ['class_id' => $class_id]) :
                                route('admin.certificates.showCourse', ['class_id' => $course_id]);
        $request->session()->flash('success_message', trans('system_message.cert-success-generateByBatch'));
        return $this->data;
    }

    /**
     * @param $class_id
     * @return mixed
     */
    public function downloadRoster($class_id)
    {
        $pdf = CertificateService::generateRoster($class_id);
        $tmpFile = 'Class Roster.pdf';
        return $pdf->download($tmpFile);
    }

    /**
     * @param $course_id
     * @return mixed
     */
    public function downloadCourseRoster($course_id)
    {
        $pdf = CertificateService::generateRoster($course_id, 1);
        $tmpFile = 'Class Roster.pdf';
        return $pdf->download($tmpFile);
    }

    /**
     * @param $class_id
     * @return mixed
     */
    public function downloadSignSheet($class_id)
    {
        $pdf = CertificateService::generateSignSheet($class_id);
        $tmpFile = 'Sign-in Sheet.pdf';
        return $pdf->download($tmpFile);
    }

    /**
     * @param $class_id
     * @return mixed
     */
    public function downloadNamePlate($class_id)
    {
        $pdf = CertificateService::generateNamePlate($class_id);
        $tmpFile = 'Name Plates.pdf';
        return $pdf->download($tmpFile);
    }

    /**
     * @param $class_id
     * @return mixed
     */
    public function downloadAll($class_id)
    {
        try {
            $zipper = new \Chumper\Zipper\Zipper;
            $zipper = $zipper->make('class_files.zip');
            $pdf = CertificateService::generateRoster($class_id);
            $tmpFile1 = 'Class Roster.pdf';
            $pdf->save($tmpFile1, true);
            $pdf = CertificateService::generateSignSheet($class_id);
            $tmpFile2 = 'Sign-in Sheet.pdf';
            $pdf->save($tmpFile2, true);
            $pdf = CertificateService::generateNamePlate($class_id);
            $tmpFile3 = 'Name Plates.pdf';
            $pdf->save($tmpFile3, true);
            $pdf = CertificateService::generateCertificates($class_id);
            $tmpFile4 = 'Certificates.pdf';
            file_put_contents($tmpFile4, $pdf);
            $zipper->add([
                $tmpFile1,
                $tmpFile2,
                $tmpFile3,
                $tmpFile4,
            ])->close();;
        } catch (\Exception $e) {
            return back()->with(['error_message' => 'Something went wrong, please try again.']);
        }

        return response()->download('class_files.zip');
    }

}
