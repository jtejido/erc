<?php

namespace App\Http\Controllers\Main;

use App\Facades\CertificateService;
use App\Facades\RedirectService;
use App\Http\Requests\RedirectRequest;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RedirectController extends BaseController
{

    /**
     * RedirectController constructor.
     */
    public function __construct()
    {
        $this->data = [];
        $this->data['page'] = 'redirect';
    }

    public function meeting($slug)
    {
        $this->data = RedirectService::getWebinarData($slug);
        return view('main.redirect.meeting', $this->data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMeeting(Request $request)
    {
        RedirectService::updateMeeting($request);
        return response()->json(['updated' => true]);
    }

    public function updateExam(Request $request)
    {
        $data = RedirectService::updateExam($request);
        if($data['flag'] != 1)
            return response()->json(['updated' => false, 'message' => 'Selected user did not yet watch the webcast. Please contact a customer support to proceed.']);
        return response()->json(['updated' => true, 'link' => $data['url']]);
    }

    public function exam($slug)
    {

        $this->data = RedirectService::getExamData($slug);
        return view('main.redirect.exam', $this->data);
    }

    public function hook(Request $request)
    {
        RedirectService::hook($request);
        return response('200', 200);
    }



}
