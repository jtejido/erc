<?php namespace App\Http\Controllers\Main;

use Auth;
use Crypt;
use App\Repositories\ContactRepository;
use App\Repositories\StateRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Events\ProfileUpdateEvent;
use App\Http\Requests\EditEmailRequest;
use App\Http\Requests\EditPasswordRequest;
use App\Http\Requests\EditPersonalInfoRequest;
use App\Http\Requests\EditSameAsBillingOrShippingRequest;
use App\Services\UserService;
use App\Facades\CourseMillService;

class ProfileController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var userService
     */
    private $userService;

    /**
     * @param UserRepository $userRepo
     * @param ContactRepository $contactRepo
     */
    public function __construct(UserRepository $userRepo,
                                ContactRepository $contactRepo,
                                UserService $userService)
    {
        $this->userRepo = $userRepo;
        $this->contactRepo = $contactRepo;
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @param StateRepository $stateRepo
     * @param ContactRepository $contactRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function editProfile(Request $request,
                                StateRepository $stateRepo,
                                ContactRepository $contactRepo)
    {
        try {

            $contactId = $request->route('contact_id');
            $this->data['page'] = 'edit_profile';

            $userDetails = $contactRepo->getById($contactId);
            $this->data['user'] = isset($contactId)
                                  ? ($userDetails->user
                                    ? $userDetails->user
                                    : null)
                                  : Auth::user();

            $this->data['userDetails'] = isset($contactId)
                                         ? $userDetails
                                         : $this->data['user']->contact;

            $this->data['states']   = $stateRepo->getByList('state', 'state')->toArray();

            $view   = isset($contactId)
                      ? view('admin.users.edit_profile', $this->data)
                      : view('main.users.edit_profile', $this->data);

            return $view;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditEmailRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function saveEditEmail(EditEmailRequest $request)
    {
        try {

            $userId = $request->get('user_id');
            $email  = $request->get('email');

            $user = $this->userRepo->getById($userId);
            $email_old = $user->email;

            $user->email = $email;
            $user->save();

            $is_password = false;

            event(new ProfileUpdateEvent($user->id, $is_password, $email_old));

            if (!is_null($user->contact->course_mill_user_id)) {
                CourseMillService::createUpdateStudentFromCustomer($user->contact);
            }

            return redirect()->back()
                             ->with([
                                 'edit_email' => true,
                                 'message'    => 'Email successfully edited.']
                             );

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditPasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function saveEditPassword(EditPasswordRequest $request)
    {
        try {
            $userId      = $request->get('user_id');
            $newPassword = $request->get('new_password');

            $user = $this->userRepo->getById($userId);

            $user->password = bcrypt($newPassword);
            $user->save();

            $is_password = true;
            event(new ProfileUpdateEvent($user->id, $is_password));

            return redirect()->back()
                             ->with([
                                 'edit_password' => true,
                                 'message'       => 'Password successfully edited.'
                             ]);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditPersonalInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function saveEditPersonalInfo(EditPersonalInfoRequest $request)
    {
        try {

            $contactId = $request->get('contact_id');
            $params = $request->all();

            $contact = $this->contactRepo->getById($contactId);
            $params['course_mill_user_id'] = isset($params['course_mill_user_id'])?$params['course_mill_user_id']:'';
            if (strlen(trim($params['course_mill_user_id'])) == 0) {
                $params['course_mill_user_id'] = null;
            }

            $contact->fill($params);
            $contact->save();

            return redirect()->back()
                             ->with([
                                 'edit_personal_info' => true,
                                 'message'            => 'Personal Information successfully edited.'
                             ]);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param EditSameAsBillingOrShippingRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function saveEditSameAsBillingOrShipping(EditSameAsBillingOrShippingRequest $request)
    {
        try {

            $params = $request->all();

            $contact = $this->contactRepo->getById($params['contact_id']);

            $contact->fill($params);
            $contact->save();

            $messageKey = isset($params['address_same_as_billing']) ? 'edit_billing_info' : 'edit_shipping_info';
            $address    = isset($params['address_same_as_billing']) ? 'Billing' : 'Shipping';

            return redirect()->back()
                             ->with([
                                $messageKey          => true,
                                'message'            => $address . ' address successfully edited.'
                             ]);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function saveEditSameAsAddress(Request $request)
    {
        try {
            $contact = $this->contactRepo->getById($request->input('contact_id'));
            $column = $request->input('column');

            $contact->{$column} = ($request->input('flag') == 'true') ? true : false;
            $contact->save();

            return $contact;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepo
     * @return mixed
     * @throws \Exception
     */
    public function saveEditOtherAttributes(Request $request,
                                            ContactRepository $contactRepo)
    {
        try {

            $userId = $request->route('user_id');
            $contactId = $request->route('contact_id');

            $object = isset($userId)
                        ? $this->userRepo->getById($userId)
                        : (isset($contactId)
                            ? $contactRepo->getById($contactId)
                            : Auth::user()->contact);

            $attrib = $request->get('attrib');
            $value  = $request->get('value');

            $object->{$attrib} = $value;
            $object->save();

            return $object;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Activate / Deactivate Account
     * @param  Request $request
     * @return mixed
     * @throws \Exception
     */
    public function toggleUserStatus(Request $request)
    {
        try {

            $userId = $request->get('user_id');
            $user = $this->userRepo->getById($userId);
            if (!$user) abort(400, 'User not found.');
            if($user->isActive()) {
                $result = $this->userService->deactivateUserById($userId);
            } else {
                $result = $this->userService->activateUserById($userId);
            }

            if (!is_null($user->contact->course_mill_user_id)) {
                CourseMillService::createUpdateStudentFromCustomer($user->contact);
            }

            $message = ($result) ? 'Account successfully updated.' : 'Account cannot be activated at the moment.';
            return redirect()->back()
                             ->with([
                                'toggle_account'     => true,
                                'message'            => $message
                             ]);

        } catch (\Exception $e){
            throw $e;
        }
    }

    /**
     * Send request email to admin to re-activate disabled account
     * @param  Request $request
     * @return mixed
     * @throws \Exception
     */
    public function requestReactivateAccount(Request $request)
    {
        $error   = '';
        $success = trans('auth.request-sent-success');
        try {
            $email = $request->get('email');
            $user = $this->userRepo->findOne(['email' => $email]);
            if (!$user) {
                $error = trans('auth.email-not-found');
            } else {
                $this->userService->requestToActivate($user);
            }
            return [
                'error'   => $error,
                'message' => $success
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
