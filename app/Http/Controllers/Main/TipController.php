<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Models\Category;
use App\Models\Tip;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;
use App\Models\Regulation;

class TipController extends BaseController
{

    public function index(Request $request)
    {

        $this->data['page'] = 'tips';
        $this->data['searchQ'] = $request->get('searchQ');

        $query = Tip::published();

        if (isset($this->data['searchQ']) and strlen(trim($this->data['searchQ'])) > 0) {
            $query = $query->where('title', 'LIKE', '%' . $this->data['searchQ'] . '%');
        }

        $this->data['tips'] = $query->paginate(50);

        return view('main.tips.index', $this->data);
    }

    public function show($id)
    {

        $tip = Tip::find($id);

        if($tip) {
            return redirect()->route('main.tips.edit', [ 'slug' => $tip->slug ]);
        }

        $tip = Tip::where('slug', $id)->first();

        if(!$tip) {
            return abort(404);
        }

        $this->data['page'] = 'tips';
        $this->data['categories'] =  Category::all();
        $this->data['tip'] =  $tip;

        $this->data['articles'] = $this->getArticles();

        return view('main.tips.show', $this->data);
    }

    /**
     * @return array
     */
    private function years()
    {
        $currentYear = date('Y');
        $oldestYear = 2001;
        $years = [];

        for ($ctr = $oldestYear; $ctr <= $currentYear; $ctr++) {
            $years[$ctr] = $ctr;
        }

        return $years;
    }

}
