<?php namespace App\Http\Controllers\Main;

use Session;
use File;
use Storage;
use App\Facades\PhotoUtility;
use App\Services\InvoiceService;
use App\Utilities\Constant;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Repositories\InvoicedPaymentRepository;
use App\Repositories\InstructorRepository;
use App\Models\Instructor;

class FileController extends BaseController
{

    /**
     * @param Request $request
     * @param InvoiceService $invoiceService
     * @return array
     * @throws \Exception
     */
    public function ajaxValidateInvoiceFile(Request $request,
                                     InvoiceService $invoiceService)
    {
        try {
            $file = $request->file('file');
            $type = $request->get('type');

            $error = PhotoUtility::validateMimeTypeAndSize($file);
            $response  = $invoiceService->uploadInvoice(Constant::FILE_TEMP, $file, $type);

            return [
                'error' => $error,
                'src'  => route('invoice_image_temp.preview', [
                    'file_name' => $response['image']
                ])
            ];

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return $this
     * @throws \Exception
     */
    public function invoiceTempPreview(Request $request)
    {
        try {
            $file = PhotoUtility::getFullFilePathFromStorage(Constant::FILE_TEMP, $request->route('file_name'));

            return (new Response($file, 200))->header('Content-Type', Session::get('mime_type'));

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param InvoicedPaymentRepository $invoicedPaymentRepo
     * @return $this
     * @throws \Exception
     */
    public function invoiceCompletedPreview(Request $request,
                                            InvoicedPaymentRepository $invoicedPaymentRepo)
    {
        try {
            $type = $request->route('type');
            $name = $request->route('file_name');
            $orderId = $request->route('order_id');
            $invoiceId = $request->route('invoice_id');

            $invoicedPayment = $invoicedPaymentRepo->getById($invoiceId);

            if ($type == Constant::PO_FILE or $type == Constant::CHECK_FILE) {
                $mime = 'application/pdf';
            }

            if ($type == Constant::PO_IMAGE) {
                $mime = $invoicedPayment->po_image_mime;
            }

            if ($type == Constant::CHECK_IMAGE) {
                $mime = $invoicedPayment->check_image_mime;
            }

            $file = PhotoUtility::getFullFilePathFromStorage(
                Constant::FILE_COMPLETE, $name, $orderId . '/' . $invoiceId
            );

            return (new Response($file, 200))->header('Content-Type', $mime);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function downloadInvoice(Request $request)
    {
        try {

            $path = '';
            $type = $request->get('type');
            $name = $request->get('file_name');
            $orderId = $request->get('order_id');
            $invoiceId = $request->get('invoice_id');

            $file = PhotoUtility::getFullFilePathFromStorage(
                Constant::FILE_COMPLETE, $name, $orderId . '/' . $invoiceId
            );

            if ($type == Constant::PO_IMAGE or $type == Constant::CHECK_IMAGE) {
                $path = $type == Constant::PO_IMAGE
                        ? 'po.png'
                        : 'check.png';
                $im = @imagecreatefromstring($file);
                @imagealphablending($im, false);
                @imagesavealpha($im, true);
                @imagepng($im, $path);
            }

            return response($path);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deleteDownloadedInvoice(Request $request)
    {
        File::delete($request->get('file'));
    }

    /**
     * @param Request $request
     * @param InstructorRepository $instructorRepo
     * @return $this
     * @throws \Exception
     */
    public function signaturePreview(Request $request,
                                     InstructorRepository $instructorRepo)
    {
        try {
            $name = $request->route('name');
            $instructor = $instructorRepo->findOne(['signature' => $name]);
            $file = PhotoUtility::getFilePathOfSignature($name);

            if ($instructor && $file) {
                return (new Response($file, 200))->header('Content-Type', $instructor->signature_mime);
            }

            return $file;

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
