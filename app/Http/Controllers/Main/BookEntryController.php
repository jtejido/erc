<?php

namespace App\Http\Controllers\Main;

use App\Http\Requests\FileRequest;
use App\Repositories\BookEntryRepository;
use App\Repositories\BookRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BookEntryController extends BaseController
{

    /**
     * @var BookEntryRepository
     */
    protected $bookEntryRepo;

    protected $bookRepo;

    /**
     * BookEntryController constructor.
     * @param BookEntryRepository $bookEntryRepo
     * @param BookRepository $bookRepo
     */
    public function __construct(BookEntryRepository $bookEntryRepo,
                                BookRepository $bookRepo)
    {
        $this->bookEntryRepo = $bookEntryRepo;
        $this->bookRepo = $bookRepo;
    }

    public function store(FileRequest $request)
    {
        $book = $this->bookRepo->find($request->get('book_id'));
        if (!$book || !$request->hasFile('file')) {
            abort(404);
        }
        $this->bookEntryRepo->addFile($request, $book);
        return redirect()->route('admin.books.show', [$book->id])
            ->with(['success_message' => 'File has been added.']);
    }

    /**
     * @param $filename
     * @return Response
     */
    public function get($filename)
    {
        $entry = $this->bookEntryRepo->findBy('filename', $filename);
        if(!$entry) {
            abort(404);
        }
        $file = $this->bookEntryRepo->getBookEntryFile($filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime)
            ->header('Content-Disposition', 'attachment; filename="'.$entry->original_filename.'"');
    }


    public function upload(Request $request, $id)
    {
        $book = $this->bookRepo->find($id);
        if (!$book || !$request->hasFile('file')) {
            abort(404);
        }
        $fields = [];
        $fields = $this->storeFile($request, $fields);
        $entry = $this->bookEntryRepo->addEntryToBook($book, $fields);
        $this->data['name'] = $fields['original_filename'];
        $this->data['size'] = 0;
        $this->data['deleteUrl'] = route('admin.bookentry.delete', [$entry->id]);
        $this->data['deleteType'] = 'DELETE';

        return response()->json($this->data, 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $entry = $this->bookEntryRepo->find($id);
        if (!$entry ) {
            abort(404);
        }
        $this->bookEntryRepo->deleteBookEntry($entry);
        return response()->json('success', 200);
    }

    /**
     * @param Request $request
     * @param $fields
     * @return mixed
     */
    public function storeFile(Request $request, $fields)
    {
        $file = $request->file('file');
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getFilename() . '.' . $extension;
        $path = 'books/files/' . $filename;
        Storage::disk('local')->put($path, File::get($file));
        $fields['filename'] = $filename;
        $fields['mime'] = $file->getClientMimeType();
        $fields['original_filename'] = $file->getClientOriginalName();
        return $fields;
    }

}
