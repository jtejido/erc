<?php namespace App\Http\Controllers\Main;

use App\Models\CourseClass;
use App\Repositories\ClassRegistrationRepository;
use Auth;
use Mail;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use App\Repositories\OrderItemRepository;
use App\Repositories\RoleRepository;
use App\Repositories\ClassRepository;
use App\Repositories\CourseRepository;
use App\Repositories\StateRepository;
use App\Repositories\ZipCodeRepository;
use App\Repositories\UserRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\ContactRepository;
use App\Repositories\AddressBookEntryRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassCombinationRepository;
use App\Services\ContactService;
use Illuminate\Support\Facades\Session;
use App\Utilities\Constant;

class ContactController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var RoleRepository
     */
    private $roleRepo;

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @var StateRepository
     */
    private $stateRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var ContactService
     */
    private $contactService;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var AddressBookEntryRepository
     */
    private $addressBookEntryRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @param UserRepository $userRepo
     * @param ContactRepository $contactRepo
     * @param CourseRepository $courseRepo
     * @param ClassRepository $classRepo
     * @param StateRepository $stateRepo
     * @param RoleRepository $roleRepo
     * @param ZipCodeRepository $zipCodeRepo
     * @param ContactService $contactService
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param AddressBookEntryRepository $addressBookEntryRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(UserRepository $userRepo,
                                ContactRepository $contactRepo,
                                CourseRepository $courseRepo,
                                ClassRepository $classRepo,
                                StateRepository $stateRepo,
                                RoleRepository $roleRepo,
                                ZipCodeRepository $zipCodeRepo,
                                ContactService $contactService,
                                RegistrationOrderRepository $registrationOrderRepo,
                                AddressBookEntryRepository $addressBookEntryRepo,
                                OrderItemRepository $orderItemRepo)
    {
        $this->userRepo              = $userRepo;
        $this->contactRepo           = $contactRepo;
        $this->courseRepo            = $courseRepo;
        $this->classRepo             = $classRepo;
        $this->stateRepo             = $stateRepo;
        $this->zipCodeRepo           = $zipCodeRepo;
        $this->roleRepo              = $roleRepo;
        $this->contactService        = $contactService;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->addressBookEntryRepo  = $addressBookEntryRepo;
        $this->orderItemRepo         = $orderItemRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('main.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(Request $request)
    {
        if ($request->get('contact_id')) {
            $this->data['contact'] = $this->contactRepo->getById($request->get('contact_id'));
        }

        if ($request->get('class_id')) {
            $this->data['class'] = $this->classRepo->getById($request->get('class_id'));
        }

        if ($request->get('course_id')) {
            $this->data['course'] = $this->courseRepo->getById($request->get('course_id'));
        }

        if ($request->get('user_id')) {
            $this->data['user'] = $this->userRepo->getById($request->get('user_id'));
        }

        if ($request->get('email')) {
            $this->data['email'] = true;
        }

        if ($request->get('customer')) {
            $this->data['customer'] = true;
        }

        if ($request->get('item')) {
            $this->data['item'] = $request->get('item');
        }

        $this->data['states'] = $this->stateRepo->getByList('state', 'state')->toArray();

        return view('main.contact.add', $this->data);
    }

    /**
     * Store a newly created resource in storage
     *
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(ContactFormRequest $request)
    {
        try {

            $data = $request->all();
            $result = $this->contactService->saveContact($data);

            if ($result['response']) {
                $contactId = $result['contact']->id;
                $message   = $result['message'];

                if (isset($data['id'])) {
                    $message   = 'Contact successfully modified.';
                    $contactId = $data['id'];
                }

                if (!isset($data['remember_contact_id'])){
                    Session::flash('contact_id', $contactId);
                }

            }

            else {
                $message = $result['message'];
            }

            return redirect()->back()
                ->with([
                    'message'      => $message,
                    'classMessage' => $result['response'] ? 'success' : 'danger'
                ]);

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Edit address book
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit(Request $request)
    {
        try {

            $userId = $request->route('user_id');

            $this->data['page'] = 'address_book';
            $this->data['user'] = isset($userId)
                                  ? $this->userRepo->getById($userId)
                                  : Auth::user();

            $this->data['contacts'] =  $this->contactRepo
                                            ->getByUserWithContact($this->data['user']->id);

            $view = isset($userId)
                    ? view('admin.users.address_book', $this->data)
                    : view('main.contact.address_book', $this->data);

            return $view;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function delete(Request $request)
    {
        $userId = $request->get('user_id');
        $contactId = $request->get('contact_id');
        $user = $this->userRepo->getById($userId);
        $contact = $this->contactRepo->getById($contactId);

        $response = $this->contactService->deleteContact($user, $contact);

        $request->session()->flash('success_message', 'Contact Deleted.');

        return $response;
    }

    /**
     * List of address book contacts
     *
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function contacts(Request $request,
                             ClassRegistrationRepository $classRegistrationRepo,
                             ClassCombinationRepository $classCombinationRepo,
                             ClassCombinationDiscountRepository $classCombinationDiscountRepo)
    {
        try {

            $register = $request->input('register');

            $this->data['page'] = 'contacts';

            $this->data['attendees'] = [];
            $this->data['noSwapYearlySubs'] = [];
            $this->data['noSwapGSAs'] = [];
            $this->data['noSwapCancelled'] = [];
            $this->data['pairedClassSwapNote'] = false;

            $userId = $request->route('user_id');
            $courseId = $request->route('course_id');
            $classId = $request->route('class_id');
            $orderItemId = $request->route('order_item_id');
            $notIn = [];

            $this->data['orderItemId'] = $orderItemId;
            $this->data['includeAll']  = isset($orderItemId);
            $orderItem   = isset($orderItemId)
                           ? $this->orderItemRepo->getById($orderItemId)
                           : null;

            $registrationOrder = isset($orderItem)
                                 ? $orderItem->orderable
                                 : null;

            $this->data['user'] = isset($userId)
                                  ? $this->userRepo->getById($userId)
                                  : Auth::user();

            $this->data['class'] = isset($classId)
                                   ? $this->classRepo->getById($classId)

                                   : (isset($orderItemId)
                                     ? (!$registrationOrder->isCourse()
                                       ? $registrationOrder->registrable
                                       : null)

                                     : null);

            $this->data['course'] = isset($courseId)
                                    ? $this->courseRepo->getById($courseId)

                                    : ($registrationOrder->isCourse()
                                      ? $registrationOrder->registrable
                                      : $registrationOrder->registrable->course);

            $registrableId = is_null($classId) ? $courseId : $classId;

            $registrationOrder = isset($orderItemId)
                             ? $registrationOrder
                             : ($this->registrationOrderRepo
                                    ->getByRegistrableIdAndAgent(
                                        $registrableId,
                                        $this->data['user']->id,
                                        Constant::PENDING
                                    )
                                ?: $this->registrationOrderRepo
                                        ->getRegisteredActingAgent(
                                            $registrableId,
                                            is_null($classId) ? Constant::COURSE_OBJECT : Constant::COURSE_CLASS_OBJECT,
                                            $this->data['user']->contact
                                        ));


            if ($registrationOrder) {
                $classRegistrations = $classRegistrationRepo->getByRegistrationIdNoSwap($registrationOrder->id);
                $count = $classRegistrationRepo->getByRegistrationIdNotNewNoSwap($registrationOrder->id)->count();

                if ($classRegistrations->count()) {
                    foreach ($classRegistrations as $key => $classRegistration) {
                        $this->data['attendees'][$key] = $classRegistration->attendee_contact_id;

                        if ($classRegistration->subscriptionDiscount) {
                            array_push($this->data['noSwapYearlySubs'], $classRegistration->attendee_contact_id);
                        }

                        if ($classRegistration->classMilitaryDiscount) {
                            array_push($this->data['noSwapGSAs'], $classRegistration->attendee_contact_id);
                        }

                        if ($classRegistration->is_cancelled) {
                            array_push($this->data['noSwapCancelled'], $classRegistration->attendee_contact_id);
                        }

                        if ($classRegistration->is_new) {
                            array_push($notIn, $classRegistration->attendee_contact_id);
                        }

                        $classCombination = $classCombinationRepo->getBySingeRegId($registrationOrder->id);
                        $classCombinationDiscount = $classCombinationDiscountRepo
                            ->getByClassRegistrationId($classRegistration->id, true);

                        if ($orderItem and $registrationOrder->has_pair and $classCombination and $classCombinationDiscount) {
                            $this->data['pairedClassSwapNote'] = true;
                        }
                    }

                    $this->data['regCount'] = $count;
                }
            }

            $this->data['contacts'] =  $this->contactRepo
                                            ->getByUserWithContact($this->data['user']->id, $notIn);

            $this->data['notIn'] = $notIn;

            $this->data['registrationOrderId'] = $registrationOrder ? $registrationOrder->id : null;

            $view = isset($userId)
                ? view('admin.users.contacts', $this->data)
                : view('main.contact.index', $this->data);

            if(isset($userId) && isset($classId) && $this->data['class']->classes->count() != 0) {
//                $view = view('admin.users.contacts-related', $this->data);
            }

            if (count($register) > 1) {
                $classes = collect();
                foreach (array_unique($register) as $class_id) {
                    $classes->push(CourseClass::findOrFail($class_id));
                }
                $this->data['classes'] = $classes;
                $view = view('main.classes.register', $this->data);
            }

            return $view;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function checkEmail(Request $request)
    {
        try {

            $response = '';

            $user = $this->userRepo->getByEmailNotLoggedIn($request->get('email'), Auth::user()->id);

            if ($user
                && $user->hasRole(Constant::ROLE_MEMBER)) {
                $response = [
                    'id'         => $user->id,
                    'first_name' => $user->contact->first_name,
                    'last_name'  => $user->contact->last_name,
                    'email'      => $user->email
                ];
            }

            return $response;

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
