<?php namespace App\Http\Controllers\Main;

use App\Facades\OrderService;
use App\Facades\OrderUtility;
use App\Http\Requests\UpdateOrderStatusRequest;
use App\Models\InvoicedPayment;
use App\Models\RegistrationOrder;
use App\Models\StateTax;
use App\Services\PaymentService;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Session;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\CouponDiscountRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\ClassSubscriptionDiscountRepository;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassMilitaryDiscountRepository;
use App\Repositories\OrderAdjustmentRepository;
use App\Repositories\OrderAdjustmentHistoryRepository;
use App\Repositories\StateRepository;
use App\Repositories\ActingOriginatingAgentRepository;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\OrderNote;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Order;
use App\Models\OrderItem;
use App\Utilities\Constant;

class OrderController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;
    private $orderNote;
    private $user;
    private $orderItem;
    private $order;
    private $actingOriginatingAgentRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                OrderNote $orderNote,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                User $user,
                                Order $order,
                                OrderItem $orderItem)
    {
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->orderNote = $orderNote;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->user = $user;
        $this->order = $order;
        $this->orderItem = $orderItem;
    }


    /**
     * @param Request $request
     * @param ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param ClassMilitaryDiscountRepository $militaryDiscountRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function invoiceInfo(Request $request,
                                ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo,
                                ClassCreditDiscountRepository $classCreditDiscountRepo,
                                ClassMilitaryDiscountRepository $militaryDiscountRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                CouponDiscountRepository $couponDiscountRepo,
                                OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo)
    {
        try {

            $userId = $request->route('user_id');
            $order = $this->orderRepo->getById($request->route('order_id'));

            if ($order) {

                $this->data['page'] = 'invoice_info';

                $this->data['user'] = isset($userId)
                    ? $this->userRepo->getById($userId)
                    : Auth::user();

                $this->data['order'] = $order;
                $this->data['items'] = $order->items;
                $this->data['shipping_rates'] = $order->order_shipping_rates;
                $this->data['order_tax'] = $order->order_state_tax;
                $this->data['shipping_to'] = $order->shipping_address.' - '.$order->shipping_zip;
                $this->data['total'] = $order->total;
                $this->data['invoices'] = [];

                $this->data['subscriptionDiscounts'] = $order
                    ? $classSubscriptionDiscountRepo->getByOrderId($order->id)
                    : null;

                $this->data['combinationDiscounts'] = $order
                    ? $classCombinationDiscountRepo
                        ->getByOrderId($order->id)
                    : null;

                $this->data['halfPriceDiscounts'] = $order
                    ? $classHalfPriceDiscountRepo
                        ->getByOrderId($order->id)
                    : null;

                $this->data['couponDiscounts'] = $order
                    ? ($couponDiscountRepo
                        ->getByOrderId($order->id)
                        ? $classCouponDiscountRepo
                            ->getByCoupDiscountNoNullRegistration(
                                $couponDiscountRepo
                                    ->getByOrderId($order->id)
                                    ->id)
                        : null)
                    : null;

                $this->data['creditDiscounts'] = $order
                    ? $classCreditDiscountRepo->getByOrderId($order->id)
                    : null;

                $this->data['orderAdjustmentDiscounts'] = $order
                    ? $orderAdjustmentDiscountRepo
                        ->getByOrderId($order->id)
                    : null;

                $this->data['militaryDiscounts']  = $order
                    ? $militaryDiscountRepo->getByOrderId($order->id)
                    : null;

                $invoices = $order->invoicePayments()->orderBy('updated_at', 'DESC')->get();

                if ($invoices->count()) {
                    foreach ($invoices as $invoice) {
                        array_push($this->data['invoices'], (object) [
                            'id' => $invoice->id,
                            'po_number' => $invoice->po_number,
                            'check_number' => $invoice->check_number,
                            'poImage' =>  $invoice->po_image,
                            'checkImage' => $invoice->check_image,
                            'recipients' => $invoice->invoicePaymentRecipients,
                            'poFile' => $invoice->po_file,
                            'checkFile' => $invoice->check_file
                        ]);
                    }
                }
                $this->data['notes'] = $this->orderNote->where('order_id','=',$order->id)
                                            ->orderBy('created_at','desc')
                                            ->get();

                $view = isset($userId)
                    ? view('admin.users.invoice_info', $this->data)
                    : view('main.users.invoice_info', $this->data);

                return $view;
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param ClassMilitaryDiscountRepository $militaryDiscountRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @param OrderAdjustmentHistoryRepository $orderAdjustmentHistoryRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function entireOrderInfo(Request $request,
                                    ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo,
                                    ClassCreditDiscountRepository $classCreditDiscountRepo,
                                    ClassMilitaryDiscountRepository $militaryDiscountRepo,
                                    ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                    ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                    CouponDiscountRepository $couponDiscountRepo,
                                    OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                    ClassCouponDiscountRepository $classCouponDiscountRepo,
                                    OrderAdjustmentRepository $orderAdjustmentRepo,
                                    OrderAdjustmentHistoryRepository $orderAdjustmentHistoryRepo)
    {
        try {

            $userId = $request->route('user_id');
            $orderId = $request->route('order_id');
            $user = isset($userId)
                    ? $this->userRepo->getById($userId)
                    : Auth::user();

            $order =  $this->orderRepo->getById($orderId);

            if ($order) {

                $this->data['page'] = 'order_info';
                $this->data['user'] = $user;
                $this->data['order'] = $order;
                $this->data['items'] = $order->items;
                $this->data['shipping_rates'] = $order->shipping_rates;
                $this->data['order_tax'] = $order->order_tax;
                $this->data['total'] = $order->total;

                $this->data['subscriptionDiscounts'] = $classSubscriptionDiscountRepo->getByOrderId($orderId);

                $this->data['combinationDiscounts'] = $classCombinationDiscountRepo->getByOrderId($orderId);

                $this->data['halfPriceDiscounts'] = $classHalfPriceDiscountRepo->getByOrderId($orderId);

                $this->data['creditDiscounts'] = $classCreditDiscountRepo->getByOrderId($orderId);

                $this->data['couponDiscounts'] = $couponDiscountRepo
                    ->getByOrderId($orderId)
                    ? $classCouponDiscountRepo
                        ->getByCoupDiscountNoNullRegistration(
                            $couponDiscountRepo
                                ->getByOrderId($orderId)
                                ->id)
                    : null;

                $this->data['orderAdjustmentDiscounts'] = $orderAdjustmentDiscountRepo->getByOrderId($orderId);

                $this->data['militaryDiscounts'] = $militaryDiscountRepo->getByOrderId($orderId);

                $this->data['corpSeatCredits'] = $user->getCorporateSeatsCredit();

                $this->data['orderAdjustments'] = $orderAdjustmentRepo->getByOrderId($orderId);

                $this->data['orderAdjustmentHistories'] = $orderAdjustmentHistoryRepo->getByOrderId($orderId);

                $this->data['notes'] = $this->orderNote->where('order_id','=',$order->id)
                                    ->orderBy('created_at','desc')
                                    ->get();
                $view = isset($userId)
                    ? view('admin.users.order_info', $this->data)
                    : view('main.users.order_info', $this->data);

                return $view;
            }

            throw new NotFoundHttpException('Order Not Found.');

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param OrderRepository $repo
     * @param UserRepository $userRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function downloadReceipt(Request $request,
                                   OrderRepository $repo,
                                   UserRepository $userRepo)
    {



        $userId = $request->route('user_id');
        $orderId = $request->route('order_id');

        $order = $repo->getById($orderId);
        $user = isset($userId)
            ? $userRepo->getById($userId)
            : Auth::user();

        $pdf = OrderService::generateReceipts($order, $user);

        return (new Response($pdf, 200))
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'attachment; filename="Receipts.pdf"');
    }

    /**
     * @param Request $request
     * @param OrderRepository $repo
     * @param UserRepository $userRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function paymentSummary(Request $request,
                                    OrderRepository $repo,
                                    UserRepository $userRepo)
    {

        $this->data['page'] = 'payment_summary';

        $userId = $request->route('user_id');
        $orderId = $request->route('order_id');

        $order = $repo->getById($orderId);
        $user = isset($userId)
            ? $userRepo->getById($userId)
            : Auth::user();

        $this->data['user'] = $user;
        $this->data['order'] = $order;
        $this->data['invoicedPayments'] = $order->invoicePayments()->whereNotNull('items')->get();

        return isset($userId)
                ? view('admin.users.payment_cc', $this->data)
                : view('main.users.payment_cc', $this->data);
    }

    /**
     * Add Notes
     *
     * @param Request $request ajax
     * @return json
     */
    public function addNotes(Request $request)
    {
        if($request->ajax()){
            $order = $this->orderRepo
                          ->createSelectStatement()
                          ->where('id','=',$request->get('order_id'))
                          ->first();

            if(count($order)<1){
                return abort(404);
            }
            $inputs = $request->only('order_id');
            $inputs['notes'] = $request->get('note');
            $inputs['user_id'] = auth()->user()->id;
            $notes = $this->orderNote->create($inputs);
            $inputs['email'] = auth()->user()->email;
            $inputs['created_at_string'] = $notes->created_at_string;
            return response()->json($inputs, 200);
        }
    }


    /**
     * @param PaymentService $paymentService
     * @param Request $request
     * @param $user_id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateOrderStatus(PaymentService $paymentService,
                                      UpdateOrderStatusRequest $request,
                                      $user_id)
    {

        try {

            $order = $this->order->where('user_id','=',$user_id)
                ->where('id','=', $request->input('order_id'))
                ->firstOrFail();

        } catch (ModelNotFoundException $e) {
            return redirect()->back()
                ->with('error_mesage', 'Order not Found');
        }

        if($request->input('update_status_only', 0) == 1)
        {
            OrderService::updateOrderAndItemsStatus($order, $request->input('status'));
            return redirect()->back()
                ->with('success_message', 'Updated Status');
        }

        $status = ($request->input('invoice_status', 0) == 1)
            ? 'COMPLETE'
            : $order->status;


        $amount = $request->input('amount');
        $payment_type = $request->input('payment_type');
        $check_num = $request->input('check_num', '0');

        $order_items = $order->items;

        $existing_order_items = [];

        if($order->paymentTransactions->count()) {

            foreach ($order->paymentTransactions as $transaction) {
                if(json_decode($transaction->items)) {
                    foreach (json_decode($transaction->items)->items as $item) {
                        $existing_order_items[] = $item->id;
                    }
                }
            }

            if( ! empty($existing_order_items)) {
                $order_items = $order_items->filter(function ($value) use ($existing_order_items) {
                    return array_key_exists($value->orderable_id, $existing_order_items);
                });
            }

        }

        $items = ($order_items->count())
                ? OrderUtility::createPaymentItems($order_items, $amount)
                : NULL;

        if($order->status != $status)
        {
            OrderService::updateOrderAndItemsStatus($order, $status);
        }

        $paymentTransaction = $paymentService->createPaymentTransaction([
            'email'             => $order->user->email,
            'card_brand'        => $payment_type,
            'auth_code'         => '000000',
            'transaction_id'    => $check_num,
            'masked_cc'         => $check_num,
            'amount'            => $amount,
            'status'            => true,
            'order_id'          => $order->id,
            'items'             => $items,
            'shipping_rates'    => $order->shipping_rates,
            'state_tax'         => $order->order_tax,
            'billing_company'           => $order->billing_company,
            'billing_address'           => $order->billing_address,
            'billing_city'              => $order->billing_city,
            'billing_state'             => $order->billing_state,
            'billing_zip'               => $order->billing_zip,
            'shipping_company'           => $order->shipping_company,
            'shipping_address'           => $order->shipping_address,
            'shipping_city'              => $order->shipping_city,
            'shipping_state'             => $order->shipping_state,
            'shipping_zip'               => $order->shipping_zip,
            'shipping_county'           => $order->shipping_county,
            'trans_total'               => $order->total,
        ]);


        if($paymentTransaction){

            $order->addToPaidTotal($amount);

            return redirect()->back()
                            ->with('success_message', 'Order Updated');
        }

        return redirect()->back()
                        ->withErrors(['message' => 'Something went wrong!']);
    }

    /**
     * @param Request $request
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassMilitaryDiscountRepository $militaryDiscountRepo
     * @param ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param StateRepository $stateRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function invoiceInfoCheckout(Request $request,
                             ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                             ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                             CouponDiscountRepository $couponDiscountRepo,
                             OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                             ClassMilitaryDiscountRepository $militaryDiscountRepo,
                             ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo,
                             ClassCreditDiscountRepository $classCreditDiscountRepo,
                            StateRepository $stateRepo)
    {
        $userId = $request->route('user_id');

        $user = isset($userId)
            ? $this->userRepo->getById($userId)
            : Auth::user();

        $this->data['page']  = 'checkout';


        $order = $this->orderRepo->getByUserIdForInvoiceInfo($user->id,$request->route('order_id'));

        $this->data['user']  = $user;
        $this->data['userDetails'] = $user->contact;
        $this->data['states']   = $stateRepo->getByList('state', 'state')->toArray();
        $this->data['order'] = $order;
        $this->data['items'] = $order ? $order->items : null;
        $this->data['total'] = $order ? $order->items()->sum('price_charged') : 0;

        if (!Auth::user()->isMember() && ($order && $order->csr && $order->csr->count() === 0)) {
            // Transaction is now being handled by a CSR/Admin
            $order->csr()->associate(Auth::user()->id);
            $order->save();
        }

        $this->data['subscriptionDiscounts'] = $order
                                                ? $classSubscriptionDiscountRepo->getByOrderId($this->data['order']->id)
                                                : null;

        $this->data['combinationDiscounts'] = $order
                                                ? $classCombinationDiscountRepo
                                                    ->getByOrderId($order->id)
                                                : null;

        $this->data['halfPriceDiscounts'] = $order
                                            ? $classHalfPriceDiscountRepo
                                                ->getByOrderId($order->id)
                                            : null;

        $this->data['couponDiscounts'] = $order
                                            ? ($couponDiscountRepo
                                                ->getByOrderId($order->id)
                                                ? $couponDiscountRepo
                                                    ->getByOrderId($order->id)
                                                    ->classDiscounts
                                                : null)
                                            : null;

        $this->data['creditDiscounts'] = $order
                                            ? $classCreditDiscountRepo->getByOrderId($order->id)
                                            : null;

        $this->data['orderAdjustmentDiscounts'] = $order
                                                    ? $orderAdjustmentDiscountRepo
                                                        ->getByOrderId($order->id)
                                                    : null;

        $this->data['militaryDiscounts']  = $order
                                            ? $militaryDiscountRepo->getByOrderId($order->id)
                                            : null;


        $currentYear = date('Y');
        $length = $currentYear + 29;
        $this->data['years'] = [];

        for ($ctr = 1; $ctr <= 12; $ctr++) {
            $months[$ctr] = date('F', mktime(0,0,0,$ctr, 1, date('Y')));
        }

        $this->data['months'] = $months;

        for ($currentYear; $length >= $currentYear; $currentYear++) {
            $this->data['years'][$currentYear] = $currentYear;
        }

        $this->data['ccOnly'] = ($order and $order->items)
            ? ($this->orderItemRepo->checkFromOrderItemsIfThereIsItemWithType(
                $order->items,
                Constant::COMPUTER_BASED))
                ? true
                : false
            : false;

        $this->data['counties']   = StateTax::lists('county', 'id');
        $this->data['counties']->prepend('Please choose county:', '');

        if(!empty(trim($order->shipping_county))) {
            $this->data['county'] = StateTax::getShippingCountyFromName($order->shipping_county);
        } else {
            $this->data['county'] = null;
        }

        return view('admin.users.invoice_info_checkout', $this->data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deleteEntireOrder(Request $request)
    {
        $order = Order::findOrFail($request->route('id'));

        foreach ($order->items as $item) {
            if ($item->orderable_type == Constant::REGISTRATION_ORDER) {
                $registrationOrder = RegistrationOrder::find($item->orderable_id);

                if ($registrationOrder) {
                    foreach ($registrationOrder->classRegistrations as $classRegistration) {
                        $classRegistration->delete();
                    }
                }

                $registrationOrder->delete();
            }

            if ($item->orderable_type == Constant::CORPORATE_SEAT) {

                $order->user->subtractCorpSeatCredits($item->orderable->credits);

            }

            $item->delete();
        }

        $order->delete();

        return redirect()->route('admin.user_details', [
            'contact_id' => $order->user->contact->id, 'user_id' => $order->user->id])
            ->with('success_message', 'Order deleted.');
    }
}
