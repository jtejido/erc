<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Regulation;
use App\Models\Tip;
use App\Models\Promo;

class BaseController extends Controller
{
    protected $data;

    protected $page_limit = 15;

    /**
     * BaseController constructor
     */
    public function __construct() {}

    public function getArticles()
    {

        $articles = [];

        $regulations = Regulation::where('is_published', true)
            ->orderby('published_date', 'desc')
            ->take(3)
            ->get()
            ->toArray();

        $tipsEnv = Tip::where('is_published', true)
            ->where('category_id', 3)
            ->orderby('published_date', 'desc')
            ->take(3)
            ->get()->toArray();
        $tipsSafe = Tip::where('is_published', true)
            ->where('category_id', 4)
            ->orderby('published_date', 'desc')
            ->take(3)
            ->get()->toArray();


        $regulationsCount = count($regulations);
        $tipsEnvCount = count($tipsEnv);
        $tipsSafeCount = count($tipsSafe);
        $max = min([$regulationsCount, $tipsEnvCount, $tipsSafeCount]);

        for ($ctr = 0; $ctr < $max; $ctr++) {
            if (isset($tipsEnv[$ctr])) {
                $tipsEnv[$ctr]['env'] = 1;
                array_push($articles, $tipsEnv[$ctr]);
            }
            if (isset($tipsSafe[$ctr])) {
                $tipsSafe[$ctr]['safety'] = 1;
                array_push($articles, $tipsSafe[$ctr]);
            }
            if (isset($regulations[$ctr])) {
                $regulations[$ctr]['reg'] = 1;
                array_push($articles, $regulations[$ctr]);
            }
        }

        return $articles;

    }

    public function getPromos()
    {
        
        return Promo::where('active', true)->orderBy('order')->get();

    }
}
