<?php

namespace App\Http\Controllers\Main;

use App\Models\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ReminderController extends Controller
{
    public function webcast(Request $request)
    {
        $classId = $request->input('classId');
        $userId = $request->input('userId');
        $user = Contact::findOrFail($userId);
        try {
            Artisan::call('reminder:webcast', [
                'classId'   => $classId,
                '--user'    => $user->id,
                '--now'     => true,
                '--resend'  => true
            ]);
        } catch (\Exception $e) {
            return back()->with(['error_message' => 'Something went wrong, please try again.']);
        }
        return back()->with(['success_message' => 'Reminder has been resent.']);
    }
}
