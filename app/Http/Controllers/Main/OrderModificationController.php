<?php namespace App\Http\Controllers\Main;

use App\Facades\OrderService;
use App\Models\Order;
use Auth;
use Session;
use Log;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\OrderAdjustment;
use App\Repositories\ContactRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\OrderAdjustmentRepository;
use App\Facades\OrderModificationUtility;
use App\Facades\OrderModificationService;
use App\Utilities\Constant;
use App\Events\SwapUserEvent;
use App\Events\EmailAccountingModificationConfirmationEvent;

class OrderModificationController extends BaseController
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo)
    {
        $this->userRepo = $userRepo;
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepo
     * @param OrderItemRepository $orderItemRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addAttendees(Request $request,
                                 ContactRepository $contactRepo,
                                 OrderItemRepository $orderItemRepo)
    {
        $userId = $request->route('user_id');
        $orderItem = $orderItemRepo->getById($request->route('order_item_id'));
        $agent = isset($userId)
                 ? $this->userRepo->getById($userId)
                 : Auth::user();

        if ($orderItem) {

            $registrationOrder = $orderItem->orderable;
            $order = $orderItem->order;
            $withinRegistration = $registrationOrder->classRegistrations
                ->pluck('attendee_contact_id')
                ->toArray();

            $notWithinRegistration = $contactRepo->getByUserNotWithinRegistration($agent->id, $withinRegistration);
            $notWithinRegistrationIds = $notWithinRegistration->pluck('attendee_contact_id')->toArray();

            $data = [
                'page' => 'add_attendees',
                'user' => $agent,
                'agent' => !in_array($agent->contact->id, $withinRegistration) ? $agent : null,
                'class' => !$registrationOrder->isCourse()
                    ? $registrationOrder->registrable
                    : null,
                'course' => $registrationOrder->isCourse()
                    ? $registrationOrder->registrable
                    : $registrationOrder->registrable->course,
                'registrationOrder' => $registrationOrder,
                'orderItem' => $orderItem,
                'order' => $order,
                'contacts' => $notWithinRegistration,
                'notWithinIds' => $notWithinRegistrationIds
            ];

            $view = isset($userId)
                ? view('admin.completed-registrations.add_attendees', $data)
                : view('main.completed.add_attendees', $data);

            return $view;
        }

        return response()->make(view('exceptions.filenotfound'), 404);
    }

    /**
     * @param Request $request
     * @param OrderItemRepository $orderItemRepo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doAddAttendees(Request $request,
                                   OrderItemRepository $orderItemRepo)
    {
        $userId = $request->get('user_id');
        $agent = $userId
                 ? $this->userRepo->getById($userId)
                 : Auth::user();
        $item = $orderItemRepo->getById($request->get('item'));
        $registrationOrder = $item->orderable;
        $order = $item->order;

        $contacts = $request->get('contacts');

        OrderModificationService::addAttendees([
            'agent' => $agent,
            'attendees' => $contacts,
            'registrationOrder' => $registrationOrder,
            'item' => $item,
            'order' => $order,
            'status' => Constant::IN_PROCESS
        ]);

        $request->session()->flash('success_message', 'Attendees added.');

        $route = $userId
                 ? route('admin.order.info', [$agent->id, $order->id])
                 : route('order.info', $order->id);

        return $this->json(true, compact('route'));;
    }

    /**
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function doSwapAttendees(Request $request,
                                    ClassRegistrationRepository $classRegistrationRepo)
    {
        try {

            $notify = $request->get('doNotEmail') == "true" ? false : true;
            $userId = $request->get('user_id');
            $course = $request->get('course_id');
            $class = $request->get('class_id');
            $contacts = $request->get('contacts');
            $orderItemId = $request->get('item');
            $orderItem   = $this->orderItemRepo->getById($orderItemId);

            $registrationOrder = $orderItem->orderable;
            $order = $orderItem->order;

            $user = $userId
                ? $this->userRepo->getById($userId)
                : $order->user;

            $registrableId   = $class == 0 ? $course : $class;
            $registrableType = $class == 0 ? Constant::COURSE_OBJECT : Constant::COURSE_CLASS_OBJECT;

            $existing = [];

            $classRegistrations = $classRegistrationRepo->getByRegistrationIdNotNewNoSwap($registrationOrder->id);

            if ($classRegistrations->count()) {
                foreach ($classRegistrations as $key => $registration) {
                    $existing[] = $registration->attendee_contact_id;
                }
            }

            $added   = collect(array_diff($contacts, $existing));
            $removed = collect(array_diff($existing, $contacts));

            if (($added->count() > 0) and ($removed->count() > 0)) {
                OrderModificationService::doSwapAttendees([
                    'agent_id' => $user->id,
                    'registrationOrderId' => $orderItem->orderable_id,
                    'addedContacts' => $added,
                    'removedContacts' => $removed,
                    'status' => $order->status
                ]);

                Session::flash('success_message', 'Attendees have been swapped.');

                event(new SwapUserEvent(
                    Auth::user()->id,
                    $user->id,
                    $registrationOrder->id,
                    $added->toArray(),
                    $removed->toArray(),
                    $notify
                ));
            }

            else {
                Session::flash('success_message', 'No change in attendees. No swap occurred.');
            }

            $route = ($userId
                ? route('admin.user_details', [
                    'contact_id' => $user->contact->id,
                    'user_id' => $user->id
                ])

                : route('user.home'));

            return $this->json(true, compact('route'));

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function swapNotification(Request $request,
                                     RegistrationOrderRepository $registrationOrderRepo,
                                     ClassRegistrationRepository $classRegistrationRepo,
                                     ClassCombinationRepository $classCombinationRepo,
                                     ClassCombinationDiscountRepository $classCombinationDiscountRepo)
    {
        try {

            $this->data['pairedClass'] = '';
            $this->data['pairedClassSwapNames'] = [];

            $contacts = $request->get('contacts');
            $registrationOrder = $registrationOrderRepo->getById($request->get('registration_order_id'));

            $existing = [];

            if ($registrationOrder) {
                foreach ($registrationOrder->classRegistrations as $key => $registration) {
                    $existing[] = $registration->attendee_contact_id;
                }
            }

            $removedContactIds = collect(array_diff($existing, $contacts));

            foreach ($removedContactIds as $removedContactId) {
                $classRegistration = $classRegistrationRepo->getByAttendeeRegistrationStatus([
                    'attendee_contact_id' => $removedContactId,
                    'registration_order_id' => $registrationOrder->id,
                    'status' => $registrationOrder->status
                ]);

                $classCombination = $classCombinationRepo->getBySingeRegId($registrationOrder->id);
                $classCombinationDiscount = $classCombinationDiscountRepo
                    ->getByClassRegistrationId($classRegistration->id, true);

                if ($registrationOrder->has_pair and $classCombination and $classCombinationDiscount) {
                    array_push($this->data['pairedClassSwapNames'], $classRegistration->contact->name);

                    $this->data['pairedClass'] = ($registrationOrder->id != $classCombination->firstRegistration->id)
                        ? $classCombination->firstRegistration->title()
                        : $classCombination->secondRegistration->title();

                }
            }

            $response = count($this->data['pairedClassSwapNames'])
                ? view('includes.swap_notification', $this->data)->render() : $this->json(0);

            return $response;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function queryIfCancellable(Request $request,
                                       ClassRegistrationRepository $classRegistrationRepo,
                                       RegistrationOrderRepository $registrationOrderRepo,
                                       OrderAdjustmentRepository $orderAdjustmentRepo)
    {
        try {
            $data['paid'] = 0;
            $data['customers'] = [];
            $cancelledClassRegistrationIds = [];

            $data['agentId'] = $request->get('agent_id');
            $newRegistration = $request->get('new_registration');
            $classRegistrationIds = $request->get('class_registration_ids');
            $registrationOrderId = $request->get('registration_order_id');
            $order = $this->orderRepo->getById($request->get('order_id'));

            $registrationOrder = $registrationOrderRepo->getById($registrationOrderId);
            $restriction = OrderModificationUtility::cancellationWithinRestriction($registrationOrder);

            $data['registrationOrderId'] = $registrationOrder->id;

            if (!$restriction or $newRegistration) {
                foreach ($classRegistrationIds as $classRegistrationId) {
                    $classRegistration = $classRegistrationRepo->getById($classRegistrationId);

                    $data['paid'] += $classRegistration->paid_charge;

                    array_push($cancelledClassRegistrationIds, $classRegistrationId);

                    $params = [
                        'id' => $classRegistration->id,
                        'name' => $classRegistration->contact->name,
                        'is_new' => $classRegistration->is_new
                    ];

                    array_push($data['customers'], $params);
                }

                $data['deferredHalfDiscount'] = OrderModificationUtility::extractDeferredHalfPriceDiscounts($registrationOrder, $cancelledClassRegistrationIds);
                $data['deferredGroupDiscount'] = OrderModificationUtility::extractDeferredGroupDiscounts($registrationOrder, $cancelledClassRegistrationIds);

                $addtionalFeesFromAdjustments = $orderAdjustmentRepo->getByOrderIdReturnTotal($order->id, Constant::ADDED);

                $data['additionalFees'] = $addtionalFeesFromAdjustments;

                $data['deductions'] = $orderAdjustmentRepo->getByOrderIdReturnTotal($order->id, Constant::DEDUCTED);

                $data['subTotalRefund'] = OrderModificationUtility::extractCancellationSubTotal(
                    $data['paid'],
                    $data['deferredHalfDiscount']['amount'],
                    $data['deferredGroupDiscount']['amount'],
                    $data['additionalFees'],
                    $data['deductions'],
                    0
                );

                $data['newOrderTotal'] = OrderModificationUtility::extractNewOrderTotal(
                    $order->total,
                    $data['paid'],
                    $data['deferredHalfDiscount']['amount'],
                    $data['deferredGroupDiscount']['amount'],
                    $data['additionalFees'],
                    $data['deductions']
                );
            }

            $view = $newRegistration
                ? view('main.includes.cancel_new_registration_confirmation', $data)
                : ($restriction
                ? view('main.includes.contact_admin_cancel_notification_modal')
                : view('main.includes.registration_cancellation_modal', $data));

            return $view;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function cancelClassRegistrations(Request $request)
    {
        try {
            OrderModificationService::cancelClassRegistration($request->all());

            $s = count($request->get('class_registration_ids')) > 1 ? 's' : '';

            return redirect()->back()
                             ->with('success_message', 'Registration' . $s .' cancelled.');

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelNewRegistrations(Request $request)
    {
        OrderModificationService::cancelNewClassRegistrations($request->all());

        $s = count($request->get('class_registration_ids')) > 1 ? 's' : '';

        return redirect()->back()
            ->with('success_message', 'Registration' . $s .' cancelled.');
    }

    /**
     * @param Request $request
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @return $this
     */
    public function confirmOrderModification(Request $request,
                                             OrderAdjustmentRepository $orderAdjustmentRepo)
    {
        $notify = $request->has('doNotEmail') ? false: true;

        $orderAdjustments = $orderAdjustmentRepo->getByIds($request->get('order_adjustment_ids'));
        $agentId = $orderAdjustments->pluck('agent_id')->first();
        $orderId = $orderAdjustments->pluck('order_id')->first();
        $agent = $this->userRepo->getById($agentId);
        $order = Order::find($orderId);

        event(new EmailAccountingModificationConfirmationEvent($request->get('order_adjustment_ids'), $agent, null, $notify));
        OrderModificationService::createAdjustmentDiscountOnDeduction($orderAdjustments, $request->get('status'));
        OrderModificationService::createAdjustmentHistoryFromItemAdjustments($orderAdjustments);
        OrderModificationService::updateStatusOfNewItems($agent, $orderAdjustments);
        $order->updateOrderTax();
        OrderService::updateOrderTotal($order);
        OrderService::updatePaidOrderTotal($order);
        OrderAdjustment::destroy($request->get('order_adjustment_ids'));

        $request->session()->flash('success_message', 'Email Sent to Accounting.');

        return '';
    }
}
