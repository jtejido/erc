<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Models\Link;
use App\Models\Link\Category;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Log;
use Session;

class LinkController extends BaseController
{

    public function index()
    {

        $this->data['page']       = 'links';
        $this->data['links']      = Link::all();
        $this->data['categories'] = Category::all();

        return view('main.links.index', $this->data);
    }
}
