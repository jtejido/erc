<?php namespace App\Http\Controllers\Main;

use App\Http\Requests;
use App\Models\Course;
use App\Models\GeneralLocation;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use App\Repositories\CourseTypeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Models\Tip;
use App\Models\Regulation;

class CourseController extends BaseController
{

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    /**
     * @var CourseTypeRepository
     */
    private $courseTypeRepo;

    /**
     * Courses
     *
     * @param CourseRepository $courseRepo
     */
    public function __construct(CourseRepository $courseRepo,
                                CategoryRepository $categoryRepo,
                                CourseTypeRepository $courseTypeRepo)
    {
        $this->courseRepo = $courseRepo;
        $this->categoryRepo = $categoryRepo;
        $this->courseTypeRepo = $courseTypeRepo;

        $this->data['page'] = 'courses';
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->data['categoryFilter']   = $request->get('category');
        $this->data['typeFilter']       = $request->get('type');
        $this->data['searchq']          = $request->get('searchq');
        $this->data['locationFilter']   = $request->get('location');
        $this->data['generalLocations']   = GeneralLocation::orderBy('location')->get();
        $this->data['courseTypes']      = $this->courseTypeRepo->byType();
        $this->data['courseCategories'] = $this->categoryRepo->getAll();
        $this->data['categories']       = !$this->data['categoryFilter'] ? $this->categoryRepo->getAll()
            : [ $this->categoryRepo->getById($this->data['categoryFilter']) ];

        return view('main.courses.index', $this->data);
    }

    /**
     * Render details of a course given id
     *
     * @param Request $request
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function getDetails(Request $request)
    {

        $this->data['articles'] = $this->getArticles();

        $courseId = $request->route('id');

        $course = Course::find($courseId);

        if($course) {
            return redirect()->route('course.show', [ 'slug' => $course->slug ]);
        }

        $course = Course::where('slug', $courseId)->first();

        if(!$course) {
            return abort(404);
        }

        try	{

            $this->data['course'] = $course;
            $classes = $course->classes()->active()->orderBy('start_date', 'asc')->get();
            $this->data['active'] = $classes;
            $this->data['now']    = Carbon::now();

            $view = ($course->courseType->id == \App\Utilities\Constant::COMPUTER_BASED)
                ? 'main.courses.cbt-details'
                : 'main.courses.details';

            return view($view, $this->data);



        } catch (\Exception $e) {
            throw $e;
        }
    }
}
