<?php

namespace App\Http\Controllers\Main;

use App\Facades\CourseUtility;
use App\Facades\OrderUtility;
use App\Repositories\OrderItemRepository;
use App\Utilities\Constant;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderHistoryController extends BaseController
{
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;


    /**
     * OrderHistoryController constructor.
     */
    public function __construct(OrderItemRepository $orderItemRepo)
    {
        $this->orderItemRepo = $orderItemRepo;
    }

    public function index_api(Request $request)
    {

        $data = [];

        $user = auth()->user();


        $transactions = $this->orderItemRepo->getAllItems($user->id, null, false);

        foreach ($transactions as $orderItem) {

            $order = $orderItem->order()->withTrashed()->first();

            if ($order->trashed()) continue;

            if($orderItem->orderable_type == Constant::REGISTRATION_ORDER
                and is_null($orderItem->orderable()->withTrashed()->first())) {
                continue;
            }

            $orderable = $orderItem->orderable()->withTrashed()->first();

            $_data['trans_date'] = $order->trans_date;

            $_data['title'] = OrderUtility::getTitle($orderItem);
            if(! ($orderable instanceof \App\Models\OnsiteTraining)
                and (class_basename($orderable) == \App\Utilities\Constant::REGISTRATION_ORDER)
                and ($orderable->registrable->course_type_id != \App\Utilities\Constant::COMPUTER_BASED)) {
                $_data['title'] .= '<div style="margin-top: 20px">';
                $_data['title'] .= '<p><em><strong style="display: inline;">Class Date: </strong>'
                    .CourseUtility::getClassDate($orderItem)
                    .'</em></p>';
                $_data['title'] .= '<p><em><strong style="display: inline;">Class Location: </strong>'
                    .CourseUtility::getClassLocation($orderItem)
                    .'</em></p>';
                $_data['title'] .= '</div>';
            }

            $_data['agent'] = OrderUtility::getAgent($orderItem);

            $status = OrderUtility::getStatus($orderItem);

            if ($status == Constant::COMPLETE) {
                $_data['status'] = '<span class="label label-success btn-flat">PAID</span>';
            } elseif ($status == Constant::PENDING) {
                $_data['status'] = '<span class="label label-danger btn-flat">'.$status.'</span>';
            } elseif ($status == Constant::CANCELLED) {
                $_data['status'] = '<span class="label label-danger btn-flat">'.$status.'</span>';
            } else {
                $_data['status'] = '<span class="label label-warning btn-flat">'.$status.'</span>';
            }

            $_data['notes']   = '';
            foreach ($order->notes as $note) {
                $_data['notes']   .= $note->notes;
            }

            $_data['actions']   = '';

            $isActingAgent = null;
            $isInvoiced = OrderUtility::isInvoiced($order);
            $completedOrInvoiced = OrderUtility::isCompleteOrInvoiced($orderItem);
            $originatingAgent = OrderUtility::isOriginatingAgent($order->id, $user->id);
            $isClassRegistration = $orderItem->orderable_type == Constant::REGISTRATION_ORDER;

            $notActiveClass = ($isClassRegistration and
                $orderable->registrable_type == Constant::COURSE_CLASS_OBJECT and
                is_null(CourseUtility::scopeActive($orderable->registrable)))
                ? 'hidden'
                : '';

            $hideIfOnsiteTraining = $orderItem->orderable_type ==  Constant::ONSITE_TRAINING
                ? 'hidden'
                : '';

            if ($isClassRegistration) {
                $isActingAgent = OrderUtility::isActingAgent([
                    'orderable_id' => $orderItem->orderable_id,
                    'orderable_type' => $orderItem->orderable_type,
                    'acting_agent_id' => $user->id
                ]);
            }

            $hideIfRemovedActingAgent = ($isActingAgent and $isActingAgent->trashed()) ? 'hidden' : '';

            if ($completedOrInvoiced and $originatingAgent) {
                $_data['actions'] .= '<a class="order-details '
                    .$notActiveClass
                    .' '
                    .$hideIfOnsiteTraining
                    .'" href="'
                    .route('order.info', $order->id)
                    .'">View Entire Order</a>';
            }

            if($completedOrInvoiced and $isClassRegistration and !$originatingAgent) {
                $_data['actions'] .= '<a class="order-details '
                    .$hideIfRemovedActingAgent
                    .' '
                    .$notActiveClass
                    .'" href="'
                    .route('order.attendees.swap', ['order_item_id' => $orderItem->id])
                    .'">Swap Attendees</a>';
            }

            if($completedOrInvoiced and $isClassRegistration) {
                $_data['actions'] .= '<a class="order-details"'
                    .'href="'
                    . route('user.training-details', [$user->id, $orderItem->id])
                    .'">View Details</a>';
            } elseif($orderItem->orderable_type == \App\Utilities\Constant::ONSITE_TRAINING) {
                $_data['actions'] .= '<a class="order-details"'
                    .'href="'
                    .route('onsite.training-details', [ $orderItem->id ])
                    .'">View Details</a>';
            }

            if($isInvoiced and $originatingAgent) {
                $_data['actions'] .= '<a class="order-details" href="'
                    .route('order.invoice_info', ['order_id' => $order->id])
                    .'"target="_blank">View Invoice</a>';
            }

            if($order->status == \App\Utilities\Constant::COMPLETE and $hideIfOnsiteTraining == '' and $originatingAgent AND $order->total > 0) {
                $_data['actions'] .= '<a class="order-details" href="'
                    .route('payment.summary', ['order_id' => $order->id])
                    .'">View Payment</a>';
            }

            $data[] = $_data;

        }

        $r_data['draw'] = intval($request->input('draw'));
        $r_data['recordsTotal'] = count($data);
        $r_data['recordsFiltered'] = count($data);
        $r_data['data'] = $data;


        return response()->json($r_data);
    }
}
