<?php

namespace App\Http\Controllers\Main;

use App\Exceptions\FileNotFoundException;
use App\Models\FileEntry;
use App\Repositories\FileEntryRepository;
use Illuminate\Http\Response;

class FileEntryController extends BaseController
{
    /**
     * @var FileEntryRepository
     */
    private $fileEntryRepository;

    /**
     * FileEntryController constructor.
     * @param FileEntryRepository $fileEntryRepository
     */
    public function __construct(FileEntryRepository $fileEntryRepository)
    {
        $this->fileEntryRepository = $fileEntryRepository;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $file = FileEntry::findOrFail($id);
        if ($file->isEbook()) {
            $this->fileEntryRepository->deleteFileAndRelatedFiles($file);
        } else {
            $this->fileEntryRepository->deleteFileEntry($file);
        }
        return back()->with(['success_message' => 'File has been deleted.']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        // check if user has access

        $entry = FileEntry::findOrFail($id);
        $entity = $entry->imageable;

        try {
            $file = $this->fileEntryRepository->getFile($entity::FOLDERPATH, $entry->filename);
        } catch (\Exception $e) {
            return abort(404);
        }

        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime)
            ->header('Content-Disposition', 'attachment; filename="'.$entry->original_filename.'"');
    }

    public function showFile($filename)
    {
        // check if user has access -- 1yr subscription

        $entry = FileEntry::where('filename', $filename)->first();
        if (!$entry) {
            throw new FileNotFoundException($filename);
        }
        $entity = $entry->imageable;
        $file = $this->fileEntryRepository->getFile($entity::FOLDERPATH, $entry->filename);
        return (new Response($file, 200))
            ->header('Content-Type', $entry->mime)
            ->header('Content-Disposition', 'attachment; filename="'.$entry->original_filename.'"');
    }
}
