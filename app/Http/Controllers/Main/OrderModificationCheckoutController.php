<?php namespace App\Http\Controllers\Main;

use App\Models\StateTax;
use App\Repositories\StateRepository;
use Auth;
use Session;
use App\Facades\OrderModificationUtility;
use App\Repositories\OrderAdjustmentRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Services\OrderModificationCheckoutService;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Utilities\Constant;

class OrderModificationCheckoutController extends BaseController
{

    /**
     * @param Request $request
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepository
     * @param StateRepository $stateRepo
     * @param OrderModificationUtility $orderModificationUtility
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkout(Request $request,
                             OrderRepository $orderRepo,
                             OrderItemRepository $orderItemRepo,
                             OrderAdjustmentRepository $orderAdjustmentRepository,
                             StateRepository $stateRepo)
    {
        $order = $orderRepo->getById($request->route('order_id'));
        $data['items']  = $orderAdjustmentRepository->getByOrderId($order->id);
        $data['total'] = $data['items']->sum('amount');
        $data['user'] = $order->user;
        $data['order'] = $order;
        $data['page'] = 'checkout';
        $data['states']   = $stateRepo->getByList('state', 'state')->toArray();

        $subTotal = $data['total'];

        $diff = $order->total - $order->paid_total;
        $subTotal += $diff;

        $added = OrderModificationUtility::extractAddedTotal($data['items']);
        $deducted = OrderModificationUtility::extractDeductedTotal($data['items']);

        $data['sales_tax'] = ($added - $deducted == $diff)
            ? 0
            : abs($added - $deducted - $diff);

        $data['balance'] = OrderModificationUtility::extractSubTotal($subTotal, $data['total']);

        $currentYear = date('Y');
        $length = $currentYear + 29;
        $this->data['years'] = [];

        for ($ctr = 1; $ctr <= 12; $ctr++) {
            $months[$ctr] = date('F', mktime(0,0,0,$ctr, 1, date('Y')));
        }

        $data['months'] = $months;

        for ($currentYear; $length >= $currentYear; $currentYear++) {
            $data['years'][$currentYear] = $currentYear;
        }

        $data['ccOnly'] = ($order and $order->items)
            ? ($orderItemRepo->checkFromOrderItemsIfThereIsItemWithType(
                $order->items,
                Constant::COMPUTER_BASED))
                ? true
                : false
            : false;

        $data['counties']   = StateTax::lists('county', 'id');
        $data['counties']->prepend('Please choose county:', '');

        if(!empty(trim($order->shipping_county))) {
            $data['county'] = StateTax::getShippingCountyFromName($order->shipping_county);
        } else {
            $data['county'] = null;
        }

        $view = Auth::user()->isMember()
                ? view('main.completed.checkout.index', $data)
                : view('admin.completed-registrations.checkout.index', $data);

        return $view;
    }

    /**
     * @param Request $request
     * @param OrderModificationCheckoutService $paymentService
     * @return \Illuminate\Http\RedirectResponse|null
     */
    public function createCreditCardPayment(Request $request,
                                            OrderModificationCheckoutService $paymentService)
    {
        $data = $request->all();

        $redirect = null;
        $year   = $request->get('exp_year');
        $month  = $request->get('exp_month');
        $data['card_exp'] = $year . '-' . $month;
        $data['amount'] = $request->has('shipping_rates')? (float) $request->get('amount') + (float) $request->get('shipping_rates'):
            $request->get('amount');


        if($request->input('billing_custom', 0) == "0") {
            $data['billing_company'] = $request->input('billing_company_default');
            $data['billing_address1'] = $request->input('billing_address_default');
            $data['billing_city'] = $request->input('billing_city_default');
            $data['billing_state'] = $request->input('billing_state_default');
            $data['billing_zip'] = $request->input('billing_zip_default');
        }

        if($request->input('shipping_custom', 0) == "0") {
            $data['shipment_company'] = $request->input('shipment_company_default');
            $data['shipment_address1'] = $request->input('shipment_address_default');
            $data['shipment_city'] = $request->input('shipment_city_default');
            $data['shipment_state'] = $request->input('shipment_state_default');
            $data['shipment_zip'] = $request->input('shipment_zip_default');
        }

        $transaction = $paymentService->doCheckoutCreditCard($data);

        if ($transaction) {
            $redirect = Auth::user()->isMember()
                         ? redirect()->route('checkout.success')
                         : redirect()->route('admin.checkout.success', $request->get('user_id'));
        }

        if (Session::has('error_message')) {
            $redirect = redirect()->back()->with('error_message', Session::get('error_message'));
        }

        return $redirect;
    }

    /**
     * @param Request $request
     * @param OrderModificationCheckoutService $checkoutService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createInvoicePayment(Request $request,
                                        OrderModificationCheckoutService $checkoutService)
    {
        $data = $request->all();

        $userId = $request->get('user_id');
        $data['recipients'] = strlen(trim($request->get('other_recipients')))
            ? array_map('trim',
                explode(',',
                    $request->get('other_recipients')
                ))
            : null;

        if($request->input('billing_custom', 0) == "0") {
            $data['billing_company'] = $request->input('billing_company_default');
            $data['billing_address1'] = $request->input('billing_address_default');
            $data['billing_city'] = $request->input('billing_city_default');
            $data['billing_state'] = $request->input('billing_state_default');
            $data['billing_zip'] = $request->input('billing_zip_default');
        }

        if($request->input('shipping_custom', 0) == "0") {
            $data['shipment_company'] = $request->input('shipment_company_default');
            $data['shipment_address1'] = $request->input('shipment_address_default');
            $data['shipment_city'] = $request->input('shipment_city_default');
            $data['shipment_state'] = $request->input('shipment_state_default');
            $data['shipment_zip'] = $request->input('shipment_zip_default');
        }

        $result = $checkoutService->doCheckoutInvoice($data, true);

        if ($result) {
            $redirect = Auth::user()->isMember()
                    ? redirect()->route('checkout.success')
                    : redirect()->route('admin.checkout.success', [
                'user_id'   => $userId]);

            return $redirect;
        }
    }
}
