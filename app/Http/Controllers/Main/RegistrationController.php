<?php namespace App\Http\Controllers\Main;

use App\Events\EditOrderEvent;
use App\Events\RegistrationConfirmationEvent;
use App\Facades\OrderService;
use App\Facades\OrderUtility;
use App\Models\Order;
use App\Models\OrderTransaction;
use App\Models\PaymentTransaction;
use App\Models\ProductOrders;
use App\Models\User;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassSubscriptionDiscountRepository;
use Auth;
use Carbon\Carbon;
use Session;
use App\Repositories\UserRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\ClientCorporateSeatCreditRepository;
use App\Repositories\ClientYearlySubscriptionRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\CouponDiscountRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Repositories\ClassMilitaryDiscountRepository;
use App\Services\RegistrationService;
use App\Facades\DiscountService;
use App\Utilities\Constant;
use Illuminate\Http\Request;

class RegistrationController extends BaseController {

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @param UserRepository $userRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     */
    public function __construct(UserRepository $userRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepo)
    {
        $this->userRepo      = $userRepo;
        $this->orderRepo     = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
    }


    /**
     * Add a contact into cart after a valid request
     *
     * @param Request $request
     * @param RegistrationService $registrationService
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addContactsToClass(Request $request,
                                       RegistrationService $registrationService)
    {
        try {

            $userId = $request->get('user_id');
            $course = $request->get('course_id');
            $class = $request->get('class_id');
            $contacts = $request->get('contacts');

            $user = $userId
                    ? $this->userRepo->getById($userId)
                    : auth()->user();

            $registrableId   = $class == 0 ? $course : $class;
            $registrableType = $class == 0 ? Constant::COURSE_OBJECT : Constant::COURSE_CLASS_OBJECT;

            $registrationService->createRegistration([
                'agent'             => $user->id,
                'contacts'          => $contacts,
                'registrationId'    => $registrableId,
                'registrationType'  => $registrableType,
                'status'            => Constant::PENDING
            ]);

            if ($request->session()->has('coupon_code')) {
                $order = $this->orderRepo->getByUserId($user->id);
                DiscountService::applyCoupon([
                    'coupon_code'   => $request->session()->get('coupon_code'),
                    'order_id'      => $order->id]);
                $request->session()->forget('coupon_code');
//                return redirect()->route('user.shopping_cart');
            }

            $route = $userId
                ? route('admin.user.shopping_cart', ['user_id' => $userId])
                : route('user.shopping_cart');

            if ($userId) {
                $param = [
                    'user_id' => $userId,
                    'open_flag' => 0,
                    'save_flag' => 1
                ];

                event(new EditOrderEvent($param));
            }

            return $this->json(true, compact('route'));
        } catch(\Exception $e) {
            throw $e;
        }
    }


    /**
     * Render user cart
     *
     * @param Request $request
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassMilitaryDiscountRepository $militaryDiscountRepo
     * @param ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function cart(Request $request,
                         ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                         ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                         CouponDiscountRepository $couponDiscountRepo,
                         OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                         ClassMilitaryDiscountRepository $militaryDiscountRepo,
                         ClassSubscriptionDiscountRepository $classSubscriptionDiscountRepo,
                         ClassCreditDiscountRepository $classCreditDiscountRepo)
    {
        try {

            $this->data['page'] = 'user_cart';
            $userId = $request->route('user_id');
            $user = isset($userId)
                    ? $this->userRepo->getById($userId)
                    : Auth::user();

            $order = $this->orderRepo->getByUserId($user->id);
            $this->data['user']  = $user;
            $this->data['order'] = $order;
            $this->data['items'] = $order ? $order->getValidItems() : null;
            $this->data['total'] = $order ? $order->current_total : null;
            
            $this->data['subscriptionDiscounts'] = $order
                                                    ? $classSubscriptionDiscountRepo->getByOrderId($order->id)
                                                    : null;


            $this->data['combinationDiscounts'] = $order
                                                    ? $classCombinationDiscountRepo
                                                      ->getByOrderId($order->id)
                                                    : null;

            $this->data['halfPriceDiscounts']   = $order
                                                    ? $classHalfPriceDiscountRepo
                                                       ->getByOrderId($order->id)
                                                    : null;

            $this->data['creditDiscounts'] = $order
                                                ? $classCreditDiscountRepo->getByOrderId($order->id)
                                                : null;

            $this->data['couponDiscounts'] = $order
                                                ? ($couponDiscountRepo
                                                        ->getByOrderId($order->id)
                                                           ? $couponDiscountRepo
                                                                  ->getByOrderId($order->id)
                                                                  ->classDiscounts
                                                           : null)
                                                : null;

            $this->data['orderAdjustmentDiscounts'] = $order
                                                        ? $orderAdjustmentDiscountRepo
                                                            ->getByOrderId($order->id)
                                                        : null;

            $this->data['militaryDiscounts'] = $order
                                                    ? $militaryDiscountRepo->getByOrderId($order->id)
                                                    : null;

            $this->data['corpSeatCredits'] = $user->getCorporateSeatsCredit();

            $view = isset($userId)
                    ? view('admin.users.cart', $this->data)
                    : view('main.users.cart', $this->data);


            return $view;
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove item
     *
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function removeItem(Request $request)
    {
        try {

            $orderItemId = $request->get('id');
            $orderItem = $this->orderItemRepo->getById($orderItemId);
            $order_id = $orderItem->order->id;

            if ($orderItem->order->shipping_rates) {
                $new_total = $orderItem->order->total - $orderItem->order->shipping_rates;
                $orderItem->order->update(['shipping_rates' => null,
                            'total' => $new_total
                            ]);
            }

            $param = [
                'order_id'  => $order_id,
                'open_flag' => 0,
                'save_flag' => 1
            ];
            
            if(auth()->user()->id != $orderItem->order->user_id) {
                event(new EditOrderEvent($param));
            }

            $orderItem->forceDelete();           

            return $orderItem;

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Change item price
     *
     * @param Request $request
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo
     * @param ClientCorporateSeatCreditRepository $corporateSeatCreditRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function itemChange(Request $request,
                               ClassRegistrationRepository $classRegistrationRepo,
                               ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo,
                               ClientCorporateSeatCreditRepository $corporateSeatCreditRepo,
                               OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo)
    {
        try {

            $attribute     = '';
            $adjustableId = null;
            $adjustableType = null;
            $orderableId   = $request->get('orderable_id');
            $orderableType = $request->get('orderable_type');
            $price         = $request->get('price');

            $order = $this->orderRepo->getById($request->get('order'));

            switch ($orderableType) {

                case Constant::REGISTRATION_ORDER :
                    $item = $classRegistrationRepo->getById($orderableId);

                    $adjustableId = $item->id;
                    $adjustableType = Constant::CLASS_REGISTRATION;

                    $attribute = 'item_charge';
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $item = $clientYearlySubscriptionRepo->getById($orderableId);

                    $adjustableId = $item->id;
                    $adjustableType = Constant::CLIENT_SUBSCRIPTION;

                    $attribute = 'temporary_price';
                    break;

                case Constant::CORPORATE_SEAT :
                    $item = $corporateSeatCreditRepo->getById($orderableId);

                    $adjustableId = $item->id;
                    $adjustableType = Constant::CORPORATE_SEAT;

                    $attribute = 'price';
                    break;

                case Constant::PRODUCT_ORDERS :
                    $item = ProductOrders::findOrFail($orderableId);

                    $adjustableId = $item->id;
                    $adjustableType = Constant::BOOK;

                    $attribute = 'price';
                    break;

                default :
                    break;
            }

            if ($order) {

                $orderAdjustmentDiscount = $orderAdjustmentDiscountRepo
                                                ->getSingleByAdjustable([
                                                    'adjustable_id' => $adjustableId,
                                                    'adjustable_type' => $adjustableType,
                                                ]);

                if ($orderAdjustmentDiscount) {
                    $orderAdjustmentDiscount->delete();
                }

                DiscountService::createOrderAdjustmentDiscount(
                    $order, $item, $price
                );
            }

            $item->{$attribute} = $price;
            $item->save();

            Session::flash('success_message', 'Item price changed');

            $param = [
                'order_id'  => $order->id,
                'open_flag' => 0,
                'save_flag' => 1
            ];

            event(new EditOrderEvent($param));

            return $item;

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Request $request
     * @param RegistrationService $registrationService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function addContactsToClasses(Request $request, RegistrationService $registrationService)
    {
        $order = null;
        $registrations = json_decode($request->input('registration'));

        if (count($registrations) > 0) {
            foreach ($registrations as $registration) {
                $user = ($request->has('user_id')) ? User::findOrFail($request->input('user_id')) : auth()->user();
                try {

                    $registrableId = $registration->class_id;
                    $registrableType = Constant::COURSE_CLASS_OBJECT;

                    $order = $registrationService->createRegistration([
                        'agent' => $user->id,
                        'contacts' => $registration->contacts,
                        'registrationId' => $registrableId,
                        'registrationType' => $registrableType,
                        'status' => Constant::PENDING
                    ]);
                } catch (\Exception $e) {
                    throw $e;
                }
            }

            if (!is_null($order)) {
                if (Session::has('coupon_code')) {
                    DiscountService::applyCoupon([
                        'coupon_code' => Session::get('coupon_code'),
                        'order_id' => $order->id]);
                    Session::forget('coupon_code');
                }

            }
        }

        if(!$request->has('user_id'))
            return redirect()->route('user.shopping_cart');

        $param = [
            'user_id'  => $user->id,
            'open_flag' => 0,
            'save_flag' => 1
        ];

        event(new EditOrderEvent($param));
        return redirect()->route('admin.user.shopping_cart', ['user_id' => $user->id]);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function completeOrder(Request $request)
    {
        $notify = $request->has('doNotEmail') ? false : true;

        $orderId = $request->get('order_id');
        $userId = $request->get('user_id');

        $order = Order::find($orderId);
        $user = $this->userRepo->getById($userId);

        $order->completed_at = Carbon::now();
        $order->status = Constant::COMPLETE;
        $order->shipping_rates = 0;
        $order->save();

        $order_items = $order->items;

        if ($order_items->count()) {
            OrderService::updateStatusOfOrders($order_items, Constant::COMPLETE);
            OrderService::checkIfSubscriptionUsed($order->user, $order_items, $notify);
        }

        PaymentTransaction::create([
            'email'             => $user->email,
            'card_brand'        => 'N/A',
            'auth_code'         => 'N/A',
            'transaction_id'    => 'N/A',
            'masked_cc'         => 'N/A',
            'amount'            => 0,
            'shipping_rates'    => 0,
            'status'            => true,
            'order_id'          => $order->id,
            'items'             => OrderUtility::createPaymentItems($order->items,  0),
            'billing_company'           => $user->contact->company,
            'billing_address'           => $user->contact->user_billing_address,
            'billing_city'              => $user->contact->user_billing_city,
            'billing_state'             => $user->contact->user_billing_state,
            'billing_zip'               => $user->contact->user_billing_zip,
            'shipping_company'          => $user->contact->company,
            'shipping_address'          => $user->contact->user_shipping_address,
            'shipping_city'             => $user->contact->user_shipping_city,
            'shipping_state'            => $user->contact->user_shipping_state,
            'shipping_zip'              => $user->contact->user_shipping_zip,
            'trans_total'               => $order->total,
        ]);

        OrderTransaction::create([
            'order_id'          => $order->id,
            'transaction_id'    => generate_transaction_id($order)
        ]);

        $request->session()->flash('success_message', 'Order has been completed.');

        event(new RegistrationConfirmationEvent($order->id, null, null, [], $notify));

        $route = isset($userId)
                 ? route('admin.user_details', ['contact_id' => $user->contact->id, 'user_id' => $user->id])
                 : route('user.home');

        return $route;
    }
}