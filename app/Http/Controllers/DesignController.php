<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\State;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use App\Repositories\CourseTypeRepository;
use App\Repositories\ClassRepository;
use App\Repositories\UserRepository;
use App\Services\RegistrationService;
use App\Services\PaymentService;
use App\Utilities\Constant;

class DesignController extends Main\BaseController
{
    
    /**
     * MainController constructor
     */
    public function __construct(
        ClassRegistrationRepository $registrationRepo,
        CourseRepository $courseRepo,
        CategoryRepository $categoryRepo,
        CourseTypeRepository $courseTypeRepo,
        PaymentService $paymentService,
        UserRepository $userRepo,
        ClassRepository $classRepo,
        RegistrationService $classRegistrationService)
    {

        $this->registrationRepo = $registrationRepo;
        $this->courseRepo       = $courseRepo;
        $this->categoryRepo     = $categoryRepo;
        $this->courseTypeRepo    = $courseTypeRepo;
        
        $this->userRepo = $userRepo;
        $this->classRepo = $classRepo;
        $this->classRegistrationService = $classRegistrationService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, State $states)
    {
/*
        $this->data['cart']['registrations'] = $this->registrationRepo
                                            ->getByAttendee($user);
*/
        //login
        $data = [
            "state_list" => $states->distinct()->get(),
        ];
        // courses
        $data['categoryFilter'] = $request->get('category');
        $data['typeFilter'] = $request->get('type');
        $data['courseTypes'] = $this->courseTypeRepo->byType();
        $data['courseCategories'] = $this->categoryRepo->getAll();
        $data['categories'] = !$data['categoryFilter'] ? $this->categoryRepo->getAll()
            : [ $this->categoryRepo->getById($data['categoryFilter']) ];

        $data['cbts'] = $this->courseRepo->getAllComputerBased() ?: null;

        if ($data['typeFilter'] == Constant::COMPUTER_BASED) {
            $data['categories'] = null;
        }

        
        //classes
        $data['registrations'] = $this->registrationRepo->getByAttendee(25);
        
        
        // cart
        /*
        $this->setCartData($request);
        $data = array_merge($data, $this->data);
        */

        return view('design', $data);
    }
    
    
    /**
     * Set cart data
     *
     * @param Request $request
     */
    public function setCartData($request, $agent_id = '')
    {
        try {

            $user = $this->userRepo->getById(7);

            $price = 0;
            $total_amount = 0;
            $attendees = [];
            $this->data['cart']['registrations'] = [];
            $this->data['cart']['associated_courses'] = [];
            $this->data['cart']['expired_registrations'] = [];
            $registrations = $this->registrationRepo->getByAgent($user->id);
            
            if ($registrations) {
                $previous_course_id = 0;
                $previous_class_id = 0;
                //$current_time = strtotime( \Carbon\Carbon::now() );
                $current_time = strtotime( date('Y-m-d') );

                foreach ($registrations as $key => $registration) {
                    $name = $registration->contact->first_name . ' ' . $registration->contact->last_name;

                    if($registration->registrable_id != $previous_course_id ||
                        $registration->registrable_id != $previous_class_id
                    ) {
                        $price = 0;
                        $attendees = [];
                        $previous_course_id = $registration->registrable_id;
                        $previous_class_id = $registration->registrable_id;
                    }

                    array_push($attendees, $name);

                    $temp_price = $registration->isCourse()
                              ? $registration->registrable->price
                              : $registration->registrable->course->price;

                    $temp_total_amount = $registration->isCourse()
                                     ? $registration->registrable->price
                                     : $registration->registrable->course->price;

                    $price_per_registration  = $registration->isCourse()
                                                     ? $registration->registrable->price
                                                     : $registration->registrable->course->price;

                    $agent     = $registration->originating_agent_user_id;

                    $title     = $registration->isCourse()
                                        ? $registration->registrable->title
                                        : $registration->registrable->course->title;

                    $registrable_id = $registration->registrable_id;

                    $date      =  date('F d, Y', strtotime($registration->registrable->start_date)) . " - " . date('F d, Y', strtotime($registration->registrable->end_date));

                    $address   = $registration->isCourse()
                                        ? ''
                                        : $registration->registrable->city . ", " . $registration->registrable->state;

                    $registration_start_date = strtotime(date('Y-m-d',strtotime($registration->registrable->start_date)));

                    if($registration_start_date > $current_time) {
                        //dd($registration->courseClass->start_date);
                        $this->data['cart']['expired_registrations_agent'] = $registration->originating_agent_user_id;

                        $this->data['cart']['expired_registrations'][$registration->registrable_id] = [
                            'registration'  => $registration,
                            'registrable_id' => $registration->registrable_id,
                            'price_per_registration' => $price_per_registration,
                            'agent' => $agent,
                            'title' => $title,
                            'registrable_id'=>$registrable_id,
                            'course_id' => $registration->isCourse()
                                           ? $registration->registrable->id
                                           : $registration->registrable->course->id,
                            'date' => $date,
                            'address' => $address,
                            'attendees' => $attendees,
                            'price' => $temp_price
                        ];
                    }
                    else {

                        $price += $temp_price;
                        $total_amount += $temp_total_amount;

                        $this->data['cart']['registrations'][$registration->registrable_id] = [
                            'registration'  => $registration,
                            'registrable_id' => $registration->registrable_id,
                            'price_per_registration' => $price_per_registration,
                            'agent' => $agent,
                            'title' => $title,
                            'course_id' => $registration->isCourse()
                                ? $registration->registrable->id
                                : $registration->registrable->course->id,
                            'date' => $date,
                            'address' => $address,
                            'attendees' => $attendees,
                            'price' => $price //+= $registration->courseClass->course->price
                        ];
                    }

                    $this->data['cart']['associated_courses'][$registration->isCourse() ? $registration->registrable->course_type_id
                                                     : $registration->registrable->course->course_type_id]
                                                     = $this->courseRepo
                                                            ->AssociatedCourses(
                                                                $registration->isCourse()
                                                                ? $registration->registrable->course_type_id
                                                                : $registration->registrable->course->course_type_id
                                                            );

                }
            }

            $this->data['cart']['total_amount'] = $total_amount;
        } catch(\Exception $e) {
            throw $e;
        }

        if(!empty($agent_id))
        {
            return $this->data;
        }
    }
}
