<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Auth\Access\UnauthorizedException;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $error   = '';
        $success = '';

        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'));

        switch ($response) {
            case Password::RESET_LINK_SENT:
                $success =  trans($response);
                break;

            case Password::INVALID_USER:
                $error = trans($response);
                break;

            case 'passwords.deactivated_user':
                $error = trans($response);
                break;
        }

        return [
            'error'   => $error,
            'message' => $success
        ];
    }

    /**
     * @param null $token
     * @return $this
     * @throws NotFoundHttpException
     */
    public function getReset($token = null)
    {
        $data['page'] = 'reset_password';

        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('auth.reset', $data)->with('token', $token);
    }
    
    /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function postReset(Request $request)
   {
       $this->validate($request, [
           'token' => 'required',
           'email' => 'required|email',
           'password' => 'required|confirmed',
       ]);

       $credentials = $request->only(
           'email', 'password', 'password_confirmation', 'token'
       );

       try {

           $response = Password::reset($credentials, function ($user, $password) {

               if (!$user->isActive()) {
                   throw new UnauthorizedException('User is Deactivated');
               }

               $user->password = bcrypt($password);
               $user->first_login = false;
               $user->save();

               Auth::login($user);
           });
       } catch (\Exception $e) {
           return redirect()->back()
               ->withInput($request->only('email'))
               ->withErrors(['email' => $e->getMessage()]);
       }

       switch ($response) {
           case Password::PASSWORD_RESET:
               return redirect($this->redirectPath());

           default:
               return redirect()->back()
                           ->withInput($request->only('email'))
                           ->withErrors(['email' => trans($response)]);
       }
   }
}
