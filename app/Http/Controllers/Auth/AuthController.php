<?php namespace App\Http\Controllers\Auth;

use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Config;
use App\Events\CustomerRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\State;
use App\Models\User;
use App\Models\ZipCode;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Services\Mailer\RegistrationMailer;
use App\Utilities\Constant;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Mail;
use ReCaptcha\ReCaptcha;
use Session;
use Validator;
use Event;
use Carbon\Carbon;
use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roleRepo;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var RegistrationMailer
     */
    protected $mailer;

    protected $checkpointsStatus = true;
    /**
     * @var EmailTemplateMailer
     */
    private $emailer;

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     */
    public function __construct(Guard $auth,
                                RoleRepository $roleRepo,
                                UserRepository $userRepo,
                                RegistrationMailer $mailer)
    {
        $this->auth     = $auth;
        $this->roleRepo = $roleRepo;
        $this->userRepo = $userRepo;
        $this->mailer = $mailer;
//        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogout(Request $request)
    {
        $request->session()->flush();
        $this->auth->logout();
        Session::forget('last_activity_url');
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister(State $states, ZipCode $zip_codes)
    {
        $data['page'] = 'register';
        $data['state_list'] = $states->distinct()->get();

        return view('auth.register', $data);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegister(Request $request)
    {
        /*
        $request['gresponse'] = $this->validateRecaptcha($request->get('g-recaptcha-response'),
                                                         $request->ip());
        */
        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        Auth::login($user);

        return redirect('courses');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function  getLogin(Request $request)
    {
        $data['page'] = 'login';

        if(!session()->has('from')){
            session()->put('from',URL::previous());
        }

        return view('auth.login', $data);
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $error = '';

        $error = $this->checkIfFirstTime($request);

        if(0 != strlen($error)) {
            return redirect('/login')
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => $error,
                ]);
        }

        if ($this->auth->attempt($credentials, $request->has('remember')))
        {
            $error = $this->checkpointAccounStatus($this->auth->user());

            if (0 == strlen($error) && $this->checkpointsStatus) {

                $default = (has_role([Constant::ROLE_CSR, Constant::ROLE_ADMIN]))
                    ? '/admin'
                    : '/home';

                return redirect()->intended($default);

            }

            $this->auth->logout();

        }

        if (0 == strlen($error)) {
            $error = $this->getFailedLoginMessage();
        }

        return redirect('/login')
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                'email' => $error,
            ]);
    }

    public function checkIfFirstTime($request)
    {
        $error = '';
        $user = User::where('email', $request->input('email'))->first();
        if ( $user && $user->first_login ) {

            $response = Password::sendResetLink($request->only('email'), function ($data) {
                $data['subject'] = 'First Login - Password Setup Request';
                $data['data']['greeting'] = "To setup your new password, kindly click on the link below.";
                return $data;
            });

            switch ($response) {
                case Password::RESET_LINK_SENT:
                    $error = "Welcome! Since this is your first time logging in the new system, we've sent an email that will allow you to <u>setup a new password</u>.";
                    break;

                case Password::INVALID_USER:
                    $error = '';
                    break;
            }

        }
        return $error;
    }

    public function checkpointAccounStatus(User $user)
    {
        $error = '';
        if ( !$user->isActive() ) {
            $this->checkpointsStatus = false;
            $error = 'Sorry but your account is disabled. Click ';
            $error .= "<a style='color:blue; text-decoration:underline' class='btn btn-link inline btn-reactivate-account' href='#'>Here</a>";
            $error .= ' to request activation of account.';
        }
        return $error;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'g-recaptcha-response.required' => 'The Recaptcha field is required.',
            're_password.required'   => 'The confirm password field is required.'
        ];

        $validation = [
            'email'         => 'required|email|max:255|unique:users',
            'password'      => 'required|min:6|max:50|same:re_password',
            're_password'   => 'required|min:6|max:50',
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'address'       => 'required|max:255',
            'city'          => 'required|max:255',
            'state'         => 'required|max:255|exists:states,state',
            'zip_code'      => 'required|min:5|exists:zip_codes,zip_code',
            'phone_number'  => 'required',
            'fax_number'    => 'min:10',
            'company'       => 'required|max:255'
        ];

        $is_remote = in_array($_SERVER['SERVER_NAME'], ['ercweb.com', 'ercweb.sitegage.com']);
        if (Session::has('TEST-TOKEN')) {
            $is_remote = false;
        }
        
        if ($is_remote) {
            $validation = array_merge($validation, [
                'g-recaptcha-response' => 'required'
           ]);
        }
        return Validator::make($data, $validation, $messages);
    }

    /**
     * @param $gresponse
     * @param $ip
     * @return bool
     */
    protected function validateRecaptcha($gresponse, $ip)
    {
        $recaptcha = new ReCaptcha(env('GOOGLE_RECAPTCHA_SITE_SECRET'));
        $response = $recaptcha->verify($gresponse, $ip);

        return $response->isSuccess();
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = null;

        $contact = new Contact([
            'first_name'	=> $data['first_name'],
            'last_name'	    => $data['last_name'],
            'company'	    => $data['company'],
            'title'		    => $data['title'],
            'address1'	    => $data['address'],
            'address2'	    => $data['address2'],
            'city'		    => $data['city'],
            'state'		    => $data['state'],
            'zip'		    => $data['zip_code'],
            'phone'		    => $data['phone_number'],
            'phone_local'		    => $data['phone_local'],
        ]);

        $contact->save();

        // Assign `ROLE_MEMBER` to user account
        $role = $this->roleRepo->findOne(['role' => Constant::ROLE_MEMBER]);

        $user = User::create([
            'contact_id'   => $contact->id,
            'email'        => $data['email'],
            'password'     => bcrypt($data['password']),
            'date_created' => Carbon::now(),
        ]);
        $user->assignRole($role);

        Event::fire(new CustomerRegisteredEvent($user, $data['password']));

        return $user;
    }

    /**
     * Get a validator for an incoming verification request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validatorVerify(array $data)
    {
        return Validator::make($data, [
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Set password of a user instance after filling verification form.
     *
     * @param  array $data
     * @return User
     */
    public function setPassword(array $data)
    {
        return User::where('verification_token', '=', $data['verification_token'])
            ->get()
            ->first()
            ->update(['password_hash' => bcrypt($data['password']), 'state' => 1, 'date_activated' => date('Y-m-d H:i:s')]);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function sessionExpired(Request $request)
    {
        $authenticated = true;
        $route = '';

        if (!session('lastActivityTime')) {
            $authenticated = false;
            $route = route('get.login');

            $this->auth->logout();

            $url = Session::has('last_activity_url')
                    ? Session::get('last_activity_url')
                    : $request->get('current_location');

            Session::put('last_activity_url', $url);
        }

        else {
            Session::forget('last_activity_url');
        }

        return response()->json([
            'authenticated' => $authenticated,
            'route' => $route
        ]);
    }
}
