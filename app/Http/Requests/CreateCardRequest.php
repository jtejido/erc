<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateCardRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              "order_id" => "required",
              "amount" => "required",
              "user_id" => "required",
              "first_name" => "required",
              "last_name" => "required",
              "email" => "required|email",
              "card_number" => "required",
              "cvv" => "required",
              "exp_month" => "required",
              "exp_year" => "required",
              "billing_company_default" => "required_if:billing_custom,0",
              "billing_address_default" => "required_if:billing_custom,0",
              "billing_state_default" => "required_if:billing_custom,0",
              "billing_city_default" => "required_if:billing_custom,0",
              "billing_zip_default" => "required_if:billing_custom,0",
              "billing_custom" => "required|in:0,1",
              "billing_company" => "required_if:billing_custom,1",
              "billing_address1" => "required_if:billing_custom,1",
              "billing_city" => "required_if:billing_custom,1",
              "billing_state" => "required_if:billing_custom,1",
              "billing_zip" => "required_if:billing_custom,1",
              "shipping_rates" => "",
              "shipment_company_default" => "required_if:shipping_custom,0",
              "shipment_address_default" => "required_if:shipping_custom,0",
              "shipment_state_default" => "required_if:shipping_custom,0",
              "shipment_city_default" => "required_if:shipping_custom,0",
              "shipment_zip_default" => "required_if:shipping_custom,0",
              "shipping_custom" => "required|in:0,1",
              "shipment_company" => "required_if:shipping_custom,1",
              "shipment_address1" => "required_if:shipping_custom,1",
              "shipment_city" => "required_if:shipping_custom,1",
              "shipment_state" => "required_if:shipping_custom,1",
              "shipment_zip" => "required_if:shipping_custom,1",
        ];
    }
}
