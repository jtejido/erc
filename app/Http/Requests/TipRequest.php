<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;
use Auth;

class TipRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasRole(Constant::ROLE_ADMIN) or
            auth()->user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'title'          => 'required',
            'content'        => 'required',
            'photo'          => 'mimes:jpeg,jpg,bmp,png,gif|max:10240',
            'category_id'    => 'required|exists:categories,id',
            'is_published'   => 'required|boolean',
            'slug' => 'unique:tips,slug,'.$this->id
        ];

    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'photo.max' => 'The photo may not be greater than 10MB.'
        ];
    }
}
