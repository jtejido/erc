<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;

class FileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
                return [
                    'title'      => 'required'
                ];
                break;
            default:
                return [
                    'title'     => 'required',
                    'file'      => 'required|mimes:pdf,jpeg,bmp,png|max:10000',
                    'book_id'   => 'required'
                ];
        }

    }
}
