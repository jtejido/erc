<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Contact;

class EditPersonalInfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $contact = Contact::find($this->get('contact_id'));

        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'address1'      => 'required',
            'city'          => 'required',
            'state'         => 'required|exists:states,state',
            'zip'           => 'required|exists:zip_codes,zip_code',
            'phone'         => 'required'
        ];
    }
}
