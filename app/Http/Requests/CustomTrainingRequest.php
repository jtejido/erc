<?php namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;
use App\Utilities\Constant;

class CustomTrainingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|max:90',
            'company'               => 'required|max:90',
            'email'                 => 'required|email',
            'phone_number'          => 'required|integer',
            'training_requested'    => 'required',
            'number_students'       => 'integer',
            'for_call'              => 'string|max:3|min:3',
        ];
    }
}
