<?php namespace App\Http\Requests;

use Auth;
use App\Models\User;

class EditEmailRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->get('user_id'));
        $hashedPassword = $user->password;

        return [
            'email'     => 'required|email|unique:users,email,'.$this->get('user_id'),
            'password'  => 'required|password_check:'.$hashedPassword
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'password.required'         => 'Old password is required.',
            'password.password_check'   => 'Incorrect password.'
        ];
    }
}