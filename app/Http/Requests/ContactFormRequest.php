<?php namespace App\Http\Requests;

use App\Repositories\ContactRepository;

class ContactFormRequest extends Request
{

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @param ContactRepository $contactRepo
     */
    public function __construct(ContactRepository $contactRepo)
    {
        $this->contactRepo = $contactRepo;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $matchUser = $this->get('match_user_id');
        $contact   = $this->contactRepo->getById($this->get('id'));

        $id = (strlen($this->get('email')) &&
              $contact &&
              $contact->user)
              ? $contact->user->id
              : 0;

        $id = isset($matchUser) ? $matchUser : $id;
 
        return [
            'email'			  => 'email|max:255|unique_without:'. isset($matchUser) .',users,email,'.$id,
            'match_user_id'   => 'numeric',
            'first_name'      => 'required_without:match_user_id|max:255',
            'last_name'		  => 'required_without:match_user_id|max:255',
            'address1'		  => 'required_without:match_user_id|min:3',
            'city'			  => 'required_without:match_user_id|max:255',
            'state'			  => 'required_without:match_user_id|max:255|exists:states,state',
            //'zip'		      => 'required|min:5|exists:zip_codes,zip_code',
            'zip'		      => 'required_without:match_user_id|min:5',
            'fax'	          => 'min:10',
        ];
    }

    /**
     * Set custom messages for validator errors
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique_without'          => 'The email is already taken.',
            'first_name.required_without'   => 'The first name is required',
            'last_name.required_without'    => 'The last name is required',
            'address1.required_without'     => 'Primary address is required.',
            'city.required_without'         => 'City is required.',
            'state.required_without'        => 'State is required',
            'zip.required_without'          => 'Zip is required'
        ];
    }
}
