<?php namespace App\Http\Requests;

use Auth;
use App\Utilities\Constant;

class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasRole(Constant::ROLE_ADMIN) or
            auth()->user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int) $this->id;
        switch($this->method()) {
            case 'PUT':
                return [
                    'title' => 'max:100|required',
                    'description' => 'required',
                    'sku' => 'required|unique:courses,sku,' . $id,
                    'price' => 'required|numeric',
                    'duration' => 'required',
                    'slug' => 'required|unique:courses,slug,' . $id
                ];
                break;
            default:
                return [
                    'title' => 'required|unique:courses,title|max:100',
                    'description' => 'required',
                    'sku' => 'required|unique:courses,sku',
                    'price' => 'required|numeric',
                    'duration' => 'required',
                    'slug' => 'required|unique:courses,slug'
                ];
        }
    }
}
