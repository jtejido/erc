<?php namespace App\Http\Requests;

use Auth;
use App\Models\User;

class AddBookToCartRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_id'      => 'required',
            'quantity'     => 'required|min:1|numeric'
        ];
    }
}