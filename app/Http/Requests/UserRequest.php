<?php namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;
use App\Utilities\Constant;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::user()->hasRole(Constant::ROLE_ADMIN) or
               Auth::user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'    => 'required|email|unique:users,email,'.$this->get('id'),
            'role'     => 'required|exists:roles,id',
            'password' => 'required_if:role, [1, 3]'
        ];
    }
}
