<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;

class BookRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
                return [
                    'code'    => 'required|unique:books,code,'.$this->books,
                    'name'      => 'required',
                    'thumbnail'      => 'image|max:1500',
                    'slug'      => 'required|unique:books,slug,'.$this->books,
                ];
            break;
            default:
                return [
                    'code'    => 'required|unique:books',
                    'name'      => 'required',
                    'thumbnail'      => 'image|max:1500',
                    'slug'      => 'required|unique:books,slug'
                ];
        }

    }
}
