<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;
use Auth;

class LinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasRole(Constant::ROLE_ADMIN) or
            auth()->user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required',
            'url'         => 'required',
            'category_id' => 'required|exists:helpful_links_categories,id',
        ];
    }
}
