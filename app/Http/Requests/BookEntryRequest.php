<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;

class BookEntryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
                return [
                ];
                break;
            default:
                $fileRule = 'mimes:jpeg,bmp,png,pdf,docx,doc|max:15000';
                $rules = [
                    'file'      => $fileRule,
                    'item_id'   => 'required',
                    'file_id'   => 'required'
                ];

                return $rules;
        }

    }
}
