<?php namespace App\Http\Requests;


class CouponLandingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()) {
            case 'POST':
                return [
                    'title'     => 'required',
                    'slug'      => 'without_spaces|required|unique:coupon_landings,slug'
                ];
                break;
            case 'PUT':
                return [
                    'title'     => 'required',
                    'slug'      => 'required|without_spaces'
                ];
                break;
        }


    }

    public function messages()
    {
        return [
            'without_spaces'    => 'The :attribute should not have any spaces in between.'
        ];
    }
}