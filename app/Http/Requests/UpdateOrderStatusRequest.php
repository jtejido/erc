<?php
/**
 * Created by PhpStorm.
 * User: greghermo
 * Date: 21/08/2017
 * Time: 5:12 PM
 */

namespace App\Http\Requests;


use App\Utilities\Constant;

class UpdateOrderStatusRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasRole(Constant::ROLE_ADMIN) or
            auth()->user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if($this->has('update_status_only')) {
            return [
                'order_id'  =>  'required|integer',
            ];
        } else {
            return [
                'amount'    => 'required|between:0,99999999.99',
                'order_id'  =>  'required|integer',
            ];
        }

    }
}