<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;

class LocationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->user()->hasRole(Constant::ROLE_ADMIN) or
            auth()->user()->hasRole(Constant::ROLE_CSR));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
        ];
    }
}
