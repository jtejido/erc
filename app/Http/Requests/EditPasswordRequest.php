<?php namespace App\Http\Requests;

use Auth;
use App\Models\User;

class EditPasswordRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->get('user_id');
        $user   = User::find($userId);
        $hashedPassword = $user->password;

        return [
            'old_password'    => 'required|password_check:'.$hashedPassword,
            'new_password'    => 'required|unique:users,password,'.$userId.'|new_password_check:'.$hashedPassword,
            're_new_password' => 'required|same:new_password'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            're_new_password.same'              => 'New passwords must match.',
            'old_password.password_check'       => 'Incorrect old password.',
            'new_password.new_password_check'   => 'New and old password are the same.'
        ];
    }
}