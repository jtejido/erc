<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Utilities\Constant;

class ClassRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date'    => 'required|date_format:"F d, Y"',
            'end_date'      => 'required|date_format:"F d, Y"',
            'start_time'    => 'required',
            'end_time'      => 'required',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'address.required_if' => 'Address is required if class is a Seminar',
            'city.required_if'    => 'City is required if class is a Seminar',
            'state.required_if'   => 'State is required if class is a Seminar',
            'zip.required'        => 'Zip is required if class is a Seminar',
        ];
    }
}
