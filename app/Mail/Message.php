<?php namespace App\Mail;

use App;

class Message
{

    private $to;
    private $cc = [];
    private $bcc = [];
    private $subject;
    private $raw;
    private $view;
    private $data = [];
    private $attachments = [];

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachments;
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    public function __construct()
    {

    }

    /**
     * Set recipient
     *
     * @param   mixed   $to
     * @return  App\Mail\Message
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return  mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set cc recipients
     *
     * @param   array $cc
     * @return  App\Mail\Message
     */
    public function setCc(array $cc)
    {
        $this->cc = App::environment() == 'production' ? $cc : ['erc@dev.com'];

        return $this;
    }

    public function addCc($cc) {
        $this->cc[] = $cc;

        return $this;
    }

    /**
     * Get cc recipients
     *
     * @return array
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set bcc recipients
     *
     * @param   array $bcc
     * @return  App\Mail\Message
     */
    public function setBcc(array $bcc)
    {
        $this->bcc = App::environment() == 'production' ? $bcc : ['erc@dev.com'];

        return $this;
    }

    public function addBcc($bcc) {
        $this->bcc[] = $bcc;
        
        return $this;
    }

    /**
     * Get bcc recipients
     *
     * @return array
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set subject
     *
     * @param   string  $subject
     * @return  App\Mail\Message
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return  string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set raw
     *
     * @param   string  $raw
     * @return  App\Mail\Message
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;

        return $this;
    }

    /**
     * Get raw
     *
     * @return  string
     */
    public function getRaw()
    {
        return $this->raw;
    }

    /**
     * Set view/template
     *
     * @param   string  $view
     * @return  App\Mail\Message
     */
    public function setView($view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view/template
     *
     * @return  string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Set data to be passed to view/template
     *
     * @param   array   $viewData
     * @return  App\Mail\Message
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Set view data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}