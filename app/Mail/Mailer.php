<?php namespace App\Mail;

use App;
use Mail;
use App\Log\FileLogger;
use Session;

class Mailer
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Mailer constructor
     *
     * @param FileLogger $logger
     */
    public function __construct(FileLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Send templated email content
     *
     * @param   Message $message
     * @param bool $queue
     * @return int
     * @throws \Swift_SwiftException
     */
    public function send(Message $message, $queue = false)
    {

        $catch_flag = env('CATCH_ALL', false);
        $catch_all_email = env('CATCH_ALL_EMAIL', 'service@ercweb.com');
        $allowed_domain = env('CATCH_ALL_ALLOWED_DOMAIN', 'environmentalresourcecenter.com,ercweb.com');

        try {

            if($catch_flag) {
                $to_domain = getDomainFromEmail($message->getTo());

                if(strpos($allowed_domain, $to_domain) === false) {
                    $message->setTo($catch_all_email);
                }

            }

        } catch (\Exception $e) {
            $this->logError($message, $e);
            return 0;
        }

        try {
            $func = $queue === true ? 'queue' : 'send';

            if ($func === 'send' and (env('QUEUE_DRIVER') === 'redis')) {
                Mail::later(8, ['html'=>$message->getView()], $message->getData(), function($m) use ($message)
                {
                    $m
                        ->to($message->getTo())
                        ->subject($message->getSubject());

                    if (!is_null($message->getCc())) {
                        $m->cc($message->getCc());
                    }

                    if (!is_null($message->getBcc())) {
                        $m->bcc($message->getBcc());
                    }

                    if (!is_null($message->getAttachment())) {
                        foreach ($message->getAttachment() as $a)
                        $m->attach($a['path'], [
                            'as'    => $a['filename'],
                            'mime'  => $a['mime']
                        ]);
                    }
                });
                return true;
            } else {
                $sent = Mail::$func(['html'=>$message->getView()], $message->getData(), function($m) use ($message)
                {
                    $m
                        ->to($message->getTo())
                        ->subject($message->getSubject());

                    if (!is_null($message->getCc())) {
                        $m->cc($message->getCc());
                    }

                    if (!is_null($message->getBcc())) {
                        $m->bcc($message->getBcc());
                    }
                    if (!is_null($message->getAttachment())) {
                        foreach ($message->getAttachment() as $a)
                        $m->attach($a['path'], [
                            'as'    => $a['filename'],
                            'mime'  => $a['mime']
                        ]);
                    }
                });
            }

            return Mail::isPretending() ? 1 : $sent;
        }
        catch (\Swift_SwiftException $e) {
            $this->logError($message, $e);

            throw $e;
        }
        catch (\Exception $e) {
            $this->logError($message, $e);

            return 0;
        }
    }

    /**
     * Send raw email content
     *
     * @param   Message   $message
     * @throws \Swift_SwiftException
     * @return  int
     */
    public function sendRaw(Message $message)
    {
        try {
            $sent = Mail::raw($message->getRaw(), function($m) use ($message)
            {
                $m
                    ->to($message->getTo())
                    ->subject($message->getSubject());

                if (!is_null($message->getCc())) {
                    $m->cc($message->getCc());
                }
            });

            return Mail::isPretending() ? 1 : $sent;
        }
        catch (\Swift_SwiftException $e) {
            $this->logError($message, $e);

            throw $e;
        }
        catch (\Exception $e) {
            $this->logError($message, $e);

            return 0;
        }
    }

    /**
     * Logs error to file
     *
     * @param Message $message
     * @param \Exception $e
     */
    private function logError(Message $message, \Exception $e)
    {
        $logBody =  'Mail for "' . print_r($message->getTo(), true) . '" with subject "' . $message->getSubject() . '" failed';
        $logError = $e->getMessage();

        $this->logger->addError("$logBody [$logError]", 'mailer');
    }
}