<?php namespace App\Utilities;

use App\Models\ClassRegistration;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassRegistrationRepository;
use Auth;
use App\Models\RegistrationOrder;
use App\Models\Order;
use App\Repositories\OrderAdjustmentRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class OrderModificationUtility
{

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @var ClassCombinationRepository
     */
    private $classCombinationRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     */
    public function __construct(OrderAdjustmentRepository $orderAdjustmentRepo,
                                ClassCombinationRepository $classCombinationRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
        $this->classCombinationRepo = $classCombinationRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * @param RegistrationOrder $registrationOrder
     * @return bool
     */
    public function cancellationWithinRestriction(RegistrationOrder $registrationOrder)
    {
        $boolean = false;
        $today = Carbon::today();
        $classStartDate = $registrationOrder->registrable->start_date;
        $difference = $today->diffInDays($classStartDate, false);

        if ($difference >= 0 and $difference <= 7) {
            $boolean = true;
        }

        return $boolean;
    }

    /**
     * @param $orderAdjustments
     * @param $total
     * @param $checkout
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extractOrderAdjustment($orderAdjustments, $total, $checkout = false)
    {
        $subTotal = $total;

        $orderId = $orderAdjustments->pluck('order_id')->first();
        $order = Order::find($orderId);

        $diff = $order->total - $order->paid_total;
        $subTotal += $diff;

        $added = self::extractAddedTotal($orderAdjustments);
        $deducted = self::extractDeductedTotal($orderAdjustments);

        $sales_tax = ($added - $deducted == $diff)
            ? 0
            : abs($added - $deducted - $diff);

        $refund = self::extractRefund($subTotal, $total);
        $subTotal = self::extractSubTotal($subTotal, $total);
        $view = $checkout
            ? 'main.completed.order_adjustments_checkout'
            : 'main.completed.order_adjustments_cart';

        return view($view,
            compact('order', 'orderAdjustments', 'total', 'added', 'deducted', 'refund', 'subTotal', 'sales_tax'));
    }

    /**
     * @param $orderAdjustments
     * @return mixed
     */
    public function extractAddedTotal($orderAdjustments)
    {
        return $orderAdjustments->where('adjustment_action', Constant::ADDED)
            ->sum('amount');
    }

    /**
     * @param $orderAdjustments
     * @return mixed
     */
    public function extractDeductedTotal($orderAdjustments)
    {
        return $orderAdjustments->where('adjustment_action', Constant::DEDUCTED)
            ->sum('amount');
    }

    /**
     * @param $subTotal
     * @param $total
     * @return int|number
     */
    public function extractRefund($subTotal, $total)
    {
        return ($subTotal - $total) < 0
            ? abs($subTotal - $total)
            : 0;
    }

    /**
     * @param $subTotal
     * @param $total
     * @return int
     */
    public function extractSubTotal($subTotal, $total)
    {
        return ($subTotal - $total) > 0
            ? ($subTotal - $total)
            : 0;
    }

    /**
     * @param RegistrationOrder $registrationOrder
     * @param array $cancelledClassRegistrationIds
     * @return int
     */
    public function extractDeferredHalfPriceDiscounts(RegistrationOrder $registrationOrder, $cancelledClassRegistrationIds = [])
    {
        $ctrGetFirstTwo = 0;
        $ctrDeferred = 0;
        $deferredDiscounts = 0;

        $firstTwoClassRegistrations = collect();
        $classRegistrationIds = collect();

        if ($registrationOrder) {
            $classRegistrations = $this->classRegistrationRepo
                                        ->getByRegistrationIdNoSwapNoCancel($registrationOrder->id);

            foreach ($classRegistrations as $classRegistration) {

                if (!$classRegistration->subscriptionDiscount and
                    !$classRegistration->classCreditDiscount and
                    !$classRegistration->is_cancelled) {

                    $ctrGetFirstTwo++;

                    if ($ctrGetFirstTwo < 3) {

                        $firstTwoClassRegistrations->push($classRegistration->id);
                    }
                }
            }

            if ($firstTwoClassRegistrations->count()) {
                foreach ($registrationOrder->classRegistrations as $classRegistration) {

                    if (!$classRegistration->subscriptionDiscount and
                        !$classRegistration->classCreditDiscount and
                        !$classRegistration->is_cancelled and
                        !in_array($classRegistration->id, $cancelledClassRegistrationIds)
                    ) {
                        $ctrDeferred++;

                        if ($ctrDeferred < 3  and
                            $classRegistration->is_half_priced
                        ) {
                            $deferredDiscounts += $classRegistration->classHalfPriceDiscount->deduction;
                            $classRegistrationIds->push($classRegistration->id);
                        }
                    }
                }
            }
        }

        return [
            'amount' => $deferredDiscounts,
            'class_registration_ids' => $classRegistrationIds
        ];
    }

    /**
     * @param RegistrationOrder $registrationOrder
     * @param array $cancelledRegistrationIds
     * @return array
     */
    public function extractDeferredGroupDiscounts(RegistrationOrder $registrationOrder, $cancelledRegistrationIds = [])
    {
        $deferredDiscounts = 0;
        $classRegistrationIds = collect();
        $classCombinationDiscountIds = collect();

        foreach ($cancelledRegistrationIds as $cancelledRegistrationId) {
            $classRegistration = ClassRegistration::find($cancelledRegistrationId);

            $classCombination = $this->classCombinationRepo->getBySingeRegId($registrationOrder->id);
            $classCombinationDiscount = $this->classCombinationDiscountRepo
                ->getByClassRegistrationId($classRegistration->id, true);

            if ($registrationOrder->has_pair and $classCombination and $classCombinationDiscount) {
                $classOne = $classCombinationDiscount->classOne;
                $classTwo = $classCombinationDiscount->classTwo;

                if (!$classOne->is_cancelled and !$classTwo->is_cancelled) {
                    $deferredDiscounts += 50;
                }

                $classRegistrationIds->push($classRegistration->id);
                $classCombinationDiscountIds->push($classCombinationDiscount->id);
            }
        }

        return [
            'amount' => $deferredDiscounts,
            'class_registration_ids' => $classRegistrationIds,
            'class_combination_discount_ids' => $classCombinationDiscountIds
        ];
    }

    /**
     * @param $paid
     * @param $deferredHalfDiscount
     * @param $deferredGroupDiscount
     * @param $additionalFees
     * @param $deductions
     * @param $totalFee
     * @return int
     */
    public function extractCancellationSubTotal($paid,
                                                $deferredHalfDiscount,
                                                $deferredGroupDiscount,
                                                $additionalFees,
                                                $deductions,
                                                $totalFee
    )
    {
        return ($paid - $deferredHalfDiscount - $deferredGroupDiscount - $additionalFees + $deductions - $totalFee) > 0
            ? ($paid - $deferredHalfDiscount - $deferredGroupDiscount - $additionalFees + $deductions - $totalFee)
            : 0;
    }

    /**
     * @param $currentTotal
     * @param $paid
     * @param $deferredHalfPrice
     * @param $deferredGroupDiscount
     * @param $additionalFees
     * @param $deductions
     * @return mixed
     */
    public function extractNewOrderTotal($currentTotal,
                                         $paid,
                                         $deferredHalfPrice,
                                         $deferredGroupDiscount,
                                         $additionalFees,
                                         $deductions)
    {
        return $currentTotal - $paid + $deferredHalfPrice + $deferredGroupDiscount - $additionalFees + $deductions;
    }

    /**
     * @param $amountPaid
     * @param $classRegId
     * @return int
     */
    public function extractAdjustmentClassRegistrationTotal($amountPaid, $classRegId)
    {
        $orderAdjustments = $this->orderAdjustmentRepo
                                 ->getByAdjustable($classRegId, Constant::CLASS_REGISTRATION);

        if ($orderAdjustments->count()) {
            foreach ($orderAdjustments as $orderAdjustment) {
                if ($orderAdjustment->adjustment_action == Constant::ADDED) {
                    $amountPaid -= $orderAdjustment->amount;
                }

                else if ($orderAdjustment->adjustment_action == Constant::DEDUCTED) {
                    $amountPaid += $orderAdjustment->amount;
                }
            }

            return $amountPaid;
        }

        return $amountPaid;
    }

    /**
     * @param Collection $orderAdjustmentHistories
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extractOrderAdjustmentHistory(Collection $orderAdjustmentHistories)
    {
        return view('main.completed.order_adjustment_history', compact('orderAdjustmentHistories'));
    }

    /**
     * @param ClassRegistration $classRegistration
     * @return mixed
     */
    public function getOrderAdjustmentsOfClassRegistration(ClassRegistration $classRegistration)
    {
        return $this->orderAdjustmentRepo
                    ->getByAdjustable($classRegistration->id, Constant::CLASS_REGISTRATION)
                    ->count();
    }
}