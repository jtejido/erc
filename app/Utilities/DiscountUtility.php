<?php namespace App\Utilities;

use App\Models\ClassCombinationDiscount;
use App\Models\ClassCouponDiscount;
use App\Models\ClassCreditDiscount;
use App\Models\ClassHalfPriceDiscount;
use App\Models\ClassMilitaryDiscount;
use App\Models\ClassSubscriptionDiscount;
use App\Models\Course;
use App\Models\OrderAdjustmentDiscount;
use App\Models\YearlySubscriptionType;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\CouponDiscountRepository;

class DiscountUtility
{

    /**
     * @var CouponDiscountRepository
     */
    protected $couponDiscountRepo;

    /**
     * @var
     */
    protected $classCouponDiscountRepo;

    /**
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     */
    public function __construct(CouponDiscountRepository $couponDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo)
    {
        $this->couponDiscountRepo = $couponDiscountRepo;
        $this->classCouponDiscountRepo = $classCouponDiscountRepo;
    }

    /**
     * @param ClassCombinationDiscount $classDiscount
     * @return string
     */
    public function extractClassCombinationInfo(ClassCombinationDiscount $classDiscount)
    {
        return extract_name($classDiscount->attendee);
    }

    /**
     * @param ClassHalfPriceDiscount $classHalfPriceDiscount
     * @return string
     */
    public function extractHalfPriceInfo(ClassHalfPriceDiscount $classHalfPriceDiscount)
    {
        $classReg = $classHalfPriceDiscount->classRegistration;
        $name     = extract_name($classReg->contact);
        $title    = $classReg->registrationOrder->title();

        return $name . ' - ' . $title;
    }

    /**
     * @param ClassCouponDiscount $classCouponDiscount
     * @return string
     */
    public function extractCouponDiscountInfo(ClassCouponDiscount $classCouponDiscount)
    {
        $classReg = $classCouponDiscount->classRegistration;

        if ($classReg) {
            $name = extract_name($classReg->contact) ?: '';
            $title = $classReg->registrationOrder->title();

            return $name . ' - ' . $title . ' (' . $classCouponDiscount->couponDiscount->coupon->coupon_code . ')';
        }
    }

    /**
     * @param OrderAdjustmentDiscount $orderAdjustmentDiscount
     * @return string
     */
    public function extractOrderAdjustment(OrderAdjustmentDiscount $orderAdjustmentDiscount)
    {
        $adjustment = '';
        $name = '';
        $adjustable  = $orderAdjustmentDiscount->adjustable;
        /*
        $extractUser = !$isClassRegistration
                       ? $adjustable = $adjustable->agent : $adjustable;
       */


        switch ($orderAdjustmentDiscount->adjustable_type) {

            case Constant::CLASS_REGISTRATION :
                $adjustment = 'Class Registration';
                $name = extract_name($adjustable->contact);
                break;

            case Constant::CLIENT_SUBSCRIPTION :
                $adjustment = 'Yearly Subscription';
                $name = extract_name($adjustable->agent->contact);
                break;

            case Constant::CORPORATE_SEAT :
                $adjustment = 'Corporate Seat';
                $name = extract_name($adjustable->agent->contact);
                break;

            case Constant::PRODUCT_ORDERS :
                $product = $orderAdjustmentDiscount->adjustable->product;
                return '['.$orderAdjustmentDiscount->adjustable->quantity . '] Book - ' . $product->name;
                break;

            default:
                return;
                break;
        }

        return  $name . '\'s ' . $adjustment;
    }

    /**
     * @param ClassMilitaryDiscount $militaryDiscount
     * @return string
     */
    public function extractMilitaryDiscount(ClassMilitaryDiscount $militaryDiscount)
    {
        $name = extract_name($militaryDiscount->contact);
        $title = $militaryDiscount->classRegistration->registrationOrder->title();

        return $name . ' - ' . $title;
    }

    /**
     * @param ClassSubscriptionDiscount $classSubscriptionDiscount
     * @return string
     */
    public function extractSubscriptionDiscount(ClassSubscriptionDiscount $classSubscriptionDiscount)
    {
        $name = extract_name($classSubscriptionDiscount->classRegistration->contact);
        $title = $classSubscriptionDiscount->classRegistration->registrationOrder->title();

        return $name . ' - ' . $title;
    }

    /**
     * @param ClassCreditDiscount $classCreditDiscount
     * @return string
     */
    public function extractCreditDiscount(ClassCreditDiscount $classCreditDiscount)
    {
        $name = extract_name($classCreditDiscount->classRegistration->contact);
        $title = $classCreditDiscount->classRegistration->registrationOrder->title();

        return $name . ' - ' . $title;
    }

    /**
     * @param Course $course
     * @param YearlySubscriptionType $type
     * @return bool
     */
    public function subscriptionMatchesType(Course $course, YearlySubscriptionType $type)
    {
        $bool = false;

        switch ($type->name) {

            case Constant::WEBCASTS :
                $bool = $course->isWebCast();
                break;

            case Constant::WEBCASTS_SEMINARS :
                $bool = $course->isSeminar() || $course->isWebCast();
                break;
        }

        return $bool;
    }

    /**
     * @param $coupondId
     * @return int
     */
    public function countRegistrationsOfCoupon($coupondId)
    {
        $count = 0;
        $couponDiscounts = $this->couponDiscountRepo->getByCouponId($coupondId);

        if ($couponDiscounts->count()) {
            $couponDiscountIds = $couponDiscounts->pluck('id')->toArray();
            $count = $this->classCouponDiscountRepo->getByCouponDiscountIds($couponDiscountIds)->count();
        }

        return $count;
    }
}