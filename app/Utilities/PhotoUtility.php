<?php namespace App\Utilities;

use App\Models\Regulation;
use App\Models\Tip;
use File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class PhotoUtility
{

    /**
     * @var string
     */
    const PHOTO_ROOT = 'invoice_photos/';

    /**
     * Validate file mime type and size
     *
     * @param UploadedFile $file
     * @return string
     */
    public function validateMimeTypeAndSize(UploadedFile $file)
    {
        $error = '';
        $sizeLimit = 2048000;
        $sizeLimitInMb = floor($sizeLimit/1024000);
        $validMimeTypes = ['application/pdf', 'image/jpg', 'image/jpeg', 'image/png', 'image/gif'];

        if (!in_array($file->getMimeType(), $validMimeTypes)) {
            $error = 'Only images and pdf are allowed.';
        } else if ($file->getSize() > $sizeLimit) {
            $error = 'File size should not exceed '.$sizeLimitInMb.' MB.';
        }

        return $error;
    }

    /**
     * @param $container
     * @return string
     */
    public function getFilePath($container)
    {
        return self::PHOTO_ROOT.$container;
    }

    /**
     * @param $container
     * @return string
     */
    public function getOrCreateFilePath($container)
    {
        if (!Storage::disk('local')->exists(self::PHOTO_ROOT.$container)) {
            Storage::disk('local')->makeDirectory(self::PHOTO_ROOT.$container, 0777, true);
        }

        return self::PHOTO_ROOT.$container;
    }

    /**
     * @param $path
     * @return string
     */
    public function getFilePathByOrderInvoiceId($path)
    {
        return Constant::FILE_COMPLETE . '/' . $path;
    }

    /**
     * @param $container
     * @param $fileName
     * @param null $path
     * @return mixed
     */
    public function getFullFilePathFromStorage($container, $fileName, $path = null)
    {
        if ($path) {
            $container = self::getFilePathByOrderInvoiceId($path);
        }

        $destination = self::getFilePath($container);

        return Storage::disk('local')->get($destination . '/' . $fileName);
    }

    /**
     * @param $holder
     * @param $type
     * @return string
     */
    public function getInvoiceFile($holder, $type)
    {
        $object = filter_invoice_files(self::PHOTO_ROOT . $holder, $type);

        if ($holder == Constant::FILE_TEMP) {
            $object = Storage::disk('local')->files(self::PHOTO_ROOT . $holder);
        }

        return $object;
    }

    /**
     * @param $file
     * @return string
     */
    public static function embed($file)
    {
        $path = $file;
        $file = (!empty($file) and !is_array($file)) ? $file : '';

        if (file_exists($file)) {
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            return $base64;
        }
        return '';
        
    }

    /**
     * @param $name
     * @return bool
     */
    public function getFilePathOfSignature($name)
    {
        $path = 'signatures/' . $name;

        if (Storage::disk('local')->exists($path)) {
            return Storage::disk('local')->get($path);
        }

        return false;
    }
}