<?php namespace App\Utilities;

class Constant {

    CONST

        # Roles

        ROLE_MEMBER         = 'ROLE_MEMBER',
        ROLE_CSR            = 'ROLE_CSR',
        ROLE_ADMIN          = 'ROLE_ADMIN',

        # User Types

        ADMIN               = 1,
        MEMBER              = 2,
        SUPER_ADMIN         = 3,

        # Course Types

        SEMINAR             = 1,
        WEBCAST             = 2,
        COMPUTER_BASED      = 3,

        # Course Categories

        HWM_CAT             = 1,
        HMT_CAT             = 2,
        ENV_CAT             = 3,
        SAFE_CAT            = 4,

        SEMINAR_COLOR             = '#00a65a',
        WEBCAST_COLOR             = '#00c0ef',
        COMPUTER_BASED_COLOR      = '#f39c12',

        # Registration Objects

        COURSE_OBJECT       = 'Course',
        COURSE_CLASS_OBJECT = 'CourseClass',

        # Status Types

        PENDING             = 'PENDING',
        INVOICED            = 'INVOICED',
        INCOMPLETE          = 'INCOMPLETE',
        COMPLETE            = 'COMPLETE',
        CANCELLED           = 'CANCELLED',
        REMOVED             = 'REMOVED',
        IN_PROCESS          = 'IN PROCESS',
        RESTORE             = 'RESTORE',

        # Subscription Types

        WEBCASTS            = 'Webcasts',
        WEBCASTS_SEMINARS   = 'Webcasts and Seminars',

        # Invoice objects

        FILE_COMPLETE       = 'completed',
        FILE_TEMP           = 'temp',

        PO_IMAGE             = 'po_image',
        PO_FILE              = 'po_file',
        CHECK_IMAGE          = 'check_image',
        CHECK_FILE           = 'check_file',

        # Order Objects

        REGISTRATION_ORDER  = 'RegistrationOrder',
        PRODUCT_ORDERS      = 'ProductOrders',
        CLASS_REGISTRATION  = 'ClassRegistration',
        YEARLY_SUBSCRIPTION = 'YearlySubscription',
        CLIENT_SUBSCRIPTION = 'ClientYearlySubscription',
        CORPORATE_SEAT      = 'ClientCorporateSeatCredit',
        ONSITE_TRAINING     = 'OnsiteTraining',
        BOOK                = 'Book',
        COURSE              = 'Course',

        # Coupon Types

        FIXED               = 'FIXED',
        PERCENT             = 'PERCENT',

        # Courses for Pricing Rules

        HWM                 = 'Hazardous Waste Management: The Complete Course (RCRA)',
        HWM_TEXAS           = 'Texas Hazardous Waste Management',
        HWM_CAL             = 'California Hazardous Waste Management',
        DOT                 = 'DOT Hazardous Materials Training: The Complete Course',

        # Excluded Half Price Discounts

        TDG                 = 'Transportation of Dangerous Goods: Compliance with IATA and IMO Regulations',
        HWO_40              = 'Hazardous Waste Operations and Emergency Response 40-Hour Training (HAZWOPER)',
        HWO_24              = 'Hazardous Waste Operations and Emergency Response 24-Hour Training (HAZWOPER)',
        TDG_WEB             = 'Transportation of Dangerous Goods: Compliance with IATA Regulations - Webcast',
        DGU                 = 'IATA Dangerous Goods Update - Webcast',

        # CM WEB API
        CM_CREATE_UPDATE_STUDENTS       = 'createUpdateStudents',
        CM_GET_STUDENTS                 = 'getStudents',
        CM_ENROLL_STUDENTS              = 'enrollStudents',

        # Email Subjects

        CLASS_CANCELLED           = 'Class has been cancelled',
        ORDER_RECEIPT             = 'Order is Complete',
        INVOICE_RECEIPT           = 'Invoice Received',
        REGISTRATION_CONFIRMATION = 'Registration Confirmation',
        PRODUCT_CONFIRMATION      = 'Product Purchase Confirmation',
        REGISTRATION_AND_PRODUCT_CONFIRMATION = 'Registration and Product Purchase Confirmation',
        PURCHASE_CONFIRMATION = 'Purchase Confirmation',

        # Discount Objects
        CLASS_HALF_PRICE          = 'ClassHalfPriceDiscount',
        CLASS_COUPON              = 'ClassCouponDiscount',
        CLASS_COMBINATION         = 'ClassCombinationDiscount',
        CLASS_MILITARY            = 'ClassMilitaryDiscount',
        ORDER_ADJUSTMENT          = 'OrderAdjustmentDiscount',
        ORDER_ADJUSTMENT_OBJECT   = 'OrderAdjustment',

        # File Entries
        EBOOK                       = 'EBOOK',
        EBOOK_UPDATE                = 'EBOOK_UPDATE',
        MATERIAL                    = 'MATERIAL',

        #Order Adjusment Actions
        ADDED          = 'ADDED',
        DEDUCTED       = 'DEDUCTED',

        RCRA            = 1,
        DOT1            = 2,
        DOT3            = 3,
        IATA            = 4,
        HAZWOPER        = 5;
}