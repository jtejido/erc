<?php namespace App\Utilities;

use App\Models\Category;
use App\Models\CourseClass;
use App\Models\CourseOrder;
use App\Models\OrderItem;
use App\Models\RegistrationOrder;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClassRepository;
use App\Repositories\CourseRepository;
use App\Repositories\RegistrationOrderRepository;
use Carbon\Carbon;

class CourseUtility
{

    /**
     * @var CourseRepository
     */
    protected $repo;

    /**
     * @var ClassRepository
     */
    protected $classRepo;

    /**
     * @var RegistrationOrderRepository
     */
    protected $registrationOrderRepo;

    /**
     * @var ClassRegistrationRepository
     */
    protected $classRegistrationRepo;

    /**
     * @param CourseRepository $repo
     * @param ClassRepository $classRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     */
    public function __construct(CourseRepository $repo,
                                ClassRepository $classRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->repo = $repo;
        $this->classRepo = $classRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * Gets list of courses given instance of a category
     *
     * @param Category $category
     * @return mixed
     */
    public function getCoursesGivenParams(Category $category, $searchq = '', $type = null, $location = null)
    {
        $params = [
            'category' => $category->id,
            'type'     => !empty($type) ? $type : null,
            'searchq'  => $searchq,
            'location' => $location,
            'byOrder' => true
        ];

        $query = $this->repo->searchFilter($params);
        $courses = $this->repo->getSortedCourseList($query);

        return $courses->get();
    }

    /**
     * @param $classId
     * @return mixed
     */
    public function hasAttendees($classId)
    {
        return $this->registrationOrderRepo
                    ->getByRegistrable($classId, Constant::COURSE_CLASS_OBJECT);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function getInstructorClasses($id)
    {
        return $this->classRepo->getByActiveInstructorId($id);
    }

    /**
     * @param CourseClass $class
     * @return mixed
     */
    public function scopeActive(CourseClass $class)
    {
        return $this->classRepo->getByActiveClassId($class->id);
    }

    /**
     * @param RegistrationOrder $registrationOrder
     * @return bool
     */
    public function excludedClassHalfPriceRule(RegistrationOrder $registrationOrder)
    {
        $excludedClassIds = $this->classRepo->getIdsOfExcluded();

        if ($registrationOrder->registrable_type == Constant::COURSE_CLASS_OBJECT) {
            if (in_array($registrationOrder->registrable_id, $excludedClassIds)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $classRegistration
     * @param $classRegistrationPair
     * @return bool
     */
    public function isTheSameLocationOrWeek($classRegistration, $classRegistrationPair)
    {
        $registrationOrder = $classRegistration->registrationOrder;
        $registrationOrderPair = $classRegistrationPair->registrationOrder;
        $bool = false;

        if ($registrationOrder->registrable_type == Constant::COURSE_CLASS_OBJECT and
            $registrationOrderPair->registrable_type == Constant::COURSE_CLASS_OBJECT) {

            $class = $registrationOrder->registrable;
            $classPair = $registrationOrderPair->registrable;

            $classStartDate = new Carbon($class->start_date);
            $classPairStartDate = new Carbon($classPair->start_date);

            if ($class->location_id == $classPair->location_id) {
                $bool = true;
            }

            if ($classStartDate->diffInDays($classPair->start_date, false) <= 7 and
                $classStartDate->diffInDays($classPair->start_date, false) >= 0) {
                $bool = true;
            }

            if ($classPairStartDate->diffInDays($class->start_date, false) <= 7 and
                $classPairStartDate->diffInDays($class->start_date, false) >= 0) {
                $bool = true;
            }
        }

        return $bool;
    }

    /**
     * @param $classId
     * @return int
     */
    public function countAttendeesOfClass($classId)
    {
        $count = 0;
        $registrations = $this->registrationOrderRepo->getRegistrationIdsByClassId($classId);

        if ($registrations->count()) {
            $registrationIds = $registrations->pluck('id')->toArray();

            return $this->classRegistrationRepo->countActiveRegistrationsByIds($registrationIds);

        }

        return $count;
    }

    /**
     * @param CourseClass $class
     * @return bool
     */
    public function isClassStartedNotExpired(CourseClass $class)
    {
        $now = Carbon::now();
        $startDate = $class->start_date;
        $oneYear = $class->start_date->addYear();

        return ($now >= $startDate and $now <= $oneYear);
    }

    /**
     * @param OrderItem $orderItem
     * @return mixed
     */
    public function getClassDate(OrderItem $orderItem)
    {
        if ($orderItem->orderable()->withTrashed()->first()->registrable_type == Constant::COURSE_CLASS_OBJECT) {
            return $orderItem->orderable()->withTrashed()->first()->registrable->date_range;
        }
    }

    /**
     * @param OrderItem $orderItem
     * @return mixed
     */
    public function getClassLocation(OrderItem $orderItem)
    {
        if ($orderItem->orderable()->withTrashed()->first()->registrable_type == Constant::COURSE_CLASS_OBJECT) {
            return $orderItem->orderable()->withTrashed()->first()->registrable->location_name;
        }
    }

    /**
     * @param CourseOrder $courseOrder
     * @return mixed
     */
    public function getCourseOrderCount(CourseOrder $courseOrder)
    {
        $arr = [];
        $courses = $this->repo->getByCategoryAndType($courseOrder->category_id, $courseOrder->course_type_id);

        for ($i = 1; $i <= $courses->count(); $i++) {
            $arr[$i] = $i;
        }
        return $arr;
    }
}