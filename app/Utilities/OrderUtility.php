<?php namespace App\Utilities;

use App\Models\State;
use App\Repositories\OrderAdjustmentRepository;
use Auth;
use Request;
use App\Models\User;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\Book;
use App\Models\RegistrationOrder;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\ProductOrdersRepository;
use App\Repositories\ClientYearlySubscriptionRepository;
use Carbon\Carbon;

class OrderUtility
{

    /**
     * @var object
     */
    private $user;

    /**
     * @var string
     */
    private $page;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var ClientYearlySubscriptionRepository
     */
    private $clientYearlySubscriptionRepo;

    /**
     * @var ProductOrdersRepository
     */
    private $productOrdersRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo
     * @param ProductOrdersRepository $productOrdersRepository
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     */
    public function __construct(OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo,
                                ProductOrdersRepository $productOrdersRepository,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                OrderAdjustmentRepository $orderAdjustmentRepo)
    {
        $this->user = Auth::user();
        $this->orderRepo    = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->clientYearlySubscriptionRepo = $clientYearlySubscriptionRepo;
        $this->productOrdersRepo = $productOrdersRepository;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
    }

    /**
     * Extract template from each items
     *
     * @param OrderItem $item
     * @param User $user
     * @param $page
     * @param $items
     * @return OrderUtility|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Exception
     */
    public function extractItems(OrderItem $item, User $user, $page = null, $items = [])
    {
        try {

            $template = '';
            $this->page = $page;

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $template = $this->extractRegistrations($item, $user, $items);
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $template = $this->extractYearlySubs($item, $user);
                    break;

                case Constant::CORPORATE_SEAT :
                    $template = $this->extractCorpSeats($item, $user);
                    break;

                case Constant::PRODUCT_ORDERS :
                    $template = $this->extractProduct($item, $user);
                    break;

                default :
                    break;
            }

            return $template;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Extract from registrations info
     *
     * @param OrderItem $item
     * @param User $user
     * @param $items
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    private function extractRegistrations(OrderItem $item, User $user, $items)
    {
        $registration = $item->orderable()->withTrashed()->first();
        $registrable = $registration->registrable()->withTrashed()->first();
        $status = $item->status;
        $order = $item->order;

        $data['item'] = $item;
        $data['user'] = $this->user;
        $data['orderId'] = Request::route('order_id');
        $data['orderItemId'] = $item->id;
        $data['registrationOrderId'] = $registration->id;
        $data['isCBT'] = $registration->isCBT();
        $data['classRegistrations'] = [];
        $data['newRegistrationsInOrder'] = 0;
        $data['agent'] = $item->order->user;

        $data['title'] = $registration->title();
        $course = $registration->isCourse()
                  ? $registrable
                  : $registrable->course;

        $data['instructor'] = '';
        if (!$registration->isCourse()) {
            $data['instructor'] = $registrable->instructor_name;
        }

        $class = !$registration->isCourse()
                  ? $registrable->id
                  : null;

        $data['schedule']  = $registration->date();

        $data['venue'] = ($course->isSeminar() && !$registration->isCourse())
                         ? $registrable->completeLocation
                         : (($course->isWebcast()) ? 'Webcast' : '');

        $params = [
            'user_id'   => !$this->user->isMember()
                           ? $user->id
                           : null,
            'course_id' => $course->id,
            'class_id'  => $class
        ];

        $noSwapRegistrations = $this->classRegistrationRepo
                                         ->getByRegistrationIdNoCancel($registration->id);

        $data['newRegistrations'] = $this->classRegistrationRepo
                                              ->getByRegistrationIdCountNew($registration->id);


        // Get new registrations in order
        $orderItems = $this->orderItemRepo->getByOrderIdAndOrderableType(
            $order->id, Constant::REGISTRATION_ORDER, false
        );

        if ($orderItems->count()) {
            $registrationOrderIds = is_array($orderItems->pluck('orderable_id'))
                                    ? $orderItems->pluck('orderable_id')
                                    : [$orderItems->pluck('orderable_id')];

            $data['newRegistrationsInOrder'] = $this->classRegistrationRepo
                                                    ->getByRegistrationIdsCountNew($registrationOrderIds);
        }

        $data['activeClassRegistrations'] = $this->classRegistrationRepo
                                                 ->getByRegistrationIdNoSwapNoCancel($registration->id)
                                                 ->pluck('id')
                                                 ->toArray();

        $data['orderAdjustments'] = $this->orderAdjustmentRepo
                                          ->getByOrderId($order->id)
                                          ->count();

        foreach ($noSwapRegistrations as $value) {
            array_push($data['classRegistrations'], $value);
        }

        $data['total'] = $item->price_charged;
        $data['canRemove'] = ($status == Constant::PENDING
                            or !Auth::user()->isMember());


        $data['originatingAgent'] = self::isOriginatingAgent($item->order->id, $user->id);

        $data['attendeesRoute'] = $status != Constant::PENDING
                                  ? ($this->user->isMember()
                                    ? route('order.attendees.swap', ['order_item_id' => $item->id])
                                    : route('admin.order.attendees.swap', [
                                        'user_id'           => $user->id,
                                        'order_item_id'     => $item->id
                                    ]))

                                  : ($this->user->isMember()
                                    ? route('contacts.register', array_filter($params))
                                    : route('admin.contacts.register', array_filter($params)));

        $data['attendeeText'] = $status == Constant::PENDING
                                ? 'ADD / EDIT'
                                : 'EDIT / SWAP';

        $view = (self::isCompleteOrInvoiced($item) and !$this->page)
                ? view('main.completed.class_registration', $data)
                : ($this->page
                    ? view('main.checkout.class_registration', $data)
                    : view('main.cart.class_registration', $data));

        // Show related books to this course
        if ($items && $course->isWebcast()) {
            foreach ($course->books as $book) {
                $show = true;
                // If book is already part of the orders then don't show it
                $items->each(function($item, $key) use ($book, &$show) {
                    if($item->orderable_type == 'ProductOrders' && $item->orderable->product->id == $book->id) {
                        $show = false;
                        return false;
                    }
                });
                if ($show && $status == Constant::PENDING)
                $view .= $this->extractProductAdd($book, $user, true);
            }
        }

        return $view;
    }


    /**
     * Extract from subscription info
     *
     * @param OrderItem $item
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function extractYearlySubs(OrderItem $item, User $user)
    {
        $subscription       = $item->orderable;
        $data['item']       = $item;
        $data['user']       = $this->user;
        $data['client']     = $user;
        $data['attendees']  = [];
        $data['title'] = 'Yearly Subscription';
        $data['total'] = $subscription->total_price;

        $data['attendees'] = $this->clientYearlySubscriptionRepo->getByYearlySubscription($subscription);

        $view = $this->page
                ? view('main.checkout.yearly_subscription', $data)
                : view('main.cart.yearly_subscription', $data);

        return $view;
    }

    /**
     * Extract from credit info
     *
     * @param OrderItem $item
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function extractCorpSeats(OrderItem $item, User $user)
    {
        $corporateSeat    = $item->orderable;
        $data['user']     = $this->user;
        $data['title']    = 'Corporate Seat Credit';
        $data['corpSeat'] = $corporateSeat;
        $data['item']     = $item;
        $data['client']   = $user;
        $data['total']    = $corporateSeat->price;

        $view = $this->page
            ? view('main.checkout.corporate_seat', $data)
            : view('main.cart.corporate_seat', $data);

        return $view;
    }

    public function extractProduct(OrderItem $item, User $user)
    {
        $status                 = $item->status;
        $data['status']         = $status;
        $data['productOrder']   = $productOrder = $item->orderable;
        $data['book']           = $book   = $productOrder->product;
        $data['user']           = $this->user;
        $data['item']           = $item;
        $data['total']          = $productOrder->getProductOrderPrice();
        $data['total_notax']    = $productOrder->getProductOrderPrice(null,0);
        $data['noRemove']       = $status == Constant::PENDING;
        $data['client']         = $user;
        $data['state_tax']      = $item->state_tax;
        $data['state_tax_amt']  = $data['total'] - $data['total_notax'];

        $view = $this->page
            ? view('main.checkout.book', $data)
            : view('main.cart.book', $data);
        return $view;
    }

    public function extractProductAdd(Book $item, User $user, $discounted = false)
    {
        $status                 = Constant::PENDING;
//        $data['productOrder']   = $productOrder = $item->orderable;
        $data['book']           = $book   = $item;
        $data['user']           = $this->user;
        $data['item']           = $item;
        $data['total']          = '0';
        $data['noRemove']       = $status == Constant::PENDING;
        $data['client']         = $user;
        $data['discounted']         = $discounted;

        return view('main.cart.bookadd', $data);
    }

    /***************************************************************************************/
    /*                              Order History Details                                  */
    /***************************************************************************************/

    /**
     * @param OrderItem $item
     * @return string
     * @throws \Exception
     */
    public function getLocation($item)
    {
        try {
            $location = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;

                    if (self::isCompleteOrInvoiced($item)) {
                        $registrable = $item->orderable()->withTrashed()->first();
                    }

                    if($registrable) {
                        $location = !$registrable->isCourse()
                            ? ($registrable->registrable->course->isSeminar()

                                ? "<p>" . $registrable->registrable->address . "</p>" .
                                "<p class='block'>" . $registrable->registrable->city .
                                ', ' . $registrable->registrable->state .
                                ' ' . $registrable->registrable->zip

                                : 'Webcast')
                            : 'Online Training';
                    }

                    break;

                case Constant::ONSITE_TRAINING :
                    $location = $item->orderable->address;
                    break;

                case Constant::PRODUCT_ORDERS :
                    $location = null;
                    break;

                default :
                    break;
            }

            return $location;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param OrderItem $orderItem
     * @return mixed
     * @throws \Exception
     */
    public function getTransactionId(OrderItem $orderItem)
    {
        return $orderItem->transaction_id;
        /*
        try {

            $order = $orderItem->order;

            if ($order->orderTransaction) {
                $trans = $order->orderTransaction;

                return $trans->transaction_id;
            }

        } catch (\Exception $e) {
            throw $e;
        }
        */
    }

    /**
     * @param OrderItem $orderItem
     * @return bool|string
     * @throws \Exception
     */
    public function getTransactionDate(OrderItem $orderItem)
    {
        try {
            $date = '';

            $order = $orderItem->order;

            if (Auth::user()->isAdminorCSR()) {
                $order = $orderItem->order()->withTrashed()->first();
            }

            if ($order->orderTransaction) {
                $trans = $order->orderTransaction;

                $date = $trans->created_at->toFormattedDateString();
            }

            return $date;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getFiles($item)
    {
        switch ($item->orderable_type) {
            case Constant::REGISTRATION_ORDER :
                $registrable = $item->orderable;
                $files = $registrable->materials();
                break;
        }
        return $files;
    }

    public function getEbooks($item)
    {
        switch ($item->orderable_type) {
            case Constant::REGISTRATION_ORDER :
                $registrable = $item->orderable;
                $files = $registrable->ebooks();
                break;
        }
        return $files;
    }

    public function getCourses($item)
    {
        switch ($item->orderable_type) {
            case Constant::REGISTRATION_ORDER :
                $registrable = $item->orderable;
                $courses = $registrable->courses();
                break;
        }
        return $courses;
    }

    public function getCourseInstance($item)
    {
        switch ($item->orderable_type) {
            case Constant::REGISTRATION_ORDER :
                $registrable = $item->orderable;
                $course = $registrable->course();
                break;
        }
        return $course;
    }

    public function getClassInstance($item)
    {
        switch ($item->orderable_type) {
            case Constant::REGISTRATION_ORDER :
                $registrable = $item->orderable;
                break;
            default:
                $registrable = null;
        }
        return $registrable;
    }

    /**
     * @param OrderItem $item
     * @return string
     * @throws \Exception
     */
    public function getTitle($item)
    {
        try {
            $title = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;

                    if (self::isCompleteOrInvoiced($item)) {
                        $registrable = $item->orderable()->withTrashed()->first();
                    }

                    $title = ($registrable)
                        ? $registrable->title()
                        : $title;
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $title = 'Yearly Subscription';
                    break;

                case Constant::CORPORATE_SEAT :
                    $title = 'Corporate Seats';
                    break;

                case Constant::ONSITE_TRAINING :
                    $title = 'Onsite Training';
                    break;

                case Constant::PRODUCT_ORDERS :
                    $order = $item->orderable()->withTrashed()->first();
                    $title = ($order) ? $order->title() : '';
                    break;

                default :
                    break;
            }

            return $title;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getDescription($item)
    {
        try {
            $title = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;
                    $title = $registrable->description();
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $title = 'Yearly Subscription';
                    break;

                case Constant::CORPORATE_SEAT :
                    $title = 'Corporate Seats';
                    break;

                case Constant::ONSITE_TRAINING :
                    $title = 'Onsite Training';
                    break;

                case Constant::PRODUCT_ORDERS :
                    $title = 'test';
                    break;

                default :
                    break;
            }

            return $title;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getPrice($item)
    {
        try {
            $title = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;
                    $title = $registrable->originalPrice();
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $title = 'Yearly Subscription';
                    break;

                case Constant::CORPORATE_SEAT :
                    $title = 'Corporate Seats';
                    break;

                case Constant::ONSITE_TRAINING :
                    $title = 'Onsite Training';
                    break;

                case Constant::PRODUCT_ORDERS :
                    $title = '1';
                    break;

                default :
                    break;
            }

            return $title;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getDuration($item)
    {
        try {
            $title = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;
                    $title = $registrable->description();
                    break;

                case Constant::YEARLY_SUBSCRIPTION :
                    $title = 'Yearly Subscription';
                    break;

                case Constant::CORPORATE_SEAT :
                    $title = 'Corporate Seats';
                    break;

                case Constant::ONSITE_TRAINING :
                    $title = 'Onsite Training';
                    break;

                case Constant::PRODUCT_ORDERS :
                    $title = 'd';
                        break;

                default :
                    break;
            }

            return $title;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $item
     * @return string
     * @throws \Exception
     */
    public function getDate($item)
    {
        try {

            $date = '--';

            switch ($item->orderable_type) {

                case Constant::REGISTRATION_ORDER :
                    $registrable = $item->orderable;

                    if ($registrable->registrable_type === Constant::COURSE_CLASS_OBJECT) {
                        $date = $registrable->date().' '.$registrable->registrable->start->format('g:ia');
                    }

                    break;

                case Constant::ONSITE_TRAINING :
                    $training = $item->orderable;
                    $date = $training->training_date->format('F d, Y');
                    break;
                case Constant::PRODUCT_ORDERS :

                    break;
                default :
                    break;
            }

            return $date;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param OrderItem $item
     * @return string
     */
    public function getAgent($item)
    {
        $data = '--';

        switch ($item->orderable_type) {

            case Constant::REGISTRATION_ORDER ||
                 Constant::CORPORATE_SEAT ||
                 Constant::YEARLY_SUBSCRIPTION :
                try {
                    $user = self::isCompleteOrInvoiced($item)
                        ? $item->orderable()->withTrashed()->first()->agent
                        : $item->orderable->agent;
                    $data = ($user->contact) ? $user->contact->name : $user->email;
                } catch (\Exception $e) {
                    $data = '--';
                }
                break;

            case Constant::PRODUCT_ORDERS :
                $user = $item->orderable->agent;
                $data = ($user->contact) ? $user->contact->name : $user->email;
                break;
        }
        return $data;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getStatus($item)
    {
        $status = $item->status;

        switch ($item->orderable_type) {

            case Constant::REGISTRATION_ORDER :
                $registrationOrder = $item->orderable;

                if (self::isCompleteOrInvoiced($item)) {
                    $registrationOrder = $item->orderable()->withTrashed()->first();
                }

                if ($registrationOrder
                    and !$registrationOrder->isCourse()
                    and $registrationOrder->registrable->is_cancelled) {

                    $status = Constant::CANCELLED;
                }
                break;

            default :
                break;
        }

        return $status;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getConvertedStatus($item)
    {
        $status = $item->status;

        switch ($item->orderable_type) {

            case Constant::REGISTRATION_ORDER :
                $registrationOrder = $item->orderable()->withTrashed()->first();

                if ($registrationOrder
                    and !$registrationOrder->isCourse()
                    and $registrationOrder->registrable->is_cancelled) {

                    $status = Constant::CANCELLED;
                }
                break;

            default :
                break;
        }

        $status = ($status == Constant::COMPLETE) ? 'PAID' : $status;

        return $status;
    }

    /**
     * @param $item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getStudents($item)
    {
        $data['students'] = [];
        $classRegistrations = $item->orderable->classRegistrations;

        if ($classRegistrations) {
            foreach ($classRegistrations as $classRegistration) {
                $id = $classRegistration->attendee_contact_id;
                $cancelled = $classRegistration->is_cancelled;
                $new = $classRegistration->is_new;
                $swapped = $classRegistration->is_swapped;
                $swapNote = $classRegistration->swap_note;
                $cancelNote = $classRegistration->cancellationNote
                              ? $classRegistration->cancellationNote->content
                              : '';

                $contact = $classRegistration->contact;
                $name = $contact->name;

                $userId = $contact->user ? $contact->user->id : null;
                $contactId = $contact->id;
                $params = array_filter(['user_id' => $userId, 'contact_id' => $contactId]);

                array_push($data['students'], compact(
                    'id',
                    'name',
                    'cancelled',
                    'swapped',
                    'swapNote',
                    'cancelNote',
                    'new',
                    'params'));
            }
        }

        return view('includes.class_students', $data);
    }

    /**
     * @param Order $order
     * @param User $user
     * @param $credits
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     * @throws \Exception
     */
    public function extractCredits(Order $order, User $user, $credits)
    {
        try {

            $registrationsIds = [];
            $orderItemRegistrations = $this->orderItemRepo
                                           ->getByOrderIdAndOrderableType(
                                               $order->id,
                                               Constant::REGISTRATION_ORDER,
                                               false
                                           );

            if ($orderItemRegistrations->count()) {

                foreach ($orderItemRegistrations as $orderItemRegistration) {
                    array_push($registrationsIds, $orderItemRegistration->orderable_id);
                }

                if (count($registrationsIds)) {
                    $classRegistrations = $this->classRegistrationRepo
                                               ->getByRegistrationOrderIdsNoCredit($registrationsIds);

                    if ($classRegistrations->count()) {
                        $data['agentId'] = $user->getCorporateSeatsCreditUser()->id;
                        $data['agentName'] = extract_name($user->contact);
                        $data['credits'] = $credits;
                        $data['orderId'] = $order->id;

                        foreach ($classRegistrations as $classRegistration) {
                            if (!$classRegistration->registrationOrder->isCBT()) {
                                $title = $classRegistration->registrationOrder->title();
                                $attendee = $classRegistration->contact;

                                if (!$classRegistration->subscriptionDiscount) {
                                    $data['attendees'][$classRegistration->id] = extract_name($attendee)
                                        . ' in ' . $title;
                                }
                            }
                        }

                        return  isset($data['attendees'])
                                ? view('main.cart.user_credits', $data)
                                : '';
                    }
                }
            }

            return;

        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function hasItems($userId)
    {
        $order = $this->orderRepo->getByUserId($userId);

        return $order ? $order->items->count() : 0;
    }

    /**
     * @param $userId
     * @param $orderId
     * @return bool
     */
    public function isAgent($userId, $orderId)
    {
        $order = $this->orderRepo->getById($orderId);

        return $order->user_id == $userId;
    }

    /**
     * @param $contactId
     * @param $registrable
     * @return bool|\Illuminate\Database\Eloquent\Model
     */
    public function isRegistered($contactId, $registrable)
    {
        $response = false;

        if ($registrable and class_basename($registrable) == Constant::COURSE_CLASS_OBJECT) {
            $registrationOrder = $this->registrationOrderRepo
                ->findOne([
                    'registrable_id'   => $registrable->id,
                    'registrable_type' => class_basename($registrable)
                ]);

            if ($registrationOrder and self::isCompleteOrInvoiced($registrationOrder)) {
                $response = $this->classRegistrationRepo
                    ->findOne([
                        'registration_order_id'    => $registrationOrder->id,
                        'attendee_contact_id'      => $contactId
                    ]);
            }
        }
        return $response;
    }

    /**
     * @param $object
     * @return bool
     */
    public function isCompleteOrInvoiced($object)
    {
        return $object->status == Constant::COMPLETE ||
               $object->status == Constant::INVOICED;
    }

    /**
     * @param $object
     * @return bool
     */
    public function isInvoiced($object)
    {
        return $object->status == Constant::INVOICED;
    }

    /**
     * @param $object
     * @return bool
     */
    public function isPending($object)
    {
        return $object->status == Constant::PENDING;
    }

    /**
     * @param $orderId
     * @param $userId
     * @return mixed
     */
    public function isOriginatingAgent($orderId, $userId)
    {
        return $this->orderRepo
                    ->getByIdAndUserId($orderId, $userId);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function isActingAgent($params = [])
    {
        return $this->actingOriginatingAgentRepo
                    ->getByOrderableAndAgent($params);
    }

    /**
     * @param $orderItem
     * @return bool
     */
    public function isMaterialAvailable($orderItem)
    {
        $available = false;
        if ($orderItem->orderable instanceof RegistrationOrder) {
            if (!$orderItem->orderable->isCourse()) {
                $available = $orderItem->orderable->registrable->isAvailable();
            }
        }
        return $available;
    }

    /**
     * @param $orderItem
     * @return static
     */
    public function getMaterialAvailabiltyDate($orderItem)
    {
        $date = Carbon::now();
        if ($orderItem->orderable instanceof RegistrationOrder) {
            if (!$orderItem->orderable->isCourse()) {
                $date = $orderItem->orderable->registrable->availability;
            }
        }
        return $date;
    }

    /**
     * @param $payment
     * @param User $user
     * @param $item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extractItemsFromPayment($payment, User $user, $item)
    {
        $object = $item->object;
        $contents = $item->contents;

        if ($object == Constant::ORDER_ADJUSTMENT_OBJECT) {
            $adjustmentObject = $item->adjustment_object;
        }

        return view('main.users.payment_cc_items', compact(
            'user',
            'item',
            'object',
            'contents',
            'adjustmentObject'
        ));
    }

    /**
     * @param $payment
     * @param User $user
     * @param $item
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function extractItemsFromPaymentReceipt($payment, User $user, $item)
    {
        $object = $item->object;
        $contents = $item->contents;

        if ($object == Constant::ORDER_ADJUSTMENT_OBJECT) {
            $adjustmentObject = $item->adjustment_object;
        }

        return view('main.receipt.items', compact(
            'user',
            'item',
            'object',
            'contents',
            'adjustmentObject'
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getPaymentClassTitle($id)
    {
        $registrationOrder = RegistrationOrder::find($id);

        return $registrationOrder->title()
            . '<br/>'
            . $registrationOrder->date()
            . ' '
            . $registrationOrder->general_location;
    }

    public function getNotes($item)
    {
        $order = $item->order;

        if (Auth::user()->isAdminorCSR()) {
            $order = $item->order()->withTrashed()->first();
        }

        return $order->notes;
    }

    public function isContactRegistered($contactId, $registrable)
    {
        $response = false;

        $registrationOrders = RegistrationOrder::where('registrable_id', $registrable->id)
            ->where('registrable_type', class_basename($registrable))
            ->whereIn('status', ['INVOICED','COMPLETE'])
            ->get();

        foreach ($registrationOrders as $registrationOrder) {
            if($registrationOrder
                ->classRegistrations()
                ->where('attendee_contact_id', $contactId)
                ->count()) {
                $response = true;
                break;
            }
        }


        return $response;
    }

    public function createPaymentItems($items, $totalAmount)
    {
        $data = (object) [];
        $itemObjects = [];

        $data->total_amount = $totalAmount;

        if ($items->count()) {
            foreach ($items as $item) {
                $itemObjects[] = (object) [
                    'id' => $item->orderable_id,
                    'object' => $item->orderable_type,
                    'contents' => $this->getItemsFromOrderItems($item->orderable)
                ];
            }

            $data->items = $itemObjects;
        }

        return json_encode($data);
    }

    /**
     * @param $orderable
     * @return array
     */
    public function getItemsFromOrderItems($orderable)
    {
        $content = [];

        switch (class_basename($orderable)) {
            case Constant::REGISTRATION_ORDER:

                foreach ($orderable->classRegistrations as $classRegistration) {

                    if($classRegistration->is_cancelled) continue;

                    $content[] = (object) [
                        'id' => $classRegistration->id,
                        'price_paid' => $classRegistration->paid_charge,
                        'name' => $classRegistration->contact->name
                    ];
                }

                break;

            case Constant::YEARLY_SUBSCRIPTION:

                foreach ($orderable->clientSubscriptions as $clientSubscription) {
                    $content[] = (object) [
                        'id' => $clientSubscription->id,
                        'price_paid' => $clientSubscription->paid_price,
                        'name' => $clientSubscription->agent->contact->name
                    ];
                }
                break;

            case Constant::CORPORATE_SEAT:
                $content[] = (object) [
                    'id' => $orderable->id,
                    'price_paid' => $orderable->paid_price,
                    'seats' => $orderable->credits
                ];
                break;

            case Constant::PRODUCT_ORDERS:
                $content[] = (object) [
                    'id' => $orderable->id,
                    'price_paid' => $orderable->getProductOrderPrice(),
                    'quantity' => $orderable->quantity,
                    'title' => $orderable->title()
                ];

                break;
        }

        return $content;
    }

}
