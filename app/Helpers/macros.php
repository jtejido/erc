<?php

use Collective\Html\FormBuilder as Form;

Form::macro('rLabel', function($name, $value = null, $options = [])
{
    $opts = '';
    foreach ($options as $key => $val)
    {
        $opts .= ' ' . $key . '="' . $val . '"';
    }

    return '<label for="' . $name . '" ' . $opts . '>' . $value . ' <span class="required">*</span></label>';
});