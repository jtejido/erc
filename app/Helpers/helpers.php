<?php

if ( ! function_exists('has_role')) {
    /**
     * Check if the current user has role
     * If $role is array, check if the user has at least one of the given roles
     *
     * @param   string | array $role
     * @param   UserAccount $userAccount
     * @return  boolean
     */
    function has_role($role, $user = null)
    {
        $user = is_null($user) ? app('auth.driver')->user() : $user;

        if (!$user) return false;

        if (is_array($role)) {
            foreach ($role as $r) {
                if ($user->hasRole($r)) return true;
            }

            return false;
        }

        return $user->hasRole($role);
    }
}

if ( ! function_exists('extract_name')) {

    /**
     * Extract name string in a contact object
     *
     * @param $object
     * @return string
     */
    function extract_name($object)
    {
        return $object->first_name . ' ' . $object->last_name;
    }
}

if ( ! function_exists('tokenTruncate')) {
    function tokenTruncate($string, $your_desired_width)
    {
        $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);

        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
            $length += strlen($parts[$last_part]);
            if ($length > $your_desired_width) {
                break;
            }
        }

        return implode(array_slice($parts, 0, $last_part));
    }
}

if ( ! function_exists('filter_invoice_files')) {

    function filter_invoice_files($destination, $type)
    {
        return array_filter(\Illuminate\Support\Facades\Storage::disk('local')->files($destination), function ($file) use ($type)
        {
            return preg_match('/' . $type .'(.*)/', $file);
        });
    }
}

if ( ! function_exists('generate_slug')) {
    function generate_slug($id = 0)
    {
        $salt = substr(sprintf('%06d', $id),0,6);
        return uniqid($salt);
    }
}

if ( ! function_exists('customer_cancellation_note')) {

    /**
     * Extract customer note string in registration object
     *
     * @param $note
     * @parma $title
     * @return string
     */
    function customer_cancellation_note($note = '')
    {
        return $note . ' Cancellation Date: ' .
               \Carbon\Carbon::now()->toFormattedDateString();
    }
}

if ( ! function_exists('customer_cancellation_history_note')) {

    /**
     * Extract customer note string in registration object
     *
     * @param $note
     * @param $classRegistration
     * @param $registrationOrder
     * @parma $title
     * @return string
     */
    function customer_cancellation_history_note($classRegistration, $registrationOrder, $note = '')
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        return $name . ' ' . $registrationOrder->title() . ' - ' . $registrationOrder->date() . "'s registration was cancelled. " .
            'Reason: '. $note . ' Cancellation Date: ' . \Carbon\Carbon::now()->toFormattedDateString();
    }
}


if ( ! function_exists('adjustment_registration_added')) {

    /**
     * Extract note string in registration object
     *
     * @param $classRegistration
     * @param $regstrationOrder
     * @param $amount
     * @return string
     */
    function adjustment_registration_added($classRegistration, $regstrationOrder, $amount)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name . ' registered to ' . $regstrationOrder->title() . ' - ' . $regstrationOrder->date() .
                 '. $' . number_format($amount, 2) . ' was charged.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_change_price')) {

    /**
     *  Extract note string in classRegistration and registration object
     *
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_change_price($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
                 ' price of registration was changed.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_half_price_discount')) {

    /**
     *  Extract note string in classRegistration and registration object
     *
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_half_price_discount($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
            ' half price discount was removed.';

        return $text;
    }
}

if ( ! function_exists('adjustment_product_change_price')) {

    /**
     *  Extract note string in productOrder
     *
     * @param $productOrder
     * @return string
     */
    function adjustment_product_change_price($productOrder)
    {
        $text = 'Book - '. $productOrder->title() . ' order price changed.';

        return $text;
    }
}

if ( ! function_exists('adjustment_product_change_qty')) {

    /**
     *  Extract note string in productOrder
     *
     * @param $productOrder
     * @return string
     */
    function adjustment_product_change_qty($productOrder)
    {
        $text = 'Book - '. $productOrder->title() . ' order quantity changed.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_group_discount')) {

    /**
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_group_discount($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
            ' group discount was removed.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_restore')) {

    /**
     *  Extract note string in classRegistration and registration object
     *
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_restore($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
            ' registration was restored.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_credit_applied')) {

    /**
     *  Extract note string in classRegistration and registration object
     *
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_credit_applied($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
            ' registration was given corporate credit discount.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_coupon_applied')) {

    /**
     *  Extract note string in classRegistration and registration object
     *
     * @param $classRegistration
     * @param $registrationOrder
     * @return string
     */
    function adjustment_registration_coupon_applied($classRegistration, $registrationOrder)
    {
        $contactObject = $classRegistration->contact;
        $name = extract_name($contactObject);

        $text = $name ."'s " . $registrationOrder->title() . ' - ' . $registrationOrder->date() .
            ' registration was given coupon discount.';

        return $text;
    }
}

if ( ! function_exists('adjustment_registration_cancellation_fee')) {

    /**
     * Cancellation fee adjustment
     *
     * @param bool|true $totalFee
     * @return string
     */
    function adjustment_registration_cancellation_fee($totalFee = true)
    {
        $text =  $totalFee
                ? 'Balance due for cancelling registrations within the 7-day restriction.'
                : 'Balance due for cancelling registrations.';

        return $text;
    }
}

if ( ! function_exists('getDomainFromEmail')) {
    function getDomainFromEmail($email = '')
    {
        list($user, $domain) = explode('@', $email);
        return $domain;
    }
}

if ( ! function_exists('generate_transaction_id')) {

    /**
     * @param \App\Models\Order $order
     * @return string
     */
    function generate_transaction_id(\App\Models\Order $order)
    {
        $idlen = (strlen($order->id) * 2);
        $transactionId = $order->id .  str_pad($order->id, 10 - $idlen, "0", STR_PAD_LEFT);

        return $transactionId;
    }
}

if ( ! function_exists('string_in_arr_of_obj')) {

    function string_in_arr_of_obj($obj, $arr, $key)
    {

        if(empty($arr)) return false;

        foreach ($arr as $item) {
            if(property_exists($item, $key) and $item->{$key} == $obj) {
                return true;
                break;
            }
        }

        return false;

    }
}

if ( ! function_exists('course_title_add_the')) {

    function course_title_add_the($title)
    {

        if(preg_match('/^[aeiou]/i', $title)) {
            return 'the ' . $title;
        } else {
            return $title;
        }

    }
}

if ( ! function_exists('apply_gsa_discount')) {

    function apply_gsa_discount($originalPrice)
    {
        return round((($originalPrice * 0.8) / 0.9925), 2);
    }
}

function computeProductPrice($price, $qty, $tax)
{
    return round($price * $qty * (1 + $tax / 100), 2);
}

if ( ! function_exists('parse_book_discount')) {

    function parse_book_discount($condition, $qty)
    {
        $conditions = explode('-', $condition);

        if (count($conditions) > 1) {
            return ($qty >= (int) $conditions[0] and $qty <= (int) $conditions[1]);
        } else {
            if (strpos($condition, '+') !== false) {
                $num = (int) str_replace('+', '', $condition);

                return $qty >= $num;
            }
        }
    }
}

if ( ! function_exists('getPercentageAmountFromTotal')) {
    function getPercentageAmountFromTotal($percentage, $total)
    {
        return round($percentage * $total / 100, 2);
    }
}