<?php

namespace App\Listeners;

use App\Events\AssociateContactRegistrationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\UserRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\AddressBookEntryRepository;
use App\Utilities\Constant;
use App\Models\AddressBook;

class AssociateContactRegistrationListener
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var AddressBookEntryRepository
     */
    private $addressBookRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo,
                                OrderItemRepository $orderItemRepo,
                                AddressBookEntryRepository $addressBookRepo)
    {
        $this->userRepo = $userRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->addressBookRepo = $addressBookRepo;
    }

    /**
     * Handle the event.
     *
     * @param  AssociateContactRegistrationEvent  $event
     * @return void
     */
    public function handle(AssociateContactRegistrationEvent $event)
    {
        $usersInClass = collect();
        $agent = $this->userRepo->getById($event->agent_id);
        $orderItem = $this->orderItemRepo->getById($event->order_item_id);
        $usersInClass->push($agent);

        if ($orderItem->orderable_type == Constant::REGISTRATION_ORDER) {
            $registrationOrder = $orderItem->orderable;
            $classRegistrations = $registrationOrder->classRegistrations;

            foreach ($classRegistrations as $classRegistration) {
                $attendee = $classRegistration->contact;

                if ($attendee->user && $attendee->user->id != $agent->id) {
                    $usersInClass->push($attendee->user);
                }
            }

            if ($usersInClass->count()) {
                $usersInClass->each(function($user, $index) use ($usersInClass) {
                    for ($i = $index + 1; $i <= $usersInClass->count(); $i++) {
                        if (isset($usersInClass[$i])) {
                            $masterAddressBooks = $user->addressBooks;
                            $slaveAddressBooks = $usersInClass->get($i)->addressBooks;

                            if ($slaveAddressBooks->count()) {
                                foreach ($slaveAddressBooks as $slaveAddressBook) {
                                    if(empty($slaveAddressBook->contact)
                                        or empty($usersInClass->get($i)->contact))
                                        continue;

                                    $userId = $user->id;
                                    $contactId = $slaveAddressBook->contact->id;
                                    $name = $usersInClass->get($i)->contact->name;

                                    $existsInAddressBook = $this->addressBookRepo
                                        ->getByOwnerAndContact($userId, $contactId);

                                    if (!$existsInAddressBook && $user->contact->id != $contactId) {
                                        AddressBook::create([
                                            'user_id' => $userId,
                                            'contact_id' => $contactId,
                                            'note' => 'Contact of ' . $name
                                        ]);
                                    }
                                }
                            }

                            if ($masterAddressBooks->count()) {
                                foreach ($masterAddressBooks as $masterAddressBook) {
                                    if(empty($masterAddressBook->contact)
                                        or empty($usersInClass->get($i)->contact))
                                        continue;
                                    $userId = $usersInClass->get($i)->id;
                                    $contactId = $masterAddressBook->contact->id;
                                    $name = $user->contact->name;

                                    $existsInAddressBook = $this->addressBookRepo
                                        ->getByOwnerAndContact($userId, $contactId);

                                    if (!$existsInAddressBook && $usersInClass->get($i)->contact->id != $contactId) {
                                        AddressBook::create([
                                            'user_id' => $userId,
                                            'contact_id' => $contactId,
                                            'note' => 'Contact of ' . $name
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                });
            }
        }
    }
}
