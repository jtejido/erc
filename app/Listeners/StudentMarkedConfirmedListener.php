<?php

namespace App\Listeners;

use App\Events\StudentMarkedConfirmedEvent;
use App\Models\Contact;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Utilities\Constant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;

class StudentMarkedConfirmedListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * Create the event listener.
     *
     * @param EmailTemplateMailer $mailer
     * @param ClassRegistrationRepository $classRegistrationRepository
     * @param RegistrationOrderRepository $registrationOrderRepository
     */
    public function __construct(EmailTemplateMailer $mailer,
                                ClassRegistrationRepository $classRegistrationRepository,
                                RegistrationOrderRepository $registrationOrderRepository)
    {
        $this->mailer = $mailer;
        $this->classRegistrationRepo = $classRegistrationRepository;
        $this->registrationOrderRepo = $registrationOrderRepository;
    }

    /**
     * Handle the event.
     *
     * @param  StudentMarkedConfirmedEvent  $event
     * @return void
     */
    public function handle(StudentMarkedConfirmedEvent $event)
    {
        $user = Contact::find($event->contactId)->user;
        $regOrder = $this->registrationOrderRepo->getById($event->regOrderId);
        $classReg = $this->classRegistrationRepo->getByRegistrationAndAttendee($event->regOrderId, $event->contactId);

        if(!$user || !$regOrder || !$classReg || !$user->isActive()) {
            return ;
        }

        if ($regOrder->registrable_type === Constant::COURSE_CLASS_OBJECT) {
            $class = $regOrder->registrable()->first();

                $class_type = 'seminar';
                if ($class->course->course_type_id === Constant::WEBCAST) {
                    $class_type = 'webinar';
                }

                $_class = (object) [
                    'name'        => $class->course->title,
                    'city'        => $class->city,
                    'state'       => $class->state,
                    'start_date'  => $class->start->toDateString(),
                    'end_date'    => $class->start->toDateString(),
                    'start_time'  => $class->start->toTimeString(),
                    'end_time'    => $class->start->toTimeString(),
                    'start'       => $class->start->copy()->format('M d, Y h:i:s A'),
                    'end'         => $class->end->copy()->format('M d, Y h:i:s A'),
                    'instruction' => $class->instructions,
                    'type'        => $class_type,
                ];

        }


        if ($regOrder->registrable_type === Constant::COURSE_OBJECT) {
            $course = $regOrder->registrable()->first();
                $_class = (object) [
                    'name'        => $course->title,
                    'sku'         => $course->sku,
                    'duration'    => $course->duration,
                    'category'    => $course->category->name,
                    'type'        => 'online',
                ];

        }

        $subject = 'Certificate of Training - Environmental Resource Center';

        $data = [
            'to'      => $user->email,
            'subject' => $subject,
            'data'    => [
                'name'     => $user->contact->f_name,
                'fullname'     => $user->contact->name,
                'company'  => $user->contact->company,
                'title'    => $user->contact->title,
                'address1' => $user->contact->address1,
                'address2' => $user->contact->city.', '.$user->contact->state.' '.$user->contact->zip,
                'classes'   => [$_class],
                'status'    => ($event->status == 1) ? 'PRESENT' : 'ABSENT',
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        if ($event->status == 1) {
            $data['attachment'] = [
                'path'      => storage_path('app/' . $classReg->certificate_file_path),
                'filename'  => 'Certificate.pdf',
                'mime' => 'application/pdf'
            ];
        }

        if ($this->mailer->send('attendance-confirmation', $data) && $classReg->isConfirmed() == 1) {
            $classReg->setCertificateSent();
        }
    }

}
