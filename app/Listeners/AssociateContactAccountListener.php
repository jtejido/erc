<?php

namespace App\Listeners;

use App\Events\AssociateContactAccountEvent;
use App\Models\AddressBook;
use App\Repositories\AddressBookEntryRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssociateContactAccountListener
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var AddressBookEntryRepository
     */
    private $addressBookEntryRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepo,
                                AddressBookEntryRepository $addressBookEntryRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                RegistrationOrderRepository $registrationOrderRepo)
    {
        $this->userRepo = $userRepo;
        $this->addressBookEntryRepo = $addressBookEntryRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
    }

    /**
     * Handle the event.
     *
     * @param  AssociateContactAccountEvent  $event
     * @return void
     */
    public function handle(AssociateContactAccountEvent $event)
    {
        $registrationOrderIds = collect([]);
        $user = $this->userRepo->getById($event->user_id);
        $classRegistrations = $this->classRegistrationRepo->getByAttendeeCompletedOrInvoiced($user->contact->id);

        if ($classRegistrations->count()) {
            foreach ($classRegistrations->pluck('registration_order_id') as $regId) {
                $registrationOrderIds->push($regId);
            }

            if ($registrationOrderIds->count()) {
                $registrationOrderIds->each(function($registrationOrderId) use ($user) {
                    $usersInClass = collect();
                    $registrationOrder = $this->registrationOrderRepo->getById($registrationOrderId);

                    $registrationOrder->classRegistrations->each(function($classRegistration)
                    use ($usersInClass, $user) {
                        $contact = $classRegistration->contact;

                        if ($contact->user and
                            $contact->id != $user->contact->id) {
                            $usersInClass->push($contact->user);
                        }
                    });

                    if ($usersInClass->count()) {
                        $usersInClass->each(function($singleUserInClass) use ($user) {
                            $addressBooks = $singleUserInClass->addressBooks;

                            if ($addressBooks->count()) {
                                $addressBooks->each(function($addressBook) use ($singleUserInClass, $user) {
                                    $userId = $user->id;
                                    $contactId = $addressBook->contact->id;
                                    $name = $singleUserInClass->contact->name;

                                    $existsInAddressBook = $this->addressBookEntryRepo
                                                                ->getByOwnerAndContact($userId, $contactId);

                                    if (!$existsInAddressBook and
                                        $user->contact->id != $contactId) {
                                        AddressBook::create([
                                            'user_id' => $userId,
                                            'contact_id' => $contactId,
                                            'note' => 'Contact of ' . $name
                                        ]);
                                    }

                                });
                            }
                        });
                    }
                });
            }
        }


    }
}
