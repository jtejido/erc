<?php

namespace App\Listeners;

use Carbon\Carbon;
use App\Events\RegistrationConfirmationEvent;
use App\Facades\PhotoUtility;
use App\Facades\ReminderHistoryService;
use App\Models\InvoicedPayment;
use App\Models\Order;
use App\Models\OrderTransaction;
use App\Models\User;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\OrderRepository;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;


class UserRegistrationConfirmationListener implements ShouldQueue
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    private $event;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                OrderRepository $orderRepo)
    {
        $this->mailer = $mailer;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->orderRepo = $orderRepo;
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationConfirmationEvent  $event
     * @return void
     */
    public function handle(RegistrationConfirmationEvent $event)
    {

        $this->event = $event;

        $order      = Order::find($event->order);
        $user       = $order->user;

        $_user_data = (object) [
            'name'     => $user->contact->f_name,
            'fullname'     => $user->contact->name,
            'company'  => $user->contact->company,
            'title'    => $user->contact->title,
            'address1' => $user->contact->address1,
            'address2' => $user->contact->city.', '.$user->contact->state.' '.$user->contact->zip,
        ];

        $total = 0;
        foreach ($order->items()->get() as $item) {

            if ($item->orderable_type == Constant::YEARLY_SUBSCRIPTION) {
                $yearlySubscription = $item->orderable;

                foreach ($yearlySubscription->clientSubscriptions as $clientSubscription) {

                    $userD = (object) [
                        'name'     => $clientSubscription->agent->contact->name,
                        'company'  => $clientSubscription->agent->contact->company,
                        'title'    => $clientSubscription->agent->contact->title,
                        'address1' => $clientSubscription->agent->contact->address1,
                        'address2' => $clientSubscription->agent->contact->city.', '.
                            $clientSubscription->agent->contact->state.' '.
                            $clientSubscription->agent->contact->zip,
                    ];

                    $dataYearlySubs = [
                        'to'        => $clientSubscription->agent->email,
                        'subject'   => $yearlySubscription->type->name == Constant::WEBCASTS
                                        ? 'Annual Subscription Confirmation - Webcasts'
                                        : 'Annual Subscription Confirmation - Seminars and Webcasts',
                        'data'      => [
                            'users' => [
                                $userD,
                            ],
                            'type' => $yearlySubscription->type->name,
                            'is_webCast' => $yearlySubscription->type->name == Constant::WEBCASTS,
                            'activationCode' => $clientSubscription->userYearlySubscription->activation_code,
                            'cbtsPage' => route('courses') . '?type=3' ,
                            'coursesPage' => env('APP_URL').'/courses',
                            'homePage' => route('home'),
                        ]
                    ];

                    if($event->notify) {
                        $this->mailer->send('yearly-subscription-purchase', $dataYearlySubs);
                        $this->emailCsrAndAccounting($dataYearlySubs, 'yearly-subscription-purchase');
                    }
                }

            }

            if ($item->orderable_type == Constant::CORPORATE_SEAT) {
                $clientCredit = $item->orderable;
                $total += $clientCredit->paid_price;

                $dataCorpSeat = [
                    'to'        => $user->email,
                    'subject'   => 'Corporate Subscription Confirmation - Environmental Resource Center',
                    'data'      => [
                        'users' => [
                            $_user_data,
                        ],
                        'number' => $clientCredit->credits,
                        'price' => $clientCredit->paid_price,
                        'confirmationCode' => $item->transaction_id,
                        'cbtsPage' => route('courses') . '?type=3' ,
                        'coursesPage' => env('APP_URL').'/courses',
                        'homePage' => route('home'),
                    ]
                ];
                if($event->notify) {
                    $this->mailer->send('corp-seat-subscription-purchase', $dataCorpSeat);
                }
            }

        }

    }

    /**
     * @param $data
     * @param $slug
     */
    protected function emailCsrAndAccounting($data, $slug)
    {
        $csr = array_merge($data, [
            'to' => env('CSR_EMAIL'),
            'subject' => $data['subject'] . ' - CSR Copy',
        ]);

        if($this->event->notify_csr) {
            $this->mailer->send($slug, $csr);
        }


        $Accounting = array_merge($data, [
            'to' => env('ACCOUNTING_EMAIL'),
            'subject' => $data['subject'] . ' - Accounting Copy',
        ]);

        if($this->event->notify_accounting) {
            $this->mailer->send($slug, $Accounting);
        }
    }

}
