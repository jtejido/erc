<?php

namespace App\Listeners;

use App\Models\Order;
use Auth;
use App\Models\OrderAdjustment;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use App\Events\EmailAccountingModificationConfirmationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailAccountingModificationConfirmationListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param EmailAccountingModificationConfirmationEvent $event
     */
    public function handle(EmailAccountingModificationConfirmationEvent $event)
    {
        $added = 0;
        $deducted = 0;
        $adjustments = [];
        $paymentInvoice = null;
        $agent = $event->agent;
        $payment = $event->payment;
        $csr = Auth::user();
        $total = 0;
        $has_product = false;

        foreach ($event->order_adjustment_ids as $orderAdjustmentId) {
            $orderAdjustment = OrderAdjustment::find($orderAdjustmentId);

            if ($orderAdjustment->adjustment_action == Constant::ADDED) {
                $added += $orderAdjustment->amount;
            } else {
                $deducted += $orderAdjustment->amount;
            }

            array_push($adjustments, (object) [
                'note' => $orderAdjustment->note
            ]);
        }

        $order = $orderAdjustment->order;
        $diff = $order->total - $order->paid_total;
        if($diff > 0) {
            $added = $diff;
        } else {
            $deducted = abs($diff);
        }

        $subject = 'Order Modification Confirmation - from Environmental Resource Center';

        if (!is_null($payment) and $payment['method'] == 'invoice') {
            $invoice = $payment['object'];
            $order = Order::find($invoice->order_id);


            $po = route('invoice_image.preview', [
                'type'              => Constant::PO_IMAGE,
                'order_id'          => $order->id,
                'invoice_id'        => $invoice->id,
                'file_name'         => 'po_file.bin',
            ]);


            $cheque = route('invoice_image.preview', [
                'type'              => Constant::CHECK_IMAGE,
                'order_id'          => $order->id,
                'invoice_id'        => $invoice->id,
                'file_name'         => 'check_file.bin',
            ]);

            $po_file = storage_path('app/') . 'invoice_photos/completed/' .
                $order->id  . '/' . $invoice->id . '/po_file.bin';

            $cheque_file = storage_path('app/') . 'invoice_photos/completed/' .
                $order->id  . '/' . $invoice->id . '/check_file.bin';

            $paymentInvoice['has_po_image'] = (!is_null($invoice->po_image) and file_exists($po_file)) ? true : false;
            $paymentInvoice['has_cheque_image'] = (!is_null($invoice->check_image) and file_exists($cheque_file)) ? true : false;
            $paymentInvoice['po_imglink'] = $po;
            $paymentInvoice['cheque_imglink'] = $cheque;
            $paymentInvoice['po_number'] = (!is_null($invoice->po_number) and strlen($invoice->po_number))
                ? $invoice->po_number : 'No Purchase Order Number';
            $paymentInvoice['cheque_number'] = (!is_null($invoice->check_number) and strlen($invoice->check_number))
                ? $invoice->check_number : 'No Cheque Number';
        }

        $data = [
            'to' => $agent->email,
            'subject' => $subject,
            'data' => [
                'isCSR' => $csr->isAdminorCSR(),
                'csr' => $csr->contact->name,
                'adjustments' => (object)$adjustments,
                'additional_fee' => ($diff >= 0) ? number_format($diff, 2) : 0.00,
                'deduction' => number_format($deducted, 2),
                'refund' => ($diff < 0) ? number_format(abs($diff), 2) : 0.00,
                'hasPayment' => !is_null($payment) ? true : false,
                'isInvoice' => (!is_null($payment) and $payment['method'] == 'invoice') ? true : false,
                'payments'  => [
                    (object) $paymentInvoice
                ],
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        if ($event->notify) {
            $this->mailer->send('order-modification-confirmed-agent', $data);
        }

        if ($event->notify_csr) {
            $data['to'] = env('CSR_EMAIL');
            $data['subject'] = $data['subject'] . ' - CSR Copy';
            $this->mailer->send('order-modification-confirmed-agent', $data);
        }

        if ($event->notify_accounting) {
            $data['to'] = env('ACCOUNTING_EMAIL');
            $data['subject'] = $data['subject'] . ' - Accounting Copy';
            $this->mailer->send('order-modification-confirmed-agent', $data);
        }

    }
}
