<?php

namespace App\Listeners;

use App\Events\TrainingCancellationEvent;
use App\Models\Course;
use App\Models\CourseClass;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class TrainingCancellationListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  TrainingCancellationEvent  $event
     * @return void
     */
    public function handle(TrainingCancellationEvent $event)
    {
        $type = $event->type;
        $class_id   = $event->id;

        $subject = 'Training Cancellation - Environmental Resource Center';

        if ($type === CourseClass::class) {
            $_classes = [];
            $class = CourseClass::find($class_id);

            $class_type = 'seminar';
            if ($class->course->course_type_id === Constant::WEBCAST) {
                $class_type = 'webinar';
            }

            $_class = (object) [
                'name'        => $class->course->title,
                'city'        => $class->city,
                'state'       => $class->state,
                'start_date'  => $class->start->format('M d, Y'),
                'end_date'    => $class->end->format('M d, Y'),
                'start_time'  => $class->start->format('g:iA'),
                'end_time'    => $class->end->format('g:iA'),
                'start'       => $class->start->copy()->format('M d, Y g:iA'),
                'end'         => $class->end->copy()->format('M d, Y g:iA'),
                'type'        => $class_type,
            ];

            $_classes[] = $_class;

            foreach ($class->registrations as $registration)
            {
                foreach ($registration->classRegistrations as $classRegistration)
                {
                    $contact = $classRegistration->contact;
                    $contact_name = $contact->name;
                    if (!$contact->user) {
                        // Only proceed if contact has email
                        continue;
                    }
                    $user = $contact->user;

                    $data = [
                        'to'        => $user->email,
                        'subject'   => $subject,
                        'data'      => [
                            'name'  => $contact_name,
                            'classes'   => $_classes,
                            'cbtsPage' => route('courses') . '?type=3' ,
                            'coursesPage' => route('courses'),
                            'homePage' => route('home'),
                        ]
                    ];
                    $this->mailer->send('reg-cancellation', $data);
                }
            }
        } else if ($type === Course::class) {
            abort(500, 'No implementation defined yet');
        } else {
            abort(500, 'Unhandled Training Cancellation');
        }

    }

}