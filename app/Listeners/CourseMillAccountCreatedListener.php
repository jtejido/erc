<?php

namespace App\Listeners;

use App\Events\CourseMillAccountCreatedEvent;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseMillAccountCreatedListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CourseMillAccountCreatedEvent  $event
     * @return void
     */
    public function handle(CourseMillAccountCreatedEvent $event)
    {
        $user = $event->user;
        $accounts = $event->accounts->all();

        $hasMultiple = (count($accounts) > 1) ? true : false;

        $subject = 'Online Training Account Created - Environmental Resource Center';


        $data = [
            'to'      => $user->email,
            'subject' => $subject,
            'data'    => [
                'name'     => $user->contact->f_name,
                'hasMultiple'   => $hasMultiple,
                'accounts' => (object) $accounts,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        $this->mailer->send('course-mill-account-create', $data);
    }
}
