<?php

namespace App\Listeners;

use App\Events\CustomerRegisteredEvent;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;
use App\Services\Mailer\RegistrationMailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CustomerRegisteredListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CustomerRegisteredEvent  $event
     * @return void
     */
    public function handle(CustomerRegisteredEvent $event)
    {
        $user = $event->user;
        $password = $event->password;

        $subject = 'Welcome to Environmental Resource Center';

        $data = [
            'to' => $user->email,
            'subject' => $subject,
            'data'    => [
                'users' => [
                    (object) [
                        'name'     => $user->contact->f_name,
                        'password' => $password,
                        'email'    => $user->email,
                    ],
                ],
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];
        
        $this->mailer->send('ua-register', $data);

    }
}
