<?php

namespace App\Listeners;

use App\Events\ClassAttendeeWasRescheduled;
use App\Models\ClassRegistration;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClassAttendeeWasRescheduledListener implements ShouldQueue
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        //
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ClassAttendeeWasRescheduled  $event
     * @return void
     */
    public function handle(ClassAttendeeWasRescheduled $event)
    {

        $current_classRegistration = ClassRegistration::withTrashed()->find($event->current_classRegistration);
        $classRegistration = ClassRegistration::find($event->classRegistration);

        $attendee_name = $classRegistration->contact->name;
        $old_date = $current_classRegistration->registrationOrder()->withTrashed()->first()->date();
        $new_date = $classRegistration->registrationOrder->date();
        $course_name = $classRegistration->registrationOrder->title();

        if($classRegistration->contact->user) {
            $agent_email = $classRegistration->contact->user->email;
        } else {
            $agent_email = $classRegistration->registrationOrder->agent->email;
        }


        try {
            $csr_name = $classRegistration->registrationOrder->orderItem->order->csr->contact->name;
        } catch (\Exception $e) {
            $csr_name = 'ERC Admin';
        }

        $slug = 'class-reschedule';

        $subject = "Class Registration Rescheduled - Environmental Resource Center";

        $data = [
            'to'        => $agent_email,
            'subject'   => $subject,
            'data'      => [
                'name'  => $attendee_name,
                'oldDate'   => $old_date,
                'newDate'   => $new_date,
                'courseName'    => $course_name,
                'csrName'    => $csr_name,

            ]
        ];

        if($event->notify)
        $this->mailer->send($slug, $data);

        $this->emailCsrAndAccounting($data, $slug);

    }

    protected function emailCsrAndAccounting($data, $slug)
    {
        $csr = array_merge($data, [
            'to' => env('CSR_EMAIL'),
            'subject' => $data['subject'] . ' - CSR Copy',
        ]);

        $this->mailer->send($slug, $csr);

        $Accounting = array_merge($data, [
            'to' => env('ACCOUNTING_EMAIL'),
            'subject' => $data['subject'] . ' - Accounting Copy',
        ]);

        $this->mailer->send($slug, $Accounting);
    }
}
