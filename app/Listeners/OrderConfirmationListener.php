<?php

namespace App\Listeners;

use App\Events\RegistrationConfirmationEvent;
use App\Facades\OrderUtility;
use App\Facades\ReminderHistoryService;
use App\Models\ClassCombinationDiscount;
use App\Models\ClassRegistration;
use App\Models\ClientCorporateSeatCredit;
use App\Models\ClientYearlySubscription;
use App\Models\Order;
use App\Models\PaymentTransaction;
use App\Models\ProductOrders;
use App\Models\RegistrationOrder;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmationListener implements ShouldQueue
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param EmailTemplateMailer $mailer
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationConfirmationEvent  $event
     * @return void
     */
    public function handle(RegistrationConfirmationEvent $event)
    {
        $this->event = $event;

        $order      = Order::find($event->order);
        $user       = $order->user;
        $show_policy = false;

        $_user_data = (object) [
            'name'     => $user->contact->f_name,
            'fullname' => $user->contact->name,
            'company'  => $user->contact->company,
            'title'    => $user->contact->title,
            'address1' => $user->contact->address1,
            'address2' => $user->contact->city.', '.$user->contact->state.' '.$user->contact->zip,
        ];

        $pt = $order->getTransaction();

        $transactions = $this->extractItems($pt->items);

        $subject = 'Order Confirmation - Environmental Resource Center';

        $data = [
            'to'        => $user->email,
            'subject'   => $subject,
            'data'      => [
                'users'         => [
                    $_user_data,
                ],
                'orderDate'     => $order->completedDateFormatted(),
                'isInvoiced'     => $order->is_invoiced,
                'isCard'        => $order->is_card,
                'paymentMethod' => $order->payment_method,
                'transactions'  => $transactions,
                'shippingFee'   => $pt->shipping_fee,
                'stateTax'      => $pt->state_tax_formatted,

            ]
        ];

        if($order->is_invoiced) {

            $data['data'] = array_merge($data['data'], [
                'poNum'          => $pt->po_number,
                'paymentType'    => 'Purchase Order',
                'totalPaid'      => $pt->trans_total_formatted,
            ]);

        } else {

            $data['data'] = array_merge($data['data'], [
                'ccNum'         => $pt->masked_cc,
                'isCheck'       => $pt->isCheck(),
                'paymentType'   => $pt->card_brand,
                'totalPaid'     => $pt->amount_formatted,
            ]);

        }

        if($event->notify) {
            $slug = 'order-confirmation';
            if ($this->mailer->send($slug, $data)) {
                $params = [];
                $params['to'] = $user->id;
                $params['subject'] = $subject;
                $params['data'] = serialize($data);
                $params['slug'] = $slug;
                $params['remindable_id'] = $order->id;
                $params['remindable_type'] = 'Order';
                ReminderHistoryService::log($params);
            }
        }

        $this->emailCsrAndAccounting($data, 'order-confirmation');

    }

    public function extractItems($items)
    {

        try {
            $items = json_decode($items);
        } catch (\Exception $e) {
            return [];
        }

        $data = [];
        $data_discount = [];

        foreach ($items->items as $item) {
            try {
                $e_item = $this->extractItem($item);
            } catch (\Exception $e) {
                $e_item = [];
            }
            if(!empty($e_item))
                $data = array_merge($data, $e_item);
        }

        $data_discount = [];
        // get discounts
        foreach ($items->items as $item) {
            try {
                $e_item = $this->extractDiscount($item, $data_discount);
            } catch (\Exception $e) {
                $e_item = [];
            }
            if(!empty($e_item))
                $data_discount = array_merge($data_discount, $e_item);
        }

        if(!empty($data_discount))
            $data = array_merge($data, $data_discount);

        return $data;
    }

    protected function extractDiscount($item, $data_discount = [])
    {

        $object = $item->object;
        $contents = $item->contents;
        $title = 'No Title';
        $qty = '';
        $price = '';
        $_data = [];
        foreach ($contents as $content) {
            if ($object == Constant::REGISTRATION_ORDER) {

                $classRegistration = ClassRegistration::find($content->id);
                $classCombinationDiscount = ClassCombinationDiscount::where('class_reg_one', $content->id)
                    ->orWhere('class_reg_two', $content->id)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->first();

                if ($classRegistration->subscriptionDiscount) {
                    $subscriptionDiscount = $classRegistration->subscriptionDiscount;
                    if ($subscriptionDiscount->deduction > 0) {
                        $title = 'Annual Subscription Discount - '. $classRegistration->contact->name;
                        $price = '- $'.number_format($subscriptionDiscount->deduction, 2);
                    }
                    $_data[] = (object) [
                        'item'  => $title,
                        'qty'   => $qty,
                        'price' => $price
                    ];
                }

                if ($classRegistration->classMilitaryDiscount) {
                    $cmDiscount = $classRegistration->classMilitaryDiscount;
                    $title = 'GSA Pricing Discount - '.$classRegistration->contact->name;
                    $price = '- $'.number_format($cmDiscount->deduction, 2);
                    $_data[] = (object) [
                        'item'  => $title,
                        'qty'   => $qty,
                        'price' => $price
                    ];
                }

                if ($classRegistration->is_half_priced and !$classRegistration->is_cancelled and $classRegistration->classHalfPriceDiscount) {
                    $chDiscount = $classRegistration->classHalfPriceDiscount;
                    if ($chDiscount->deduction > 0) {
                        $title = '1/2 Price Discount - '.$classRegistration->contact->name;
                        $price = '- $'.number_format($chDiscount->deduction, 2);
                        $_data[] = (object) [
                            'item'  => $title,
                            'qty'   => $qty,
                            'price' => $price
                        ];
                    }
                }

                if (!is_null($classCombinationDiscount)) {
                    $amount = 100;
                    $title = 'HWM-DOT Discount - '.$classRegistration->contact->name;
                    $price = '- $'.number_format($amount, 2);
                    $obj = (object) [
                        'item'  => $title,
                        'qty'   => $qty,
                        'price' => $price
                    ];

                    if(!in_array($obj, $data_discount)) $_data[] = $obj;

                }

                if ($classRegistration->classCouponDiscounts->count()) {
                    foreach($classRegistration->classCouponDiscounts as $classCouponDiscount) {
                        $title = 'Coupon Discount - '.$classRegistration->contact->name;
                        $price = '- $'.number_format($classCouponDiscount->deduction, 2);
                        $_data[] = (object) [
                            'item'  => $title,
                            'qty'   => $qty,
                            'price' => $price
                        ];
                    }
                }

                if ($classRegistration->classCreditDiscount and !$classRegistration->classCreditDiscount->is_deleted) {
                    $creditDiscount = $classRegistration->classCreditDiscount;
                    $title = 'Corporate Seat Discount - '.$classRegistration->contact->name;
                    $price = '- $'.number_format($creditDiscount->deduction, 2);
                    $_data[] = (object) [
                        'item'  => $title,
                        'qty'   => $qty,
                        'price' => $price
                    ];
                }

            }
        }

        return $_data;

    }

    protected function extractItem($item)
    {
        $object = $item->object;
        $contents = $item->contents;
        $title = 'No Title';
        $qty = '';
        $_data = [];

        if ($object == Constant::REGISTRATION_ORDER or
            $object == Constant::YEARLY_SUBSCRIPTION or
            $object == Constant::CORPORATE_SEAT or
            $object == Constant::PRODUCT_ORDERS) {
            foreach ($contents as $content) {

                $_price = $content->price_paid;

                if ($object == Constant::REGISTRATION_ORDER) {
                    $classRegistration = ClassRegistration::find($content->id);
                    $title = OrderUtility::getPaymentClassTitle($item->id) . ' - ' . $classRegistration->contact->name;
                    $registrationOrder = RegistrationOrder::find($item->id);
                    $_price = $registrationOrder->originalPrice();
                } elseif ($object == Constant::YEARLY_SUBSCRIPTION) {
                    $title = 'Yearly Subscription';
                    $yearlySub = ClientYearlySubscription::find($content->id) ;
                    $_price = $yearlySub->yearlySubscription->type->price;
                } elseif ($object == Constant::CORPORATE_SEAT) {
                    $title = 'Corporate Seat';
                    $corporateCredit = ClientCorporateSeatCredit::find($content->id);
                    $_price = $corporateCredit->raw_rate;
                } elseif ($object == Constant::PRODUCT_ORDERS) {
                    $product = ProductOrders::find($item->id);
                    $title = $product->title();
                    $qty = $content->quantity;
                    $_price = $product->getProductOrderPrice(1);
                }

                $price = '$'.number_format($_price, 2);

                $_data[] = (object) [
                    'item'  => $title,
                    'qty'   => $qty,
                    'price' => $price
                ];

            }
        }


        $qty = '';

        if ($object == Constant::ORDER_ADJUSTMENT_OBJECT) {
            foreach ($contents as $content) {
                $title = 'Order Adjustment - '. $content->description or '';
                $price = '$'.number_format($content->price_paid, 2);
                $_data[] = (object) [
                    'item'  => $title,
                    'qty'   => $qty,
                    'price' => $price
                ];
            }
        }

        return $_data;
    }

    protected function emailCsrAndAccounting($data, $slug)
    {
        $csr = array_merge($data, [
            'to' => env('CSR_EMAIL'),
            'subject' => $data['subject'] . ' - CSR Copy',
        ]);

        if($this->event->notify_csr) {
            $this->mailer->send($slug, $csr);
        }


        $Accounting = array_merge($data, [
            'to' => env('ACCOUNTING_EMAIL'),
            'subject' => $data['subject'] . ' - Accounting Copy',
        ]);

        if($this->event->notify_accounting) {
            $this->mailer->send($slug, $Accounting);
        }
    }

}
