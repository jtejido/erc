<?php

namespace App\Listeners;

use App\Events\SubscriptionUsedEvent;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionUsedListener
{

    private $mailer;
    protected $event;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionUsedEvent  $event
     * @return void
     */
    public function handle(SubscriptionUsedEvent $event)
    {
        $this->event = $event;
        $agent = $event->agent;
        $classRegistrations = $event->classRegistrations;
        $yearlySubscriptionUsed = [];
        $creditSubscriptionUsed = [];

        foreach ($classRegistrations as $classRegistration) {
            $registrationOrder = $classRegistration->registrationOrder;
            $name = extract_name($classRegistration->contact);

            if ($classRegistration->subscriptionDiscount) {

                array_push($yearlySubscriptionUsed, (object) [
                    'title' => $name . ' - ' . $registrationOrder->title() . ' registration.'
                ]);
            }

            if ($classRegistration->classCreditDiscount) {
                array_push($creditSubscriptionUsed, (object) [
                    'title' => $name . ' - ' . $registrationOrder->title() . ' registration.'
                ]);
            }
        }

        if (count($creditSubscriptionUsed)) {

            $data = [
                'to' => $agent->email,
                'subject' => 'Corporate Subscription Used - Environmental Resource Center',
                'data' => [
                    'name' => $agent->contact->f_name,
                    'hasYearlySubscription' => count($yearlySubscriptionUsed) > 0 ? true : false,
                    'yearlySubscriptions' => (object)$yearlySubscriptionUsed,
                    'hasCreditSubscription' => count($creditSubscriptionUsed) > 0 ? true : false,
                    'creditSubscriptions' => (object)$creditSubscriptionUsed,
                    'cbtsPage' => route('courses') . '?type=3' ,
                    'coursesPage' => route('courses'),
                    'homePage' => route('home'),
                ]
            ];

            if($event->notify) {
                $this->mailer->send('subscription-used', $data);
            }

            $this->emailCsrAndAccounting($data, 'subscription-used');

        }

    }

    /**
     * @param $data
     * @param $slug
     */
    protected function emailCsrAndAccounting($data, $slug)
    {
        $csr = array_merge($data, [
            'to' => env('CSR_EMAIL'),
            'subject' => $data['subject'] . ' - CSR Copy',
        ]);

        if($this->event->notify_csr) {
            $this->mailer->send($slug, $csr);
        }


        $Accounting = array_merge($data, [
            'to' => env('ACCOUNTING_EMAIL'),
            'subject' => $data['subject'] . ' - Accounting Copy',
        ]);

        if($this->event->notify_accounting) {
            $this->mailer->send($slug, $Accounting);
        }
    }

}
