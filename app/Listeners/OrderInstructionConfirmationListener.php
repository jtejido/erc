<?php

namespace App\Listeners;

use App\Events\RegistrationConfirmationEvent;
use App\Models\Order;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderInstructionConfirmationListener implements ShouldQueue
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param EmailTemplateMailer $mailer
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationConfirmationEvent  $event
     * @return void
     */
    public function handle(RegistrationConfirmationEvent $event)
    {

        $this->event = $event;

        $order      = Order::find($event->order);

        if(!$order->has_class_registration) return ;

        $user       = $order->user;

        $_user_data = (object) [
            'name'     => $user->contact->f_name,
            'fullname' => $user->contact->name,
            'company'  => $user->contact->company,
            'title'    => $user->contact->title,
            'address1' => $user->contact->address1,
            'address2' => $user->contact->city.', '.$user->contact->state.' '.$user->contact->zip,
        ];

        $subject = 'Training Instruction - Environmental Resource Center';

        $data = [
            'to'        => $user->email,
            'subject'   => $subject,
            'data'      => [
                'users'         => [
                    $_user_data,
                ],
                'orderDate'     => $order->completedDateFormatted(),
                'paymentMethod' => $order->payment_method,
                'fbUrl'         => 'https://www.facebook.com',
                'orderItems'    => $this->extractItems($order),
                'hasMultipleClasses'    => $order->has_multiple_classes,
                'hasClassInCary'        => $order->has_class_in_cary,
                'hasSeminar'            => $order->has_seminar,
                'hasCbt'                => $order->has_cbt,
                'hasWebcast'            => $order->has_webcast,
                'hasCmid'               => $user->contact->has_cmid,
                'courseMillUser'        => $user->contact->course_mill_user_id,
                'hasOtherAttendees'     => $order->has_other_attendees
            ]
        ];

        if($event->notify) {
            $this->mailer->send('class-instruction', $data);
        }

    }

    private function extractItems($order)
    {

        $user       = $order->user;
        $order_items = [];
        $hasMultipleClasses = ($order->items()->registrationOrders()->count() > 1) ? true : false;

        foreach ($order->items()->registrationOrders()->get() as $item) {

            $_class = [];

            $registration_order = $item->orderable()->first();

            $is_attendee = $registration_order->isAttendee($user->contact->id);

            if ($registration_order->registrable_type === Constant::COURSE_CLASS_OBJECT) {

                $class = $registration_order->registrable()->first();

                $_attendees = [];

                $class_registrations = $registration_order->classRegistrations()->get();

                foreach ($class_registrations as $class_registration) {
                    if ($class_registration->contact) {

                        $_contact = $class_registration->contact;
                        $_contact_name = $_contact->name;

                        if (!$class_registration->contact->user) {
                            $_contact_name .= ' (No Email)';
                        }

                        $_attendees[] = (object) [
                            'name' => $_contact_name
                        ];
                    }
                }

                $_courses = $this->getRelatedCourses($class->course->courses);

                $webinar_link = '';
                $exam_link = '';

                if ($registration_order->isWebcast()) {
                    $webinar_link = $class->course->meetingLink('Webcast');
                    $exam_link = $class->course->examLink('Test');
                }

                $_class = [
                    'name'              => $class->course->title,
                    'cname'             => course_title_add_the($class->course->title),
                    'city'              => $class->city,
                    'state'             => $class->state,
                    'startDate'         => $class->start->toDateString(),
                    'endDate'           => $class->start->toDateString(),
                    'startTime'         => $class->start_time_formatted,
                    'regTime'           => $class->reg_time_formatted,
                    'endTime'           => $class->end_time_formatted,
                    'instruction'       => $class->instructions,
                    'dateRange'         => $class->dates_for_email,
                    'locationName'      => $class->location_name_linked,
                    'location_link'     => $class->location_link,
                    'locationAddress'   => $class->location_address,
                    'locationPhone'     => $class->location_phone,
                    'locationGeneral'   => $class->short_city,
                    'isCary'            => $class->is_cary,
                    'attendees'         => $_attendees,
                    'webinar'           => $webinar_link,
                    'exam'              => $exam_link,
                    'courses'           => $_courses,
                    'hasCourse'         => (!empty($_courses)) ? true : false,
                    'course_id'         => $class->course->id,
                ];

            }

            if ($registration_order->registrable_type === Constant::COURSE_OBJECT) {

                $course = $registration_order->registrable()->first();

                $_attendees = [];

                $class_registrations = $registration_order->classRegistrations()->get();
                foreach ($class_registrations as $class_registration) {
                    if ($class_registration->contact) {
                        $_contact = $class_registration->contact;
                        $_contact_name = $_contact->name;

                        if (!$class_registration->contact->user) {
                            $_contact_name .= ' (No Email)';
                        }

                        $_attendees[] = (object) [
                            'name' => $_contact_name
                        ];
                    }
                }

                $categories = '';

                foreach ($course->courseCategories as $courseCategory) {
                    $categories = $categories . "\n" . $courseCategory->category->name;
                }

                $_class = [
                    'course_id'         => $course->id,
                    'name'              => $course->title,
                    'cname'             => course_title_add_the($course->title),
                    'sku'               => $course->sku,
                    'duration'          => $course->duration,
                    'category'          => $categories,
                    'attendees'         => $_attendees,
                    'hasCourse'         => (!empty($_courses)) ? true : false,
                    'courseMillUser'    => 'CourseMillUser'
                ];

            }

            $_item['isAttendee'] = $is_attendee;
            $_item['isSeminar'] = $registration_order->isSeminar();
            $_item['isWebinar'] = $registration_order->isWebcast();
            $_item['isOnline'] = $registration_order->isCourse();
            $_item['hasMultipleClasses'] = $hasMultipleClasses;

            $order_items[] = (object) array_merge($_item, $_class);

        }

        return $order_items;

    }

    private function getRelatedCourses($courses)
    {

        $_courses = [];

        if ($courses->count()) {
            foreach ($courses as $crs) {
                if(!string_in_arr_of_obj($crs->publicUrl, $_courses, 'course_link')) {
                    $_courses[] = (object)[
                        'title' =>  $crs->title,
                        'courseLink' => $crs->publicUrl
                    ];
                }
            }
        }

        return $_courses;

    }

}
