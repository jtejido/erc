<?php

namespace App\Listeners;

use App\Events\EmailAccountingCancellationEvent;
use App\Repositories\ClassRegistrationRepository;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facades\OrderModificationUtility;

class EmailAccountingCancellationListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->mailer = $mailer;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * Handle the event.
     *
     * @param  EmailAccountingCancellationEvent  $event
     * @return void
     */
    public function handle(EmailAccountingCancellationEvent $event)
    {
        $agent = $event->agent;
        $user = $event->user;
        $attendeeClass = [];

        foreach ($event->classRegistrationIds as $classRegistrationId) {
            $classRegistration = $this->classRegistrationRepo->getById($classRegistrationId);
            $registrationOrder = $classRegistration->registrationOrder;

            array_push($attendeeClass, (object) [
                'attendee' => $classRegistration->contact->name,
                'class' => $registrationOrder->title() . ' ' . $registrationOrder->dates_for_email
            ]);
        }

        $data = [
            'to' => $agent->email,
            'subject' => 'Class Registration Cancelled - Environmental Resource Center',
            'data' => [
                'user' => $user->contact->f_name,
                'registrations' => (object) $attendeeClass,
                'csrName' => $user->contact->name,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
                'isMember'     => $user->isMember()
            ]
        ];

        if($event->notify) {
            $this->mailer->send('class-registration-cancel-agent', $data);
        }

        if ($event->notify_csr) {
            $data['to'] = env('CSR_EMAIL');
            $data['subject'] = $data['subject'] . ' - CSR Copy';
            $this->mailer->send('class-registration-cancel-agent', $data);
        }

        if ($event->notify_accounting) {
            $data['to'] = env('ACCOUNTING_EMAIL');
            $data['subject'] = $data['subject'] . ' - Accounting Copy';
            $this->mailer->send('class-registration-cancel-agent', $data);
        }

    }
}
