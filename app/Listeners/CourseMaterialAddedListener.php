<?php

namespace App\Listeners;

use App\Events\CourseMaterialAddedEvent;
use App\Facades\CourseUtility;
use App\Models\RegistrationOrder;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;

class CourseMaterialAddedListener
{
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CourseMaterialAddedEvent  $event
     * @return void
     */
    public function handle(CourseMaterialAddedEvent $event)
    {
        $file = $event->file;
        $course  = $event->course;

        if (class_basename($course) == Constant::COURSE) {
            if ($course->classes->count()) {
                foreach ($course->classes as $class) {
                    if (CourseUtility::isClassStartedNotExpired($class)) {
                        $registrationOrders = RegistrationOrder::where([
                            'registrable_id' => $class->id,
                            'registrable_type' => Constant::COURSE_CLASS_OBJECT
                        ])->get();

                        if ($registrationOrders->count()) {
                            foreach ($registrationOrders as $registrationOrder) {
                                $agent = $registrationOrder->agent;

                                if (!$agent->isActive()) continue;

                                $subject = 'New Course Material Added - Environmental Resource Center';

                                $data = [
                                    'to' => $agent->email,
                                    'subject' => $subject,
                                    'data' => [
                                        'name' => $agent->contact->f_name,
                                        'link' => $file->is_file ? route('main.course.getmaterial', [$file->filename]) : $file->path,
                                        'title' => $course->title . ' ' . $class->date_range,
                                        'cbtsPage' => route('courses') . '?type=3',
                                        'coursesPage' => route('courses'),
                                        'homePage' => route('home'),
                                        'fbUrl' => '',
                                        'course_name' => $course->title,
                                        'start_date_and_time' => $class->date_range,
                                    ]
                                ];
                                if ($event->notify) {
                                    $this->mailer->send('course-material-added', $data);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
