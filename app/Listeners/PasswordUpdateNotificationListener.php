<?php

namespace App\Listeners;

use App\Events\ProfileUpdateEvent;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PasswordUpdateNotificationListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  ProfileUpdateEvent  $event
     * @return void
     */
    public function handle(ProfileUpdateEvent $event)
    {
        // only proceed for profile password updates
        if (!$event->is_password) {
            return;
        }

        $user = User::find($event->user_id);

        $this->initTemplate();
        $subject = 'Password Successfully Changed!';

        $contact_name = $user->email;
        if ($user->contact) {
            $contact_name = $user->contact->name;
        }

        $data = [
            'to' => $user->email,
            'subject' => $subject,
            'data' => [
                'name' => $contact_name,
                'updates' => [
                    (object) [
                        'type'      => 'password',
                        'email'     => $user->email,
                    ],
                ],
                'link' => env('APP_URL').'/login#forgot_password',
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        $this->mailer->send('ua-profile-update', $data);

    }

    public function initTemplate() {
        $template = EmailTemplate::bySlug('ua-profile-update');
        $hasTemplate = ($template instanceof EmailTemplate);
        $hasContent = false;
        
        if (!$hasTemplate) {
            $template = EmailTemplate::create([
                'slug'        => 'ua-profile-update',
                'description' => 'Profile Update'
            ]);
        }

        $hasContent = ($template->contents()->count() > 0);
        if ($hasTemplate && !$hasContent) {
            $template->contents()->save(new EmailTemplateContent([
                'content' => EmailTemplate::buildContent($template->slug)
            ]));
        }
    }
}
