<?php

namespace App\Listeners;

use App\Events\SwapUserEvent;
use App\Models\Contact;
use App\Models\RegistrationOrder;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAgentWhenSwapListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SwapUserEvent  $event
     * @return void
     */
    public function handle(SwapUserEvent $event)
    {
        $added = collect();
        $removed = collect();
        $actor = User::find($event->actor_id);
        $user = User::find($event->user_id);
        $registrationOrder = RegistrationOrder::find($event->registrable_id);

        foreach ($event->added as $addedId) {
            $addedContact = Contact::find($addedId);

            $added->push((object) [
                'attendee' => $addedContact->name
            ]);
        }

        foreach ($event->removed as $removedId) {
            $removedContact = Contact::find($removedId);

            $removed->push((object) [
                'attendee' => $removedContact->name
            ]);
        }

        $subject = 'Attendees Swapped - Environmental Resource Center';

        $agentData = [
            'to'      => $user->email,
            'subject' => $subject,
            'data'    => [
                'name' => $user->contact->f_name,
                'actor' => $actor->contact->name,
                'title' => $registrationOrder->title(),
                'rcontacts' => (object) $removed->all(),
                'acontacts' => (object) $added->all(),
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        if($event->notify) {
            $this->mailer->send('notify-swap-agent', $agentData);
        }

        if($event->notify_csr) {

            $agentData['to'] = env('CSR_EMAIL');
            $agentData['subject'] = $agentData['subject'] . ' - CSR Copy';

            $this->mailer->send('notify-swap-agent', $agentData);
        }

    }
}
