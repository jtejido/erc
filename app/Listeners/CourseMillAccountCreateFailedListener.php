<?php

namespace App\Listeners;

use App\Events\CourseMillAccountCreateFailedEvent;
use App\Models\Order;
use App\Utilities\Constant;
use Mail;

class CourseMillAccountCreateFailedListener
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  CourseMillAccountCreateFailedEvent  $event
     * @return void
     */
    public function handle(CourseMillAccountCreateFailedEvent $event)
    {

        $notify_users = env('ERC_SYSAD','greg@cspreston.com,shall@ercweb.com');

        $registrationOrder = $event->registrationOrder;

        $course = $registrationOrder->registrable;
        $attendees = collect();
        $classRegistrations = $registrationOrder->classRegistrations;

        $classRegistrations->each(function($classRegistration) use ($attendees) {
            $attendees->push($classRegistration->contact);
        });

        $data = [
            'accounts' => $attendees,
            'course'    => $course,
            'error_message' => $event->exception->getMessage()
        ];

        foreach (explode(',', $notify_users) as $notify_user) {
            Mail::send('emails.coursemill.failedCreateStudentFromRegistration',
                $data,
                function($m) use ($notify_user) {
                    $m->to($notify_user)->subject('ERC - Coursemill - Failed To Create Student');
                });
        }


    }
}
