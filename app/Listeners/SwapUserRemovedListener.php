<?php

namespace App\Listeners;

use App\Events\SwapUserEvent;
use App\Models\Contact;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\RegistrationOrder;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SwapUserRemovedListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  SwapUserEvent  $event
     * @return void
     */
    public function handle(SwapUserEvent $event)
    {
        $registrable_id = $event->registrable_id;
        $removed = $event->removed;

        $subject = 'Student Update - Environmental Resource Center';

        $registration_order = RegistrationOrder::find($registrable_id);
        $_classes = collect($this->processRegistrationOrder($registration_order))->each(function(&$item, $key) {
            $item->nominated = 'removed';
        })->toArray();

        foreach ($removed as $contact_id) {

            $contact = Contact::find($contact_id);

            if ($contact->user) {
                $user    = $contact->user;

                $_user_data = (object) [
                    'name'      => $contact->name,
                    'company'   => $contact->company,
                    'title'     => $contact->title,
                    'address1'  => $contact->address1,
                    'address2'  => $contact->city.', '.$contact->state.' '.$contact->zip,
                ];


                $contact_name = $user->email;
                if ($user->contact) {
                    $contact_name = $user->contact->name;
                }

                $data = [
                    'to'        => $user->email,
                    'subject'   => $subject,
                    'data'      => [
                        'name'      => $contact_name,
                        'users' => [
                            $_user_data,
                        ],
                        'classes'   => $_classes,
                        'cbtsPage' => route('courses') . '?type=3' ,
                        'coursesPage' => route('courses'),
                        'homePage' => route('home'),
                    ]
                ];

                if($event->notify) {
                    $this->mailer->send('reg-training-swap-user', $data);
                }

            }
            
        }

    }

    public function initTemplate() {
        $template = EmailTemplate::bySlug('reg-training-swap-user');
        $hasTemplate = ($template instanceof EmailTemplate);
        $hasContent = false;
        
        if (!$hasTemplate) {
            $template = EmailTemplate::create([
                'slug'        => 'reg-training-swap-user',
                'description' => 'Confirmation - Swap'
            ]);
        }

        $hasContent = ($template->contents()->count() > 0);
        if ($hasTemplate && !$hasContent) {
            $template->contents()->save(new EmailTemplateContent([
                'content' => EmailTemplate::buildContent($template->slug)
            ]));
        }
    }

    public function processRegistrationOrder($registration_order) {
        if (is_int($registration_order)) {
            $registration_order = RegistrationOrder::find($registration_order);
        }

        // $registration_order->status = $order->status;
        // $registration_order->save();
        // $total += $registration_order->classRegistrations()->sum('item_charge');
        $_classes = [];
        if ($registration_order->registrable_type === Constant::COURSE_CLASS_OBJECT) {

            $classes = $registration_order->registrable()->get();
            foreach ($classes as $class) {
                // xdebug_break();
                // $_attendees = [];
                
                $class_registrations = $registration_order->classRegistrations()->get();
                foreach ($class_registrations as $class_registration) {
                    if ($class_registration->contact) {
                        $_contact = $class_registration->contact;
                        $_contact_name = $_contact->name;

                        if (!$class_registration->contact->user) {
                            $_contact_name .= ' (No Email)';
                        }

                        // $name    = $_contact->name;
                        // $_attendees[] = (object) [
                        //     'name' => $_contact_name
                        // ];
                    }
                }

                $class_type = 'seminar';
                if ($class->course->course_type_id === Constant::WEBCAST) {
                    $class_type = 'webinar';
                }

                $_class = (object) [
                    'name'        => $class->course->title,
                    'city'        => $class->city,
                    'state'       => $class->state,
                    'start_date'  => $class->start->toDateString(),
                    'end_date'    => $class->start->toDateString(),
                    'start_time'  => $class->start->toTimeString(),
                    'end_time'    => $class->start->toTimeString(),
                    'start'       => $class->start->copy()->format('M d, Y h:i:s A'),
                    'end'         => $class->end->copy()->format('M d, Y h:i:s A'),
                    'instruction' => $class->instructions,
                    'type'        => $class_type,
                    // 'attendees'   => $_attendees,
                ];
                $_classes[] = $_class;

            }
        }

        if ($registration_order->registrable_type === Constant::COURSE_OBJECT) {

            $courses = $registration_order->registrable()->get();
            
            foreach ($courses as $course) {
                // $_attendees = [];

                // $class_registrations = $registration_order->classRegistrations()->get();
                // foreach ($class_registrations as $class_registration) {
                //     if ($class_registration->contact) {
                //         $_contact = $class_registration->contact;
                //         $_contact_name = $_contact->name;

                //         if (!$class_registration->contact->user) {
                //             $_contact_name .= ' (No Email)';
                //         }

                //         // $name    = $_contact->name;
                //         // $_attendees[] = (object) [
                //         //     'name' => $_contact_name
                //         // ];
                //     }
                // }

                $categories = '';

                foreach ($course->courseCategories as $courseCategory) {
                    $categories = $categories . "\n" . $courseCategory->category->name;
                }

                $_class = (object) [
                    'name'        => $course->title,
                    'sku'         => $course->sku,
                    'duration'    => $course->duration,
                    'category'    => $categories,
                    'type'        => 'online',
                    // 'attendees'   => $_attendees,
                ];

                $_classes[] = $_class;

            }
        }

        return $_classes;
    }
}
