<?php

namespace App\Listeners;

use App\Events\AddressBookEntryEvent;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddressBookNotificationListener
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  AddressBookEntryEvent  $event
     * @return void
     */
    public function handle(AddressBookEntryEvent $event)
    {
        $user      = User::find($event->user_id);
        $details   = $event->details;
        $password  = $event->password;

        $new = false;
        if (strlen($password) > 0) {
            $new = 'true';
        }
        

        $contact_user = User::whereEmail($event->details['email'])->first();

        $this->initTemplate();
        $subject = $user->contact->name.' created an account for you at Environmental Resource Center';

        $data = [
            'to' => $contact_user->email,
            'subject' => $subject,
            'data' => [
                'users' => [
                    (object) [
                        'new' => $new,
                        'name' => $details['first_name'],
                        'agent'    => $user->contact->name,
                        'password' => $password,
                        'email'    => $details['email'],
                    ]
                ],

                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];
        
        $this->mailer->send('ua-contact-add', $data);
    }

    public function initTemplate() {
        $template = EmailTemplate::bySlug('ua-contact-add');
        $hasTemplate = ($template instanceof EmailTemplate);
        $hasContent = false;

        
        if (!$hasTemplate) {
            $template = EmailTemplate::create([
                'slug'        => 'ua-contact-add',
                'description' => 'Confirmation - Attendee'
            ]);
        }

        $hasContent = ($template->contents()->count() > 0);
        if ($hasTemplate && !$hasContent) {
            $template->contents()->save(new EmailTemplateContent([
                'content' => EmailTemplate::buildContent($template->slug)
            ]));
        }
    }
}
