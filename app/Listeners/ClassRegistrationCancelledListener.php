<?php

namespace App\Listeners;

use App\Events\ClassRegistrationCancelledEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClassRegistrationCancelledListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClassRegistrationCancelledEvent  $event
     * @return void
     */
    public function handle(ClassRegistrationCancelledEvent $event)
    {
        //
    }
}
