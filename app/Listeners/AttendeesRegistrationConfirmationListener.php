<?php

namespace App\Listeners;

use App\Events\RegistrationConfirmationEvent;
use App\Models\Order;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Carbon\Carbon;

class AttendeesRegistrationConfirmationListener implements ShouldQueue
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  RegistrationConfirmationEvent  $event
     * @return void
     */
    public function handle(RegistrationConfirmationEvent $event)
    {
        $order      = Order::find($event->order);
        $_attendees = [];
        $_other_attendees = [];
        $agent_name = $order->user->contact->name;
        $orderTransaction = $order->orderTransaction;
        $show_policy = false;

        foreach ($order->items()->registrationOrders()->get() as $item) {

            $registration_order = $item->orderable()->first();

            if ($registration_order->registrable_type === Constant::COURSE_CLASS_OBJECT) {
                        $classes = $registration_order->registrable()->get();
                        foreach ($classes as $class) {

                            if ($registration_order->isCBT()) {
                                $class_type = 'cbt';
                            } else if ($class->course->course_type_id === Constant::WEBCAST) {
                                $class_type = 'webinar';
                            } else {
                                $show_policy = true;
                                $class_type = 'seminar';
                            }

                            $_class = (object) [
                                'course_id'   => $class->course->id,
                                'is_class'    => true,
                                'name'        => $registration_order->isCBT()
                                                 ? $class->title
                                                 : $class->course->title,

                                'city'        => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->city,

                                'state'       => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->state,

                                'start_date'  => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->start->toDateString(),

                                'end_date'    => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->end->toDateString(),

                                'start_time'  => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->start_time_formatted,

                                'reg_time'  => $registration_order->isCBT()
                                                ? ''
                                                : $class->reg_time_formatted,

                                'end_time'    => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->end_time_formatted,

                                'start'       => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->start->copy()->format('M d, Y h:i:s A'),

                                'end'         => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->end->copy()->format('M d, Y h:i:s A'),

                                'instruction' => $registration_order->isCBT()
                                                 ? ''
                                                 : $class->instructions,

                                'type'        => $class_type,

                                'dates_for_email'   => $registration_order->isCBT()
                                                    ? ''
                                                    : $class->dates_for_email,

                                'location_name' => ($class_type == 'seminar' AND count($class->location)>0)? $class->location->location_name :  '',
                                'location_address' => ($class_type == 'seminar' AND count($class->location)>0)? $class->location->location : '',
                                'location_phone' => ($class_type == 'seminar' AND count($class->location)>0)? $class->location->phone_number : '',
                                'location_general'  => ($class_type == 'seminar' AND count($class->location)>0)? $class->location->general_location_name :  '',

                                'is_cary' => ($class_type == 'seminar'AND count($class->location)>0 and strtolower($class->location->city) == 'cary')
                                    ? true
                                    : false,

                            ];

                            $class_registrations = $registration_order->classRegistrations()->get();
                            foreach ($class_registrations as $class_registration) {
                                if ($class_registration->contact) {
                                    $_contact = $class_registration->contact;
                                    $user = $class_registration->contact->user;

                                    if ($user && ($order->user->contact->id != $_contact->id)) {
                                        $_attendees[$user->email]['user'] = $user;
                                        $_attendees[$user->email]['classes'][] = $_class;
                                    } elseif(!$user) {
                                        $_other_attendees[$_contact->id]['user'] = $_contact;
                                        $_other_attendees[$_contact->id]['classes'][] = $_class;
                                    }
                                }
                            }

                        }
                    }

            if ($registration_order->registrable_type === Constant::COURSE_OBJECT) {
                        $courses = $registration_order->registrable()->get();

                        foreach ($courses as $course) {

                            $categories = '';

                            foreach ($course->courseCategories as $courseCategory) {
                                $categories = $categories . "\n" . $courseCategory->category->name;
                            }

                            $class_registrations = $registration_order->classRegistrations()->get();
                            $_class = (object) [
                                'course_id'   => $course->id,
                                'name'        => $course->title,
                                'is_class'    => false,
                                'sku'         => $course->sku,
                                'duration'    => $course->duration,
                                'category'    => $categories,
                                'type'        => 'online',
                                'is_cary'     => false
                            ];

                            foreach ($class_registrations as $class_registration) {
                                if ($class_registration->contact) {
                                    $_contact = $class_registration->contact;
                                    $user = $class_registration->contact->user;

                                    if ($user && ($order->user->contact->id != $_contact->id)) {
                                        $_attendees[$user->email]['user'] = $user;
                                        $_attendees[$user->email]['classes'][] = $_class;
                                    } elseif(!$user) {
                                        $_other_attendees[$_contact->id]['user'] = $_contact;
                                        $_other_attendees[$_contact->id]['classes'][] = $_class;
                                    }
                                }
                            }
                        }
                    }

        }

        foreach ($_attendees as $email => $details)  {

            foreach ($details['classes'] as $class) {
                $_user = $details['user'];

                if($class->type == 'online')
                $_subject = $agent_name . ' registered you for online training with Environmental Resource Center';
                else
                $_subject = $agent_name . ' registered you for training with Environmental Resource Center';

                $data = [
                    'to' => $_user->email,
                    'subject' => $_subject,
                    'data' => [
                        'agent_name'    => $agent_name,
                        'users'         => [
                            (object)[
                                'name'          => $_user->contact->f_name,
                                'company'       => $_user->contact->company,
                                'fullname'      => $_user->contact->name,
                                'title'         => $_user->contact->title,
                                'address1'      => $_user->contact->address1,
                                'address2'      => $_user->contact->city . ', ' . $_user->contact->state . ' ' . $_user->contact->zip,
                            ],
                        ],
                        'isClass'       => $class->is_class,
                        'className'     => $class->name,
                        'city'          => $class->is_class ? $class->city : '',
                        'state'         => $class->is_class ? $class->state : '',
                        'startDate'     => $class->is_class ? Carbon::parse($class->start_date)->format('F d, Y') : '',
                        'startTime'     => $class->is_class ? $class->start_time : '',
                        'regTime'       => $class->is_class ? $class->reg_time : '',
                        'endTime'       => $class->is_class ? $class->end_time : '',
                        'dateRange'     => $class->is_class ? $class->dates_for_email : '',

                        'locationName' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_name
                            : '',

                        'locationAddress' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_address
                            : '',

                        'locationPhone' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_phone
                            : '',

                        'locationGeneral'  => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_general
                            :  '',

                        'isSeminar'         => $class->type == 'seminar' ? true : false,
                        'isWebinar'         => $class->type == 'webinar' ? true : false,
                        'isOnline'          => $class->type == 'online' ? true : false,
                        'isCary'            => $class->is_cary,
                        'hasCmid'           => $_user->contact->has_cmid,
                        'courseMillUser'    => $_user->contact->course_mill_user_id,
                        'cbtsPage'          => route('courses') . '?type=3',
                        'coursesPage'       => route('courses'),
                        'homePage'          => route('home'),
                        'fbUrl'             => route('course.show', $class->course_id),
                        'isPolicy'          => $show_policy
                    ]
                ];


                if($event->notify) {
                    $this->mailer->send('reg-seminars-confirmation-attendee', $data);
                }
            }
        }

        foreach ($_other_attendees as $email => $details)  {

            foreach ($details['classes'] as $class) {
                $_user = $details['user'];
                if($class->type == 'online')
                    $_subject = $agent_name . ' registered you for online training with Environmental Resource Center';
                else
                    $_subject = $agent_name . ' registered you for training with Environmental Resource Center';

                $data = [
                    'to' => $order->user->email,
                    'subject' => $_subject,
                    'data' => [
                        'agent_name' => $agent_name,
                        'users' => [
                            (object)[
                                'name' => $_user->f_name,
                                'company' => $_user->company,
                                'fullname'     => $_user->name,
                                'title' => $_user->title,
                                'address1' => $_user->address1,
                                'address2' => $_user->city . ', ' . $_user->state . ' ' . $_user->zip,
                            ],
                        ],
                        'isClass'     => $class->is_class,
                        'className'   => $class->name,
                        'city'        => $class->is_class ? $class->city : '',
                        'state'       => $class->is_class ? $class->state : '',
                        'startDate'   => $class->is_class ? Carbon::parse($class->start_date)->format('F d, Y') : '',
                        'startTime'   => $class->is_class ? $class->start_time : '',
                        'regTime'   => $class->is_class ? $class->reg_time : '',
                        'endTime'       => $class->is_class ? $class->end_time : '',
                        'dateRange'     => $class->is_class ? $class->dates_for_email : '',

                        'locationName' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_name
                            : '',

                        'locationAddress' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_address
                            : '',

                        'locationPhone' => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_phone
                            : '',

                        'locationGeneral'  => ($class->is_class and $class->type == 'seminar')
                            ? $class->location_general
                            :  '',

                        'isSeminar'   => $class->type == 'seminar' ? true : false,
                        'isWebinar'   => $class->type == 'webinar' ? true : false,
                        'isOnline'    => $class->type == 'online' ? true : false,
                        'isCary'            => $class->is_cary,
                        'hasCmid'           => $_user->has_cmid,
                        'courseMillUser'    => $_user->course_mill_user_id,
                        'cbtsPage'          => route('courses') . '?type=3',
                        'coursesPage'       => route('courses'),
                        'homePage'          => route('home'),
                        'fbUrl'             => route('course.show', $class->course_id),
                        'isPolicy'          => $show_policy
                    ]
                ];
                if($event->notify) {
                    $this->mailer->send('reg-seminars-confirmation-attendee', $data);
                }
            }
        }

    }
}
