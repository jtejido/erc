<?php

namespace App\Listeners;

use App\Events\CourseEbookUpdateEvent;
use App\Models\Book;
use App\Models\Course;
use App\Models\FileEntry;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourseEbookUpdateListener
{
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create the event listener.
     *
     * @param EmailTemplateMailer $mailer
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  CourseEbookUpdateEvent  $event
     * @return void
     */
    public function handle(CourseEbookUpdateEvent $event)
    {
        $book = Book::findOrFail($event->bookId);
        $_files = [];
        foreach ($event->fileEntryIds as $fileEntryId) {
            $file = FileEntry::find($fileEntryId);
            if(!$file) break;
            $filename = $file->title . ' - ' . $file->original_filename;
            $_file = (object) [
                'filename'        => $filename,
                'filelink'         => $file->getPublicURI()
            ];
            $_files[] = $_file;
        }

        $users = collect([]);
        foreach ($book->courses as $course) {
            $availableRegOrders = $course->activeRegistrationOrders();
            foreach ($availableRegOrders as $regOrder) {
                foreach ($regOrder->classRegistrations as $classRegistration) {
                    if ($user = $classRegistration->contact->user) {
                        $user->course_name = $course->title;
                        $users->push($user);
                    }
                }
            }
            foreach ($course->classes as $class) {
                $availableRegOrders = $class->activeRegistrationOrders();
                foreach ($availableRegOrders as $regOrder) {
                    foreach ($regOrder->classRegistrations as $classRegistration) {
                        if ($user = $classRegistration->contact->user) {
                            $user->course_name = $class->course->title;
                            $users->push($user);
                        }
                    }
                }
            }
        }

        $subject = 'Course Material Update - Environmental Resource Center';

        foreach ($users as $user) {

            $data = [
                'to'      => $user->email,
                'subject' => $subject,
                'data'    => [
                    'name'     => $user->contact->f_name,
                    'company'  => $user->contact->company,
                    'title'    => $user->contact->title,
                    'address1' => $user->contact->address1,
                    'address2' => $user->contact->city.', '.$user->contact->state.' '.$user->contact->zip,
                    'course_name'   => $user->course_name,
                    'files'         => $_files,
                    'cbtsPage' => route('courses') . '?type=3' ,
                    'coursesPage' => route('courses'),
                    'homePage' => route('home'),
                ]
            ];
            if($event->notify) {
                $this->mailer->send('course-update', $data);
            }

        }

    }
}
