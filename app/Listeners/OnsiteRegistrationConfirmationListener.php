<?php

namespace App\Listeners;

use App\Events\OnsiteRegistrationConfirmationEvent;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use App\Models\FileEntry;
use App\Models\User;
use App\Services\Mailer\EmailTemplateMailer;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OnsiteRegistrationConfirmationListener
{

    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    private $event;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  OnsiteRegistrationConfirmationEvent  $event
     * @return void
     */
    public function handle(OnsiteRegistrationConfirmationEvent $event)
    {

        $this->event = $event;

        $user    = User::find($event->user_id);
        $date    = $event->date;
        $address = $event->address;

        $subject = ($event->new)
                    ? 'Registration Confirmation - Training for your company'
                    : 'Registration Confirmation - Training for your company (Update)';

        $contact_name = $user->email;
        if ($user->contact) {
            $contact_name = $user->contact->name;
        }

        $content = ($event->new)
                    ? "You have been registered to an Onsite Training on $date at $address."
                    : "Details of your Onsite Training on $date at $address has been updated.";

        $slug = ($event->new) ? 'reg-onsite-confirmation' : 'reg-onsite-update';

        $data = [
            'to'      => $user->email,
            'subject' => $subject,
            'data'    => [
                'name'    => $contact_name,
                'address' => $address,
                'date'    => $date,
                'content'   => $content,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        if (is_array($event->files)) {
            foreach ($event->files as $file) {
                $file = FileEntry::find($file);
                if ($file) {
                    $data['attachments'][] = [
                        'path' => storage_path('app/' . $file->path),
                        'filename' => $file->original_filename,
                        'mime' => $file->mime
                    ];
                }
            }
        } else {
            $file = $event->files;
            $data['attachments'][] = [
                'path' => storage_path('app/' . $file->path),
                'filename' => $file->original_filename,
                'mime' => $file->mime
            ];
        }


        if ($event->notify) {
            $this->mailer->send($slug, $data);
        }

        $this->emailCsrAndAccounting($data, $slug);


    }

    /**
     * @param $data
     * @param $slug
     */
    protected function emailCsrAndAccounting($data, $slug)
    {
        $csr = array_merge($data, [
            'to' => env('CSR_EMAIL'),
            'subject' => $data['subject'] . ' - CSR Copy',
        ]);

        if($this->event->notify_csr) {
            $this->mailer->send($slug, $csr);
        }


        $Accounting = array_merge($data, [
            'to' => env('ACCOUNTING_EMAIL'),
            'subject' => $data['subject'] . ' - Accounting Copy',
        ]);

        if($this->event->notify_accounting) {
            $this->mailer->send($slug, $Accounting);
        }
    }

}
