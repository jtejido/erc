<?php namespace App\Traits;

use App\Models\Order;

trait UpsShippingTrait
{
    public function ship_rate($items = [],$zip,$is_pr = false)
    {
        $rate = new \Ups\Rate(
            env('UPS_ACCESS_KEY'),
            env('UPS_USER_ID'),
            env('UPS_PASSWORD')
        );

        try {
            $shipment = new \Ups\Entity\Shipment();
            //shipper
            $shipperAddress = $shipment->getShipper()->getAddress();
            $shipperAddress->setPostalCode('27513');

            $address = new \Ups\Entity\Address();
            $address->setPostalCode('27513');
            $address->setStateProvinceCode('NC');
            $address->setCity('Cary');
            $address->setCountryCode('US');
            $shipFrom = new \Ups\Entity\ShipFrom();
            $shipFrom->setAddress($address);
            $shipment->setShipFrom($shipFrom);

            //customer
            $shipTo = $shipment->getShipTo();
            $shipTo->setCompanyName('Customer');
            $shipToAddress = $shipTo->getAddress();
            $shipToAddress->setPostalCode($zip);

            if($is_pr){
            	$shipToAddress->setCountryCode('PR');
            }

            foreach($items as $item){
                $package = new \Ups\Entity\Package();
                $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
                $package->getPackageWeight()->setWeight($item);
                $shipment->addPackage($package);
            }

            $rate = $rate->getRate($shipment);
            return $rate->RatedShipment[0]->TotalCharges->MonetaryValue;
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    public function ship_order_update($order_id, $shipping_rate, $current_total = 0)
    {
        if( $order_id > 0 AND ($shipping_rate > 0 OR $shipping_rate == 'current') ){
            $order = Order::findOrFail($order_id);
            $shipping_rate = ($shipping_rate == 'current')
                                ? $order->shipping_rates
                                : $shipping_rate;
            $new_total = $current_total + $shipping_rate;
            $order->update(['shipping_rates' => $shipping_rate,
                            'total' => $new_total
                            ]);
            return $order;
        }
    }

    public function packages_chunk($item_weight, $limit = 150)
    {
        $ll = ceil($item_weight / $limit);
        $x = [];
        for($i=0;$i<$ll;$i++){
            // echo $item_weight. "<br>";
            if($item_weight > 150){
                $x[] = 150;
            }else{
                $x[] = $item_weight;
            }
            $item_weight -= $limit;
        }
        return $x;
    }
}
