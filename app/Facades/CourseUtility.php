<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CourseUtility extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'course_util'; }
}