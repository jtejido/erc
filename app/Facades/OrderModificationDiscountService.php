<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class OrderModificationDiscountService extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'order_mod_disc_service'; }
}