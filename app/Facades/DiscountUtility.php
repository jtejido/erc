<?php namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class DiscountUtility extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'disc_util'; }
}