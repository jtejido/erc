<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'course_categories';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['course_id', 'category_id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}