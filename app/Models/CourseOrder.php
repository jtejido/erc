<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseOrder extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'course_orders';

    protected $fillable = [
        'course_id',
        'category_id',
        'course_type_id',
        'order',
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(CourseType::class, 'course_type_id');
    }
}