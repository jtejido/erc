<?php
namespace App\Models;

use App\Models\Link\Category;
use App\Models\Link\Status;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'helpful_links';
    protected $fillable = [
        'name',
        'url',
        'category_id',
        'status_id',
    ];
    protected $appends = [
        'category'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function getCategoryAttribute()
    {
        return $this->category()->get();
    }
}
