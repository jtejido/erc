<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassHalfPriceDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_half_price_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'class_reg_id', 'deduction', 'is_deleted_by_credit', 'is_deleted'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classRegistration()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_id');
    }

}