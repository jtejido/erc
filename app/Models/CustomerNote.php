<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerNote extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'customer_notes';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['customer_id', 'content'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Contact::class, 'customer_id');
    }
}