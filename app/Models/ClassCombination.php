<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassCombination extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_combinations';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['reg_one', 'reg_two'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firstRegistration()
    {
        return $this->belongsTo(RegistrationOrder::class, 'reg_one');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondRegistration()
    {
        return $this->belongsTo(RegistrationOrder::class, 'reg_two');
    }

}