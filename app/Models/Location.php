<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    protected $guarded = ['id'];

    public function classes()
    {
        return $this->hasMany(CourseClass::class, 'location_id');
    }

    public function getLocationAttribute()
    {
        return trim($this->address . ' ' . $this->city . ', ' . $this->state . ' ' . $this->zip);
    }

    public function getShortLocationAttribute()
    {
        return $this->city . ', ' . $this->state;
    }

    public function getHasClassAttribute()
    {
        return $this->classes()->count();
    }

    public function generalLocation()
    {
        return $this->belongsTo(GeneralLocation::class, 'general_location_id');
    }

    public function getGeneralLocationNameAttribute()
    {
        return ($this->generalLocation) ? $this->generalLocation->location : '';
    }

}
