<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserYearlySubscription extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'user_yearly_subscriptions';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['user_id', 'client_yearly_subscription_id', 'activation_code', 'activated'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientSubscription()
    {
        return $this->belongsTo(ClientYearlySubscription::class, 'client_yearly_subscription_id');
    }
}