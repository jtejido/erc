<?php

namespace App\Models;

use App\Models\ModelInterface\Imageable;
use Illuminate\Database\Eloquent\Model;

class StateHandout extends Model implements Imageable
{

    protected $guarded = ['id'];

    const FOLDERPATH = 'handouts/';

    //
    public function files()
    {
        return $this->morphMany('App\Models\FileEntry', 'imageable');
    }

    public function scopeWithFiles($query)
    {
        return $query->has('FileEntry');
    }

    public function getTypeValueAttribute()
    {
        return StateHandoutTypes::getType($this->type)["type"];
    }

}
