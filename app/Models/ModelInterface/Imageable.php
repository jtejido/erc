<?php namespace App\Models\ModelInterface;

interface Imageable {

    public function files();

}