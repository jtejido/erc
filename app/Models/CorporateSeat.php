<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorporateSeat extends Model
{

    /**
     * Table used by model
     *
     * @var string
     */
    protected $table = 'corporate_seats';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['credit_number', 'price', 'created_at', 'updated_at'];
}