<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassMilitaryDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_military_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'contact_id', 'class_reg_id', 'deduction', 'is_deleted'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classRegistration()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_id');
    }


}