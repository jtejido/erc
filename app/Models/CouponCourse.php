<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponCourse extends Model
{
    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'coupon_courses';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['coupon_id', 'discountable_id', 'discountable_type'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function discountable()
    {
        return $this->morphTo();
    }

}