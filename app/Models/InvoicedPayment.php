<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicedPayment extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'invoiced_payments';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'po_image', 'po_file', 'po_image_mime', 'po_number',
                           'check_image', 'check_file', 'check_image_mime', 'check_number', 'items',
                            'shipping_address','shipping_zip','billing_address','billing_zip',
                            'billing_company', 'shipping_company', 'billing_state', 'billing_city',
                            'shipping_state', 'shipping_city', 'trans_total', 'shipping_rate', 'shipping_county',
                            'state_tax'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoicePaymentRecipients()
    {
        return $this->hasMany(InvoicedPaymentRecipient::class, 'invoiced_payment_id');
    }

    public function getShippingFeeAttribute()
    {
        $fee = (empty($this->shipping_rate)) ? 0 : $this->shipping_rate;
        return '$'.number_format($fee, 2);
    }

    public function getTransTotalFormattedAttribute()
    {
        return '$'.number_format($this->trans_total, 2);
    }

    public function updateBillingAndShippingAddress($params)
    {
        $this->update([
            'billing_company'       => $params['billing_company'],
            'billing_address'       => $params['billing_address1'],
            'billing_city'          => $params['billing_city'],
            'billing_state'         => $params['billing_state'],
            'billing_zip'           => $params['billing_zip'],
            'shipping_company'      => $params['shipment_company'],
            'shipping_address'      => $params['shipment_address1'],
            'shipping_city'         => $params['shipment_city'],
            'shipping_state'        => $params['shipment_state'],
            'shipping_zip'          => $params['shipment_zip'],
            'shipping_county'       => $params['shipment_county'],
        ]);
    }

    public function getStateTaxFormattedAttribute()
    {
        return (!empty(trim($this->state_tax)))
            ? '$' . $this->state_tax
            : '$0.00';
    }

}