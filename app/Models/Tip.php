<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tip extends Model
{
    protected $fillable = [
        'title',
        'content',
        'category_id',
        'published_date',
        'is_published',
        'photo',
        'photo_mime',
        'slug'
    ];
    protected $casts = [
        'published_date' => 'date',
    ];
    protected $appends = [
        'category'
    ];

    public function scopePublished($query)
    {
        return $query->where('is_published', true)
                     ->orderBy('published_date', 'DESC');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function cat()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getCategoryAttribute()
    {
        return $this->category()->get();
    }
}
