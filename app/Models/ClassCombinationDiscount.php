<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassCombinationDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_combination_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'hwm_class_id', 'dot_class_id', 'class_reg_one',
                           'class_reg_two', 'attendee_contact_id', 'deduction',
                           'is_deleted_by_credit', 'is_deleted'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hwm()
    {
        return $this->belongsTo(CourseClass::class, 'hwm_class_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dot()
    {
        return $this->belongsTo(CourseClass::class, 'dot_class_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classOne()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_one');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classTwo()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_two');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attendee()
    {
        return $this->belongsTo(Contact::class, 'attendee_contact_id');
    }
}