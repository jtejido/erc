<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderAdjustment extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'order_adjustments';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['agent_id', 'order_id', 'order_item_id', 'adjustable_id',
                           'adjustable_type', 'adjustment_action', 'amount', 'note',
                           'is_half_price_adjustment', 'is_group_discount_adjustment', 'is_product_qty_adjustment',
                           'is_corporate_discount_adjustment'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function item()
    {
        return $this->belongsTo(OrderItem::class, 'order_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function adjustable()
    {
        return $this->morphTo();
    }

    public function getIsProductOrderAttribute()
    {
        return ($this->adjustable_type == 'ProductOrders') ? true : false;
    }

}