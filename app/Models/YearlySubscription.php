<?php namespace App\Models;

use App\Observers\YearlySubscriptionObserver;
use Illuminate\Database\Eloquent\Model;

class YearlySubscription extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'yearly_subscriptions';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['type_id', 'originating_agent_user_id', 'start_date',
                           'end_date', 'total_price', 'ended', 'status'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(YearlySubscriptionType::class, 'type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientSubscriptions()
    {
        return $this->hasMany(ClientYearlySubscription::class, 'yearly_subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'originating_agent_user_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        YearlySubscription::observe(new YearlySubscriptionObserver());
    }
}