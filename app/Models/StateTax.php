<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateTax extends Model
{
    protected $table = 'state_tax';
    protected $guarded = ['id'];

    public function getTotalTaxAttribute()
    {
        return (float) ($this->state_rate * 100)
            + ($this->county_rate * 100)
            + ($this->transit_rate * 100);
    }

    public static function getShippingCounty($id)
    {
        $st = self::find($id);
        return ($st)
            ? $st->county
            : null;
    }

    public static function getShippingCountyFromName($county)
    {
        $st = self::where('county', $county)->first();
        return ($st)
            ? $st->id
            : null;
    }

    public static function getTaxFromCountyName($county)
    {
        $st = self::where('county', $county)->first();
        return ($st)
            ? $st->total_tax
            : null;
    }

}
