<?php namespace App\Models;

use App\Observers\OrderItemObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;
    public static $FIRE_EVENTS = true;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    protected $appends = [
        'transaction_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function orderable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return string
     */
    public function getTransactionIdAttribute()
    {
        $head = str_pad($this->order_id, 6, '0');
        $tail = str_pad($this->id, 9, '0', STR_PAD_LEFT);

        return $head.substr($tail, 6);
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        OrderItem::observe(new OrderItemObserver());
    }

    /**
     * @return string
     */
    public function getPriceAttribute()
    {
        return '$'.number_format($this->price_charged, 2);
    }

    public function scopeRegistrationOrders($query)
    {
        return $query->where('orderable_type', 'RegistrationOrder');
    }


    public function getTaxedPrice($state = '', $county = 1)
    {

        if($state == '')
            return $this->price_charged;

        if($this->orderable_type != 'ProductOrders')
            return $this->price_charged;

        $state = State::where('state', $state)->first();

        if(!$state)
            return $this->price_charged;

        if(!$state->has_tax)
            return $this->price_charged;

        $state_tax = StateTax::find($county)->total_tax;

        return computeProductPrice($this->orderable->price, $this->orderable->quantity, $state_tax);

    }

    public function getTaxedPriceAttribute()
    {
        return computeProductPrice($this->price_charged, 1, $this->state_tax);
    }

    public function scopeProductOrders($query)
    {
        return $query->where('orderable_type', 'ProductOrders');
    }

    public function getIsProductOrderAttribute()
    {
        return ($this->orderable_type == 'ProductOrders') ? true : false;
    }


}