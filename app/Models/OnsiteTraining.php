<?php namespace App\Models;

use App\Models\ModelInterface\Imageable;
use App\Observers\OnsiteTrainingObserver;
use Illuminate\Database\Eloquent\Model;

class OnsiteTraining extends Model implements Imageable
{

    const FOLDERPATH = 'onsite_training/';

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'onsite_trainings';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['originating_agent_user_id', 'address', 'training_date', 'price'];
    protected $dates = [
        'training_date'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'originating_agent_user_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        OnsiteTraining::observe(new OnsiteTrainingObserver());
    }

    public function files()
    {
        return $this->morphMany('App\Models\FileEntry', 'imageable');
    }

    public function materials()
    {
        return $this->files()->where('type', 'MATERIAL')->get();
    }

    public function certificates()
    {
        return $this->files()->where('type', 'CERTIFICATE')->get();
    }

}