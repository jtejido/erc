<?php
namespace App\Models\Link;

use Illuminate\Database\Eloquent\Model;
use App\Models\Link;

class Category extends Model
{
    protected $table = 'helpful_links_categories';
    protected $fillable = [
        'name',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function link()
    {
        return $this->hasMany(Link::class, 'category_id');
    }

    // public function getCategoryAttribute()
    // {
    //     return $this->category()->get();
    // }
}
