<?php
namespace App\Models\Link;

use Illuminate\Database\Eloquent\Model;
use App\Models\Link;

class Status extends Model
{
    protected $table = 'helpful_links_statuses';
    protected $fillable = [
        'name',
    ];

    public function link()
    {
        return $this->belongsTo(Link::class);
    }
}
