<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassLink extends Model
{
    public function linkable()
    {
        return $this->morphTo();
    }
}
