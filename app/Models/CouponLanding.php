<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponLanding extends Model
{

    CONST CP_URL_PREFIX = 'cp';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id');
    }

    public function getUpdatedAttribute()
    {
        return $this->updated_at->toDateString();
    }

    public function getCouponCodeLink()
    {
        if($this->coupon) {
            return "<a href='".route('coupons.edit', $this->coupon->id)."'>".$this->coupon->coupon_code."</a>";
        } else {
            return '';
        }
    }

    public function getPublishedLabel()
    {
        $label = '';
        $format = "<small class=\"label label-disabled label-%s\"><i class='fa fa-check-square-o'></i> %s</small>";
        $format_tbc = "<small class=\"label label-disabled label-%s\"><i class='fa fa-square-o'></i> %s</small>";
        if($this->published)
            $label = sprintf($format, 'success', 'published');
        else
            $label = $tbc = sprintf($format_tbc, 'default', 'draft');
        return $label;
    }

    public function getUrlAttribute()
    {
        return url($this::CP_URL_PREFIX . '/' . $this->slug);
    }

}
