<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use View;

class EmailTemplateContent extends Model
{
    protected $fillable = ['content'];

    public function toHtml($data)
    {
        $PlaceholderService = \App::make(\App\Services\Mailer\PlaceholderService::class);

        $slug = str_random(15);
        
        $path = storage_path('app/emails/generated');
        
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file     = "{$slug}.blade.php";
        $filename = $path."/{$file}";
        
        file_put_contents($filename, $PlaceholderService->shortcodify($this->content)."\n");

        View::addNamespace('email-generated', $path);
        $view = "email-generated::{$slug}";

        $view = View::make($view);
        $view->with($data);
        $output = $view->render();        

        unlink($filename);
        return $output;
    }
}
