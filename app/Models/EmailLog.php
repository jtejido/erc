<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use View;

class EmailLog extends Model
{
    protected $fillable = ['slug', 'data'];
    protected $casts = [
        'data' => 'json'
    ];
}
