<?php

namespace App\Models;

use App\Observers\ProductOrdersObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOrders extends Model
{

    use SoftDeletes;

    protected $guarded = ['id'];

    public function product()
    {
        return $this->morphTo();
    }

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'orderable');
    }

    public function agent()
    {
        return $this->belongsTo(User::class, 'originating_agent_user_id');
    }

    public static function boot()
    {
        parent::boot();

        ProductOrders::observe(new ProductOrdersObserver());
    }

    public function isBook()
    {
        return get_class($this->product) == Book::class;
    }

    public function title()
    {
        return $this->product->name;
    }

    public function description()
    {
        return $this->product->description;
    }

    public function getProductOrderPrice($qty = null, $tx = null, $price = null)
    {
        $quantity = is_null($qty) ? $this->quantity : $qty;
        $price = is_null($price) ? $this->price : $price;
        return $price * $quantity;

    }

    public function getProductPriceAttribute()
    {
        $price = $this->getProductOrderPrice(1, 0);
        return '$'.number_format($price, 2);
    }

    public function getProductPriceWithTaxAttribute()
    {
        $price = $this->getProductOrderPrice(1);
        return '$'.number_format($price, 2);
    }

    public function getProductRateAttribute()
    {
        return $this->product->price;
    }

}
