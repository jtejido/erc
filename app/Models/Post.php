<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['name','slug','content','category_id','order'];
    protected $appends = ['url'];

    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id');
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function getUrlAttribute()
    {
        $prefix = '';
        if ($this->category_id) {
            $prefix = $this->category()->first()->slug.'/';
        }

        return $prefix . $this->slug;
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
