<?php

namespace App\Models;

class StateHandoutTypes
{

    public static $types = [
            [
                'id'    => 1,
                'type'  => 'Hazardous Waste Management Regulations'
            ],
            [
                'id'    => 2,
                'type'  => 'SARA Title III State Program'
            ],
        ];

    public static function getTypes()
    {
        return collect(self::$types);
    }

    public static function getType($id = 1)
    {
        return self::getTypes()->where('id', $id)->first();
    }

}
