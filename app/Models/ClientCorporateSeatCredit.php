<?php namespace App\Models;

use App\Observers\ClientCorpSeatCreditObserver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClientCorporateSeatCredit extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'client_corporate_seat_credits';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['originating_agent_user_id', 'credits', 'price', 'paid_price', 'status', 'activation_code', 'activated'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'originating_agent_user_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        ClientCorporateSeatCredit::observe(new ClientCorpSeatCreditObserver());
    }

    public function getRateAttribute()
    {

        try {
            $corp_seat = CorporateSeat::where('credit_number', $this->credits)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return '--';
        }

        return '$' . $corp_seat->price;

    }

    public function getRawRateAttribute()
    {

        try {
            $corp_seat = CorporateSeat::where('credit_number', $this->credits)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return '--';
        }

        return $corp_seat->price;

    }

}