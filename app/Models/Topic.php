<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $guarded = ['id'];

    public function courses()
    {
        return $this->hasMany(Course::class, 'topic_id');
    }

    public function getIsAssignedAttribute()
    {
        return $this->courses()->count();
    }
}
