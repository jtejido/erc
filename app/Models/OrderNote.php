<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderNote extends Model
{
    protected $fillable = ['user_id','order_id','notes'];
    protected $appends = ['created_at_string'];

    public function getCreatedAtStringAttribute($value)
    {
        return $this->created_at->toFormattedDateString();
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(\App\Models\Order::class, 'order_id');
    }
}
