<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'instructors';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'signature_mime', 'signature'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes()
    {
        return $this->hasMany(CourseClass::class, 'instructor_id', 'id');
    }
}
