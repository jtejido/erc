<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['coupon_code', 'coupon_amount', 'coupon_type'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function couponCourses()
    {
        return $this->hasMany(CouponCourse::class, 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function couponDiscounts()
    {
        return $this->hasMany(CouponDiscount::class, 'coupon_id');
    }
}
