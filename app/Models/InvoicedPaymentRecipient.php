<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicedPaymentRecipient extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'invoiced_payment_recipients';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['invoiced_payment_id', 'email'];

    /**
     * @var bool
     */
    public $timestamps = true;
}