<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CouponDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'coupon_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'coupon_id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'coupon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classDiscounts()
    {
        return $this->hasMany(ClassCouponDiscount::class, 'coupon_discount_id');
    }
}