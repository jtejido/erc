<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'promos';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['photo', 'photo_mime', 'url', 'active', 'order'];

    /**
     * @var bool
     */
    public $timestamps = true;
}