<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table  = 'roles';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['role'];

    /**
     * Timestamps of table
     *
     * @var objects
     */
    public $timestamps = false;
} 