<?php namespace App\Models;

use App\Observers\OrderObserver;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    use SoftDeletes;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'orders';

    protected $dates = ['completed_at', 'deleted_at'];

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['user_id', 'user_csr_id', 'order_notes', 'status', 'paid_total',
                           'order_tax', 'order_fees_handling', 'total', 'completed_at', 'archived_at',
                           'shipping_address','shipping_zip','billing_address','billing_zip','shipping_rates',
                           'billing_company', 'shipping_company', 'billing_state', 'billing_city', 'shipping_state',
                            'shipping_city', 'shipping_county'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    public function getValidItems()
    {

        if($this->items->count() == 0) return null;

        return $this->items->reject(function($val) {
            return ($val->orderable_type == Constant::REGISTRATION_ORDER
                and is_null($val->orderable()->withTrashed()->first()));
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classDiscounts()
    {
        return $this->hasMany(ClassCombinationDiscount::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classHalfPriceDiscounts()
    {
        return $this->hasMany(ClassHalfPriceDiscount::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function couponDiscounts()
    {
        return $this->hasMany(CouponDiscount::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoicePayments()
    {
        return $this->hasMany(InvoicedPayment::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentTransactions()
    {
        return $this->hasMany(PaymentTransaction::class, 'order_id');
    }

    public function getTransaction()
    {
        $trans = null;
        try {

            if($this->status === Constant::INVOICED) {
                $trans = $this->invoicePayments()->firstOrFail();
            }

            if($this->status === Constant::COMPLETE) {
                $trans = $this->paymentTransactions()->firstOrFail();
            }

        } catch (\Exception $e) {
            return $trans;
        }

        return $trans;

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function orderTransaction()
    {
        return $this->hasOne(OrderTransaction::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderAdjustments()
    {
        return $this->hasMany(OrderAdjustment::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderAdjustmentHistories()
    {
        return $this->hasMany(OrderAdjustmentHistory::class, 'order_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        Order::observe(new OrderObserver());
    }

    public function csr()
    {
        return $this->belongsTo(User::class, 'user_csr_id');
    }

    public function scopeLast($query)
    {
        return $query->orderBy('id', 'DESC')->first();
    }

    public function isAvailable()
    {
        return $this->availability->gt(Carbon::now()->startOfDay());
    }

    public function getAvailabilityAttribute()
    {
        return $this->completed_at->addYear();
    }

    public function getCompletedAttribute()
    {
        return $this->completed_at->copy()
            ->setTimeFromTimeString($this->completed_at);
    }

    public function reminder()
    {
        return $this->morphMany(ReminderHistory::class, 'remindable');
    }

    public function scopeCompleted($query)
    {
        return $query->whereIn('orders.status', ['INVOICED', 'COMPLETE']);
    }

    public function getTotalCostAttribute()
    {
        return '$' . number_format($this->paid_total, 2, '.', ',');
    }

    public function getTotalInDollarsAttribute()
    {
        return '$' . number_format($this->total, 2, '.', ',');
    }

    public function notes()
    {
        return $this->hasMany(\App\Models\OrderNote::class, 'order_id');
    }

    public function getStatusFormattedAttribute()
    {
        if ($this->status == Constant::INVOICED) {
            return "<span class='label label-warning'>Invoiced</span>";
        } elseif ($this->status == Constant::COMPLETE) {
            return "<span class='label label-success'>Paid</span>";
        } elseif ($this->status == Constant::PENDING) {
            return "<span class='label label-danger'>Pending</span>";
        } else {
            return "<span class='label label-danger'>Pending</span>";
        }

    }

    public function scopeArchived($query)
    {
        return $query->whereNotNull('archived_at');
    }

    public function getIsArchivedAttribute()
    {
        return ($this->archived_at) ? 1 : 0;
    }

    public function archiveNow()
    {
        return $this->update([
            'archived_at' => Carbon::now()
        ]);
    }

    public function UnArchiveNow()
    {
        return $this->update([
            'archived_at' => NULL
        ]);
    }

    public function getIsCompletedAttribute()
    {
        return ($this->status == Constant::COMPLETE);
    }

    public function getBalanceAttribute()
    {

        $bal = $this->total - $this->paid_total;

        return ($bal >= 0) ? '$'.number_format($bal, 2, '.', ',') : '$0';
    }

    public function addToPaidTotal($amount)
    {
        $paid_total = (float) $amount + $this->paid_total;

        return $this->update(['paid_total' => $paid_total]);
    }

    public function isOnsite()
    {
        if($this->items()->count()) {
            return $this->items()->first()->orderable_type == Constant::ONSITE_TRAINING;
        }
        return false;

    }

    public function getCompleteBillingAddressAttribute()
    {

        return '<strong>'
            . $this->billing_company
            . '</strong>'
            . '<br/>'
            . $this->billing_address
            . '<br/>'
            . $this->billing_city.', '.$this->billing_state.' '.$this->billing_zip;
    }

    public function getCompleteShippingAddressAttribute()
    {

        $shipping = '<strong>'
            . $this->shipping_company
            . '</strong>'
            . '<br/>'
            . $this->shipping_address
            . '<br/>'
            . $this->shipping_city.', '.$this->shipping_state.' '.$this->shipping_zip;

        if(!empty(trim($this->shipping_county))) {
            $shipping .= ' (' . $this->shipping_county . ')';
        }

        return $shipping;
    }

    public function getCheckoutCombinationAttribute()
    {
        $product_count = 0;
        $course_count = 0;

        foreach ($this->items as $item) {
            if($item->orderable_type == 'ProductOrders') {
                $product_count++;
            } else {
                $course_count++;
            }
        }

        if($product_count and $course_count) {
            $type = 'PRODUCT_COURSE';
        } elseif($product_count) {
            $type = 'PRODUCT';
        } elseif($course_count) {
            $type = 'COURSE';
        }

        return $type;

    }

    public function getCurrentTotalAttribute()
    {
        return ($this->items()->count())
            ? $this->items->sum('price_charged')
            : 0;
    }

    public function getCurrentProductTotalAttribute()
    {
        return ($this->items()->count())
            ? $this->items()->productOrders()->get()->sum('price_charged')
            : 0;
    }

    public function getCurrentProductTotalTaxAttribute()
    {

        if(!$this->has_tax)
            return 0;

        if(empty($this->shipping_rates))
            return 0;

        if(empty($this->shipping_county))
            return 0;

        $current_prod_total = $this->current_product_total;

        $tax = StateTax::getTaxFromCountyName($this->shipping_county);

        $current_prod_total += $this->shipping_rates;

        return getPercentageAmountFromTotal($tax, $current_prod_total);

    }

    public function getCurrentTotal($state = '', $county = 1)
    {

        if($this->items()->count() == 0) return 0;

        $total = 0;

        foreach ($this->items as $item) {
            $total += $item->getTaxedPrice($state, $county);
        }

        return $total;
    }

    public function completedDateFormatted($format = 'l, F j, Y')
    {
        return $this->completed_at->format($format);
    }

    public function getPaymentMethodAttribute()
    {
        if ($this->status === Constant::INVOICED) {
            return 'Purchase Order';
        } else {
            return 'Credit Card';
        }
    }

    public function getHasMultipleClassesAttribute()
    {
        return ($this->items()->registrationOrders()->count() > 1) ? true : false;
    }

    public function getHasClassRegistrationAttribute()
    {
        return ($this->items()->registrationOrders()->count()) ? true : false;
    }

    public function getHasSeminarAttribute()
    {
        foreach($this->items()->registrationOrders()->get() as $item) {
            $registration_order = $item->orderable()->first();
            if($registration_order->isSeminar()) {
                return true;
            }
        }
        return false;
    }

    public function getHasCbtAttribute()
    {
        foreach($this->items()->registrationOrders()->get() as $item) {
            $registration_order = $item->orderable()->first();
            if($registration_order->isCourse()
                and $registration_order
                    ->classRegistrations()
                    ->where('attendee_contact_id', '=', $this->user->contact_id)
                    ->count()) {
                return true;
            }
        }
        return false;
    }

    public function getHasWebcastAttribute()
    {
        foreach($this->items()->registrationOrders()->get() as $item) {
            $registration_order = $item->orderable()->first();
            if($registration_order->isWebcast()) {
                return true;
            }
        }
        return false;
    }

    public function getIsInvoicedAttribute()
    {
        return ($this->status === Constant::INVOICED);
    }

    public function getIsCardAttribute()
    {
        return ($this->status === Constant::COMPLETE);
    }

    public function getHasOtherAttendeesAttribute()
    {
        foreach($this->items()->registrationOrders()->get() as $item) {
            $registration_order = $item->orderable()->first();
            if($registration_order
                    ->classRegistrations()
                    ->where('attendee_contact_id', '!=', $this->user->contact_id)
                    ->count()) {
                return true;
            }
        }
        return false;
    }

    public function getHasClassInCaryAttribute()
    {
        foreach($this->items()->registrationOrders()->get() as $item) {
            $registration_order = $item->orderable()->first();
            if($registration_order->isClass()) {
                $class = $registration_order->registrable()->first();
                if($class->is_cary) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getTransDateAttribute()
    {
        return ($this->completed_at)
            ? $this->completed_at->format('M j, Y g:i a')
            : '';
    }

    public function updateBillingAndShippingAddress($params)
    {

        $this->update([
            'billing_company'       => $params['billing_company'],
            'billing_address'       => $params['billing_address1'],
            'billing_city'          => $params['billing_city'],
            'billing_state'         => $params['billing_state'],
            'billing_zip'           => $params['billing_zip'],
            'shipping_company'      => $params['shipment_company'],
            'shipping_address'      => $params['shipment_address1'],
            'shipping_city'         => $params['shipment_city'],
            'shipping_state'        => $params['shipment_state'],
            'shipping_zip'          => $params['shipment_zip'],
            'shipping_county'       => $params['shipment_county'],
        ]);

    }

    public function updateTotal($param)
    {

        $total = $param['current_total'];

        $shipping_rates = (!empty(trim($param['shipping_fee'])))
            ? (float) $param['shipping_fee']
            : 0;

        $state_tax = (!empty(trim($param['state_tax'])))
            ? (float) $param['state_tax']
            : 0;

        $this->update([
            'total'                 => $total,
            'shipping_rates'        => $shipping_rates,
            'order_tax'             => $state_tax,
        ]);

    }

    public function updateOrderTax()
    {
        $this->update([
            'order_tax'     => $this->current_product_total_tax
        ]);
    }

    public function getOrderShippingRatesAttribute()
    {
        return (!empty($this->shipping_rates)) ? $this->shipping_rates : 0;
    }

    public function getOrderStateTaxAttribute()
    {
        return (!empty($this->order_tax)) ? $this->order_tax : 0;
    }

    public function getHasTaxAttribute()
    {
        return (empty($this->order_tax)) ? false : true;
    }

}
