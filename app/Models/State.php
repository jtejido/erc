<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['state', 'abbrev'];

    /**
     * @var bool
     */
    public $timestamps = false;

    public function getHasTaxAttribute()
    {
        return ($this->tax > 0);
    }

}