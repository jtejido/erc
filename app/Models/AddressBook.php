<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'address_book_entries';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['user_id', 'contact_id', 'note'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }
}
