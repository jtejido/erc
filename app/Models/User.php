<?php namespace App\Models;

use App\Observers\UserObserver;
use App\Utilities\Constant;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['contact_id', 'email', 'coursemill_user_id',
        'password', 'state', 'has_military_discount', 'corporate_seat_credits',
        'remember_token', 'status'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'verification_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function getCorporateSeatsCredit()
    {

        $that = $this;
        $users = null;

        try {
            $users = User::whereHas('contact', function($query) use ($that) {
                $query->where('company', 'like', $that->contact->company);
            })
                ->where('corporate_seat_credits', '!=', 0)
                ->get();

            $users = $users->reject(function ($user) use ($that) {
               return ($user->addressBooks()->where('contact_id', $that->contact_id)->count() or ($user->id == $that->id))
                   ? false
                   : true;
            });

        } catch (\Exception $e) {

        } finally {
            return ($users) ? $users->sum('corporate_seat_credits') : 0;
        }

    }

    public function getCorporateSeatsCreditUser()
    {

        $that = $this;
        $user = null;
        try {
            $users = User::whereHas('contact', function($query) use ($that) {
                $query->where('company', 'like', $that->contact->company);
            })
                ->has('corporateSeatCredits')
                ->where('corporate_seat_credits', '!=', 0)
                ->get();

            $users = $users->reject(function ($user) use ($that) {
                return ($user->addressBooks()->where('contact_id', $that->contact_id)->count() or ($user->id == $that->id))
                    ? false
                    : true;
            });
            $user = $users->first();

        } catch (\Exception $e) {

        } finally {
            return ($user) ? $user : $that;
        }

    }

    public function subtractCorpSeatCredits($credit_to_subtract)
    {
        $updated_credits = ($this->corporate_seat_credits >= $credit_to_subtract)
                            ? $this->corporate_seat_credits - $credit_to_subtract
                            : 0;

        $this->update([
            'corporate_seat_credits'    => $updated_credits
        ]);

    }

    /**
     * Get roles
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id');
    }

    public function getRoleAttribute()
    {
        return $this->roles->first()->id;
    }

    public function getRoleNameAttribute()
    {
        switch ($this->roles->first()->role) {
            case Constant::ROLE_CSR:
                return 'CSR';
            case Constant::ROLE_ADMIN:
                return 'Admin';
            default:
                return 'Member';
        }
    }

    /**
     * Check if has role
     *
     * @param $r
     * @return bool
     */
    public function hasRole($r)
    {
        foreach ($this->roles as $role)
        {
            if ($role->role == $r) return true;
        }

        return false;
    }

    /**
     * Assign role
     *
     * @param   mixed     $role   $role can be role object or role id
     */
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }

    /**
     * Remove role
     *
     * @param $role
     * @return int
     */
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'contact_id');
    }

    /**
     * Get user contacts using user_id
     *
     * @return  Contacts
     */
    public function addressBooks()
    {
        return $this->hasMany(AddressBook::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function actingAgents()
    {
        return $this->hasMany(ActingOriginatingAgent::class, 'acting_agent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function corporateSeatCredits()
    {
        return $this->hasMany(ClientCorporateSeatCredit::class, 'originating_agent_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientYearlySubscriptions()
    {
        return $this->hasMany(ClientYearlySubscription::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function onsiteTrainings()
    {
        return $this->hasMany(OnsiteTraining::class, 'originating_agent_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function registrationOrders()
    {
        return $this->hasMany(RegistrationOrder::class, 'originating_agent_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function yearlySubscriptions()
    {
        return $this->hasMany(YearlySubscription::class, 'originating_agent_user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productOrders()
    {
        return $this->hasMany(ProductOrders::class, 'originating_agent_user_id', 'id');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return ($this->status) ? true : false;
    }


    /**
     * Check if user has member role
     *
     * @return bool
     */
    public function isMember()
    {
        return $this->hasRole(Constant::ROLE_MEMBER);
    }

    /**
     * Check if user has admin/cs role
     *
     * @return bool
     */
    public function isAdminorCSR()
    {
        return $this->hasRole(Constant::ROLE_ADMIN)
               or $this->hasRole(Constant::ROLE_CSR);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeManagement($query)
    {
        $roles = Role::whereIn('role', [
            Constant::ROLE_ADMIN,
            Constant::ROLE_CSR])->lists('id');
        
        // return $query->has('roles', $roles);
        return $query->whereHas('roles', function($query) use ($roles) {
            $query->whereIn('id', $roles);
        });
    }

    /**
     * @return mixed
     */
    public function getContactAttribute()
    {
        return $this->contact()->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function hasSubscription()
    {
        return $this->hasOne(UserYearlySubscription::class, 'user_id');
    }

    public function getLinkedNameForAdmin()
    {
        return "<a href='".route('admin.user_details', [$this->contact->id, $this->id])."'>".
            $this->contact->name."</a>";
    }

    public function getNameAttribute()
    {
        return $this->contact->name;
    }

    public function getCompanyAttribute()
    {
        return $this->contact->company;
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        User::observe(new UserObserver());
    }

}
