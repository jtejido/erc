<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralLocation extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}
