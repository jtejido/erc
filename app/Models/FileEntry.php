<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileEntry extends Model
{

    const EXT = '.bin';

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getSizeAttribute($value)
    {
        return number_format($value/(1<<20),2)."MB";
    }

    public function isEbook()
    {
        return get_class($this->imageable) == Book::class && $this->type == 'EBOOK';
    }

    public function getPublicURI()
    {
        return route('main.course.getmaterial', [$this->filename]);
    }
}
