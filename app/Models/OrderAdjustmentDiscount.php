<?php namespace App\Models;

use App\Utilities\Constant;
use Illuminate\Database\Eloquent\Model;

class OrderAdjustmentDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'order_adjustment_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'adjustable_id', 'adjustable_type', 'adjustment_notes', 'deduction', 'is_deleted'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function adjustable()
    {
        return $this->morphTo();
    }

    public function getAdjustedDeductionAttribute()
    {
        $value = $this->deduction;
        if($this->adjustable_type == Constant::PRODUCT_ORDERS) {
            $value = number_format($value / $this->adjustable->quantity, 2);
        }
        return $value;
    }

    public function getComputedDeductionAttribute()
    {
        $value = $this->deduction;
        return $value;
    }

}