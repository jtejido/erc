<?php namespace App\Models;


class LinkMeeting
{

    CONST LINK = "https://global.gotomeeting.com/join/";

    public function generateLink($id, $name='meeting')
    {
        $link = "<a href='%s' target='_blank'>%s</a>";
        return sprintf($link, $this::LINK.$id, $name);
    }

}