<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YearlySubscriptionType extends Model
{

    /**
     * Table used by model
     *
     * @var string
     */
    protected $table = 'yearly_subscription_types';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name', 'price', 'created_at', 'updated_at'];
}