<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CancellationNote extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'cancellation_notes';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['customer_id', 'class_registration_id', 'content'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Contact::class, 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classRegistration()
    {
        return $this->belongsTo('class_registration_id');
    }
}