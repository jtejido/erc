<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemovedDiscountByCredit extends Model
{
    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'removed_discount_by_credits';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['class_credit_discount_id', 'class_reg_id',
                           'discountable_type', 'discountable_id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function discountable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classCreditDiscount()
    {
        return $this->belongsTo(ClassCreditDiscount::class, 'class_credit_discount_id');
    }
}
