<?php

namespace App\Models;

use App\Models\EmailTemplateContent;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $fillable = ['slug', 'description','active_content_id'];
    protected $appends = ['contents'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contents()
    {
        return $this->hasMany(EmailTemplateContent::class, 'email_template_id');
    }

    public function getContentAttribute()
    {
        $content = null;
        if (!$this->active_content_id) {
            $content = $this->contents()->first();
            if ($content) {
                $this->active_content_id = $content->id;
                $this->save();
            }
        } else {
            $content = EmailTemplateContent::find($this->active_content_id);
        }
        return $content;
    }

    public function scopeBySlug($query, $slug)
    {
        return $query->whereSlug($slug)->first();
    }

    public function getContentsAttribute()
    {
        return $this->contents()->select(['content','created_at','id'])->get();
    }

   public static function buildContent($slug)
   {
        $content_mapping = [
            'ua-register'                        => 'customer/new-account.html',
            'reg-seminars-confirmation-user'     => 'classes/registration.html',
            'reg-seminars-confirmation-attendee' => 'classes/attendee/registration.html',
            'ua-contact-add'                     => 'contacts/add.html',
            'ua-forgot-password'                 => 'customer/forgot-password.html',
            'ua-profile-update'                  => 'customer/profile-update.html',
            'reg-onsite-confirmation'            => 'classes/onsite-registration.html',
            'reg-training-swap-user'             => 'classes/attendee/swap.html',
            'reg-cancellation'                   => 'classes/cancellation.html',
            'attendance-confirmation'            => 'classes/attendee/attendance-confirmation.html',
        ];
        
        $file = $content_mapping[$slug];

        $templates = base_path('resources/views/emails/templates/');
        $css       = file_get_contents($templates.'zurb-foundation/foundation-live.css');
        $content   = file_get_contents($templates.$file);

        $css       = "<style>{$css}.</style>";
        $content   = str_replace('<!-- <style> -->', $css, $content);
        $email_tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';

        $content   = $email_tag."\n".$content;
        return $content;
   }
}
