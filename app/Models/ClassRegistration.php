<?php namespace App\Models;

use App\Utilities\Constant;
use Illuminate\Database\Eloquent\Model;
use App\Observers\ClassRegistrationObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassRegistration extends Model
{

    use SoftDeletes;
    public static $FIRE_EVENTS = true;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_registrations';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['registration_order_id', 'attendee_contact_id', 'item_charge', 'paid_charge',
                           'attended', 'meeting_flag', 'exam_flag', 'reminder_flag', 'is_half_priced',
                           'corporate_seat_credit_applied', 'certificate_sent', 'certificate_file_path',
                           'certificate_link', 'exam_result','is_new', 'is_cancelled', 'is_swapped', 'swap_note'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class, 'attendee_contact_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registrationOrder()
    {
        return $this->belongsTo(RegistrationOrder::class, 'registration_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscriptionDiscount()
    {
        return $this->hasOne(ClassSubscriptionDiscount::class, 'class_reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function classHalfPriceDiscount()
    {
        return $this->hasOne(ClassHalfPriceDiscount::class, 'class_reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function classCreditDiscount()
    {
        return $this->hasOne(ClassCreditDiscount::class, 'class_reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function classMilitaryDiscount()
    {
        return $this->hasOne(ClassMilitaryDiscount::class, 'class_reg_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classCouponDiscounts()
    {
        return $this->hasMany(ClassCouponDiscount::class, 'class_reg_id');
    }

    /**
     * Check if the class registration has discount applied already
     * @return bool
     */
    public function getIsDiscountedAttribute()
    {
        return ($this->classCouponDiscounts->count() == 0);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cancellationNote()
    {
        return $this->hasOne(CancellationNote::class, 'class_registration_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        ClassRegistration::observe(new ClassRegistrationObserver());
    }

    public function isCertificateAvailable()
    {
        return ($this->certificate_file_path) ? true : false;
    }

    public function isConfirmed()
    {
        switch ($this->registrationOrder->courseTypeId()) {

            case Constant::SEMINAR:
                return $this->attended;
                break;
            case Constant::WEBCAST:
                return ($this->meeting_flag == 1 && $this->exam_flag == 1) ? 1 : 0;
                break;
            default:
                return $this->attended;
        }
    }

    public function attendSeminar($flag = true)
    {
        $this->attended = $flag;
        return $this->save();
    }

    public function attendMeeting($flag = true)
    {
        $this->meeting_flag = $flag;
        return $this->save();
    }

    public function takeExam($flag = true)
    {
        $this->exam_flag = $flag;
        return $this->save();
    }

    public function setSendReminderTrue()
    {
        $this->reminder_flag = true;
        return $this->save();
    }

    public function setCertificateSent()
    {
        $this->certificate_sent = true;
        return $this->save();
    }

    public function orderAdjustmentDiscounts()
    {
        return $this->morphMany(OrderAdjustmentDiscount::class, 'adjustable');
    }

    public function orderAdjustment()
    {
        return $this->morphOne(OrderAdjustment::class, 'adjustable');
    }

    public function getHasOrderAdjustmentAttribute()
    {
        return ($this->orderAdjustment) ? true : false;
    }

    public function orderAdjustmentHistory()
    {

        $note = adjustment_registration_added(
            $this,
            $this->registrationOrder,
            $this->item_charge
        );

        return OrderAdjustmentHistory::where('adjustment_action', 'ADDED')
            ->where('note', $note)
            ->orderby('created_at', 'desc')
            ->first();
    }

    public function getHasOrderAdjustmentHistoryAttribute()
    {
        return ($this->orderAdjustmentHistory()) ? true : false;
    }

}