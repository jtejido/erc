<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['course_id', 'start_date', 'end_date', 'instructor_name',
                           'primary_address', 'secondary_address', 'city', 'state',
                           'zip', 'instructions'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseCategories()
    {
        return $this->hasMany(CourseCategory::class, 'category_id');
    }
}