<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'book_discounts';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
}