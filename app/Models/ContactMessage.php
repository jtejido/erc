<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactMessage extends Model
{

    /**
     * Table used by model
     *
     * @var string
     */
    protected $table = 'contact_messages';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'phone', 'message'];
}