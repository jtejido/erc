<?php namespace App\Models;

use App\Models\ModelInterface\Imageable;
use App\Utilities\Constant;
use Illuminate\Database\Eloquent\Model;

class Book extends Model implements Imageable
{

    protected $guarded = ['id'];

    const TMAXLENGTH = 70;
    const FOLDERPATH = 'books/';
    const FOLDERPATH_THUMBS = 'books/thumbs/';
    const NO_AVAIL_THUMB = 'img/no-img-avail.png';

    public function getShortNameAttribute()
    {
        return (strlen($this->name) > self::TMAXLENGTH) ? tokenTruncate($this->name, self::TMAXLENGTH) . '...' : $this->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookEntries()
    {
        return $this->hasMany(BookEntry::class);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return '$'.number_format($this->price, 2);
    }

    /**
     * @return string
     */
    public function getDiscountedPrice()
    {
        return '$'.number_format($this->discounted_price, 2);
    }

    /**
     * @return string
     */
    public function getWeight()
    {
        return $this->weight.' lbs';
    }

    public function courses()
    {
        return $this->morphToMany(Course::class, 'linkable', 'course_links');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function orders()
    {
        return $this->morphMany(ProductOrders::class, 'orderable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function files()
    {
        return $this->morphMany('App\Models\FileEntry', 'imageable');
    }

    public function ebooks()
    {
        return $this->files()->where('type', Constant::EBOOK)->get();
    }

    public function ebook()
    {
        return $this->ebooks()->first();
    }

    public function ebookUpdates()
    {
        return $this->files()->where('type', Constant::EBOOK_UPDATE)->get();
    }

    /**
     * @param $query
     * @param $keyword
     * @return mixed
     */
    public function scopeSearchByKeyword($query, $keyword)
    {
        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("code", "LIKE", "%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }

    public function getLinkToBook($name = null, $admin = false)
    {
        $text = $name ? $name : $this->code;
        $link = ($admin)
            ? "<a href='".route('admin.books.show', [$this->id])."'>".$text."</a>"
            : "<a href='".route('products.show', [$this->id])."'>".$text."</a>";
        return $link;
    }

    public function getThumbnailPathAttribute()
    {
        $file = storage_path('app/').$this::FOLDERPATH_THUMBS.$this->thumbnail;
        return ($this->thumbnail && file_exists($file))
            ? $file
            : public_path($this::NO_AVAIL_THUMB);
    }

    public function getThumbnailRoute()
    {
        if($this->thumbnail)
            return route('book.thumbnail', $this->thumbnail);
        else
            return asset('img/no-img-avail.png');

    }


    public function productOrders()
    {
        return $this->morphMany('App\Models\ProductOrders', 'product');
    }

    public function canBeDeleted()
    {
        return $this->productOrders()->count()
            ? false
            : true;
    }

}
