<?php namespace App\Models;

use App\Observers\ClientYearlySubscriptionObserver;
use Illuminate\Database\Eloquent\Model;

class ClientYearlySubscription extends Model
{

    /**
     * Table used by model
     *
     * @var string
     */
    protected $table = 'client_yearly_subscriptions';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['yearly_subscription_id', 'user_id', 'temporary_price', 'paid_price',
                            'subscription_start_date', 'subscription_end_date', 'activation_code', 'activated'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function yearlySubscription()
    {
        return $this->belongsTo(YearlySubscription::class, 'yearly_subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userYearlySubscription()
    {
        return $this->hasOne(UserYearlySubscription::class, 'client_yearly_subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        ClientYearlySubscription::observe(new ClientYearlySubscriptionObserver());
    }
}