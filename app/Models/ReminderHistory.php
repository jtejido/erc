<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReminderHistory extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'to');
    }

    public function scopeReminders($query)
    {
        return $query
                ->where('remindable_type', 'Course')
                ->orWhere('remindable_type', 'CourseClass');
    }

    public function getUsernameAttribute()
    {
        return $this->user->contact->name . ' ('.$this->user->email . ')';
    }

    public function remindable()
    {
        return $this->morphTo();
    }

    public function isCourse()
    {
        return get_class($this->remindable) == Course::class;
    }

}
