<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTransaction extends Model
{


    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'order_transactions';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

}