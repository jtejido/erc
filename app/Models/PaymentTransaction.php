<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    const  UPDATED_AT = null;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'payment_transactions';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function isCheck()
    {
        return $this->card_brand == 'CHECK';
    }

    public function isCash()
    {
        return $this->card_brand == 'CASH';
    }

    public function getShippingFeeAttribute()
    {
        $fee = (empty($this->shipping_rates)) ? 0 : $this->shipping_rates;
        return '$'.number_format($fee, 2);
    }

    public function getAmountFormattedAttribute()
    {
        $amt = (empty($this->amount)) ? 0 : $this->amount;
        return '$'.number_format($amt, 2);
    }

    public function getCompleteBillingAddressAttribute()
    {

        return '<strong>'
            . $this->billing_company
            . '</strong>'
            . '<br/>'
            . $this->billing_address
            . '<br/>'
            . $this->billing_city.', '.$this->billing_state.' '.$this->billing_zip;
    }

    public function getCompleteShippingAddressAttribute()
    {

        $shipping = '<strong>'
            . $this->shipping_company
            . '</strong>'
            . '<br/>'
            . $this->shipping_address
            . '<br/>'
            . $this->shipping_city.', '.$this->shipping_state.' '.$this->shipping_zip;

        if(!empty(trim($this->shipping_county))) {
            $shipping .= ' (' . $this->shipping_county . ')';
        }

        return $shipping;

    }

    public function updateBillingAndShippingAddress($params)
    {
        $this->update([
            'billing_company'       => $params['billing_company'],
            'billing_address'       => $params['billing_address1'],
            'billing_city'          => $params['billing_city'],
            'billing_state'         => $params['billing_state'],
            'billing_zip'           => $params['billing_zip'],
            'shipping_company'      => $params['shipment_company'],
            'shipping_address'      => $params['shipment_address1'],
            'shipping_city'         => $params['shipment_city'],
            'shipping_state'        => $params['shipment_state'],
            'shipping_zip'          => $params['shipment_zip'],
            'shipping_county'       => $params['shipment_county'],
        ]);
    }

    public function getStateTaxFormattedAttribute()
    {
        return (!empty(trim($this->state_tax)))
            ? '$' . $this->state_tax
            : '$0.00';
    }

}