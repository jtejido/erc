<?php namespace App\Models;

use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Observers\RegistrationOrderObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistrationOrder extends Model
{
    use SoftDeletes;

    CONST MEETING_LINK = 'redirect/meeting';
    CONST TEST_LINk = 'redirect/exam';

    public static $FIRE_EVENTS = true;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'registration_orders';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'registrable_id', 'registrable_type', 'originating_agent_user_id',
        'status', 'has_pair', 'slug'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function registrable()
    {
        return $this->morphTo();
    }

    /**
     * @return bool
     */
    public function isCourse()
    {
        return get_class($this->registrable) == Course::class;
    }

    public function isClass()
    {
        return get_class($this->registrable) == CourseClass::class;
    }

    public function isWebCast()
    {
        if($this->isCourse()) return false;
        return $this->registrable->course->isWebcast();
    }

    public function isSeminar()
    {
        if($this->isCourse()) return false;
        return $this->registrable->course->isSeminar();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'originating_agent_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classRegistrations()
    {
        return $this->hasMany(ClassRegistration::class, 'registration_order_id');
    }

    public function isAttendee($contact_id = 0)
    {

        return ($this->classRegistrations()->where('attendee_contact_id', '=', $contact_id)->count())
            ? true
            : false;

    }

    public function countClassRegistrations()
    {
        return $this->hasMany(ClassRegistration::class, 'registration_order_id')
                    ->selectRaw('registration_order_id, count(*) as participants')
                    ->where('is_cancelled',0)
                    ->where('is_swapped',0)
                    ->where('is_new', 0)
                    ->groupBy('registration_order_id');
    }

    public function getCountClassRegistrationAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('countClassRegistrations')) {
            $this->load('countClassRegistrations');
        }

        $related = $this->getRelation('countClassRegistrations');

        // then return the count directly
        return ($related) ? (int) $related->participants : 0;
    }

    public function countClassRegistrationsTBC()
    {
        return $this->hasMany(ClassRegistration::class, 'registration_order_id')
                    ->selectRaw('registration_order_id, count(*) as participants_tbc')
                    ->where('is_cancelled',0)
                    ->where('is_swapped',0)
                    ->whereRaw('attended = 0 OR (meeting_flag = 0 AND exam_flag = 0)')
                    ->groupBy('registration_order_id');
    }

    public function getCountClassRegistrationTbcAttribute()
    {
        // if relation is not loaded already, let's do it first
        if (!$this->relationLoaded('countClassRegistrationsTBC')) {
            $this->load('countClassRegistrationsTBC');
        }

        $related = $this->getRelation('countClassRegistrationsTBC');

        // then return the count directly
        return ($related) ? (int) $related->participants_tbc : 0;
    }

    /**
     * @return mixed
     */
    public function trashedRegistrations()
    {
        return $this->classRegistrations()->withTrashed();
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return $this->isCourse()
                ? $this->registrable->title
                : $this->registrable->course->title;
    }

    public function files()
    {
        return $this->isCourse()
                ? $this->registrable->files
                : $this->registrable->course->files;
    }

    public function ebooks()
    {
        return $this->isCourse()
            ? $this->registrable->books
            : $this->registrable->course->books;
    }

    public function materials()
    {
        return $this->isCourse()
            ? $this->registrable->materials
            : $this->registrable->course->materials;
    }

    public function courses()
    {
        return $this->isCourse()
            ? $this->registrable->courses
            : $this->registrable->course->courses;
    }

    public function course()
    {
        return $this->isCourse()
            ? $this->registrable
            : $this->registrable->course;
    }

    public function description()
    {
        return $this->isCourse()
            ? $this->registrable->description
            : $this->registrable->course->description;
    }

    public function orderItem()
    {
        return $this->morphOne(OrderItem::class, 'orderable');
    }

    /**
 * @return string
 */
    public function date()
    {
        $object = $this->registrable;

        $string = '';

        if ($object->course && in_array($object->course->courseType->id, [Constant::SEMINAR, Constant::WEBCAST])) {
            $string = $object->shortDateRange;
        }

        return $string;
    }

    public function getDatesForEmailAttribute()
    {
        $object = $this->registrable;

        if ($this->isClass()) {
            return $object->dates_for_email;
        }

        return '';
    }

    public function getShortLocationAttribute()
    {
        $object = $this->registrable;

        $string = '';

        if ($this->isClass()) {
            $string = $object->short_location;
        }

        return $string;
    }

    public function getGeneralLocationAttribute()
    {
        $object = $this->registrable;

        $string = '';

        if ($this->isClass()) {
            $string = $object->short_city;
        }

        return $string;
    }

    /**
     * @return mixed
     */
    public function courseType()
    {
        return $this->isCourse()
                ? $this->registrable->courseType->name
                : $this->registrable->course->courseType->name;
    }

    /**
     * @return mixed
     */
    public function courseTypeId()
    {
        return $this->isCourse()
                ? $this->registrable->courseType->id
                : $this->registrable->course->courseType->id;
    }

    /**
     * @return mixed
     */
    public function originalPrice()
    {
        return $this->isCourse()
                 ? $this->registrable->price
                 : $this->registrable->course->price;
    }

    public function scopeAvailable($query)
    {
        return $query->where('updated_at', '>=', Carbon::now()->subYear());
    }

    public function isAvailable()
    {
        return $this->availability->gt(Carbon::now()->startOfDay());
    }

    public function getAvailabilityAttribute()
    {
        return $this->updated_at->addYear();
    }

    /**
     * @return bool
     */
    public function isCBT()
    {
        return $this->isCourse() && $this->courseTypeId() == Constant::COMPUTER_BASED;
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        RegistrationOrder::observe(new RegistrationOrderObserver());
    }

    public function getMeetingLinkAttribute()
    {
        return route('redirect.meeting', $this->slug);
    }

    public function getTestLinkAttribute()
    {
        return route('redirect.exam', $this->slug);
    }

    public function meetingLink($name='meeting')
    {
        $link = "<a href='%s' target='_blank'>%s</a>";
        return sprintf($link, $this->meeting_link, $name);
    }

    public function examLink($name='exam')
    {
        $link = "<a href='%s' target='_blank'>%s</a>";
        return sprintf($link, $this->test_link, $name);
    }

}