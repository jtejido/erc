<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'zip_codes';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['zip_code'];

    /**
     * @var bool
     */
    public $timestamps = false;
}