<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActingOriginatingAgent extends Model
{
    use SoftDeletes;

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'acting_originating_agents';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['orderable_id', 'orderable_type',
                           'acting_agent_id', 'status'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function orderable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}