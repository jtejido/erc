<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassCouponDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_coupon_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['coupon_discount_id', 'class_reg_id', 'deduction', 'is_deleted'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function couponDiscount()
    {
        return $this->belongsTo(CouponDiscount::class, 'coupon_discount_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classRegistration()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_id');
    }

}