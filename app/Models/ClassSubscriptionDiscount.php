<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassSubscriptionDiscount extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'class_subscription_discounts';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'user_id', 'client_subscription_id', 'class_reg_id', 'deduction'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function clientYearlySubscription()
    {
        return $this->belongsTo(ClientYearlySubscription::class, 'client_subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function classRegistration()
    {
        return $this->belongsTo(ClassRegistration::class, 'class_reg_id');
    }
}