<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderAdjustmentHistory extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'order_adjustment_histories';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['order_id', 'note', 'adjustment_action', 'amount'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}