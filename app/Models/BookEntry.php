<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookEntry extends Model
{

    protected $fillable = ['title','size','description','filename', 'mime', 'original_filename'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    public function getSizeAttribute($value)
    {
        return number_format($value/(1<<20),2)."MB";
    }

}
