<?php

namespace App\App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseLink extends Model
{

    public function linkable()
    {
        return $this->morphTo();
    }

}
