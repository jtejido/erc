<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'course_types';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;

}