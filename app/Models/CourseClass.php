<?php namespace App\Models;

use App\Models\RegistrationOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CourseClass extends Model
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'classes';

    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['course_id', 'code', 'start_date', 'end_date', 'location_id',
                           'start_time',  'end_time', 'instructor_id', 'address', 'city',
                           'state', 'zip', 'website', 'instructions', 'is_deactivated',
                            'is_private', 'webcast_notification_flag'];


    /**
     * @var array
     */
    protected $dates = ['start_date', 'end_date'];

    /**
     * @var bool
     */
    public $timestamps = true;

    protected static $exception = [60];


    public function getWebsiteAttribute()
    {
        return ($this->location) ? $this->location->url : '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function registrations()
    {
        return $this->morphMany(RegistrationOrder::class, 'registrable');
    }

    public function scopeActive($query)
    {
        return $query
            ->where('end_date', '>=', Carbon::now()->startOfDay())
            ->where('is_cancelled', false)
            ->where('is_deactivated', false);
    }

    public function scopeOngoing($query)
    {
        return $this->scopeActive($query)
            ->where('start_date', '<=', Carbon::now()->startOfDay());
    }

    public function getActiveAttribute()
    {
        return !$this->end->isPast();
    }

    public function getIsOngoingAttribute()
    {
        return $this->active && (
        $this->start->lt(Carbon::now()->startOfDay()));
    }

    public function getIsStartingInAWeekAttribute()
    {
        return ($this->start->copy()->subDays(7)->diffInDays(Carbon::now()) == 0)
            ? true
            : false;
    }

    public function getIsOngoingOnDayAttribute()
    {
        return $this->active && (
        $this->start_date->lte(Carbon::now()->startOfDay()));
    }

    public function getStartedAttribute()
    {
        return $this->start->isPast();
    }

    public function getStartAttribute()
    {
        return $this->start_date->copy()
            ->setTimeFromTimeString($this->start_time);
    }

    public function getEndAttribute()
    {
        return $this->end_date->copy()
            ->setTimeFromTimeString($this->end_time);
    }

    /**
     * @return mixed
     */
    public function activeRegistrationOrders()
    {
        return $this->registrations()->available()->get();
    }

    public function isAvailable()
    {
        return $this->availability->gt(Carbon::now()->startOfDay());
    }

    public function getAvailabilityAttribute()
    {
        return $this->start->addYear();
    }

    public function getTitleAttribute()
    {
        return $this->course->title;
    }

    public function getTitleWithInfoAttribute()
    {
        return $this->title . ' (Code: ' . $this->id . ')';
    }

    public function getDescriptionAttribute()
    {
        return $this->course->description;
    }

    public function getSkuAttribute()
    {
        return $this->course->sku;
    }

    public function getPriceAttribute()
    {
        return '$' . $this->course->price;
    }

    public function getDurationAttribute()
    {
        return $this->course->duration;
    }

    public function getTypeAttribute()
    {
        return $this->course->course_type_id;
    }

    public function getDateRangeAttribute()
    {
        if ($this->start->isSameDay($this->end)) {
            $range = $this->start->toFormattedDateString()
                . ' from '
                . $this->start_time_formatted
                . '-'
                . $this->end_time_formatted;
        } else {
            $range = $this->start->toFormattedDateString()
                . ' from '
                . $this->start_time_formatted
                . '-'
                . $this->end_time_formatted
                . ' and concludes on '
                . $this->end->toFormattedDateString()
                . ' from '
                . $this->start_time_formatted
                . '-'
                . $this->end_time_formatted;
        }

        return $range;
    }

    public function getDateRangeOnlyAttribute()
    {
        if (!$this->start->isSameDay($this->end))
        $range = $this->start->format('m/d/y') . ' - ' . $this->end->format('m/d/y');
        else
        $range = $this->start->format('m/d/y');

        return $range;
    }

    public function getDateTimeRange($format = 'M d, Y g:ia')
    {
        if (!$this->start_date->eq($this->end_date))
            $dates = $this->start->format($format) . ' - ' .  $this->end->format($format);
        else
            $dates = $this->start->format($format) . ' - ' . $this->end->format('g:ia');
        return $dates;
    }

    public function getShortDateRangeAttribute()
    {
        if ($this->start->format('n') != $this->end->format('n'))
            $range = $this->start->format('M j, Y') . ' to ' . $this->end->format('M j, Y');
        elseif($this->start->format('j') == $this->end->format('j'))
            $range = $this->start->format('M j, Y');
        else
            $range = $this->start->format('M j') . ' - ' . $this->end->format('j, Y');
        return $range;
    }

    public function getDatesAttribute()
    {
        if (!$this->start->isSameDay($this->end))
            $dates = $this->start->toFormattedDateString() . ' - ' . $this->end->toFormattedDateString();
        else
            $dates = $this->start->toFormattedDateString();
        return $dates;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function instructor()
    {
        return $this->belongsTo(Instructor::class, 'instructor_id');
    }

    /**
     * @return string
     */
    public function instructorName()
    {
        if ($this->instructor) {
            return $this->instructor->name;
        }

        return '';
    }

    public function getAddressTextAttribute()
    {

        if ($this->course->isCbt())
            $addressText = 'Online Training';
        elseif ($this->course->isWebcast())
            $addressText = 'Webcast';
        else
            $addressText = $this->address . ' ' . $this->city . ', ' . $this->state . ' ' . $this->zip;
        return $addressText;
    }

    public function getLinkToClassList()
    {
        return "<a href='" . route('admin.classes', $this->course->id) . "'>$this->id</a>";
    }

    public function getLinkedClassAdmin($text = '')
    {
        $text = ($text) ? $text : $this->sku;
        return "<a href='".route('admin.classes.show', [$this->id])."'>".$text."</a>";
    }

    public function getClassDateLabel()
    {
        if (!$this->active) {
            return "<span class='label label-danger'> Finished</span>";
        } elseif ($this->is_ongoing) {
            return "<span class='label label-success'> In Progress </span>";
        } else {
            return "<span class='label label-default'> Not Started </span>";
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function classes()
    {
        return $this->morphedByMany(CourseClass::class, 'linkable', 'class_links', 'class_id');
    }

    public function getFormattedTitle()
    {
        $title = $this->start_date->toDateString() . ' ' . $this->title;
        return $title;
    }

    public function getTitleCodeAttribute()
    {
        return $this->title . ' (Code: ' . $this->id . ')';
    }

    public function getTitleCodeLinkAttribute()
    {
        return
            $this->title .
            ' <a href="' . route('admin.classes.show', [$this->id]) . '">(Code: ' . $this->id . ')</a>';
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }


    public function getLocationNameAttribute()
    {
        return ($this->location) ? $this->location->location_name : '--';
    }

    public function getLocationNameLinkedAttribute()
    {
        return ($this->location)
            ? '<a href="' . $this->location->url . '" target="_blank">' . $this->locationName . '</a>'
            : '--';
    }

    public function getLocationNameLinkAttribute()
    {
        return ($this->location)
            ? '<a href="' . $this->location->url . '" target="_blank">' . $this->locationName . ' <i class="fa fa-external-link"></i></a>'
            : '--';
    }

    public function getLocationAddressAttribute()
    {
        return ($this->location) ? $this->location->location : '--';
    }

    public function getLocationLinkAttribute()
    {
        return ($this->location) ? $this->location->url : '--';
    }

    public function getCompleteLocationAttribute()
    {
        return ($this->location) ? $this->location->location_name . ' ' . $this->location->location : '--';
    }

    public function getLocationPhoneAttribute()
    {
        return ($this->location) ? $this->location->phone_number : '--';
    }

    public function getShortLocationAttribute()
    {
        return ($this->location) ? $this->location->shortLocation : '--';
    }

    public function getShortCityAttribute()
    {
        return ($this->location) ?
            ($this->location->generalLocation) ?
                $this->location->generalLocation->location
                : '--'
            : '--';
    }

    public function getCityAttribute()
    {
        return ($this->location) ? $this->location->city : '--';
    }

    public function getStateAttribute()
    {
        return ($this->location) ? $this->location->state : '--';
    }

    public function getZipAttribute()
    {
        return ($this->location) ? $this->location->zip : '--';
    }

    public function getStartFormattedAttribute()
    {
        return $this->start_date->copy()
            ->setTimeFromTimeString($this->start_time)
            ->format('m/d/y g:i');
    }

    public function getTitleLocationAttribute()
    {
        if($this->course->isSeminar)
            return $this->short_city . ' | ' . $this->shortDateRange;
        else
            return $this->shortDateRange;
    }

    public function getTitleTileLocationAttribute()
    {
        if($this->course->isSeminar)
            return $this->shortLocation . ' - ' . $this->shortDateRange;
        else
            return $this->shortDateRange;
    }

    public function getTitleLocationLinkAttribute()
    {
        return "<a href='".route('admin.classes.show', [$this->id])."'>$this->titleLocation</a>";
    }

    public function getTimeAttribute()
    {
        return $this->start->format('g:i a') . ' - ' . $this->end->format('g:i a');
    }

    public function isReminderEligible($months)
    {
        $flag = true;
        // participated
        // date
        $now = Carbon::now();
        $flag = ($now->diffInDays($this->start->addMonths($months)) == 0) ? true : false;
        return $flag;
    }

    public function getStartTimeFormattedAttribute()
    {

        return $this->start->format('g:ia');

    }

    public function getEndTimeFormattedAttribute()
    {

        if($this->course->isSeminar) {
            return $this->end->format('g:ia');
        } elseif(in_array($this->course->id, $this::$exception)) {
            return $this->end->format('g:ia') . ' PST';
        } else {
            return $this->end->format('g:ia') . ' ' . env('APP_TIMEZONE_LABEL', 'EST');
        }

    }

    public function getRegTimeFormattedAttribute()
    {

        if($this->course->isSeminar) {
            return $this->start->subMinutes(30)->format('g:ia');
        } elseif(in_array($this->course->id, $this::$exception)) {
            return $this->start->subMinutes(30)->format('g:ia') . ' PST';
        } else {
            return $this->start->subMinutes(30)->format('g:ia') . ' ' . env('APP_TIMEZONE_LABEL', 'EST');
        }

    }

    public function getDatesForEmailAttribute()
    {
        if (!$this->start->isSameDay($this->end))
            $dates = $this->start->format('F d, Y') . ' through ' . $this->end->format('F d, Y');
        else
            $dates = $this->start->format('F d, Y');
        return $dates;
    }

    public function getInstructionAttribute()
    {
        return $this->instructions;
    }

    public function getIsCaryAttribute()
    {
        return (strtolower($this->city) == 'cary');
    }

}