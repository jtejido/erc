<?php namespace App\Models;

use App\Models\ModelInterface\Imageable;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Course extends Model implements Imageable
{

    /**
     * Table used by Model
     *
     * @var string
     */
    protected $table = 'courses';
    const FOLDERPATH = 'course/materials/';
    /**
     * Attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
                'course_mill_course_id', 'course_mill_session_id', 'sku', 'title', 'description',
                'course_type_id', 'price', 'duration', 'citation', 'time', 'slug',
                'meeting_id', 'test_link', 'is_deactivated', 'topic_id', 'additional_info',
                'reviews'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function courseCategories()
    {
        return $this->hasMany(CourseCategory::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function courseType()
    {
        return $this->belongsTo(CourseType::class);
    }

    /**
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return $this->courseType->name;
    }

    /**
     * @return mixed
     */
    public function getTypeIdAttribute()
    {
        return $this->courseType->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes()
    {
        return $this->hasMany(CourseClass::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function registrations()
    {
        return $this->morphMany(RegistrationOrder::class, 'registrable');
    }

    /**
     * @return mixed
     */
    public function activeRegistrationOrders()
    {
        return $this->registrations()->available()->get();
    }

    /**
     * @return bool
     */
    public function isSeminar()
    {
        return $this->courseType->id == Constant::SEMINAR;
    }

    /**
     * @return bool
     */
    public function getIsSeminarAttribute()
    {
        return $this->isSeminar();
    }

    /**
     * @return bool
     */
    public function isWebCast()
    {
        return $this->courseType->id == Constant::WEBCAST;
    }

    /**
     * @return bool
     */
    public function isCBT()
    {
        return $this->courseType->id == Constant::COMPUTER_BASED;
    }

    /**
     * @return bool
     */
    public function isInTransportationCategory()
    {
        $bool = false;

        foreach ($this->courseCategories as $courseCategory) {
            if ($courseCategory->category->id == Constant::HMT_CAT) {
                $bool = true;
            }
        }

        return $bool;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function books()
    {
        return $this->morphedByMany(Book::class, 'linkable', 'course_links');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function courses()
    {
        return $this->morphedByMany(Course::class, 'linkable', 'course_links');

    }


    /**
     * @return array|null
     */
    public function getSpanAttribute()
    {
        if ($this->course_type_id === Constant::COMPUTER_BASED) {
            return null;
        }

        $span = [
            'days'  => 0,
            'hours' => 0,
        ];

        $duration = $this->duration;

        $patterns = collect([
            /*
                1 Day; Lunch is provided
                2 Hour Webcast
                4 Hour Webcast
                2 Days; Lunch is provided both days
                4 hours (self paced)
             */
            'word' => '/^([0-9]+) ([Hh]ours?|[Dd]ays?)/',
            /*
                12:00 pm - 5:00 pm
             */
            'range' => '/([0-9]{1,2}:[0-9]{2}\s?[ap]m)/'
        ]);

        $patterns->each(function($pattern, $name) use (&$span) {
            $matches = [];
            if (preg_match_all($pattern, $this->duration, $matches) > 0) {
                switch ($name) {
                    case 'word':
                        if (str_contains(strtolower($matches[2][0]), 'day')) {
                            $span['days'] = (int) $matches[1][0];
                        }

                        if (str_contains(strtolower($matches[2][0]), 'hour')) {
                            $span['hours'] = (int) $matches[1][0];
                        }
                        break;
                    case 'range':
                        // always a time range is within/one day
                        $match = $matches[0];
                        $start = $match[0];
                        $end = $match[1];
                        $start = Carbon::now()->setTimeFromTimeString(date('G:i:00', strtotime($start)));
                        $end   = Carbon::now()->setTimeFromTimeString(date('G:i:00', strtotime($end)));

                        $span['hours'] = $start->diffInHours($end);
                    break;
                }
            }
        });

        return $span;
    }

    public function files()
    {
        return $this->morphMany('App\Models\FileEntry', 'imageable');
    }

    public function ebooks()
    {
        return $this->files()->where('type','EBOOK');
    }

    public function materials()
    {
        return $this->files()->where('type','MATERIAL');
    }

    public function getLinkedCourseTitle()
    {
        return "<a href='".route('course.show', [$this->slug])."'>".$this->sku."</a>";
    }

    public function getLinkedCourseTitleAdmin($text = '')
    {
        $text = ($text) ? $text : $this->sku;
        return "<a href='".route('admin.course.edit', [$this->id])."'>".$text."</a>";
    }

    CONST LINK = "https://global.gotomeeting.com/join/";


    public function meetingLink($name='meeting')
    {
        $link = "<a href='%s' target='_blank'>%s</a>";
        return sprintf($link, $this->meetingUrl, $name);
    }

    public function getTestUrlAttribute()
    {
        return env('CM_LINK')."?quiz=" . $this->test_link;
    }

    public function getMeetingUrlAttribute()
    {
        return $this->meeting_id;
    }

    public function examLink($name='exam')
    {
        $link = "<a href='%s' target='_blank'>%s</a>";
        return sprintf($link, $this->test_link, $name);
    }

    public function getPublicUrlAttribute()
    {
        return route('course.show', $this->id);
    }

    public function getPrice()
    {
        return '$'.number_format($this->price, 2);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public static function boot() {
        parent::boot();

        static::saving(function($model){
            foreach ($model->attributes as $key => $value) {
                if($value == '' AND $key == 'topic_id') {
                    $model->{$key} = empty($value) ? null : $value;
                }
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(CourseOrder::class, 'course_id');
    }
}