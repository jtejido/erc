<?php namespace App\Models;

use App\Observers\ContactObserver;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword, SoftDeletes;

    /**
    * Table used by Model
    *
    * @var string
    */
    protected $table = 'contacts';

    /**
    * Attributes that are mass assignable
    *
    * @var array
    */
    protected $fillable = ['course_mill_user_id', 'first_name', 'last_name', 'company',
                           'title', 'address1', 'address2', 'phone_local',
                           'city', 'state', 'zip', 'phone', 'fax','address_same_as_billing',
                           'address_same_as_shipping', 'billing_address', 'billing_city',
                           'billing_state', 'billing_zip', 'shipping_address',
                           'shipping_city', 'shipping_state', 'shipping_zip'];

    /**
    * @var bool
    */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'contact_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function militaryDiscounts()
    {
        return $this->hasMany(ClassMilitaryDiscount::class, 'contact_id', 'id');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @return string
     */
    public function getFNameAttribute()
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getAddressAttribute() {
        return implode("\n",[
            $this->address1,
            $this->address2,
            $this->city.', '.$this->state.' '.$this->zip
        ]);
    }

    public function getCompanyAddressAttribute()
    {
        return '<strong>'
            . $this->company
            . '</strong>'
            . '<br/>'
            . $this->address1
            . '<br/>'
            . $this->city.', '.$this->state.' '.$this->zip;
    }

    public function getCompleteAddressAttribute()
    {
        return $this->address1
            . ' '
            . $this->city
            . ', '
            . $this->state
            . ' '
            . $this->zip;
    }

    public function getCompleteBillingAddressAttribute()
    {

        if($this->address_same_as_billing) {
            return $this->company_address;
        }

        if($this->emptyBillingAddress()) {
            return $this->company_address;
        }

        return '<strong>'
            . $this->company
            . '</strong>'
            . '<br/>'
            . $this->billing_address
            . '<br/>'
            . $this->billing_city.', '.$this->billing_state.' '.$this->billing_zip;

    }

    public function getUserBillingAddressAttribute()
    {
        if($this->address_same_as_billing) return $this->address1;
        if($this->emptyBillingAddress()) return $this->address1;
        return $this->billing_address;
    }

    public function getUserBillingCityAttribute()
    {
        if($this->address_same_as_billing) return $this->city;
        if($this->emptyBillingAddress()) return $this->city;
        return $this->billing_city;
    }

    public function getUserBillingStateAttribute()
    {
        if($this->address_same_as_billing) return $this->state;
        if($this->emptyBillingAddress()) return $this->state;
        return $this->billing_state;
    }

    public function getUserBillingZipAttribute()
    {
        if($this->address_same_as_billing) return $this->zip;
        if($this->emptyBillingAddress()) return $this->zip;
        return $this->billing_zip;
    }

    public function emptyBillingAddress()
    {
        return (empty(trim($this->billing_address))
        or empty(trim($this->billing_city))
        or empty(trim($this->billing_state))
        or empty(trim($this->billing_zip)));
    }

    public function emptyShippingAddress()
    {
        return (empty(trim($this->shipping_address))
            or empty(trim($this->shipping_city))
            or empty(trim($this->shipping_state))
            or empty(trim($this->shipping_zip)));
    }

    public function getCompleteShippingAddressAttribute()
    {

        if($this->address_same_as_shipping) {
            return $this->company_address;
        }

        if($this->emptyShippingAddress()) {
            return $this->company_address;
        }

        return '<strong>'
            . $this->company
            . '</strong>'
            . '<br/>'
            . $this->shipping_address
            . '<br/>'
            . $this->shipping_city.', '.$this->shipping_state.' '.$this->shipping_zip;

    }

    public function getUserShippingAddressAttribute()
    {
        if($this->address_same_as_shipping) return $this->address1;
        if($this->emptyShippingAddress()) return $this->address1;
        return $this->shipping_address;
    }

    public function getUserShippingCityAttribute()
    {
        if($this->address_same_as_shipping) return $this->city;
        if($this->emptyShippingAddress()) return $this->city;
        return $this->shipping_city;
    }

    public function getUserShippingStateAttribute()
    {
        if($this->address_same_as_shipping) return $this->state;
        if($this->emptyShippingAddress()) return $this->state;
        return $this->shipping_state;
    }

    public function getUserShippingZipAttribute()
    {
        if($this->address_same_as_shipping) return $this->zip;
        if($this->emptyShippingAddress()) return $this->zip;
        return $this->shipping_zip;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function generalNote()
    {
        return $this->hasOne(CustomerNote::class, 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes()
    {
        return $this->hasMany(ClassRegistration::class, 'attendee_contact_id');
    }

    /**
     * Event in boot
     */
    public static function boot()
    {
        parent::boot();

        Contact::observe(new ContactObserver());
    }

    public function hasNoCompany()
    {
        return empty(trim($this->company));
    }

    public function getHasCmidAttribute()
    {
        return empty(trim($this->course_mill_user_id))
            ? false
            : true;
    }

}