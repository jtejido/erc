<?php namespace App\Services;

use App\Http\Requests\Request;
use App\Models\CourseClass;
use App\Repositories\ClassRepository;
use App\Repositories\CourseRepository;
use App\Utilities\Constant;

class ClassService {


    private $courseRepo;
    private $classRepo;

    /**
     * ClassService constructor.
     * @param CourseRepository $courseRepository
     * @param ClassRepository $classRepository
     */
    public function __construct(CourseRepository $courseRepository,
                                ClassRepository $classRepository)
    {
        $this->courseRepo = $courseRepository;
        $this->classRepo = $classRepository;
    }

    public function getNumberOfParticipantsPerCourseTBC($course_id)
    {
        $class = $this->courseRepo->getById($course_id);
        return $this->countParticipantsWhoAttended($class, false);
    }

    public function getNumberOfParticipantsPerClassTBC($class_id)
    {
        $class = $this->classRepo->getById($class_id);
        return $this->countParticipantsWhoAttended($class, false);
    }

    public function getNumberOfParticipantsPerCourse($course_id)
    {
        $class = $this->courseRepo->getById($course_id);
        return $this->countParticipants($class);
    }

    public function getNumberOfParticipantsPerClass($class_id)
    {
        $class = $this->classRepo->getById($class_id);
        return $this->countParticipants($class);
    }

    private function countParticipants($class)
    {
        $count = 0;
        foreach($class->registrations as $registration)
        {
            if($registration->status == Constant::INVOICED || $registration->status == Constant::COMPLETE) {
                $count += $registration->classRegistrations
                    ->where('is_cancelled',0)
                    ->where('is_swapped',0)
                    ->where('is_new', 0)
                    ->count();
            }
        }
        return $count;
    }

    private function countParticipantsWhoAttended($class, $attended = true)
    {
        $count = 0;
        foreach($class->registrations as $registration)
        {
            if($registration->status == Constant::INVOICED || $registration->status == Constant::COMPLETE) {

                foreach ($registration->classRegistrations as $classReg) {

                    // skip swapped or cancelled students
                    if ($classReg->is_swapped || $classReg->is_cancelled || $classReg->is_new)
                        continue;

                    switch($registration->courseTypeId()) {
                        case Constant::SEMINAR:
                            $flag = $classReg->isConfirmed();
                            break;
                        case Constant::WEBCAST:
                            $flag = $classReg->isConfirmed();
                            break;
                        case Constant::COMPUTER_BASED:
                            $flag = 1;
                            break;
                        default:
                            $flag = $classReg->attended;
                    }
                    if ($attended)
                        $count += ($flag) ? 1 : 0;
                    else
                        $count += (!$flag) ? 1 : 0;
                }
            }
        }
        return $count;
    }

    public function getNumberOfClassThatNeedsAction()
    {
        $count = 0;
        $courses = $this->courseRepo->getAll();
        foreach ($courses as $course) {
            foreach ($course->classes as $class) {
                $count += ($this->countParticipantsWhoAttended($class, false)) ? 1 : 0;
            }
        }


        return $count;
    }

    public function linkItem($request)
    {
        $classId = $request->get('class_id');
        $item_id = $request->get('item_id');
        $class = CourseClass::findOrFail($classId);
        $linkClass = CourseClass::findOrFail($item_id);
        $class->classes()->save($linkClass);
        $linkClass->classes()->save($class);
        return true;
    }

    public function unLinkItem($request)
    {
        $classId = $request->get('class_id');
        $item_id = $request->get('item_id');
        $class = CourseClass::findOrFail($classId);
        $class->classes()->detach($item_id);
        $linkedClass = CourseClass::findOrFail($item_id);
        $linkedClass->classes()->detach($classId);
        return true;
    }

    public function isClassDeletable($id)
    {
        $class = CourseClass::findOrFail($id);
        return ($class->registrations->count()) ? false : true;
    }


}