<?php namespace App\Services;

use App\Models\OrderAdjustment;
use App\Models\OrderItem;
use net\authorize\api\contract\v1\MerchantAuthenticationType;
use net\authorize\api\contract\v1\CreditCardType;
use net\authorize\api\contract\v1\PaymentType;
use net\authorize\api\contract\v1\OrderType;
use net\authorize\api\contract\v1\LineItemType;
use net\authorize\api\contract\v1\CustomerDataType;
use net\authorize\api\contract\v1\CustomerAddressType;
use net\authorize\api\contract\v1\TransactionRequestType;
use net\authorize\api\contract\v1\CreateTransactionRequest;
use net\authorize\api\controller\CreateTransactionController;
use net\authorize\api\constants\ANetEnvironment;
use net\authorize\api\contract\v1\ExtendedAmountType;

class AuthorizeService
{
    /**
     * @var mixed
     */
    private $apiId;

    /**
     * @var mixed
     */
    private $transactionKey;

    /**
     * @var string
     */
    private $aNetEnv;

    /**
     * AuthorizeService constructor
     */
    public function __construct()
    {
        $this->apiId = env('AUTHORIZENET_APP_LOGIN_ID');
        $this->transactionKey = env('AUTHORIZENET_TRANSACTION_KEY');

        if (!defined('AUTHORIZENET_LOG_FILE')) {
            define('AUTHORIZENET_LOG_FILE', storage_path('logs/authorize.log'));
        }

        $this->aNetEnv = env('APP_ENV') == 'production' ? ANetEnvironment::PRODUCTION
                                                        : ANetEnvironment::SANDBOX;

    }

    /**
     * Charge a credit card
     *
     * @param $cardNumber
     * @param $cvv
     * @param $cardExpiration
     * @return array
     * @throws \Exception
     */
    public function chargeCreditCard($cardNumber, $cvv, $cardExpiration)
    {
        try {

            // setup API credentials
            $authentication = new MerchantAuthenticationType();
            $authentication->setName($this->apiId);
            $authentication->setTransactionKey($this->transactionKey);
            $refId = 'ref' . time();

            // payment data for credit card
            $creditCard = new CreditCardType();
            $creditCard->setCardNumber($cardNumber);
            $creditCard->setCardCode($cvv);
            $creditCard->setExpirationDate($cardExpiration);

            $payment = new PaymentType();
            $payment->setCreditCard($creditCard);

            return ['auth'    => $authentication,
                    'ref'     => $refId,
                    'payment' => $payment
            ];

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Set order info
     *
     * @param $description
     * @return OrderType
     * @throws \Exception
     */
    public function orderInfo($description)
    {
        try {

            // order info
            $order = new OrderType();
            $order->setInvoiceNumber(time());
            $order->setDescription($description);

            return $order;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Set line items
     *
     * @param OrderItem $item
     * @return LineItemType
     * @throws \Exception
     */
    public function lineItem(OrderItem $item)
    {
        try {

            $lineItem = new LineItemType();
            $lineItem->setItemId($item->id);
            $lineItem->setName($item->orderable_type);
            $lineItem->setDescription($item->orderable_type);
            $lineItem->setQuantity("1");
            $lineItem->setUnitPrice($item->price_charged);
            $lineItem->setTaxable(false);

            return $lineItem;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param OrderAdjustment $orderAdjustment
     * @return LineItemType
     * @throws \Exception
     */
    public function lineItemOrderModification(OrderAdjustment $orderAdjustment)
    {
        try {

            $lineItem = new LineItemType();
            $lineItem->setItemId($orderAdjustment->id);
            $lineItem->setName($orderAdjustment->adjustment_action);
            $lineItem->setDescription($orderAdjustment->note);
            $lineItem->setQuantity("1");
            $lineItem->setUnitPrice($orderAdjustment->amount);
            $lineItem->setTaxable(false);

            return $lineItem;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Set customer info
     *
     * @param array $customerInfo
     * @return CustomerDataType
     * @throws \Exception
     */
    public function customerInfo($customerInfo = [])
    {
        try {

            // customer info
            $customer = new CustomerDataType();
            $customer->setEmail($customerInfo['email']);

            // bill to
            $billTo = new CustomerAddressType();
            $billTo->setFirstName($customerInfo['first_name']);
            $billTo->setLastName($customerInfo['last_name']);
            $billTo->setCompany($customerInfo['company']);
            $billTo->setAddress($customerInfo['address']);
            $billTo->setState($customerInfo['state']);
            $billTo->setCity($customerInfo['city']);
            $billTo->setZip($customerInfo['zip']);

            return ['customer' => $customer,
                    'billTo'   => $billTo
            ];

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create a transaction request
     *
     * @param array $transactionInfo
     * @return \net\authorize\api\contract\v1\AnetApiResponseType
     * @throws \Exception
     */
    public function createTransaction($transactionInfo = [])
    {
        try {

            $transactionRequestType = new TransactionRequestType();
            $transactionRequestType->setTransactionType(env('AUTHORIZE_TRANS_TYPE', 'authOnlyTransaction'));
            $transactionRequestType->setAmount($transactionInfo['amount']);
            $transactionRequestType->setPayment($transactionInfo['payment']);
            $transactionRequestType->setOrder($transactionInfo['order']);
            $transactionRequestType->setCustomer($transactionInfo['customer']);
            $transactionRequestType->setBillTo($transactionInfo['billTo']);

            //shipping info
            if(isset($transactionInfo['shipping_rates'])){
                $shippingTransaction = new ExtendedAmountType();
                $shippingTransaction->setAmount($transactionInfo['shipping_rates']);
                $shippingTransaction->setName(env('SHIPPING_PROVIDER','UPS'));
                $transactionRequestType->setShipping($shippingTransaction);
            }


            foreach ($transactionInfo['items'] as $items) {
                $transactionRequestType->addToLineItems(
                   isset($transactionInfo['is_order_modification'])
                    ? $this->lineItemOrderModification($items)
                    : $this->lineItem($items)
                );
            }

            $request = new CreateTransactionRequest();
            $request->setMerchantAuthentication($transactionInfo['auth']);
            $request->setRefId($transactionInfo['ref']);
            $request->setTransactionRequest($transactionRequestType);

            $controller = new CreateTransactionController($request);

            $response = $controller->executeWithApiResponse($this->aNetEnv);

            return $response;

        } catch (\Exception $e) {
            throw $e;
        }
    }
}