<?php
namespace App\Services;

use App\Services\Mailer\EmailTemplateMailer;
use Closure;
use Illuminate\Contracts\Mail\Mailer as MailerContract;
use Illuminate\Auth\Passwords\PasswordBroker as BasePasswordBroker;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\PasswordBroker as PasswordBrokerContract;
use Illuminate\Contracts\Auth\UserProvider;

class PasswordBroker extends BasePasswordBroker implements PasswordBrokerContract
{

    /**
     * Create a new password broker instance.
     *
     * @param  \Illuminate\Auth\Passwords\TokenRepositoryInterface  $tokens
     * @param  \Illuminate\Contracts\Auth\UserProvider  $users
     * @param  \Illuminate\Contracts\Mail\Mailer  $mailer
     * @param  string  $emailView
     * @return void
     */
    public function __construct(TokenRepositoryInterface $tokens,
                                UserProvider $users,
                                MailerContract $mailer,
                                $emailView,
                                EmailTemplateMailer $emailer)
    {
        $this->users = $users;
        $this->mailer = $mailer;
        $this->tokens = $tokens;
        $this->emailView = $emailView;
        $this->emailer = $emailer;
    }

    /**
     * Send the password reset link via e-mail.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $token
     * @param  \Closure|null  $callback
     * @return int
     */
    public function emailResetLink(CanResetPasswordContract $user, $token, Closure $callback = null)
    {
        $data = [
            'subject' => 'Password Recovery Request - Environmental Resource Center',
            'to'      => $user->getEmailForPasswordReset(),
            'data'    => [
                'name' => $user->contact->name,
                'link' => env('APP_URL').'/password/reset/'.$token,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
                'greeting'  => "Forgot your password? We are happy to help! Just reset your password by clicking the link below:"
            ],
        ];

        if (! is_null($callback)) {
            $data = call_user_func($callback, $data);
        }

        $this->emailer->send('ua-forgot-password', $data);
        return self::RESET_LINK_SENT;
    }
}