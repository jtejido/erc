<?php namespace App\Services;

use App\Models\User;
use App\Models\Order;
use App\Models\ClassRegistration;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\ClassMilitaryDiscountRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Repositories\OrderItemRepository;
use App\Utilities\Constant;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\OrderAdjustmentRepository;
use App\Facades\OrderModificationService as ModificationService;

class OrderModificationDiscountService
{

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @var ClassHalfPriceDiscountRepository
     */
    private $classHalfPriceDiscountRepo;

    /**
     * @var ClassCouponDiscountRepository
     */
    private $classCouponDiscountRepo;

    /**
     * @var ClassMilitaryDiscountRepository
     */
    private $classMilitaryDiscountRepo;

    /**
     * @var ClassCreditDiscountRepository
     */
    private $classCreditDiscountRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var OrderAdjustmentDiscountRepository
     */
    private $orderAdjustmentDiscountRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var ClassCombinationRepository
     */
    private $classCombinationRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @param ClassMilitaryDiscountRepository $classMilitaryDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param OrderItemRepository $orderItemRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     */
    public function __construct(OrderAdjustmentRepository $orderAdjustmentRepo,
                                ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo,
                                ClassMilitaryDiscountRepository $classMilitaryDiscountRepo,
                                ClassCreditDiscountRepository $classCreditDiscountRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                OrderItemRepository $orderItemRepo,
                                ClassCombinationRepository $classCombinationRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
        $this->classHalfPriceDiscountRepo = $classHalfPriceDiscountRepo;
        $this->classCouponDiscountRepo = $classCouponDiscountRepo;
        $this->classMilitaryDiscountRepo = $classMilitaryDiscountRepo;
        $this->classCreditDiscountRepo = $classCreditDiscountRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->orderAdjustmentDiscountRepo = $orderAdjustmentDiscountRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->classCombinationRepo = $classCombinationRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     *  Recompute halfprice discount when cancellation is made
     *
     * @param Collection $classRegistrations
     * @param User $agent
     * @param array $deferredHalfPriceRegistrationIds = []
     * @param array $deferredGroupRegistrationIds
     * @throws \Exception
     */
    public function reComputeOrderWhenCancelled(Collection $classRegistrations,
                                                User $agent,
                                                $deferredHalfPriceRegistrationIds = [],
                                                $deferredGroupRegistrationIds = [])
    {
        if ($classRegistrations->count()) {
            $ctr = 0;

            foreach ($classRegistrations as $classRegistration) {

                if (!$classRegistration->subscriptionDiscount and
                    !$classRegistration->corporate_seat_credit_applied and
                    !$classRegistration->is_cancelled) {

                    $registrationOrder = $classRegistration->registrationOrder;
                    $orderItem = $registrationOrder->orderItem;
                    $order = $orderItem->order;
                    $originalPrice = $registrationOrder->originalPrice();

                    $ctr++;

                    if ($classRegistration->classMilitaryDiscount) {
                        $originalPrice = apply_gsa_discount($originalPrice);
                    }

                    if ($classRegistration->classHalfPriceDiscount) {
                        $classHalfPriceDiscount = $classRegistration->classHalfPriceDiscount;
                        $deduction = ($originalPrice / 2);
                        $newPrice = $originalPrice - $deduction;

                        if ($ctr < 3 and $classRegistration->is_half_priced) {

                            $classRegistration->item_charge = $originalPrice;
                            $classRegistration->paid_charge = $originalPrice;
                            $classRegistration->is_half_priced = false;
                            $classRegistration->save();

                            $classHalfPriceDiscount->is_deleted = true;
                            $classHalfPriceDiscount->save();

                            $classCombination = $this->classCombinationRepo
                                                     ->getBySingeRegId($registrationOrder->id);

                            if ($registrationOrder->has_pair and $classCombination) {
                                $pairedRegistration = $classCombination->firstRegistration->id == $registrationOrder->id
                                                        ? $classCombination->secondRegistration
                                                        : $classCombination->firstRegistration;

                                $pairClass = $this->classRegistrationRepo->getByRegistrationAndAttendee(
                                    $pairedRegistration->id,
                                    $classRegistration->attendee_contact_id
                                    );

                                if ($pairedRegistration and
                                    ($pairClass and !$pairClass->is_cancelled and !$pairClass->is_half_priced)) {

                                    $classRegistrationIdsWithDisc = [
                                        'class_one' => $classRegistration,
                                        'class_two' => $pairClass
                                    ];

                                    foreach ($classRegistrationIdsWithDisc as $key => $classRegObj) {
                                        $classRegObj->item_charge -= 50;
                                        $classRegObj->paid_charge -= 50;
                                        $classRegObj->save();

                                        if ($key == 'class_two') {
                                            $registrationOrder = $pairClass->registrationOrder;

                                            $orderItem = $this->orderItemRepo
                                                ->getByOrderable(
                                                    $registrationOrder->id,
                                                    Constant::REGISTRATION_ORDER,
                                                    false
                                                );

                                            $order = $orderItem->order;
                                        }

                                        $paramsAdjustmentHWMDOTDiscount = [
                                            'agent_id' => $agent->id,
                                            'order_id' => $order->id,
                                            'order_item_id' => $orderItem->id,
                                            'adjustable_id' => $classRegObj->id,
                                            'adjustable_type' => Constant::CLASS_REGISTRATION,
                                            'amount' => 50,
                                            'adjustment_action' => Constant::DEDUCTED,
                                            'note' => adjustment_registration_group_discount($classRegObj, $registrationOrder),
                                            'is_group_discount_adjustment' => true
                                        ];

                                        ModificationService::saveOrderAdjustment($paramsAdjustmentHWMDOTDiscount);
                                    }
                                }
                            }

                            if (!in_array($classRegistration->id, $deferredHalfPriceRegistrationIds) and
                                !in_array($classRegistration->id, $deferredGroupRegistrationIds)) {

                                $params = [
                                    'agent_id' => $agent->id,
                                    'order_id' => $order->id,
                                    'order_item_id' => $orderItem->id,
                                    'adjustable_id' => $classRegistration->id,
                                    'adjustable_type' => Constant::CLASS_REGISTRATION,
                                    'amount' => $deduction,
                                    'adjustment_action' => Constant::ADDED,
                                    'note' => adjustment_registration_half_price_discount($classRegistration, $registrationOrder),
                                    'is_half_price_adjustment' => true
                                ];

                                ModificationService::saveOrderAdjustment($params);
                            }
                        }

                        if ($ctr > 2) {
                            $halfPriceAdjusment = $this->orderAdjustmentRepo
                                                        ->findOne([
                                                            'adjustable_id' => $classRegistration->id,
                                                            'adjustable_type' => Constant::CLASS_REGISTRATION,
                                                            'is_half_price_adjustment' => true
                                                        ]);

                            if ($halfPriceAdjusment) {
                                $classRegistration->item_charge -= $halfPriceAdjusment->amount;
                                $classRegistration->paid_charge -= $halfPriceAdjusment->amount;
                                $classRegistration->is_half_priced = true;
                                $classRegistration->save();

                                if ($classRegistration->classHalfPriceDiscount) {

                                    $classRegistration->classHalfPriceDiscount->is_deleted = false;
                                    $classRegistration->classHalfPriceDiscount->save();
                                }

                                $halfPriceAdjusment->delete();
                            }

                            elseif (!$classRegistration->is_half_priced) {
                                $deduction = $classRegistration->item_charge / 2;

                                $classRegistration->item_charge = $deduction;
                                $classRegistration->paid_charge = $deduction;
                                $classRegistration->save();

//                                $params = [
//                                    'agent_id' => $agent->id,
//                                    'order_id' => $order->id,
//                                    'order_item_id' => $orderItem->id,
//                                    'adjustable_id' => $classRegistration->id,
//                                    'adjustable_type' => Constant::CLASS_REGISTRATION,
//                                    'amount' => $deduction,
//                                    'adjustment_action' => Constant::DEDUCTED,
//                                    'note' => adjustment_registration_half_price_discount($classRegistration, $registrationOrder),
//                                    'is_half_price_adjustment' => true
//                                ];
//
//                                ModificationService::saveOrderAdjustment($params);
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * @param  Collection $classRegistrations
     * @return void
     */
    public function removeDiscountsCancelledClassRegistrations(Collection $classRegistrations)
    {
        if ($classRegistrations->count()) {

            $classRegistrations->each(function($classRegistration) {
                self::archiveDiscounts($classRegistration);
            });
        }
    }

    /**
     * Remove discounts related to soft-deleted Class Registration
     *
     * @param ClassRegistration $classRegistration
     * @return $this
     * @throws \Exception
     */
    public function archiveDiscounts(ClassRegistration $classRegistration)
    {
        try {

            //Remove ClassHalfPriceDiscount Object
            $classHalfPriceDisc = $this->classHalfPriceDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classHalfPriceDisc) {
                $classHalfPriceDisc->is_deleted = true;
                $classHalfPriceDisc->save();
            }

            $classCombinationDiscount = $this->classCombinationDiscountRepo
                                             ->getByClassRegistrationId($classRegistration->id, true);

            if ($classCombinationDiscount) {

                $classOne = $classCombinationDiscount->classOne;
                $classTwo = $classCombinationDiscount->classTwo;

                if (!$classOne->is_cancelled or !$classTwo->is_cancelled) {
                    $classRegistration->paid_charge += 50;
                    $classRegistration->save();
                }

                if ($classOne->id == $classRegistration->id and
                    !$classTwo->is_cancelled) {

                    $classTwo->item_charge += 50;
                    $classTwo->paid_charge += 50;
                    $classTwo->save();
                }

                elseif ($classTwo->id == $classRegistration->id and
                        !$classOne->is_cancelled) {

                    $classOne->item_charge += 50;
                    $classOne->paid_charge += 50;
                    $classOne->save();
                }

                if ($classRegistration->is_new) {
                    $classCombinationDiscount->delete();
                } else {

                    $classCombinationDiscount->is_deleted = true;
                    $classCombinationDiscount->save();
                }
            }

            //Remove ClassCouponDiscounts Object
            $classCouponDiscounts = $this->classCouponDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCouponDiscounts->count()) {
                foreach ($classCouponDiscounts as $classCouponDiscount) {
                    $classCouponDiscount->is_deleted = true;
                    $classCouponDiscount->save();
                }
            }

            //Remove ClassMilitaryDiscounts Object
            $classMilitaryDiscount = $this->classMilitaryDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classMilitaryDiscount) {
                $classMilitaryDiscount->is_deleted = true;
                $classMilitaryDiscount->save();
            }

            $classCreditDiscount = $this->classCreditDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCreditDiscount) {
                $classCreditDiscount->is_deleted = true;
                $classCreditDiscount->save();
            }

            $orderAdjustments = $this->orderAdjustmentDiscountRepo
                ->getByAdjustable([
                    'adjustable_id' => $classRegistration->id,
                    'adjustable_type' => Constant::CLASS_REGISTRATION
                ]);

            if ($orderAdjustments->count()) {
                foreach ($orderAdjustments as $orderAdjustment) {
                    $orderAdjustment->is_deleted = true;
                    $orderAdjustment->save();
                }
            }

            return $this;
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * Restore discounts related to soft-deleted Class Registration
     *
     * @param ClassRegistration $classRegistration
     * @return $this
     * @throws \Exception
     */
    public function restoreDiscounts(ClassRegistration $classRegistration)
    {
        try {

            $classCombinationDiscount = $this->classCombinationDiscountRepo
                ->getByClassRegistrationId($classRegistration->id, true);

            if ($classCombinationDiscount) {
                $classOne = $classCombinationDiscount->classOne;
                $classTwo = $classCombinationDiscount->classTwo;

                if (!$classOne->is_cancelled and !$classTwo->is_cancelled) {
                    $classCombinationDiscount->is_deleted = false;
                    $classCombinationDiscount->save();

                    $classRegistration->item_charge = $classRegistration->paid_charge - 50;
                    $classRegistration->paid_charge -= 50;
                    $classRegistration->save();
                }

                else {
                    $classRegistration->item_charge = $classRegistration->paid_charge;
                    $classRegistration->save();
                }

                if ($classRegistration->id == $classOne->id and
                    !$classTwo->is_cancelled) {

                    $classTwo->item_charge -= 50;
                    $classTwo->paid_charge -= 50;
                    $classTwo->save();
                }

                if ($classRegistration->id == $classTwo->id and
                    !$classOne->is_cancelled) {

                    $classOne->item_charge -= 50;
                    $classOne->paid_charge -= 50;
                    $classOne->save();
                }
            } else {
                $classRegistration->item_charge = $classRegistration->paid_charge;
                $classRegistration->save();
            }

            //Remove ClassHalfPriceDiscount Object
            $classHalfPriceDisc = $this->classHalfPriceDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classHalfPriceDisc) {
                $classHalfPriceDisc->is_deleted = false;
                $classHalfPriceDisc->save();
            }

            //Remove ClassCouponDiscounts Object
            $classCouponDiscounts = $this->classCouponDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCouponDiscounts->count()) {
                foreach ($classCouponDiscounts as $classCouponDiscount) {
                    $classCouponDiscount->is_deleted = false;
                    $classCouponDiscount->save();
                }
            }

            //Remove ClassMilitaryDiscounts Object
            $classMilitaryDiscount = $this->classMilitaryDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classMilitaryDiscount) {
                $classMilitaryDiscount->is_deleted = false;
                $classMilitaryDiscount->save();
            }

            $classCreditDiscount = $this->classCreditDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCreditDiscount) {
                $classCreditDiscount->is_deleted = false;
                $classCreditDiscount->save();
            }

            $orderAdjustments = $this->orderAdjustmentDiscountRepo
                ->getByAdjustable([
                    'adjustable_id' => $classRegistration->id,
                    'adjustable_type' => Constant::CLASS_REGISTRATION
                ]);

            if ($orderAdjustments->count()) {
                foreach ($orderAdjustments as $orderAdjustment) {
                    $orderAdjustment->is_deleted = false;
                    $orderAdjustment->save();
                }
            }

            return $this;
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param Order $order
     * @param ClassRegistration $classRegistration
     * @param $deduction
     */
    public function createOrderAdjustmentForCredit(Order $order, ClassRegistration $classRegistration, $deduction)
    {
        $agent = $order->user;
        $registrationOrder = $classRegistration->registrationOrder;
        $orderItem = $this->orderItemRepo->getByOrderable($registrationOrder->id, Constant::REGISTRATION_ORDER, false);

        $params = [
            'agent_id' => $agent->id,
            'order_id' => $order->id,
            'order_item_id' => $orderItem->id,
            'adjustable_id' => $classRegistration->id,
            'adjustable_type' => Constant::CLASS_REGISTRATION,
            'amount' => $deduction,
            'adjustment_action' => Constant::DEDUCTED,
            'is_corporate_discount_adjustment' => true,
            'note' => adjustment_registration_credit_applied($classRegistration, $registrationOrder),
        ];

        ModificationService::saveOrderAdjustment($params);
    }
}