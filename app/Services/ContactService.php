<?php namespace App\Services;

use Auth;
use Event;
use App\Models\User;
use App\Models\AddressBook;
use App\Models\Contact;
use App\Repositories\AddressBookEntryRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ContactRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Services\Mailer\RegistrationMailer;
use App\Utilities\Constant;
use App\Events\AddressBookEntryEvent;
use App\Events\CustomerRegisteredEvent;
use App\Events\AssociateContactAccountEvent;
use App\Facades\CourseMillService;

class ContactService
{
    /**
     * @var RoleRepository
     */
    private $roleRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var RegistrationMailer
     */
    private $mailer;

    /**
     * @var AddressBookEntryRepository
     */
    private $entryRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param RoleRepository $roleRepo
     * @param UserRepository $userRepo
     * @param ContactRepository $contactRepo
     * @param RegistrationMailer $mailer
     * @param AddressBookEntryRepository $entryRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     */
    public function __construct(RoleRepository $roleRepo,
                                UserRepository $userRepo,
                                ContactRepository $contactRepo,
                                RegistrationMailer $mailer,
                                AddressBookEntryRepository $entryRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->roleRepo = $roleRepo;
        $this->userRepo = $userRepo;
        $this->contactRepo = $contactRepo;
        $this->mailer = $mailer;
        $this->entryRepo = $entryRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * Create new contact for user
     *
     * @param array $params
     * @return bool
     */
    public function saveContact($params = [])
    {
        $notify = (isset($params['doNotEmail'])) ? false : true;
        $addressBook = [];
        $newUserCreatedText = '';
        $contactId   = null;
        $email   = isset($params['email']) ? $params['email'] : null;
        $userId  = isset($params['user_id'])
                   ? $params['user_id']
                   : Auth::user()->id;
        $match_user_id = null;
        if (array_key_exists('match_user_id', $params) && strlen($params['match_user_id']) > 0) {
            $match_user_id = $params['match_user_id'];
        }

        if ($match_user_id) {
            $user    = $this->userRepo->getById($params['match_user_id']);
            $contact = $user->contact;

            $contactId   = $contact->id;
            $addressBook[$user->id] = $this->userRepo->getById($userId)->contact->id;

            if ($this->entryRepo->getByOwnerAndContact($userId, $contact->id)) {
                    return [
                        'response'  => false,
                        'message'   => 'Contact already added.'
                    ];
            }
        } else {
            $contact = isset($params['id'])
                   ? $this->contactRepo->getById($params['id'])
                   : new Contact();

            if (array_key_exists('course_mill_user_id', $params) and strlen(trim($params['course_mill_user_id'])) == 0) {
                $params['course_mill_user_id'] = null;
            }

            $contact->fill($params);
            $contact->save();
            $role = $this->roleRepo
                         ->findOne(['role' => Constant::ROLE_MEMBER]);

            $contactId = $contact->id;

            if ($email) {

                $user = $contact->user ?: new User();
                $user->contact_id = $contact->id;
                $user->email      = $email;
                $user->state = 1;
                $user->status = 'active';

                $password         = mt_rand(10000000, 99999999);//str_random(10);
                $user->password   = bcrypt($password);

                $newUserCreatedText = ' An email has been sent to that user with the password.';


                $user->save();
                $user->assignRole($role);

                if($notify) {
                    event(new AddressBookEntryEvent($userId, $params, $password));
                }

                if (!is_null($user->contact->course_mill_user_id)) {
                    CourseMillService::createUpdateStudentFromCustomer($user->contact);
                }

                $contactUser            = $this->userRepo->getById($userId);
                $addressBook[$user->id] = $contactUser->contact->id;
            }
        }

        $addressBook[$userId] = $contactId;

        if (!isset($params['id'])) {

            foreach ($addressBook as $key => $value) {

                $existsInAddressBook = $this->entryRepo->getByOwnerAndContact($key, $value);

                if (!$existsInAddressBook) {
                    AddressBook::create([
                        'user_id' => $key,
                        'contact_id' => $value
                    ]);
                }
            }
        }

        return [
            'contact'   => $contact,
            'response'  => true,
            'message'   => 'New contact successfully added to your contact list.' . $newUserCreatedText
        ];
    }

    /**
     * Merge contacts
     *
     * @param $masterId
     * @param $slaveIds
     * @return $this
     * @throws \Exception
     */
    public function merge($masterId, $slaveIds)
    {
        foreach ($slaveIds as $slaveId) {

            if ($masterId != $slaveId) {
                //$this->replaceContactsInAddressBook($masterId, $slaveId);

                $this->replaceContactsInDiscounts($masterId, $slaveId);
                $this->replaceContactsInClassRegistration($masterId, $slaveId);
                $this->replaceContactsInMilitaryDiscount($masterId, $slaveId);
                $this->replaceOtherTransaction($masterId, $slaveId);

                $contact = $this->contactRepo->getById($slaveId);
                $contactMaster = $this->contactRepo->getById($masterId);

                if (!is_null($contact->course_mill_user_id) and is_null($contactMaster->course_mill_user_id)) {
                    $contactMaster->course_mill_user_id = $contact->course_mill_user_id;
                    $contactMaster->save();
                }

                if ($contact->user) {
                    $contact->user->forceDelete();
                }

                $contact->delete();
            }

        }

        return $this;
    }

    /**
     * Replace contacts in address book
     *
     * @param $masterId
     * @param $slaveId
     */
    private function replaceContactsInAddressBook($masterId, $slaveId)
    {
        $addressBooks = $this->entryRepo->getByContactId($slaveId);

        if ($addressBooks->count()) {

            foreach ($addressBooks as $addressBook) {
                $addressBook->contact_id = $masterId;
                $addressBook->save();
            }

        }
    }

    /**
     * Replace contacts in discounts
     *
     * @param $masterId
     * @param $slaveId
     */
    private function replaceContactsInDiscounts($masterId, $slaveId)
    {
        $discounts = $this->classCombinationDiscountRepo->getByContactId($slaveId);

        if ($discounts->count()) {

            foreach ($discounts as $discount) {
                $discount->attendee_contact_id = $masterId;
                $discount->save();
            }

        }
    }

    /**
     * Replace contacts in class registrations
     *
     * @param $masterId
     * @param $slaveId
     */
    private function replaceContactsInClassRegistration($masterId, $slaveId)
    {
        $classRegistrations = $this->classRegistrationRepo->getByAttendee($slaveId);

        if ($classRegistrations->count()) {

            foreach ($classRegistrations as $classRegistration) {
                $classRegistration->attendee_contact_id = $masterId;
                $classRegistration->save();
            }

        }
    }

    /**
     * Replace contacts in military discounts
     *
     * @param $masterId
     * @param $slaveId
     */
    private function replaceContactsInMilitaryDiscount($masterId, $slaveId)
    {
        $slaveContact = $this->contactRepo->getById($slaveId);

        if ($slaveContact && $slaveContact->militaryDiscounts) {

            foreach ($slaveContact->militaryDiscounts as $militaryDiscount) {
                $militaryDiscount->contact_id = $masterId;
                $militaryDiscount->save();
            }
        }
    }

    /**
     * Replace user/contacts in other transactions
     *
     * @param $masterId
     * @param $slaveId
     */
    private function replaceOtherTransaction($masterId, $slaveId)
    {
        $masterContact = $this->contactRepo->getById($masterId);
        $slaveContact = $this->contactRepo->getById($slaveId);
        $masterUser = $masterContact->user;
        $slaveUser = $slaveContact->user;

        if ($masterUser && $slaveUser) {
            $userContacts = $slaveUser->addressBooks;
            $actingAgents = $slaveUser->actingAgents;
            $corpSeatCredits = $slaveUser->corporateSeatCredits;
            $clientYearlySubscriptions = $slaveUser->clientYearlySubscriptions;
            $onsiteTrainings = $slaveUser->onsiteTrainings;
            $orders = $slaveUser->orders;
            $registrationOrders = $slaveUser->registrationOrders;
            $yearlySubscriptions = $slaveUser->yearlySubscriptions;
            $hasSubscriptions = $slaveUser->hasSubscription;
            $productOrders = $slaveUser->productOrders;

            if ($userContacts->count()) {

                foreach ($userContacts as $userContact) {
                    if($userContact->contact)
                    $existInAddressBook = $this->entryRepo
                                               ->getByOwnerAndContact($masterUser->id, $userContact->contact->id);

                    if (!$existInAddressBook &&
                        $masterId != $userContact->contact->id) {
                        $userContact->user_id = $masterUser->id;
                        $userContact->save();
                    }
                }

            }

            if ($actingAgents->count()) {

                foreach ($actingAgents as $actingAgent) {
                    $actingAgent->acting_agent_id = $masterUser->id;
                    $actingAgent->save();
                }

            }

            if ($corpSeatCredits->count()) {

                foreach ($corpSeatCredits as $corpSeatCredit) {
                    $corpSeatCredit->originating_agent_user_id = $masterUser->id;
                    $corpSeatCredit->save();
                }

            }

            if ($clientYearlySubscriptions->count()) {

                foreach ($clientYearlySubscriptions as $clientYearlySubscription) {
                    $clientYearlySubscription->user_id = $masterUser->id;
                    $clientYearlySubscription->save();
                }

            }

            if ($onsiteTrainings->count()) {

                foreach ($onsiteTrainings as $onsiteTraining) {
                    $onsiteTraining->originating_agent_user_id = $masterUser->id;
                    $onsiteTraining->save();
                }

            }

            if ($orders->count()) {

                foreach ($orders as $order) {
                    $order->user_id = $masterUser->id;
                    $order->save();
                }

            }

            if ($productOrders->count()) {

                foreach ($productOrders as $productOrder) {
                    $productOrder->originating_agent_user_id = $masterUser->id;
                    $productOrder->save();
                }

            }

            if ($registrationOrders->count()) {

                foreach ($registrationOrders as $registrationOrder) {
                    $registrationOrder->originating_agent_user_id = $masterUser->id;
                    $registrationOrder->save();
                }

            }

            if ($hasSubscriptions) {
				
                    $hasSubscriptions->user_id = $masterUser->id;
                    $hasSubscriptions->save();

            }

            if ($yearlySubscriptions->count()) {

                foreach ($yearlySubscriptions as $yearlySubscription) {
                    $yearlySubscription->originating_agent_user_id = $masterUser->id;
                    $yearlySubscription->save();
                }
            }

            if ($slaveUser->corporate_seat_credits) {

                $masterUser->corporate_seat_credits = $slaveUser->corporate_seat_credits;
                $masterUser->save();

            }

            if ($slaveUser->has_military_discount) {

                $masterUser->has_military_discount = $slaveUser->has_military_discount;
            	$masterUser->save();

            }

        }
    }

    /**
     * @param User $user
     * @param Contact $contact
     * @return $this
     */
    public function deleteContact(User $user, Contact $contact)
    {
        $response = '';
        $addressBook = $this->entryRepo
                            ->getByOwnerAndContact($user->id, $contact->id);

        if ($addressBook) {
            $addressBook->delete();
            $response = 'deleted';
        }

        if ($contact->user) {
            $addressBookInverse = $this->entryRepo
                                       ->getByOwnerAndContact($contact->user->id, $user->contact->id);

            if ($addressBookInverse) {
                $addressBookInverse->delete();
            }
        }

        return $response;
    }
}