<?php namespace App\Services;

use App\Repositories\ClassRepository;
use App\Repositories\CouponRepository;
use PDF;

class ReportService
{

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @var CouponRepository
     */
    private $couponRepo;

    /**
     * @param ClassRepository $classRepo
     * @param CouponRepository $couponRepo
     */
    public function __construct(ClassRepository $classRepo,
                                CouponRepository $couponRepo)
    {
        $this->classRepo = $classRepo;
        $this->couponRepo = $couponRepo;
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function generateClassReport($params = [])
    {
        $classIds = explode(',', $params['class_ids']);

        $data['classes'] = $this->classRepo->getByIds($classIds);

        return PDF::loadView('admin.reports.export_class', $data)
            ->setPaper('a4')
            ->setOrientation('portrait');
    }

    public function generateCouponReport($params = [])
    {
        $couponIds = explode(',', $params['coupon_ids']);

        $data['coupons'] = $this->couponRepo->getByIds($couponIds);

        return PDF::loadView('admin.reports.export_coupon', $data)
            ->setPaper('a4')
            ->setOrientation('portrait');
    }
}