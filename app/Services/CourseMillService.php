<?php namespace App\Services;

use App\Models\Course;
use App\Models\Order;
use App\Models\State;
use App\Repositories\ApiModels\EnrollStudent;
use App\Repositories\ApiRequest\CreateUpdateStudents;
use App\Repositories\ApiRequest\EnrollStudents;
use App\Repositories\ApiRequest\GetStudents;
use App\Repositories\ApiModels\CmStudent;
use App\Repositories\Auth\BasicAuth;
use App\Repositories\OrderRepository;
use Illuminate\Support\Collection;
use App\Models\Contact;
use App\Models\RegistrationOrder;;
use App\Utilities\Constant;
use App\Events\CourseMillAccountCreatedEvent;
use Carbon\Carbon;

class CourseMillService
{

    /**
     * @var BasicAuth
     */
    private $basicAuth;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    public function __construct(OrderRepository $orderRepo)
    {
        $this->basicAuth = new BasicAuth();
        $this->collection = new Collection();
        $this->orderRepo = $orderRepo;
    }

    /**
     * Create a new student if doesn't exist
     *
     * @param Contact $contact
     * @return mixed
     * @throws \Exception
     */
    public function createUpdateStudentFromCustomer(Contact $contact)
    {
        try {
            $id = trim($contact->course_mill_user_id);

            if (!empty($id)) {
                $id = $contact->course_mill_user_id;
            }

            $requestCreateUpdate = new CreateUpdateStudents($this->basicAuth, $this->collection);
            $reqGetStudents = new GetStudents($this->basicAuth, $this->collection);
            $isExisting = !empty($id);

            $student = new CmStudent(env('CM_API_ORGID'), $contact->first_name, $contact->last_name);

            if ($isExisting) {
                $student->setFName($contact->first_name);
                $student->setLName($contact->last_name);
            }

            $student->setAddress($contact->address1);
            $student->setCity($contact->city);
            $student->setZip($contact->zip);
            $student->setPhone($contact->phone);
            $student->addSubOrg($contact->company);
            $state = State::where('state', $contact->state)->first();

            if ($state) {
                $student->setState($state->abbrev);
            }

            if ($contact->user) {
                $user = $contact->user;
                $student->setEmail($user->email);

                if ($contact->user->isActive()) {
                    $student->setIsActive(1);
                } else {
                    $student->setIsActive(0);
                }
            }

            if ($isExisting) {
                $requestCreateUpdate->update($id, $student);
            } else {
                $response = $requestCreateUpdate->create($student);

                if ($response and is_object($response) and $response->created) {

                    $id = $response->ids[0]->id;

                    $contact->course_mill_user_id = $id;
                    $contact->save();

                    return [
                        'isNewAccount' => true,
                        'name' => $student->getFName() . ' ' . $student->getLName(),
                        'student' => $student
                    ];
                }
            }

            return [
                'isNewAccount' => false,
                'student' => $student
            ];



        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create student when not existing in ERC CourseMill
     *
     * @param RegistrationOrder $registrationOrder
     * @throws \Exception
     */
    public function createStudentFromRegistration(RegistrationOrder $registrationOrder)
    {
        try {

            $notify_agents = session()->pull('notify_agents', false);

            if ($registrationOrder->courseTypeId() == Constant::COMPUTER_BASED) {
                $course = $registrationOrder->registrable;
                $accounts = collect();
                $attendees = collect();
                $classRegistrations = $registrationOrder->classRegistrations;
                $order = $this->orderRepo->getSingleByOrderable(
                    Constant::REGISTRATION_ORDER,
                    $registrationOrder->id
                );

                $user = Order::find($order->id)->user;

                $classRegistrations->each(function($classRegistration) use ($attendees) {
                    $attendees->push($classRegistration->contact);
                });

                if ($attendees->count()) {
                    $attendees->each(function($contact) use ($accounts, $course) {
                        $createUpdateStudent = self::createUpdateStudentFromCustomer($contact);

                        if ($createUpdateStudent['isNewAccount']) {
                            $accounts->push((object) [
                                'name' => $createUpdateStudent['name'],
                                'userId' => $createUpdateStudent['student']->getId(),
                                'password' => $createUpdateStudent['student']->getPassword()
                            ]);
                        }

                        $this->enrollStudentToCourse($createUpdateStudent['student']->getId(), $course);
                    });

                    if ($accounts->count() and $notify_agents) {
                        event(new CourseMillAccountCreatedEvent($user, $accounts));
                    }
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $studentId
     * @param Course $course
     */
    private function enrollStudentToCourse($studentId, Course $course)
    {
        $req = new EnrollStudents($this->basicAuth, $this->collection);
        $enrollStudent = new EnrollStudent();
        $enrollStudent->setId($studentId);
        $enrollStudent->setCourse($course->course_mill_course_id);
        $enrollStudent->setOrgId(env('CM_API_ORGID'));
        $enrollStudent->setCurriculum("");
        $enrollStudent->setStart(Carbon::parse('now')->toDateTimeString());

        $req->enroll($enrollStudent);
    }
}