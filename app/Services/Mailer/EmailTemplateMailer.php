<?php namespace App\Services\Mailer;

use App\Mail\Mailer;
use App\Mail\Message;
use App\Models\EmailTemplate;
use Log;
use File;
use App\Models\EmailLog;
use Session;
use Config;

class EmailTemplateMailer
{
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    public function send($slug, $params = [])
    {
        if (Session::has('TEST-TOKEN')) {
            $data = $params;
            EmailLog::create(compact('slug', 'data'))->save();

            Config::set('mail.host', '127.0.0.1');
            Config::set('mail.port', 1025);
        }

        $content = EmailTemplate::whereSlug($slug)->first()->content->toHtml($params['data']) ?: '';
        $hyphens = <<<HYPHEN

* {
-moz-hyphens:none;
-ms-hyphens:none;
-webkit-hyphens: none;
hyphens:none;
}

HYPHEN;
        $content = str_replace('</body>', "<style type=\"text/css\">{$hyphens}</style></body>", $content);


        $base = env('APP_URL');
        $content = str_replace("url('/", "url('{$base}/", $content);
        $content = str_replace("url(../", "url({$base}/", $content);
        $content = str_replace('img src="/', "img src=\"{$base}/", $content);
        $content = str_replace('img src="../', "img src=\"{$base}/", $content);
        $content = str_replace('../fileman', env('APP_URL').'/fileman', $content);
        $content = str_replace('../', env('APP_URL').'/', $content);

        $message = new Message();
        $message->setTo($params['to']);
        if (array_key_exists('cc', $params)) {
            if (!is_array($params['cc'])) {
                $params['cc'] = [$params['cc']];
            }
            foreach ($params['cc'] as $cc) {
                $message->addCc($cc);
            }
        }

        if (array_key_exists('bcc', $params)) {
            if (!is_array($params['bcc'])) {
                $params['bcc'] = [$params['bcc']];
            }
            foreach ($params['bcc'] as $bcc) {
                $message->addBcc($bcc);
            }
        }

        $message->setSubject(html_entity_decode($params['subject'],ENT_QUOTES));
        $message->setView('emails.raw');
        $message->setData(compact('content'));
        if(isset($params['attachment'])) {
            $message->setAttachment($params['attachment']);
        }
        if(isset($params['attachments'])) {
            foreach ($params['attachments'] as $attachment)
            $message->setAttachment($attachment);
        }

        return $this->mailer->send($message);
    }

    public function getRandomBanner() {
        $path = null;
        $matchedFiles = File::glob(base_path('public').'/fileman/Uploads/Email/Banners/*.*');
        \Log::info(['found', compact('matchedFiles')]);
        if (!empty($matchedFiles)) {
            $path = collect($matchedFiles)->random();
            $path = str_replace(base_path('public').'/', '', $path);
        }

        return $path;
    }

}