<?php namespace App\Services\Mailer;

use View;
use App\Mail\Mailer;
use App\Mail\Message;
use App\Models\EmailTemplate;
use Illuminate\View\Compilers\BladeCompiler;
use Doctrine\Common\Inflector\Inflector;
use Log;
use Pingpong\Shortcode\Shortcode;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class PlaceholderService
{
    protected $tokens = null;
    protected $nested = [];
    protected $handlers = [];

    public function __construct() {
        $this->shortcode = new Shortcode();
    }

    public function getTokens()
    {
        return $this->tokens;
    }

    public function parse($string, $data = [])
    {
        $slug = str_random(15);

        $path = storage_path('app/emails/simulations');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file     = "{$slug}.blade.php";
        $filename = $path."/{$file}";
        $output = $this->shortcodify($string)."\n";

        file_put_contents($filename, $output."\n");

        View::addNamespace('email-simulations', $path);
        $view = "email-simulations::{$slug}";

        $view = View::make($view);
        $view->with($data);
        $output = $view->render();

        unlink($filename);
        return $output;
    }

    private function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }


    public function getShortcodes(&$body) {
        /*
         * This will match [/user]
         */
        $edgecase_pattern = '/\[\/\s*([a-z_]+)\s*]/i';
        $edgecases = collect();
        preg_match_all($edgecase_pattern, $body, $edgecases);
        if (!empty($edgecases)) {
            $edgecases = collect(end($edgecases));
        }


        $shortcode_pattern = '/\[\s*([a-z_]+)\s*]/i';

        $_shortcodes = [];
        $shortcodes = collect();
        preg_match_all($shortcode_pattern, $body, $_shortcodes);
        if (!empty($_shortcodes)) {
            $shortcodes = collect(end($_shortcodes));
        }

        $singular = collect();
        $plural   = collect();


        $valid_plural_shortcodes = [];

        if (!$edgecases->isEmpty()) {
            $edgecases->each(function($edgecase) use (&$shortcodes, &$edgecases, &$body) {
                $_singular = Inflector::singularize($edgecase);
                if ($edgecase === $_singular) {
                    // edge case found. e.g. [/user]
                    // remove it's old shortcode from list of shortcodes
                    $shortcodes = $shortcodes->reject(function($_shortcode) use ($edgecase) {
                        return $_shortcode === $edgecase;
                    });
                    // add update to old shortcode
                    $shortcodes->push(Inflector::pluralize($edgecase));
                    $shortcode_pattern = '/\[(\/?)\s*('.$edgecase.')\s*]/im';
                    $replacement = '[${1}'.Inflector::pluralize($edgecase).']';

                    // Note: original string will be modified by this to fix edgecase
                    $body = preg_replace($shortcode_pattern, $replacement, $body);
                } else {
                    $edgecases = $edgecases->reject(function($_edgecase) use ($edgecase) {
                        return $edgecase === $edgecase;
                    });
                }
            });

        }

        // $shortcodes = ;
        $shortcodes->each(function($shortcode) use (&$singular, &$plural, &$edgecases) {
            $_singular = Inflector::singularize($shortcode);

            if ($shortcode !== $_singular) {
                $plural->push([
                    'plural'   => $shortcode,
                    'singular' => $_singular
                ]);
            } else {
                $singular->push($_singular);
            }
        });

        return [
            'plural'   => $plural,
            'singular' => $singular,
        ];
    }

    /**
     * Items need to be moved to a new line, so that bbcode
     * replacements will not delete anything else
     * but just wysiwyg tags
     *
     * @param  string $content
     * @return string
     */
    public function removeTagSpaces($content) {
        $pattern = '/>(\s+)</ms';
        $replace = ">\n<";
        return preg_replace($pattern, $replace, $content, -1);
    }

    private function replaceSingular($content, $reference_singular = null) {
        $shortcode_pattern = '/\[\s*([a-z_]+[0-9]*)\s*]/im';
        $replacement = '{!! $${1} !!}';
        if ($reference_singular) {
            $replacement = '{!! $'.$reference_singular.'->${1} !!}';
        }

        $content = preg_replace($shortcode_pattern, $replacement, $content);

        // if (str_contains($content, "[if")) {
        //     // change word from
        //     //      [if type=online]
        //     //  to
        //     //      [if class.type=online]
        //     $shortcode_pattern = '/\[if\s*([a-z_]+)\s*=(.+)]/im';
        //     $replacement = '[if '.$reference_singular.'.${1}=${2}]';
        //     $content = preg_replace($shortcode_pattern, $replacement, $content);
        //     dd($content);
        // }

        if (str_contains($content, "[if")) {
            $if_value = '/\[if\s*([a-z_]+)\s*=(.+)]/im';
            $if_solid = '/\[if\s*([a-z_]+)\s*]/im';

            if (!$reference_singular) {
                $replacement_value = "\n" . '@if ($${1}" == "${2}")' . "\n";
                $replacement_solid = "\n" . '@if ($${1} == true)' . "\n";
            } else {
                $replacement_value = "\n" . '@if ($' . $reference_singular . '->${1} == "${2}")' . "\n";
                $replacement_solid = "\n" . '@if (strlen($' . $reference_singular . '->${1}) > 0)' . "\n";
            }

            $patterns     = [$if_value, $if_solid];
            $replacements = [$replacement_value, $replacement_solid];

            $content = preg_replace($patterns, $replacements, $content);
        }


        return $content;
    }

    public function shortcodify($string, $singular_reference = null) {
        // $string = $this->removeTagSpaces($string);
        $string = str_replace('[else]', "\n".'@else'."\n", $string);
        $string = str_replace('[/if]', "\n".'@endif'."\n", $string);

        if (!$this->tokens && !$singular_reference) {
            $tokens = $this->getShortcodes($string);
            $this->tokens = [
                'plural'   => $tokens['plural'],
                'singular' => $tokens['singular'],
            ];
        }

        if (array_key_exists('plural', $this->tokens)) {
            $this->tokens['plural']->each(function($shortcode) use ($singular_reference) {
                if (in_array($shortcode['plural'], $this->handlers)) {
                    return;
                }
                $this->handlers[] = $shortcode['plural'];
                // if ($shortcode['plural'] != 'classes') {
                //     dd([$singular_reference, $shortcode['plural']]);
                // }
                // dd([
                //     $singular_reference,
                //     $shortcode['plural'],
                //     array_pluck($this->tokens['plural'], 'plural')
                // ]);

                $possible_nested = array_pluck($this->tokens['plural'], 'plural');

                $this->shortcode->register(
                    $shortcode['plural'],
                    function(ShortcodeInterface $s) use ($shortcode, $possible_nested)
                {
                    $this->tokens['plural'] = $this->tokens['plural']->reject(function($value) use ($shortcode){
                        return $shortcode['plural'] === $value['plural'];
                    });

                    $_pattern = '/\$('.implode('|', $possible_nested).')/';




                    $matches = [];
                    preg_match_all($_pattern, $s->getContent(), $matches);

                    $content = $s->getContent();
                    if (!empty($matches)) {
                        $nested_words = last($matches);
                        foreach ($nested_words as $nested_word) {
                            if (!in_array($nested_word, $this->nested)) {
                                $this->nested[] = $nested_word;

                                $parent_variable = '$'.$shortcode['singular']."->".$nested_word;
                                $content = str_replace('$'.$nested_word, $parent_variable, $content);

                            }
                        }
                    }

                    // if ($this->tokens['plural']->isEmpty()) {
                    //     unset($this->tokens['plural']);
                    // }

                    $return  = "\n";
                    $return .= "@if (!empty(\${$shortcode['plural']}))\n";
                    $return .= "@foreach (\${$shortcode['plural']} as \${$shortcode['singular']})\n";
                    $return .= $this->replaceSingular($content, $shortcode['singular']);
                    $return .= "@endforeach\n";
                    $return .= "@endif\n";
                    return $return;
                });
            });
        }

        $plural_rendering = $this->shortcode->parse($string);
        $including_singular = $this->replaceSingular($plural_rendering);
        $excess_markup_pattern = '/^(.+)(@foreach \(.+\)|@endforeach)(.*)/m';
        $replacement = '${2}';

        $final = preg_replace($excess_markup_pattern, $replacement, $including_singular);


        $final = $this->embed_image_tags($final);
        $final = $this->replace_imglink_tags($final);
        $final = $this->replace_whimglink_tags($final);
        $final = $this->replace_link_tags($final);

        return $final;
    }

    public function embed_image_tags($string) {
        $pattern = '/{{\s(.+_image)\s}}/';
        $replacement = '<img src="{{ PhotoUtility::embed($1) }}">';
        return preg_replace($pattern, $replacement, $string);
    }

    public function replace_imglink_tags($string) {
        $pattern = '/{{\s(.+_imglink)\s}}/';
        $replacement = '<img src="{{$1}}" height="100%" width="100%" />';
        return preg_replace($pattern, $replacement, $string);
    }

    public function replace_whimglink_tags($string) {
        $pattern = '/{{\s(.+_whimglink)\s}}/';
        $replacement = '<img src="{{$1}}" height="180" width="165" />';
        return preg_replace($pattern, $replacement, $string);
    }

    public function replace_link_tags($string) {
        $pattern = '/{{(\s[$](\w|\w+->+\w)+_link\s)}}/';
        $replacement = '<a href="{{$1}}">{{$1}}</a>';
        $r = preg_replace($pattern, $replacement, $string);
        return $r;
    }
}
