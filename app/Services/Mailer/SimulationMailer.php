<?php namespace App\Services\Mailer;

use View;
use App\Mail\Mailer;
use App\Mail\Message;
use App\Models\EmailTemplate;
use Illuminate\View\Compilers\BladeCompiler;

class SimulationMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * RegistrationMailer constructor
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send registration verification email
     *
     * @param array $params
     * @return Message
     * @throws \Exception
     * @throws \Swift_SwiftException
     */
    public function send($data)
    {
        $email_template = EmailTemplate::find($data['id']);
        $content        = $data['content'];
        $email          = $data['email'];

        $hyphens = <<<HYPHEN

* {
-moz-hyphens:none;
-ms-hyphens:none;
-webkit-hyphens: none;
hyphens:none;
}

HYPHEN;
        $content = str_replace('</body>', "<style type=\"text/css\">{$hyphens}</style></body>", $content);

        $base = url('/');
        $content = str_replace("url('/", "url('{$base}/", $content);
        $content = str_replace("url(../", "url({$base}/", $content);
        $content = str_replace(' src="/', " src=\"{$base}/", $content);
        $content = str_replace(' src="../', " src=\"{$base}/", $content);


        $slug = $email_template->slug;
        
        $path = storage_path('app/emails/simulations');
        
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file     = "{$slug}.blade.php";
        $filename = $path."/{$file}";
        

        file_put_contents($filename, $content);

        View::addNamespace('email-simulations', $path);
        $view = "email-simulations::{$slug}";

        // $view = View::make($view);
        // $view->with([
        //     'name' => 'something'
        //     ]);
        // echo $view->render();

        // echo $path;
        // exit;
        
        
        
        // $content = BladeCompiler::something($content, $data);

        $message = new Message();
        $message->setSubject($email_template->description);
        $message->setView($view);
        $message->setData([
            'name' => 'Jaggy'
        ]);
        $message->setTo($email);

        $this->mailer->send($message);

        return $message;
    }
}