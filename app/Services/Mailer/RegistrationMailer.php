<?php namespace App\Services\Mailer;

use App\Mail\Mailer;
use App\Mail\Message;
use App\Models\EmailTemplate;
use Log;
class RegistrationMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * RegistrationMailer constructor
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer, EmailTemplateMailer $emailer)
    {
        $this->mailer = $mailer;
        $this->emailer = $emailer;
    }

    /**
     * Send registration verification email
     *
     * @param array $params
     * @return Message
     * @throws \Exception
     * @throws \Swift_SwiftException
     */
    public function sendRegVerificationEmail($params = [])
    {
        return $this->emailer->send('ua-register', $params);
    }

    /**
     * Send instant email
     *
     * @param array $params
     * @return int
     */
    public function sendInstantEmail($params = [])
    {
        $_sent = 0;
        
        if (is_array($params['to'])) {
            $recipients = $params['to'];
            foreach ($recipients as $recipient) {
                $_params = array_except($params, ['to']);
                $_params['to'] = $recipient;
                $_sent += $this->emailer->send('reg-seminars-confirmation-user', $_params);
            }
        } else {
            $params['to'] = reset($params['to']);
            $_sent += $this->emailer->send('reg-seminars-confirmation-user', $params);
        }
        return $_sent;
        
        // $message = new Message();
        // $message->setSubject($params['subject']);
        // $message->setCc($params['cc']);
        // $message->setView($params['view']);
        // $message->setData($params['data']);

        // $sent = 0;

        // foreach ($params['to'] as $recipient)
        // {
        //     try {
        //         $sent += $this->mailer->send($message->setTo($recipient));
        //     }
        //     catch (\Swift_SwiftException $e) {}
        // }

        // return $sent;
    }

    /**
     * @param array $params
     * @throws \Exception
     * @throws \Swift_SwiftException
     */
    public function sendConfirmationToAgentEmail($params = [])
    {
        $message = new Message();
        $message->setSubject($params['subject']);
        if (array_key_exists('cc', $params)) {
            $message->setCc($params['cc']);
        }
        $message->setView($params['view']);
        $message->setData($params['data']);
        $message->setTo($params['to']);

        $this->mailer->send($message);
    }

    public function userStatusEmail($user, $status)
    {
        $params = [
            'subject' => 'Environmental Resource Center Account Update',
            'to' => $user->email,
            'data' => [
                'name' => $user->contact->name,
                'updates' => [
                    (object) [
                        'type'      => 'status',
                        'email'     => $user->email,
                        'status'    => $status
                    ],
                ],
                'link' => env('APP_URL').'/login#forgot_password',
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];
        return $this->emailer->send('ua-profile-update', $params);
    }

    public function requestUserActivation($admin, $user)
    {
        $params = [
            'subject' => 'User Account - re-activation request',
            'to' => $admin->email,
            'data' => [
                'name' => $admin->contact->name,
                'email' => $user->email,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
                ],
        ];
        return $this->emailer->send('ua-activation-request', $params);
    }
}