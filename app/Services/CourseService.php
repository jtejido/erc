<?php namespace App\Services;

use App\Facades\ClassService;
use App\Models\Course;
use App\Models\CourseCategory;
use App\Models\CourseClass;
use App\Models\CourseOrder;
use App\Repositories\CourseRepository;
use App\Repositories\ClassRepository;
use App\Utilities\Constant;
use Carbon\Carbon;

class CourseService {

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @param CourseRepository $courseRepo
     */
    public function __construct(CourseRepository $courseRepo,
                                ClassRepository $classRepo)
    {
        $this->courseRepo = $courseRepo;
        $this->classRepo = $classRepo;
    }

    /**
     * Save course
     *
     * @param array $params
     * @return Course|\Illuminate\Database\Eloquent\Model
     */
    public function save($params = [])
    {
        $course = isset($params['id']) ? $this->courseRepo->getById($params['id'])
                  : new Course();

        if ($params['course_type_id'] != Constant::COMPUTER_BASED or empty($params['course_mill_course_id'])) {
            $params['course_mill_course_id'] = null;
        }

        if (isset($params['course_order'])) {
            foreach ($params['course_order'] as $courseOrderId => $order) {
                $courseOrder = CourseOrder::find($courseOrderId);

                if ($courseOrder) {
                    $subCourseOrder = CourseOrder::where([
                        'category_id' => $courseOrder->category_id,
                        'course_type_id' => $courseOrder->course_type_id,
                        'order' => $order
                    ])->first();

                    if ($subCourseOrder) {
                        $subCourseOrder->order = $courseOrder->order;
                        $subCourseOrder->save();
                    }

                    $courseOrder->order = $order;
                    $courseOrder->save();
                }
            }
        }

        $course->fill($params);
        $course->save();

        CourseCategory::destroy($course->courseCategories->pluck('id')->toArray());

        if(isset($params['categories'])) {
            foreach ($params['categories'] as $category) {
                CourseCategory::create([
                    'course_id' => $course->id,
                    'category_id' => $category
                ]);

                $paramsCourseOrder = [
                    'course_id' => $course->id,
                    'category_id' => $category,
                    'course_type_id' => $course->course_type_id
                ];

                $existCourseOrder = CourseOrder::where($paramsCourseOrder)->first();

                if (is_null($existCourseOrder)) {

                    $courses = $this->courseRepo->getByCategoryAndType($category, $course->course_type_id);

                    $paramsCourseOrder['order'] = $courses->count();

                    CourseOrder::create($paramsCourseOrder);
                }
            }
        }


        return $course;
    }

    /**
     * Save class
     *
     * @param array $params
     * @return CourseClass
     */
    public function saveClass($params = [])
    {
        $class = isset($params['id']) ? $this->classRepo->getById($params['id'])
                 : new CourseClass();

        $params['start_date'] = Carbon::parse($params['start_date'])->format('Y-m-d');
        $params['end_date']   = Carbon::parse($params['end_date'])->format('Y-m-d');
        $params['start_time'] = date('H:i:s', strtotime($params['start_time']));
        $params['end_time'] = date('H:i:s', strtotime($params['end_time']));

        if (!strlen(trim($params['instructor_id']))) {
            $params['instructor_id'] = NULL;
        }

        if (!array_key_exists('location_id', $params)) {
            $params['location_id'] = NULL;
        } elseif (!strlen(trim($params['location_id']))) {
            $params['location_id'] = NULL;
        }

        $class->fill($params);
        $class->save();

        if ($class->course->isSeminar()) {
            $rel_classes = $this->classRepo->getRelatedClasses($class);

            if ($rel_classes) {
                foreach ($rel_classes as $rel_class) {
                    $this->classRepo->linkClass($class, $rel_class, true);
                }
            }
        }

        return $class;
    }

    public function isCourseDeletable($id)
    {
        $course = Course::findOrFail($id);
        foreach ($course->classes as $class) {
            if(!ClassService::isClassDeletable($class->id)) {
                return false;
            }
        }
        return ($course->registrations->count()) ? false : true;
    }

}