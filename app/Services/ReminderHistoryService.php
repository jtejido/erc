<?php namespace App\Services;


use App\Models\ReminderHistory;
use App\Services\Mailer\EmailTemplateMailer;

class ReminderHistoryService
{

    public function __construct(EmailTemplateMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function log($params = [])
    {
        return ReminderHistory::create($params);
    }

    public function send(ReminderHistory $reminder)
    {
        $data = unserialize($reminder->data);
        $data['cbtsPage'] = route('courses') . '?type=3';
        $data['coursesPage'] = route('courses');
        $data['homePage'] = route('home');

        return $this->mailer->send($reminder->slug, $data);
    }

}