<?php namespace App\Services;


use App\Models\Contact;
use App\Models\RegistrationOrder;
use App\Repositories\ClassRegistrationRepository;
use Carbon\Carbon;

class RedirectService
{
    /**
     * @var ClassRegistrationRepository
     */
    private $classRegRepo;


    /**
     * RedirectService constructor.
     * @param ClassRegistrationRepository $classRegRepo
     */
    public function __construct(ClassRegistrationRepository $classRegRepo)
    {
        $this->classRegRepo = $classRegRepo;
    }

    public function getWebinarData($slug)
    {
        $regOrder = RegistrationOrder::whereSlug($slug)->firstOrFail();
        $webinar = $regOrder->registrable;
        $data['page']           = 'redirect';
        $data['page_title']     = 'Join Webcast';
        $data['regOrderId']     = $regOrder->id;
        $data['webinar']        = $webinar;
        $data['meeting_id']     = $webinar->course->meeting_id;
        $data['meeting_link']   = $webinar->course->meetingUrl;
        $data['students']       = $this->classRegRepo->getStudentsOfRegistrationOrder($regOrder);
        return $data;
    }

    public function getExamData($slug)
    {

        $regOrder = RegistrationOrder::whereSlug($slug)->firstOrFail();
        $webinar = $regOrder->registrable;

        $data['page']           = 'redirect';
        $data['page_title']     = 'Take Online Test';
        $data['regOrderId']     = $regOrder->id;
        $data['webinar']        = $webinar;
        $data['meeting_id']     = $webinar->course->meeting_id;
        $data['meeting_link']   = $webinar->course->meetingUrl;
        $data['students']       = $this->classRegRepo->getStudentsOfRegistrationOrder($regOrder);
        return $data;
    }

    public function updateMeeting($request)
    {
        $students = $request->input('students');
        $regOrderId = $request->input('regOrderId');
        foreach($students as $id) {
            $classReg = $this->classRegRepo->getByRegistrationAndAttendee($regOrderId, $id);
            $classReg->attendMeeting();
        }
    }

    public function updateExam($request)
    {
        $this->data = [];
        $regOrder = RegistrationOrder::findOrFail($request->input('regOrderId'));
        $student = Contact::findOrFail($request->input('student'));
        $classReg = $this->classRegRepo->getByRegistrationAndAttendee($regOrder->id, $student->id);
        $this->data['flag'] = $classReg->meeting_flag;
        $course = $regOrder->course();
        $url = env('CM_LINK')."?quiz=%s&cm_fn=%s&cm_ln=%s&cm_e=%s&cm_user_id=%s";
        $email = ($student->user) ? $student->user->email : '' ;
        $sid = $student->id . '-' . $regOrder->id;
        $this->data['url'] = sprintf($url, $course->test_link, $student->first_name, $student->last_name, $email, $sid);
        return $this->data;
    }

    public function hook($request)
    {

        $header_hmac_signature = $request->server('HTTP_X_CLASSMARKER_HMAC_SHA256');
        $json_string_payload = $request->getContent();

        $verified = $this->verify_classmarker_webhook($json_string_payload, $header_hmac_signature);

        $array_payload = json_decode($json_string_payload);

        if ($verified)
        {
            try {
                $sid = explode('-', $array_payload->result->cm_user_id);
                if ($sid) {
                    $classReg = $this->classRegRepo->getByRegistrationAndAttendee($sid[1], $sid[0]);
                    if ($classReg) $classReg->takeExam();
                }
            } catch (\Exception $e) {

            }
        }
    }

    public function verify_classmarker_webhook($json_data, $header_hmac_signature)
    {

        $calculated_signature = base64_encode(hash_hmac('sha256', $json_data, env('CLASSMARKER_WEBHOOK_SECRET'), true));
        return ($header_hmac_signature == $calculated_signature);

    }

}