<?php namespace App\Services;

use App\Events\StudentMarkedConfirmedEvent;
use App\Models\ClassRegistration;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClassRepository;
use App\Repositories\ContactRepository;
use App\Repositories\CourseRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Utilities\Constant;
use iio\libmergepdf\Merger;
use PDF;

class CertificateService
{
    /**
     * @var ClassRepository
     */
    private $classRepository;
    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepository;
    /**
     * @var ContactRepository
     */
    private $contactRepository;
    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepository;
    /**
     * @var OrderItemRepository
     */
    private $orderItemRepository;
    /**
     * @var CourseRepository
     */
    private $courseRepository;


    /**
     * CertificateService constructor.
     * @param ClassRepository $classRepository
     * @param ClassRegistrationRepository $classRegistrationRepository
     * @param ContactRepository $contactRepository
     * @param RegistrationOrderRepository $registrationOrderRepository
     * @param OrderItemRepository $orderItemRepository
     * @param CourseRepository $courseRepository
     */
    public function __construct(ClassRepository $classRepository,
                                ClassRegistrationRepository $classRegistrationRepository,
                                ContactRepository $contactRepository,
                                RegistrationOrderRepository $registrationOrderRepository,
                                OrderItemRepository $orderItemRepository,
                                CourseRepository $courseRepository)
    {
        $this->classRepository = $classRepository;
        $this->classRegistrationRepository = $classRegistrationRepository;
        $this->data = [];
        $this->contactRepository = $contactRepository;
        $this->registrationOrderRepository = $registrationOrderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->courseRepository = $courseRepository;
    }

    /**
     * Generate Name Plate
     * @param $class_id
     * @param int $course
     * @return mixed
     */
    public function generateNamePlate($class_id, $course = 0)
    {
        $this->getClassInfo($class_id, $course);
        return PDF::loadView('main.certificate.template-d', $this->data)
            ->setPaper('a4')
            ->setOrientation('landscape');
    }

    /**
     * Generate Sign Sheet
     * @param $class_id
     * @param int $course
     * @return mixed
     */
    public function generateSignSheet($class_id, $course = 0)
    {
        $this->getClassInfo($class_id, $course);
        return PDF::loadView('main.certificate.template-c', $this->data)
            ->setPaper('a4')
            ->setOrientation('portrait');
    }

    /**
     * Generate Class or Course Roster
     * @param $class_id
     * @param int $course
     * @return mixed
     */
    public function generateRoster($class_id, $course = 0)
    {
        $template = 'main.certificate.template-b';
        $this->getClassInfo($class_id, $course);
        return PDF::loadView($template, $this->data)
            ->setPaper('a4')
            ->setOrientation('landscape');
    }

    /**
     * @param $class_id
     * @param $course
     */
    public function getClassInfo($class_id, $course)
    {
        $class = ($course)
            ? $this->courseRepository->getById($class_id)
            : $this->classRepository->getById($class_id);
        $this->data['students'] = $this->classRegistrationRepository->getStudentsOfClass($class, false);
        $this->data['course'] = $class->title;
        $this->data['instructor'] = $class->instructorName() . ', Instructor';
        $this->data['address'] = $class->shortLocation;
        $this->data['date'] = ($course) ? '' : $class->start->toDateString();
    }

    public function mergeCertificates($students, $attendedOnly = false)
    {
        $m = new Merger();
        $files = [];
        foreach ($students as $student) {
            if ($attendedOnly && $student->confirmed != 1) continue;
            $reg_order = $this->classRegistrationRepository
                ->getByRegistrationAndAttendee($student->regOrderId, $student->id);

            if (!$reg_order->certificate_file_path) {
                $pdf = $this->generateCertificate($student->id, $student->regOrderId);
                $path = $this->classRegistrationRepository->saveCertificate($student->id, $student->regOrderId, $pdf);
            } else {
                $path = $reg_order->certificate_file_path;
            }
            $files[] = storage_path('app/' . $path);
        }
        if (!$files) {
            abort(404);
        }

        $m->addIterator($files);
        return $m->merge();
    }

    public function generateCertificate($user_id, $regOrderId)
    {
        $user = $this->contactRepository
            ->getById($user_id);
        $reg_order = $this->registrationOrderRepository
            ->getById($regOrderId);
        $orderItem = $this->orderItemRepository
            ->getByOrderable($regOrderId, Constant::REGISTRATION_ORDER, false);
        $class = $reg_order->registrable;
        $course = $reg_order->isCourse()
                  ? $class
                  : $class->course;


        $params = [
            'name'              => $user->name,
            'title'             => $user->title,
            'course'            => $reg_order->title(),
            'instructor'        => ($reg_order->isCourse()) ? '' : $class->instructorName(),
            'sig_is_image'      => ($reg_order->isCourse())
                                    ? false
                                    : (($class->instructor && $class->instructor->signature)
                                        ? true
                                        : false),
            'instructor_sig'    => ($reg_order->isCourse())
                                   ? ''
                                   : (($class->instructor && $class->instructor->signature)
                                      ? $class->instructor->signature
                                      : $class->instructorName()),
            'date'              => ($reg_order->isCourse()) ? '' : $class->end_date->format('F d, Y'),
            'cert_num'          => $orderItem->getTransactionIdAttribute() . $user->id,
            'citation'          => ($reg_order->isCourse()) ? $class->citation : $class->course->citation,
            'in_transportation' => $course->isInTransportationCategory()
        ];

        return PDF::loadView('main.certificate.template-a', $params)
            ->setPaper('a4')
            ->setOrientation('landscape');

    }

    /**
     * @param $class_id
     * @return string
     */
    public function generateCertificates($class_id)
    {
        $class = $this->classRepository->getById($class_id);
        $students = $this->classRegistrationRepository->getStudentsOfClass($class, false);
        return $this->mergeCertificates($students);
    }

    /**
     * Save PDF to DB and Storage
     * @param $userId
     * @param $regOrderId
     * @return string
     */
    public function saveCertToDBandStorage($userId, $regOrderId)
    {
        // add checker if pwede na mag generate ng certificate
        $pdf = $this->generateCertificate($userId, $regOrderId);
        return $this->classRegistrationRepository->saveCertificate($userId, $regOrderId, $pdf);
    }

    public function updateStatus($request)
    {
        $userId = $request->input('user_id');
        $regOrderId = $request->input('class_id');
        $status = $request->input('status');
        $key = $request->input('key');
        $class_reg = $this->classRegistrationRepository->getByRegistrationAndAttendee($regOrderId, $userId);
        switch ($key) {
            case 'attended':
                $class_reg->attendSeminar($status);
                break;
            case 'exam_flag':
                $class_reg->takeExam($status);
                break;
            case 'meeting_flag':
                $class_reg->attendMeeting($status);
                break;
            case 'certificate_sent':
                event(new StudentMarkedConfirmedEvent($userId, $regOrderId, 1));
                break;
        }
    }

    public function updateStatusAll($request)
    {
        $courseId = $request->input('course_id');
        $classId = $request->input('class_id');
        $flag = $request->input('flag');
        $key = $request->input('key');
        if($classId)
            $this->classRegistrationRepository->updateStudentStatusOfClass($classId, $flag, $key);
        else
            $this->classRegistrationRepository->updateStudentStatusOfCourse($courseId, $flag, $key);
    }


}