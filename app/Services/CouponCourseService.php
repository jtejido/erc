<?php namespace App\Services;

use App\Models\Coupon;
use App\Models\CouponCourse;
use App\Repositories\CouponCourseRepository;
use App\Repositories\CouponRepository;

class CouponCourseService
{

    /**
     * @var CouponRepository
     */
    private $couponRepo;

    /**
     * @var CouponCourseRepository
     */
    private $couponCourseRepo;

    /**
     * @param CouponRepository $couponRepo
     * @param CouponCourseRepository $couponCourseRepo
     */
    public function __construct(CouponRepository $couponRepo,
                                CouponCourseRepository $couponCourseRepo)
    {
        $this->couponRepo = $couponRepo;
        $this->couponCourseRepo = $couponCourseRepo;
    }

    /**
     * Create new coupon-courses association
     *
     * @param array $params
     * @return $this
     */
    public function save($params = [])
    {
        $coupon = isset($params['id'])
                  ? $this->couponRepo->getById($params['id'])
                  : new Coupon();

        $couponCourseIds = [];

        if (isset($params['id'])) {
            $couponCourses = $this->couponCourseRepo->getByCouponId($params['id']);

            foreach ($couponCourses as $couponCourse) {
                array_push($couponCourseIds, $couponCourse->id);
            }
        }

        $coupon->fill($params);
        $coupon->save();

        foreach ($params['discountable_ids'] as $discountable_id) {
            CouponCourse::create([
                'coupon_id'         => $coupon->id,
                'discountable_id'   => $discountable_id,
                'discountable_type' => $params['discountable_type']
            ]);
        }

        CouponCourse::destroy($couponCourseIds);

        return $coupon;
    }
}