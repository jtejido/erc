<?php namespace App\Services;


use App\Repositories\RegistrationOrderRepository;
use Auth;
use Event;
use App\Models\User;
use App\Models\Contact;
use App\Models\Order;
use App\Models\ActingOriginatingAgent;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use App\Repositories\ContactRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\OrderItemRepository;
use App\Utilities\Constant;
use App\Services\Mailer\RegistrationMailer;
use App\Facades\OrderUtility;
use Mockery\CountValidator\Exception;

class UserService
{

    /**
     * @var RoleRepository
     */
    private $roleRepo;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var RegistrationMailer
     */
    private $mailer;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    private $registrationOrderRepo;


    /**
     * @param RoleRepository $roleRepo
     * @param UserRepository $userRepo
     * @param ContactRepository $contactRepo
     * @param RegistrationMailer $mailer
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(RoleRepository $roleRepo,
                                UserRepository $userRepo,
                                ContactRepository $contactRepo,
                                RegistrationMailer $mailer,
                                ClassRegistrationRepository $classRegistrationRepo,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepository)
    {
        $this->roleRepo = $roleRepo;
        $this->userRepo = $userRepo;
        $this->contactRepo = $contactRepo;
        $this->mailer = $mailer;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepository;
    }

    /**
     * Create new user
     *
     * @param array $params
     * @return bool
     */
    public function save($params = [])
    {
        $notify = (array_key_exists('doNotEmail', $params)) ? false : true;
        $contact  = null;
        $email    = isset($params['email']) ? $params['email'] : null;
        $password_orig = isset($params['password']) ? $params['password'] : mt_rand(10000000, 99999999);
                                                                            //substr(uniqid(),0,8);
        $role     = isset($params['role'])
                    ? $this->roleRepo->getById($params['role'])
                    : null;

        $password = bcrypt($password_orig);

        $contact = Contact::create([
            'course_mill_user_id' => (isset($params['course_mill_user_id']) and strlen(trim($params['course_mill_user_id']))) ? $params['course_mill_user_id'] : null,
            'first_name'    => isset($params['first_name']) ? $params['first_name'] : '',
            'last_name'     => isset($params['last_name']) ? $params['last_name'] : '',
            'company'       => isset($params['company']) ? $params['company'] : '',
            'title'         => isset($params['title']) ? $params['title'] : '',
            'address1'      => isset($params['address1']) ? $params['address1'] : '',
            'address2'      => isset($params['address2']) ? $params['address2'] : '',
            'city'          => isset($params['city']) ? $params['city'] : '',
            'state'         => isset($params['state']) ? $params['state'] : '',
            'zip'           => isset($params['zip']) ? $params['zip'] : '',
            'phone'         => isset($params['phone']) ? $params['phone'] : '',
            'fax'           => isset($params['fax']) ? $params['fax'] : ''
        ]);

        $user = User::create([
            'contact_id'   => $contact ? $contact->id : null,
            'email'       => $email,
            'password'    => $password,
            'state'       => true,
            'status'        => 'active'
        ]);

        $user->assignRole($role);

        if($notify) {
            $this->mailer->sendRegVerificationEmail([
                'subject' => 'Welcome to Environmental Resource Center',
                'to' => $email,
                'data' => [
                    'users' => [
                        (object) [
                            'name' => $contact->name,
                            'email' => $user->email,
                            'password' => $password_orig
                        ]
                    ],

                    'cbtsPage' => route('courses') . '?type=3' ,
                    'coursesPage' => route('courses'),
                    'homePage' => route('home'),
                ]
            ]);
        }


        return $user;
    }

    /**
     * Save user as acting agent when associated with email
     *
     * @param User $user
     * @return $this
     * @throws \Exception
     */
    public function saveAsAgent(User $user)
    {
        try {

            $contact = $user->contact;

            $classRegistrations = $this->classRegistrationRepo->getByAttendee($contact->id);

            if ($classRegistrations->count()) {
                foreach ($classRegistrations as $classRegistration) {
                    $registrationOrder = $classRegistration->registrationOrder;
                    $params = [
                        'orderable_id' => $registrationOrder->id,
                        'orderable_type' => class_basename($registrationOrder),
                        'acting_agent_id' => $user->id,
                        'status' => $registrationOrder->status
                    ];

                    $alreadyAnAgent = $this->actingOriginatingAgentRepo
                                           ->getByOrderableAndAgent($params);

                    if (!$alreadyAnAgent) {
                        ActingOriginatingAgent::create($params);
                    }
                }
            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Save corporate seat credit
     *
     * @param Order $order
     * @return $this
     * @throws \Exception
     */
    public function saveCorporateSeatCreditToUser(Order $order)
    {
        try {

            if (OrderUtility::isCompleteOrInvoiced($order)) {
                $orderItem = $this->orderItemRepo->getByOrderIdAndOrderableType(
                    $order->id, Constant::CORPORATE_SEAT
                );

                if ($orderItem) {
                    $user = $order->user;
                    $credits = $orderItem->orderable->credits;

                    $user->corporate_seat_credits += $credits;
                    $user->save();
                }

                return $this;
            }

            throw new Exception('Order is not Complete.');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Update a users corp seat credits
     * Assumes user has credits
     * Assumes company wide credits is >= creditUsed
     *
     * @param User $user
     * @param $creditsUsed
     * @return User
     */
    public function updateCredits(User $user, $creditsUsed)
    {

        if($user->corporate_seat_credits >= $creditsUsed) {
            $remainingCredits = $user->corporate_seat_credits - $creditsUsed;
            $creditsUsed = 0;
        } else {
            $remainingCredits = 0;
            $creditsUsed = $creditsUsed - $user->corporate_seat_credits;
        }

        $user->update(['corporate_seat_credits' => $remainingCredits]);

        if($creditsUsed <= 0) {
            return $user;
        } else {
            return $this->updateCredits($user->getCorporateSeatsCreditUser(), $creditsUsed);
        }
    }

    /**
     * Activate user service
     * @param  $userId
     * @throws \Exception
     */
    public function activateUserById($userId) {
        try {
            $user = $this->userRepo->getById($userId);
            $user->status = 'active';
            $user->save();
            $this->mailer->userStatusEmail($user, 'Activated');
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Deactivate user service
     * @param  $userId
     * @return bool
     * @throws \Exception
     */
    public function deactivateUserById($userId) {
        try {
            $user = $this->userRepo->getById($userId);
//            if (!$this->validForDeactivation($user->contact->id)) {
//                return false;
//            }
            $user->status = null;
            $user->save();
            $this->mailer->userStatusEmail($user, 'Deactivated');

            if ($user->id == auth()->user()->id) {
                auth()->logout();
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Request to activate account
     * @param  User   $user
     * @throws \Exception
     */
    public function requestToActivate(User $user)
    {
        try {
            $admin = $this->userRepo->getById(1);
            $this->mailer->requestUserActivation($admin, $user);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function validForDeactivation($contact_id)
    {
        $valid = true;

        $classRegs = $this->classRegistrationRepo->getByAttendee($contact_id);

        foreach ($classRegs as $classReg) {
            $regOrder = $classReg->registrationOrder;
            if(($regOrder->status == Constant::INVOICED || $regOrder->status == Constant::COMPLETE)
                && !$regOrder->isCourse()) {
                if($regOrder->registrable->active) {
                    $valid = false;
                    break;
                }
            }
        }

        return $valid;
    }

    public function getActiveClasses($user_id)
    {
        $classes = [];

        $classRegs = $this->classRegistrationRepo->getByAttendee($user_id);

        foreach ($classRegs as $classReg) {
            $regOrder = $classReg->registrationOrder;
            if(($regOrder->status == Constant::INVOICED || $regOrder->status == Constant::COMPLETE)
                && !$regOrder->isCourse()) {
                if ($regOrder->registrable->active) {
                    $classes[] = $regOrder->registrable;
                }
            }
        }

        return $classes;
    }
}