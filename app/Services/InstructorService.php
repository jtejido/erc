<?php namespace App\Services;

use Image;
use Storage;
use App\Models\Instructor;
use App\Repositories\InstructorRepository;

class InstructorService
{

    /**
     * @var InstructorRepository
     */
    private $instructorRepo;

    /**
     * @param InstructorRepository $instructorRepo
     */
    public function __construct(InstructorRepository $instructorRepo)
    {
        $this->instructorRepo = $instructorRepo;
    }

    /**
     * Save instructor details
     *
     * @param array $params
     * @return Instructor|\Illuminate\Database\Eloquent\Model
     */
    public function save($params = [])
    {
        $instructor = new Instructor();

        if (isset($params['id'])) {
            $instructor = $this->instructorRepo->getById($params['id']);

            if (isset($params['signature'])) {

                if (Storage::disk('local')->exists('signatures/'.$params['signature'])) {
                    Storage::disk('local')->delete('signatures/'.$params['signature']);
                }
            }
        }

        if (isset($params['signature'])) {
            $firstWordFName = explode(' ', trim($params['first_name']));
            $firstWordLName = explode(' ', trim($params['last_name']));

            $filename = strtolower(substr($firstWordFName[0], 0, 1) . $firstWordLName[0]) . '.bin';

            $resizeImage = Image::make($params['signature'])->resize(300, 150);
            $resizeImage->resizeCanvas(300, 150);
            $contents = $resizeImage->stream()->__toString();

            if (!Storage::disk('local')->exists('signatures')) {
                Storage::disk('local')->makeDirectory('signatures', 0777, true);
            }

            Storage::disk('local')->put('signatures/'.$filename, $contents);
            $params['signature_mime'] = 'image/png';
            $params['signature'] = $filename;
        }

        $instructor->fill($params);
        $instructor->save();

        return $instructor;
    }
}