<?php namespace App\Services;

use App\Facades\CourseUtility;
use App\Models\User;
use Auth;
use Session;
use Carbon\Carbon;
use App\Models\ClassRegistration;
use App\Models\ClientYearlySubscription;
use App\Models\YearlySubscription;
use App\Models\RegistrationOrder;
use App\Models\ActingOriginatingAgent;
use App\Models\OnsiteTraining;
use App\Models\ClientCorporateSeatCredit;
use App\Repositories\UserRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClientYearlySubscriptionRepository;
use App\Repositories\YearlySubscriptionRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\YearlySubscriptionTypesRepository;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\ContactRepository;
use App\Repositories\CorporateSeatRepository;
use App\Repositories\ClientCorporateSeatCreditRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Facades\DiscountService as DiscService;
use App\Facades\OrderService as OrdService;
use App\Utilities\Constant;

class RegistrationService
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var YearlySubscriptionRepository
     */
    private $subscriptionRepo;

    /**
     * @var ClientYearlySubscriptionRepository
     */
    private $clientYearlySubscriptionRepo;

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var YearlySubscriptionTypesRepository
     */
    private $subscriptionTypesRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingAgentRepo;

    /**
     * @var CorporateSeatRepository
     */
    private $corporateSeatRepo;

    /**
     * @var ClientCorporateSeatCreditRepository
     */
    private $clientCorpSeatCreditRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;


    /**
     * RegistrationService constructor
     *
     * @param UserRepository $userRepo
     * @param YearlySubscriptionRepository $subscriptionRepo
     * @param ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ContactRepository $contactRepo
     * @param YearlySubscriptionTypesRepository $subscriptionTypesRepo
     * @param ActingOriginatingAgentRepository $actingAgentRepo
     * @param CorporateSeatRepository $corporateSeatRepo
     * @param ClientCorporateSeatCreditRepository $clientCorpSeatCreditRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param OrderRepository $orderRepository
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(UserRepository $userRepo,
                                YearlySubscriptionRepository $subscriptionRepo,
                                ClientYearlySubscriptionRepository $clientYearlySubscriptionRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ContactRepository $contactRepo,
                                YearlySubscriptionTypesRepository $subscriptionTypesRepo,
                                ActingOriginatingAgentRepository $actingAgentRepo,
                                CorporateSeatRepository $corporateSeatRepo,
                                ClientCorporateSeatCreditRepository $clientCorpSeatCreditRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                OrderRepository $orderRepository,
                                OrderItemRepository $orderItemRepo)
    {
        $this->userRepo                     = $userRepo;
        $this->subscriptionRepo             = $subscriptionRepo;
        $this->clientYearlySubscriptionRepo = $clientYearlySubscriptionRepo;
        $this->registrationOrderRepo        = $registrationOrderRepo;
        $this->contactRepo                  = $contactRepo;
        $this->subscriptionTypesRepo        = $subscriptionTypesRepo;
        $this->actingAgentRepo              = $actingAgentRepo;
        $this->corporateSeatRepo            = $corporateSeatRepo;
        $this->clientCorpSeatCreditRepo     = $clientCorpSeatCreditRepo;
        $this->classRegistrationRepo        = $classRegistrationRepo;
        $this->orderRepository              = $orderRepository;
        $this->orderItemRepo                = $orderItemRepo;
    }

    /***************************************************************************************/
    /*                                  Class Registration                                 */
    /***************************************************************************************/

    /**
     * Create new class registration
     *
     * @param array $params
     * @return ClassRegistration
     */
    public function saveClassRegistration($params = [])
    {
        $classRegistration = new ClassRegistration();
        $classRegistration->fill($params);
        $classRegistration->save();

        return $classRegistration;
    }

    /**
     * Create new acting agent
     *
     * @param $order
     * @param $contactId
     * @return ActingOriginatingAgent|mixed
     */
    public function saveActingAgent($order, $contactId, $status)
    {
        $contact = $this->contactRepo->getById($contactId);

        if ($contact
            and
            $contact->user
            and
            $order->originating_agent_user_id != $contact->user->id) {

            $params = [
                'orderable_id'       => $order->id,
                'orderable_type'     => class_basename($order),
                'acting_agent_id'    => $contact->user->id,
                'status'             => $status
            ];

            $actingAgentReg = $this->actingAgentRepo
                ->getByOrderableAndAgent($params);

            if (!$actingAgentReg) {
                $actingAgentReg = new ActingOriginatingAgent();
                $actingAgentReg->fill($params);
                $actingAgentReg->save();

                return $actingAgentReg;
            }
        }

    }

    /**
     * Remove acting agents
     *
     * @param $order
     */
    private function removeActingAgent($order)
    {
        $existingActingAgents = $this->actingAgentRepo
            ->getByOrderableAndAgent([
                'orderable_id'     => $order->id,
                'orderable_type'   => class_basename($order),
                'status'           => Constant::PENDING
            ]);

        if ($existingActingAgents->count()) {
            $existingActingAgents->each(function($existingActingAgent) {
                $existingActingAgent->forceDelete();
            });
        }
    }

    /**
     * @param $existingContacts
     * @param $retrievableContacts
     * @param $registrationOrderId
     * @param $charge
     * @return $this|mixed
     */
    private function changeStatusContact($existingContacts,
                                         $retrievableContacts,
                                         $registrationOrderId,
                                         $charge)
    {
        foreach ($existingContacts as $existingContact) {
            $classReg = $this->classRegistrationRepo
                ->getByRegistrationAndAttendee($registrationOrderId, $existingContact);

            if ($classReg) {

                $classReg->item_charge  = 0;
                $classReg->save();

                $classReg->delete();
            }
        }

        foreach ($retrievableContacts as $retrievableContact) {
            $classReg = $this->classRegistrationRepo
                ->getByRegistrationAndAttendee($registrationOrderId, $retrievableContact);

            if ($classReg) {

                $classReg->item_charge  = $charge;
                $classReg->save();

                $classReg->restore();
            }
        }

        return $this;
    }

    /**
     * Create new registration order
     *
     * @param array $params
     * @return RegistrationOrder
     */
    private function saveRegistrationOrder($params = [])
    {
        $registrationOrder = new RegistrationOrder();
        $registrationOrder->fill($params);
        $registrationOrder->save();

        return $registrationOrder;
    }

    /**
     * Save new registration for either class or course
     *
     * @param array $params
     * @return $this
     */
    public function createRegistration($params = [])
    {
        $ctr                 = 0;
        $classRegs           = [];
        $registrationIds     = [];
        $existingContactIds  = [];
        $retrievableContacts = [];
        $hasMilitaryDiscount = false;
        $order = null;

        $existingRegistrationOrder = $this->registrationOrderRepo
            ->getByRegistrableIdAndAgent(
                $params['registrationId'],
                $params['agent'],
                $params['status']
            );

        if ($existingRegistrationOrder) {
            foreach ($existingRegistrationOrder->classRegistrations as $existingClassRegistration) {
                array_push($registrationIds, $existingClassRegistration->id);
            }
        }

        else {
            $existingRegistrationOrder = $this->saveRegistrationOrder([
                'registrable_id'    => $params['registrationId'],
                'registrable_type'  => $params['registrationType'],
                'originating_agent_user_id' => $params['agent'],
                'status'                    => $params['status']
            ]);
        }

        $charge = $existingRegistrationOrder->originalPrice();
        $originalPrice = $charge;

        $this->removeActingAgent($existingRegistrationOrder);
        $registrationOrder = $existingRegistrationOrder;

        if ((count($existingContactIds) || count($retrievableContacts))) {
            $this->changeStatusContact(
                $existingContactIds,
                $retrievableContacts,
                $existingRegistrationOrder->id,
                $charge
            );
        }

        $classRegistrations = $this->classRegistrationRepo
            ->getByIds($registrationIds);

        foreach ($classRegistrations as $classRegistration) {
            $classRegistration->forceDelete();
        }

        foreach ($params['contacts'] as $contact) {
            $contactObject = $this->contactRepo->getById($contact);

            if ($contactObject->user and $contactObject->user->has_military_discount) {
                $hasMilitaryDiscount = true;
            }
        }

        if (User::find($params['agent'])->has_military_discount) {
            $hasMilitaryDiscount = true;
        }

        foreach($params['contacts'] as $contact) {
            $ctr++;

            $paramsClass = [
                'registration_order_id' => $existingRegistrationOrder->id,
                'attendee_contact_id'   => $contact,
                'attended'              => false,
                'item_charge'           => $originalPrice
            ];

            $classRegistration = $this->saveClassRegistration($paramsClass);

            $registrationOrder = $classRegistration->registrationOrder;


            if ($classRegistration->subscriptionDiscount) {
                $ctr--;
            } else {

                $halfPriced = ($ctr > 2 and
                                !$registrationOrder->isCBT() and
                                !CourseUtility::excludedClassHalfPriceRule($registrationOrder)) ? true : false;

                $apply_gsa_discount = ($hasMilitaryDiscount and !$halfPriced) ? true : false;

                $charge = ($apply_gsa_discount) ? apply_gsa_discount($originalPrice) : $originalPrice;

                $itemCharge = ($halfPriced) ? $charge / 2 : $charge;


                $classRegistration->item_charge = $itemCharge;
                $classRegistration->is_half_priced = $halfPriced;
                $classRegistration->save();

                if($apply_gsa_discount) {

                    $order = $this->orderRepository->getSingleByOrderable(
                        Constant::REGISTRATION_ORDER,
                        $registrationOrder->id
                    );

                    DiscService::saveMilitaryDiscount([
                        'order_id' => $order->id,
                        'contact_id' => $contact,
                        'class_reg_id' => $classRegistration->id,
                        'deduction' => $originalPrice - $charge
                    ]);

                }

            }

            array_push($classRegs, $classRegistration);
            $this->saveActingAgent($registrationOrder, $contact, $params['status']);
        }

        OrdService::setPriceCharged($registrationOrder);
        DiscService::applyDiscountAndSorting($classRegs, $registrationOrder);

        $order = $this->orderRepository->getSingleByOrderable(
            Constant::REGISTRATION_ORDER,
            $registrationOrder->id
        );

        return $order;
    }

    /***************************************************************************************/
    /*                                  Yearly Subscription                                */
    /***************************************************************************************/

    /**
     * @param array $params
     * @return ClientYearlySubscription
     */
    private function saveClientSubscription($params = [])
    {
        $clientSubscription = new ClientYearlySubscription();
        $clientSubscription->fill($params);
        $clientSubscription->save();

        return $clientSubscription;
    }

    /**
     * Create new subscription
     *
     * @param array $params
     * @return YearlySubscription
     */
    public function saveSubscription($params = [])
    {
        $subscription = new YearlySubscription();
        $subscription->fill($params);
        $subscription->save();

        return $subscription;
    }

    /**
     * Save new yearly subscription
     *
     * @param array $params
     * @return $this
     */
    public function createSubscription($params = [])
    {
        $subscriptionIds = [];

        $existingSubscription = $this->subscriptionRepo
            ->getByAgent($params['originating_agent_user_id']);

        if ($existingSubscription) {
            foreach ($existingSubscription->clientSubscriptions as $subscription) {
                array_push($subscriptionIds, $subscription->id);
            }

            $existingSubscription->type_id = $params['subscription_id'];
            $existingSubscription->save();
        }

        else {

            $existingSubscription = $this->saveSubscription([
                'type_id'                   => $params['subscription_id'],
                'originating_agent_user_id' => $params['originating_agent_user_id'],
                'status'                    => Constant::PENDING
            ]);
        }

        ClientYearlySubscription::destroy($subscriptionIds);

        $this->removeActingAgent($existingSubscription);

        $type = $this->subscriptionTypesRepo->getById($params['subscription_id']);

        foreach($params['contacts'] as $contact) {
            $contactObject = $this->contactRepo->getById($contact);
            $user = $contactObject->user;

            $this->saveClientSubscription([
                'yearly_subscription_id'    => $existingSubscription->id,
                'user_id'                   => $user->id,
                'temporary_price'           => $type->price
            ]);

            $this->saveActingAgent($existingSubscription, $user->contact->id, Constant::PENDING);
        }

        foreach ($subscriptionIds as $subscriptionId) {

            DiscService::removeOrderAdjustmentDiscountByAdjustable([
                'adjustable_id'   => $subscriptionId,
                'adjustable_type' => Constant::CLIENT_SUBSCRIPTION
            ]);
        }

        return $this;
    }

    /***************************************************************************************/
    /*                                  Corporate Seat Credit                              */
    /***************************************************************************************/

    /**
     * Create new credit
     *
     * @param array $params
     * @return $this
     */
    public function createCredit($params = [])
    {
        $existingCredit = $this->clientCorpSeatCreditRepo
            ->getByAgent($params['user_id'])
            ?: new ClientCorporateSeatCredit();

        if ($existingCredit) {
            DiscService::removeOrderAdjustmentDiscountByAdjustable([
                'adjustable_id'     => $existingCredit->id,
                'adjustable_type'   => Constant::CORPORATE_SEAT
            ]);
        }

        $corpSeat = $this->corporateSeatRepo->getById($params['corp_seat_id']);

        $creditNumber = !is_null($corpSeat) ? $corpSeat->credit_number: $params['credit_number'];
        $price = !is_null($corpSeat) ? $corpSeat->price : $params['price'];

        $existingCredit->originating_agent_user_id = $params['user_id'];
        $existingCredit->credits = $creditNumber;
        $existingCredit->price   = $price;
        $existingCredit->status  = Constant::PENDING;
        $existingCredit->save();

        return $this;
    }

    /***************************************************************************************/
    /*                                  OnSite Training                                    */
    /***************************************************************************************/

    /**
     * @param $params
     * @return OnsiteTraining
     */
    public function createTraining($params)
    {
        $training = new OnsiteTraining();

        $params['training_date'] = Carbon::parse($params['training_date'])->format('Y-m-d');

        $training->fill($params);
        $training->save();

        return $training;
    }
}