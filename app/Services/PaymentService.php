<?php namespace App\Services;

use App\Facades\DiscountService as DiscService;
use App\Facades\OrderUtility;
use App\Models\ClassCombinationDiscount;
use App\Models\Contact;
use App\Models\OrderTransaction;
use App\Models\StateTax;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Log;
use Session;
use App\Models\Order;
use App\Models\PaymentTransaction;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Services\Mailer\RegistrationMailer;
use App\Utilities\Constant;
use Carbon\Carbon;
use App\Events\RegistrationConfirmationEvent;
use App\Facades\OrderService;


class PaymentService
{

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var AuthorizeService
     */
    private $authorize;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $itemRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var RegistrationMailer
     */
    private $mailer;

    protected static $GSA_CC_START = [4486,4614,4716,5568,5565];

    /**
     * @param UserRepository $userRepo
     * @param AuthorizeService $authorize
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $itemRepo
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     * @param RegistrationMailer $mailer
     */
    public function __construct(UserRepository $userRepo,
                                AuthorizeService $authorize,
                                OrderRepository $orderRepo,
                                OrderItemRepository $itemRepo,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                RegistrationMailer $mailer)
    {
        $this->userRepo = $userRepo;
        $this->authorize = $authorize;
        $this->orderRepo = $orderRepo;
        $this->itemRepo = $itemRepo;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->mailer = $mailer;
    }

    /**
     * Make payment
     *
     * @param array $paymentInfo
     * @return PaymentTransaction
     * @throws \Exception
     */
    public function doPayment($paymentInfo = [])
    {

        $notifyAgents = (array_key_exists('doNotEmail', $paymentInfo)) ? false : true;
        session(['notify_agents' => $notifyAgents]);
        $skipEmail = (array_key_exists('skip-email-notification', $paymentInfo) &&
            $paymentInfo['skip-email-notification'] == 1) ? true : false;

        try {

            $order = Order::find($paymentInfo['order_id']);

            if($order->status == Constant::COMPLETE) {
                throw new \Exception('Order is already completed. Please call customer service if you did not receive your receipt and login details.', 601);
            }

            $paymentInfo['amount'] = $paymentInfo['current_total'];

            $updatedStatus = false;
            $errors = [];
            $paymentInfo['card_number'] = preg_replace('/\s+/', '', $paymentInfo['card_number']);

            $shipping_rates = (!empty(trim($paymentInfo['shipping_fee'])))
                ? (float) $paymentInfo['shipping_fee']
                : 0;

            $state_tax = (!empty(trim($paymentInfo['state_tax'])))
                ? (float) $paymentInfo['state_tax']
                : 0;

            if($this->checkIfHasMilitaryDiscount($paymentInfo['card_number'])) {
                $paymentInfo['amount'] = $this->applyGSADiscount($paymentInfo);
                if(env('GSA_TEST_MODE', 0)) {
                    $paymentInfo['card_number'] = '4111111111111111';
                }
            }

            $charge = $this->authorize
                ->chargeCreditCard(
                    $paymentInfo['card_number'],
                    $paymentInfo['cvv'],
                    $paymentInfo['card_exp']
                );

            $mask_number = function ($number, $maskingCharacter = 'X') {
                return str_repeat($maskingCharacter, (strlen($number) - 8) < 0 ? 0 : (strlen($number) - 8)) . substr($number, -4);
            };

            $masked_number = $mask_number($paymentInfo['card_number']);
            unset($paymentInfo['card_number']);

            $_orders = $paymentInfo['order_id'];

            if (!is_array($_orders)) {
                $_orders = [$paymentInfo['order_id']];
            }

            $order_info = $this->authorize->orderInfo($paymentInfo['description']);

            $items = isset($paymentInfo['order_id'])
                ? $this->itemRepo->getByOrderIds($_orders)
                : $this->itemRepo->getByIds($paymentInfo['items']);

            $customerInfo = $this->authorize->customerInfo([
                'email'         => $paymentInfo['email'],
                'first_name'    => $paymentInfo['first_name'],
                'last_name'     => $paymentInfo['last_name'],
                'company'       => $paymentInfo['billing_company'],
                'address'       => $paymentInfo['billing_address1'],
                'city'          => $paymentInfo['billing_city'],
                'state'         => $paymentInfo['billing_state'],
                'zip'           => $paymentInfo['billing_zip']
            ]);

            $response = $this->authorize->createTransaction([
                'amount'        => $paymentInfo['amount'],
                'shipping_rates'=> $shipping_rates,
                'payment'       => $charge['payment'],
                'order'         => $order_info,
                'items'         => $items,
                'customer'      => $customerInfo['customer'],
                'billTo'        => $customerInfo['billTo'],
                'auth'          => $charge['auth'],
                'ref'           => $charge['ref']
            ]);

            if (!is_null($response)) {

                $tresponse = $response->getTransactionResponse();

                if ($tresponse) {
                    collect($tresponse->getErrors())->each(function ($_error, $key) use (&$errors) {
                        $errors[] = $_error->getErrorText();
                    });
                }

                if ($response->getMessages() && $response->getMessages()->getResultCode() == 'Error') {

                    array_push($errors, 'An error occurred while processing your payment.');

                    $errorMessages = $response->getMessages()->getMessage();

                    if (isset($errorMessages[0]) and $errorMessages[0]->getCode() == 'E00003') {
                        array_push($errors, 'Address is to long.');
                    } else {
                        array_push($errors, 'Credit Card is invalid.');
                    }

                    throw new \Exception(implode(' ', $errors));
                }

                if (!is_null($tresponse) and $tresponse->getResponseCode() == '1' and empty($errors)) {

                    $paymentInfo['shipment_county'] = StateTax::getShippingCounty($paymentInfo['shipment_county']);

                    $order = Order::find($paymentInfo['order_id']);
                    $order->status = Constant::COMPLETE;
                    $order->completed_at = Carbon::now();
                    $order->shipping_rates = $shipping_rates;
                    $order->total =  $paymentInfo['amount'];
                    $order->paid_total = $paymentInfo['amount'];
                    $order->save();
                    $order->updateTotal($paymentInfo);
                    $order->updateBillingAndShippingAddress($paymentInfo);

                    if (isset($items)) {
                        $order = $order->fresh();
                        OrderService::updatePaidCharge($order);
                        OrderService::checkIfSubscriptionUsed($order->user, $items, $notifyAgents);
                        $updatedStatus = OrderService::updateStatusOfOrders($items, Constant::COMPLETE, $paymentInfo['tax_rate']);
                    }

                    $paymentTransaction = $this->createPaymentTransaction([
                        'email'             => $paymentInfo['email'],
                        'card_brand'        => $tresponse->getAccountType(),
                        'auth_code'         => $tresponse->getAuthCode(),
                        'transaction_id'    => $tresponse->getTransId(),
                        'masked_cc'         => $masked_number,
                        'amount'            => $paymentInfo['amount'],
                        'shipping_rates'    => $shipping_rates,
                        'state_tax'         => $state_tax,
                        'status'            => true,
                        'order_id'          => $order->id,
                        'items'             => OrderUtility::createPaymentItems($order->items,  $paymentInfo['amount']),
                        'trans_total'       => $paymentInfo['amount'],
                    ]);

                    $paymentTransaction->updateBillingAndShippingAddress($paymentInfo);


                    $this->createOrderTransaction([
                        'order_id'          => $order->id,
                        'transaction_id'    => generate_transaction_id($order)
                    ]);

                    if ($updatedStatus) {
                        Session::flash('PAYMENT_SUCCESS', true);

                        if(!$skipEmail) {
                            event(new RegistrationConfirmationEvent($order->id, $masked_number, null, [], $notifyAgents));
                        }

                    }

                    Session::flash('TYPE', $order->checkout_combination);

                    return $paymentTransaction;
                }
            }

            throw new \Exception(implode(' ', $errors));

        } catch (\Exception $e) {
            return Session::flash('error_message', $e->getMessage());
        }
    }

    /**
     * Create payment transaction instance
     *
     * @param array $params
     * @return PaymentTransaction
     * @throws \Exception
     */
    public function createPaymentTransaction($params = [])
    {
        try {

            $paymentTransaction = new PaymentTransaction();
            $paymentTransaction->fill($params);
            $paymentTransaction->save();

            return $paymentTransaction;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    private function checkIfHasMilitaryDiscount($ccnum) {
        return in_array(substr($ccnum, 0, 4), $this::$GSA_CC_START);
    }

    private function applyGSADiscount($paymentInfo)
    {

        $amount = array_key_exists('amount', $paymentInfo )
                    ? $paymentInfo['amount']
                    : 0;

        try {
            $order = Order::findOrFail($paymentInfo['order_id']);
        } catch (ModelNotFoundException $e) {
            return $amount;
        }

        if ($order->user->has_military_discount) {
            return $amount;
        }

        foreach ($order
                     ->items()
                     ->where('orderable_type', '=', 'RegistrationOrder')
                     ->get() as $order_item) {
            $registrationOrder = $order_item->orderable;
            $contacts = $registrationOrder->classRegistrations->pluck('attendee_contact_id')->toArray();

            // if one of the contact has GSA already then it means that the GSA discount has already been applied
            foreach ($contacts as $contact) {
                $contactObject = Contact::find($contact);
                if ($contactObject->user and $contactObject->user->has_military_discount) {
                    return $amount;
                }
            }

            // apply gsa discount on class registrations
            foreach ($registrationOrder->classRegistrations as $classRegistration) {

                if($classRegistration->is_half_priced) {
                    continue;
                }

                $curr_charge = $classRegistration->item_charge;

                if($curr_charge == 0) {
                    continue;
                }

                $original_price = $this->getOriginalPrice($classRegistration);

                $gsa_price = apply_gsa_discount($original_price);

                $discounted_charge = $this->reApplyExistingDiscounts($classRegistration, $gsa_price);

                $classRegistration->item_charge = $discounted_charge;

                $classRegistration->save();

                $deduction = $curr_charge - $discounted_charge;

                DiscService::saveMilitaryDiscount([
                    'order_id' => $order->id,
                    'contact_id' => $classRegistration->attendee_contact_id,
                    'class_reg_id' => $classRegistration->id,
                    'deduction' => $deduction
                ]);

                // adjust total amount
                $amount = $amount - $deduction;

            }

        }

        // apply GSA tag on user
        $order->user->update([
            'has_military_discount' => 1
        ]);

        // update order total amount
        $order->update([
            'total' => $amount
        ]);

        return $amount;

    }

    private function createOrderTransaction($params)
    {
        $orderTransaction = new OrderTransaction();
        $orderTransaction->fill($params);
        $orderTransaction->save();

        return $orderTransaction;
    }

    protected function getOriginalPrice($classRegistration)
    {
        $ccDiscount = $classRegistration->classCouponDiscounts()->first();
        $classCombinationDiscount =
            ClassCombinationDiscount::where(function($query) use ($classRegistration) {
                $query->where('class_reg_one', $classRegistration->id)
                    ->orWhere('class_reg_two', $classRegistration->id);
            })->first();

        if ($ccDiscount != null) {
            return $classRegistration->item_charge + $ccDiscount->deduction;
        }

        if ($classCombinationDiscount != null) {
            return $classRegistration->item_charge + 50;
        }

        return $classRegistration->item_charge;

    }

    protected function reApplyExistingDiscounts($classRegistration, $gsa_price)
    {
        $ccDiscount = $classRegistration->classCouponDiscounts()->first();
        $classCombinationDiscount =
            ClassCombinationDiscount::where(function($query) use ($classRegistration) {
                $query->where('class_reg_one', $classRegistration->id)
                    ->orWhere('class_reg_two', $classRegistration->id);
            })->first();

        if ($ccDiscount != null) {
            return $gsa_price - $ccDiscount->deduction;
        }

        if ($classCombinationDiscount != null) {
            return $gsa_price - 50;
        }

        return $gsa_price;
    }

}