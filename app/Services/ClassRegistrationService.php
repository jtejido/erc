<?php namespace App\Services;

use App\Repositories\ClassRegistrationRepository;
use App\Utilities\Constant;

class ClassRegistrationService
{

    private $classRegistrationRepo;

    /**
     * ClassRegistrationService constructor.
     * @param ClassRegistrationRepository $classRegistrationRepository
     */
    public function __construct(ClassRegistrationRepository $classRegistrationRepository)
    {
        $this->classRegistrationRepo = $classRegistrationRepository;
    }

    public function getAttendanceStatusOfAttendee($orderId, $studentId)
    {

        $format = "<small class=\"label label-disabled label-%s\"><i class='fa fa-check-square-o'></i> %s</small>";
        $format_absent = "<small class=\"label label-disabled label-%s\"><i class='fa fa-minus-square-o'></i> %s</small>";
        $format_tbc = "<small class=\"label label-disabled label-%s\"><i class='fa fa-square-o'></i> %s</small>";

        $classReg = $this->classRegistrationRepo->getByRegistrationAndAttendee($orderId, $studentId);

        $status = $tbc = sprintf($format_tbc, 'default', 'to be confirmed'); //"<span class=\"label label-default\">TBC <i>To be confirmed</i></span>";

        switch ($classReg->registrationOrder->course()->typeId) {

            case Constant::SEMINAR:
                if ($classReg->attended == 1)
                    $status = sprintf($format, 'success', 'present');
                elseif ($classReg->attended == 2)
                    $status = sprintf($format_absent, 'danger', 'absent');

                if ($classReg->certificate_sent) {
                    $status .= ' ';
                    $status .= sprintf($format, 'success', 'certificate');
                }

                break;
            case Constant::WEBCAST:
                if ($classReg->meeting_flag == 1)
                    $status = sprintf($format, 'success', 'webcast');
                elseif ($classReg->meeting_flag == 2)
                    $status = sprintf($format_absent, 'danger', 'webcast');
                else
                    $status = sprintf($format_tbc, 'default', 'webcast');

                $status .= '<br/>';

                if ($classReg->exam_flag == 1)
                    $status .= sprintf($format, 'success', 'exam');
                elseif ($classReg->exam_flag == 2)
                    $status .= sprintf($format_absent, 'danger', 'exam');
                else
                    $status .= sprintf($format_tbc, 'default', 'exam');

                if ($classReg->certificate_sent) {
                    $status .= '<br/>';
                    $status .= sprintf($format, 'success', 'certificate');
                }

                if ($classReg->reminder_flag) {
                    $status .= '<br/>';
                    $status .= sprintf($format, 'success', 'reminder');
                }

                break;
            case Constant::COMPUTER_BASED:
                if ($classReg->attended == 1)
                    $status = sprintf($format, 'success', 'present');
                elseif ($classReg->attended == 2)
                    $status = sprintf($format_absent, 'danger', 'absent');
                break;
            default:
                $status = $tbc;
        }

        return $status;
    }

    public function getActionView($typeId, $flag = 1 )
    {
        switch ($typeId) {
            case Constant::SEMINAR:
                $view = ($flag)
                    ? 'admin.certificates._seminar-action'
                    : 'admin.certificates._seminar-allAction';
                break;
            case Constant::WEBCAST:
                $view = ($flag)
                    ? 'admin.certificates._webcast-action'
                    : 'admin.certificates._webcast-allAction';
                break;
            case Constant::COMPUTER_BASED:
                $view = null;
                break;
            default:
                $view = ($flag)
                    ? 'admin.certificates._seminar-action'
                    : 'admin.certificates._seminar-allAction';
        }
        return $view;
    }

    public function getAttendanceStatusOfAttendeeFlag($orderId, $studentId)
    {
        $classReg = $this->classRegistrationRepo->getByRegistrationAndAttendee($orderId, $studentId);
        return $classReg->attended;
    }

    public function getAttendanceMeetingStatusOfAttendeeFlag($orderId, $studentId)
    {
        $classReg = $this->classRegistrationRepo->getByRegistrationAndAttendee($orderId, $studentId);
        return $classReg->meeting_flag;
    }

    public function getAttendanceExamStatusOfAttendeeFlag($orderId, $studentId)
    {
        $classReg = $this->classRegistrationRepo->getByRegistrationAndAttendee($orderId, $studentId);
        return $classReg->exam_flag;
    }

    public function getAttendanceWithFlag($classId, $flag, $key)
    {
        return $this->classRegistrationRepo->getCount($classId, $flag, $key);
    }

    public function getAttendanceWithFlagOfCourse($courseId, $flag, $key)
    {
        return $this->classRegistrationRepo->getCountForCourse($courseId, $flag, $key);
    }

}
