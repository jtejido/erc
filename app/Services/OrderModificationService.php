<?php namespace App\Services;

use App\Events\SubscriptionUsedEvent;
use App\Facades\CourseUtility;
use App\Facades\OrderUtility;
use App\Models\ClientCorporateSeatCredit;
use App\Models\ClientYearlySubscription;
use App\Models\ProductOrders;
use Auth;
use App\Models\User;
use App\Models\Contact;
use App\Models\ClassRegistration;
use App\Models\ActingOriginatingAgent;
use App\Models\OrderAdjustment;
use App\Models\CancellationNote;
use App\Models\OrderAdjustmentHistory;
use App\Events\SwapUserEvent;
use App\Models\RegistrationOrder;
use App\Repositories\OrderRepository;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\RemovedDiscountByCreditRepository;
use App\Repositories\OrderAdjustmentRepository;
use App\Repositories\OrderItemRepository;
use App\Models\OrderAdjustmentDiscount;
use App\Events\EmailAccountingCancellationEvent;
use App\Facades\OrderModificationDiscountService as ModDiscountService;
use App\Utilities\Constant;
use App\Facades\DiscountService as DiscService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Facades\OrderService;

class OrderModificationService
{

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var ClassCombinationRepository
     */
    private $classCombinationRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var ClassHalfPriceDiscountRepository
     */
    private $classHalfPriceDiscountRepo;

    /**
     * @var ClassCouponDiscountRepository
     */
    private $classCouponDiscountRepo;

    /**
     * @var ClassCreditDiscountRepository
     */
    private $classCreditDiscountRepo;

    /**
     * @var OrderAdjustmentDiscountRepository
     */
    private $orderAdjustmentDiscountRepo;

    /**
     * @var RemovedDiscountByCreditRepository
     */
    private $removedDiscountByCreditRepo;

    /**
     * @var RegistrationService
     */
    private $registrationService;

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param RemovedDiscountByCreditRepository $removedDiscountByCreditRepo
     * @param RegistrationService $registrationService
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     */
    public function __construct(ClassRegistrationRepository $classRegistrationRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ClassCombinationRepository $classCombinationRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo,
                                ClassCreditDiscountRepository $classCreditDiscountRepo,
                                OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                RemovedDiscountByCreditRepository $removedDiscountByCreditRepo,
                                RegistrationService $registrationService,
                                OrderAdjustmentRepository $orderAdjustmentRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo)
    {
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->classCombinationRepo = $classCombinationRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->classHalfPriceDiscountRepo = $classHalfPriceDiscountRepo;
        $this->classCouponDiscountRepo = $classCouponDiscountRepo;
        $this->classCreditDiscountRepo = $classCreditDiscountRepo;
        $this->orderAdjustmentDiscountRepo = $orderAdjustmentDiscountRepo;
        $this->removedDiscountByCreditRepo = $removedDiscountByCreditRepo;
        $this->registrationService = $registrationService;
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function doSwapAttendees($params = [])
    {
        $pairedClassAddedIds = collect();
        $pairedClassRemovedIds = collect();
        $pairedRegistrationOrderId = null;

        $agent = User::find($params['agent_id']);
        $count = $params['addedContacts']->count();
        $addedContacts = $params['addedContacts']->values()->toArray();
        $removedContacts = $params['removedContacts']->values()->toArray();

        for ($i = 0; $i < $count; $i++) {
            $removedClassRegistration = $this->classRegistrationRepo
                ->getByRegistrationAndAttendee(
                    $params['registrationOrderId'],
                    $removedContacts[$i]
                );

            $addedClassRegistration = $this->classRegistrationRepo
                ->getByRegistrationAndAttendee(
                    $params['registrationOrderId'],
                    $addedContacts[$i]
                );

            if (!$addedClassRegistration) {
                $addedClassRegistration = new ClassRegistration();
            }

            else {
                if ($addedClassRegistration->trashed()) {
                    $addedClassRegistration->restore();
                }
            }

            $addedClassRegistration->fill([
                'registration_order_id' => $params['registrationOrderId'],
                'attendee_contact_id' => $addedContacts[$i],
                'attended' => $removedClassRegistration->attended,
                'meeting_flag' => $removedClassRegistration->meeting_flag,
                'exam_flag' => $removedClassRegistration->exam_flag,
                'reminder_flag' => $removedClassRegistration->reminder_flag,
                'is_half_priced' => $removedClassRegistration->is_half_priced,
                'corporate_seat_credit_applied' => $removedClassRegistration->corporate_seat_credit_applied,
                'certificate_sent' => $removedClassRegistration->certificate_sent,
                'certificate_file_path' => $removedClassRegistration->certificate_file_path,
                'certificate_link' => $removedClassRegistration->certificate_link,
                'exam_result' => $removedClassRegistration->exam_result,
                'is_cancelled' => false,
                'is_swapped' => false,
                'swap_note' => null
            ]);

            $addedClassRegistration->save();

            $response = $this->swapHWMDOTRegistration(
                $removedContacts[$i],
                $addedContacts[$i],
                $params['registrationOrderId'],
                $params['status']
            );

            if (isset($response['added_contact_id']) and
                isset($response['removed_contact_id']) and
                isset($response['registration_order_id'])) {

                $pairedClassAddedIds->push($response['added_contact_id']);
                $pairedClassRemovedIds->push($response['removed_contact_id']);
                $pairedRegistrationOrderId = $response['registration_order_id'];
            }

            $this->swapActingAgent($removedContacts[$i], $addedContacts[$i], $params['registrationOrderId']);
            $this->swapGroupDiscount($removedClassRegistration, $addedClassRegistration);
            $this->swapCouponDiscount($removedClassRegistration, $addedClassRegistration);
            $this->swapHWMDOTDiscount($removedClassRegistration, $addedClassRegistration);
            $this->swapCorporateSeatDiscount($removedClassRegistration, $addedClassRegistration);
            $this->swapAdjustmentDiscount($removedClassRegistration, $addedClassRegistration);
            $this->swapRemovedDiscountByCredit($removedClassRegistration, $addedClassRegistration);

            $addedClassRegistration->item_charge = $removedClassRegistration->item_charge;
            $addedClassRegistration->paid_charge = $removedClassRegistration->item_charge;
            $addedClassRegistration->save();

            $removedClassRegistration->is_half_priced = false;
            $removedClassRegistration->item_charge = 0;
            $removedClassRegistration->paid_charge = 0;
            $removedClassRegistration->is_swapped = true;
            $removedClassRegistration->swap_note = 'Swapped to ' . $addedClassRegistration->contact->name . '. Date: ' .
                                                   Carbon::now()->toFormattedDateString();
            $removedClassRegistration->save();
        }

        if ($pairedClassAddedIds->count() and $pairedClassRemovedIds->count()) {
            event(new SwapUserEvent(
                Auth::user()->id,
                $agent->id,
                $pairedRegistrationOrderId,
                $pairedClassAddedIds->toArray(),
                $pairedClassRemovedIds->toArray()
            ));
        }

        return $this;
    }

    /**
     * @param $removedContactId
     * @param $addedContactId
     * @param $registrationOrderId
     * @param $status
     * @return $this
     */
    private function swapHWMDOTRegistration($removedContactId, $addedContactId, $registrationOrderId, $status)
    {
        $classRegistration = $this->classRegistrationRepo->getByAttendeeRegistrationStatus([
            'attendee_contact_id' => $removedContactId,
            'registration_order_id' => $registrationOrderId,
            'status' => $status
        ]);

        if ($classRegistration) {
            $registrationOrder = $this->registrationOrderRepo->getById($registrationOrderId);
            $classCombination = $this->classCombinationRepo->getBySingeRegId($registrationOrderId);
            $classCombinationDiscount = $this->classCombinationDiscountRepo->getByClassRegistrationId($classRegistration->id, true);

            if ($registrationOrder->has_pair and $classCombination and $classCombinationDiscount) {

                $pairRemovedClassRegistration = $classCombinationDiscount->classOne->id != $classRegistration->id
                    ? $classCombinationDiscount->classOne
                    : $classCombinationDiscount->classTwo;

                $pairRegistrationOrder = $pairRemovedClassRegistration->registrationOrder;
                $pairAddedClassRegistration = $this->classRegistrationRepo
                    ->getByRegistrationAndAttendee(
                        $pairRegistrationOrder->id,
                        $addedContactId
                    );
                $pairedRegistrationOrderId = $classCombination->firstRegistration->id == $registrationOrderId
                    ? $classCombination->secondRegistration->id
                    : $classCombination->firstRegistration->id;

                if (!$pairAddedClassRegistration) {
                    $pairAddedClassRegistration = new ClassRegistration();
                }

                else {
                    if ($pairAddedClassRegistration->trashed()) {
                        $pairAddedClassRegistration->restore();
                    }
                }

                $pairAddedClassRegistration->fill([
                    'registration_order_id' => $pairRegistrationOrder->id,
                    'attendee_contact_id' => $addedContactId,
                    'attended' => $pairRemovedClassRegistration->attended,
                    'meeting_flag' => $pairRemovedClassRegistration->meeting_flag,
                    'exam_flag' => $pairRemovedClassRegistration->exam_flag,
                    'reminder_flag' => $pairRemovedClassRegistration->reminder_flag,
                    'is_half_priced' => $pairRemovedClassRegistration->is_half_priced,
                    'corporate_seat_credit_applied' => $pairRemovedClassRegistration->corporate_seat_credit_applied,
                    'certificate_sent' => $pairRemovedClassRegistration->certificate_sent,
                    'certificate_file_path' => $pairRemovedClassRegistration->certificate_file_path,
                    'certificate_link' => $pairRemovedClassRegistration->certificate_link,
                    'exam_result' => $pairRemovedClassRegistration->exam_result
                ]);

                $pairAddedClassRegistration->save();

                $this->swapGroupDiscount($pairRemovedClassRegistration, $pairAddedClassRegistration);
                $this->swapCouponDiscount($pairRemovedClassRegistration, $pairAddedClassRegistration);
                $this->swapHWMDOTDiscount($pairRemovedClassRegistration, $pairAddedClassRegistration);
                $this->swapCorporateSeatDiscount($pairRemovedClassRegistration, $pairAddedClassRegistration);
                $this->swapAdjustmentDiscount($pairRemovedClassRegistration, $pairAddedClassRegistration);
                $this->swapRemovedDiscountByCredit($pairRemovedClassRegistration, $pairAddedClassRegistration);

                $pairAddedClassRegistration->item_charge = $pairRemovedClassRegistration->item_charge;
                $pairAddedClassRegistration->save();

                $pairRemovedClassRegistration->is_half_priced = false;
                $pairRemovedClassRegistration->item_charge = 0;
                $pairRemovedClassRegistration->is_swapped = true;
                $pairRemovedClassRegistration->swap_note = 'Swapped to ' . $pairAddedClassRegistration->contact->name;
                $pairRemovedClassRegistration->save();

                return [
                    'added_contact_id' => $addedContactId,
                    'removed_contact_id' => $removedContactId,
                    'registration_order_id' => $pairedRegistrationOrderId
                ];
            }
        }
    }

    /**
     * @param $removedContactId
     * @param $addedContactId
     * @param $registrationId
     * @return $this
     */
    private function swapActingAgent($removedContactId, $addedContactId, $registrationId)
    {
        $removedContact = Contact::find($removedContactId);
        $addedContact = Contact::find($addedContactId);

        if ($removedContact->user) {
            $removedActingAgent = $this->actingOriginatingAgentRepo
                ->getByOrderableAndAgent([
                    'orderable_id' => $registrationId,
                    'orderable_type' => Constant::REGISTRATION_ORDER,
                    'acting_agent_id' => $removedContact->user->id,
                ]);

            if ($removedActingAgent) {
                $removedActingAgent->delete();
            }
        }

        if ($addedContact->user) {
            $addedActingAgent = $this->actingOriginatingAgentRepo
                ->getByOrderableAndAgent([
                    'orderable_id' => $registrationId,
                    'orderable_type' => Constant::REGISTRATION_ORDER,
                    'acting_agent_id' => $addedContact->user->id,
                ]);

            if ($addedActingAgent and $addedActingAgent->trashed()) {
                $addedActingAgent->restore();
            }

            else {
                ActingOriginatingAgent::create([
                    'orderable_id' => $registrationId,
                    'orderable_type' => Constant::REGISTRATION_ORDER,
                    'acting_agent_id' => $addedContact->user->id
                ]);
            }
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapGroupDiscount($removedClassRegistration, $addedClassRegistration)
    {
        $groupDiscount = $this->classHalfPriceDiscountRepo
            ->getByClassRegId($removedClassRegistration->id);

        if ($groupDiscount) {
            $groupDiscount->class_reg_id = $addedClassRegistration->id;
            $groupDiscount->save();
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapCouponDiscount($removedClassRegistration, $addedClassRegistration)
    {
        $couponDiscounts = $this->classCouponDiscountRepo
            ->getByClassRegId($removedClassRegistration->id);

        if ($couponDiscounts->count()) {
            $couponDiscounts->each(function($couponDiscount) use ($addedClassRegistration) {
                $couponDiscount->class_reg_id = $addedClassRegistration->id;
                $couponDiscount->save();
            });
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapHWMDOTDiscount($removedClassRegistration, $addedClassRegistration)
    {
        $inClassOne = $this->classCombinationDiscountRepo
            ->getInClassOne($removedClassRegistration->id);

        $inClassTwo = $this->classCombinationDiscountRepo
            ->getInClassTwo($removedClassRegistration->id);

        if ($inClassOne) {
            $inClassOne->class_reg_one = $addedClassRegistration->id;
            $inClassOne->save();
        }

        if ($inClassTwo) {
            $inClassTwo->class_reg_two = $addedClassRegistration->id;
            $inClassTwo->save();
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapCorporateSeatDiscount($removedClassRegistration, $addedClassRegistration)
    {
        $corpSeatDiscount = $this->classCreditDiscountRepo
            ->getByClassRegId($removedClassRegistration->id);

        if ($corpSeatDiscount) {
            $corpSeatDiscount->class_reg_id = $addedClassRegistration->id;
            $corpSeatDiscount->save();
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapAdjustmentDiscount($removedClassRegistration, $addedClassRegistration)
    {
        $orderAdjustmentDiscounts = $this->orderAdjustmentDiscountRepo
            ->getByAdjustable([
                'adjustable_id' => $removedClassRegistration->id,
                'adjustable_type' => Constant::CLASS_REGISTRATION
            ]);

        if ($orderAdjustmentDiscounts->count()) {
            $orderAdjustmentDiscounts->each(function($orderAdjustmentDiscount)
            use ($addedClassRegistration) {
                $orderAdjustmentDiscount->adjustable_id = $addedClassRegistration->id;
                $orderAdjustmentDiscount->save();
            });
        }

        return $this;
    }

    /**
     * @param $removedClassRegistration
     * @param $addedClassRegistration
     * @return $this
     */
    private function swapRemovedDiscountByCredit($removedClassRegistration, $addedClassRegistration)
    {
        $removedDiscounts = $this->removedDiscountByCreditRepo
            ->getByClassRegId($removedClassRegistration->id);

        if ($removedDiscounts->count()) {
            $removedDiscounts->each(function($removedDiscount) use ($addedClassRegistration) {
                $removedDiscount->class_reg_id = $addedClassRegistration->id;
                $removedDiscount->save();
            });
        }

        return $this;
    }

    /**
     * @param array $params
     */
    public function cancelClassRegistration($params = [])
    {
        $notify = (array_key_exists('doNotEmail', $params)) ? false : true;

        $pairRegistrationOrder = null;
        $classRegistrationIds = collect();
        $agent = User::find($params['agent_id']);
        $registrationOrder = $this->registrationOrderRepo->getById($params['registration_order_id']);

        foreach ($params['class_registration_ids'] as $classRegistrationId) {
            $classRegistration = $this->classRegistrationRepo->getById($classRegistrationId);

            if ($classRegistration->is_new) {
                $classRegistrationIds->pluck($classRegistration->id);
                $classRegistration->forceDelete();
            }

            else {
                $charge = $classRegistration->item_charge;

                $classRegistration->is_cancelled = true;
                $classRegistration->item_charge = 0;
                $classRegistration->save();

                $classRegistrationIds->push($classRegistration->id);

                $note = customer_cancellation_note($params['notes'][$classRegistrationId]);
                $orderAdjustmentHistoryNote = customer_cancellation_history_note(
                    $classRegistration,
                    $registrationOrder,
                    $params['notes'][$classRegistrationId]
                );
                $order = $this->orderRepo->getSingleByOrderable(Constant::REGISTRATION_ORDER, $registrationOrder->id);

                $this->createCancellationNote(
                    $classRegistration->attendee_contact_id,
                    $classRegistration->id,
                    $note
                );

                $this->createOrderAdjustmentHistory([
                    'order_id' => $order->id,
                    'note' => $orderAdjustmentHistoryNote,
                    'adjustment_action' => Constant::CANCELLED,
                    'amount' => $charge
                ]);
            }

        }

        if ($classRegistrationIds->count()) {
            $classRegistrations = $this->classRegistrationRepo->getByRegistrationIdNoSwapNoCancel($registrationOrder->id);

            $this->createBalanceDue($params, $agent, $registrationOrder);

            ModDiscountService::reComputeOrderWhenCancelled(
                $classRegistrations,
                $agent,
                isset($params['deferred_half_price_class_registration_ids'])
                    ? $params['deferred_half_price_class_registration_ids']
                    : [],
                isset($params['deferred_group_discounts_class_registration_ids'])
                    ? $params['deferred_group_discounts_class_registration_ids']
                    : []
            );

            ModDiscountService::removeDiscountsCancelledClassRegistrations(
                $this->classRegistrationRepo->getByIds($classRegistrationIds->toArray())
            );

            $order = $registrationOrder->orderItem->order;
            OrderService::updatePaidOrderTotal($order);

            $order = $order->fresh();

            if($order->invoicePayments()->count()) {
                $trans = $order->invoicePayments()->first();
                $trans->update([
                    'items'     => OrderUtility::createPaymentItems($order->items, $order->total),
                ]);
            }

            if($order->paymentTransactions()->count()) {
                $trans = $order->paymentTransactions()->first();
                $trans->update([
                    'items'     => OrderUtility::createPaymentItems($order->items, $order->total),
                    'amount'    => $order->total
                ]);
            }

            event(new EmailAccountingCancellationEvent(
                $agent,
                Auth::user(),
                $params['amount_paid'],
                isset($params['deferred_half_price_fees']) ? $params['deferred_half_price_fees'] : 0 ,
                isset($params['deferred_goup_discount_fees']) ? $params['deferred_goup_discount_fees'] : 0 ,
                isset($params['additional_fees']) ? $params['additional_fees'] : 0,
                isset($params['deductions']) ? $params['deductions'] : 0,
                isset($params['total_fee']) ? $params['total_fee'] : 0 ,
                $params['refund'],
                $params['order_total'],
                $classRegistrationIds,
                $notify
            ));
        }
    }

    /**
     * @param $contactId
     * @param $classRegistrationId
     * @param $content
     * @return static
     */
    public function createCancellationNote($contactId, $classRegistrationId, $content)
    {
        $customerNote = CancellationNote::create([
            'customer_id' => $contactId,
            'class_registration_id' => $classRegistrationId,
            'content' => $content
        ]);

        return $customerNote;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function addAttendees($params = [])
    {
        $item = $params['item'];
        $order = $params['order'];
        $agent = $params['agent'];
        $registrationOrder = $params['registrationOrder'];
        $hasMilitaryDiscount = false;
        $classRegistrations = $this->classRegistrationRepo
                                   ->getByRegistrationIdNoSwapNoCancel($registrationOrder->id);
        $numRegistrations = $classRegistrations->count();
        $originalPrice = $registrationOrder->originalPrice();
        $status = $registrationOrder->status;

        if ($numRegistrations) {
            foreach ($classRegistrations as $classRegistration) {
                if ($classRegistration->classMilitaryDiscount) {
                    $hasMilitaryDiscount = true;
                }
            }
        }

        foreach ($params['attendees'] as $attendee) {
            $contactObject = Contact::find($attendee);

            if ($contactObject->user and $contactObject->user->has_military_discount) {
                $hasMilitaryDiscount = true;
            }
        }

        foreach ($params['attendees'] as $attendee) {
            $numRegistrations++;

            $paramsClass = [
                'registration_order_id' => $registrationOrder->id,
                'attendee_contact_id'   => $attendee,
                'item_charge'           => $originalPrice,
                'is_new'                => true
            ];

            $classRegistration = $this->registrationService->saveClassRegistration($paramsClass);

            if ($classRegistration->subscriptionDiscount) {
                $numRegistrations--;
            } else {

                $halfPriced = ($numRegistrations > 2 and
                    !$registrationOrder->isCBT() and
                    !CourseUtility::excludedClassHalfPriceRule($registrationOrder)) ? true : false;

                $apply_gsa_discount = ($hasMilitaryDiscount and !$halfPriced) ? true : false;

                $charge = ($apply_gsa_discount) ? apply_gsa_discount($originalPrice) : $originalPrice;

                $itemCharge = ($halfPriced) ? $charge / 2 : $charge;

                $classRegistration->item_charge = $itemCharge;
                $classRegistration->is_half_priced = $halfPriced;
                $classRegistration->save();

                if($apply_gsa_discount) {

                    DiscService::saveMilitaryDiscount([
                        'order_id' => $order->id,
                        'contact_id' => $attendee,
                        'class_reg_id' => $classRegistration->id,
                        'deduction' => $originalPrice - $charge
                    ]);

                }

            }

            if (!$classRegistration->subscriptionDiscount) {
                DiscService::checkDiscount($classRegistration, true, $status);
            }

            $this->registrationService->saveActingAgent($registrationOrder, $attendee, $params['status']);
            $this->saveOrderAdjustment([
                'agent_id' => $agent->id,
                'order_id' => $order->id,
                'order_item_id' => $item->id,
                'adjustable_id' => $classRegistration->id,
                'adjustable_type' => Constant::CLASS_REGISTRATION,
                'amount' => $classRegistration->item_charge,
                'adjustment_action' => Constant::ADDED,
                'note' => adjustment_registration_added(
                    $classRegistration,
                    $registrationOrder,
                    $classRegistration->item_charge
                )
            ]);
        }

        return $this;
    }


    /**
     * @param array $params
     * @return OrderAdjustment
     */
    public function saveOrderAdjustment($params = [])
    {
        $orderAdjustment = new OrderAdjustment();
        $orderAdjustment->fill($params);
        $orderAdjustment->save();

        return $orderAdjustment;
    }

    /**
     * @param $orderAdjustments
     * @param $status
     */
    public function updateStatusOfOrders($orderAdjustments, $status)
    {
        if ($orderAdjustments->count()) {
            $orderAdjustments->each(function($orderAdjustment) use ($status) {

                if ($orderAdjustment->item->status == Constant::IN_PROCESS) {
                    $orderAdjustment->item->status = $status;
                    $orderAdjustment->item->status->save();
                }

                $orderableObject  = $orderAdjustment->adjustable;

                switch ($orderAdjustment->adjustable_type) {
                    case Constant::CLASS_REGISTRATION :
                        $orderableObject = $orderAdjustment->adjustable->registrationOrder;

                        if ($orderAdjustment->adjustment_action == Constant::ADDED
                            and $status == Constant::COMPLETE) {
                            $orderItem = $orderableObject->orderItem;
                            $orderItem->save();
                        }

                        break;

                    case Constant::CLIENT_SUBSCRIPTION :
                        $orderableObject = $orderAdjustment->adjustable->yearlySubscription;
                        break;
                }

                if ($orderableObject) {
                    if ($orderableObject->status == Constant::IN_PROCESS) {
                        $orderableObject->status = $status;
                        $orderableObject->save();
                    }

                    $actingAgents = $this->actingOriginatingAgentRepo
                        ->getByOrderableAndAgent([
                            'orderable_id' => $orderableObject->id,
                            'orderable_type' => class_basename($orderableObject),
                            'status' => Constant::IN_PROCESS
                        ]);

                    if ($actingAgents->count()) {
                        $actingAgents->each(function ($actingAgent) use ($status) {
                            $actingAgent->status = $status;
                            $actingAgent->save();
                        });
                    }
                }
            });

            return true;
        }
    }

    /**
     * @param User $agent
     * @param Collection $orderAdjustments
     */
    public function updateStatusOfNewItems(User $agent, Collection $orderAdjustments)
    {
        if ($orderAdjustments->count()) {
            $classRegistrations = collect();

            foreach ($orderAdjustments as $orderAdjustment)

                if ($orderAdjustment->adjustable_type == Constant::CLASS_REGISTRATION) {
                    $orderAdjustment->adjustable->is_new = false;
                    $orderAdjustment->adjustable->save();

                    $classRegistrations->push($orderAdjustment->adjustable);
                }

            if ($classRegistrations->count()) {
                event(new SubscriptionUsedEvent($agent, $classRegistrations));
            }
        }
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function changePrice($params = [])
    {
        $params['amount'] = null;
        $params['adjustment_action'] = null;

        switch ($params['adjustable_type']) {
            case Constant::CLASS_REGISTRATION :
                $classRegistration = $this->classRegistrationRepo->getById($params['adjustable_id']);
                $classRegistration->item_charge = $params['new_price'];
                $classRegistration->save();

                $params['note'] = adjustment_registration_change_price($classRegistration, $classRegistration->registrationOrder);
                break;

            case Constant::PRODUCT_ORDERS :
                $productOrder = ProductOrders::findOrFail($params['adjustable_id']);
                $order = $productOrder->orderItem->order;

                $originalPaidAmount = $productOrder->paid_quantity * $productOrder->price;
                $newAmount = $productOrder->paid_quantity * $params['new_price'];

                if ($originalPaidAmount > $newAmount) {
                    $params['adjustment_action'] = Constant::DEDUCTED;
                    $params['amount'] = $originalPaidAmount - $newAmount;
                } else {
                    $params['adjustment_action'] = Constant::ADDED;
                    $params['amount'] = $newAmount - $originalPaidAmount;
                }

                $productOrder->price = $params['new_price'];
                $productOrder->save();
                $params['note'] = adjustment_product_change_price($productOrder);
                break;

            case Constant::CORPORATE_SEAT :
                $item = ClientCorporateSeatCredit::findOrFail($params['adjustable_id']);

                $originalPaidAmount = $item->paid_price;
                $newAmount = $params['new_price'];

                if ($originalPaidAmount > $newAmount) {
                    $params['adjustment_action'] = Constant::DEDUCTED;
                    $params['amount'] = $originalPaidAmount - $newAmount;
                } else {
                    $params['adjustment_action'] = Constant::ADDED;
                    $params['amount'] = $newAmount - $originalPaidAmount;
                }

                $item->price = $params['new_price'];
                $item->save();
                $params['note'] = $text = 'Corporate Seat price changed.';
                break;
            case Constant::YEARLY_SUBSCRIPTION :
                $item = ClientYearlySubscription::findOrFail($params['adjustable_id']);

                $originalPaidAmount = $item->paid_price;
                $newAmount = $params['new_price'];

                if ($originalPaidAmount > $newAmount) {
                    $params['adjustment_action'] = Constant::DEDUCTED;
                    $params['amount'] = $originalPaidAmount - $newAmount;
                } else {
                    $params['adjustment_action'] = Constant::ADDED;
                    $params['amount'] = $newAmount - $originalPaidAmount;
                }

                $item->temporary_price = $params['new_price'];
                $item->save();
                $params['note'] = $text = 'Yearly Subscription price changed.';
                break;
        }


        $params['amount'] = is_null($params['amount'])
                            ? abs($params['original_price'] - $params['new_price'])
                            : $params['amount'];

        $params['adjustment_action'] = is_null($params['adjustment_action'])
                                        ? ($params['original_price'] > $params['new_price']
                                            ? Constant::DEDUCTED
                                            : Constant::ADDED)
                                        : $params['adjustment_action'];

        $orderAdjustment = self::saveOrderAdjustment($params);

        return $orderAdjustment;
    }

    /**
     * @param Collection $orderAdjustments
     * @param $status
     */
    public function createAdjustmentDiscountOnDeduction(Collection $orderAdjustments, $status)
    {
        if ($orderAdjustments->count()) {
            $orderAdjustments->each(function($orderAdjustment) use ($status) {
                if ($orderAdjustment->adjustment_action == Constant::DEDUCTED) {
                    OrderAdjustmentDiscount::create([
                        'order_id' => $orderAdjustment->order_id,
                        'adjustable_id' => $orderAdjustment->adjustable_id,
                        'adjustable_type' => $orderAdjustment->adjustable_type,
                        'adjustment_notes' => $orderAdjustment->note,
                        'deduction' => $orderAdjustment->amount,
                        'status' => $status,
                        'is_deleted_by_credit' => false,
                    ]);
                }
            });
        }
    }

    /**
     * @param array $params
     */
    public function cancelNewClassRegistrations($params = [])
    {
        $classRegistrations = $this->classRegistrationRepo->getByIds($params['class_registration_ids']);

        ModDiscountService::removeDiscountsCancelledClassRegistrations(
            $this->classRegistrationRepo->getByIds($params['class_registration_ids'])
        );

        if ($classRegistrations->count()) {
            $classRegistrations->each(function($classRgistration) {
                self::removeAdjustments($classRgistration);
                $classRgistration->forceDelete();
            });
        }
    }

    /**
     * @param ClassRegistration $classRegistration
     */
    public function removeAdjustments(ClassRegistration $classRegistration)
    {
        $orderAdjustments = $this->orderAdjustmentRepo
                                 ->getByAdjustable($classRegistration->id, Constant::CLASS_REGISTRATION);

        if ($orderAdjustments->count()) {
            $orderAdjustments->each(function($orderAdjustment) {
                $orderAdjustment->delete();
            });
        }
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function changeAdjustmentPrice($params = [])
    {
        $orderAdjustment = $this->orderAdjustmentRepo->getById($params['adjustment_id']);
        $originalPrice = $orderAdjustment->amount;
        $newPrice = $params['new_price'];

        switch ($orderAdjustment->adjustable_type) {
            case Constant::CLASS_REGISTRATION:
                $classRegistration = $orderAdjustment->adjustable;

                if ($orderAdjustment->adjustment_action == Constant::ADDED) {
                    $classRegistration->item_charge -= $originalPrice;
                    $classRegistration->save();

                    $classRegistration->item_charge += $newPrice;
                    $classRegistration->save();
                }

                else {
                    $classRegistration->item_charge += $originalPrice;
                    $classRegistration->save();

                    $classRegistration->item_charge -= $newPrice;
                    $classRegistration->save();
                }

                break;


        }

        $orderAdjustment->amount = $newPrice;
        $orderAdjustment->save();

        return $orderAdjustment;
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function removeAdjustment($params = [])
    {
        $orderAdjustment = $this->orderAdjustmentRepo->getById($params['adjustment_id']);

        if ($orderAdjustment) {

            switch ($orderAdjustment->adjustable_type) {
                case Constant::CLASS_REGISTRATION:
                    $classRegistration = $orderAdjustment->adjustable;

                    if ($classRegistration->is_new) {
                        self::removeAdjustments($classRegistration);
                        $classRegistration->forceDelete();
                    } else {
                        if ($orderAdjustment->adjustment_action == Constant::ADDED) {
                            $classRegistration->item_charge -= $orderAdjustment->amount;
                        } else {
                            $classRegistration->item_charge += $orderAdjustment->amount;

                            if ($orderAdjustment->is_corporate_discount_adjustment) {
                                $agent = $orderAdjustment->agent;
                                $classCreditDiscount = $this->classCreditDiscountRepo->getByClassRegId($classRegistration->id);

                                if ($classCreditDiscount) {
                                    $classCreditDiscount->delete();
                                }

                                $agent->corporate_seat_credits += 1;
                                $agent->save();

                                $classRegistration->corporate_seat_credit_applied = false;
                            }
                        }


                        $orderAdjustment->delete();
                        $classRegistration->save();
                    }

                    break;

                case Constant::PRODUCT_ORDERS:

                    $productOrder = $orderAdjustment->adjustable;

                    if ($orderAdjustment->is_product_qty_adjustment) {
                        $productOrder->quantity = $productOrder->paid_quantity;
                    } else {

                        if ($orderAdjustment->adjustment_action == Constant::ADDED) {
                            $productOrder->price -= ($orderAdjustment->amount / $productOrder->paid_quantity);
                        } else {
                            $productOrder->price += ($orderAdjustment->amount / $productOrder->paid_quantity);
                        }
                    }

                    $orderAdjustment->delete();
                    $productOrder->save();

                    break;
            }

            return $orderAdjustment;
        }
    }

    /**
     * @param Collection $orderAdjustments
     */
    public function createAdjustmentHistoryFromItemAdjustments(Collection $orderAdjustments)
    {
        if ($orderAdjustments->count()) {
            $orderAdjustments->each(function($orderAdjustment) {
                self::createOrderAdjustmentHistory($orderAdjustment->getAttributes());
            });
        }
    }

    /**
     * @param array $params
     */
    public function createOrderAdjustmentHistory($params = [])
    {
        $orderAdjustmentHistory = new OrderAdjustmentHistory();
        $orderAdjustmentHistory->fill($params);
        $orderAdjustmentHistory->save();
    }

    /**
     * @param User $user
     * @param ClassRegistration $classRegistration
     * @param RegistrationOrder $registrationOrder
     */
    public function createAdjustmentFromRestoration(User $user,
                                                    ClassRegistration $classRegistration,
                                                    RegistrationOrder $registrationOrder)
    {
        $orderItem = $this->orderItemRepo->getByOrderable($registrationOrder->id, Constant::REGISTRATION_ORDER, false);

        self::saveOrderAdjustment([
            'agent_id' => $user->id,
            'order_id' => $orderItem->order->id,
            'order_item_id' => $orderItem->id,
            'adjustable_id' => $classRegistration->id,
            'adjustable_type' => Constant::CLASS_REGISTRATION,
            'amount' => $classRegistration->item_charge,
            'adjustment_action' => Constant::ADDED,
            'note' => adjustment_registration_restore($classRegistration, $registrationOrder)
        ]);
    }

    /**
     * @param Collection $orderAdjustments
     */
    public function updatePaidPriceOfProduct(Collection $orderAdjustments)
    {
        if ($orderAdjustments->count()) {

            foreach ($orderAdjustments as $orderAdjustment) {

                if ($orderAdjustment->adjustable_type == Constant::PRODUCT_ORDERS and
                    $orderAdjustment->is_product_qty_adjustment) {
                    $productOrder = ProductOrders::find($orderAdjustment->adjustable_id);

                    if ($productOrder) {
                        $productOrder->paid_quantity = $productOrder->quantity;
                        $productOrder->save();
                    }
                }
            }
        }
    }

    /**
     * @param array $params
     * @param User $agent
     * @param RegistrationOrder $registrationOrder
     */
    public function createBalanceDue($params = [], User $agent, RegistrationOrder $registrationOrder)
    {
        $amountPaid = $params['amount_paid'];
        $halfPriceFee =  isset($params['deferred_half_price_fees']) ? $params['deferred_half_price_fees'] : 0;
        $groupDiscountFee = isset($params['deferred_goup_discount_fees']) ? $params['deferred_goup_discount_fees'] : 0;
        $restrictionFee = isset($params['total_fee']) ? $params['total_fee'] : 0;
        $difference = $amountPaid - $halfPriceFee - $groupDiscountFee - $restrictionFee;
        $orderItem = $registrationOrder->orderItem;
        $order = $orderItem->order;

        if ($difference < 0) {
            $this->saveOrderAdjustment([
                'agent_id' => $agent->id,
                'order_id' => $order->id,
                'order_item_id' => $orderItem->id,
                'adjustable_id' => null,
                'adjustable_type' => null,
                'amount' => abs($difference),
                'adjustment_action' => Constant::ADDED,
                'note' => adjustment_registration_cancellation_fee(isset($params['total_fee']))
            ]);
        }
    }
}