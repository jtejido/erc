<?php namespace App\Services;

use Session;
use App\Models\Order;
use App\Models\InvoicedPayment;
use App\Models\InvoicedPaymentRecipient;
use App\Models\OrderAdjustment;
use App\Repositories\OrderAdjustmentRepository;
use App\Facades\OrderModificationService;
use App\Facades\PhotoUtility;
use App\Utilities\Constant;
use App\Facades\OrderService;
use App\Events\EmailAccountingModificationConfirmationEvent;

class OrderModificationCheckoutService
{

    /**
     * @var AuthorizeService
     */
    private $authorize;

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @var InvoiceService
     */
    private $invoiceService;

    /**
     * @param AuthorizeService $authorize
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     * @param PaymentService $paymentService
     * @param InvoiceService $invoiceService
     */
    public function __construct(AuthorizeService $authorize,
                                OrderAdjustmentRepository $orderAdjustmentRepo,
                                PaymentService $paymentService,
                                InvoiceService $invoiceService)
    {
        $this->authorize = $authorize;
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
        $this->paymentService = $paymentService;
        $this->invoiceService = $invoiceService;
    }

    /**
     * @param array $paymentInfo
     * @return mixed
     */
    public function doCheckoutCreditCard($paymentInfo = [])
    {

        $notifyAgents = (array_key_exists('doNotEmail', $paymentInfo)) ? false : true;
        $skipEmail = (array_key_exists('skip-email-notification', $paymentInfo) &&
            $paymentInfo['skip-email-notification'] == 1) ? true : false;

        try {

            $updatedStatus = false;
            $errors = [];
            $paymentInfo['card_number'] = preg_replace('/\s+/', '', $paymentInfo['card_number']);
            $shipping_rates = ( isset($paymentInfo['shipping_rates']) AND  strlen($paymentInfo['shipping_rates'])>0 )? (float) $paymentInfo['shipping_rates']:0.00;

            $charge = $this->authorize
                ->chargeCreditCard(
                    $paymentInfo['card_number'],
                    $paymentInfo['cvv'],
                    $paymentInfo['card_exp']
                );

            $mask_number = function ($number, $maskingCharacter = 'X') {
                return str_repeat($maskingCharacter, (strlen($number) - 8) < 0 ? 0 : (strlen($number) - 8)) . substr($number, -4);
            };

            $masked_number = $mask_number($paymentInfo['card_number']);

            unset($paymentInfo['card_number']);

            $order = $this->authorize->orderInfo($paymentInfo['description']);
            $items = $this->orderAdjustmentRepo->getByIds($paymentInfo['items']);

            $customerInfo = $this->authorize->customerInfo([
                'email'         => $paymentInfo['email'],
                'first_name'    => $paymentInfo['first_name'],
                'last_name'     => $paymentInfo['last_name'],
                'company'       => $paymentInfo['billing_company'],
                'address'       => $paymentInfo['billing_address1'],
                'city'       => $paymentInfo['billing_city'],
                'state'       => $paymentInfo['billing_state'],
                'zip'           => $paymentInfo['billing_zip']
            ]);

            $response = $this->authorize->createTransaction([
                'amount'        => $paymentInfo['amount'],
                'shipping_rates' => $shipping_rates,
                'payment'       => $charge['payment'],
                'order'         => $order,
                'items'         => $items,
                'customer'      => $customerInfo['customer'],
                'billTo'        => $customerInfo['billTo'],
                'auth'          => $charge['auth'],
                'ref'           => $charge['ref'],
                'is_order_modification' => true
            ]);

            if (!is_null($response)) {
                $tresponse = $response->getTransactionResponse();

                if ($tresponse) {
                    collect($tresponse->getErrors())->each(function ($_error, $key) use (&$errors) {
                        $errors[] = $_error->getErrorText();
                    });
                }

                if ($response->getMessages() && $response->getMessages()->getResultCode() == 'Error') {
                    array_push($errors, 'An error occurred while processing your payment.');

                    $errorMessages = $response->getMessages()->getMessage();

                    if (isset($errorMessages[0]) and $errorMessages[0]->getCode() == 'E00003') {
                        array_push($errors, 'Address is to long.');
                    }

                    else {
                        array_push($errors, 'Credit Card is invalid.');
                    }

                    throw new \Exception(implode(' ', $errors));
                }

                if (!is_null($tresponse) and $tresponse->getResponseCode() == '1' and empty($errors)) {

                    $orderObject = Order::find($paymentInfo['order_id']);
                    event(new EmailAccountingModificationConfirmationEvent($paymentInfo['items'], $orderObject->user, [
                        'method' => 'cc',
                        'object' =>  null
                    ], $notifyAgents));

                    OrderModificationService::updateStatusOfOrders($items, Constant::COMPLETE);
                    OrderModificationService::updateStatusOfNewItems($orderObject->user, $items);
                    OrderModificationService::createAdjustmentDiscountOnDeduction($items, Constant::COMPLETE);
                    OrderModificationService::createAdjustmentHistoryFromItemAdjustments($items);
                    OrderModificationService::updatePaidPriceOfProduct($items);
                    $orderObject->updateOrderTax();
                    OrderService::updateOrderTotal($orderObject);
                    OrderService::updatePaidOrderTotal($orderObject);
                    OrderService::updatePaidCharge($orderObject);

                    OrderAdjustment::destroy($paymentInfo['items']);

                    Session::flash('PAYMENT_SUCCESS', true);

                    return $this->paymentService->createPaymentTransaction([
                        'email'             => $paymentInfo['email'],
                        'card_brand'        => $tresponse->getAccountType(),
                        'auth_code'         => $tresponse->getAuthCode(),
                        'transaction_id'    => $tresponse->getTransId(),
                        'masked_cc'         => $masked_number,
                        'amount'            => $paymentInfo['amount'],
                        'shipping_rates'    => $shipping_rates,
                        'state_tax'         => $paymentInfo['sales_tax'],
                        'status'            => true,
                        'order_id'          => $orderObject->id,
                        'items'             => json_encode($this->createPaymentItems($items,  $paymentInfo['amount'])),
                        'billing_company'           => $paymentInfo['billing_company'],
                        'billing_address'           => $paymentInfo['billing_address1'],
                        'billing_city'              => $paymentInfo['billing_city'],
                        'billing_state'             => $paymentInfo['billing_state'],
                        'billing_zip'               => $paymentInfo['billing_zip'],
                        'shipping_company'           => $paymentInfo['shipment_company'],
                        'shipping_address'           => $paymentInfo['shipment_address1'],
                        'shipping_city'              => $paymentInfo['shipment_city'],
                        'shipping_state'             => $paymentInfo['shipment_state'],
                        'shipping_zip'               => $paymentInfo['shipment_zip'],
                        'trans_total'               => $orderObject->total,
                    ]);
                }
            }

            throw new \Exception(implode(' ', $errors));

        } catch (\Exception $e) {
            return Session::flash('error_message', $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return InvoicedPayment
     */
    public function doCheckoutInvoice($params = [])
    {

        Session::flash('PAYMENT_SUCCESS', true);
        $notify = (array_key_exists('doNotEmailInvoice', $params)) ? false : true;

        $order = Order::findOrFail($params['order_id']);

        if (isset($params['po_file']) or isset($params['check_file']) or
            isset($params['po_number']) or isset($params['check_number']) or
            isset($param['recipients'])) {

            $invoicedPayment = new InvoicedPayment();


            $invoicedPayment->order_id = $order->id;


            $poName = isset($params['po_file'])
                ? Constant::PO_FILE
                : null;

            $checkName = isset($params['check_file'])
                ? Constant::CHECK_FILE
                : null;


            if (isset($params['po_file'])) {
                $poResponse = $this->invoiceService->uploadInvoice(
                    PhotoUtility::getFilePathByOrderInvoiceId($params['order_id'] . '/' . $invoicedPayment->id),
                    $params['po_file'],
                    $poName
                );

                $poName = $poResponse['image'];
                $this->invoiceService->removeFiles(Constant::FILE_TEMP);
            }

            if (isset($params['check_file'])) {
                $checkResponse = $this->invoiceService->uploadInvoice(
                    PhotoUtility::getFilePathByOrderInvoiceId($params['order_id'] . '/' . $invoicedPayment->id),
                    $params['check_file'],
                    $checkName
                );

                $checkName = $checkResponse['image'];
                $this->invoiceService->removeFiles(Constant::FILE_TEMP);
            }

            $invoicedPayment->po_number = isset($params['po_number']) ? $params['po_number'] : null;
            $invoicedPayment->check_number = isset($params['check_number']) ? $params['check_number'] : null;
            $invoicedPayment->po_image_mime = isset($poResponse['mime']) ? $poResponse['mime'] : null;
            $invoicedPayment->check_image_mime = isset($checkResponse['mime']) ? $checkResponse['mime'] : null;

            if ($poName) {
                $invoicedPayment->po_image = $poName;
                $invoicedPayment->po_file = $poResponse['pdf'];
            }

            if ($checkName) {
                $invoicedPayment->check_image = $checkName;
                $invoicedPayment->check_file = $checkResponse['pdf'];
            }

            $invoicedPayment->billing_company = $params['billing_company'];
            $invoicedPayment->billing_address = $params['billing_address1'];
            $invoicedPayment->billing_city = $params['billing_city'];
            $invoicedPayment->billing_state = $params['billing_state'];
            $invoicedPayment->billing_zip = $params['billing_zip'];

            $invoicedPayment->shipping_company = $params['shipment_company'];
            $invoicedPayment->shipping_address = $params['shipment_address1'];
            $invoicedPayment->shipping_city = $params['shipment_city'];
            $invoicedPayment->shipping_state = $params['shipment_state'];
            $invoicedPayment->shipping_zip = $params['shipment_zip'];
            $invoicedPayment->trans_total = $order->total;
            $invoicedPayment->shipping_rate = $order->shipping_rates;
            $invoicedPayment->state_tax = $params['sales_tax'];

            $invoicedPayment->save();

            if ($params['recipients']) {
                foreach ($params['recipients'] as $recipient) {
                    InvoicedPaymentRecipient::create([
                        'invoiced_payment_id' => $invoicedPayment->id,
                        'email' => $recipient
                    ]);
                }
            }

            $orderObject = Order::find($params['order_id']);
            $items = $this->orderAdjustmentRepo->getByIds($params['items']);
            event(new EmailAccountingModificationConfirmationEvent($params['items'], $orderObject->user, [
                'method' => 'invoice',
                'object' => $invoicedPayment
            ], $notify));

            OrderModificationService::updateStatusOfOrders($items, Constant::INVOICED);
            OrderModificationService::updateStatusOfNewItems($orderObject->user, $items);
            OrderModificationService::createAdjustmentDiscountOnDeduction($items, Constant::INVOICED);
            OrderModificationService::createAdjustmentHistoryFromItemAdjustments($items);
            OrderModificationService::updatePaidPriceOfProduct($items);
            $orderObject->updateOrderTax();
            OrderService::updateOrderTotal($orderObject);
            OrderService::updatePaidCharge($orderObject);

            OrderAdjustment::destroy($params['items']);

            return $invoicedPayment;
        }

        return true;
    }

    /**
     * @param $items
     * @param $totalAmount
     * @return object
     */
    private function createPaymentItems($items, $totalAmount)
    {
        $data = (object) [];
        $itemObjects = [];

        $data->total_amount = $totalAmount;

        if ($items->count()) {
            foreach ($items as $item) {
                if ($item->adjustment_action == Constant::ADDED) {
                    $itemObjects[] = (object) [
                        'id' => $item->id,
                        'object' => Constant::ORDER_ADJUSTMENT_OBJECT,
                        'adjustment_object' => $item->adjustable_type,
                        'adjustment_id' => $item->adjustable_id,
                        'contents' => $this->getItemsFromAdjustment($item)
                    ];
                }
            }

            $data->items = $itemObjects;
        }

        return $data;
    }

    /**
     * @param $item
     * @return array
     */
    private function getItemsFromAdjustment($item)
    {
        $content = [];

        switch (class_basename($item->adjustable_type)) {
            case Constant::CLASS_REGISTRATION:
                $pos = strpos($item->note, '$');
                $content[] = (object) [
                    'price_paid' => $item->amount,
                    'description' => substr($item->note, 0, $pos - 1)
                ];

                break;
            case Constant::PRODUCT_ORDERS:
                $pos = strpos($item->note, '$');
                $content[] = (object) [
                    'price_paid' => $item->amount,
                    'description' => substr($item->note, 0, $pos - 1)
                ];

                break;

            default:
                $pos = strpos($item->note, '$');
                $content[] = (object) [
                    'price_paid' => $item->amount,
                    'description' => substr($item->note, 0, $pos - 1)
                ];

                break;

        }

        return $content;
    }
}