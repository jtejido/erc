<?php namespace App\Services;

use App\Events\SubscriptionUsedEvent;
use App\Facades\OrderUtility;
use App\Models\InvoicedPayment;
use App\Models\PaymentTransaction;
use App\Models\User;
use Auth;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\RegistrationOrder;
use App\Models\YearlySubscription;
use App\Models\ActingOriginatingAgent;
use App\Models\ClassRegistration;
use App\Models\OrderTransaction;
use App\Models\ProductOrders;
use App\Models\ClientCorporateSeatCredit;
use App\Repositories\ActingOriginatingAgentRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\OrderRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\YearlySubscriptionRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\ClassCreditDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\RemovedDiscountByCreditRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use PDF;
use iio\libmergepdf\Merger;
use Illuminate\Support\Facades\Storage;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var YearlySubscriptionRepository
     */
    private $yearlySubscriptionRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var ClassCombinationRepository
     */
    private $classCombinationRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var ClassHalfPriceDiscountRepository
     */
    private $classHalfPriceDiscountRepo;

    /**
     * @var ClassCouponDiscountRepository
     */
    private $classCouponDiscountRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var ClassCreditDiscountRepository
     */
    private $classCreditDiscountRepo;

    /**
     * @var
     */
    private $orderAdjustmentDiscountRepo;

    /**
     * @var RemovedDiscountByCreditRepository
     */
    private $removedDiscountByCreditRepo;

    /**
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param YearlySubscriptionRepository $yearlySubscriptionRepo
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param RemovedDiscountByCreditRepository $removedDiscountByCreditRepo
     */
    public function __construct(OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                YearlySubscriptionRepository $yearlySubscriptionRepo,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo,
                                ClassCombinationRepository $classCombinationRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                ClassCreditDiscountRepository $classCreditDiscountRepo,
                                OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                RemovedDiscountByCreditRepository $removedDiscountByCreditRepo)
    {
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->yearlySubscriptionRepo = $yearlySubscriptionRepo;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
        $this->classCombinationRepo = $classCombinationRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->classHalfPriceDiscountRepo = $classHalfPriceDiscountRepo;
        $this->classCouponDiscountRepo = $classCouponDiscountRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->classCreditDiscountRepo = $classCreditDiscountRepo;
        $this->orderAdjustmentDiscountRepo = $orderAdjustmentDiscountRepo;
        $this->removedDiscountByCreditRepo = $removedDiscountByCreditRepo;
    }

    /**
     * Create a new order
     *
     * @param $model
     * @return $this
     * @throws \Exception
     */
    public function addOrder($model)
    {
        try {

            $existingOrder = $this->orderRepo
                ->getByUserId($model->originating_agent_user_id);

            if (!$existingOrder ||
                class_basename($model) == Constant::ONSITE_TRAINING) {

                $existingOrder = Order::create([
                    'user_id'       => $model->originating_agent_user_id,
                    'user_csr_id'   => !Auth::user()->isMember() ? Auth::user()->id : NULL,
                    'status'        => class_basename($model) == Constant::ONSITE_TRAINING
                                       ? Constant::COMPLETE
                                       : Constant::PENDING,
                    'completed_at'  => class_basename($model) == Constant::ONSITE_TRAINING
                                        ? Carbon::now()
                                        : '0000-00-00 00:00:00'
                ]);
            }

            $this->addItem($existingOrder, $model);

            if (class_basename($model) == Constant::ONSITE_TRAINING) {
                $this->createOrderTransaction([
                    'order_id'          => $existingOrder->id,
                    'transaction_id'    => generate_transaction_id($existingOrder)
                ]);
            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Create a new order instance
     *
     * @param Order $order
     * @param $model
     * @return $this
     * @throws \Exception
     */
    public function addItem(Order $order, $model)
    {
        try {

            OrderItem::create([
                'order_id'          => $order->id,
                'orderable_id'      => $model->id,
                'orderable_type'    => class_basename($model),
                'status'            => class_basename($model) == Constant::ONSITE_TRAINING
                                       ? Constant::COMPLETE
                                       : Constant::PENDING
            ]);

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Update the price charge of item
     *
     * @param $model
     * @return mixed
     * @throws \Exception
     */
    public function setPriceCharged($model)
    {
        try {
            $orderItem = $this->orderItemRepo
                              ->getByOrderable(
                                  $model->id,
                                  class_basename($model),
                                  false
                              );

            $orderItem->price_charged = $this->getPriceCharged($model);
            $orderItem->save();

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Get total charge by order type
     *
     * @param $model
     * @return OrderService|int|void
     */
    private function getPriceCharged($model)
    {
        $charge = 0;

        switch(class_basename($model)) {

            case Constant::REGISTRATION_ORDER :
                $charge = $this->getChargeByRegistration($model->classRegistrations);
                break;

            case Constant::YEARLY_SUBSCRIPTION :
                $charge = $this->getChargeByYearlySubs($model->clientSubscriptions);
                $model->total_price = $charge;
                $model->save();
                break;

            case Constant::PRODUCT_ORDERS :
                $charge = $model->getProductOrderPrice(null, 0, null);
                break;

            case Constant::CORPORATE_SEAT ||
                 Constant::ONSITE_TRAINING :
                $charge = $model->price;
                break;

            default:
                break;
        }

        return $charge;
    }

    /**
     * @param Collection $classRegistrations
     * @return int
     */
    private function getChargeByRegistration(Collection $classRegistrations)
    {
        $total = 0.00;

        if ($classRegistrations->count()) {
            $total = $classRegistrations->sum('item_charge');
        }

        return $total;
    }

    /**
     * @param array $clientSubscriptions
     * @return int
     */
    private function getChargeByYearlySubs($clientSubscriptions = [])
    {
        $total = 0.00;

        if (count($clientSubscriptions)) {

            foreach ($clientSubscriptions as $clientSubscription) {
                $total += $clientSubscription->temporary_price;
            }
        }

        return $total;
    }

    /**
     * @param OrderItem $item
     * @return $this
     */
    public function removeSubItems(OrderItem $item)
    {
        switch ($item->orderable_type) {

            case Constant::REGISTRATION_ORDER :
                if ($item->orderable) {
                    $this->removeRegistrations($item->orderable);
                }

                break;

            case Constant::YEARLY_SUBSCRIPTION :
                $this->removeSubscriptions($item->orderable);
                break;

            case Constant::CORPORATE_SEAT :
                $this->removeCorpSeats($item->orderable);
                break;

            case Constant::PRODUCT_ORDERS :
                $this->removeProductOrders($item->orderable);
                break;
        }

        return $this;
    }

    /**
     * @param RegistrationOrder $registrationOrder
     * @return $this
     * @throws \Exception
     */
    private function removeRegistrations(RegistrationOrder $registrationOrder)
    {
        $registration = $this->registrationOrderRepo
                             ->getById($registrationOrder->id);

        $this->removeActingAgents($registration);
        $registration->forceDelete();

        return $this;
    }

    /**
     * @param YearlySubscription $yearlySubscription
     * @return $this
     * @throws \Exception
     */
    private function removeSubscriptions(YearlySubscription $yearlySubscription)
    {
        $subscription = $this->yearlySubscriptionRepo->getById($yearlySubscription->id);

        $this->removeActingAgents($subscription);
        $subscription->delete();

        return $this;
    }

    /**
     * @param ClientCorporateSeatCredit $corporateSeatCredit
     * @return $this
     * @throws \Exception
     */
    private function removeCorpSeats(ClientCorporateSeatCredit $corporateSeatCredit)
    {
        $corporateSeatCredit->delete();

        return $this;
    }

    /**
     * @param $registrable
     * @return $this
     */
    private function removeActingAgents($registrable)
    {
        $actingAgents = $this->actingOriginatingAgentRepo
                             ->getByOrderableAndAgent([
                                    'orderable_id'      => $registrable->id,
                                    'orderable_type'    => class_basename($registrable),
                                    'status'            => Constant::PENDING
                             ]);

        if ($actingAgents) {
            $ids = $actingAgents->pluck('id')->toArray();
            ActingOriginatingAgent::destroy($ids);
        }


        return $this;
    }

    /**
     * Remove order if no item
     *
     * @param OrderItem $item
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function checkRemovedItem(OrderItem $item)
    {
        try {
            $order = $this->orderRepo->getById($item->order_id);

            if ($order and !$order->items->count()) {

                if (OrderUtility::isCompleteOrInvoiced($order)) {
                    $order->delete();
                } else {
                    $order->forceDelete();
                }
            }

            return $order;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $params
     */
    private function createOrderTransaction($params = [])
    {
        $transaction = new OrderTransaction();
        $transaction->fill($params);
        $transaction->save();
    }

    /**
     * Remove registration order if no class registrations
     *
     * @param ClassRegistration $classRegistration
     */
    public function checkRemovedClassRegistrations(ClassRegistration $classRegistration)
    {
        $registrationOrder = $classRegistration->registrationOrder;

        if (!$registrationOrder->classRegistrations->count()) {
            $classCombination = $this->classCombinationRepo
                                     ->getBySingeRegId($registrationOrder->id);

            if ($classCombination) {
                $classCombination->delete();
            }
        }
    }

    /**
     * Update the status of item objects
     *
     * @param $orderItems
     * @param $status
     * @return boolean
     */
    public function updateStatusOfOrders($orderItems, $status, $tax_rate = 0)
    {
        if ($orderItems->count()) {
            $orderItems->each(function($orderItem) use ($status, $tax_rate) {
                $orderItem->status = $status;

                if($orderItem->orderable_type == 'ProductOrders') {
                    $orderItem->state_tax = $tax_rate;
                }

                $orderItem->save();

                $orderableObject = $orderItem->orderable;
                $orderableObject->status = $status;
                $orderableObject->save();

                $actingAgents = $this->actingOriginatingAgentRepo
                                     ->getByOrderableAndAgent([
                                         'orderable_id' => $orderItem->orderable_id,
                                         'orderable_type' => $orderItem->orderable_type
                                     ]);

                if ($actingAgents->count()) {
                    $actingAgents->each(function($actingAgent) use ($status) {
                        $actingAgent->status = $status;
                        $actingAgent->save();
                    });
                }
            });

            return true;
        }
    }

    public function removeProductOrders(ProductOrders $productOrders)
    {
        if (OrderUtility::isCompleteOrInvoiced($productOrders->orderItem)) {
            $productOrders->delete();
        } else {
            $productOrders->forceDelete();
        }

        return $this;
    }

    /**
     * @param Order $order
     */
    public function updateOrderTotal(Order $order)
    {
        $items = $order->items;

        if ($items->count()) {
            $total = 0;

            foreach ($items as $item) {
                $total += $item->price_charged;
            }

            if(!empty($order->shipping_rates))
                $total += $order->shipping_rates;

            if(!empty($order->order_tax))
                $total += $order->order_tax;

            $order->total = $total;
            $order->save();
        }
    }

    /**
     * @param Order $order
     */
    public function updatePaidOrderTotal(Order $order)
    {
        $items = $order->items;

        if ($items->count()) {
            $total = 0;

            foreach ($items as $item) {
                $total += $item->price_charged;
            }

            if(!empty($order->shipping_rates))
                $total += $order->shipping_rates;

            if(!empty($order->order_tax))
                $total += $order->order_tax;

            $order->paid_total = $total;

            $order->save();
        }
    }

    /**
     * @param Order $order
     */
    public function updatePaidCharge(Order $order)
    {
        $items = $order->items;

        if ($items->count()) {
            $items->each(function($item) {

                switch ($item->orderable_type) {

                    case Constant::REGISTRATION_ORDER:
                        $registrationOrder = $item->orderable;
                        $classRegistrations = $registrationOrder->classRegistrations;

                        $classRegistrations->each(function($classRegistration) {
                            $classRegistration->paid_charge = $classRegistration->item_charge;
                            $classRegistration->save();
                        });

                        break;

                    case Constant::YEARLY_SUBSCRIPTION:
                        $yearlySubscription = $item->orderable;
                        $clientYearlySubs = $yearlySubscription->clientSubscriptions;

                        $clientYearlySubs->each(function($clientYearlySub) {
                            $clientYearlySub->paid_price = $clientYearlySub->temporary_price;
                            $clientYearlySub->save();
                        });

                        break;

                    case Constant::CORPORATE_SEAT:
                        $clientCredit = $item->orderable;

                        $clientCredit->paid_price = $clientCredit->price;
                        $clientCredit->save();

                        break;

                    case Constant::PRODUCT_ORDERS:
                        $productOrder = $item->orderable;

                        $productOrder->paid_price = $item->price_charged;
                        $productOrder->save();
                        break;
                }
            });
        }
    }

    public function deleteClassRegistration(RegistrationOrder $registrationOrder)
    {
        foreach ($registrationOrder->classRegistrations as $classRegistration) {
            $classRegistration->delete();
        }
    }

    /**
     * @param User $agent
     * @param Collection $items
     */
    public function checkIfSubscriptionUsed(User $agent, Collection $items, $notify_agents)
    {
        $classRegistrations = collect();

        foreach ($items as $item) {

            if ($item->orderable_type == Constant::REGISTRATION_ORDER) {
                $registrationOrder = $item->orderable;

                foreach ($registrationOrder->classRegistrations as $classRegistration) {
                    $classRegistrations->push($classRegistration);
                }
            }
        }

        if ($classRegistrations->count()) {
            event(new SubscriptionUsedEvent($agent, $classRegistrations, $notify_agents));
        }
    }

    /**
     * @param Order $order
     * @param User $user
     * @return string
     * @throws \iio\libmergepdf\Exception
     */
    public function generateReceipts(Order $order, User $user)
    {
        $m = new Merger();
        $files = [];

        $invoicedPayments = $order->invoicePayments()->whereNotNull('items')->get();

        if ($order->paymentTransactions->count()) {

            foreach ($order->paymentTransactions as $paymentTransaction) {

                $filename = 'receipt_' . $paymentTransaction->id . '.pdf';
                $path = 'receipts/' . $filename;
                $pdf = $this->generateSingleReceipt($order, $paymentTransaction, $user);

                Storage::put(
                    $path,
                    $pdf->output()
                );

                $files[] = storage_path('app/' . $path);
            }
        } elseif ($invoicedPayments->count()) {
            foreach ($invoicedPayments as $invoicedPayment) {
                $filename = 'po_receipt_' . $invoicedPayment->id . '.pdf';
                $path = 'po_receipts/' . $filename;
                $pdf = $this->generateSingleReceiptPO($order, $invoicedPayment, $user);

                Storage::put(
                    $path,
                    $pdf->output()
                );

                $files[] = storage_path('app/' . $path);
            }
        }

        if (!$files) {
            abort(404);
        }

        $m->addIterator($files);
        return $m->merge();
    }

    /**
     * @param Order $order
     * @param PaymentTransaction $paymentTransaction
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateSingleReceipt(Order $order, PaymentTransaction $paymentTransaction, User $user)
    {
        $params = [
            'order' => $order,
            'user' => $user,
            'paymentTransaction' => $paymentTransaction
        ];

        return PDF::loadView('main.receipt.template', $params)
            ->setPaper('a4');
    }

    /**
     * @param Order $order
     * @param InvoicedPayment $invoicedPayment
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generateSingleReceiptPO(Order $order, InvoicedPayment $invoicedPayment, User $user)
    {
        $params = [
            'order' => $order,
            'user' => $user,
            'invoicedPayment' => $invoicedPayment
        ];

        return PDF::loadView('main.receipt.template-b', $params)
            ->setPaper('a4');
    }

    public function updateOrderAndItemsStatus(Order $order, $status = 'COMPLETE')
    {

        $res = $order->update(['status' => $status]);
        $orderItems = $order->items;

        if ($orderItems->count()) {
            $orderItems->each(function($orderItem) use ($status) {
                $orderItem->status =  $status;
                $orderItem->save();

                $orderableObject = $orderItem->orderable;
                $orderableObject->status =  $status;
                $orderableObject->save();

                $actingAgents = $this->actingOriginatingAgentRepo
                    ->getByOrderableAndAgent([
                        'orderable_id' => $orderItem->orderable_id,
                        'orderable_type' => $orderItem->orderable_type
                    ]);

                if ($actingAgents->count()) {
                    $actingAgents->each(function($actingAgent) use ($status) {
                        $actingAgent->status =  $status;
                        $actingAgent->save();
                    });
                }
            });
        }

        return $res;
    }

}