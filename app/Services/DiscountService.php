<?php namespace App\Services;

use App\Facades\CourseUtility;
use App\Facades\OrderService as OrdService;
use App\Models\OrderAdjustment;
use Session;
use App\Models\User;
use App\Models\Course;
use App\Models\Order;
use App\Models\CouponDiscount;
use App\Models\ClassRegistration;
use App\Models\RegistrationOrder;
use App\Models\ClassHalfPriceDiscount;
use App\Models\ClassCombinationDiscount;
use App\Models\ClassCouponDiscount;
use App\Models\OrderAdjustmentDiscount;
use App\Models\ClassMilitaryDiscount;
use App\Models\ClassCombination;
use App\Models\ClassCreditDiscount;
use App\Models\RemovedDiscountByCredit;
use App\Models\UserYearlySubscription;
use App\Models\YearlySubscription;
use App\Models\ClassSubscriptionDiscount;
use App\Models\ClientYearlySubscription;
use App\Models\CourseClass;
use App\Repositories\ContactRepository;
use App\Repositories\ClassRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OrderItemRepository;
use App\Repositories\CouponRepository;
use App\Repositories\ClassCombinationRepository;
use App\Repositories\ClassCombinationDiscountRepository;
use App\Repositories\CouponDiscountRepository;
use App\Repositories\RegistrationOrderRepository;
use App\Repositories\ClassRegistrationRepository;
use App\Repositories\ClassCouponDiscountRepository;
use App\Repositories\OrderAdjustmentDiscountRepository;
use App\Repositories\ClassHalfPriceDiscountRepository;
use App\Repositories\ClassMilitaryDiscountRepository;
use App\Repositories\ClassCreditDiscountRepository;
use App\Utilities\Constant;
use App\Facades\DiscountUtility;
use App\Events\EditOrderEvent;
use App\Facades\OrderModificationDiscountService as ModificationDiscountService;
use App\Facades\OrderModificationService as ModificationService;
use App\Facades\OrderUtility;
use App\Models\ProductOrders;
use Illuminate\Database\Eloquent\Collection;

class DiscountService
{

    /**
     * @var ContactRepository
     */
    private $contactRepo;

    /**
     * @var ClassRepository
     */
    private $classRepo;

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var OrderItemRepository
     */
    private $orderItemRepo;

    /**
     * @var RegistrationOrderRepository
     */
    private $registrationOrderRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @var ClassCombinationRepository
     */
    private $classCombinationRepo;

    /**
     * @var ClassCombinationDiscountRepository
     */
    private $classCombinationDiscountRepo;

    /**
     * @var CouponRepository
     */
    private $couponRepo;

    /**
     * @var CouponDiscountRepository
     */
    private $couponDiscountRepo;

    /**
     * @var ClassCouponDiscountRepository
     */
    private $classCouponDiscountRepo;

    /**
     * @var OrderAdjustmentDiscountRepository
     */
    private $orderAdjustmentDiscountRepo;

    /**
     * @var ClassHalfPriceDiscountRepository
     */
    private $classHalfPriceDiscountRepo;

    /**
     * @var ClassMilitaryDiscountRepository
     */
    private $classMilitaryDiscountRepo;

    /**
     * @var ClassCreditDiscountRepository
     */
    private $classCreditDiscountRepo;

    /**
     * @var array|mixed
     */
    private $hwmIds = [];

    /**
     * @var array|mixed
     */
    private $dotIds = [];

    /**
     * @param ContactRepository $contactRepo
     * @param ClassRepository $classRepo
     * @param OrderRepository $orderRepo
     * @param OrderItemRepository $orderItemRepo
     * @param RegistrationOrderRepository $registrationOrderRepo
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ClassCombinationRepository $classCombinationRepo
     * @param ClassCombinationDiscountRepository $classCombinationDiscountRepo
     * @param CouponRepository $couponRepo
     * @param CouponDiscountRepository $couponDiscountRepo
     * @param ClassCouponDiscountRepository $classCouponDiscountRepo
     * @param OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo
     * @param ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo
     * @param ClassMilitaryDiscountRepository $classMilitaryDiscountRepo
     * @param ClassCreditDiscountRepository $classCreditDiscountRepo
     */
    public function __construct(ContactRepository $contactRepo,
                                ClassRepository $classRepo,
                                OrderRepository $orderRepo,
                                OrderItemRepository $orderItemRepo,
                                RegistrationOrderRepository $registrationOrderRepo,
                                ClassRegistrationRepository $classRegistrationRepo,
                                ClassCombinationRepository $classCombinationRepo,
                                ClassCombinationDiscountRepository $classCombinationDiscountRepo,
                                CouponRepository $couponRepo,
                                CouponDiscountRepository $couponDiscountRepo,
                                ClassCouponDiscountRepository $classCouponDiscountRepo,
                                OrderAdjustmentDiscountRepository $orderAdjustmentDiscountRepo,
                                ClassHalfPriceDiscountRepository $classHalfPriceDiscountRepo,
                                ClassMilitaryDiscountRepository $classMilitaryDiscountRepo,
                                ClassCreditDiscountRepository $classCreditDiscountRepo)
    {

        $this->contactRepo = $contactRepo;
        $this->classRepo = $classRepo;
        $this->orderRepo = $orderRepo;
        $this->orderItemRepo = $orderItemRepo;
        $this->registrationOrderRepo = $registrationOrderRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
        $this->couponRepo = $couponRepo;
        $this->classCombinationDiscountRepo = $classCombinationDiscountRepo;
        $this->classCombinationRepo = $classCombinationRepo;
        $this->couponDiscountRepo = $couponDiscountRepo;
        $this->classCouponDiscountRepo = $classCouponDiscountRepo;
        $this->orderAdjustmentDiscountRepo = $orderAdjustmentDiscountRepo;
        $this->classHalfPriceDiscountRepo = $classHalfPriceDiscountRepo;
        $this->classMilitaryDiscountRepo = $classMilitaryDiscountRepo;
        $this->classCreditDiscountRepo = $classCreditDiscountRepo;
        $this->hwmIds = $classRepo->getIdsOfHWMPricingRules();
        $this->dotIds = $classRepo->getIdsOfDOTPricingRules();
    }

    /**
     * @param ClassRegistration $model
     * @param bool|false $hasPair
     * @param string $status
     * @return $this
     * @throws \Exception
     */
    public function checkDiscount(ClassRegistration $model, $hasPair = false, $status = Constant::PENDING)
    {
        try {
            $attendee = $model->attendee_contact_id;
            $registrationOrder = $model->registrationOrder;
            $orderItem = $this->orderItemRepo->getByOrderable(
                $registrationOrder->id,
                Constant::REGISTRATION_ORDER,
                false
            );

            $order = $orderItem->order;

            $inHwm = in_array($registrationOrder->registrable_id, $this->hwmIds) &&
                     $registrationOrder->registrable_type == Constant::COURSE_CLASS_OBJECT;

            $inDot = in_array($registrationOrder->registrable_id, $this->dotIds) &&
                $registrationOrder->registrable_type == Constant::COURSE_CLASS_OBJECT;

            if ($inHwm || $inDot) {

                $registrationOrderObject = $this->registrationOrderRepo
                                                ->getForHWMDOTDiscount([
                                                    'registrable_ids'   => $inHwm ? $this->dotIds : $this->hwmIds,
                                                    'order_id'          => $order->id,
                                                    'has_pair'          => $hasPair,
                                                    'status'            => $status
                                                ]);

                if ($registrationOrderObject) {

                    $classRegistrations = $this->classRegistrationRepo
                        ->getForPricingDiscountWithAttendee([
                            'attendee' => $attendee,
                            'registration_order_id' => $registrationOrderObject->id
                        ]);

                    $allPricing = $this->classRegistrationRepo
                        ->getAllPricingDiscount($classRegistrations);

                    $singlePricing = $this->classRegistrationRepo
                        ->getSinglePricingDiscount($classRegistrations);

                    $firstCourseIsNotCouponDiscounted = ($singlePricing)
                        ? $singlePricing->isDiscounted
                        : false;
                    $secondCourseIsNotCouponDiscounted = $model->isDiscounted;

                    if ($allPricing->count() % 2 and $singlePricing and
                        (!$model->is_half_priced and !$singlePricing->is_half_priced) and
                        CourseUtility::isTheSameLocationOrWeek($singlePricing, $model) and
                        $firstCourseIsNotCouponDiscounted and
                        $secondCourseIsNotCouponDiscounted
                    ) {

                        $hwmId = $inHwm ? $registrationOrder->registrable_id
                            : $singlePricing->registrationOrder->registrable_id;

                        $dotId = $inHwm ? $singlePricing->registrationOrder->registrable_id
                            : $registrationOrder->registrable_id;

                        $this->createHWMDOTDiscount([
                            'order_id' => $order->id,
                            'hwm_class_id' => $hwmId,
                            'dot_class_id' => $dotId,
                            'class_reg_one' => $model->id,
                            'class_reg_two' => $singlePricing->id,
                            'attendee_contact_id' => $attendee,
                            'deduction' => 100
                        ]);

                        $existingClassCombination = $this->classCombinationRepo
                            ->getByRegIds(
                                $model->registrationOrder->id,
                                $singlePricing->registrationOrder->id
                            );

                        if (!$existingClassCombination) {
                            ClassCombination::create([
                                'reg_one' => $model->registrationOrder->id,
                                'reg_two' => $singlePricing->registrationOrder->id
                            ]);

                        }

                        $singlePricing->item_charge = $singlePricing->item_charge - 50;
                        $singlePricing->save();

                        $model->item_charge = $model->item_charge - 50;
                        $model->save();
                    }
                }

            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Apply discounts and sorting
     *
     * @param $classRegs
     * @param $registrationOrder
     * @return $this
     * @throws \Exception
     */
    public function applyDiscountAndSorting($classRegs, $registrationOrder)
    {
        if (count($classRegs)) {

            foreach ($classRegs as $classReg) {

                $resp = self::sortRegistrationsForClassCombination($classReg);
            }

            if ($resp['regOrder'] && $resp['contacts']) {

                foreach ($resp['contacts'] as $contact) {
                    $classReg = $this->classRegistrationRepo
                        ->getByRegistrationAndAttendee($resp['regOrder']->id, $contact);

                    if ($classReg && !$classReg->subscriptionDiscount) {
                        self::checkDiscount($classReg);
                    }
                }

                $resp['otherReg']->has_pair = true;
                $resp['otherReg']->save();

                $resp['inverseReg']->has_pair = true;
                $resp['inverseReg']->save();
            } else {
                $classCombination = $this->classCombinationRepo->getBySingeRegId($resp['regOrder']->id);

                if ($classCombination) {
                    $classCombination->delete();
                }
            }

            $order = $this->orderRepo->getSingleModelByOrderable(Constant::REGISTRATION_ORDER, $registrationOrder->id);

            DiscountService::checkIfCouponExists($order);
        }

        return $this;
    }

    /**
     * Apply discounts and sorting on deleted registration
     *
     * @param RegistrationOrder $registration
     */
    public function applyDiscountAndSortingInDeleted(RegistrationOrder $registration)
    {
        $registrations = $this->registrationOrderRepo
            ->getByAgentPending($registration->originating_agent_user_id);

        if ($registrations->count()) {

            foreach ($registrations as $reg) {
                self::applyDiscountAndSorting($reg->classRegistrations, $reg);
            }
        }
    }

    /**
     * Sort registrations
     *
     * @param ClassRegistration $model
     * @return $this
     */
    public function sortRegistrationsForClassCombination(ClassRegistration $model)
    {
        $sameValues = null;
        $regOrder = null;

        $response = false;
        $otherHasMilitaryDiscount = false;
        $inverseHasMilitaryDiscount = false;

        $registrationOrder = $model->registrationOrder;

        $order = $this->orderRepo->getSingleByOrderable(
            Constant::REGISTRATION_ORDER,
            $registrationOrder->id
        );

        $regOrder = $registrationOrder;

        if ($registrationOrder->registrable_type == Constant::COURSE_CLASS_OBJECT) {
            $registrableId = $registrationOrder->registrable_id;

            $response = in_array($registrableId, $this->dotIds) || in_array($registrableId, $this->hwmIds);
        }

        $registrationWithoutAttendee = $this->registrationOrderRepo
            ->getForHWMDOTDiscount([
                'registrable_ids'   => $this->dotIds,
                'order_id'          => $order->id,
                'has_pair'          => 0,
                'status'            => Constant::PENDING
            ]);

        $inverseWithoutAttendee = $this->registrationOrderRepo
            ->getForHWMDOTDiscount([
                'registrable_ids'   => $this->hwmIds,
                'order_id'          => $order->id,
                'has_pair'          => 0,
                'status'            => Constant::PENDING
            ]);

        $otherPricing = $registrationWithoutAttendee
                         ? $registrationWithoutAttendee->classRegistrations
                         : null;

        $inverseOtherPricing = $inverseWithoutAttendee
                               ? $inverseWithoutAttendee->classRegistrations
                               : null;

        if ($otherPricing && $inverseOtherPricing) {

            $ctr1 = $ctr2 = 0;

            $existingClassCombination = $this->classCombinationRepo->getByRegIds(
                $registrationWithoutAttendee->id,
                $inverseWithoutAttendee->id
            );

            if (!$existingClassCombination) {

                $regOrder = $registrationWithoutAttendee ?: $inverseWithoutAttendee;

                $otherPrice = $registrationWithoutAttendee->originalPrice();
                $inversePrice = $inverseWithoutAttendee->originalPrice();

                $otherOriginalPrice = $otherPrice;
                $inverseOriginalPrice = $inversePrice;

                $contactsOtherPricing = $otherPricing->pluck('attendee_contact_id')->toArray();
                $contactsInverse = $inverseOtherPricing->pluck('attendee_contact_id')->toArray();

                $otherDiff = array_diff($contactsOtherPricing, $contactsInverse);
                $inverseDiff = array_diff($contactsInverse, $contactsOtherPricing);
                $sameValues = array_intersect($contactsOtherPricing, $contactsInverse);

                $otherContactIds = array_merge($sameValues, $otherDiff);
                $inverseContactIds = array_merge($sameValues, $inverseDiff);

                foreach ($otherContactIds as $otherContactId) {
                    $otherContact = $this->contactRepo->getById($otherContactId);

                    if ($otherContact->user && $otherContact->user->has_military_discount) {
                        $otherHasMilitaryDiscount = true;
                    }
                }

                foreach ($otherContactIds as $otherContactId) {
                    $ctr1++;

                    $classRegistration = ClassRegistration::create([
                        'registration_order_id' => $registrationWithoutAttendee->id,
                        'attendee_contact_id' => $otherContactId,
                        'attended' => false,
                        'item_charge' => $otherOriginalPrice
                    ]);

                    $registrationOrder = $classRegistration->registrationOrder;

                    if ($registrationOrder->agent->has_military_discount) {
                        $otherHasMilitaryDiscount = true;
                    }

                    if ($classRegistration->subscriptionDiscount) {
                        $ctr1--;
                    } else {

                        $halfPriced = ($ctr1 > 2 and
                            !$registrationOrder->isCBT() and
                            !CourseUtility::excludedClassHalfPriceRule($registrationOrder)) ? true : false;

                        $apply_gsa_discount = ($otherHasMilitaryDiscount and !$halfPriced) ? true : false;

                        $charge = ($apply_gsa_discount) ? apply_gsa_discount($otherOriginalPrice) : $otherOriginalPrice;

                        $itemCharge = ($halfPriced) ? $charge / 2 : $charge;

                        $classRegistration->item_charge = $itemCharge;
                        $classRegistration->is_half_priced = $halfPriced;
                        $classRegistration->save();

                        if ($apply_gsa_discount) {

                            $order = $this->orderRepo->getSingleByOrderable(
                                Constant::REGISTRATION_ORDER,
                                $registrationOrder->id
                            );

                            self::saveMilitaryDiscount([
                                'order_id' => $order->id,
                                'contact_id' => $otherContactId,
                                'class_reg_id' => $classRegistration->id,
                                'deduction' => $otherOriginalPrice - $charge
                            ]);

                        }

                    }
                }

                foreach ($inverseContactIds as $inverseContactId) {
                    $inverseContact = $this->contactRepo->getById($inverseContactId);

                    if ($inverseContact->user && $inverseContact->user->has_military_discount) {
                        $inverseHasMilitaryDiscount = true;
                    }
                }

                foreach ($inverseContactIds as $inverseContactId) {
                    $ctr2++;

                    $classRegistration = ClassRegistration::create([
                        'registration_order_id' => $inverseWithoutAttendee->id,
                        'attendee_contact_id' => $inverseContactId,
                        'attended' => false,
                        'item_charge' => $inverseOriginalPrice
                    ]);

                    $registrationOrder = $classRegistration->registrationOrder;

                    if ($registrationOrder->agent->has_military_discount) {
                        $inverseHasMilitaryDiscount = true;
                    }

                    if ($classRegistration->subscriptionDiscount) {
                        $ctr2--;
                    } else {

                        $halfPriced = ($ctr2 > 2 and
                            !$registrationOrder->isCBT() and
                            !CourseUtility::excludedClassHalfPriceRule($registrationOrder)) ? true : false;

                        $apply_gsa_discount = ($inverseHasMilitaryDiscount and !$halfPriced) ? true : false;

                        $charge = ($apply_gsa_discount) ? apply_gsa_discount($inverseOriginalPrice) : $inverseOriginalPrice;

                        $itemCharge = ($halfPriced) ? $charge / 2 : $charge;

                        $classRegistration->item_charge = $itemCharge;
                        $classRegistration->is_half_priced = $halfPriced;
                        $classRegistration->save();

                        if ($apply_gsa_discount) {

                            $order = $this->orderRepo->getSingleByOrderable(
                                Constant::REGISTRATION_ORDER,
                                $registrationOrder->id
                            );

                            self::saveMilitaryDiscount([
                                'order_id' => $order->id,
                                'contact_id' => $inverseContactId,
                                'class_reg_id' => $classRegistration->id,
                                'deduction' => $inverseOriginalPrice - $charge
                            ]);

                        }

                    }

                    foreach ($otherPricing as $otherClass) {
                        $otherClass->forceDelete();
                    }

                    foreach ($inverseOtherPricing as $inverseClass) {
                        $inverseClass->forceDelete();
                    }
                }
            }

        } else {

            $dotRegistrations = $this->registrationOrderRepo
                ->getForHWMDOTDiscount([
                    'registrable_ids'   => $this->dotIds,
                    'order_id'          => $order->id,
                    'has_pair'          => 1,
                    'all'               => true,
                    'status'            => Constant::PENDING
                ]);

            $hwmRegistrations = $this->registrationOrderRepo
                ->getForHWMDOTDiscount([
                    'registrable_ids'   => $this->hwmIds,
                    'order_id'          => $order->id,
                    'has_pair'          => 1,
                    'all'               => true,
                    'status'            => Constant::PENDING
                ]);

            if ($dotRegistrations) {

                foreach ($dotRegistrations as $dotRegistration) {

                    if (!$this->classCombinationRepo->getBySingeRegId($dotRegistration->id)) {

                        $dotRegistration->has_pair = false;
                        $dotRegistration->save();
                    }
                }

            }

            if ($hwmRegistrations) {

                foreach ($hwmRegistrations as $hwmRegistration) {

                    if (!$this->classCombinationRepo->getBySingeRegId($hwmRegistration->id)) {

                        $hwmRegistration->has_pair = false;
                        $hwmRegistration->save();
                    }
                }
            }
        }


        return [
            'contacts'   => $sameValues,
            'regOrder'   => $regOrder,
            'otherReg'   => $registrationWithoutAttendee,
            'inverseReg' => $inverseWithoutAttendee,
            'response'   => $response
        ];
    }

    /**
     * Recount Class Registration to apply group discount
     * Recounting when a Corporate Seat change
     *
     * @param $registrationOrder $registrationOrder
     * @throws \Exception
     */
    public function reCountOrderOfClassRegistrationCorpSeatChange(RegistrationOrder $registrationOrder)
    {
        try {
            $ctr = 0;

            $classRegistrations = $this->classRegistrationRepo
                                       ->getByRegistrationIdNoSwapNoCancel($registrationOrder->id);

            if ($classRegistrations->count()) {
                foreach ($classRegistrations as $classRegistration) {

                    if (!$classRegistration->subscriptionDiscount) {

                        if (!$classRegistration->corporate_seat_credit_applied) {

                            $ctr++;

                            if ($classRegistration->classHalfPriceDiscount) {
                                $classHalfPriceDiscount = $classRegistration->classHalfPriceDiscount;
                                $deduction = $classHalfPriceDiscount->deduction;

                                if ($ctr < 3) {

                                    if (($classHalfPriceDiscount->is_deleted_by_credit ||
                                        !$classHalfPriceDiscount->is_deleted_by_credit) &&
                                        $classRegistration->item_charge <= $deduction) {

                                        $classRegistration->item_charge += $deduction;
                                        $classRegistration->is_half_priced = false;

                                        $classRegistration->save();
                                        $classHalfPriceDiscount->is_deleted_by_credit = true;

                                        $militaryDiscount = $classRegistration->classMilitaryDiscount;

                                        if ($militaryDiscount and !$militaryDiscount->is_deleted_by_credit) {
                                            $originalPrice = $registrationOrder->originalPrice();
                                            $gsaDeduction = $originalPrice - apply_gsa_discount($originalPrice);

                                            $militaryDiscount->deduction = $gsaDeduction;
                                            $militaryDiscount->save();
                                        }

                                        if (OrderUtility::isCompleteOrInvoiced($registrationOrder)) {
                                            $orderItem = $this->orderItemRepo->getByOrderable($registrationOrder->id, Constant::REGISTRATION_ORDER, false);
                                            $order = $orderItem->order;
                                            $agent = $order->user;

                                            $params = [
                                                'agent_id' => $agent->id,
                                                'order_id' => $order->id,
                                                'order_item_id' => $orderItem->id,
                                                'adjustable_id' => $classRegistration->id,
                                                'adjustable_type' => Constant::CLASS_REGISTRATION,
                                                'amount' => $deduction,
                                                'adjustment_action' => Constant::ADDED,
                                                'note' => adjustment_registration_half_price_discount($classRegistration, $registrationOrder),
                                                'is_half_price_adjustment' => true
                                            ];

                                            ModificationService::saveOrderAdjustment($params);
                                        }
                                    }
                                } elseif ($ctr > 2) {

                                    if ((!$classHalfPriceDiscount->is_deleted_by_credit &&
                                          $classRegistration->item_charge != $deduction) ||
                                         ($classHalfPriceDiscount->is_deleted_by_credit &&
                                          $classRegistration->item_charge > $deduction)) {

                                        $classRegistration->item_charge -= $deduction;
                                        $classRegistration->is_half_priced = true;
                                        $classRegistration->save();

                                        $classHalfPriceDiscount->is_deleted_by_credit = false;

                                        $militaryDiscount = $classRegistration->classMilitaryDiscount;

                                        if ($militaryDiscount and !$militaryDiscount->is_deleted_by_credit) {
                                            $originalPrice = $registrationOrder->originalPrice() / 2;
                                            $gsaDeduction = $originalPrice - apply_gsa_discount($originalPrice);

                                            $militaryDiscount->deduction = $gsaDeduction;
                                            $militaryDiscount->save();
                                        }
                                    }

                                    if ($registrationOrder->status != Constant::PENDING) {
                                        $halfPriceOrderAdjustment = OrderAdjustment::where([
                                            'adjustable_id' => $classRegistration->id,
                                            'adjustable_type' => Constant::CLASS_REGISTRATION,
                                            'adjustment_action' => Constant::ADDED,
                                            'is_half_price_adjustment' => true
                                        ])->first();

                                        if ($halfPriceOrderAdjustment) {
                                            $halfPriceOrderAdjustment->delete();

                                            $halfPriceDiscount = $this->classHalfPriceDiscountRepo
                                                ->getByClassRegId($classRegistration->id);
                                            
                                            if ($halfPriceDiscount) {
                                                $halfPriceDiscount->delete();
                                            }
                                        }
                                    }
                                }

                                $classHalfPriceDiscount->save();
                            }
                        }
                    }

                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create HWM DOT Discount instance
     *
     * @param array $params
     * @return ClassDiscount
     */
    private function createHWMDOTDiscount($params = [])
    {
        $classDiscount = new ClassCombinationDiscount();
        $classDiscount->fill($params);
        $classDiscount->save();

        return $classDiscount;
    }

    /**
     * Reset HTM-DOT combination discount
     *
     * @param ClassRegistration $classRegistration
     * @return $this
     */
    public function resetHWMDOTDiscounts(ClassRegistration $classRegistration)
    {
        $registrationOrder = $classRegistration->registrationOrder;

        $classCombination = $this->classCombinationRepo->getBySingeRegId($registrationOrder->id);

        if ($classCombination) {
            $classCombination->firstRegistration->has_pair = false;
            $classCombination->firstRegistration->save();

            $classCombination->secondRegistration->has_pair = false;
            $classCombination->secondRegistration->save();
        }

        return $this;
    }

    /**
     * Create Half Price Discount instance
     *
     * @param ClassRegistration $model
     * @return ClassHalfPriceDiscount
     */
    public function createHalfPriceDiscount(ClassRegistration $model)
    {
        if ($model->is_half_priced && !$model->subscriptionDiscount) {

            $registrationOrder = $model->registrationOrder;
            $orderItem = $this->orderItemRepo->getByOrderable(
                $registrationOrder->id,
                Constant::REGISTRATION_ORDER,
                false
            );

            $order = $orderItem->order;

            if(ClassHalfPriceDiscount::where('order_id', $order->id)
                            ->where('class_reg_id', $model->id)->exists())
            {
                return null;
            }

            $classHalfPriceDiscount = new ClassHalfPriceDiscount();
            $classHalfPriceDiscount->fill([
                'order_id'     => $order->id,
                'class_reg_id' => $model->id,
                'deduction'    => $model->item_charge
            ]);

            $classHalfPriceDiscount->save();

            return $classHalfPriceDiscount;
        }

        return false;
    }

    /**
     * Extract class registrations
     *
     * @param $classRegistrationObject
     * @return $this
     * @throws \Exception
     */
    public function extractFromRegistrations($classRegistrationObject)
    {
        try {

            if (is_a($classRegistrationObject, 'Illuminate\Database\Eloquent\Collection')) {

                foreach ($classRegistrationObject as $classRegistration) {
                    $this->restoreDeduction($classRegistration->id);
                }

                return $this;
            }

            $this->restoreDeduction($classRegistrationObject->id);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Restore deduction made
     *
     * @param $classRegistrationId
     */
    private function restoreDeduction($classRegistrationId)
    {
        $classDiscounts = $this->classCombinationDiscountRepo
            ->getByClassRegistrationId($classRegistrationId);

        if ($classDiscounts->count()) {

            foreach ($classDiscounts as $classDiscount) {
                $order    = $classDiscount->order;
                $classOne = $classDiscount->classOne()->withTrashed()->first();
                $classTwo = $classDiscount->classTwo()->withTrashed()->first();
                $classOneCouponDiscount = $this->classCouponDiscountRepo
                                               ->getByClassIdAndOrderId($order->id, $classOne->id);

                $classTwoCouponDiscount = $this->classCouponDiscountRepo
                                               ->getByClassIdAndOrderId($order->id, $classTwo->id);

                if ($classOneCouponDiscount) {
                    $coupon = $classOneCouponDiscount->couponDiscount->coupon;

                    if ($coupon->coupon_type == Constant::PERCENT) {
                        $originalAmount = $classOne->registrationOrder->originalPrice();
                        $deduction      = ($coupon->coupon_amount / 100) * $originalAmount;
                        $itemChargeOne  = $originalAmount - $deduction;

                        $classOneCouponDiscount->deduction = $deduction;
                        $classOneCouponDiscount->save();
                    }
                }

                else {
                    $itemChargeOne = $classOne->item_charge + 50;
                }

                if ($classTwoCouponDiscount) {
                    $coupon = $classTwoCouponDiscount->couponDiscount->coupon;

                    if ($coupon->coupon_type == Constant::PERCENT) {
                        $originalAmount = $classTwo->registrationOrder->originalPrice();
                        $deduction      = ($coupon->coupon_amount / 100) * $originalAmount;
                        $itemChargeTwo  = $originalAmount - $deduction;

                        $classTwoCouponDiscount->deduction = $deduction;
                        $classTwoCouponDiscount->save();
                    }
                }

                else {
                    $itemChargeTwo = $classTwo->item_charge + 50;
                }

                $classOne->item_charge = $itemChargeOne;
                $classTwo->item_charge = $itemChargeTwo;

                $classOne->save();
                $classTwo->save();

                $classDiscount->delete();
            }

        }
    }

    /***************************************************************************************/
    /*                                   Coupon Discounts                                  */
    /***************************************************************************************/

    /**
     * Apply coupon
     *
     * @param array $params
     * @return string
     * @throws \Exception
     */
    public function applyCoupon($params = [])
    {
        try {

            $registrationOrderCBT = [];
            $coupon = $this->couponRepo
                           ->findOne(['coupon_code' => $params['coupon_code']]);

            $order = $this->orderRepo->getById($params['order_id']);
            $code = $params['coupon_code'];

            if ($coupon) {

                $couponType = $coupon->coupon_type;
                $couponAmount = $coupon->coupon_amount;

                $discountable = $this->extractDiscountables($coupon);
                $discountableIds = $discountable['discIds'];
                $discountableType = $discountable['discTypes'];
                $computerBasedIds = $discountable['cbtIds'];

                if (count($computerBasedIds)) {
                    $registrationOrderCBT = $this->registrationOrderRepo
                                                 ->getForDiscount(
                                                    $order->id,
                                                    $computerBasedIds,
                                                    Constant::COURSE_OBJECT
                                                 );
                }

                $registrationOrders = $this->registrationOrderRepo
                                           ->getForDiscount(
                                               $order->id,
                                               $discountableIds,
                                               $discountableType
                                           )->merge($registrationOrderCBT);


                if ($registrationOrders->count()) {

                    self::removeExistingCouponDiscounts($order->id);

                    $couponDiscount = CouponDiscount::create([
                        'order_id' => $order->id,
                        'coupon_id' => $coupon->id
                    ]) ;

                    self::computeCouponDiscount([
                        'coupon_discount_id' => $couponDiscount->id,
                        'reg_orders' => $registrationOrders,
                        'coupon_type' => $couponType,
                        'amount' => $couponAmount
                    ]);

                    $code = '';
                    $message = 'Coupon code applied.';
                    Session::flash('coupon_applied', true);

                    $param = [
                        'order_id'  => $order->id,
                        'open_flag' => 0,
                        'save_flag' => 1
                    ];

                    event(new EditOrderEvent($param));

                }

                else {
                    $message = 'No Courses/Classes associated the coupon code.';
                }
            }

            else {
                $message = 'Coupon Code does not exist.';
            }


            Session::flash('coupon_message', $message);
            Session::flash('coupon_code', $code);

            return $message;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Check whether coupon exists
     *
     * @param Order $order
     * @return $this
     * @throws \Exception
     */
    public function checkIfCouponExists(Order $order)
    {
        try {

            $orderItems = $order->items;

            if ($order->couponDiscounts->count() and $orderItems->count()) {

                foreach ($order->couponDiscounts as $couponDiscount) {
                    $coupon           = $couponDiscount->coupon;
                    $discountable     = self::extractDiscountables($coupon);
                    $discountableIds  = $discountable['discIds'];
                    $discountableType = $discountable['discTypes'];
                    $cbtIds           = $discountable['cbtIds'];

                   foreach ($orderItems as $item) {

                       if ($item->orderable_type == Constant::REGISTRATION_ORDER) {
                           $registrationOrder = $item->orderable;
                           $registrableId = $registrationOrder->registrable_id;
                           $registrableType = $registrationOrder->registrable_type;

                           if ($registrableType == $discountableType) {

                               if (in_array($registrableId, $discountableIds) ||
                                   in_array($registrableId, $cbtIds)
                               ) {

                                   self::computeCouponDiscount([
                                       'coupon_discount_id' => $couponDiscount->id,
                                       'reg_orders' => [
                                           $registrationOrder
                                       ],
                                       'coupon_type' => $coupon->coupon_type,
                                       'amount' => $coupon->coupon_amount
                                   ]);
                               }
                           }
                       }
                   }
                }

            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Extract discounts
     *
     * @param $coupon
     * @return array
     */
    private function extractDiscountables($coupon)
    {
        $computerBasedIds = [];
        $classesIds = [];
        $couponCourses = $coupon->couponCourses;
        $discountableIds = $couponCourses->pluck('discountable_id')->toArray();
        $discountableType = $couponCourses->pluck('discountable_type')->first();

        if ($discountableType == Constant::COURSE_OBJECT) {

            foreach ($discountableIds as $discountableId) {
                $course = Course::find($discountableId);

                if ($course->course_type_id == Constant::COMPUTER_BASED) {
                    array_push($computerBasedIds, $course->id);
                }

                $classesIds = array_merge($classesIds, $course->classes->pluck('id')->toArray());
            }

            $discountableIds  = $classesIds;
            $discountableType = Constant::COURSE_CLASS_OBJECT;
        }

        return [
            'discIds' => $discountableIds,
            'discTypes' => $discountableType,
            'cbtIds' => $computerBasedIds
        ];

    }

    /**
     * Calculate coupon discount
     *
     * @param array $params
     * @return float|int
     * @throws \Exception
     */
    public function computeCouponDiscount($params = [])
    {
        try {
            $total = 0;

            foreach ($params['reg_orders'] as $registrationOrder) {
                $classRegs = $registrationOrder->classRegistrations;

                foreach ($classRegs as $classReg) {

                    if (!$classReg->subscriptionDiscount and
                        !$classReg->corporate_seat_credit_applied and
                        !$classReg->is_cancelled) {

                        switch ($params['coupon_type']) {

                            case Constant::FIXED :
                                $deduction = $params['amount'];
                                break;

                            case Constant::PERCENT :
                                $price = $classReg->item_charge;

                                $hwmDotDiscount = $this->classCombinationDiscountRepo
                                    ->getByClassRegistrationId($classReg->id, true);

                                if ($hwmDotDiscount and is_null($classReg->classMilitaryDiscount)) {
                                    $price = $classReg->registrationOrder->originalPrice();
                                }

                                if ($classReg->classMilitaryDiscount) {
                                    $price = apply_gsa_discount($classReg->registrationOrder->originalPrice());
                                }

                                $deduction = ($params['amount'] / 100) * $price;
                                break;

                            default :
                                break;
                        }

                        if (!$classReg->is_half_priced and $classReg->isDiscounted) {
                            $hwmDotDiscount = $this->classCombinationDiscountRepo
                                                   ->getByClassRegistrationId($classReg->id, true);


                            $total += $deduction;

                            if ($hwmDotDiscount) {
                                if ($classReg->id != $hwmDotDiscount->classOne->id) {
                                    $hwmDotDiscount->classOne->item_charge += 50;
                                    $hwmDotDiscount->classOne->save();
                                }

                                if ($classReg->id != $hwmDotDiscount->classTwo->id) {
                                    $hwmDotDiscount->classTwo->item_charge += 50;
                                    $hwmDotDiscount->classTwo->save();
                                }

                                $classReg->item_charge += 50;
                                $classReg->save();

                                $hwmDotDiscount->delete();

                                $charge = $classReg->registrationOrder->originalPrice();
                            }

                            else {
                                $charge = $classReg->item_charge;
                            }

                            $newItemCharge = $charge - $deduction;

                            if ($hwmDotDiscount and $classReg->classMilitaryDiscount) {
                                $newItemCharge = $deduction;
                            }

                            $classReg->item_charge = $newItemCharge;
                            $classReg->paid_charge = $newItemCharge;
                            $classReg->save();

                            self::saveCouponDiscount([
                                'coupon_discount_id' => $params['coupon_discount_id'],
                                'class_reg_id' => $classReg->id,
                                'deduction' => $deduction
                            ]);

                            if (OrderUtility::isCompleteOrInvoiced($registrationOrder)) {

                                $paramsAdjustment = [
                                    'agent_id' => $registrationOrder->orderItem->order->user->id,
                                    'order_id' => $registrationOrder->orderItem->order->id,
                                    'order_item_id' => $registrationOrder->orderItem->id,
                                    'adjustable_id' => $classReg->id,
                                    'adjustable_type' => Constant::CLASS_REGISTRATION,
                                    'amount' => $deduction,
                                    'adjustment_action' => Constant::DEDUCTED,
                                    'note' => adjustment_registration_coupon_applied($classReg, $registrationOrder)
                                ];

                                ModificationService::saveOrderAdjustment($paramsAdjustment);
                            }
                        }
                    }
                }

                OrdService::setPriceCharged($registrationOrder);
            }

            return $total;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Save coupon discount
     *
     * @param array $params
     * @return CouponDiscount
     */
    public function saveCouponDiscount($params = [])
    {
        $classDiscount = new ClassCouponDiscount();
        $classDiscount->fill($params);
        $classDiscount->save();

        return $classDiscount;
    }

    /**
     * Remove an existing coupon discount
     *
     * @param $orderId
     * @return $this
     * @throws \Exception
     */
    public function removeExistingCouponDiscounts($orderId)
    {
        try {
            $couponDiscount = $this->couponDiscountRepo
                ->getByOrderId($orderId);

            if ($couponDiscount) {
                $classDiscounts = $couponDiscount->classDiscounts;

                if ($classDiscounts->count()) {

                    foreach ($classDiscounts as $classDiscount) {
                        $classReg = $classDiscount->classRegistration;

                        $classReg->item_charge += $classDiscount->deduction;
                        $classReg->save();
                    }

                }

                $couponDiscount->delete();
            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Create Order Adjustment instance
     *
     * @param Order $order
     * @param $adjustable
     * @param $price
     * @return $this|OrderAdjustmentDiscount
     * @throws \Exception
     */
    public function createOrderAdjustmentDiscount(Order $order, $adjustable, $price)
    {
        try {

            $originalAmount = 0;
            $class = class_basename($adjustable);

            switch ($class) {
                case Constant::CLASS_REGISTRATION :
                    $originalAmount = $adjustable->registrationOrder->originalPrice();

                    $classCombinatonDiscount = $this->classCombinationDiscountRepo
                                                    ->getByClassRegistrationId($adjustable->id, true);

                    if ($classCombinatonDiscount) {
                        $originalAmount -= 50;
                    }

                    if ($adjustable->is_half_priced) {
                        $originalAmount = $adjustable->registrationOrder->originalPrice() / 2;
                    }

                    break;

                case Constant::CLIENT_SUBSCRIPTION :
                    $originalAmount = $adjustable->temporary_price;
                    break;

                case Constant::CORPORATE_SEAT :
                    $originalAmount = $adjustable->price;
                    break;

                case Constant::PRODUCT_ORDERS :
                    $originalAmount = $adjustable->price;
                    break;

                default :
                    break;
            }


            if ($originalAmount > $price) {
                $adjustment = new OrderAdjustmentDiscount();
                $adjustment->order_id = $order->id;
                $adjustment->adjustable_id = $adjustable->id;
                $adjustment->adjustable_type = $class;
                $adjustment->deduction = ($originalAmount - $price);
                $adjustment->save();

                return $adjustment;
            }

            return $this;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Remove order adjustment given adjustable objects
     *
     * @param array $params
     * @return $this
     */
    public function removeOrderAdjustmentDiscountByAdjustable($params = [])
    {
        $orderAdjustmentObjects = $this->orderAdjustmentDiscountRepo->getByAdjustable($params);

        if ($orderAdjustmentObjects->count()) {
            foreach ($orderAdjustmentObjects as $orderAdjustmentObject) {
                $orderAdjustmentObject->delete();
            }
        }

        return $this;
    }

    /**
     * Create instance of ClassMilitaryDiscount
     *
     * @param $params
     * @return ClassMilitaryDiscount
     */
    public function saveMilitaryDiscount($params)
    {
        $militaryDiscount = new ClassMilitaryDiscount();
        $militaryDiscount->fill($params);
        $militaryDiscount->save();

        return $militaryDiscount;
    }


    /**
     * Remove discounts related to soft-deleted Class Registration
     *
     * @param ClassRegistration $classRegistration
     * @return $this
     * @throws \Exception
     */
    public function removeDiscounts(ClassRegistration $classRegistration)
    {
        try {

            //Remove ClassHalfPriceDiscount Object
            $classHalfPriceDisc = $this->classHalfPriceDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classHalfPriceDisc) {
                $classHalfPriceDisc->delete();
            }

            //Remove ClassCouponDiscounts Object
            $classCouponDiscounts = $this->classCouponDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCouponDiscounts->count()) {
                foreach ($classCouponDiscounts as $classCouponDiscount) {
                    $classCouponDiscount->delete();
                }
            }

            //Remove ClassMilitaryDiscounts Object
            $classMilitaryDiscount = $this->classMilitaryDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classMilitaryDiscount) {
                $classMilitaryDiscount->delete();
            }

            $classCreditDiscount = $this->classCreditDiscountRepo->getByClassRegId($classRegistration->id);

            if ($classCreditDiscount) {
                $user = $classCreditDiscount->agent;
                $user->corporate_seat_credits += 1;
                $user->save();

                $classCreditDiscount->delete();
            }

            $orderAdjustments = $this->orderAdjustmentDiscountRepo
                ->getByAdjustable([
                    'adjustable_id' => $classRegistration->id,
                    'adjustable_type' => Constant::CLASS_REGISTRATION
                ]);

            if ($orderAdjustments->count()) {
                foreach ($orderAdjustments as $orderAdjustment) {
                    $orderAdjustment->delete();
                }
            }

            return $this;
        } catch(\Exception $e) {
            throw $e;
        }
    }

    /***************************************************************************************/
    /*                             Corporate Seat Discounts                                */
    /***************************************************************************************/

    /**
     * Save credit used
     *
     * @param User $user
     * @param Order $order
     * @param array $classIds
     */
    public function giveCredit(User $user, Order $order, $classIds = [])
    {
        $classRegistrations = $this->classRegistrationRepo
            ->getByIds($classIds);

        if ($classRegistrations->count()) {

            foreach ($classRegistrations as $classRegistration) {
                $classCreditDiscount = ClassCreditDiscount::create([
                    'order_id' => $order->id,
                    'agent_id' => $user->id,
                    'class_reg_id' => $classRegistration->id,
                    'deduction' => $classRegistration->item_charge
                ]);

                $deduction = $classRegistration->item_charge;

                $classRegistration->item_charge = 0;
                $classRegistration->paid_charge = 0;
                $classRegistration->corporate_seat_credit_applied = true;
                $classRegistration->save();

                $this->removeDiscountsOnceCorpSeatsApplied(
                    $classCreditDiscount->id,
                    $classRegistration->id
                );

                if (OrderUtility::isCompleteOrInvoiced($classRegistration->registrationOrder)) {
                    ModificationDiscountService::createOrderAdjustmentForCredit(
                        $order, $classRegistration, $deduction
                    );
                }
            }
        }
    }

    /**
     * Remove the credit that has been used
     *
     * @param $classRegId
     * @return mixed
     */
    public function removeCredit($classRegId)
    {
        $classRegistration = $this->classRegistrationRepo->getById($classRegId);
        $classCreditDiscount = $this->classCreditDiscountRepo->getByClassRegId($classRegistration->id);

        if ($classCreditDiscount) {
            $agent = $classCreditDiscount->agent->getCorporateSeatsCreditUser();
            $agent->corporate_seat_credits += 1;
            $agent->save();
            $classRegistration = $classCreditDiscount->classRegistration;


            $classRegistration->item_charge = $classCreditDiscount->deduction;
            $classRegistration->corporate_seat_credit_applied = false;
            $classRegistration->save();

            $this->restoreDiscountsOneCorpSeatsRemoved($classCreditDiscount, $classRegistration->id);
            $classCreditDiscount->delete();
        }

        return $this;
    }

    /***************************************************************************************/
    /*                Restore/Restore Discounts Once Corp Seats Applied/Removed            */
    /***************************************************************************************/

    /**
     * Remove the class discount when corp seat is applied
     *
     * @param $classCreditDiscountId
     * @param $classRegId
     */
    private function removeDiscountsOnceCorpSeatsApplied($classCreditDiscountId, $classRegId)
    {
        $classHalfPriceDiscount = $this->classHalfPriceDiscountRepo
                                       ->getByClassRegId($classRegId);

        $classCombinationDiscount = $this->classCombinationDiscountRepo
                                         ->getByClassRegistrationId($classRegId, true);

        $classMilitaryDiscount = $this->classMilitaryDiscountRepo
                                      ->getByClassRegId($classRegId);


        $classCouponDiscounts = $this->classCouponDiscountRepo
            ->getByClassRegId($classRegId);

        if ($classCouponDiscounts->count()) {
            foreach ($classCouponDiscounts as $classCouponDiscount) {
                $this->createRemovedDiscountByCredit($classCouponDiscount, $classCreditDiscountId, $classRegId);
            }
        }

        $orderAdjustments = $this->orderAdjustmentDiscountRepo
            ->getByAdjustable([
                'adjustable_id' => $classRegId,
                'adjustable_type' => Constant::CLASS_REGISTRATION
            ]);

        if ($orderAdjustments->count()) {
            foreach ($orderAdjustments as $orderAdjustment) {
                $this->createRemovedDiscountByCredit($orderAdjustment, $classCreditDiscountId, $classRegId);
            }
        }

        $this->createRemovedDiscountByCredit($classHalfPriceDiscount, $classCreditDiscountId, $classRegId);
        $this->createRemovedDiscountByCredit($classCombinationDiscount, $classCreditDiscountId, $classRegId);
        $this->createRemovedDiscountByCredit($classMilitaryDiscount, $classCreditDiscountId, $classRegId);
    }

    /**
     * Create removed discount by credit instance
     *
     * @param $discountObject
     * @param $classCreditDiscountId
     * @param $classRegId
     */
    private function createRemovedDiscountByCredit($discountObject, $classCreditDiscountId, $classRegId)
    {
        if ($discountObject) {

            if (class_basename($discountObject) == Constant::CLASS_COMBINATION) {
                $classOne = $discountObject->classOne;
                $classTwo = $discountObject->classTwo;

                if (!($classOne->corporate_seat_credit_applied &&
                     $classTwo->corporate_seat_credit_applied)) {

                    if ($classOne->id == $classRegId) {
                        $classTwo->item_charge += 50;
                        $classTwo->save();
                    }

                    if ($classTwo->id == $classRegId) {
                        $classOne->item_charge += 50;
                        $classOne->save();
                    }
                }
            }

            $discountObject->is_deleted_by_credit = true;
            $discountObject->save();

            $params = [
                'class_credit_discount_id' => $classCreditDiscountId,
                'class_reg_id' => $classRegId,
                'discountable_type' => class_basename($discountObject),
                'discountable_id' => $discountObject->id
            ];

            RemovedDiscountByCredit::create($params);
        }
    }

    /**
     * Delete removed discount object
     *
     * @param ClassCreditDiscount $classCreditDiscount
     * @param $classRegId int
     */
    private function restoreDiscountsOneCorpSeatsRemoved(ClassCreditDiscount $classCreditDiscount, $classRegId)
    {
        if ($classCreditDiscount->removedDiscountByCredits) {

            foreach ($classCreditDiscount->removedDiscountByCredits as $removedDiscountByCredit) {
                $discountId = $removedDiscountByCredit->discountable_id;
                $discountType = $removedDiscountByCredit->discountable_type;

                switch ($discountType) {

                    case Constant::CLASS_COUPON :

                        $classCouponDiscount = $this->classCouponDiscountRepo
                                                    ->getById($discountId);

                        if ($classCouponDiscount) {
                            $classCouponDiscount->is_deleted_by_credit = false;
                            $classCouponDiscount->save();
                        }

                        break;

                    case Constant::CLASS_COMBINATION :

                        $classCombinationDiscount = $this->classCombinationDiscountRepo
                                                         ->getById($discountId);

                        if ($classCombinationDiscount) {
                            $classCombinationDiscount->is_deleted_by_credit = false;
                            $classCombinationDiscount->save();

                            $classOne = $classCombinationDiscount->classOne;
                            $classTwo = $classCombinationDiscount->classTwo;

                            if ($classOne->id == $classRegId) {
                                $classTwo->item_charge -= 50;
                                $classTwo->save();
                            }

                            if ($classTwo->id == $classRegId) {
                                $classOne->item_charge -= 50;
                                $classOne->save();
                            }
                        }

                        break;

                    case Constant::CLASS_MILITARY :

                        $classMilitaryDiscount = $this->classMilitaryDiscountRepo
                                                      ->getById($discountId);

                        if ($classMilitaryDiscount) {
                            $classMilitaryDiscount->is_deleted_by_credit = false;
                            $classMilitaryDiscount->save();
                        }


                        break;

                    case Constant::ORDER_ADJUSTMENT :

                        $orderAdjustment = $this->orderAdjustmentDiscountRepo
                                                ->getById($discountId);

                        if ($orderAdjustment) {
                            $orderAdjustment->is_deleted_by_credit = false;
                            $orderAdjustment->save();
                        }

                        break;

                    case Constant::CLASS_HALF_PRICE :

                        $classHalfPriceDiscount = $this->classHalfPriceDiscountRepo
                            ->getById($discountId);

                        if ($classHalfPriceDiscount) {
                            $classHalfPriceDiscount->is_deleted_by_credit = false;
                            $classHalfPriceDiscount->save();
                        }

                        break;
                }

                $removedDiscountByCredit->delete();
            }
        }
    }

    /***************************************************************************************/
    /*                               Yearly Subscription                                   */
    /***************************************************************************************/

    /**
     * Create user and yearly subscription instance
     *
     * @param YearlySubscription $yearlySubscription
     * @return $this
     */
    public function createHasSubscribed(YearlySubscription $yearlySubscription)
    {
        if ($yearlySubscription->clientSubscriptions->count()) {

            foreach($yearlySubscription->clientSubscriptions as $clientSubscription) {
                UserYearlySubscription::create([
                    'user_id' => $clientSubscription->agent->id,
                    'client_yearly_subscription_id' => $clientSubscription->id,
                    'activation_code' => str_random(10)
                ]);
            }
        }

        return $this;
    }

    /**
     * Create Subscription Discount object
     *
     * @param ClassRegistration $classRegistration
     * @return static
     */
    public function createSubscriptionDiscount(ClassRegistration $classRegistration)
    {
        $user = $classRegistration->contact->user;

        if ($user and $user->hasSubscription and $user->hasSubscription->activated) {
            $registrationOrder = $classRegistration->registrationOrder;
            $courseObject = $registrationOrder->isCourse()
                            ? $registrationOrder->registrable
                            : $registrationOrder->registrable->course;

            $subscriptionType = $user->hasSubscription
                                     ->clientSubscription
                                     ->yearlySubscription
                                     ->type;

            if (DiscountUtility::subscriptionMatchesType($courseObject, $subscriptionType)) {

                $order = $this->orderRepo->getSingleByOrderable(
                    Constant::REGISTRATION_ORDER,
                    $registrationOrder->id
                );

                $clientYearlySubscription = $user->hasSubscription
                                                 ->clientSubscription;

                $discount = ClassSubscriptionDiscount::create([
                    'order_id' => $order->id,
                    'user_id' => $user->id,
                    'client_subscription_id' => $clientYearlySubscription->id,
                    'class_reg_id' => $classRegistration->id,
                    'deduction' => $classRegistration->item_charge
                ]);

                $classRegistration->item_charge = 0;
                $classRegistration->save();

                return $discount;
            }
        }

        return false;
    }

    /**
     *
     *
     * @param Collection $items
     * @throws \Exception
     */
    public function searchAndStartSubscription(Collection $items)
    {
        try {
            $subscriptionStartDates = collect();
            $registrationOrders = collect();
            $classIds = collect();
            $classToSubscriptions = collect();
            $classSubscriptionIds = collect();

            if ($items->count()) {
                $items->each(function ($item)
                use ($registrationOrders,
                    $classIds,
                    $classToSubscriptions) {
                    if ($item->orderable_type == Constant::REGISTRATION_ORDER) {
                        $registrationOrders->push($item->orderable);
                    }
                });

                if ($registrationOrders->count()) {
                    $registrationOrders->each(function ($registrationOrder)
                        use ($classIds,
                            $classToSubscriptions,
                            $classSubscriptionIds,
                            $subscriptionStartDates) {
                        $classRegistrations = $registrationOrder->classRegistrations;

                        $classRegistrations->each(function ($classRegistration)
                        use ($registrationOrder,
                            $classIds,
                            $classToSubscriptions,
                            $classSubscriptionIds,
                            $subscriptionStartDates) {
                            if ($classRegistration->subscriptionDiscount) {
                                $clientYearlySubscription = $classRegistration->subscriptionDiscount
                                                                              ->clientYearlySubscription;

                                $subscriptionStartDates->push($clientYearlySubscription->subscription_start_date);
                                $classIds->push($registrationOrder->registrable_id);
                                $classSubscriptionIds->push($clientYearlySubscription->id);
                            }
                        });
                    });

                    if ($classIds->count() && $classSubscriptionIds->count()) {
                        $class = $this->classRepo->getByIdsOrderByStartDate(
                            $classIds->all(),
                            $subscriptionStartDates->first()
                        );

                        if ($class) {
                            $classSubscriptionIds->each(function($classSubscriptionId) use($classToSubscriptions, $class) {
                                $classToSubscriptions->put($classSubscriptionId, $class->id);
                            });
                        }
                    }

                    if ($classToSubscriptions->count()) {
                        $classToSubscriptions->each(function($classId, $clientYearlySubId) {
                            $clientYearlySub = ClientYearlySubscription::find($clientYearlySubId);
                            $class = CourseClass::find($classId);

                            if ($clientYearlySub and $clientYearlySub->userYearlySubscription->activated and $class) {
                                $clientYearlySub->subscription_start_date = $class->start_date;
                                $clientYearlySub->subscription_exp_date = $class->start_date->addYear();
                                $clientYearlySub->save();
                            }
                        });
                    }
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Search for credits used then restore
     *
     * @param RegistrationOrder $registrationOrder
     * @throws \Exception
     */
    public function restoreCreditsIfExists(RegistrationOrder $registrationOrder)
    {
        try {

            if ($registrationOrder->classRegistrations->count()) {

                foreach ($registrationOrder->classRegistrations as $classRegistration) {

                    if ($classRegistration->classCreditDiscount) {
                        $classCreditDiscount = $classRegistration->classCreditDiscount;
                        $user = $classCreditDiscount->agent;

                        $user->corporate_seat_credits += 1;
                        $user->save();
                    }
                }
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function removeProductDiscounts(ProductOrders $productOrders)
    {
        try {

            $orderAdjustments = $this->orderAdjustmentDiscountRepo
                ->getByAdjustable([
                    'adjustable_id' => $productOrders->id,
                    'adjustable_type' => Constant::PRODUCT_ORDERS
                ]);

            if ($orderAdjustments->count()) {
                foreach ($orderAdjustments as $orderAdjustment) {
                    $orderAdjustment->delete();
                }
            }

            return $this;
        } catch(\Exception $e) {
            throw $e;
        }
    }
}