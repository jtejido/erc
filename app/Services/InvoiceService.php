<?php namespace App\Services;

use App\Facades\OrderUtility;
use App\Models\StateTax;
use App\Repositories\OrderAdjustmentRepository;
use File;
use Session;
use Image;
use Imagick;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\InvoicedPayment;
use App\Models\InvoicedPaymentRecipient;
use App\Repositories\OrderItemRepository;
use App\Repositories\UserRepository;
use App\Utilities\Constant;
use App\Facades\PhotoUtility;
use Illuminate\Support\Facades\Storage;
use App\Events\RegistrationConfirmationEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Facades\OrderService;
use App\Facades\OrderModificationService;

class InvoiceService
{

    private $userRepo;

    /**
     * @var \App\Services\PaymentService
     */
    private $paymentService;

    /**
     * @var OrderItemRepository
     */
    private $itemRepo;

    /**
     * @var OrderAdjustmentRepository
     */
    private $orderAdjustmentRepo;

    /**
     * @param UserRepository $userRepo
     * @param PaymentService $paymentService
     * @param OrderItemRepository $itemRepo
     * @param OrderAdjustmentRepository $orderAdjustmentRepo
     */
    public function __construct(UserRepository $userRepo,
                                PaymentService $paymentService,
                                OrderItemRepository $itemRepo,
                                OrderAdjustmentRepository $orderAdjustmentRepo)
    {
        $this->userRepo       = $userRepo;
        $this->paymentService = $paymentService;
        $this->itemRepo       = $itemRepo;
        $this->orderAdjustmentRepo = $orderAdjustmentRepo;
    }

    /**
     * Save invoice payment
     *
     * @param array $params
     * @return InvoicedPayment
     */
    public function save($params = [])
    {

        $updateStatus = false;
        $po_change = false;
        $notify = (array_key_exists('doNotEmailInvoice', $params)) ? false : true;

        $invoicedPayment = isset($params['invoice_payment'])
                            ? InvoicedPayment::find($params['invoice_payment'])
                            : new InvoicedPayment();

        //updated
        if(isset($params['invoice_payment']) AND isset($params['po_number'])){
            $em = [];
            $order = Order::find($params['order_id']);
            $em['items'] = [];

            foreach($order->items as $item){
                $obj_item = [];
                $obj_item['is_book'] = ($item->orderable()->first()->product_type == 'Book' AND $item->orderable()->first()->isBook())?true:false;
                $obj_item['title'] = $item->orderable()->first()->title();
                $obj_item['date'] = ($item->orderable()->first()->product_type != 'Book')?$item->orderable()->first()->date():null;
                $em['items'][] = (object) $obj_item;
            }

            $em['customer'] = $order->user->contact->name;
            $em['old_po'] = $invoicedPayment->po_number;
            $em['new_po'] = $params['po_number'];
            $po_change = $invoicedPayment->po_number !=  $params['po_number'];
        }

        if (isset($params['order_id'])) {
            $invoicedPayment->order_id = $params['order_id'];
            $invoicedPayment->save();
        }

        $poName  = isset($params['po_file'])
                   ? Constant::PO_FILE
                   : null;

        $checkName = isset($params['check_file'])
                       ? Constant::CHECK_FILE
                       : null;


        if (isset($params['po_file'])) {
            $poResponse = $this->uploadInvoice(
                PhotoUtility::getFilePathByOrderInvoiceId($params['order_id'].'/'.$invoicedPayment->id),
                $params['po_file'],
                $poName
            );

            $poName = $poResponse['image'];
            $this->removeFiles(Constant::FILE_TEMP);
        }

        if (isset($params['check_file'])) {
            $checkResponse = $this->uploadInvoice(
                PhotoUtility::getFilePathByOrderInvoiceId($params['order_id'].'/'.$invoicedPayment->id),
                $params['check_file'],
                $checkName
            );

            $checkName = $checkResponse['image'];
            $this->removeFiles(Constant::FILE_TEMP);
        }

        $invoicedPayment->po_number = isset($params['po_number']) ? $params['po_number'] : null;
        $invoicedPayment->check_number = isset($params['check_number']) ? $params['check_number'] : null;
        $invoicedPayment->po_image_mime = isset($poResponse['mime']) ? $poResponse['mime'] : null;
        $invoicedPayment->check_image_mime = isset($checkResponse['mime']) ? $checkResponse['mime'] : null;

        if ($poName) {
            $invoicedPayment->po_image = $poName;
            $invoicedPayment->po_file = $poResponse['pdf'];
        }

        if ($checkName) {
            $invoicedPayment->check_image = $checkName;
            $invoicedPayment->check_file = $checkResponse['pdf'];
        }

        $invoicedPayment->save();

        if ($params['recipients']) {
            foreach ($params['recipients'] as $recipient) {
                InvoicedPaymentRecipient::create([
                    'invoiced_payment_id' => $invoicedPayment->id,
                    'email' => $recipient
                ]);
            }
        }

        if (!isset($params['invoice_payment'])) {
            $params['shipment_county'] = StateTax::getShippingCounty($params['shipment_county']);
            $order = Order::find($params['order_id']);
            $order->status = Constant::INVOICED;
            $order->completed_at = Carbon::now();
            $order->save();
            $order->updateTotal($params);
            $order->updateBillingAndShippingAddress($params);


            if (isset($params['items'])) {
                $items = $this->itemRepo->getByIds($params['items']);

                $updateStatus = OrderService::updateStatusOfOrders($items, Constant::INVOICED, $params['tax_rate']);
                OrderService::updatePaidCharge($order);
                OrderService::checkIfSubscriptionUsed($order->user, $items, $notify);
            }

            $invoicedPayment->trans_total = $order->fresh()->total;
            $invoicedPayment->shipping_rate = $order->fresh()->shipping_rates;
            $invoicedPayment->state_tax = $order->fresh()->order_tax;
            $invoicedPayment->items = OrderUtility::createPaymentItems($order->items, $order->fresh()->total);

            $invoicedPayment->save();
            $invoicedPayment->updateBillingAndShippingAddress($params);

            if ($updateStatus) {
                event(new RegistrationConfirmationEvent($order->id, null, $invoicedPayment->id, $params['recipients'], $notify));
            }
        }

        if($po_change AND isset($params['invoice_payment'])){
            $em['subject'] = 'Purchase Order Update - Environmental Resource Center';
            $em['po_image_file'] = $invoicedPayment->po_image;
            $em['new_po_image'] = url('/').'/invoice-preview/po_image/'.$invoicedPayment->order_id.'/'.$invoicedPayment->id.'/'.$invoicedPayment->po_image;
            \Mail::send('emails.templates.po-changes',$em, function($m){
                $to_emails = [env('CSR_EMAIL','greg+csr@cspreston.com'),env('ACCOUNTING_EMAIL','greg+accounting@cspreston.com')];
                $m->to($to_emails);
                $m->subject('Purchase Order Update - Environmental Resource Center');
            });
        }

        Session::flash('PAYMENT_SUCCESS', true);
        Session::flash('TYPE', $order->checkout_combination);

        return $invoicedPayment;
    }

    /**
     * Upload files for invoice
     *
     * @param $holder
     * @param UploadedFile $file
     * @param $type
     * @return string
     */
    public function uploadInvoice($holder, UploadedFile $file, $type)
    {
        $pdf = null;
        $directory = PhotoUtility::getOrCreateFilePath($holder);
        $mimeType = $file->getMimeType();
        $fileExtension = $mimeType == 'application/pdf'
                         ? $file->getClientOriginalExtension()
                         : 'bin';
        $timestamp = Carbon::now()->getTimestamp();

        $this->removeFiles($holder, $type);

        if (!Storage::disk('local')->exists($directory)) {
            Storage::disk('local')->makeDirectory($directory, 0777, true);
        }

        $name = ($holder == Constant::FILE_TEMP)
                ? $type . '-' . $timestamp
                : $type;

        \Log::info($file);

        $filePathWithoutExt = $directory . '/' . $name;
        $filePath =  $filePathWithoutExt . '.' . $fileExtension;

        if (in_array($mimeType, ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'])) {
            $image = Image::make($file);
            $contents = $image->stream()->__toString();
            $name = $name . '.' . 'bin';
        }

        if ($mimeType == 'application/pdf') {
            $contents = file_get_contents($file);
        }

        Storage::disk('local')->put($filePath, $contents);

        \Log::info(compact('directory', 'name', 'path'));

        if ($mimeType == 'application/pdf') {
            $pdf = $name . '.' . $fileExtension;
            $im = new Imagick(storage_path('app/' . $filePath));
            $im->setResolution(240, 200);
            $im->setImageFormat('jpeg');
            $mimeType = $im->getImageMimeType();
            $im->writeImage(storage_path('app/' . $filePathWithoutExt . '.bin'));
            $name = $name . '.bin';

            $path = $directory . '/' . $type . $mimeType;
        }

        Session::flash('mime_type', $mimeType);

        return [
            'image' => $name,
            'mime' => $mimeType,
            'pdf' => $pdf,
        ];
    }

    /**
     * Remove existing file
     *
     * @param $holder
     * @param $type
     * @return $this
     */
    public function removeFiles($holder, $type = null)
    {
        $object = PhotoUtility::getInvoiceFile($holder, $type);

        if (!empty($object)) {

            if (is_array($object)) {

                foreach ($object as $file) {
                    Storage::disk('local')->delete($file);
                }
            }
        }

        return $this;
    }
    public function createPaymentItems($items, $totalAmount)
    {
        $data = (object) [];
        $itemObjects = [];

        $data->total_amount = $totalAmount;

        if ($items->count()) {
            foreach ($items as $item) {
                $itemObjects[] = (object) [
                    'id' => $item->orderable_id,
                    'object' => $item->orderable_type,
                    'contents' => $this->getItemsFromOrderItems($item->orderable)
                ];
            }

            $data->items = $itemObjects;
        }

        return $data;
    }
}