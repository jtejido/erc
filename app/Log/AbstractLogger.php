<?php namespace App\Log;

abstract class AbstractLogger
{

    /**
     * Log error
     *
     * @param   string  $error
     * @param   string  $context
     * @return  void
     */
    abstract protected function addError($error, $context);

}