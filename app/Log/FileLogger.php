<?php namespace App\Log;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class FileLogger extends AbstractLogger
{

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var StreamHandler
     */
    private $handler;

    /**
     * FileLogger constructor
     */
    public function __construct()
    {
        $this->logger = new Logger('erc_log');
    }

    /**
     * Set stream handler
     *
     * @param   string $filePath    filepath should be relative to the logs directory
     * @return  void
     */
    public function setHandler($filePath)
    {
        try {
            if ($this->handler) {
                $this->logger->popHandler();
            }
        }
        catch (\Exception $e) {}

        $filepath = storage_path() . '/logs/' . $filePath . '.log';

        $this->logger->pushHandler(
            $this->handler = new StreamHandler($filepath, Logger::ERROR)
        );
    }

    /**
     * Log error
     *
     * @param   string  $error
     * @param   string  $context
     * @return  void
     */
    public function addError($error, $context = 'erc_log')
    {
        $this->setHandler($context);

        $this->logger->addError($error);
    }

}