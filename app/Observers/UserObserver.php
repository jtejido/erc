<?php namespace App\Observers;

use App\Facades\UserService;

class UserObserver extends AbstractObserver
{

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model)
    {
        UserService::saveAsAgent($model);
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model) {}
}