<?php namespace App\Observers;

use App\Facades\CourseMillService;
use App\Facades\OrderService;
use App\Facades\DiscountService;
use App\Facades\OrderUtility;
use App\Jobs\CreateStudentFromRegistration;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Mockery\Exception;
use Vinkla\Hashids\Facades\Hashids;

class RegistrationOrderObserver extends AbstractObserver
{

    use DispatchesJobs;

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {

    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model)
    {
        if($model::$FIRE_EVENTS) {
            OrderService::addOrder($model);
            $model->slug = Hashids::encode($model->id, $model->originating_agent_user_id);
            $model->save();
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('status', $attr)) {

            if (OrderUtility::isCompleteOrInvoiced($model)) {
                $job = (new CreateStudentFromRegistration($model));
                $this->dispatch($job);
            }
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {

    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model)
    {
        if($model::$FIRE_EVENTS) {
            DiscountService::extractFromRegistrations($model->classRegistrations);
            OrderService::deleteClassRegistration($model);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model)
    {
        if($model::$FIRE_EVENTS) {
            DiscountService::applyDiscountAndSortingInDeleted($model);
        }
    }

}