<?php namespace App\Observers;

use App\Facades\OrderService;

class ClientCorpSeatCreditObserver extends AbstractObserver
{

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model)
    {
        OrderService::addOrder($model);
        OrderService::setPriceCharged($model);
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model){}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('price', $attr)) {
            OrderService::setPriceCharged($model);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model) {}
}