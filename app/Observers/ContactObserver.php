<?php namespace App\Observers;

use App\Facades\CourseMillService;

class ContactObserver extends AbstractObserver
{

    /**
     * @var array
     */
    private $editable = [
        'first_name',
        'last_name',
        'address1',
        'city',
        'state',
        'zip',
        'phone',
        'deleted_at'
    ];

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (count(array_intersect($this->editable, $attr))) {
            if (!is_null($model->course_mill_user_id)) {
                CourseMillService::createUpdateStudentFromCustomer($model);
            }
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model) {}

}