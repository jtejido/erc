<?php namespace App\Observers;

abstract class AbstractObserver
{
    /**
     * @param $model
     * @return mixed
     */
    abstract public function creating($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function created($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function saving($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function saved($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function updating($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function updated($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function deleting($model);

    /**
     * @param $model
     * @return mixed
     */
    abstract public function deleted($model);

}