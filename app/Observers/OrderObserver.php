<?php namespace App\Observers;

use App\Facades\OrderService;
use App\Facades\OrderUtility;
use App\Facades\UserService;
use App\Facades\DiscountService;

class OrderObserver extends AbstractObserver
{

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model){}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model)
    {

    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('status', $attr)) {

            if (OrderUtility::isCompleteOrInvoiced($model)) {
                UserService::saveCorporateSeatCreditToUser($model);
                DiscountService::searchAndStartSubscription($model->items);
            }
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model) {}
}