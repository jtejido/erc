<?php namespace App\Observers;

use App\Facades\OrderService;

class ClientYearlySubscriptionObserver extends AbstractObserver
{
    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model)
    {
        OrderService::setPriceCharged($model->yearlySubscription);
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model){}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('temporary_price', $attr)) {
            OrderService::setPriceCharged($model->yearlySubscription);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model) {}
}