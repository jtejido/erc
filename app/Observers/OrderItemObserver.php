<?php namespace App\Observers;

use App\Events\AssociateContactRegistrationEvent;
use App\Facades\OrderUtility;
use App\Facades\OrderService;

class OrderItemObserver extends AbstractObserver
{

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('status', $attr)) {

            if (OrderUtility::isCompleteOrInvoiced($model)) {
                event(new AssociateContactRegistrationEvent($model->order->user->id, $model->id));
            }
        }

        if (in_array('price_charged', $attr)) {

            if($model->order->has_tax and $model->is_product_order)
                $model->order->updateOrderTax();

            OrderService::updateOrderTotal($model->order);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model)
    {
        if($model::$FIRE_EVENTS) {
            OrderService::removeSubItems($model);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model)
    {
        if($model::$FIRE_EVENTS) {
            OrderService::checkRemovedItem($model);

            if ($model->order) {
                OrderService::updateOrderTotal($model->order);
            }
        }
    }

}