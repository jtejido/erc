<?php namespace App\Observers;

use App\Facades\OrderService;

class OnSiteTrainingObserver extends AbstractObserver 
{
	/**
     * @param $model
     * @return mixed
     */
    public function creating($model){}

    /**
     * @param $model
     * @return mixed
     */
    public function created($model) 
    {
    	OrderService::addOrder($model);
        OrderService::setPriceCharged($model);
    }

    /**
     * @param $model
     * @return mixed
     */
    public function saving($model){}

    /**
     * @param $model
     * @return mixed
     */
    public function saved($model){}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model) {}

    /**
     * @param $model
     * @return mixed
     */
    public function deleting($model){}

    /**
     * @param $model
     * @return mixed
     */
    public function deleted($model){}
}