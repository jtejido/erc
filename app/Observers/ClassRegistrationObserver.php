<?php namespace App\Observers;

use App\Events\StudentMarkedConfirmedEvent;
use App\Facades\CertificateService;
use App\Facades\OrderModificationService;
use App\Facades\OrderService;
use App\Facades\DiscountService;
use App\Facades\OrderUtility;
use Carbon\Carbon;

class ClassRegistrationObserver extends AbstractObserver
{

    /**
     * @param $model
     * @return mixed|void
     */
    public function creating($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function created($model)
    {
        if($model::$FIRE_EVENTS) {
            DiscountService::createSubscriptionDiscount($model);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function saving($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function saved($model) {}

    /**
     * @param $model
     * @return mixed|void
     */
    public function updating($model)
    {
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function updated($model)
    {
        $attr = [];

        foreach ($model->getDirty() as $attribute => $value) {
            $attr[] = $attribute;
        }

        if (in_array('item_charge', $attr)) {
            OrderService::setPriceCharged($model->registrationOrder);
        }

        if (in_array('is_new', $attr)) {
            OrderService::setPriceCharged($model->registrationOrder);
        }

        if (in_array('corporate_seat_credit_applied', $attr)) {
            $registrationOrder = $model->registrationOrder;

            DiscountService::reCountOrderOfClassRegistrationCorpSeatChange($registrationOrder);
        }

        if (in_array('is_half_priced', $attr)) {
            DiscountService::createHalfPriceDiscount($model);
        }


        if (in_array('attended', $attr)) {
            if($model->isConfirmed() == 1 && !$model->certificate_file_path) {
                CertificateService::saveCertToDBandStorage($model->attendee_contact_id, $model->registration_order_id);
            }
            if($model->isConfirmed() == 1 && !$model->certificate_sent) {
                event(new StudentMarkedConfirmedEvent($model->attendee_contact_id, $model->registration_order_id, 1));
            }
        }

        if (in_array('exam_flag', $attr) || in_array('meeting_flag', $attr)) {
            if($model->isConfirmed() == 1 && !$model->certificate_file_path) {
                CertificateService::saveCertToDBandStorage($model->attendee_contact_id, $model->registration_order_id);
            }
            if($model->isConfirmed() == 1 && !$model->certificate_sent) {
                event(new StudentMarkedConfirmedEvent($model->attendee_contact_id, $model->registration_order_id, 1));
            }
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleting($model)
    {
        if($model::$FIRE_EVENTS) {
            if (!OrderUtility::isCompleteOrInvoiced($model->registrationOrder)) {
                DiscountService::extractFromRegistrations($model);

                // Todo: Move this to appropriate resetting of discounts
                DiscountService::resetHWMDOTDiscounts($model);
            }

            DiscountService::removeDiscounts($model);
        }
    }

    /**
     * @param $model
     * @return mixed|void
     */
    public function deleted($model)
    {
        if($model::$FIRE_EVENTS) {
            OrderService::setPriceCharged($model->registrationOrder);
            OrderService::checkRemovedClassRegistrations($model);
        }
    }

}