<?php namespace App\Exceptions;

use RuntimeException;

class BookNotFoundException extends RuntimeException
{
    # nothing to override
}
