<?php

namespace App\Exceptions;

use Session;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        } elseif($e instanceof BookNotFoundException) {
            return redirect()->route('products');
        } elseif($e instanceof FileNotFoundException) {
            return response()->make(view('exceptions.filenotfound'), 404);
        } elseif ($e instanceof TokenMismatchException) {
            if ($request->ajax()) {
                $authenticated = false;
                $route = route('get.login');

                auth()->logout();

                $url = Session::has('last_activity_url')
                    ? Session::get('last_activity_url')
                    : $request->get('current_location');

                Session::put('last_activity_url', $url);

                return Response::json([
                    'authenticated' => $authenticated,
                    'route' => $route
                ]);
            }

            return redirect()->route('get.login')->withErrors('token_error', 'Your session has expired. Please login again to do further transactions.');
        }

        return parent::render($request, $e);
    }
}
