<?php namespace App\Exceptions;

use RuntimeException;

class FileNotFoundException extends RuntimeException
{
    /**
     * @var string
     */
    private $path;

    public function __construct($path)
    {
        parent::__construct(sprintf('The file "%s" does not exist', $path));
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

}
