<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrainingCancellationEvent extends Event
{
    use SerializesModels;
    public $type;
    public $id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($type, $id)
    {
        $this->type    = $type;
        $this->id      = $id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
