<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProfileUpdateEvent extends Event
{
    use SerializesModels;
    public $user_id;
    public $is_password;
    public $email_old;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $is_password = false, $email_old = '')
    {
        $this->user_id = $user_id;
        $this->is_password = $is_password;
        if (strlen($email_old) > 0) {
            $this->email_old = $email_old;
        }
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
