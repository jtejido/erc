<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SwapUserEvent extends Event
{
    use SerializesModels;

    public $actor_id;
    public $user_id;
    public $registrable_id;
    public $added;
    public $removed;
    /**
     * @var bool
     */
    public $notify;
    /**
     * @var bool
     */
    public $notify_csr;
    /**
     * @var bool
     */
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($actor_id, $user_id, $registrable_id, $added, $removed,
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->actor_id = $actor_id;
        $this->user_id = $user_id;
        $this->registrable_id = $registrable_id;
        $this->added = $added;
        $this->removed = $removed;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
