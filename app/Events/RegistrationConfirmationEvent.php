<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Order;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Queue\SerializesModels;

class RegistrationConfirmationEvent extends Event
{
    use SerializesModels;
    public $order;
    public $masked_number;
    public $invoice_id;
    public $recipients;
    public $notify;
    public $notify_csr;
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order_id,
                                $masked_number = null,
                                $invoice_id = null,
                                $recipients = [],
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->order = $order_id;
        $this->masked_number = $masked_number;
        $this->invoice_id = $invoice_id;
        $this->recipients = $recipients;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
