<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmailAccountingModificationConfirmationEvent extends Event
{
    use SerializesModels;
    public $order_adjustment_ids;
    public $agent;
    public $payment;
    public $notify;
    public $notify_csr;
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order_adjustment_ids, $agent, $payment = null,
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->order_adjustment_ids = $order_adjustment_ids;
        $this->agent = $agent;
        $this->payment = $payment;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
