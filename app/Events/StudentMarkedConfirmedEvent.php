<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StudentMarkedConfirmedEvent extends Event
{
    use SerializesModels;

    public $contactId;
    public $regOrderId;

    /**
     * This is either 1 - present or 2 - absent
     * @var $status
     */
    public $status;

    /**
     * Create a new event instance.
     *
     * @param $contactId
     * @param $regOrderId
     * @param $status
     */
    public function __construct($contactId, $regOrderId, $status)
    {
        $this->contactId = $contactId;
        $this->regOrderId = $regOrderId;
        $this->status = $status;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
