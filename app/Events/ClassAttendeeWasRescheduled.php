<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ClassAttendeeWasRescheduled extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $current_classRegistration;
    /**
     * @var
     */
    public $classRegistration;
    /**
     * @var
     */
    public $notify;

    /**
     * Create a new event instance.
     *
     * @param $current_classRegistration
     * @param $classRegistration
     * @param $notify
     */
    public function __construct($current_classRegistration, $classRegistration, $notify)
    {
        //
        $this->current_classRegistration = $current_classRegistration;
        $this->classRegistration = $classRegistration;
        $this->notify = $notify;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
