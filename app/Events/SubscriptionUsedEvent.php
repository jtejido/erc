<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SubscriptionUsedEvent extends Event
{
    use SerializesModels;
    public $agent;
    public $classRegistrations;
    /**
     * @var bool
     */
    public $notify;
    /**
     * @var bool
     */
    public $notify_csr;
    /**
     * @var bool
     */
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($agent, $classRegistrations,
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->agent = $agent;
        $this->classRegistrations = $classRegistrations;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
