<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AssociateContactRegistrationEvent extends Event
{
    use SerializesModels;

    public $agent_id;
    public $order_item_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($agent_id, $order_item_id)
    {
        $this->agent_id = $agent_id;
        $this->order_item_id = $order_item_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
