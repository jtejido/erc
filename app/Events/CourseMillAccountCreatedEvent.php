<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CourseMillAccountCreatedEvent extends Event
{
    use SerializesModels;

    public $user;
    public $accounts = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $accounts = [])
    {
        $this->user = $user;
        $this->accounts = $accounts;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
