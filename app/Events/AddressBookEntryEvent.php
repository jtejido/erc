<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AddressBookEntryEvent extends Event
{
    use SerializesModels;

    public $user_id;
    public $details;

    /**
     * Password provided when user is newly registered
     * @var string
     */
    public $password;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $details, $password = null)
    {
        $this->user_id = $user_id;
        $this->details = $details;
        $this->password = $password;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
