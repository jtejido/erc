<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CourseMillAccountCreateFailedEvent extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $registrationOrder;
    /**
     * @var
     */
    public $exception;

    /**
     * Create a new event instance.
     *
     * @param $registrationOrder
     * @param $exception
     */
    public function __construct($registrationOrder, $exception)
    {

        $this->registrationOrder = $registrationOrder;
        $this->exception = $exception;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
