<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OnsiteRegistrationConfirmationEvent extends Event
{
    use SerializesModels;
    public $user_id;
    public $date;
    public $address;
    public $files;
    public $new;
    public $notify;
    public $notify_csr;
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user_id, $date, $address, $files = [], $new = true, $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->user_id = $user_id;
        $this->date    = $date;
        $this->address = $address;
        $this->files = $files;
        $this->new = $new;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
