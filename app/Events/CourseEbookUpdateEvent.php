<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CourseEbookUpdateEvent extends Event
{
    use SerializesModels;
    /**
     * @var
     */
    public $bookId;
    /**
     * @var
     */
    public $fileEntryIds = [];
    /**
     * @var bool
     */
    public $notify;
    /**
     * @var bool
     */
    public $notify_csr;
    /**
     * @var bool
     */
    public $notify_accounting;

    /**
     * Create a new event instance.
     *
     * @param $courseId
     * @param $fileEntryId
     */
    public function __construct($bookId, $fileEntryIds = [],
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        //
        $this->bookId = $bookId;
        $this->fileEntryIds = $fileEntryIds;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
