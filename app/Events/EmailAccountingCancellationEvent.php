<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmailAccountingCancellationEvent extends Event
{
    use SerializesModels;

    public $agent;
    public $user;
    public $paid;
    public $deferredHalfPriceFees;
    public $deferredGoupDiscountFees;
    public $additionalFee;
    public $deduction;
    public $fee;
    public $refund;
    public $orderTotal;
    public $classRegistrationIds;
    /**
     * @var bool
     */
    public $notify;
    /**
     * @var bool
     */
    public $notify_csr;
    /**
     * @var bool
     */
    public $notify_accounting;

    /**
     * @param $agent
     * @param $user
     * @param $paid
     * @param $deferredHalfPriceFees
     * @param $deferredGoupDiscountFees
     * @param $addionalFee
     * @param $deduction
     * @param $fee
     * @param $refund
     * @param $orderTotal
     * @param $classRegistrationIds
     */
    public function __construct($agent,
                                $user,
                                $paid,
                                $deferredHalfPriceFees,
                                $deferredGoupDiscountFees,
                                $addionalFee,
                                $deduction,
                                $fee,
                                $refund,
                                $orderTotal,
                                $classRegistrationIds,
                                $notify = true,
                                $notify_csr = true,
                                $notify_accounting = true)
    {
        $this->agent = $agent;
        $this->user = $user;
        $this->paid = $paid;
        $this->deferredHalfPriceFees = $deferredHalfPriceFees;
        $this->deferredGoupDiscountFees = $deferredGoupDiscountFees;
        $this->additionalFee = $addionalFee;
        $this->deduction = $deduction;
        $this->fee = $fee;
        $this->refund = $refund;
        $this->orderTotal = $orderTotal;
        $this->classRegistrationIds = $classRegistrationIds;
        $this->notify = $notify;
        $this->notify_csr = $notify_csr;
        $this->notify_accounting = $notify_accounting;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
