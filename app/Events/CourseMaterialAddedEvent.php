<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Course;
use App\Models\FileEntry;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CourseMaterialAddedEvent extends Event
{
    use SerializesModels;

    public $file;
    public $course;
    /**
     * @var bool
     */
    public $notify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(FileEntry $file, $course, $notify = true)
    {
        $this->file = $file;
        $this->course = $course;
        $this->notify = $notify;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
