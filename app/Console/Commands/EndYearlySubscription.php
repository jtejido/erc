<?php

namespace App\Console\Commands;

use App\Repositories\ClientYearlySubscriptionRepository;
use App\Repositories\UserYearlySubscriptionRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

class EndYearlySubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'end:yearly-subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'End yearly subscription.';

    /**
     * @var UserYearlySubscriptionRepository
     */
    private $userYearlySubRepo;

    /**
     * @var ClientYearlySubscriptionRepository
     */
    private $clientYearlySubRepo;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserYearlySubscriptionRepository $userYearlySubRepo,
                                ClientYearlySubscriptionRepository $clientYearlySubRepo)
    {
        parent::__construct();

        $this->userYearlySubRepo = $userYearlySubRepo;
        $this->clientYearlySubRepo = $clientYearlySubRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = Carbon::today()->toDateTimeString();

        $userYearlySubs = $this->userYearlySubRepo->getAll();

        if ($userYearlySubs->count()) {
            $userYearlySubs->each(function($userYearlySub) use ($currentDate) {
                $clientSubscription = $userYearlySub->clientSubscription;

                if ($clientSubscription->subscription_exp_date and
                    $clientSubscription->subscription_exp_date < $currentDate) {
                    $userYearlySub->delete();

                    echo '.';
                }
            });

            echo "Done.";
        }
    }
}
