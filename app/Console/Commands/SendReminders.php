<?php

namespace App\Console\Commands;


use App\Facades\ReminderHistoryService;
use App\Models\RegistrationOrder;
use App\Services\Mailer\EmailTemplateMailer;
use App\Utilities\Constant;
use Illuminate\Console\Command;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send webcast instruction to registered users';
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create a new command instance.
     * @param EmailTemplateMailer $mailer
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        parent::__construct();
        $this->mailer = $mailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url = app('url');
        $url->forceRootUrl(env('APP_URL')); // Force the application URL

        $regOrders = RegistrationOrder::all();

        foreach ($regOrders as $regOrder) {

            $flag = false;

            if($regOrder->isCourse()) {
                // handle courses

            } else {
                // handle classes

                $class = $regOrder->registrable;

                if($class->course->topic) {
                    $flag = $class->isReminderEligible($class->course->topic->duration);
                }

            }

            if(!$flag) continue;

            $this->sendToGroup($regOrder);

        }

    }


    /**
     * @param $regOrder
     */
    public function sendToGroup($regOrder)
    {
        foreach ($regOrder->classRegistrations as $classRegistration) {


            if (!$classRegistration->contact->user)
                continue;

            $this->sendReminder($classRegistration);
        }
    }

    /**
     * @param $classRegistration
     * @return bool
     */
    public function sendReminder($classRegistration)
    {
        $regOrder = $classRegistration->registrationOrder;
        $class = $regOrder->registrable;
        $course = $regOrder->registrable->course;
        $contact = $classRegistration->contact;
        $user = $classRegistration->contact->user;
        $subject = 'Environmental Resource Center - Certification Reminder';
        $slug = 'cert-reminder';
        $data = [
            'to' => $user->email,
            'subject' => $subject,
            'data' => [
                'name'      => $contact->name,
                'startDate' => $class->start->toDayDateTimeString(),
                'endDate'   => $class->end->toDayDateTimeString(),
                'course'    => $course->title,
                'cbtsPage' => route('courses') . '?type=3' ,
                'coursesPage' => route('courses'),
                'homePage' => route('home'),

            ]
        ];

        if ($this->mailer->send($slug, $data)) {
            $params = [];
            $params['to'] = $user->id;
            $params['subject'] = $subject;
            $params['data'] = serialize($data);
            $params['slug'] = $slug;
            $params['remindable_id'] = $regOrder->registrable_id;
            $params['remindable_type'] = $regOrder->registrable_type;
            ReminderHistoryService::log($params);
            return true;
        }
        return false;
    }
}
