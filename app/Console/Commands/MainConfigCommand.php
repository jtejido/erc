<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MainConfigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'erc:config';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Main Configuration for ERC web';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("\nMigrating Tables:");
        $this->call('migrate:refresh');

        $this->comment("\nSeeding Tables:");
        $this->call('db:seed');
    }
}
