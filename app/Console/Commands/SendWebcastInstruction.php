<?php namespace App\Console\Commands;

use App\Models\CourseClass;
use App\Models\RegistrationOrder;
use App\Services\Mailer\EmailTemplateMailer;
use Illuminate\Console\Command;

class SendWebcastInstruction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:webcast 
            {classId=all : id of class}
            {--user=all : send email now}
            {--now : send email now}
            {--resend : resend email reminder}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send webcast instruction to registered users';
    /**
     * @var EmailTemplateMailer
     */
    private $mailer;

    /**
     * Create a new command instance.
     *
     */
    public function __construct(EmailTemplateMailer $mailer)
    {
        parent::__construct();
        $this->mailer = $mailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('classId');
        $now = $this->option('now');
        $resend = $this->option('resend');
        $user = $this->option('user');
        $url = app('url');
        $url->forceRootUrl(env('APP_URL')); // Force the application URL

        if ($id != 'all') {
            $class = CourseClass::findOrFail($id);
            $regOrders = $class->registrations;
        } else {
            $regOrders = RegistrationOrder::all();
        }

        foreach ($regOrders as $regOrder) {
            if($regOrder->isCourse()
                || ! $regOrder->registrable->course->isWebcast()
                || (! $regOrder->registrable->isStartingInAWeek
                    && !$now)
                )
                continue;

            if(! $regOrder->registrable->webcast_notification_flag
                && !$now)
                continue;

            $this->sendToGroup($regOrder, $resend, $user);

        }

    }

    /**
     * @param $classRegistration
     * @param $regOrder
     * @return bool
     */
    public function sendWebcastReminder($classRegistration)
    {
        $regOrder = $classRegistration->registrationOrder;
        $class = $regOrder->registrable;
        $course = $regOrder->registrable->course;
        $contact = $classRegistration->contact;
        $user = $classRegistration->contact->user;
        $subject = 'Webcast Instructions - ' . $course->title . ' - ' . $class->start->toFormattedDateString();
        $data = [
            'to' => $user->email,
            'subject' => $subject,
            'data' => [
                'name' => $contact->name,
                'webcast' => $course->title,
                'startDate' => $class->start->toDayDateTimeString(),
                'endDate' => $class->end->toDayDateTimeString(),
                'dateRange' => $class->date_range,
                'webcast_link' => $regOrder->meetingLink($regOrder->meeting_link),
                'exam_link' => $regOrder->examLink($regOrder->test_link),
                'instruction' => $class->instructions,
                'cbtsPage' => route('courses') . '?type=3',
                'coursesPage' => route('courses'),
                'homePage' => route('home'),
            ]
        ];

        if ($this->mailer->send('webcast-instruction', $data)) {
            $classRegistration->setSendReminderTrue();
            return true;
        }
        return false;
    }

    /**
     * @param $regOrder
     * @param $resend
     * @param $user
     */
    public function sendToGroup($regOrder, $resend, $user)
    {
        foreach ($regOrder->classRegistrations as $classRegistration) {

            if ($classRegistration->reminder_flag && !$resend)
                continue;

            if (!$classRegistration->contact->user)
                continue;

            if ($user != 'all' && $classRegistration->contact->user->id != $user)
                continue;

            $this->sendWebcastReminder($classRegistration);
        }
    }
}
