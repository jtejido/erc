<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\MainConfigCommand::class,
        \App\Console\Commands\SendWebcastInstruction::class,
        \App\Console\Commands\EndYearlySubscription::class,
        \App\Console\Commands\SendReminders::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('reminder:webcast')->everyMinute();
        $schedule->command('end:yearly-subscription')->daily();
        $schedule->command('reminder:send')->daily();
    }
}
