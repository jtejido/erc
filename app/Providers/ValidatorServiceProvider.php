<?php namespace App\Providers;

use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{

    /** 
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('password_check', function($attrib, $val, $params)
        {
            return Hash::check($val, $params[0]);
        });

        Validator::extend('new_password_check', function($attrib, $val, $params)
        {
            return !(Hash::check($val, $params[0]));
        });

        Validator::extend('unique_without', function($attrib, $val, $params)
        {
            if ($params[0]) return true;

            $user = User::where('email', $val)->first();

            if ($user) {
                return false;
            }

            return true;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
