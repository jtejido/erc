<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CustomerRegisteredEvent' => [
            'App\Listeners\CustomerRegisteredListener',
        ],
        'App\Events\RegistrationConfirmationEvent' => [
            'App\Listeners\OrderConfirmationListener',
            'App\Listeners\OrderInstructionConfirmationListener',
            'App\Listeners\AttendeesRegistrationConfirmationListener',
            'App\Listeners\UserRegistrationConfirmationListener',
        ],
        'App\Events\AddressBookEntryEvent' => [
            'App\Listeners\AddressBookNotificationListener'
        ],
        'App\Events\ProfileUpdateEvent' => [
            'App\Listeners\PasswordUpdateNotificationListener',
            'App\Listeners\EmailUpdateNotificationListener'
        ],
        'App\Events\OnsiteRegistrationConfirmationEvent' => [
            'App\Listeners\OnsiteRegistrationConfirmationListener',
        ],
        'App\Events\SwapUserEvent' => [
            'App\Listeners\SwapUserAddedListener',
            'App\Listeners\SwapUserRemovedListener',
            'App\Listeners\NotifyAgentWhenSwapListener'
        ],
        'App\Events\TrainingCancellationEvent' => [
            'App\Listeners\TrainingCancellationListener',
        ],
        'App\Events\StudentMarkedConfirmedEvent' => [
            'App\Listeners\StudentMarkedConfirmedListener',
        ],
        'App\Events\CourseEbookUpdateEvent' => [
            'App\Listeners\CourseEbookUpdateListener',
        ],
        'App\Events\CourseMillAccountCreatedEvent' => [
            'App\Listeners\CourseMillAccountCreatedListener',
        ],
        'App\Events\AssociateContactRegistrationEvent' => [
            'App\Listeners\AssociateContactRegistrationListener',
        ],
//        'App\Events\AssociateContactAccountEvent' => [
//            'App\Listeners\AssociateContactAccountListener',
//        ],
        'App\Events\ClassRegistrationCancelledEvent' => [
            'App\Listeners\ClassRegistrationCancelledListener',
        ],
        'App\Events\EmailAccountingCancellationEvent' => [
            'App\Listeners\EmailAccountingCancellationListener',
        ],
        'App\Events\EmailAccountingModificationConfirmationEvent' => [
            'App\Listeners\EmailAccountingModificationConfirmationListener',
        ],
        'App\Events\SubscriptionUsedEvent' => [
            'App\Listeners\SubscriptionUsedListener',
        ],
        'App\Events\CourseMaterialAddedEvent' => [
            'App\Listeners\CourseMaterialAddedListener',
        ],
        'App\Events\CourseMillAccountCreateFailedEvent' => [
            'App\Listeners\CourseMillAccountCreateFailedListener'
        ],
        'App\Events\ClassAttendeeWasRescheduled' => [
            'App\Listeners\ClassAttendeeWasRescheduledListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
