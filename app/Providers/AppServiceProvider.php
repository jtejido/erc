<?php

namespace App\Providers;

use App;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Relation::morphMap([
            'Course'                => App\Models\Course::class,
            'CourseClass'           => App\Models\CourseClass::class,
            'Book'                  => App\Models\Book::class,
            'ProductOrders'         => App\Models\ProductOrders::class,
            'RegistrationOrder'     => App\Models\RegistrationOrder::class,
            'OnsiteTraining'        => App\Models\OnsiteTraining::class,
            'Order'                 => App\Models\Order::class,
            'ClassRegistration'     => App\Models\ClassRegistration::class,
            'StateHandout'          => App\Models\StateHandout::class
        ]);

        App::bind('user_service', function()
        {
            return new App\Services\UserService(
                new App\Repositories\RoleRepository(
                    new App\Models\Role()
                ),
                new App\Repositories\UserRepository(
                    new App\Models\User()
                ),
                new App\Repositories\ContactRepository(
                    new App\Models\Contact()
                ),
                new App\Services\Mailer\RegistrationMailer(
                    new App\Mail\Mailer(
                        new App\Log\FileLogger()
                    ),
                    new App\Services\Mailer\EmailTemplateMailer(
                        new App\Mail\Mailer(
                            new App\Log\FileLogger()
                        )
                    )
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                ),
                new App\Repositories\ActingOriginatingAgentRepository(
                    new App\Models\ActingOriginatingAgent()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                ),
                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                )
            );
        });

        App::bind('course_util', function()
        {
            return new App\Utilities\CourseUtility(
                new App\Repositories\CourseRepository(
                    new App\Models\Course()
                ),

                new App\Repositories\ClassRepository(
                    new App\Models\CourseClass(),
                    new App\Repositories\CourseRepository(
                        new App\Models\Course()
                    )
                ),

                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),

                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                ),

                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                )
            );
        });

        App::bind('order_util', function()
        {
            return new App\Utilities\OrderUtility(
                new App\Repositories\OrderRepository(
                  new App\Models\Order()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                ),
                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                ),
                new App\Repositories\ClientYearlySubscriptionRepository(
                    new App\Models\ClientYearlySubscription()
                ),
                new App\Repositories\ProductOrdersRepository(
                    new \Illuminate\Container\Container(),
                    new \Illuminate\Database\Eloquent\Collection()
                ),
                new App\Repositories\ActingOriginatingAgentRepository(
                    new App\Models\ActingOriginatingAgent()
                ),
                new App\Repositories\OrderAdjustmentRepository(
                    new App\Models\OrderAdjustment()
                )
            );
        });

        App::bind('photo_util', function()
        {
            return new App\Utilities\PhotoUtility();
        });

        App::bind('order_service', function()
        {
            return new App\Services\OrderService(
                new App\Repositories\OrderRepository(
                  new App\Models\Order()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                ),
                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                ),
                new App\Repositories\YearlySubscriptionRepository(
                    new App\Models\YearlySubscription()
                ),
                new App\Repositories\ActingOriginatingAgentRepository(
                    new App\Models\ActingOriginatingAgent()
                ),
                new App\Repositories\ClassCombinationRepository(
                    new App\Models\ClassCombination()
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                ),
                new App\Repositories\ClassHalfPriceDiscountRepository(
                    new App\Models\ClassHalfPriceDiscount()
                ),
                new App\Repositories\ClassCouponDiscountRepository(
                    new App\Models\ClassCouponDiscount()
                ),
                new App\Repositories\ClassCombinationDiscountRepository(
                    new App\Models\ClassCombinationDiscount()
                ),
                new App\Repositories\ClassCreditDiscountRepository(
                    new App\Models\ClassCreditDiscount()
                ),
                new App\Repositories\OrderAdjustmentDiscountRepository(
                    new App\Models\OrderAdjustmentDiscount()
                ),
                new App\Repositories\RemovedDiscountByCreditRepository(
                    new App\Models\RemovedDiscountByCredit()
                )
            );
        });

        App::bind('disc_service', function()
        {
            return new App\Services\DiscountService(
                new App\Repositories\ContactRepository(
                    new App\Models\Contact()
                ),
                new App\Repositories\ClassRepository(
                    new App\Models\CourseClass(),
                    new App\Repositories\CourseRepository(
                        new App\Models\Course()
                    )
                ),
                new App\Repositories\OrderRepository(
                    new App\Models\Order()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                ),
                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                ),
                new App\Repositories\ClassCombinationRepository(
                    new App\Models\ClassCombination()
                ),
                new App\Repositories\ClassCombinationDiscountRepository(
                    new App\Models\ClassCombinationDiscount()
                ),
                new App\Repositories\CouponRepository(
                    new App\Models\Coupon(),
                    new App\Repositories\CourseRepository(
                        new App\Models\Course()
                    ),
                    new App\Repositories\CouponCourseRepository(
                        new App\Models\CouponCourse()
                    )
                ),
                new App\Repositories\CouponDiscountRepository(
                    new App\Models\CouponDiscount()
                ),
                new App\Repositories\ClassCouponDiscountRepository(
                    new App\Models\ClassCouponDiscount()
                ),
                new App\Repositories\OrderAdjustmentDiscountRepository(
                    new App\Models\OrderAdjustmentDiscount()
                ),
                new App\Repositories\ClassHalfPriceDiscountRepository(
                    new App\Models\ClassHalfPriceDiscount()
                ),
                new App\Repositories\ClassMilitaryDiscountRepository(
                    new App\Models\ClassMilitaryDiscount()
                ),
                new App\Repositories\ClassCreditDiscountRepository(
                    new App\Models\ClassCreditDiscount()
                )
            );
        });

        App::bind('disc_util', function()
        {
            return new App\Utilities\DiscountUtility(
                new App\Repositories\CouponDiscountRepository(
                    new App\Models\CouponDiscount()
                ),

                new App\Repositories\ClassCouponDiscountRepository(
                    new App\Models\ClassCouponDiscount()
                )
            );
        });

        App::bind('cm_service', function()
        {
            return new App\Services\CourseMillService(
                new App\Repositories\OrderRepository(
                    new App\Models\Order()
                )
            );
        });

        App::bind('cert_service', function()
        {
           return new App\Services\CertificateService(
               new App\Repositories\ClassRepository(
                   new App\Models\CourseClass(),
                   new App\Repositories\CourseRepository(
                       new App\Models\Course()
                   )
               ),
               new App\Repositories\ClassRegistrationRepository(
                   new App\Models\ClassRegistration()
               ),
               new App\Repositories\ContactRepository(
                   new App\Models\Contact()
               ),
               new App\Repositories\RegistrationOrderRepository(
                   new App\Models\RegistrationOrder(),
                   new App\Repositories\ClassRegistrationRepository(
                       new App\Models\ClassRegistration()
                   ),
                   new App\Repositories\ActingOriginatingAgentRepository(
                       new App\Models\ActingOriginatingAgent()
                   )
               ),
               new App\Repositories\OrderItemRepository(
                   new App\Models\OrderItem(),
                   new App\Repositories\OrderRepository(
                       new App\Models\Order()
                   ),
                   new App\Repositories\ActingOriginatingAgentRepository(
                       new App\Models\ActingOriginatingAgent()
                   ),
                   new App\Repositories\ClassRegistrationRepository(
                       new App\Models\ClassRegistration()
                   )
               ),
               new App\Repositories\CourseRepository(
                   new App\Models\Course()
               )
           );
        });

        App::bind('redirect_service', function() {
            return new App\Services\RedirectService(
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                )
            );
        });

        App::bind('class_service', function() {
           return new App\Services\ClassService(
               new App\Repositories\CourseRepository(
                   new App\Models\Course()
               ),
               new App\Repositories\ClassRepository(
                   new App\Models\CourseClass(),
                   new App\Repositories\CourseRepository(
                       new App\Models\Course()
                   )
               )
           );
        });

        App::bind('order_mod_service', function()
        {
            return new App\Services\OrderModificationService(
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                ),
                new App\Repositories\RegistrationOrderRepository(
                    new App\Models\RegistrationOrder(),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    )
                ),
                new App\Repositories\ClassCombinationRepository(
                    new App\Models\ClassCombination()
                ),
                new App\Repositories\ClassCombinationDiscountRepository(
                    new App\Models\ClassCombinationDiscount()
                ),
                new App\Repositories\ActingOriginatingAgentRepository(
                    new App\Models\ActingOriginatingAgent()
                ),
                new App\Repositories\ClassHalfPriceDiscountRepository(
                    new App\Models\ClassHalfPriceDiscount()
                ),
                new App\Repositories\ClassCouponDiscountRepository(
                    new App\Models\ClassCouponDiscount()
                ),
                new App\Repositories\ClassCreditDiscountRepository(
                    new App\Models\ClassCreditDiscount()
                ),
                new App\Repositories\OrderAdjustmentDiscountRepository(
                    new App\Models\OrderAdjustmentDiscount()
                ),
                new App\Repositories\RemovedDiscountByCreditRepository(
                    new App\Models\RemovedDiscountByCredit()
                ),
                new App\Services\RegistrationService(
                    new App\Repositories\UserRepository(
                        new App\Models\User()
                    ),
                    new App\Repositories\YearlySubscriptionRepository(
                        new App\Models\YearlySubscription()
                    ),
                    new App\Repositories\ClientYearlySubscriptionRepository(
                        new App\Models\ClientYearlySubscription()
                    ),
                    new App\Repositories\RegistrationOrderRepository(
                        new App\Models\RegistrationOrder(),
                        new App\Repositories\ClassRegistrationRepository(
                            new App\Models\ClassRegistration()
                        ),
                        new App\Repositories\ActingOriginatingAgentRepository(
                            new App\Models\ActingOriginatingAgent()
                        )
                    ),
                    new App\Repositories\ContactRepository(
                        new App\Models\Contact()
                    ),
                    new App\Repositories\YearlySubscriptionTypesRepository(
                        new App\Models\YearlySubscriptionType()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\CorporateSeatRepository(
                        new App\Models\CorporateSeat()
                    ),
                    new App\Repositories\ClientCorporateSeatCreditRepository(
                        new App\Models\ClientCorporateSeatCredit()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    ),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\OrderItemRepository(
                        new App\Models\OrderItem(),
                        new App\Repositories\OrderRepository(
                            new App\Models\Order()
                        ),
                        new App\Repositories\ActingOriginatingAgentRepository(
                            new App\Models\ActingOriginatingAgent()
                        ),
                        new App\Repositories\ClassRegistrationRepository(
                            new App\Models\ClassRegistration()
                        )
                    )
                ),
                new App\Repositories\OrderAdjustmentRepository(
                    new App\Models\OrderAdjustment()
                ),
                new App\Repositories\OrderRepository(
                    new App\Models\Order()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                )
            );
        });

        App::bind('order_mod_util', function()
        {
            return new App\Utilities\OrderModificationUtility(
                new App\Repositories\OrderAdjustmentRepository(
                    new App\Models\OrderAdjustment()
                ),
                new App\Repositories\ClassCombinationRepository(
                    new App\Models\ClassCombination()
                ),
                new App\Repositories\ClassCombinationDiscountRepository(
                    new App\Models\ClassCombinationDiscount()
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                )
            );
        });

        App::bind('order_mod_disc_service', function()
        {
            return new App\Services\OrderModificationDiscountService(
                new App\Repositories\OrderAdjustmentRepository(
                    new App\Models\OrderAdjustment()
                ),
                new App\Repositories\ClassHalfPriceDiscountRepository(
                    new App\Models\ClassHalfPriceDiscount()
                ),
                new App\Repositories\ClassCouponDiscountRepository(
                    new App\Models\ClassCouponDiscount()
                ),
                new App\Repositories\ClassMilitaryDiscountRepository(
                    new App\Models\ClassMilitaryDiscount()
                ),
                new App\Repositories\ClassCreditDiscountRepository(
                    new App\Models\ClassCreditDiscount()
                ),
                new App\Repositories\ClassCombinationDiscountRepository(
                    new App\Models\ClassCombinationDiscount()
                ),
                new App\Repositories\OrderAdjustmentDiscountRepository(
                    new App\Models\OrderAdjustmentDiscount()
                ),
                new App\Repositories\OrderItemRepository(
                    new App\Models\OrderItem(),
                    new App\Repositories\OrderRepository(
                        new App\Models\Order()
                    ),
                    new App\Repositories\ActingOriginatingAgentRepository(
                        new App\Models\ActingOriginatingAgent()
                    ),
                    new App\Repositories\ClassRegistrationRepository(
                        new App\Models\ClassRegistration()
                    )
                ),
                new App\Repositories\ClassCombinationRepository(
                    new App\Models\ClassCombination()
                ),
                new App\Repositories\ClassRegistrationRepository(
                    new App\Models\ClassRegistration()
                )
            );
        });

        App::bind('reminder_history_service', function() {
            return new App\Services\ReminderHistoryService(
                new App\Services\Mailer\EmailTemplateMailer(
                    new App\Mail\Mailer(
                        new App\Log\FileLogger()
                    )
                )
            );
        });

        require base_path() . '/app/Helpers/helpers.php';
        require base_path() . '/app/Helpers/macros.php';
    }
}
