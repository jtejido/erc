<?php namespace App\Repositories;

use App\Models\CourseType;
use DB;

class CourseTypeRepository extends BaseRepository {

    /**
     * @param CourseType $model
     */
    public function __construct(CourseType $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return CourseType::select('course_types.*');
    }

    /**
     * @return mixed
     */
    public function byType() {
    	return $this->createSelectStatement()
                    ->orderByRaw(DB::raw("FIELD(id, 1, 2, 3)"))->get();
    }
}