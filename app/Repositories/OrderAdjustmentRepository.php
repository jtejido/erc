<?php namespace App\Repositories;

use App\Models\OrderAdjustment;
use App\Utilities\Constant;

class OrderAdjustmentRepository extends BaseRepository
{

    /**
     * @param OrderAdjustment $model
     */
    public function __construct(OrderAdjustment $model)
    {
        $this->model = $model;
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        $query = $this->model
                      ->where('order_id', $orderId)
                      ->orderBy('created_at', 'DESC');

        return $query->get();
    }

    /**
     * @param $orderId
     * @param $action
     * @return mixed
     */
    public function getByOrderIdReturnTotal($orderId, $action)
    {
        $orderAdjustments = $this->model
                      ->where('order_id', $orderId)
                      ->where('adjustment_action', $action)
                      ->get();

        return $orderAdjustments->sum('amount');
    }

    /**
     * @param $adjustableId
     * @param $adjustableType
     * @return mixed
     */
    public function getByAdjustable($adjustableId, $adjustableType)
    {
        $query = $this->model
                      ->where('adjustable_id', $adjustableId)
                      ->where('adjustable_type', $adjustableType);

        return $query->get();
    }
}