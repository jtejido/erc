<?php namespace App\Repositories\ApiRequest;

use App\Repositories\ApiModels\CmStudent;
use App\Repositories\Eloquent\CmRepository;
use App\Utilities\Constant;

class CreateUpdateStudents extends CmRepository {

    public function model()
    {
        return Constant::CM_CREATE_UPDATE_STUDENTS;
    }

    public function create(CmStudent $student)
    {
        $student = $this->prepareStudent($student);

        $data = ['students' => [$student->toArray()]];

        try {
            $response = $this->acceptAndAddJson($data)->post();
            return json_decode($response->getBody());
        } catch (\Exception $e) {
            return $e->getMessage();
            //echo (string) $e->getResponse()->getBody();
        }
    }

    /**
     * Update an existing student
     * @param $id
     * @param CmStudent $student
     * @return mixed
     */
    public function update($id, CmStudent $student)
    {
        $student->setId($id);
        unset($student->password);

        $response = $this->acceptAndAddJson(['students' => [$student->toArray()]])->post();

        return json_decode($response->getBody());
    }

    /**
     * The nomenclature for CourseMill ID will be first letter of the first name + last_name + random 3 digit number.
     * If such CourseMill ID already exists, add the last 3 digits of zip code.
     * @param CmStudent $student
     * @return string
     */
    private function generateId(CmStudent $student)
    {
        $digits = 3;
        $rnum = rand(pow(10, $digits-1), pow(10, $digits)-1);
        return preg_replace('/\s/', '', strtolower($student->fName . $student->lName . $rnum));
    }

    /**
     * @param CmStudent $student
     * @return CmStudent
     */
    private function prepareStudent(CmStudent $student)
    {
        $id = $this->generateId($student);
        $student->setId($id);
        $student->setPassword($id);
        return $student;
    }

}