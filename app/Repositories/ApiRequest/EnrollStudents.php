<?php namespace App\Repositories\ApiRequest;

use App\Repositories\ApiModels\EnrollStudent;
use App\Repositories\Eloquent\CmRepository;
use App\Utilities\Constant;

class EnrollStudents extends CmRepository
{

    public function model()
    {
        return Constant::CM_ENROLL_STUDENTS;
    }

    /**
     * @param EnrollStudent $enrollStudent
     * @return string
     */
    public function enroll(EnrollStudent $enrollStudent)
    {
        $created = false;
        $errorMsg = '';

        try {

            $response = $this->acceptAndAddJson(['enrollments' => [$enrollStudent->toArray()]])->post();
            $jsr = json_decode($response->getBody(), true);

            $created = $jsr['created'] ? true : false;

        } catch (\Exception $e) {
            throw $e;
            $errorMsg = (string) $e->getMessage();
        }

        return ['created' => $created, 'error' => $errorMsg];
    }
}