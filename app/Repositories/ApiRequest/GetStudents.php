<?php namespace App\Repositories\ApiRequest;

use App\Repositories\Eloquent\CmRepository;
use App\Utilities\Constant;

class GetStudents extends CmRepository {

    public function model()
    {
        return Constant::CM_GET_STUDENTS;
    }

    public function listAll($ids = [])
    {
        $response = $this->acceptAndAddJson(['ids' => $ids])->post();
        return json_decode($response->getBody())->students;
    }

    public function isExisting($id)
    {
        $req = $this->listAll(["$id"]);
        return sizeof($req) ? true : false;
    }

}