<?php namespace App\Repositories;

use App\Events\StudentMarkedConfirmedEvent;
use App\Models\ClassRegistration;
use App\Models\Course;
use App\Models\CourseClass;
use App\Utilities\Constant;
use Illuminate\Support\Facades\Storage;

class ClassRegistrationRepository extends BaseRepository
{

    /**
     * @param ClassRegistration $model
     */
    public function __construct(ClassRegistration $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassRegistration::select('class_registrations.*');
    }

    /**
     * Get registrations by agent
     *
     * @param $attendee
     * @return mixed
     */
    public function getByAttendee($attendee)
    {
        $query = $this->getByColumn('attendee_contact_id', '=', $attendee);

        return $query->get();
    }

    /**
     * @param $attendee
     * @return mixed
     */
    public function getByAttendeeCompletedOrInvoiced($attendee)
    {
        $query = $this->getByColumn('attendee_contact_id', '=', $attendee)
                      ->join('registration_orders', function($join)
                      {
                          $join
                              ->on('registration_orders.id', '=', 'class_registrations.registration_order_id');
                      })
                      ->where('registration_orders.status', '=', Constant::COMPLETE)
                      ->orWhere('registration_orders.status', '=', Constant::INVOICED);

        return $query->get();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function getByAttendeeRegistrationStatus($params = [])
    {
        $query = $this->createSelectStatement()
                      ->where('attendee_contact_id', $params['attendee_contact_id'])
                      ->where('registration_order_id', $params['registration_order_id'])
                      ->join('registration_orders', function($join)
                      {
                          $join
                              ->on('registration_orders.id', '=', 'class_registrations.registration_order_id');
                      })
                      ->where('registration_orders.status', '=', $params['status']);

        return $query->first();
    }

    /**
     * @param array $registrationOrderIds
     * @return mixed
     */
    public function getByRegistrationOrderIdsNoCredit($registrationOrderIds = [])
    {
        $query = $this->createSelectStatement()
            ->whereIn('registration_order_id', $registrationOrderIds)
            ->where('corporate_seat_credit_applied', false);

        return $query->get();
    }

    /**
     * Get registrations by class and attendee
     *
     * @param $registrationId
     * @param $attendee
     * @return mixed
     */
    public function getByRegistrationAndAttendee($registrationId, $attendee)
    {
        $query = $this->model
            ->where('registration_order_id', $registrationId)
            ->where('attendee_contact_id', $attendee)
            ->withTrashed();

        return $query->first();
    }

    /**
     * @param $registrableId
     * @param $registrableType
     * @param $attendee
     * @return mixed
     */
    public function getByRegistrationOrderAttendeePending($registrableId, $registrableType, $attendee)
    {
        $query = $this->createSelectStatement()
            ->where('attendee_contact_id', $attendee->id)
            ->join('registration_orders', function ($join) {
                $join
                    ->on('registration_orders.id', '=', 'class_registrations.registration_order_id');
            })
            ->where('registration_orders.registrable_id', '=', $registrableId)
            ->where('registration_orders.registrable_type', '=', $registrableType)
            ->where('registration_orders.status', '=', Constant::PENDING);


        return $query->first();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getForPricingDiscountWithAttendee($params = [])
    {
        $query = $this->createSelectStatement()
            ->where('attendee_contact_id', $params['attendee'])
            ->where('registration_order_id', $params['registration_order_id']);

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function getAllPricingDiscount($query)
    {
        return $query->get();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function getSinglePricingDiscount($query)
    {
        return $query->first();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdNoSwap($registrationOrderId)
    {
        $query = $this->createSelectStatement()
            ->where('registration_order_id', $registrationOrderId)
            ->where('is_swapped', false);

        return $query->get();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdNoSwapNoCancel($registrationOrderId)
    {
        $query = $this->createSelectStatement()
                      ->where('registration_order_id', $registrationOrderId)
                      ->where('is_cancelled', false)
                      ->where('is_swapped', false);

        return $query->get();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdNotNewNoSwapNoCancel($registrationOrderId)
    {
        $query = $this->createSelectStatement()
            ->where('registration_order_id', $registrationOrderId)
            ->where('is_new', false)
            ->where('is_cancelled', false)
            ->where('is_swapped', false);

        return $query->get();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdNotNewNoSwap($registrationOrderId)
    {
        $query = $this->createSelectStatement()
            ->where('registration_order_id', $registrationOrderId)
            ->where('is_new', false)
            ->where('is_swapped', false);

        return $query->get();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdNoCancel($registrationOrderId)
    {
        $query = $this->createSelectStatement()
            ->where('registration_order_id', $registrationOrderId)
            ->where('is_swapped', false);

        return $query->get();
    }

    /**
     * @param $registrationOrderId
     * @return mixed
     */
    public function getByRegistrationIdCountNew($registrationOrderId)
    {
        $query = $this->createSelectStatement()
                      ->where('registration_order_id', $registrationOrderId)
                      ->where('is_new', true);

        return $query->get()->count();
    }

    /**
     * @param array $registrationOrderIds
     * @return mixed
     */
    public function getByRegistrationIdsCountNew($registrationOrderIds = [])
    {
        $query = $this->createSelectStatement()
            ->whereIn('registration_order_id', $registrationOrderIds)
            ->where('is_new', true);

        return $query->get()->count();
    }

    /**
     * Get list students of a class
     * @param $class
     * @return \Illuminate\Support\Collection
     */
    public function getStudentsOfClass($class, $includeCancelled = true)
    {
        $students = collect();
        foreach ($class->registrations as $registration) {

            foreach ($registration->classRegistrations as $item) {
                if (!$includeCancelled) {
                    if ($item->is_cancelled || $item->is_swapped || $item->is_new)
                        continue;
                }
                $this->createStudentsModel($item, $registration, $students);
            }

        }

        return $students->flatten();
    }

    public function getStudentsOfRegistrationOrder($registration, $includeCancelled = true)
    {
        $students = collect();

        foreach ($registration->classRegistrations as $item) {
            if (!$includeCancelled) {
                if ($item->is_cancelled || $item->is_swapped)
                    continue;
            }
            $this->createStudentsModel($item, $registration, $students);
        }


        return $students->flatten();
    }

    /**
     * @param $userId
     * @param $regOrderId
     * @param $pdf
     * @return string
     */
    public function saveCertificate($userId, $regOrderId, $pdf)
    {
        $filename = 'certificate_' . $userId . '_' . $regOrderId . '.pdf';
        $path = 'certificates/' . $filename;
        Storage::put(
            $path,
            $pdf->output()
        );
        $class_reg = $this->getByRegistrationAndAttendee($regOrderId, $userId);
        $this->update(['certificate_file_path' => $path], $class_reg->id);
        return $path;
    }

    public function getCount($classId, $flag, $key='attended')
    {
        $class = CourseClass::findOrFail($classId);
        $count = 0;
        foreach ($class->registrations as $registration) {
            foreach ($registration->classRegistrations as $classRegistration) {
                if ($classRegistration->{$key} == $flag) $count++;
            }
        }
        return $count;

    }

    public function getCountForCourse($courseId, $flag, $key='attended')
    {
        $course = Course::findOrFail($courseId);
        $count = 0;
        foreach ($course->registrations as $registration) {
            foreach ($registration->classRegistrations as $classRegistration) {
                if ($classRegistration->{$key} == $flag) $count++;
            }
        }
        return $count;

    }

    public function updateStudentStatusOfClass($classId, $flag, $key)
    {
        $class = CourseClass::findOrFail($classId);
        foreach ($class->registrations as $registration) {
            foreach ($registration->classRegistrations as $classRegistration) {
                if(!$classRegistration->{$key}) {
                    $classRegistration->{$key} = $flag;
                    $classRegistration->save();
                }
            }
        }
    }

    public function updateStudentStatusOfCourse($courseId, $flag, $key)
    {
        $course = Course::findOrFail($courseId);
        foreach ($course->registrations as $registration) {
            foreach ($registration->classRegistrations as $classRegistration) {
                if(!$classRegistration->{$key}) {
                    $classRegistration->{$key} = $flag;
                    $classRegistration->save();
                }
            }
        }
    }

    /**
     * @param $item
     * @param $registration
     * @param $students
     */
    public function createStudentsModel($item, $registration, $students)
    {
        $s = $item->contact;
        $s->regOrderId = $registration->id;
        $s->meeting_flag = $item->meeting_flag;
        $s->exam_flag = $item->exam_flag;
        $s->attended = $item->attended;
        $s->confirmed = $item->isConfirmed();
        $s->cert = $this
            ->getByRegistrationAndAttendee($registration->id, $s->id)
            ->isCertificateAvailable();
        try {
            $s->poNumber = $registration->orderItem->order->invoicePayment->po_number;
            $s->checkNumber = $registration->orderItem->order->invoicePayment->check_number;
        } catch (\Exception $e) {
        }

        if (!in_array($s->id, $students->pluck('id')->toArray())) {
            $students->push($s);
        }
    }

    /**
     * @param array $registrationOrderIds
     * @return mixed
     */
    public function countActiveRegistrationsByIds($registrationOrderIds = [])
    {
        $query = $this->model
                      ->whereIn('registration_order_id', $registrationOrderIds)
                      ->where('is_cancelled', false)
                      ->where('is_swapped', false)
                      ->where('is_new', false);

        return $query->get()->count();
    }
}
