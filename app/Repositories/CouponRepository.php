<?php namespace App\Repositories;

use App\Models\Coupon;
use App\Utilities\Constant;

class CouponRepository extends BaseRepository
{

    /**
     * @var CourseRepository
     */
    protected $courseRepo;

    /**
     * @var CouponCourseRepository
     */
    protected $couponCourseRepo;

    /**
     * @param Coupon $model
     * @param CourseRepository $courseRepo
     * @param CouponCourseRepository $couponCourseRepo
     */
    public function __construct(Coupon $model,
                                CourseRepository $courseRepo,
                                CouponCourseRepository $couponCourseRepo)
    {
        $this->model = $model;
        $this->courseRepo = $courseRepo;
        $this->couponCourseRepo = $couponCourseRepo;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return Coupon::select('coupons.*');
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function searchFilterReports($params = [])
    {
        $query = $this->createSelectStatement()
                      ->select('coupons.*');

        if (isset($params['title']) and strlen(trim($params['title'])) > 0) {
            $courses = $this->courseRepo->searchFilterReport($params['title']);

            if ($courses->count()) {
                $classIds = [];
                $couponIds = [];
                $courseIds = $courses->pluck('id')->toArray();

                foreach ($courses as $course) {
                    if ($course->classes->count()) {
                        foreach ($course->classes as $class) {
                            array_push($classIds, $class->id);
                        }
                    }
                }

                if (count($courseIds)) {
                    $courseObjectCoupons = $this->couponCourseRepo->getByDiscountable([
                        'discountable_type' => Constant::COURSE_OBJECT,
                        'discountable_ids'  => $courseIds
                    ]);

                    if ($courseObjectCoupons->count()) {
                        foreach ($courseObjectCoupons->pluck('coupon_id')->toArray() as $courseObjectCoupon) {
                            array_push($couponIds, $courseObjectCoupon);
                        }
                    }
                }

                if (count($classIds)) {
                    $courseClassObjectCoupons = $this->couponCourseRepo->getByDiscountable([
                        'discountable_type' => Constant::COURSE_CLASS_OBJECT,
                        'discountable_ids'  => $classIds
                    ]);

                    if ($courseClassObjectCoupons->count()) {
                        foreach ($courseClassObjectCoupons->pluck('coupon_id')->toArray() as $courseClassObjectCoupon) {
                            array_push($couponIds, $courseClassObjectCoupon);
                        }
                    }
                }

                if (count(array_unique($couponIds))) {
                    $query = $query->whereIn('id', array_unique($couponIds));
                }
            }
        }

        return $query;
    }
}