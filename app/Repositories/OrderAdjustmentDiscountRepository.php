<?php namespace App\Repositories;

use App\Models\OrderAdjustmentDiscount;

class OrderAdjustmentDiscountRepository extends BaseRepository
{

    /**
     * @param OrderAdjustmentDiscount $model
     */
    public function __construct(OrderAdjustmentDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return OrderAdjustmentDiscount::select('order_adjustment_discounts.*');
    }

    /**
     * @param $orderId
     */
    public function getByOrderId($orderId)
    {
        return  $this->createSelectStatement()
                     ->where('order_id', $orderId)
                     ->where('is_deleted_by_credit', false)
                     ->where('is_deleted', false)
                     ->get();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getByAdjustable($params = [])
    {
        $query = $this->createSelectStatement()
                      ->where('adjustable_id', $params['adjustable_id'])
                      ->where('adjustable_type', $params['adjustable_type']);

        return $query->get();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getSingleByAdjustable($params = [])
    {
        $query = $this->createSelectStatement()
            ->where('adjustable_id', $params['adjustable_id'])
            ->where('adjustable_type', $params['adjustable_type']);

        return $query->first();
    }
}