<?php namespace App\Repositories;

use App\Models\State;

class StateRepository extends BaseRepository {

    /**
     * @param State $model
     */
    public function __construct(State $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return State::select('states.*');
    }
}