<?php namespace App\Repositories;

use App\Models\RemovedDiscountByCredit;

class RemovedDiscountByCreditRepository extends BaseRepository
{
    /**
     * @param RemovedDiscountByCredit $model
     */
    public function __construct(RemovedDiscountByCredit $model)
    {
        $this->model = $model;
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getByClassRegId($classRegId)
    {
        $query = $this->model
                      ->where('class_reg_id', $classRegId);

        return $query->get();
    }
}