<?php namespace App\Repositories;

use App\Facades\OrderModificationService;
use App\Models\Book;
use App\Models\BookDiscount;
use App\Models\OrderAdjustment;
use App\Models\OrderItem;
use App\Models\ProductOrders;
use App\Repositories\Eloquent\Repository;
use App\Utilities\Constant;


class ProductOrdersRepository extends Repository
{
    function model()
    {
        return 'App\Models\ProductOrders';
    }

    public function updateBookToCart(Book $book, $qty, $userId, $status)
    {
        $params['status']           = $status;
        $params['product_type']     = class_basename(Book::class);
        $params['product_id']       = $book->id;
        $params['originating_agent_user_id'] = $userId;

        $discounted = false;
        $productOrder = $this->model->where($params)->first();
        $bookDiscounts = BookDiscount::where('book_id', $book->id)->get();

        if ($bookDiscounts->count()) {
            foreach ($bookDiscounts as $bookDiscount) {
                if (parse_book_discount($bookDiscount->condition, $qty)) {
                    $productOrder->price = $bookDiscount->discount;
                    $productOrder->save();

                    $discounted = true;
                }
            }
        }

        if (!$discounted) {
            $productOrder->price = $book->price;
            $productOrder->save();
        }

        if(!$productOrder) {
            abort(404);
        }

        $order = [];

        if ($productOrder->quantity != intval($qty) and ($status == Constant::INVOICED or $status == Constant::COMPLETE)) {
            $orderAdjustment = OrderAdjustment::where([
                'adjustable_id' => $productOrder->id,
                'adjustable_type' => Constant::PRODUCT_ORDERS,
                'is_product_qty_adjustment' => true
            ])->first();

            if ($orderAdjustment) {
                $orderAdjustment->delete();
            }

            $orderItem = OrderItem::where([
                'orderable_id' => $productOrder->id,
                'orderable_type' => Constant::PRODUCT_ORDERS
            ])->first();

            $order['order_id'] = count($orderItem->order)>0?$orderItem->order->id:0;
            $order['shipping_zip'] = count($orderItem->order)>0?$orderItem->order->shipping_zip:0;

            $params = [
                'agent_id' => $productOrder->agent->id,
                'order_id' => $orderItem->order->id,
                'order_item_id' => $orderItem->id,
                'adjustable_id' => $productOrder->id,
                'adjustable_type' => Constant::PRODUCT_ORDERS,
                'amount' => $productOrder->price * (abs($productOrder->quantity - intval($qty))),
                'adjustment_action' => $productOrder->quantity > intval($qty)
                    ? Constant::DEDUCTED
                    : Constant::ADDED,
                'note' => adjustment_product_change_qty($productOrder),
                'is_product_qty_adjustment' => true
            ];

            OrderModificationService::saveOrderAdjustment($params);
        }

        $productOrder->quantity = intval($qty);

        if ($status == Constant::PENDING) {
            $productOrder->paid_quantity = intval($qty);
        }

        $productOrder->save();
        $productOrder->{'modified_order_id'} = isset($order['order_id'])?$order['order_id']:0;
        $productOrder->{'shipping_zip'} = isset($order['shipping_zip'])?$order['shipping_zip']:0;
        return $productOrder;

    }

    public function addBookToCart(Book $book, $qty, $userId, $discounted = false)
    {

        $params['status']           = Constant::PENDING;
        $params['product_type']     = class_basename(Book::class);
        $params['product_id']       = $book->id;
        $params['originating_agent_user_id'] = $userId;

        if ($this->productOrderExists($params)) {
            $productOrder = $this->model->where($params)->first();
            if(!$productOrder) {
                abort(404);
            }
            $productOrder->quantity = $productOrder->quantity + intval($qty);
            $productOrder->paid_quantity = $productOrder->quantity + intval($qty);
            $productOrder->save();
        } else {
            $params['quantity']         = $qty;
            $params['paid_quantity']    = $qty;
            $params['price']            = ($discounted) ? $book->discounted_price : $book->price;
            $params['paid_price']       = ($discounted) ? $book->discounted_price : $book->price;
            $productOrder = ProductOrders::create($params);
        }

        $bookDiscounts = BookDiscount::where('book_id', $book->id)->get();

        if ($bookDiscounts->count()) {
            foreach ($bookDiscounts as $bookDiscount) {
                if (parse_book_discount($bookDiscount->condition, $qty)) {
                    $productOrder->price = $bookDiscount->discount;
                    $productOrder->save();
                }
            }
        }


        return $productOrder;

    }

    public function productOrderExists($params)
    {
        $exists = $this->model->where($params)->count();
        return $exists;

    }

}