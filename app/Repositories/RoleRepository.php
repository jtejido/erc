<?php namespace App\Repositories;

use App\Models\Role;
use App\Utilities\Constant;

class RoleRepository extends BaseRepository
{
    /**
     * Address Repository constructor
     *
     * @param Role $model
     */
    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    public function all($labeled = false) {
        $_return = $this->model
                        ->where('role', '!=', Constant::ROLE_MEMBER)
                        ->get();

        if (!$labeled) {
            return $_return;
        }

        $_return = $_return->each(function($role, $key) {
            $label = explode('_', $role->role);
            $role->label = end($label);
            if ($role->label !== 'CSR') {
                $role->label = ucfirst(strtolower($role->label));
            }
        });

        return $_return;
    }

}