<?php namespace App\Repositories;

use App\Models\Course;
use App\Utilities\Constant;
use Illuminate\Support\Facades\DB;

class CourseRepository extends BaseRepository {

    /**
     * @param Course $model
     */
    public function __construct(Course $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return Course::select('courses.*');
    }

    /**
     * Get courses by search string or filters
     *
     * @param array $params
     * @return mixed
     */
    public function searchFilter($params = [])
    {
        $query = $this->createSelectStatement();

        if (isset($params['status'])) {
            $query = 1 == $params['status']
                ? $query->where('is_deactivated', false)
                : $query->where('is_deactivated', true);
        }

        if (isset($params['searchq'])) {
            $query = $query->where(function($q) use ($params) {
                $q->where('title', 'LIKE', '%' . $params['searchq'] . '%')
                    ->orWhere('description', 'LIKE', '%' . $params['searchq'] . '%');
            });
        }

        if (isset($params['sku'])) {
            $query = $query->where('sku', $params['sku']);
        }

        if (isset($params['category'])) {
            $query = $query->join('course_categories', function($courseCatjoin)
                            {
                                $courseCatjoin->on('courses.id', '=', 'course_categories.course_id');
                            })
                            ->where('course_categories.category_id', $params['category']);
        }

        if (isset($params['type'])) {
            $query = $query->where('courses.course_type_id', '=', $params['type']);
        }

        if (isset($params['location']) && $params['location'] != null) {
            $query = $query->whereExists(function ($query) use ($params) {
                $query->select(DB::raw(1))
                    ->from('classes')
                    ->join('locations', 'classes.location_id', '=', 'locations.id')
                    ->whereRaw('classes.course_id = courses.id')
                    ->where('locations.general_location_id', '=', $params['location']);
            });
        }

        if (isset($params['byOrder']) and $params['byOrder']) {
            $query->join('course_orders', function($courseOrderJoin)
            {
                $courseOrderJoin->on('courses.id', '=', 'course_orders.course_id');
            })
                ->where('course_orders.course_type_id', '=', $params['type'])
                ->where('course_orders.category_id', '=', $params['category'])
                ->orderBy('course_orders.order', 'ASC');
        }

        return $query;
    }

    /**
     * Get courses by search string or filters
     *
     * @param $title
     * @return mixed
     */
    public function searchFilterReport($title)
    {
        $query = $this->model
                      ->whereIn('course_type_id', [Constant::SEMINAR, Constant::WEBCAST])
                      ->where('title', 'LIKE',  '%' . $title . '%');

        return $query->get();
    }

    /**
     * Get all with trashed
     *
     * @param $query
     */
    public function getCourseWithTrashed($query)
    {
        return $query->all();
    }

    /**
     * Get all excluding trashed
     *
     * @param $query
     */
    public function getCourseWithoutTrashed($query)
    {
        return $query->where('is_deactivated', false);
    }

    /**
     * Get courses by type
     *
     * @param $type
     * @return mixed
     */
    public function associatedCourses($type)
    {
        $query = $this->getByColumn('course_type_id', '=', $type)->limit(3);

        return $query->get();
    }

    /**
     * @return mixed
     */
    public function getHWMWithPricingRules()
    {
        $courses = collect();

        $query1 = $this->searchFilter([
            'searchq'   => Constant::HWM,
            'type'      => Constant::SEMINAR,
            'sku'       => 'HWM'
        ]);

        $query2 = $this->searchFilter([
            'searchq'   => Constant::HWM_TEXAS,
            'type'      => Constant::SEMINAR,
            'sku'       => 'TXHWM'
        ]);

        $query3 = $this->searchFilter([
            'searchq'   => Constant::HWM_CAL,
            'type'      => Constant::SEMINAR,
            'sku'       => 'CAHWM'
        ]);

        if ($query1->first()) {
            $courses->push($query1->first());
        }

        if ($query2->first()) {
            $courses->push($query2->first());
        }

        if ($query3->first()) {
            $courses->push($query3->first());
        }

        return $courses;
    }

    /**
     * @return mixed
     */
    public function getDOTWithPricingRules()
    {
        $query = $this->searchFilter([
            'searchq'   => Constant::DOT,
            'type'      => Constant::SEMINAR
        ]);

        return $query->first();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getExcludedGroupDiscountRules()
    {
        $courses = collect();

        $query1 = $this->searchFilter([
            'searchq'   => Constant::TDG,
            'type'      => Constant::SEMINAR
        ]);

        $query2 = $this->searchFilter([
            'searchq'   => Constant::HWO_40,
            'type'      => Constant::SEMINAR
        ]);

        $query3 = $this->searchFilter([
            'searchq'   => Constant::HWO_24,
            'type'      => Constant::SEMINAR
        ]);

        $query4 = $this->searchFilter([
            'searchq'   => Constant::TDG_WEB,
            'type'      => Constant::WEBCAST
        ]);

        $query5 = $this->searchFilter([
            'searchq'   => Constant::DGU,
            'type'      => Constant::WEBCAST
        ]);

        $courses->push($query1->first());
        $courses->push($query2->first());
        $courses->push($query3->first());
        $courses->push($query4->first());
        $courses->push($query5->first());

        return $courses;
    }

    public function getSortedCourseList($query)
    {
        return $this->getCourseWithoutTrashed($query)->orderBy('title');
    }

    /**
     * @param $categoryId
     * @param $typeId
     * @return mixed
     */
    public function getByCategoryAndType($categoryId, $typeId)
    {
        $query = $this->createSelectStatement();

        return $query->where('course_type_id', $typeId)
            ->join('course_categories', function($join) use ($categoryId)
            {
                $join->on('courses.id', '=', 'course_categories.course_id')
                    ->where('course_categories.category_id', '=', $categoryId);
            })->get();
    }

}