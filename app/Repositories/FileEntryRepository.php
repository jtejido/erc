<?php namespace App\Repositories;

use App\Events\CourseEbookUpdateEvent;
use App\Events\CourseMaterialAddedEvent;
use App\Http\Requests\FilesEntryRequest;
use App\Models\FileEntry;
use App\Models\ModelInterface\Imageable;
use App\Repositories\Eloquent\Repository;
use App\Utilities\Constant;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class FileEntryRepository extends Repository
{
    function model()
    {
        return 'App\Models\FileEntry';
    }

    public function deleteFileAndRelatedFiles(FileEntry $fileEntry)
    {
        foreach($fileEntry->imageable->files as $file) {
            $this->deleteFileEntry($file);
        }
    }

    /**
     * @param FileEntry $fileEntry
     */
    public function deleteFileEntry(FileEntry $fileEntry)
    {
        $this->deleteEntryFile($fileEntry);
        $fileEntry->delete();
    }

    /**
     * @param FileEntry $fileEntry
     */
    private function deleteEntryFile(FileEntry $fileEntry)
    {
        $entity = $fileEntry->imageable()->first();
        $this->deleteFile($entity::FOLDERPATH, $fileEntry->filename);
    }

    /**
     * @param FilesEntryRequest $request
     * @param Imageable|Model $model
     * @return array
     */
    public function addFiles(FilesEntryRequest $request, Imageable $model)
    {
        $updatedFiles = [];
        foreach ($request->file('files') as $key => $file) {
            if (!$file) break;
            $fields['title'] = $file->getClientOriginalName();
            $fields['type'] = $request->input('type');
            $fields = $this->storeFileEntryFile($fields, $model, $file);
            $fileEntry = $model->files()->create($fields);
            $updatedFiles[] = $fileEntry->id;
        }
        return $updatedFiles;
    }

    public function addBookFiles(Request $request, Model $model)
    {
        $updatedFiles = [];
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $fields['title'] = $request->get('title')
                                ? $request->get('title')
                                : $file->getClientOriginalName();
            $fields['type'] = Constant::EBOOK;
            $fields = $this->storeFileEntryFile($fields, $model, $file);
            if ($request->get('file_id') == '0') {
                $fileEntry = $model->files()->create($fields);
                $updatedFiles[] = $fileEntry->id;
            } else {
                $fileEntryId = intval($request->input('file_id'));
                $this->update($fields, $fileEntryId);
                $updatedFiles[] = $fileEntryId;
            }
        }

        if($request->hasFile('otherfiles')) {
            $filenames = $request->input('otherFilesNames');
            foreach ($request->file('otherfiles') as $key => $file) {
                if (!$file) break;
                $otherFileFields['title'] = $filenames[$key];
                $otherFileFields['type'] = Constant::EBOOK_UPDATE;
                $otherFileFields = $this->storeFileEntryFile($otherFileFields, $model, $file);
                $fileEntry = $model->files()->create($otherFileFields);
                $updatedFiles[] = $fileEntry->id;
            }
        }

        return $updatedFiles;
    }


    /**
     * @param Request $request
     * @param Model $model
     * @return Model
     */
    public function addFile(Request $request, Model $model)
    {
        $path = $request->get('path');
        $materialType = $request->get('material_type');
        $fields['is_file'] = $materialType == 0;
        $fields['title'] = $request->get('title');
        $fields['type'] = ($request->has('type')) ? $request->get('type') : 'FILE';

        $notify = ($request->has('doNotEmailInvoice')) ? false : true;


        if ($materialType == 0) {
            $fields = $this->storeFileEntryFile($fields, $model, $request->file('file'));
        }

        else {
            $fields['path'] = $path;
        }

        $fileEntry = $model->files()->create($fields);
        if ($fields['type'] == Constant::EBOOK) {
            event(new CourseEbookUpdateEvent($model->id, $fileEntry->id));
        }

        if ($fields['type'] == Constant::MATERIAL) {
            event(new CourseMaterialAddedEvent($fileEntry, $model, $notify));
        }

        return $fileEntry;
    }

    /**
     * @param $fields
     * @param Model $model
     * @return mixed
     */
    public function storeFileEntryFile($fields, Model $model, $file)
    {
        $filename = $file->getFilename() . FileEntry::EXT;
        $path = $model::FOLDERPATH . $filename;
        $fields['disk'] = 'local';
        $fields['path'] = $path;
        $fields['filename'] = $filename;
        $fields['size'] = $file->getSize();
        $fields['mime'] = $file->getClientMimeType();
        $fields['original_filename'] = $file->getClientOriginalName();
        $this->storeFile($path, $file, $fields['disk']);
        return $fields;
    }


}