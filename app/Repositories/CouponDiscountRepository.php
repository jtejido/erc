<?php namespace App\Repositories;

use App\Models\CouponDiscount;

class CouponDiscountRepository extends BaseRepository
{

    /**
     * @param CouponDiscount $model
     */
    public function __construct(CouponDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return CouponDiscount::select('coupon_discounts.*');
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->first();
    }

    /**
     * @param $couponId
     * @return mixed
     */
    public function getByCouponId($couponId)
    {
        return $this->createSelectStatement()
                    ->where('coupon_id', $couponId)
                    ->get();
    }

    /**
     * @param $orderId
     * @param $couponId
     * @return mixed
     */
    public function getByOrderIdAndCouponId($orderId, $couponId)
    {
        $query = $this->createSelectStatement()
                      ->where('order_id', $orderId)
                      ->where('coupon_id', $couponId);

        return $query->first();
    }
}