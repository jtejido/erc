<?php namespace App\Repositories;

use App\Models\ClientCorporateSeatCredit;
use App\Utilities\Constant;

class ClientCorporateSeatCreditRepository extends BaseRepository
{
    /**
     * @param ClientCorporateSeatCredit $model
     */
    public function __construct(ClientCorporateSeatCredit $model)
    {
        $this->model = $model;
    }

    /**
     * Get by agent
     *
     * @param $userId
     * @param bool|true $pending
     * @return mixed
     */
    public function getByAgent($userId, $pending = true)
    {
        $query = $this->model
                      ->where('originating_agent_user_id', '=', $userId);

        if ($pending) {
            $query->where('status', '=', Constant::PENDING);

            return $query->first();
        }

        return $query->get();
    }
}