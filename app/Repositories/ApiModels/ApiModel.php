<?php namespace App\Repositories\ApiModels;

use Illuminate\Contracts\Support\Arrayable;

abstract class ApiModel implements Arrayable {

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return (array) $this;
    }

}