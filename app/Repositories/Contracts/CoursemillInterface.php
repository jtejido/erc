<?php namespace App\Repositories\Contracts;

interface CoursemillInterface {

    /**
     * Send a get request
     * @return mixed
     */
    public function get();

    /**
     * Send a post request
     * @return mixed
     */
    public function post();

    /**
     * Build request of repository
     * @param string $method
     * @return mixed
     */
    public function makeRequest($method = 'GET');

    /**
     * Add options to request repository
     * @param array $option
     * @return mixed
     */
    public function pushOption($option = []);

    /**
     * Push a header for accepting JSON response
     * @return mixed
     */
    public function acceptJson();

    /**
     * Accept JSON header and Push JSON Form Body
     * @param array $param
     * @return mixed
     */
    public function acceptAndAddJson($param = []);
}