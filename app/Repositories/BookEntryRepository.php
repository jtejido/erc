<?php namespace App\Repositories;

use App\Models\Book;
use App\Models\BookEntry;
use App\Repositories\Eloquent\Repository;
use Illuminate\Http\Request;

class BookEntryRepository extends Repository
{
    function model()
    {
        return 'App\Models\BookEntry';
    }

    public function addEntryToBook(Book $book, $params)
    {
        return $book->bookEntries()->create($params);
    }

    /**
     * @param BookEntry $bookEntry
     */
    public function deleteBookEntry(BookEntry $bookEntry)
    {
        $this->deleteBookEntryFile($bookEntry->filename);
        $this->delete($bookEntry->id);
    }

    /**
     * @param $filename
     */
    private function deleteBookEntryFile($filename)
    {
        $this->deleteFile('books/files/', $filename);
    }

    /**
     * @param Request $request
     * @param Book $book
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addFile(Request $request, Book $book)
    {
        $fields['title'] = $request->get('title');
        $fields = $this->storeBookEntryFile($request, $fields);
        return $book->bookEntries()->create($fields);
    }

    /**
     * @param Request $request
     * @param $fields
     * @return mixed
     */
    public function storeBookEntryFile(Request $request, $fields)
    {
        $file = $request->file('file');
        $filename = $this->storeFile('books/files/', $file);
        $fields['size'] = $file->getSize();
        $fields['filename'] = $filename;
        $fields['mime'] = $file->getClientMimeType();
        $fields['original_filename'] = $file->getClientOriginalName();
        return $fields;
    }

    public function getBookEntryFile($filename)
    {
        return $this->getFile('books/files/', $filename);
    }

}