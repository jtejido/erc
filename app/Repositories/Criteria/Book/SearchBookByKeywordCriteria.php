<?php namespace App\Repositories\Criteria\Book;

use App\Repositories\Criteria\Criteria;
use App\Repositories\Eloquent\Repository;

class SearchBookByKeywordCriteria extends Criteria {

    protected $keyword;

    /**
     * CategoryFilter constructor.
     * @param string $keyword
     */
    public function __construct($keyword = '')
    {
        $this->keyword = $keyword;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $keyword = $this->keyword;
        if ($keyword!='') {
            $query = $model->where(function ($query) use ($keyword) {
                $query->where("name", "LIKE","%$keyword%")
                    ->orWhere("code", "LIKE", "%$keyword%")
                    ->orWhere("description", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}