<?php namespace App\Repositories\Criteria;

use App\Repositories\Eloquent\Repository;

class SortByCriteria extends Criteria {

    protected $key;
    protected $by;

    /**
     * SortByCriteria constructor.
     * @param $key
     * @internal param array $params
     */
    public function __construct($key, $by = 'ASC')
    {
        $this->key = $key;
        $this->by = $by;
    }

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $query = $model->orderBy($this->key, $this->by);
        return $query;
    }
}