<?php namespace App\Repositories\Criteria;

use App\Repositories\Eloquent\Repository;

class SearchFilterCriteria extends Criteria {

    protected $params = [];

    /**
     * CategoryFilter constructor.
     * @param array $params
     */
    public function __construct($params = [])
    {
        $this->params = $params;
    }

    /**
     * @param $model
     * @param RepositoryInterface|Repository $repository
     * @return mixed
     */
    public function apply($model, Repository $repository)
    {
        $key = key($this->params);
        $value = current($this->params);
        $query = $model->where($key, $value);
        return $query;
    }
}