<?php namespace App\Repositories;

use App\Models\ClassMilitaryDiscount;

class ClassMilitaryDiscountRepository extends BaseRepository
{

    /**
     * @param ClassMilitaryDiscount $model
     */
    public function __construct(ClassMilitaryDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassMilitaryDiscount::select('class_military_discounts.*');
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->get();
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getByClassRegId($classRegId)
    {
        return $this->createSelectStatement()
                    ->where('class_reg_id', $classRegId)
                    ->first();
    }

}