<?php namespace App\Repositories;

use App\Models\ClassCreditDiscount;

class ClassCreditDiscountRepository extends BaseRepository
{

    /**
     * @param ClassCreditDiscount $model
     */
    public function __construct(ClassCreditDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassCreditDiscount::select('class_credit_discounts.*');
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getByClassRegId($classRegId)
    {
        $query = $this->createSelectStatement()
                      ->where('class_reg_id', $classRegId);

        return $query->first();
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->where('is_deleted', false)
                    ->get();
    }
}