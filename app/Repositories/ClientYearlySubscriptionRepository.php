<?php namespace App\Repositories;

use App\Models\ClientYearlySubscription;
use App\Models\YearlySubscription;

class ClientYearlySubscriptionRepository extends BaseRepository
{
    /**
     * @param ClientYearlySubscription $model
     */
    public function __construct(ClientYearlySubscription $model)
    {
        $this->model = $model;
    }

    /**
     * Get by yearly subscription
     *
     * @param YearlySubscription $yearlySubscription
     * @return mixed
     */
    public function getByYearlySubscription(YearlySubscription $yearlySubscription)
    {
        $query = $this->model
                      ->where('yearly_subscription_id', '=', $yearlySubscription->id);

        return $query->get();
    }
}