<?php namespace App\Repositories\Eloquent;

use App\Repositories\Auth\BasicAuth;
use App\Repositories\Contracts\CoursemillInterface;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

abstract class CmRepository implements CoursemillInterface {
    /**
     * @var Client
     */
    private $client;

    private $model;

    private $options;

    /**
     * CmRepository constructor
     * Auto Inject BasicAuth as part of the option of the request
     * @param BasicAuth $auth
     * @param Collection $collection
     */
    public function __construct(BasicAuth $auth, Collection $collection)
    {
        $this->client = new Client([
            'base_uri' => env('CM_WEB_API')
        ]);
        $this->options = $collection;
        $this->pushOption($auth->getAuth());
        $this->pushOption(['verify'=>false]);
        $this->model = $this->model();
    }

    /**
     * Require path to API method
     * @return mixed
     */
    public abstract function model();

    public function get()
    {
        return $this->makeRequest();
    }

    public function post()
    {
        return $this->makeRequest('POST');
    }

    public function makeRequest($method = 'GET')
    {
        return $this->client->request($method, $this->model, $this->options->toArray());
    }

    public function pushOption($option = []) {
        $this->options = $this->options->merge($option);
        return $this;
    }

    public function acceptJson()
    {
        $this->pushOption(['headers' => ['Accept' => 'application/json']]);
        return $this;
    }

    public function acceptAndAddJson($param = [])
    {
        $this->acceptJson()->pushOption(['json' => $param]);
        return $this;
    }
}