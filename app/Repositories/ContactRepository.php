<?php namespace App\Repositories;

use App\Utilities\Constant;
use Auth;
use App\Models\Contact;

class ContactRepository extends BaseRepository {

    /**
     * @param Contact $model
     */
    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return Contact::select('contacts.*');
    }

    /**
     * Get member customers
     *
     * @return mixed
     */
    public function getAllCustomers()
    {
        $getNonMemberAccounts = $this->createSelectStatement()
                                    ->join('users', function($join)
                                    {
                                        $join->on('contacts.id' , '=', 'users.contact_id');
                                    })
                                    ->join('user_roles', function($joinUserRoles)
                                    {
                                        $joinUserRoles->on('users.id', '=', 'user_roles.user_id');
                                    })
                                    ->where('user_roles.role_id', '!=', Constant::MEMBER)
                                    ->orderBy('created_at', 'DESC');

        $nonMemberContactIds = $getNonMemberAccounts->get()->pluck('id')->toArray();

        $getCustomers = $this->createSelectStatement()
                             ->whereNotIn('id', $nonMemberContactIds)
                             ->orderBy('created_at', 'DESC');

        return $getCustomers->get();
    }

    /**
     * @param $id
     * @param array $notIn
     * @return mixed
     */
    public function getByUserWithContact($id, $notIn = [])
    {
        $query = $this->createSelectStatement()
            ->whereNotIn('contacts.id', $notIn)
            ->join('address_book_entries', function($join) use ($id)
            {
                $join->on('contacts.id', '=', 'address_book_entries.contact_id');
            })
            ->where('address_book_entries.user_id',  $id)
            ->orderBy('contacts.first_name');

        return $query->get();
    }

    /**
     * @param $agentId
     * @param $contactsWithinIds
     * @return mixed
     */
    public function getByUserNotWithinRegistration($agentId, $contactsWithinIds)
    {
        $query = $this->createSelectStatement()
                      ->join('address_book_entries', 'contacts.id', '=', 'address_book_entries.contact_id')
                      ->where('address_book_entries.user_id', $agentId)
                      ->whereNotIn('contacts.id', $contactsWithinIds)
                      ->orderBy('contacts.first_name');

        return $query->get();
    }
}