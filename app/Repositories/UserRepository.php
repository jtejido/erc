<?php namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return User::select('users.*');
    }

    /**
     * Get users by their roles
     *
     * @param $role
     * @return mixed
     */
    public function getByRoles($role)
    {
        $query = $this->createSelectStatement()
                      ->join('user_roles', 'user_roles.user_id', '=', 'users.id')
                      ->where(function($query) use ($role){
                          $query->where('user_roles.role_id', '=', 2);
                      });

        return $query->get();
    }

    /**
     * Get user account using verification token.
     *
     * @param   string $verificationId
     * @return  User
     */
    public function getUserByVerificationId($verificationId)
    {
        return $this->createSelectStatement()
            ->where('verification_token', $verificationId)
            ->get()
            ->first();
    }

    /**
     * Get user by email but not logged in user
     *
     * @param $email string
     * @param $userId integer
     * @return User 
     */
    public function getByEmailNotLoggedIn($email, $userId)
    {
        return $this->createSelectStatement()
                    ->where('id', '!=', $userId)
                    ->where('email', $email)
                    ->first();
    }

    /**
     * @return mixed
     */
    public function all() {
        return $this->createSelectStatement()->all();
    }

    /**
     * @return mixed
     */
    public function management() {
        return User::management()->get();
    }

}