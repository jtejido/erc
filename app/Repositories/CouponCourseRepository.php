<?php namespace App\Repositories;

use App\Models\CouponCourse;

class CouponCourseRepository extends BaseRepository
{
    /**
     * @param CouponCourse $model
     */
    public function __construct(CouponCourse $model)
    {
        $this->model = $model;
    }

    /**
     * Get by coupon id
     *
     * @param $couponId
     * @return mixed
     */
    public function getByCouponId($couponId)
    {
        $query = $this->model
                      ->where('coupon_id', '=', $couponId);

        return $query->get();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getByDiscountable($params = [])
    {
        $query = $this->model
                      ->where('discountable_type', $params['discountable_type'])
                      ->whereIn('discountable_id', $params['discountable_ids']);

        return $query->get();
    }
}