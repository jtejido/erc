<?php namespace App\Repositories;

use App\Models\YearlySubscriptionType;

class YearlySubscriptionTypesRepository extends BaseRepository
{

    /**
     * @param YearlySubscriptionType $model
     */
    public function __construct(YearlySubscriptionType $model)
    {
        $this->model = $model;
    }
}