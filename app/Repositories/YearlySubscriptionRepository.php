<?php namespace App\Repositories;

use App\Models\YearlySubscription;
use App\Utilities\Constant;

class YearlySubscriptionRepository extends BaseRepository
{
    /**
     * @param YearlySubscription $model
     */
    public function __construct(YearlySubscription $model)
    {
        $this->model = $model;
    }

    /**
     * Get pending subscription
     *
     * @param $agent
     * @param bool|true $pending
     * @return mixed
     */
    public function getByAgent($agent, $pending = true)
    {
        $query = $this->model
                      ->where('originating_agent_user_id', '=', $agent);

        if ($pending) {
            $query->where('status', '=', Constant::PENDING);

            return $query->first();
        }

        return $query->get();
    }
}