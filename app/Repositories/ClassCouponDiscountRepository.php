<?php namespace App\Repositories;

use App\Models\ClassCouponDiscount;

class ClassCouponDiscountRepository extends BaseRepository
{

    /**
     * @param ClassCouponDiscount $model
     */
    public function __construct(ClassCouponDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassCouponDiscount::select('class_coupon_discounts.*');
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getByClassRegId($classRegId)
    {
        return $this->createSelectStatement()
                    ->where('class_reg_id', $classRegId)
                    ->get();
    }

    /**
     * @param $coupDiscountId
     * @param $classId
     * @return mixed
     */
    public function getByCoupDiscountAndClassId($coupDiscountId, $classId)
    {
        $query = $this->createSelectStatement()
                      ->where('coupon_discount_id', $coupDiscountId)
                      ->where('class_reg_id', $classId);

        return $query->first();
    }

    /**
     * @param $orderId
     * @param $classId
     * @return mixed
     */
    public function getByClassIdAndOrderId($orderId, $classId)
    {
        $query = $this->createSelectStatement()
                      ->where('class_reg_id', $classId)
                      ->join('coupon_discounts', function($join)
                      {
                          $join->on('class_coupon_discounts.coupon_discount_id', '=', 'coupon_discounts.id');
                      })
                      ->where('coupon_discounts.order_id', $orderId);

        return $query->first();
    }


    /**
     * @param $coupDiscountId
     * @return mixed
     */
    public function getByCoupDiscountNoNullRegistration($coupDiscountId)
    {
        $query = $this->createSelectStatement()
                      ->where('coupon_discount_id', $coupDiscountId)
                      ->join('class_registrations', function($join)
                      {
                          $join
                              ->on('class_registrations.id', '=', 'class_coupon_discounts.class_reg_id');
                      })
                      ->where('class_registrations.deleted_at', NULL);

        return $query->get();
    }

    /**
     * @param array $couponDiscountIds
     * @return mixed
     */
    public function getByCouponDiscountIds($couponDiscountIds = [])
    {
        return $this->model
                    ->whereIn('coupon_discount_id', $couponDiscountIds)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->get();
    }

}