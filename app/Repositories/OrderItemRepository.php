<?php namespace App\Repositories;

use App\Models\OrderItem;
use App\Models\RegistrationOrder;
use App\Utilities\Constant;

class OrderItemRepository extends BaseRepository
{

    /**
     * @var OrderRepository
     */
    private $orderRepo;

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingAgentRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param OrderItem $model
     */
    public function __construct(OrderItem $model,
                                OrderRepository $orderRepo,
                                ActingOriginatingAgentRepository $actingAgentRepo,
                                ClassRegistrationRepository $classRegistrationRepo)
    {
        $this->model = $model;
        $this->orderRepo = $orderRepo;
        $this->actingAgentRepo = $actingAgentRepo;
        $this->classRegistrationRepo = $classRegistrationRepo;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return OrderItem::select('order_items.*');
    }

    /**
     * @param $orderIds
     * @return mixed
     */
    public function getByOrderIds($orderIds = [], $pending = true)
    {
        $query = $this->createSelectStatement()
            ->whereIn('order_id', $orderIds)
            ->orderBy('created_at', 'DESC');

        if ($pending) {
            $query->whereIn('status', [Constant::PENDING,Constant::INVOICED]);
        }

        else {
            $query = $query->withTrashed();
        }

        return $query->get();
    }

    /**
     * Get by orderable
     *
     * @param $orderableId
     * @param $orderableType
     * @return mixed
     */
    public function getByOrderable($orderableId, $orderableType, $all = true)
    {
        $query =  $this->createSelectStatement()
                       ->where('orderable_id', $orderableId)
                       ->where('orderable_type', $orderableType)
                       ->withTrashed()
                       ->orderBy('created_at', 'DESC');

        if ($all) {
            return $query->get();
        }

        return $query->first();
    }

    /**
     * Get single by order id and orderable type
     *
     * @param $orderId
     * @param $orderableType
     * @return mixed
     */
    public function getByOrderIdAndOrderableType($orderId, $orderableType, $getSingle = true)
    {
        $query = $this->createSelectStatement()
                      ->where('order_id', $orderId)
                      ->where('orderable_type', $orderableType);

        if ($getSingle) {
            return $query->first();
        }

        return $query->get();
    }

    /**
     * Get by multiple orderables
     *
     * @param $orderableIds
     * @param $orderableType
     * @return mixed
     */
    public function getByOrderables($orderableIds, $orderableType)
    {
        $query =  $this->createSelectStatement()
                       ->whereIn('orderable_id', $orderableIds)
                       ->where('orderable_type', $orderableType)
                       ->withTrashed();

        return $query->get();
    }

    public function checkFromOrderItemsIfThereIsItemWithType($orderItems, $type)
    {
        $count = 0;
        foreach ($orderItems as $orderItem) {
            if ($orderItem->orderable instanceof RegistrationOrder) {
                if ($orderItem->orderable->isCourse()) {
                    $count += ($orderItem->orderable->registrable->course_type_id == $type) ? 1 : 0;
                }
            }
        }
        return $count;
    }

    /**
     * Get all items by order
     *
     * @param $userId
     * @param $contactId
     * @param bool|true $pending
     * @return mixed|null
     */
    public function getAllItems($userId, $contactId = null, $pending = true)
    {
        $result = null;
        $orderIds = [];
        $orders = $this->orderRepo
                       ->getByUserId($userId, $pending);



        if (isset($userId)) {

            if ($orders) {

                if ($pending) {
                    array_push($orderIds, $orders->id);
                } else {
                    foreach ($orders as $order) {
                        array_push($orderIds, $order->id);
                    }
                }
            }

            $result = $this->getByOrderIds($orderIds, $pending);

            $actingAgentTransactions = $this->actingAgentRepo
                ->getByAgent($userId, $pending);

            if ($actingAgentTransactions->count()) {
                foreach ($actingAgentTransactions as $actingAgentTransaction) {
                    $orderItems = $this->getByOrderable(
                        $actingAgentTransaction->orderable_id,
                        $actingAgentTransaction->orderable_type
                    );

                    $result = $result->merge($orderItems);
                }
            }
        }

        elseif (isset($contactId)) {
            $registrationOrderIds = [];
            $classRegistrations = $this->classRegistrationRepo->getByAttendee($contactId);

            if ($classRegistrations->count()) {

                foreach ($classRegistrations as $classRegistration) {
                    array_push($registrationOrderIds, $classRegistration->registrationOrder->id);
                }

                if (count($registrationOrderIds)) {
                    $result = $this->getByOrderables(
                        $registrationOrderIds,
                        Constant::REGISTRATION_ORDER
                    );
                }
            }
        }

        return $result;
    }
}