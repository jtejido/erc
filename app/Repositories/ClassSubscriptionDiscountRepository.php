<?php namespace App\Repositories;

use App\Models\ClassSubscriptionDiscount;

class ClassSubscriptionDiscountRepository extends BaseRepository
{

    /**
     * @param ClassSubscriptionDiscount $model
     */
    public function __construct(ClassSubscriptionDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassSubscriptionDiscount::select('class_subscription_discounts.*');
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->get();
    }
}