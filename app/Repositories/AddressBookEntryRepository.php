<?php namespace App\Repositories;

use App\Models\AddressBook;

class AddressBookEntryRepository extends BaseRepository
{

    /**
     * @param AddressBook $model
     */
    public function __construct(AddressBook $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return AddressBook::select('address_book_entries.*');
    }

    /**
     * Get by contact id
     *
     * @param $contactId
     * @return mixed
     */
    public function getByContactId($contactId)
    {
        return $this->createSelectStatement()
                    ->where('contact_id', $contactId)
                    ->get();
    }

    /**
     * Get by owner id and contact id
     *
     * @param $owner
     * @param $contact
     * @return mixed
     */
    public function getByOwnerAndContact($owner, $contact)
    {
        $query = $this->createSelectStatement()
            ->where('user_id', $owner)
            ->where('contact_id', $contact);

        return $query->first();
    }
}
