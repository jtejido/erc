<?php namespace App\Repositories;

use DB;
use App\Models\Order;
use App\Models\RegistrationOrder;
use App\Utilities\Constant;
use Illuminate\Support\Facades\Auth;

class OrderRepository extends BaseRepository
{

    /**
     * @param Order $model
     */
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return Order::select('orders.*');
    }

    /**
     * @return mixed
     */
    public function createDBSelectStatement()
    {
        return DB::table('orders');
    }

    /**
     * @param $userId
     * @param bool|true $pending
     * @return mixed
     */
    public function getByUserId($userId, $pending = true)
    {
        $query = $this->createSelectStatement()
                       ->where('user_id', '=', $userId);

        if ($pending) {
            $query->where('status', '=', Constant::PENDING);

            return $query->first();
        }

        if (Auth::user()->isAdminorCSR()) {
            return $query->withTrashed()->get();
        }

        return $query->get();
    }

    /**
     * @param $userId
     * @param bool|true $pending
     * @return mixed
     */
    public function getByUserIdForInvoiceInfo($userId, $orderId, $pending = true)
    {
        $query = $this->createSelectStatement()
                       ->where('user_id', '=', $userId)
                       ->where('id','=',$orderId);

        if ($pending) {
            $query->where('status', '<>', Constant::COMPLETE);

            return $query->first();
        }

        return $query->get();
    }

    /**
     * @param $orderId
     * @param $userId
     * @return mixed
     */
    public function getByIdAndUserId($orderId, $userId)
    {
        $query = $this->createSelectStatement()
                      ->where('id', $orderId)
                      ->where('user_id', $userId);

        return $query->first();
    }

    /**
     * @param $orderableType
     * @param $orderableId
     * @return mixed
     */
    public function getSingleByOrderable($orderableType, $orderableId)
    {
        $query = $this->createDBSelectStatement()
                      ->select('orders.id')
                      ->leftJoin('order_items', function($join) use ($orderableType, $orderableId)
                      {
                            $join->on('orders.id', '=', 'order_items.order_id');
                      })
                      ->where('order_items.orderable_type', '=', $orderableType)
                      ->where('order_items.orderable_id', '=', $orderableId);

        return $query->first();
    }

    /**
     * @param $orderableType
     * @param $orderableId
     * @return mixed
     */
    public function getSingleModelByOrderable($orderableType, $orderableId)
    {
        $query = $this->model
                      ->leftJoin('order_items', function($join) use ($orderableType, $orderableId)
                      {
                        $join->on('orders.id', '=', 'order_items.order_id');
                      })
                      ->where('order_items.orderable_type', '=', $orderableType)
                      ->where('order_items.orderable_id', '=', $orderableId);

        return $query->first();
    }

}
