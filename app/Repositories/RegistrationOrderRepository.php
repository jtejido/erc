<?php namespace App\Repositories;

use App\Models\RegistrationOrder;
use App\Utilities\Constant;

class RegistrationOrderRepository extends BaseRepository
{

    /**
     * @var ActingOriginatingAgentRepository
     */
    private $actingOriginatingAgentRepo;

    /**
     * @var ClassRegistrationRepository
     */
    private $classRegistrationRepo;

    /**
     * @param RegistrationOrder $model
     * @param ClassRegistrationRepository $classRegistrationRepo
     * @param ActingOriginatingAgentRepository $actingOriginatingAgentRepo
     */
    public function __construct(RegistrationOrder $model,
                                ClassRegistrationRepository $classRegistrationRepo,
                                ActingOriginatingAgentRepository $actingOriginatingAgentRepo)
    {
        $this->model = $model;
        $this->classRegistrationRepo     = $classRegistrationRepo;
        $this->actingOriginatingAgentRepo = $actingOriginatingAgentRepo;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return RegistrationOrder::select('registration_orders.*');
    }

    /**
     * Get registrations by class and agent
     *
     * @param $registrableId
     * @param $agent
     * @param null $status
     * @return mixed
     */
    public function getByRegistrableIdAndAgent($registrableId, $agent, $status = null)
    {
        $query = $this->createSelectStatement()
            ->where('registrable_id', $registrableId)
            ->where('originating_agent_user_id', $agent);

        if ($status) {
            $query->where('status', '=', $status);
        }

        return $query->first();
    }

    /**
     * Get registrations by agent and pending
     *
     * @param $agent
     * @return mixed
     */
    public function getByAgentPending($agent)
    {
        $query = $this->createSelectStatement()
            ->where('originating_agent_user_id', $agent)
            ->where('status', '=', Constant::PENDING);

        return $query->get();
    }

    /**
     * @param $registrableId
     * @param $registrableType
     * @param $attendee
     * @return null
     */
    public function getRegisteredActingAgent($registrableId, $registrableType, $attendee)
    {
        $item = null;
        $registeredClass = $this->classRegistrationRepo
                                ->getByRegistrationOrderAttendeePending(
                                    $registrableId, $registrableType, $attendee);

        if ($registeredClass) {
            $registrationOrder = $registeredClass->registrationOrder;

            $actingAgent = $this->actingOriginatingAgentRepo
                                ->getByOrderableAndAgent([
                                    'orderable_id'      => $registrationOrder->id,
                                    'orderable_type'    => class_basename($registrationOrder),
                                    'acting_agent_id'   => $attendee->user->id,
                                    'status'            => Constant::PENDING
                                ]);

            if ($actingAgent)
                $item = $actingAgent->orderable;

        }

        return $item;
    }

    /**
     * @param $registrableId
     * @param $registrableType
     * @return mixed
     */
    public function getByRegistrable($registrableId, $registrableType)
    {
        $query = $this->createSelectStatement()
                      ->where('registrable_id', $registrableId)
                      ->where('registrable_type', $registrableType);

        return $query->first();
    }

    /**
     * @param $orderId
     * @param $discountableIds
     * @param $discountableType
     * @return mixed
     */
    public function getForDiscount($orderId, $discountableIds, $discountableType)
    {
        $query = $this->createSelectStatement()
                      ->whereIn('registrable_id', $discountableIds)
                      ->where('registrable_type', $discountableType)
                      ->join('order_items', function($join)
                      {
                            $join->on('registration_orders.id', '=', 'order_items.orderable_id');
                      })
                      ->where('order_items.order_id', $orderId)
                      ->where('order_items.orderable_type', Constant::REGISTRATION_ORDER);

        return $query->get();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function getForHWMDOTDiscount($params = [])
    {
        $query = $this->createSelectStatement()
                      ->whereIn('registration_orders.registrable_id', $params['registrable_ids'])
                      ->where('registration_orders.registrable_type', Constant::COURSE_CLASS_OBJECT)
                      ->where('registration_orders.status', $params['status'])
                      ->where('registration_orders.has_pair', $params['has_pair'])
                      ->join('order_items', function($joinItems) use ($params)
                      {
                          $joinItems
                              ->on('order_items.orderable_id', '=', 'registration_orders.id');
                      })
                      ->where('order_items.orderable_type', '=', Constant::REGISTRATION_ORDER)
                      ->where('order_items.order_id', '=', $params['order_id']);

        if (isset($params['all'])) {
            return $query->get();
        }

        return $query->first();
    }

    /**
     * @param $classId
     * @return mixed
     */
    public function getRegistrationIdsByClassId($classId)
    {
        $query = $this->model
                      ->where('registrable_id', $classId)
                      ->where('registrable_type', Constant::COURSE_CLASS_OBJECT);

        return $query->get();
    }
}