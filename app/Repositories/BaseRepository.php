<?php namespace App\Repositories;

abstract class BaseRepository
{

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    public function __construct()
    {

    }

    /**
     * Get all
     *
     * @return  \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get by Id
     *
     * @param   integer     $id
     * @param   bool        $ignoreTrash
     * @return  \Illuminate\Database\Eloquent\Model
     */
    public function getById($id, $withTrash = false)
    {
        return $withTrash ? $this->model->withTrashed()->find($id) : $this->model->find($id);
    }

    /**
     * Get query by ids
     *
     * @param array     $ids
     */
    public function getByIdsQuery($ids)
    {
        return $this->model->whereIn('id', $ids);
    }

    /**
     * Get by Ids
     *
     * @param   array   $ids
     * @param   bool    $withTrash
     * @return  \Illuminate\Database\Eloquent\Collection
     */
    public function getByIds($ids, $withTrash = false)
    {
        $query = $this->getByIdsQuery($ids);

        return $withTrash ?  $query->withTrashed()->get() : $query->get();
    }

    /**
     * Get one given column values
     *
     * @param   array   $columnValues
     * @return  \Illuminate\Database\Eloquent\Model
     */
    public function findOne($columnValues = [])
    {
        if ( ! is_array($columnValues))
            throw new \InvalidArgumentException('columnValues must be an array.');

        if (empty($columnValues))
            throw new \LengthException('columnValues must not be empty.');

        return $this->model->where(function($query) use ($columnValues)
        {
            foreach ($columnValues as $column => $value) {
                $query->where($column, '=', $value);
            }
        })->first();
    }

    /**
     * Get by column with optional condition ['=', '<>', '>', '<', 'LIKE', etc...]
     *
     * @param string $column
     * @param string $value
     * @param string $condition
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByColumn($column, $value, $condition = '=')
    {
        return $this->model->where($column, $condition, $value);
    }

    /**
     * Get by lists | When specifying fields to be included in select
     *
     * @param string $name
     * @param string $value
     * @return mixed
     */
    public function getByList($name = 'name', $value = 'id')
    {
        return $this->model->lists($name, $value);
    }

    public function update($data, $id, $attribute="id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }
}