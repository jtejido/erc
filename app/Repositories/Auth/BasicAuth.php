<?php namespace App\Repositories\Auth;

class BasicAuth {

    private $auth;

    /**
     * BasicAuth constructor.
     */
    public function __construct()
    {
        $this->auth = $this->makeAuth();
    }

    /**
     * @return array
     */
    private function makeAuth()
    {
        return ['auth' => [env('CM_API_USERNAME'), env('CM_API_PASSWORD')]];
    }

    public function getAuth()
    {
        return $this->auth;
    }

}