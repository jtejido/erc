<?php namespace App\Repositories;

use App\Http\Requests\BookRequest;
use App\Models\Book;
use App\Models\OrderItem;
use App\Repositories\Criteria\Book\SearchBookByKeywordCriteria;
use App\Repositories\Criteria\SearchOrFilterCriteria;
use App\Repositories\Criteria\SortByCriteria;
use App\Repositories\Eloquent\Repository;
use App\Utilities\Constant;

class BookRepository extends Repository
{
    function model()
    {
        return 'App\Models\Book';
    }

    /**
     * @param BookRequest $request
     * @param $fields
     * @return mixed
     */
    public function createBook(BookRequest $request, $fields)
    {
        if ($request->hasFile('thumbnail')) {
            $fields = $this->storeBookFile($request, $fields);
        }
        return $this->create($fields);
    }

    /**
     * @param BookRequest $request
     * @param $book
     * @param $fields
     * @return mixed
     * @internal param $id
     */
    public function updateBook(BookRequest $request , $book, $fields)
    {
        if ($request->hasFile('thumbnail')) {
            $this->deleteBookFile($book);
            $fields = $this->storeBookFile($request, $fields);
        }

        return $this->update($fields, $book->id);
    }

    /**
     * @param Book $book
     * @param FileEntryRepository $bookEntryRepo
     */
    public function deleteBook(Book $book, FileEntryRepository $bookEntryRepo)
    {
        /**
         * Delete Associated Files of Given Book
         */
        foreach ($book->files as $bookEntry)
        {
            $bookEntryRepo->deleteFileEntry($bookEntry);
        }
        $this->deleteBookFile($book->thumbnail);
        $book->delete();
    }

    /**
     * @param $filename
     */
    protected function deleteBookFile($filename)
    {
        $this->deleteFile('books/', $filename);
    }

    /**
     * @param BookRequest $request
     * @param $fields
     * @return mixed
     * @internal param $ |Request $request
     */
    public function storeBookFile(BookRequest $request, $fields)
    {
        $file = $request->file('thumbnail');
        $filename = $this->storeThumbnail(Book::FOLDERPATH_THUMBS, $file);
        $fields['thumbnail'] = $filename;
        $fields['thumbnail_mime'] = $file->getClientMimeType();
        $fields['thumbnail_orig_filename'] = $file->getClientOriginalName();
        return $fields;
    }

    public function sortBooks($sortby)
    {
        switch ($sortby) {
            case '0':
                $this->pushCriteria(new SortByCriteria('updated_at', 'DESC'));
                break;
            case '2':
                $this->pushCriteria(new SortByCriteria('name', 'DESC'));
                break;
            case '3':
                $this->pushCriteria(new SortByCriteria('price'));
                break;
            case '4':
                $this->pushCriteria(new SortByCriteria('price', 'DESC'));
                break;
            case '5':
                $this->pushCriteria(new SortByCriteria('code'));
                break;
            case '6':
                $this->pushCriteria(new SortByCriteria('code', 'DESC'));
                break;
            default:
                $this->pushCriteria(new SortByCriteria('name'));
        }
    }

    /**
     * @param $keyword
     */
    public function searchByKeyword($keyword)
    {
        if($keyword != '') {
            $this->pushCriteria(new SearchBookByKeywordCriteria($keyword));
        }
    }

    public function addToCart($request)
    {
        $qty = intval($request->get('quantity'));
        $book = $this->find($request->get('book_id'));
        $discounted = $request->input('discounted', false);

        $params['status']           = Constant::PENDING;
        $params['orderable_type']   = Book::class;
        $params['orderable_id']     = $book->id;
        $params['quantity']         = $qty;
        $price = ($discounted) ? $book->discounted_price : $book->price;
        $params['price_charged']    = $price * $qty;

        $this->saveProductOrder($params);

        return true;
    }

    private function saveProductOrder($params = [])
    {
        $orderItem = new OrderItem();
        $orderItem->fill($params);
        $orderItem->save();

        return $orderItem;
    }

}