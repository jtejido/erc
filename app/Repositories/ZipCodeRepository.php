<?php namespace App\Repositories;

use App\Models\ZipCode;

class ZipCodeRepository extends BaseRepository {

    /**
     * @param ZipCode $model
     */
    public function __construct(ZipCode $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ZipCode::select('zip_codes.*');
    }
}