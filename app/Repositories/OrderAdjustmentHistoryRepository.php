<?php namespace App\Repositories;

use App\Models\OrderAdjustmentHistory;

class OrderAdjustmentHistoryRepository extends BaseRepository
{

    /**
     * @param OrderAdjustmentHistory $model
     */
    public function __construct(OrderAdjustmentHistory $model)
    {
        $this->model = $model;
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->model
                    ->where('order_id', $orderId)
                    ->orderBy('created_at', 'DESC')
                    ->get();
    }
}