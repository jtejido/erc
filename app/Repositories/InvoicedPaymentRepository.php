<?php namespace App\Repositories;

use App\Models\InvoicedPayment;

class InvoicedPaymentRepository extends BaseRepository
{

    /**
     * @param InvoicedPayment $model
     */
    public function __construct(InvoicedPayment $model)
    {
        $this->model = $model;
    }

    /**
     * @param $type
     * @param $name
     * @return mixed
     */
    public function getSingleByType($type, $name)
    {
        return $this->model
                    ->where($type, $name)
                    ->first();
    }
}