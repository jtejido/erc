<?php namespace App\Repositories;

use App\Models\ActingOriginatingAgent;
use App\Utilities\Constant;

class ActingOriginatingAgentRepository extends BaseRepository
{
    /**
     * @param ActingOriginatingAgent $model
     */
    public function __construct(ActingOriginatingAgent $model)
    {
        $this->model = $model;
    }

    /**
     * Get by orderable and agent
     *
     * @param array $params
     * @return mixed
     */
    public function getByOrderableAndAgent($params = [])
    {
        $query = $this->model
                      ->withTrashed()
                      ->where('orderable_id', '=', $params['orderable_id'])
                      ->where('orderable_type', '=', $params['orderable_type']);

        if (isset($params['status'])) {
            $query->where('status', '=', $params['status']);
        }

        if (isset($params['acting_agent_id'])) {
            $query ->where('acting_agent_id', '=', $params['acting_agent_id']);

            return $query->first();
        }

        return $query->get();
    }

    /**
     * Get by agent
     *
     * @param $agentId
     * @return mixed
     */
    public function getByAgent($agentId, $pending = true, $all = true)
    {
        $query = $this->model
                      ->withTrashed()
                      ->where('acting_agent_id', '=', $agentId);

        if ($pending) {
            $query->where('status', Constant::PENDING);
        }

        $response = $all ? $query->get() : $query->first();

        return $response;
    }
}