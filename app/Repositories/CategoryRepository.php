<?php namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository {

    /**
     * @param Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return Category::select('categories.*');
    }
}