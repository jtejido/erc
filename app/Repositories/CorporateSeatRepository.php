<?php namespace App\Repositories;

use App\Models\CorporateSeat;

class CorporateSeatRepository extends BaseRepository
{
    /**
     * @param CorporateSeat $model
     */
    public function __construct(CorporateSeat $model)
    {
        $this->model = $model;
    }
}