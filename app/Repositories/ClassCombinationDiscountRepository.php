<?php namespace App\Repositories;

use App\Models\ClassCombinationDiscount;

class ClassCombinationDiscountRepository extends BaseRepository
{

    /**
     * @param ClassCombinationDiscount $model
     */
    public function __construct(ClassCombinationDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassCombinationDiscount::select('class_combination_discounts.*');
    }

    /**
     * @param $contactId
     * @return mixed
     */
    public function getByContactId($contactId)
    {
        return $this->createSelectStatement()
                    ->where('attendee_contact_id', $contactId)
                    ->get();
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->get();
    }

    /**
     * @param $classRegId
     * @param bool|false $single
     * @return mixed
     */
    public function getByClassRegistrationId($classRegId, $single = false)
    {
        $query = $this->createSelectStatement()
                      ->where('class_reg_one', $classRegId)
                      ->orWhere('class_reg_two', $classRegId)
                      ->where('is_deleted_by_credit', false)
                      ->where('is_deleted', false);

        if ($single) {
            return $query->first();
        }

        return $query->get();
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getInClassOne($classRegId)
    {
        $query = $this->createSelectStatement()
                      ->where('class_reg_one', $classRegId);

        return $query->first();
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getInClassTwo($classRegId)
    {
        $query = $this->createSelectStatement()
                      ->where('class_reg_two', $classRegId);

        return $query->first();
    }
}