<?php namespace App\Repositories;

use App\Models\InvoicedPaymentRecipient;

class InvoicedPaymentRecipientRepository extends BaseRepository
{

    /**
     * @param InvoicedPaymentRecipient $model
     */
    public function __construct(InvoicedPaymentRecipient $model)
    {
        $this->model = $model;
    }
}