<?php namespace App\Repositories;

use App\Models\CourseClass;
use Carbon\Carbon;

class ClassRepository extends BaseRepository
{

    /**
     * @var CourseRepository
     */
    private $courseRepo;

    /**
     * @param CourseClass $model
     */
    public function __construct(CourseClass $model,
                                CourseRepository $courseRepo)
    {
        $this->model = $model;
        $this->courseRepo = $courseRepo;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return CourseClass::select('classes.*');
    }

    /**
     * Get courses by search string or filters
     *
     * @param array $params
     * @return mixed
     */
    public function searchFilter($params = [])
    {
        $query = $this->createSelectStatement();


        if (isset($params['status'])) {
            $query = 1 == $params['status']
                ? $query->where('is_deactivated', false)
                : $query->where('is_deactivated', true);
        }

        if (isset($params['course'])) {
            $query->where('course_id', '=', $params['course']);
        }

        return $query->get();
    }

    public function between($start, $end)
    {
        $query = $this->createSelectStatement();
        $query->whereBetween('start_date', [$start, $end]);
        $query->orderBy('start_date', 'ASC');
        return $query->get();
    }

    public function between_and_activated($start, $end)
    {
        $query = $this->createSelectStatement();
        $query->whereHas('course',function($q){
                $q->where('is_deactivated', false);
            })
            ->where('is_cancelled', false)
            ->where('is_deactivated', false)
            // ->whereBetween('start_date', [$start, $end])
            ->where('start_date', '>=', $start)
            ->where('end_date', '<=', $end)
            ->where('end_date', '>=', Carbon::now()->startOfDay())
            ->orderBy('start_date', 'ASC');
        return $query->get();
    }

    public function NotPrivatebetween_and_activated($start, $end)
    {
        $query = $this->createSelectStatement();
        $query->whereHas('course',function($q){
                $q->where('is_deactivated', false);
            })
            ->where('is_cancelled', false)
            ->where('is_deactivated', false)
            ->where('is_private', false)
            // ->whereBetween('start_date', [$start, $end])
            ->where('start_date', '>=', $start)
            ->where('end_date', '<=', $end)
            ->where('end_date', '>=', Carbon::now()->startOfDay())
            ->orderBy('start_date', 'ASC');
        return $query->get();
    }

    /**
     * Get ids of classes of HWM in pricing rules
     *
     * @return mixed
     */
    public function getIdsOfHWMPricingRules()
    {
        $classArray = [];
        $courses = $this->courseRepo->getHWMWithPricingRules();

        if ($courses->count()) {
            foreach ($courses as $course) {
                foreach ($course->classes as $class) {
                    array_push($classArray, $class->id);
                }
            }
        }

        return $classArray;
    }

    /**
     * Get ids of classes of HWM in pricing rules
     *
     * @return mixed
     */
    public function getIdsOfDOTPricingRules()
    {
        return $this->courseRepo
                    ->getDOTWithPricingRules()
                    ->classes
                    ->pluck('id')
                    ->toArray();
    }

    /**
     * Get ids of classes of excluded
     *
     * @return mixed
     */
    public function getIdsOfExcluded()
    {
        $classArray = [];
        $courses = $this->courseRepo->getExcludedGroupDiscountRules();

        if ($courses->count()) {
            foreach ($courses as $course) {
                if($course && $course->classes()->count()) {
                    foreach ($course->classes as $class) {
                        array_push($classArray, $class->id);
                    }
                }
            }
        }

        return $classArray;
    }

    /**
     * @param array $ids
     * @param null $expDate
     * @return mixed
     */
    public function getByIdsOrderByStartDate($ids = [], $expDate = null)
    {
        $query = $this->model
                      ->whereIn('id', $ids);

        if ($expDate) {
            $query->where('start_date', '<=', $expDate);
        }

        $query->orderBy('start_date', 'ASC');

        return $query->first();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByActiveInstructorId($id)
    {
        $query = $this->model->scopeActive($this->model->where('instructor_id', $id));

        return $query->get();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByActiveClassId($id)
    {
        $query = $this->model->scopeActive($this->model->where('id', $id));

        return $query->first();
    }

    public function getRelatedClasses(CourseClass $class)
    {
        if ($class->location_id) {
            $from = $class->start_date->subDays(5)->toDateString();
            $to = $class->end_date->addDays(5)->toDateString();
            return CourseClass::whereBetween('start_date', [$from, $to])
                ->where('location_id', $class->location_id)
                ->where('id', '!=', $class->id)
                ->get();
        }

        return null;
    }

    public function linkClass($class, $rel_class, $auto = false)
    {
        if($class->classes()->where('id', $rel_class->id)->count() == 0) {
            $class->classes()->save($rel_class, ['auto' => $auto]);
            $rel_class->classes()->save($class, ['auto' => $auto]);
        }

    }

    /**
     * @param array $params
     * @return mixed
     */
    public function searchFilterReports($params = [])
    {
        $query = $this->createSelectStatement()
                      ->select('classes.*');

        if (isset($params['start_date']) and strlen(trim($params['start_date'])) > 0) {
            $query = $query->where('start_date', '>=', $params['start_date']);
        }

        if (isset($params['end_date']) and strlen(trim($params['end_date'])) > 0) {
            $query = $query->where('end_date', '<=', $params['end_date']);
        }

        if ((isset($params['city']) and strlen(trim($params['city'])) > 0) or
            isset($params['state']) and strlen(trim($params['state'])) > 0) {
            $query = $query->join('locations', function($joinLocation)
                        {
                            $joinLocation
                                ->on('classes.location_id', '=', 'locations.id');
                        });
        }

        if (isset($params['city']) and strlen(trim($params['city'])) > 0) {
            $query = $query->where('locations.city', 'LIKE', '%' . $params['city'] . '%');
        }

        if (isset($params['state']) and strlen(trim($params['state'])) > 0) {
            $query = $query->where('locations.state', 'LIKE', '%' . $params['state'] . '%');
        }

        if (isset($params['title']) and strlen(trim($params['title'])) > 0) {
            $query = $query->join('courses', function($joinCourses)
                        {
                            $joinCourses
                                ->on('classes.course_id', '=', 'courses.id');
                        })
                        ->where('courses.title', 'LIKE', '%' . $params['title']  . '%');
        }

        return $query;
    }

}