<?php namespace App\Repositories;

use App\Models\ClassCombination;

class ClassCombinationRepository extends BaseRepository
{

    /**
     * @param ClassCombination $model
     */
    public function __construct(ClassCombination $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassCombination::select('class_combinations.*');
    }

    /**
     * Get by registration order ids
     *
     * @param $regOne
     * @param $regTwo
     * @return mixed
     */
    public function getByRegIds($regOne, $regTwo)
    {
        $query = $this->createSelectStatement()
                      ->where('reg_one', $regOne)
                      ->where('reg_two', $regTwo);

        return $query->first();
    }

    /**
     * Get by single registration order id
     *
     * @param $registrationId
     * @return mixed
     */
    public function getBySingeRegId($registrationId)
    {
        $query = $this->createSelectStatement()
                      ->where('reg_one', $registrationId)
                      ->orWhere('reg_two', $registrationId);

        return $query->first();
    }
}