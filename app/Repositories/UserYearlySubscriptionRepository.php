<?php namespace App\Repositories;

use App\Models\UserYearlySubscription;

class UserYearlySubscriptionRepository extends BaseRepository
{

    /**
     * @param UserYearlySubscription $model
     */
    public function __construct(UserYearlySubscription $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return UserYearlySubscription::select('user_yearly_subscriptions.*');
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getByUserId($userId)
    {
        return $this->findOne(['user_id', $userId]);
    }

    /**
     * @param $id
     * @param $activationCode
     * @return mixed
     */
    public function getByIdAndActivationCode($id, $activationCode)
    {
        $query = $this->createSelectStatement()
                      ->where('id', $id)
                      ->where('activation_code', $activationCode)
                      ->where('activated', 0);

        return $query->first();
    }
}