<?php namespace App\Repositories;

use App\Models\ClassHalfPriceDiscount;

class ClassHalfPriceDiscountRepository extends BaseRepository
{

    /**
     * @param ClassHalfPriceDiscount $model
     */
    public function __construct(ClassHalfPriceDiscount $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function createSelectStatement()
    {
        return ClassHalfPriceDiscount::select('class_half_price_discounts.*');
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getByOrderId($orderId)
    {
        return $this->createSelectStatement()
                    ->where('order_id', $orderId)
                    ->where('is_deleted_by_credit', false)
                    ->where('is_deleted', false)
                    ->get();
    }

    /**
     * @param $classRegId
     * @return mixed
     */
    public function getByClassRegId($classRegId)
    {
        return $this->createSelectStatement()
                    ->where('class_reg_id', $classRegId)
                    ->first();
    }

}