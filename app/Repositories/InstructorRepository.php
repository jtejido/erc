<?php namespace App\Repositories;

use App\Models\Instructor;

class InstructorRepository extends BaseRepository
{

    /**
     * @param Instructor $model
     */
    public function __construct(Instructor $model)
    {
        $this->model = $model;
    }

}