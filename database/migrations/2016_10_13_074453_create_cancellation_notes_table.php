<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCancellationNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cancellation_notes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id')->ungsigned();
            $table->bigInteger('class_registration_id')->unsigned();
            $table->text('content');
            $table->timestamps();

            $table->foreign('class_registration_id')->references('id')->on('class_registrations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cancellation_notes');
    }
}
