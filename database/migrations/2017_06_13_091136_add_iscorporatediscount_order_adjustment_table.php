<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIscorporatediscountOrderAdjustmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_adjustments', function(Blueprint $table)
        {
            $table->boolean('is_corporate_discount_adjustment')->after('is_product_qty_adjustment')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_adjustments', function(Blueprint $table)
        {
            $table->dropColumn('is_corporate_discount_adjustment');
        });
    }
}
