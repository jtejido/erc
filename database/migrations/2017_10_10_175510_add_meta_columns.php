<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function(Blueprint $table)
        {
            $table->text('meta_title')->nullable();
            $table->text('meta_desc')->nullable();
        });

        Schema::table('posts', function(Blueprint $table)
        {
            $table->text('meta_title')->nullable();
            $table->text('meta_desc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function(Blueprint $table)
        {
            $table->dropColumn(['meta_title', 'meta_desc']);
        });

        Schema::table('posts', function(Blueprint $table)
        {
            $table->dropColumn(['meta_title', 'meta_desc']);
        });
    }
}
