<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientCorporateSeatCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_corporate_seat_credits', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('originating_agent_user_id')->unsigned();
            $table->integer('credits');
            $table->decimal('price');
            $table->decimal('paid_price');
            $table->string('status');
            $table->timestamps();

            $table->foreign('originating_agent_user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_corporate_seat_credits');
    }
}
