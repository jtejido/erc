<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicedPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoiced_payments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->string('po_image')->nullable();
            $table->string('po_file')->nullable();
            $table->string('po_image_mime')->nullable();
            $table->string('po_number')->nullable();
            $table->string('check_image')->nullable();
            $table->string('check_file')->nullable();
            $table->string('check_image_mime')->nullable();
            $table->string('check_number')->nullable();
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoiced_payments');
    }
}
