<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDropInstructorColumnsClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classes', function(Blueprint $table)
        {
            $table->dropColumn('instructor_name');
            $table->integer('instructor_id')->unsigned()->nullable()->after('zip');

            $table->foreign('instructor_id')->references('id')->on('instructors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('classes', function(Blueprint $table)
        {
            $table->dropForeign(['instructor_id']);
        });
    }
}
