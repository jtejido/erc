<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddrssToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->text('shipping_address')->nullable()->after('completed_at');
            $table->text('shipping_zip')->nullable()->after('shipping_address');
            $table->text('billing_address')->nullable()->after('shipping_zip');
            $table->text('billing_zip')->nullable()->after('billing_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['shipping_address','shipping_zip','billing_address','billing_zip']);
        });
    }
}
