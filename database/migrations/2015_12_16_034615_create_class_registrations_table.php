<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_registrations', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->integer('registration_order_id')->unsigned();
            $table->integer('attendee_contact_id')->unsigned();
            $table->decimal('item_charge')->nullable();
            $table->decimal('paid_charge')->nullable();
            $table->boolean('attended')->default(false);
            $table->boolean('meeting_flag')->default(false);
            $table->boolean('exam_flag')->default(false);
            $table->boolean('reminder_flag')->default(false);
            $table->boolean('is_half_priced')->default(false);
            $table->boolean('corporate_seat_credit_applied')->default(false);
            $table->boolean('certificate_sent')->default(false);
            $table->string('certificate_file_path')->nullable();
            $table->string('certificate_link')->nullable();
            $table->text('exam_result')->nullable();
            $table->boolean('is_new')->default(false);
            $table->boolean('is_cancelled')->default(false);
            $table->boolean('is_swapped')->default(false);
            $table->text('swap_note')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();

            $table->softDeletes();

            $table->foreign('registration_order_id')->references('id')->on('registration_orders')
                ->onDelete('cascade');
            $table->foreign('attendee_contact_id')->references('id')->on('contacts')
                ->onDelete('cascade');
        });

        DB::unprepared('alter table class_registrations MODIFY column updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_registrations');
    }
}
