<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('email');
            $table->string('auth_code');
            $table->integer('transaction_id');
            $table->string('masked_cc');
            $table->decimal('amount');
            $table->boolean('status');
            $table->timestamp('created_at');

            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_transactions');
    }
}
