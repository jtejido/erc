<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_adjustments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('agent_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('order_item_id')->unsigned();
            $table->integer('adjustable_id')->nullable();
            $table->string('adjustable_type')->nullable();
            $table->decimal('amount');
            $table->string('adjustment_action');
            $table->text('note');
            $table->boolean('is_half_price_adjustment')->default(false);
            $table->boolean('is_group_discount_adjustment')->default(false);
            $table->boolean('is_product_qty_adjustment')->default(false);
            $table->timestamps();

            $table->foreign('agent_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('order_item_id')->references('id')->on('order_items')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_adjustments');
    }
}
