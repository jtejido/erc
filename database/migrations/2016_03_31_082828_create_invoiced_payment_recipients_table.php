<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicedPaymentRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoiced_payment_recipients', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('invoiced_payment_id')->unsigned();
            $table->string('email');
            $table->timestamps();

            $table->foreign('invoiced_payment_id')->references('id')->on('invoiced_payments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoiced_payment_recipients');
    }
}
