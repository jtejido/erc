<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('course_mill_course_id')->unique()->nullable();
            $table->string('course_mill_session_id')->nullable();
            $table->string('meeting_id')->nullable();
            $table->string('test_link')->nullable();
            $table->string('title');
            $table->string('citation')->nullable();
            $table->text('description');
            $table->string('sku')->unique();
            $table->integer('course_type_id')->unsigned();
            $table->decimal('price');
            $table->string('duration');
            $table->boolean('is_deactivated')->default(false);
            $table->timestamps();

            $table->foreign('course_type_id')->references('id')->on('course_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
