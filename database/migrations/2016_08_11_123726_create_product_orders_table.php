<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('product_type');
            $table->integer('quantity');
            $table->integer('paid_quantity');
            $table->decimal('price');
            $table->decimal('paid_price');
            $table->unsignedInteger('originating_agent_user_id');
            $table->string('status');

            $table->foreign('originating_agent_user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_orders');
    }
}
