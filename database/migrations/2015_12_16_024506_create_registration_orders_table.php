<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('registrable_id');
            $table->string('registrable_type');
            $table->integer('originating_agent_user_id')->unsigned();
            $table->string('status');
            $table->string('slug')->unique()->nullable();
            $table->boolean('has_pair')->default(false);
            $table->timestamps();

            $table->foreign('originating_agent_user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registration_orders');
    }
}
