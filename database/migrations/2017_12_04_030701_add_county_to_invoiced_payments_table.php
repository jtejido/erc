<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddCountyToInvoicedPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->string('billing_county')->nullable();
            $table->string('shipping_county')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->dropColumn(['billing_county','shipping_county']);
        });
    }
}
