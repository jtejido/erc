<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateTaxToPaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->decimal('state_tax')->nullable();
        });

        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->decimal('state_tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_transactions', function (Blueprint $table) {
            $table->dropColumn(['state_tax']);
        });

        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->dropColumn(['state_tax']);
        });
    }
}
