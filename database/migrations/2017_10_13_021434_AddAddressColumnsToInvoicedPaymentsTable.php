<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressColumnsToInvoicedPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->string('billing_company')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_state')->nullable();
            $table->string('billing_zip')->nullable();
            $table->string('shipping_company')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_state')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->decimal('trans_total')->nullable();
            $table->decimal('shipping_rate')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoiced_payments', function (Blueprint $table) {
            $table->dropColumn(['billing_address','billing_company', 'billing_city', 'billing_state', 'billing_zip',
                'shipping_address','shipping_company', 'shipping_city', 'shipping_state', 'shipping_zip',
                'trans_total', 'shipping_rate']);
        });
    }
}
