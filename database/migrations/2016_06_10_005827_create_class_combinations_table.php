<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_combinations', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('reg_one')->unsigned();
            $table->integer('reg_two')->unsigned();
            $table->timestamps();

            $table->foreign('reg_one')->references('id')->on('registration_orders')
                ->onDelete('cascade');

            $table->foreign('reg_two')->references('id')->on('registration_orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_combinations');
    }
}
