<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYearlySubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yearly_subscriptions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('type_id')->unsigned();
            $table->integer('originating_agent_user_id')->unsigned();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->decimal('total_price');
            $table->boolean('ended')->nullable();
            $table->string('status');
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('yearly_subscription_types')
                  ->onDelete('cascade');
            $table->foreign('originating_agent_user_id')->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('yearly_subscriptions');
    }
}
