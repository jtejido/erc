<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnsiteTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('onsite_trainings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('originating_agent_user_id')->unsigned();
            $table->text('address');
            $table->string('training_date');
            $table->decimal('price');
            $table->timestamps();

            $table->foreign('originating_agent_user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('onsite_trainings');
    }
}
