<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_template_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('email_template_id')->unsigned();
            $table->text('content');
            $table->timestamps();

            $table->foreign('email_template_id')
              ->references('id')->on('email_templates')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_template_contents');
    }
}
