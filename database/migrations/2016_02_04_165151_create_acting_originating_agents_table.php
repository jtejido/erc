<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActingOriginatingAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acting_originating_agents', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('orderable_id');
            $table->string('orderable_type');
            $table->integer('acting_agent_id')->unsigned();
            $table->string('status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('acting_agent_id')->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acting_originating_agents');
    }
}
