<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReminderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminder_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->string('slug')->nullable();
            $table->text('data')->nullable();
            $table->unsignedInteger('to')->nullable();
            $table->unsignedInteger('remindable_id')->nullable();
            $table->string('remindable_type')->nullable();
            $table->timestamps();

            $table->foreign('to')->references('id')->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reminder_histories');
    }
}
