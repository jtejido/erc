<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_links', function (Blueprint $table) {
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('linkable_id');
            $table->string('linkable_type');
            $table->boolean('auto')->default(false);

            $table->foreign('class_id')->references('id')->on('classes')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_links');
    }
}
