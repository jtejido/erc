<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassCreditDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_credit_discounts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->bigInteger('class_reg_id')->unsigned();
            $table->decimal('deduction');
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('agent_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('class_reg_id')->references('id')->on('class_registrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_credit_discounts');
    }
}
