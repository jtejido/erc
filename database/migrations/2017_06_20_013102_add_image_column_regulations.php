<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageColumnRegulations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regulations', function(Blueprint $table)
        {
            $table->string('photo')->after('is_published')->nullable();
            $table->string('photo_mime')->after('photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regulations', function(Blueprint $table)
        {
            $table->dropColumn(['photo', 'photo_mime']);
        });
    }
}
