<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('contact_id')->unsigned()->nullable();
            $table->string('email')->unique();
            $table->string('course_mill_user_id')->nullable();
            $table->string('password', 255);
            $table->boolean('state')->nullable();
            $table->boolean('has_military_discount')->default(false);
            $table->integer('corporate_seat_credits')->default(0);
            $table->string('remember_token');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('contact_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
