<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('filename');
            $table->string('mime');
            $table->string('original_filename');
            $table->string('disk')->default('local');
            $table->string('path');
            $table->string('visibility')->default('private');
            $table->string('publicURI')->nullable();
            $table->integer('size')->default(0);
            $table->unsignedInteger('imageable_id');
            $table->string('imageable_type');
            $table->string('type')->default('FILE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('file_entries');
    }
}
