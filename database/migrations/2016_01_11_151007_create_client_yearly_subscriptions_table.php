<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientYearlySubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_yearly_subscriptions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('yearly_subscription_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->decimal('temporary_price');
            $table->decimal('paid_price');
            $table->date('subscription_start_date')->nullable()->default(NULL);
            $table->date('subscription_exp_date')->nullable()->default(NULL);
            $table->timestamps();

            $table->foreign('yearly_subscription_id')->references('id')->on('yearly_subscriptions')
                  ->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_yearly_subscriptions');
    }
}
