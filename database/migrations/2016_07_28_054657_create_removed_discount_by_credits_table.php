<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemovedDiscountByCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('removed_discount_by_credits', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('class_credit_discount_id')->unsigned();
            $table->bigInteger('class_reg_id')->unsigned();
            $table->integer('discountable_id');
            $table->string('discountable_type');
            $table->timestamps();

            $table->foreign('class_credit_discount_id')->references('id')->on('class_credit_discounts')
                ->onDelete('cascade');
            $table->foreign('class_reg_id')->references('id')->on('class_registrations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('removed_discount_by_credits');
    }
}
