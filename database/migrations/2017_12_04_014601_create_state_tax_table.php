<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateStateTaxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('state_tax', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state_abbrev');
            $table->string('county');
            $table->double('state_rate');
            $table->double('county_rate');
            $table->double('transit_rate');
            $table->double('others_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('state_tax');
    }
}
