<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->unique();
            $table->string('thumbnail')->nullable();
            $table->string('thumbnail_mime')->nullable();
            $table->string('thumbnail_orig_filename')->nullable();
            $table->text('description')->nullable();
            $table->decimal('price')->default(0);
            $table->decimal('add_on_price')->default(0);
            $table->decimal('weight')->default(0);


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}


