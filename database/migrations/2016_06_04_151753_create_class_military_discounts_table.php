<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassMilitaryDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_military_discounts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->bigInteger('class_reg_id')->unsigned();
            $table->decimal('deduction');
            $table->boolean('is_deleted_by_credit')->default(false);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('contact_id')->references('id')->on('contacts')
                ->onDelete('cascade');

            $table->foreign('class_reg_id')->references('id')->on('class_registrations')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_military_discounts');
    }
}
