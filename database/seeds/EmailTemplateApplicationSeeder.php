<?php

use App\Models\EmailTemplate;
use App\Models\EmailTemplateApplication;
use Illuminate\Database\Seeder;

class EmailTemplateApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
{
        EmailTemplate::bySlug('ua-register')->applications()->save(
            new EmailTemplateApplication([
                'application_type' => App\Models\User::class,
                'label' => 'User',
                'display' => 'contact.name'])
        );

        EmailTemplate::bySlug('ua-forgot-password')->applications()->save(
            new EmailTemplateApplication([
                'application_type' => App\Models\User::class,
                'label' => 'User',
                'display' => 'contact.name'])
        );

        $applications = [
            new EmailTemplateApplication([
                'application_type' => App\Models\Order::class,
                'label' => 'Order',
                'display'          => ['title','created_at'],
            ]),
            new EmailTemplateApplication([
                'application_type' => App\Models\RegistrationOrder::class,
                'label' => 'Registration',
                'display'          => ['date'],
            ]),
            new EmailTemplateApplication([
                'application_type' => App\Models\ClassRegistration::class,
                'label' => 'Attendee',
                'display'          => ['date'],
            ]),
        ];

        $map = [
            \App\Models\RegistrationOrder::class => ['name'=>\App\Models\Order::class,'field'=>'order_id'],
            \App\Models\ClassRegistration::class => ['name'=>\App\Models\RegistrationOrder::class,'field'=>'registration_order_id'],
        ];

        foreach ($applications as $key => $application) {
            $type = $application->application_type;
            if (array_key_exists($type, $map)) {

                $parent_reference = $map[$type]['field'];
                $parent_id = EmailTemplate::bySlug('reg-seminars-confirmation-user')
                    ->applications()
                    ->whereApplicationType($map[$type]['name'])
                    ->first()
                    ->id;

                $application->parent_id        = $parent_id;
                $application->parent_reference = $parent_reference;
            }

            EmailTemplate::bySlug('reg-seminars-confirmation-user')
                ->applications()
                ->save($application);
        }

    }
}
