<?php

use Illuminate\Database\Seeder;
use App\Models\Coupon;
use App\Utilities\Constant;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coupon_types = [Constant::PERCENT, Constant::FIXED];
        for ($i=1; $i <= 20; $i++) {
            $k = array_rand($coupon_types);
            $coupon_type = $coupon_types[$k];
            $coupon = [
                //generate 6 digit random coupon code
                'coupon_code' => str_random(6),
                //generate random decimal between 10 and 80
                'coupon_amount' => $coupon_type == Constant::PERCENT
                                   ? intval(mt_rand (10*10, 80*10) / 10)
                                   : mt_rand (10*10, 80*10) / 10,
                'coupon_type' => $coupon_type,
                'created_at'  => new \DateTime('now')
            ];
            Coupon::create($coupon)->save();
        }
    }
}
