<?php

use Illuminate\Database\Seeder;

class ERGBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $book = \App\Models\Book::where('code', 'erg2016')->first();

        $discount1 = new \App\Models\BookDiscount();
        $discount1->book_id = $book->id;
        $discount1->condition = '10-49';
        $discount1->discount = 3.45;
        $discount1->save();

        $discount2 = new \App\Models\BookDiscount();
        $discount2->book_id = $book->id;
        $discount2->condition = '50+';
        $discount2->discount = 3.25;
        $discount2->save();

        echo "Done.";
    }
}
