<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PRStatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::beginTransaction();

        try {
            $handle = fopen('database/seeds/puerto-rico-states.csv','r') or die ('File opening failed');
            $zip_codes = [];
            while (!feof($handle)) {
                $dd = fgets($handle);

                $dd = trim($dd);

                if(!$dd) continue;

                $zip_code = explode(',', $dd)[0];

                $zip_codes[] = ['zip_code' => $zip_code];

            }
            fclose($handle);

            DB::table('zip_codes')->insert($zip_codes);

        } catch (\Exception $e) {
            DB::rollback();
            echo 'Error Encountered, rolling back data.';
            return ;
        }

        DB::commit();
        echo 'Success.';

    }
}
