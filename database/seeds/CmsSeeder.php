<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\PostCategory;

class CmsSeeder extends Seeder {

    /**
     * Run the database seed
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            1 => 'Training',
            2 => 'Consulting',
            3 => 'Resources',
        ];

        foreach($categories as $order => $name) {
            PostCategory::firstOrCreate(compact('name', 'order'));
        }

        $main = [
            1 => 'Home',
            2 => 'About Us',
            3 => 'Careers',
            4 => 'Contact Us',
            5 => 'Schedule of Classes',
            6 => 'Compliance Calendar',
            7 => 'Our Team',
            8 => 'FAQ'
        ];

        foreach($main as $order => $name) {
            Post::create(compact('name', 'order'))->save();
        }

        $training = PostCategory::byName('Training')->first();
        $trainings = [
            1 => 'Benefits',
            2 => 'On-Site and Customized Training',
            3 => 'Subscriptions',
            4 => 'Instructor / Consultant Bios',
        ];

        foreach($trainings as $order => $name) {
            Post::create(compact('name','order'))
                ->category()->associate($training)
                ->save();
        }

        $consulting = PostCategory::byName('Consulting')->first();
        $consultations = [
            1 => 'Consulting',
            2 => 'Audits',
            3 => 'Permits',
            4 => 'Hazardous Materials/Dangerous Goods Classification',
            5 => 'SARA Title II Compliance and Reporting',
            6 => 'Plans and Procedures',
            7 => 'EHS Program Management',
            8 => 'Chemical Security Anti-Terrorism Standard',
        ];

        foreach($consultations as $order => $name) {
            Post::create(compact('name','order'))
                ->category()->associate($consulting)
                ->save();
        }

        $resources = PostCategory::byName('Resources')->first();
        $resource = [
            1 => 'Helpful Links',
            2 => 'Reg of the Day Archive',
            3 => 'Tips of the Week Archive',
        ];

        foreach($resource as $order => $name) {
            Post::create(compact('name','order'))
                ->category()->associate($resources)
                ->save();
        }



    }
}