<?php

use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmailTemplateUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $emails = [
            [
                'slug'      => 'course-mill-account-create',
                'desc'      => 'CourseMill New Account',
                'template'  => 'emails.templates.customer.course-mill-new-account'
            ],
            [
                'slug'      => 'class-registration-cancel-agent',
                'desc'      => 'Class Registration Cancel',
                'template'  => 'emails.templates.completed-registration.agent.class-registration-cancel'
            ],
            [
                'slug'      => 'corp-seat-subscription-purchase',
                'desc'      => 'Corporate Seat Subscription Purchase',
                'template'  => 'emails.templates.customer.corporate-seat-subscription'
            ],
            [
                'slug'      => 'order-modification-confirmed-agent',
                'desc'      => 'Order Modification',
                'template'  => 'emails.templates.completed-registration.agent.confirmed-order-modification'
            ],
            [
                'slug'      => 'subscription-used',
                'desc'      => 'Subscription Used',
                'template'  => 'emails.templates.classes.subscription-used'
            ],
            [
                'slug'      => 'attendance-confirmation',
                'desc'      => 'Attendance Confirmation',
                'template'  => 'emails.templates.classes.attendee.attendance-confirmation'
            ],
            [
                'slug'      => 'reg-cancellation',
                'desc'      => 'Training Cancellation',
                'template'  => 'emails.templates.classes.cancellation'
            ],
            [
                'slug'      => 'ua-register',
                'desc'      => 'New Account',
                'template'  => 'emails.templates.customer.new-account'
            ],
        ];

        foreach ($emails as $email) {

            DB::beginTransaction();

            try {
                $this->dropEmailTemplate($email['slug']);

                $emailTemplate = EmailTemplate::firstOrCreate([
                    'slug' => $email['slug']
                ]);

                $emailTemplate->update([
                    'description' => $email['desc']
                ]);

                $eContent = new EmailTemplateContent();
                $eContent->email_template_id = $emailTemplate->id;
                $eContent->content = view($email['template'])->render();
                $eContent->save();

            } catch (\Exception $e) {
                DB::rollback();

                echo 'Error - Rollback';

                throw $e;
            }

            echo 'Done - Commit';

            DB::commit();

        }

    }

    private function dropEmailTemplate($slug)
    {
        $template = EmailTemplate::where('slug', $slug)->first();

        if(!$template) return ;

        EmailTemplateContent::where('email_template_id', $template->id)->delete();

        $template->delete();

    }
}
