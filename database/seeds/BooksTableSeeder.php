<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
            [
                "title"         => "8-Hour Hazardous Waste Operations and Emergency Response Handbook",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 4,
                "desc"          => " <p>Designed to serve as your primary reference during the 24- and 40-hour hazwoper training course, this manual may also be used as reference on the job site.&nbsp; This handbook covers emergency response activities including hazard recognition and spill control and containment.&nbsp; It also covers waste site activities including site characterization, waste sampling and handling, and decontamination. Requirements for personal protective equipment (including respirators) and OSHA-required procedures for confined space entry, lockout/tagout, and medical surveillance are covered in-depth. </p><p><br></p>",
            ],
            [
                "title"         => "Advanced Hazardous Waste Management Handbook",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => " <p>This manual answers the most frequently asked questions on hazardous waste from environmental coordinators, compliance officers, managers, waste handlers, and others who need to know how to manage hazardous waste properly. It contains guidance on hazardous waste issues that are not clearly addressed in the federal regulations, providing insight on the gray areas with which you must contend. The manual describes EPA's proposed regulations as well as recent final rules so you won't be taken by surprise. Issues such as treatment of waste by generators, hidden exclusions, complex aspects of satellite accumulation point requirements, used oil mixtures, handling aerosol cans, and crushing fluorescent bulbs are explained. Includes full text of applicable regulations. </p><p><br>&nbsp;</p>",
            ],
            [
                "title"         => "DOT/RCRA Compliance Manual",
                "price"         => 179,
                "add_on_price"  => 0,
                "weight"        => 5,
                "desc"          => " <p>This manual includes instructions for classifying hazardous materials and hazardous wastes; procedures for determining packing groups and the proper DOT shipping name; selecting the proper container; marking and labeling containers; loading and unloading hazardous materials; preparing shipping papers and hazardous waste manifests; placarding vehicles; characterizing waste; accumulating hazardous waste; preparing land disposal notices; preparing for and preventing spills; recognizing the presence of hazardous substances in an emergency; the role of the first responder awareness individual; and appropriate emergency notifications. </p><p><br>&nbsp;</p>",
            ],
            [
                "title"         => "Environmental Auditing and Compliance Manual",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 3,
                "desc"          => "<em>Environmental Auditing and Compliance</em> gives you step-by-step procedures for conducting environmental audits in accordance with EPA guidelines, as well as \"plain English\" summaries of regulations that may apply to areas to be audited.&nbsp;Comprehensive checklists simplify and streamline the audit process.&nbsp;EPA is increasingly expecting industry to self-audit.&nbsp;This manual shows you how. <p>&nbsp;<br><br></p>",
            ],
            [
                "title"         => "Environmental Health and Safety Laws and Regulations",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 3,
                "desc"          => " <p>This handbook includes the history and purpose of the laws and regulations, permitting, recordkeeping, monitoring, training, and enforcement strategies for most EHS laws and regulations. It also includes key terms and definitions and explains common terminology for each law and regulation.</p><p>Laws and regulations covered include: </p><ul><li>Clean Air Act </li><li>Clean Water Act </li><li>Resource Conservation and Recovery Act and Hazardous and Solid Waste Amendments&nbsp; </li><li>Introduction to CERCLA, SARA, and the NCP </li><li>Emergency Planning And Community Right To Know Act </li><li>OSHA's Hazard Communication Standard and Occupational Exposure to Hazardous Chemicals in Laboratories </li><li>Asbestos Management </li><li>Solid Waste Management </li><li>OSHA's Standards for Personal Protective Equipment </li><li>Lead Abatement </li><li>Confined Space Entry </li><li>Process Safety Management </li><li>Hazardous Waste Operations and Emergency Response </li><li>Hazardous Materials Transportation Act, International Air Transportation Association, and International Maritime Organization </li><li>Toxic Substances Control Act </li><li>National Environmental Policy Act </li><li>Federal Insecticide, Fungicide, and Rodenticide Act </li><li>Safe Drinking Water Act <br>&nbsp; </li></ul>",
            ],
            [
                "title"         => "Handbook for the Management of Hazardous Waste",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "<p>This is a comprehensive handbook that provides detailed information on OSHA standards in plain English. <em>Occupational Safety and Health Standards for General Industry</em> covers the provisions of the Occupational Safety and Health Act that must be implemented in your workplace, with in-depth coverage of a variety of general industry safety and health standards including recordkeeping and reporting, PPE, lockout/tagout, confined spaces, machine safeguarding, process safety management, bloodborne pathogens, walking and working surfaces and more. It includes step-by-step procedures for compliance, and checklists and tables to guide you through the compliance process. The manual also contains OSHA inspection guidelines and interpretations. </p><p>This is the same comprehensive handbook used in our popular, two-day OSHA Compliance Course. </p><p><br></p>",
            ],
            [
                "title"         => "Handbook for the Management of Hazardous Waste in California",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "In accordance with the unique requirements of the state of California, this comprehensive manual includes simple, step-by-step instructions for characterizing waste, determining proper shipping names for waste, manifesting, completing land disposal notices, and record keeping. Also included are accumulation point inspection checklists and up-to-date hazardous waste regulations and hazardous waste lists. <br>&nbsp; <br><br>&nbsp;",
            ],
            [
                "title"         => "Handbook for the Management of Hazardous Waste in California and Hazardous",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Materials Management: Compliance with the DOT Requirements",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Handbook for the Management of Hazardous Waste in Texas",
                "price"         => 179,
                "add_on_price"  => 0,
                "weight"        => 5,
                "desc"          => "In accordance with the unique requirements of the state of Texas, this comprehensive manual includes simple, step-by-step instructions for characterizing waste, determining proper shipping names for waste, manifesting, completing land disposal notices, and recordkeeping. Also included are accumulation point inspection checklists and up-to-date hazardous waste regulations and hazardous waste lists. <br>&nbsp; <br><br>",
            ],
            [
                "title"         => "Hazardous Materials Management: Compliance with the DOT Requirements",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Hazardous Materials Management: Compliance with the DOT Requirements and  Transportation of Dangerous Goods: Compliance with IATA Regulations",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => " <p>This manual takes you through every step in the transport of hazardous materials: determining what is a hazardous material; selecting the proper shipping name, package, label, and placard; completing shipping papers; loading and stowing; incident response; and hazmat security requirements. Also included are details on on exceptions for limited quantities, consumer commodities, and materials of trade. The handbook also includes full text of the applicable DOT hazardous materials regulations. </p><p><br>&nbsp;</p>",
            ],
            [
                "title"         => "Hazardous Waste Land Disposal Restrictions",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Hazardous Waste Manifesting",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Hazardous Waste Operations and Emergency Response Handbook",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "How to Comply with the OSHA Hazard Communication Standard",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 4,
                "desc"          => " <p>This handbook clearly describes procedures for developing a hazard communication plan.&nbsp; Instructions for using MSDSs, labeling containers, keeping records, and training employees are included.&nbsp; Compliance checklists will help you ensure that nothing is missed in your OSHA-required hazard communication program. </p><p><br>&nbsp;</p>",
            ],
            [
                "title"         => "How to Ship Batteries by Ground and Air",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "How to Ship Consumer Commodities and Limited Quantities",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "How to Transport Dangerous Goods by Road within Europe (ADR)",
                "price"         => 125,
                "add_on_price"  => 39,
                "weight"        => 1,
                "desc"          => " <p>Information about the ADR is very difficult to find in the United States.&nbsp;Here is your chance to obtain an informative manual to guide you in all your European dangerous good shipments. This handbook comprehensively explains the transport operations of dangerous goods by road within Europe. Topics include training requirements, applicability of ADR to multi-modal export shipments involving air or sea transport, hazard classification, packaging, hazard communication, documentation, regulatory relief, and transport operations. </p>",
            ],
            [
                "title"         => "Transportation of Dangerous Goods by Sea:How to Comply with the IMDG Code",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Transportation of Dangerous Goods: Compliance with IATA Regulations",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => " <p>Explains how to comply with the International Air Transport Association's (IATA's) current Dangerous Goods Regulations for shipments by air, and the International Maritime Organization's (IMO's) IMDG Code for shipments by sea. </p><p>The handbook includes step-by-step instructions for: </p><ul><li>Classifying dangerous goods </li><li>Determining the correct shipping name </li><li>Using the IATA List of Dangerous Goods and the IMDG Code </li></ul><p>&nbsp;</p><p>Dangerous Goods List </p><ul><li>Selecting and preparing packages </li><li>Finding applicable state and operator variations </li><li>Marking, labeling, and placarding </li><li>Preparing shipping papers </li><li>Handling materials safely </li></ul><p>&nbsp;</p><p>Requirements for each mode of transport are covered separately to avoid confusion. </p><p><br></p>",
            ],
            [
                "title"         => "TSCA  Import Requirements",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "TSCA Requirements for Fluorescent Light Ballasts",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Universal Waste Handbook",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
            [
                "title"         => "Used Oil and Special Wastes Handbook",
                "price"         => 179,
                "add_on_price"  => 39,
                "weight"        => 5,
                "desc"          => "",
            ],
        ];

        foreach ($books as $book) {
            factory(App\Models\Book::class)->create([
                'name'  => $book["title"],
                'price'  => $book["price"],
                'add_on_price'  => $book["add_on_price"],
                'weight'  => $book["weight"],
                'description'  => $book["desc"],
            ]);
        }


    }
}
