<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;

class ContactUsMailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emailTemplate = EmailTemplate::firstOrCreate([
            'slug' => 'contact-request',
            'description' => 'Send request from contact us form'
        ]);

        $eContent = new EmailTemplateContent();
        $eContent->email_template_id = $emailTemplate->id;
        $eContent->content = view('emails.templates.customer.contact-us')->render();
        $eContent->save();
    }
}
