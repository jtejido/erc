<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Post;

class MetaDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getCourseSetMetaData(7, 'Are you involved in hazmat handling? DOT Hazardous Material Training is the right course for you. Do you want to know why? Click here to get started.');
        $this->getCourseSetMetaData(8, 'Take your knowledge to the next level. This is DOT Hazardous Material Training part 2. Watch how we do it. Do you want to get involved? Click here for details.');
        $this->getCourseSetMetaData(1, 'Learn more about hazardous waste management. Take this course now to know more about which product has the potential to harm you. Click here to get started.');
        $this->getCourseSetMetaData(3, 'Watch how we get things done. See how we get involved in the community. Listen to what these people has to say about us. Click here to get started.');
        $this->getCourseSetMetaData(2, 'On this course, you will learn more about handling and proper disposal of Hazardous Waste. Learn all the things that you need to know right now. Click here.');
        $this->getCourseSetMetaData(5, 'Do you want to know the latest updates and laws concerning hazardous waste? Check this refresher course from RCRA and DOT. Take part. Be involved. Start here.');
        $this->getCourseSetMetaData(6, 'Do you know that even your electronic materials that you use everyday can be a source of hazardous waste? Learn more about it now. Click here to get started.');
        $this->getCourseSetMetaData(91, 'Watch how we ship your goods by air. See how we implement our plans. We have specially trained personnel that knows how to do that. Click here to know more.');
        $this->getCourseSetMetaData(90, 'From planning to implementation. You will learn on this course our proven and effective way of shipping dangerous goods by air. Check out this course now.');
        $this->getCourseSetMetaData(96, 'We started in 1981. Listen to our experts. Learn how they managed it over the years. Do you have the same goal for your business? Click here for details.');
        $this->getCourseSetMetaData(80, 'Do you want to learn how to handle exposure to hazardous materials? Do you want to know the things you could do to save someone? Take charge. Start here.');
        $this->getCourseSetMetaData(82, 'Get to know the latest update on Hazardous Waste Management and learn what our experts has to say. Watch this new webcast now to know more. Click here.');
        $this->getCourseSetMetaData(9, 'This webact will introduce you to the latest update in hazardous waste management and you will know how DOT deals with them. Click here to get started.');
        $this->getCourseSetMetaData(81, 'Hazardous waste management do\'s and don\'ts. Learn what to avoid and know the things that you should do. Watch and learn how to manage hazardous waste. Star here.');
        $this->getCourseSetMetaData(86, 'Watch this DOT Driver Compliance Program. Learn the proper management system and how to apply them. Get started now. Click here for details.');
        $this->getCourseSetMetaData(83, 'Learn how to properly document and fill up the manifest for your hazardous waste. Learn the process. Watch the proper application. Get started here.');
        $this->getCourseSetMetaData(79, 'Learn how to classify hazardous waste. Know the things behind it and ways to take care of them. We\'ll show you how to get familiar with it. Start now.');
        $this->getCourseSetMetaData(71, 'Get to know the proper shipping of hazardous waste at sea. Learn how we implement our plans and procedures to ensure safety of our personal. Watch it here.');
        $this->getCourseSetMetaData(73, 'Learn how to classify hazardous waste and which one to prioritize. Hear what our experts have to say as they share their best practice. Get started here.');
        $this->getCourseSetMetaData(74, 'Learn how to manage your personnel and how to ensure safety in the workplace. We will share our best practice that made us one of the best. Start here now.');
        $this->getCourseSetMetaData(70, 'Have you checked all your compliance documents? Do you want to have a proven strategy that allowed us to excel in this industry? Get started here.');
        $this->getCourseSetMetaData(75, 'Here you will learn what to prioritize and how to organize. Against all the laws and regulations, regular updates and compliance. Click here to know more.');
        $this->getCourseSetMetaData(72, 'Start right. Do it right. Watch our proven and tested strategy to guide you in your transporation business. Listen to our panel of experts. Join here.');
        $this->getCourseSetMetaData(76, 'Take your knowledge to the next level. This is DOT Hazardous Material Training part 2. Watch how we do it. Do you want to get involved? Click here for details.');

        $this->getPostSetMetaData(30, 'Get to know about our upcoming seminars. For more info, our helpful Customer Service Representatives will be happy to assist you. Call us 800-537-2372 today!');
        $this->getPostSetMetaData(31, 'Want to know more about Environmental Resource Center? Check our webcasts now. Watch the latest event and the most on demand topic today.');
        $this->getPostSetMetaData(9, 'Subscribe now to receive the latest updates, upcoming events, seminar schedules and more. Enter your email below.');
        $this->getPostSetMetaData(2, 'Environmental Resources Center, the leading supplier of environmental, safety, and transportation consulting and training since 1981. Click here to know more.');
        $this->getPostSetMetaData(4, 'For environmental consulting & training, you can call our Environmental Resource Center at 800-537-2372 for a no-obligation estimate for any services or courses.');
        $this->getPostSetMetaData(24, 'Our team consist of dedicated individuals that are passionate to give you a better service. Visit our Environmental Resource Center for more info.');
        $this->getPostSetMetaData(34, 'Still unsure of what to do? Give us a call now. Our helpful Customer Service Representatives will be happy to answer all your questions.');
        $this->getPostSetMetaData(16, 'Do you want a personalized approach to your business? Check the detailed information of our  procedures and plans now.');
        $this->getPostSetMetaData(26, 'We have a state of the art Safety Database Development facility to ensure that all information are safe. Check our Environmental Resource Center for details.');
        $this->getPostSetMetaData(17, 'Our commitment to our Environmental, Health and Safety management is one of the best. Check our Plans and Procedures now to know more.');
        $this->getPostSetMetaData(11,'We take pride at our Consulting here at Environmental Resource Center. Our Consulting is one of the best in our industry. Pick up that phone and call us now.' );
        $this->getPostSetMetaData(19, 'Check out these Helpful Links below to know more. Take a look at how we put value into our customers and the directions they want to achieve on their business.');
        $this->getPostSetMetaData(7, 'Do you want to have clear direction in your business? Do you want to know the benefits of training with us? Click here to see how we value our partners.');
        $this->getPostSetMetaData(35, 'Our trainings are one of the best in the industry. Visit our Training Subscription now and leave your email to get updates on our regular training schedule.');
        $this->getPostSetMetaData(13, 'Our consulting are one of the latest and up-to-date. We make sure that we provide the highest standard of service. Visit our Plans and Procedures to know more.');
    }

    /**
     * @param $id
     * @param $description
     */
    private function getCourseSetMetaData($id, $description)
    {
        $course = Course::findOrFail($id);
        $course->meta_title = $course->title . ' | Environmental Resource Center';
        $course->meta_desc = $description;
        $course->save();
    }

    /**
     * @param $id
     * @param $description
     */
    private function getPostSetMetaData($id, $description)
    {
        $post = Post::findOrFail($id);
        $post->meta_title = $post->name . ' | Environmental Resource Center';
        $post->meta_desc = $description;
        $post->save();
    }
}
