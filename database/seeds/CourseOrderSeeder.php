<?php

use Illuminate\Database\Seeder;

class CourseOrderSeeder extends Seeder
{

    private $courseRepo;

    public function __construct(\App\Repositories\CourseRepository $courseRepository)
    {
        $this->courseRepo = $courseRepository;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courseOrders = \App\Models\CourseOrder::all();

        if ($courseOrders->count()) {
            $courseOrders->each(function($courseOrder) {
                $courseOrder->delete();
            });
        }

        $courseTypes = \App\Models\CourseType::all()->pluck('id')->toArray();
        $categories = \App\Models\Category::all()->pluck('id')->toArray();

        foreach ($courseTypes as $courseType) {
            foreach ($categories as $category) {
                $courses = $this->courseRepo->getByCategoryAndType($category, $courseType);

                if ($courses->count()) {
                    $ctr = 0;
                    foreach ($courses as $course) {
                        $ctr++;
                        $params = [
                            'course_id' => $course->id,
                            'category_id' => $category,
                            'course_type_id' => $courseType
                        ];

                        $paramsByCategoryType = [
                            'category_id' => $category,
                            'course_type_id' => $courseType,
                            'order' => $ctr
                        ];

                        $exists = \App\Models\CourseOrder::where($params)->first();
                        $existsByCatType = \App\Models\CourseOrder::where($paramsByCategoryType)->first();

                        if (!$exists and !$existsByCatType) {
                            $params['order'] = $ctr;
                            \App\Models\CourseOrder::create($params);

                            echo ".";
                        }
                    }
                }
            }
        }

        //HWM seminar course default order
        $this->searchCourse('Hazardous Waste Management: The Complete Course (RCRA)', 1, 1, 1);
        $this->searchCourse('RCRA and DOT Annual Update and Refresher', 1, 1, 2);
        $this->searchCourse('California Hazardous Waste Management', 1, 1, 3);
        $this->searchCourse('California Hazardous Waste and DOT Refresher Training', 1, 1, 4);
        $this->searchCourse('Texas Hazardous Waste Management', 1, 1, 5);
        $this->searchCourse('Advanced Hazardous Waste Management', 1, 1, 6);

        //HWM webcast course default order
        $this->searchCourse('Hazardous Waste Manifest Training Webcast', 1, 2, 1);
        $this->searchCourse('Hazardous Waste Generator Improvements Rule Webcast', 1, 2, 2);
        $this->searchCourse('Hazardous Waste Management: Annual Update Webcast', 1, 2, 3);
        $this->searchCourse('Universal Waste Webcast', 1, 2, 4);
        $this->searchCourse('Used Oil and Special Waste Webcast', 1, 2, 5);
        $this->searchCourse('Solvent Wipe Rule Webcast', 1, 2, 6);
        $this->searchCourse('Advanced Hazardous Waste Management Webcast', 1, 2, 7);
        $this->searchCourse('California Hazardous Waste Management Webcast', 1, 2, 8);
        $this->searchCourse('EPA’s New Electronic Hazardous Waste Manifest Webcast', 1, 2, 9);
        $this->searchCourse('Hazardous Waste Manifest Training Webcast', 1, 2, 10);
        $this->searchCourse('EPA’s Proposed New Standards for Hazardous Waste Pharmaceuticals Webcast', 1, 2, 11);
        $this->searchCourse('Hazardous Waste Exclusions for Hazardous Secondary Materials and Solvent Recycling Webcast', 1, 2, 12);
        $this->searchCourse('Hazardous Waste Land Disposal Restrictions Webcast', 1, 2, 13);

        //HMT seminar course default order
        $this->searchCourse('DOT Hazardous Materials Training: The Complete Course', 2, 1, 1);
        $this->searchCourse('RCRA and DOT Annual Update and Refresher', 2, 1, 2);
        $this->searchCourse('DOT/IATA: How to Ship Dangerous Goods by Ground and Air', 2, 1, 3);
        $this->searchCourse('IATA: How to Ship Dangerous Goods by Air', 2, 1, 4);
        $this->searchCourse('California Hazardous Waste and DOT Refresher Training', 2, 1, 5);

        //CBT
        $this->searchCourse('DOT Hazardous Materials: General Awareness', 2, 3, 1);
        $this->searchCourse('DOT Hazardous Materials Function-Specific: Bulk Loading and Unloading', 2, 3, 2);
        $this->searchCourse('DOT Hazardous Materials Function-Specific: Marking and Labeling', 2, 3, 3);
        $this->searchCourse('DOT Hazardous Materials Function-Specific: Non-Bulk Loading and Unloading', 2, 3, 4);
        $this->searchCourse('DOT Hazardous Materials Function-Specific: Packaging', 2, 3, 5);
        $this->searchCourse('DOT Hazardous Materials Function-Specific: Shipping Papers', 2, 3, 6);
        $this->searchCourse('DOT Hazardous Materials: Safety and Security Awareness', 2, 3, 7);
        $this->searchCourse('How to Ship Batteries by Ground and Air', 2, 3, 8);
        $this->searchCourse('How to Ship Bulk Hazardous Materials by Ground', 2, 3, 9);
        $this->searchCourse('How to Ship Consumer Commodities and Limited Quantities by Ground and Air', 2, 3, 10);
        $this->searchCourse('How to Ship Dry Ice by Ground and Air', 2, 3, 11);
        $this->searchCourse('How to Ship Infectious Substances by Ground and Air', 2, 3, 12);
        $this->searchCourse('How to Ship Medical Waste', 2, 3, 13);
        $this->searchCourse('Reverse Logistics Shipments of Hazardous Materials', 2, 3, 14);
        echo "\nDone.";
    }

    /**
     * @param string $title
     * @param int $categoryId
     * @param int $typeId
     * @param int $order
     */
    private function searchCourse($title = '', $categoryId = 1, $typeId = 1, $order = 1)
    {
        $course = \App\Models\Course::where('title', 'LIKE', '%' . $title . '%')->first();

        if ($course) {
            $courseOrder = \App\Models\CourseOrder::where([
                'course_id' => $course->id,
                'category_id' => $categoryId,
                'course_type_id' => $typeId
            ])->first();

            if ($courseOrder) {
                $subCourseOrder = \App\Models\CourseOrder::where([
                    'category_id' => $courseOrder->category_id,
                    'course_type_id' => $courseOrder->course_type_id,
                    'order' => $order
                ])->first();

                if ($subCourseOrder) {
                    $subCourseOrder->order = $courseOrder->order;
                    $subCourseOrder->save();
                }

                $courseOrder->order = $order;
                $courseOrder->save();
            }

            echo ".";
        }
    }
}
