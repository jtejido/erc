<?php

use App\Models\Location;
use App\Models\State;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class LocationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locations')->delete();
        //factory(App\Models\Location::class, 30)->create();

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load('public/imports/locations.xlsx')
            ->chunk(100, function($results) use (&$existingEmployeeCount)  {
                foreach($results as $row) {
                    echo '.';

                    $city = null;
                    $state = null;

                    if (!is_null($row->address2)) {
                        $addressArrray = explode(", ", $row->address2);
                        $city = $addressArrray[0];

                        if (isset($addressArrray[1])) {
                            $state = State::where('abbrev', $addressArrray[1])->first()->state;
                        }
                    }

                    Location::create([
                        'location_name' => $row->name,
                        'address' => $row->address,
                        'address2' => $row->address2,
                        'city' => $city,
                        'state' => $state,
                        'zip' => $row->zip,
                        'phone_number' => $row->phone,
                        'url' => $row->url
                    ]);
                }
            });

        echo "\n";
    }
}
