<?php

use Illuminate\Database\Seeder;
use App\Models\Instructor;

class InstructorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $instructors = [

            [
                'first_name' => 'Eric',
                'last_name' => 'Massey',
                'signature_mime' => 'image/png',
                'signature' => 'emassey.bin',
            ],
            [
                'first_name' => 'Pretlo',
                'last_name' => 'Knight',
                'signature_mime' => 'image/png',
                'signature' => 'pknight.bin',
            ],
            [
                'first_name' => 'Kristie Cook',
                'last_name' => 'Absher',
                'signature_mime' => 'image/png',
                'signature' => 'kabsher.bin',
            ],
            [
                'first_name' => 'Rebecca',
                'last_name' => 'Spaulding',
                'signature_mime' => 'image/png',
                'signature' => 'rspaulding.bin',
            ],
            [
                'first_name' => 'Brian',
                'last_name' => 'Karnofsky',
                'signature_mime' => 'image/png',
                'signature' => 'bkarnofsky.bin',
            ],
            [
                'first_name' => 'Harry',
                'last_name' => 'Favre',
                'signature_mime' => 'image/png',
                'signature' => 'hfavre.bin',
            ],
            [
                'first_name' => 'Barry',
                'last_name' => 'Gillespie',
                'signature_mime' => 'image/png',
                'signature' => 'bgillespie.bin',
            ],
            [
                'first_name' => 'Andy',
                'last_name' => 'Smith',
                'signature_mime' => 'image/png',
                'signature' => 'asmith.bin',
            ],
            [
                'first_name' => 'Lara',
                'last_name' => 'Reidenbaugh',
                'signature_mime' => 'image/png',
                'signature' => 'lreidenbaugh.bin',
            ],
            [
                'first_name' => 'Lizzie',
                'last_name' => 'Clifton',
                'signature_mime' => 'image/png',
                'signature' => 'eclifton.bin',
            ],
            [
                'first_name' => 'Regan',
                'last_name' => 'Bottomley',
                'signature_mime' => 'image/png',
                'signature' => 'rbottomley.bin',
            ]
        ];

        foreach ($instructors as $instructor) {
            Instructor::create($instructor);
        }
    }
}
