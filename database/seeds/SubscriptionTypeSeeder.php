<?php

use Illuminate\Database\Seeder;

class SubscriptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type1 = \App\Models\YearlySubscriptionType::where('name', 'WebCasts and Seminars')->first();
        $type2 = \App\Models\YearlySubscriptionType::where('name', 'WebCasts')->first();

        if ($type1) {
            $type1->name = \App\Utilities\Constant::WEBCASTS_SEMINARS;
            $type1->save();
        }

        if ($type2) {
            $type2->name = \App\Utilities\Constant::WEBCASTS;
            $type2->save();
        }

        echo "\nDone.";
    }
}
