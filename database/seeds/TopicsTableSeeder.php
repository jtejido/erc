<?php

use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topics = [
            ['name' => 'RCRA', 'duration' => 10],
            ['name' => 'DOT1', 'duration' => 10],
            ['name' => 'DOT3', 'duration' => 34],
            ['name' => 'IATA', 'duration' => 22],
            ['name' => 'HAZWOPER', 'duration' => 10],
        ];
        foreach ($topics as $topic) {
            factory(App\Models\Topic::class)->create([
                'name'          => $topic['name'],
                'duration'          => $topic['duration'],
            ]);
        }
    }
}
