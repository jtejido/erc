<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\Book;
use App\Models\CourseCategory;
use Faker\Factory as FakerFactory;

class CourseTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $course1 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Advanced Hazardous Waste Management',
                'description' => 'Go beyond the basics with this challenging course and delve into EPA\'s interpretations on the most daunting gray areas. Advanced Hazardous Waste Management is designed for those with prior RCRA training. This class covers much more than the regulatory requirements and meets your annual training requirement.
                                  You will learn which of your wastes are exempt or excluded that you may currently be managing as hazardous, which wastes do not count toward your generator status, how to handle episodic generation, requirements for importing and exporting hazardous waste, when permits are required and when treatment is allowed without a permit, how to minimize waste generation, and  the latest land disposal restrictions. Also covered are the most recent regulatory changes plus any pending EPA proposals that may affect your facility.',
                'sku' => uniqid(),
                'price' => 499,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1,
            ]);
        };

        $course = $course1();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 1
        ]);

        $course->books()->save(Book::find(2));

        $course2 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management: The Complete Course (RCRA)',
                'description' => "If your facility generates hazardous waste, you must comply with detailed regulatory requirements for waste characterization, container marking and labeling, waste minimization, manifesting, transportation, emergency response, and land disposal certification. EPA and state regulations require annual hazardous waste management training. At this two-day class you will learn the latest requirements of the law and how to comply.&nbsp;&nbsp;<br><br>This course will teach you how to properly identify, accumulate, minimize, and ship hazardous waste. You will learn how to develop a contingency plan and how to comply with land disposal restrictions. The course provides an in-depth look at the latest regulatory requirements, waste exemptions and exclusions, new universal waste requirements, VOC standards for hazardous waste generators, and waste minimization.&nbsp;<br><br><br><p>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul><li>CEU&#8212;1.6</li><li>CM Points IHMM&#8212;May be eligible for 16 CMP</li><li>IDEM Drinking Water&#8212;16 contact hours</li><li>IDEM Waste Water&#8212;16 general contact hours</li><li>NEHA&#8212;16 contact hours</li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;15 contact hours</span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 14 Continuing Education Hours</li><li>Kentucky Drinking Water - 14 Continuing Education Hours</li></ul><p><a href=''/training/ceus.aspx'>Click here</a> for a complete list of continuing education&nbsp;credits and approval numbers.<br>&nbsp;</p>",
                'sku' => uniqid(),
                'price' => 795,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1,
            ]);
        };

        $course = $course2();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 1
        ]);

        $course->books()->save(Book::find(6));

        $course3 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Advanced Hazardous Waste Management Webcast',
                'description' => 'Go beyond the basics with this challenging course and delve into EPA\'s interpretations on the most daunting gray areas. Advanced Hazardous Waste Management is designed for those with prior RCRA training. This class covers much more than the regulatory requirements and meets your annual training requirement.
                                  You will learn which of your wastes are exempt or excluded that you may currently be managing as hazardous, which wastes do not count toward your generator status, how to handle episodic generation, requirements for importing and exporting hazardous waste, when permits are required and when treatment is allowed without a permit, how to minimize waste generation, and  the latest land disposal restrictions. Also covered are the most recent regulatory changes plus any pending EPA proposals that may affect your facility.',
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '12:00 pm - 5:00 pm',
                'course_type_id' => 2
            ]);
        };

        $course = $course3();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 1
        ]);

        $course->books()->save(Book::find(2));

        $course4 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Biennial Reporting - Webcast',
                'description' => 'Go beyond the basics with this challenging course and delve into EPA\'s interpretations on the most daunting gray areas. Advanced Hazardous Waste Management is designed for those with prior RCRA training. This class covers much more than the regulatory requirements and meets your annual training requirement.
                                  You will learn which of your wastes are exempt or excluded that you may currently be managing as hazardous, which wastes do not count toward your generator status, how to handle episodic generation, requirements for importing and exporting hazardous waste, when permits are required and when treatment is allowed without a permit, how to minimize waste generation, and  the latest land disposal restrictions. Also covered are the most recent regulatory changes plus any pending EPA proposals that may affect your facility.',
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2
            ]);
        };

        CourseCategory::create([
            'course_id' => $course4()->id,
            'category_id' => 1
        ]);

        $course5 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'RCRA Hazardous Waste and DOT Hazardous Materials Update and Refresher Training',
                'description' => "This course is the perfect way to meet both your RCRA 40 CFR 265.16 annual review and DOT 49 CFR 172.704 (c)(2) recurrent training requirements. During this one-day class you will review the important concepts and procedures required for compliance with hazardous waste (EPA/RCRA) and hazardous materials transport (DOT/HMTA) regulations. You'll also learn how to avoid the most common violations cited by EPA and DOT, as well as how to comply with new requirements that have been enacted over the past year.&nbsp;<br><br>Your course materials include the course manual, <a href='/products/view.aspx?id=1318' target='_blank'><em>RCRA/DOT Compliance Manual</em></a><em>&nbsp;</em>and the <em><a href=''/products/view.aspx?id=1014' target='_blank'><em>Emergency Response Guidebook</em></a></em>.&nbsp;<br><br>" .
                                 "<p>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul><li>CEU&#8212;0.8</li><li>CM Points IHMM&#8212;May be eligible for 8 CMP</li>" .
                                 "<li>IDEM Drinking Water&#8212;8 contact hours</li><li>IDEM Waste Water&#8212;8 general contact hours</li>" .
                                 "<li>NEHA&#8212;8 contact hours</li>" .
                                 "<li><span class=Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;7.5 contact hours<br></span></li></ul>" .
                                 "<p><a href='/training/ceus.aspx'>Click here</a> for a complete list of continuing education&nbsp;credits and approval numbers.<br><br>&nbsp;</p>",
                'sku' => uniqid(),
                'price' => 549,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1
            ]);
        };

        $id = $course5()->id;

        for ($i = 1; $i <= 2; $i++) {
            CourseCategory::create([
                'course_id' => $id,
                'category_id' => $i
            ]);
        }

        $course6 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'EPA’s New Electronic Hazardous Waste Manifest Webcast',
                'description' => 'Go beyond the basics with this challenging course and delve into EPA\'s interpretations on the most daunting gray areas. Advanced Hazardous Waste Management is designed for those with prior RCRA training. This class covers much more than the regulatory requirements and meets your annual training requirement.
                                  You will learn which of your wastes are exempt or excluded that you may currently be managing as hazardous, which wastes do not count toward your generator status, how to handle episodic generation, requirements for importing and exporting hazardous waste, when permits are required and when treatment is allowed without a permit, how to minimize waste generation, and  the latest land disposal restrictions. Also covered are the most recent regulatory changes plus any pending EPA proposals that may affect your facility.',
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 hour',
                'course_type_id' => 2
            ]);
        };

        CourseCategory::create([
            'course_id' => $course6()->id,
            'category_id' => 1
        ]);

        $course8 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'DOT Hazardous Materials Training: The Complete Course',
                'description' => "<p><strong></strong>The Department of Transportation (49 CFR 172.704) makes training mandatory for almost anyone who handles hazardous materials, regardless of the amount. This affects employees who select or fill hazardous materials packages, label containers, complete shipping papers, load or unload vehicles, transload hazardous materials, or operate vehicles used in the transport of hazardous materials.&nbsp;<br><br>Receive your required training and learn how to comply with all regulations at this comprehensive seminar. DOT requires initial training and recurrent training every three years. This class meets both your initial and recurrent training requirements.&nbsp;&nbsp;</p><p>The course handbook, <em><a href='/products/view.aspx?id=1311' target='_blank'><em>Hazardous Materials Management: Compliance with the DOT Requirements</em></a></em><em>, </em>provides step-by-step instructions for shipping hazardous materials plus the latest regulations. The latest version of the DOT&nbsp;<em><a href='/products/view.aspx?id=1014' target='_blank'><em>Emergency Response Guidebook</em></a></em> is also included.<br><br></p><p>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul><li>CEU&#8212;0.8</li><li>CM Points IHMM&#8212;May be eligible for 8 CMP</li><li>NEHA&#8212;8 contact hours</li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;7.5 contact hours</span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 7 Continuing Education Hours</li><li>Kentucky Drinking Water - 7 Continuing Education Hours</li></ul><p><a href='/training/ceus.aspx'><span style='color: #810081; text-decoration: underline;'>Click here</span></u></a> for a complete list of continuing education&nbsp;credits and approval numbers.<br><b>&nbsp;</b></p>",
                'sku' => uniqid(),
                'price' => 449,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1
            ]);
        };

        CourseCategory::create([
            'course_id' => $course8()->id,
            'category_id' => 2
        ]);

        $course9 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'DOT Hazardous Materials Training: The Complete Course - Webcast',
                'description' => "<p><strong></strong>The Department of Transportation (49 CFR 172.704) makes training mandatory for almost anyone who handles hazardous materials, regardless of the amount. This affects employees who select or fill hazardous materials packages, label containers, complete shipping papers, load or unload vehicles, transload hazardous materials, or operate vehicles used in the transport of hazardous materials.&nbsp;<br><br>Receive your required training and learn how to comply with all regulations at this comprehensive webcast. DOT requires initial training and recurrent training every three years. This&nbsp;course meets both your initial and recurrent training requirements.&nbsp;&nbsp;</p>" .
                                 "<p><span color=#0000ff><span color=#333333>The course handbook, </span><span color=#0000ff><em>Hazardous Materials Management: Compliance with the DOT Requirements</em></span></span><em> </em>and the latest version of the DOT&nbsp;<u><span color=#0000ff><em>Emergency Response Guidebook</em></span></u><em>&nbsp;</em>are included as downloadable documents.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul><li>CEU&#8212;0.8</li><li>CM Points IHMM&#8212;May be eligible for 8 CMP</li>" .
                                 "<li>NEHA&#8212;8 contact hours</li><li><span class=Apple-style-span style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;7 contact hours</span></li>" .
                                 "<li><span class=Apple-style-span style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 7 Continuing Education Hours</li>" .
                                 "<li>Kentucky Drinking Water - 7 Continuing Education Hours<br></li></ul>" .
                                 "<p><a href=/training/ceus.aspx><u><span color=#810081>Click here</span></u></a> for a complete list of continuing education&nbsp;credits and approval numbers.</p>",
                'sku' => uniqid(),
                'price' => 429,
                'duration' => '8 hours',
                'course_type_id' => 2
            ]);
        };

        $course = $course9();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 2
        ]);

        $course->books()->save(Book::find(10));

        $course10 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'DOT Hazardous Materials Update - Webcast',
                'description' => "This live interactive webcast is the perfect way to meet your DOT 49 CFR 172.704(c)(2) recurrent training and testing requirements. You will learn how to classify, package, mark, label, document, and ship hazardous materials in accordance with DOT regulations. You will also learn how to comply with recent changes in the hazardous materials regulations that can impact your shipments, including revised hazard class definitions, new labeling requirements, and changes in the requirements for limited quantities and consumer commodities.<br><br>" .
                                 "<p><span color=#0000ff><span color=#333333>The course handbook, </span><span color=#0000ff><em><span>Hazardous Materials Management: Compliance with the DOT Requirements</span></em></font></font><em> </em>and the latest version of the DOT&nbsp;<span color=#0000ff><em>Emergency Response Guidebook</em></span><em>&nbsp;</em>are included as downloadable documents.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul><li>CEU&#8212;0.4</li><li>CM Points IHMM&#8212; May be eligible for 4 CMP</li>" .
                                 "<li><span class=Apple-style-span style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;3.75 contact hours<br></span></li>" .
                                 "<li><span class=Apple-style-span style='LINE-HEIGHT: 14px'>Kentucky Wastewater - 4 Continuing Education Hours<br></span></li>" .
                                 "<li><span class=Apple-style-span style='LINE-HEIGHT: 14px'>Kentucky Drinking Water - 4 Continuing Education Hours<br></span></li></ul>" .
                                 "<p><a href=/training/ceus.aspx><span style='color: #810081; text-decoration: underline;'>Click here</span></a> for a complete list of continuing education&nbsp;credits and approval numbers.</p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2
            ]);
        };

        $course = $course10();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 2
        ]);

        $course->books()->save(Book::find(10));

        $course11 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'DOT/IATA: Transportation of Hazardous Materials by Ground and Air',
                'description' => "The Department of Transportation (49 CFR 172.704) and the International Air Transport Association (IATA DGR 1.5) make training mandatory for almost anyone who handles hazardous materials or dangerous goods, regardless of the amount. Employees who select or fill packages, label containers, complete shipping papers, load or unload vehicles, transload hazardous materials, or operate vehicles used in the transport of hazardous materials or dangerous goods must be trained and tested. DOT requires training every three years for ground shipments of hazardous materials, and IATA requires training every two years for air shipments of dangerous goods.<div><br></div><div>Course Topics Include:</div><ul><li>How to classify and prepare hazardous materials/dangerous goods for shipment<!--/li--></li><li>Essential steps for compliance with DOT and IATA regulations</li><li>Excepted quantities and limited quantities</li><li>How to prepare shipping papers</li><li>How to select, fill, and seal packages</li><li>Package marks and labels</li><li>How to load, unload, and placard</li><li>Safety and security</li></ul>",
                'sku' => uniqid(),
                'price' => 549,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1
            ]);
        };

        $course = $course11();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 2
        ]);

        $course->books()->save(Book::find(11));

        $course12 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Environmental Audits',
                'description' => 'Anyone responsible for conducting audits or preparing facilities to be audited will benefit from this class. You will learn how to assess compliance with federal, state, and local requirements, how to organize the audit process and prepare an effective audit report, and what to do with the results.  This class also addresses the auditor\'s responsibilities, the facility\'s responsibilities, and how to monitor corrective action. ',
                'sku' => uniqid(),
                'price' => 499,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1
            ]);
        };

        CourseCategory::create([
            'course_id' => $course12()->id,
            'category_id' => 3
        ]);

        $course13 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Environmental Audits - Webcast',
                'description' => "Anyone responsible for conducting audits or preparing facilities to be audited will benefit from this class. You will learn how to assess compliance with federal, state, and local requirements, how to organize the audit process and prepare an effective audit report, and what to do with the results.&nbsp; This class also addresses the auditor's responsibilities, the facility's responsibilities, and how to monitor corrective action.&nbsp;<br><br>Included with the course is the <em><a href='' target='_blank'><em>Environmental Auditing and Compliance Manual</em></a></em>. This comprehensive handbook includes step-by-step procedures for environmental audits in accordance with EPA regulations, as well as plain English summaries of applicable regulations.&nbsp;<br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> if you would like to arrange for an onsite presentation of this material.<br>&nbsp;",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 hours',
                'course_type_id' => 2
            ]);
        };

        $course = $course13();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 3
        ]);

        $course->books()->save(Book::find(4));

        $course14 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Environmental Regulations - Webcast',
                'description' => "This five-hour course is designed to provide a broad understanding of major U.S. environmental laws and regulations. You will learn how to determine which laws and regulations apply to your facility and how to maintain compliance. Major emphasis is placed on the statutes that affect most facilities, including the Clean Air Act, Clean Water Act, RCRA, CERCLA, EPCRA, and TSCA. Recordkeeping, reporting, and training requirements are also covered.&nbsp;<br><br>" .
                                 "<p><span color=#0000ff><span color=#333333>The coursebook, </span><span color=#0000ff><em><em>Environmental, Health, and Safety Laws and Regulations&nbsp;</em></em></span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> if you would like to arrange for an onsite presentation of this course. Or, call 919-469-1585 x 224. " .
                                 "<div><span class=Apple-style-span style='LINE-HEIGHT: 14px'><br></span></div>" .
                                 "<div><p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.5</li>".
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 5.0 CMP</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hours Webcast',
                'course_type_id' => 2
            ]);
        };

        $course = $course14();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 3
        ]);

        $course->books()->save(Book::find(5));

        $course15 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Environmental, Health, and Safety Laws & Regulations',
                'description' => "Whatever your responsibilities within your organization, you need to know which laws and regulations apply to your facility in order to help remain in compliance. In this class you will learn how to determine which laws and regulations apply to your facility, procedures for attaining and maintaining compliance, training and recordkeeping requirements, and which of your activities require permits.&nbsp;<br><br>Environmental Resource Center's common sense approach to compliance with environmental, health, and safety laws and regulations and ISO 14000 standards will arm you with practical steps and procedures to assist you as you carry out your responsibilities.&nbsp;<br><br>The manual for the course, <em><a href='' target='_blank'><em>Environmental, Health, and Safety Laws and Regulations</em></a></em>, includes the scope and application of the laws and regulations, permitting, recordkeeping, monitoring and training requirements, and enforcement strategies. <br>&nbsp;" .
                                 "<div><br></div><div>Pre-approved for the following&nbsp;Continuing Education Credits:<br></div>" .
                                 "<div><ul style='PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 5px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>CEU&#8212;1.6</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>CM Points IHMM&#8212;May be eligible for 16 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>ABIH&#8212;2.67 Industrial Hygiene Points</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'><a style='FONT-FAMILY: verdana; COLOR: rgb(95,115,186); TEXT-DECORATION: none' href='/training/ceus.aspx'>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 749,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1
            ]);
        };

        $course = $course15();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 3
        ]);

        $course->books()->save(Book::find(5));

        $course17 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => '2015 Safety Summit: A System that Works',
                'description' => 'EnPro developed and implemented this program, taking its companies from good to best in class. The effective and comprehensive “System That Works” has been successfully implemented across the globe. EnPro will introduce you to its program that will improve management and employee engagement, performance metrics, employee recognition and incentive programs. We\'ll help you energize your current resources to generate new levels of awareness and passion for safety',
                'sku' => uniqid(),
                'price' => 1012.50,
                'duration' => '2 Days',
                'course_type_id' => 1
            ]);
        };

        CourseCategory::create([
            'course_id' => $course17()->id,
            'category_id' => 4
        ]);

        $course18 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Environmental, Health, and Safety Laws & Regulations',
                'description' => 'EnPro developed and implemented this program, taking its companies from good to best in class. The effective and comprehensive “System That Works” has been successfully implemented across the globe. EnPro will introduce you to its program that will improve management and employee engagement, performance metrics, employee recognition and incentive programs. We\'ll help you energize your current resources to generate new levels of awareness and passion for safety',
                'sku' => uniqid(),
                'price' => 749,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1
            ]);
        };

        CourseCategory::create([
            'course_id' => $course18()->id,
            'category_id' => 4
        ]);

        $course19 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Ergonomics - Webcast',
                'description' => "With over 1.8 million musculoskeletal disease (MSD) injuries per year, 34% of lost time injuries are due to ergonomic injuries. Moreover, carpal tunnel syndrome leads to more missed days than any other workplace injury. All MSDs are preventable. Learn the simple steps that you can take to prevent ergonomic injuries in any job.<br><br>To participate in this live training, please view the technical requirements <a href=/training/webcast-requirements.aspx>here</a>&nbsp;and&nbsp;have an active internet connection.&nbsp;&nbsp;<br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> to schedule this course for your facility.",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2
            ]);
        };

        CourseCategory::create([
            'course_id' => $course19()->id,
            'category_id' => 4
        ]);

        $course20 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Operations and Emergency Response (HAZWOPER) 8-Hour Refresher Training',
                'description' => "OSHA's &nbsp;Hazardous Waste Operations and Emergency Response rule at 29 CFR 1910.120 regulates cleanup sites, TSDs and emergency responders. Sections 1910.120 (e) and (p) require workers engaged in hazardous waste operations and workers at TSDs to receive at least eight hours of refresher training annually. Emergency responders must receive annual training of sufficient duration and content to maintain their competencies.&nbsp;<br><br>This eight-hour refresher course satisfies the annual training requirement and will provide you with the refresher you need to be fully ready to respond when needed. Course topics include how to assess the risk of chemical releases, employee safety, environmental monitoring with direct reading instruments, and incident control and management.&nbsp;<br><br>The <em>Hazardous Waste Operations and Emergency Response Refresher Training Manual</em> and <em>Emergency Response Guidebook</em> are included with the course.&nbsp;&nbsp;&nbsp;<br><br>" .
                                 "<div><p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 5px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.8</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 8.0 CMP</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'><a style='FONT-FAMILY: verdana; COLOR: rgb(95,115,186); TEXT-DECORATION: none' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p><b><br></b></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '1 Day',
                'course_type_id' => 1
            ]);
        };

        $course = $course20();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 4
        ]);

        $course->books()->save(Book::find(14));

        $course21 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Operations and Emergency Response 24-Hour Training (HAZWOPER)',
                'description' => "<div><p>Any employees that respond to releases or potential releases of hazardous substances for the purpose of stopping the release must be trained to the HAZWOPER technician level. They must receive at least 24 hours of training that covers key topics that will ensure that they respond to hazardous substance releases safely and properly.</p>" .
                                 "<p>Satisfy your training requirements with this comprehensive class that provides in-depth instruction on how to respond to emergencies in accordance with OSHA&#8217;s requirements. Topics include hazard recognition, risk assessment, use of field survey equipment, spill control and containment, PPE, emergency planning and organization, Incident Command System, decontamination, and incident termination procedures. You put your training to work during a hands-on simulated spill response. </p>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;2.4</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 24.0 CMP</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.</p></div>",
                'sku' => uniqid(),
                'price' => 549,
                'duration' => '3 Days',
                'course_type_id' => 1
            ]);
        };

        $course = $course21();

        CourseCategory::create([
            'course_id' => $course->id,
            'category_id' => 4
        ]);

        $course->books()->save(Book::find(14));

        $course22 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-IATA-BAT-4',
                'course_mill_session_id' => 120,
                'title' => 'How to Ship Batteries by Ground and Air - Online Training',
                'description' => "<p>If you ship batteries by ground or air, you must comply with very detailed DOT and IATA regulations regarding how batteries are classified, packaged, marked, labeled, transported, and identified on shipping papers. The rules apply not only to batteries, but also to equipment containing batteries and batteries packed with equipment. Virtually all types of batteries are regulated, including lithium, lead-acid, nickel cadmium, metal hydride, and alkaline. </p>
                    <p>According to 49 CFR 172.704, all personnel involved in the classification, packaging, marking, labeling, or shipment of fully regulated batteries must receive initial and recurrent transportation training. </p>
                    <p style='TEXT-AUTOSPACE: '>This convenient computer-based training gives you the skills you need to safely ship batteries by ground and air in accordance with DOT and IATA requirements and meets the DOT and IATA training requirements for personnel responsible for the shipment of batteries.</p>
                    <p style='TEXT-AUTOSPACE: '>You can complete the training anytime when it's convenient for you. Interactive games and quizzes make the training engaging, for maximum retention of information and skills.<br><br></p>
                    <p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482;&nbsp;that is connected to the Internet (broadband recommended)</li>
                    <li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174;&nbsp;Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li>
                    <li>Minimum 2GB RAM</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'><a href=https://get.adobe.com/flashplayer/>Adobe Flash</a>&nbsp;version 10 or higher</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => 'dot-bat',
                'price' => 199,
                'duration' => '4 hours (self paced)',
                'course_type_id' => 3
            ]);
        };

        CourseCategory::create([
            'course_id' => $course22()->id,
            'category_id' => 2
        ]);

        $course23 = function() {
            return Course::create([
                'course_mill_course_id' => 'IATA-DI-2',
                'course_mill_session_id' => 84,
                'title' => 'How to Ship Dry Ice by Ground and Air - Online Training',
                'description' => "<p style='TEXT-AUTOSPACE: '>To transport packages containing dry ice by ground or air, you must comply with stringent DOT and IATA requirements.</p><p>Environmental Resource Center&#8217;s computer-based training gives you the skills you need to ship dry ice by ground and air&#8212;and fulfills IATA&#8217;s initial and recurrent training requirements for shipments of dry ice.</p><p style='TEXT-AUTOSPACE: '>In this interactive course, you will learn:</p><ul><li>Hazards of dry ice and how to protect yourself from them</li><li>How the requirements for air and ground shipments differ</li><li>How to properly package, mark, label, and document shipments of dry ice</li></ul><p>You can complete the online training anytime or anywhere you have access to a computer. Interactive activities and quizzes make the training fun and engaging.<br><br>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482;&nbsp;that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174;&nbsp;Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li><li>Minimum 2GB RAM</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a>&nbsp;version 10 or higher</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)<br><br><br></li></ul><ul><img style='HEIGHT: 306px; BORDER-TOP-COLOR: ; WIDTH: 400px; BORDER-LEFT-COLOR: ; BORDER-BOTTOM-COLOR: ; BORDER-RIGHT-COLOR: ' alt= src=http://www.ercweb.com/images/dryicess.jpg></ul>",
                'sku' => 'iata-dry',
                'price' => 129,
                'duration' => 'Self Paced',
                'course_type_id' => 3
            ]);
        };

        CourseCategory::create([
            'course_id' => $course23()->id,
            'category_id' => 2
        ]);

        $course24 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-IATA-CCLQ-2',
                'course_mill_session_id' => 83,
                'title' => 'How to Ship Consumer Commodities and Limited Quantities by Ground and Air - Online Training',
                'description' => "<p style='TEXT-AUTOSPACE: '>Although consumer commodities and limited quantities qualify for exceptions to some of the regulations, the personnel that package and prepare these shipments are subject to initial and recurrent training requirements by the DOT and IATA.</p><p style='TEXT-AUTOSPACE: '>Learn how to prepare consumer commodities and limited quantities for ground and air transportation in this convenient computer-based session, which fulfills your DOT and IATA training requirements for these materials.</p><p style='TEXT-AUTOSPACE: '>In this interactive course, you will learn:</p><ul><li>Which consumer commodities are subject to the regulations</li><li>What exceptions are available and how they apply to your shipments</li><li>How to classify, package, mark, label, and document your shipments</li><li>How the phase-out of the ORM-D consumer commodity designation will impact your shipments</li></ul><p style='TEXT-AUTOSPACE: '>You can complete the training anytime when it's convenient for you. Interactive games and quizzes make the training engaging, for maximum retention of information and skills.<br><br></p><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482;&nbsp;that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174;&nbsp;Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li><li>Minimum 2GB RAM</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a>&nbsp;version 10 or higher</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => 'lq-cc',
                'price' => 149,
                'duration' => 'Self Paced',
                'course_type_id' => 3
            ]);
        };

        CourseCategory::create([
            'course_id' => $course24()->id,
            'category_id' => 2
        ]);

        $course25 = function() {
            return Course::create([
                'course_mill_course_id' => 'RCRA-UO-2',
                'course_mill_session_id' => 89,
                'title' => 'How to Manage Used Oil - Online Training',
                'description' => "<p>Although subject to the Resource Conservation and Recovery Act, used oil has somewhat relaxed requirements.  In this convenient online course, you will learn how to manage used oil properly and learn how it can become fully regulated hazardous waste if it is mishandled.  The course handout, Used Oil Management, is included as a downloadable document.</p><p>You can complete the training anytime when it's convenient for you.  Interactive activities help you learn and retain the information that you need to know.</p><p><br>Technical Requirements:</p><ul><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174; Internet Explorer, or Google Chrome&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul></ul>",
                'sku' => '4ae23sb',
                'price' => 99,
                'duration' => 'Self Paced',
                'course_type_id' => 3
            ]);
        };

        CourseCategory::create([
            'course_id' => $course25()->id,
            'category_id' => 1
        ]);

        $course0 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management in California',
                'description' => "If your facility generates hazardous waste, you must comply with detailed regulatory requirements for waste characterization, container marking and labeling, waste minimization, manifesting, transportation, emergency response, and land disposal certification. EPA and state regulations require annual hazardous waste management training. At this two-day class you will learn the latest requirements of the law and how to comply.&nbsp;&nbsp;<br><br>This course will teach you how to properly identify, accumulate, minimize, and ship hazardous waste. You will learn how to develop a contingency plan and how to comply with land disposal restrictions. The course provides an in-depth look at the latest regulatory requirements, waste exemptions and exclusions, new universal waste requirements, VOC standards for hazardous waste generators, and waste minimization.&nbsp;<br><br><br><p>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul><li>CEU&#8212;1.6</li><li>CM Points IHMM&#8212;May be eligible for 16 CMP</li><li>IDEM Drinking Water&#8212;16 contact hours</li><li>IDEM Waste Water&#8212;16 general contact hours</li><li>NEHA&#8212;16 contact hours</li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;15 contact hours</span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 14 Continuing Education Hours</li><li>Kentucky Drinking Water - 14 Continuing Education Hours</li></ul><p><a href=''/training/ceus.aspx'>Click here</a> for a complete list of continuing education&nbsp;credits and approval numbers.<br>&nbsp;</p>",
                'sku' => uniqid(),
                'price' => 795,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course0()->id,
            'category_id' => 1
        ]);

        $course26 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management in Texas',
                'description' => "If your facility generates hazardous waste in the state of Texas, you must comply not only with the Texas hazardous and industrial waste regulations, but also with federal regulations that have not yet been incorporated into the Texas rules.&nbsp;&nbsp;<br><br>This two-day seminar meets the Texas <em>and </em>federal annual training requirements for generators of hazardous waste in Texas. Topics covered include waste characterization, on-site management, manifesting, land disposal restrictions, recordkeeping and reporting, source reduction, and universal waste. Information on the modifications and new rules that may affect your facility's hazardous waste management program upon promulgation is also included.&nbsp;<br><br>The&nbsp;<em><a href='/products/view.aspx?id=1341' target='_blank'><em>Handbook for the Management of Hazardous Waste in Texas</em></a></em><em> </em>includes step-by-step instructions for hazardous waste management. It also includes a section specifically dedicated to identifying and discussing federal regulations that generators in Texas must comply with, even though they are not yet incorporated into the Texas rules.&nbsp;<br><br>" .
                                 "<p>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul><li>CEU&#8212;1.6</li><li>CM Points IHMM&#8212;May be eligible for 16 CMP</li>" .
                                 "<li>IDEM Drinking Water&#8212;16 contact hours</li><li>IDEM Waste Water&#8212;16 general contact hours</li>" .
                                 "<li>NEHA&#8212;16 contact hours</li></ul><p><a href='/training/ceus.aspx'><span style='color: #810081; text-decoration: underline;'>Click here</span></a> for a complete list of continuing education&nbsp;credits and approval numbers.<br><br>&nbsp;</p>",
                'sku' => uniqid(),
                'price' => 795,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course26()->id,
            'category_id' => 1
        ]);

        $course27 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Transportation of Infectious Substances by Ground and Air - Webcast',
                'description' => "<p style='MARGIN: 3.4pt 0in'>This course will guide you through the unique requirements for the transportation of infectious substances.&nbsp; You will learn how to classify infectious substances; how to select and prepare triple-packaging systems; how to mark, label, and document infectious substances for ground and air; and how to comply with the regulations concerning shipments of infectious substances under the<i> IATA Dangerous Goods Regulations</i>.<!--?xml:namespace prefix = 'o' ns = 'urn:schemas-microsoft-com:office:office' /--><?xml:namespace prefix = 'o' /><o:p></o:p></p>" .
                                 "<p>The Guide to Shipping Infectious Substances is included as downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>.&nbsp;<br>&nbsp;&nbsp;<br>Please contact <a href='mailto:aknight@ercweb.com'>Amy Knight</a> if you would like to arrange for an onsite presentation of this material.&nbsp; </p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course27()->id,
            'category_id' => 2
        ]);

        $course28 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Transportation of Dangerous Goods: Compliance with IATA and IMO Regulations',
                'description' => "This course provides an in-depth look at the regulations governing shipments by sea and air under IMO (International Maritime Organization) and IATA (International Air Transport Association) regulations. It focuses on the differences between domestic and international regulations and covers any regulatory changes or additions that have occurred in the past two years.&nbsp;<br><br>During this class you will participate in hands-on exercises that will give you the opportunity to practice preparing goods for shipment using the IMDG (International Maritime Dangerous Goods) Code and IATA Dangerous Goods Regulations. These practical exercises demonstrate how to classify a material as a dangerous good; how to interpret information from the dangerous goods lists; packaging, marking, and labeling; and shipping paper requirements for shipping dangerous goods internationally.&nbsp;&nbsp;&nbsp;&nbsp;<br><br>IATA requires initial training and recurrent training every two years. Per competent authority reference, IMO requires initial training and recurrent training every three years in the U.S. This course meets both initial and recurrent training requirements.&nbsp;<br><br>The course handbook, <em><a href=/products/view.aspx?id=1312 target=_blank><em>Transportation of Dangerous Goods: Compliance with IATA and IMO Regulations</em></a></em><em>,</em> is a comprehensive reference and guide that includes excerpts to help you use the IMDG Code and the IATA Dangerous Goods Regulations<em>. </em>The <em>IATA Dangerous Goods Regulations, </em>57<sup>th </sup><em>Edition</em> is also included with your course materials. The IMDG Code is available for purchase.&nbsp; " .
                                 "<div><br></div><div><span class=Apple-style-span style='LINE-HEIGHT: 14px'>Pre-approved for the following&nbsp;Continuing Education Credits:<br></span>" .
                                 "<div><br></div><div><span class=Apple-style-span style='LINE-HEIGHT: 14px'>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'> " .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px'>CEU&#8212;0.8</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px'>CM Points IHMM&#8212;May be eligible for 8.0 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px'>NEHA&#8212;8.0 contact hours</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></span><b>&nbsp;</b></div></div>" ,
                'sku' => uniqid(),
                'price' => 579,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course28()->id,
            'category_id' => 2
        ]);

        $course29 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'IATA Dangerous Goods Update - Webcast',
                'description' => "<div>This session will prepare you to ship dangerous goods by air in accordance with the latest International Air Transport Association's Dangerous Goods Regulations. Topics include how to determine which materials are regulated, how to package, mark, and label dangerous goods for shipment by air. &nbsp;You&#8217;ll also learn the special requirements for shipments of consumer commodities, limited quantities, and excepted quantities.&nbsp;</div>" .
                                 "<div><br></div><div>IATA requires initial training and recurrent training every two years. This live interactive webcast meets both of these requirements.</div><br>" .
                                 "<p><span color=#0000ff><span color=#333333><em>Transportation of Dangerous Goods (IATA)&nbsp;</em>is included as a downloadable document</span></span>.<br><br>The 57<sup>th<em> </em></sup>edition of the <em>IATA Dangerous Goods Regulations </em>is included with your course materials (international shipments will be subject to shipping charges).<br><br></p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 529,
                'duration' => '5 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course29()->id,
            'category_id' => 2
        ]);

        $course30 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'International Maritime Dangerous Goods (IMDG) Code - Webcast',
                'description' => "<p>Both the DOT and the International Maritime Organization require initial training and recurrent dangerous goods training every three years. In this course, you can meet your initial and recurrent training requirements and find out how the latest changes to the IMDG code can impact your dangerous goods shipments.</p>".
                                 "<p>Learn how to classify, package, mark, label, and placard dangerous goods for shipment by vessel. You will also learn how to identify marine pollutants and prepare transport documents.</p>" .
                                 "<p>The course manual, <em>Transportation of Dangerous Goods</em>, is included as a downloadable document. The IMDG Code is available for purchase.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Pre-approved for the following&nbsp;Continuing Education Credits: </p>" .
                                 "<div><ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.5</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 5.0 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>ABIH&#8212;0.84 Industrial Hygiene Points</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course30()->id,
            'category_id' => 2
        ]);

        $course31 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Transport Dangerous Goods by Road within Europe - Webcast',
                'description' => "If you ship dangerous goods within Europe by ground you must comply with ADR (Accord European Relatif au Transport International des Marchandises Dangereuses par Route) requirements for those shipments.&nbsp;<br><br>At this 4-hour webcast you will learn how to use the 2-volume ADR European Agreement Concerning the International Transport of Dangerous Goods by Road; how to classify, describe, package, mark, label, placard, and document shipments of dangerous goods by ground within Europe; and the differences between the ADR regulations and the DOT, IATA, and IMO regulations.&nbsp;<br><br>" .
                                 "<p><span color=#0000ff><span color=#333333>The course handbook, <i><i>How to Transport Dangerous Goods by Road within Europe</i>&nbsp;</i></span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> if you would like to arrange for an onsite presentation of this material.&nbsp;",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4-Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course31()->id,
            'category_id' => 2
        ]);

        $course32 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Storm Water Management: How to Comply with State and Federal Regulations',
                'description' => "EPA's Multi-Sector General Permit requires training for all employees who work in areas where industrial materials or construction activities are exposed to storm water, and for all employees responsible for implementing activities in your storm water pollution prevention plan.&nbsp;<br><br>Learn how to comply with the latest storm water management regulations at this comprehensive one-day class. You will learn about the various permit options available, how to develop required storm water pollution prevention plans, which best management practices are right for your facility or site, and methods for collecting and handling samples. Both industrial and construction storm water management requirements are covered, as well as state-specific requirements.&nbsp;<br><br>The course handbook, <em><a href=/products/view.aspx?id=1324 target=_blank><em>Handbook for the Management of Storm Water Discharges</em></a></em><em>, </em>includes step-by-step procedures, inspection checklists, and copies of the applicable regulations. Also included with your materials is an overview of your state's storm water permitting program.&nbsp;<br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight </a>if you would like to arrange for an onsite presentation of this material.&nbsp;" .
                                 "<div><span class='Apple-style-span' style='LINE-HEIGHT: 14px'><br></span></div>" .
                                 "<div><p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 5px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>CEU&#8212;0.5</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>CM Points IHMM&#8212;May be eligible for 8.0 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>IDEM Drinking Water&#8212;8.0 contact hours</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>IDEM Waste Water&#8212;8.0 general contact hours</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours&#8212;7.5 contact hours<br></span></li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'><a style='FONT-FAMILY: verdana; COLOR: rgb(95,115,186); TEXT-DECORATION: none' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 499,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course32()->id,
            'category_id' => 3
        ]);

        $course33 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Storm Water Regulations - Webcast',
                'description' => "Learn how to comply with the industrial and construction storm water management regulations at this five-hour webcast. In addition to receiving an overview of storm water regulations, you will learn how to obtain a permit for storm water discharges, which permits are available to you, how to develop an effective SWPPP for your facility, and how to develop and implement an effective sampling plan.&nbsp;<br><br>The&nbsp;<i>Handbook for the Management of Storm Water Discharges</i>&nbsp;<em>is&nbsp;included</em> as a downloadable document.<br><br>" .
                    "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>.</p>" .
                    "<div><p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                    "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                    "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.5</li>" .
                    "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 5.0 CMP</li>" .
                    "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>IDEM Drinking Water&#8212;5.0 contact hours</li>" .
                    "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'><span class=Apple-style-span style='LINE-HEIGHT: 14px'>IDEM Waste Water&#8212;5 general contact hours<br></span></li></ul>" .
                    "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course33()->id,
            'category_id' => 3
        ]);

        $course34 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'SARA Title III (EPCRA) Workshop',
                'description' => "SARA Title III (the Emergency Planning and Community Right-To-Know Act) requires you to inform your surrounding community about your facility's hazardous substances and releases - accidental and permitted. This program provides you with a complete understanding of what this law requires as well as detailed instructions on how to ensure your facility is in compliance. A clear explanation of your reporting requirements under Title III is provided so you will understand when and how to complete the Tier II, and Toxic Chemical Release (Form R) inventory report forms.&nbsp;<br><br><em><em><a href=/products/view.aspx?id=1315 target=_blank><em>How to Comply with the Emergency Planning and Community Right-To-Know Act</em></a></em>, </em>the comprehensive course handbook, provides step-by-step instructions for compliance with all EPCRA requirements, including TPQ notification, MSDS submission, Tier II reports, Form R, Form A, and emergency notification requirements.&nbsp;<br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight </a>if you would like to arrange for an onsite presentation of this material.&nbsp; <div><span class='Apple-style-span' style='LINE-HEIGHT: 14px'><br></span></div><div><p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul style='PADDING-BOTTOM: 0px; LIST-STYLE-TYPE: none; MARGIN: 5px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'><li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px'>CM Points IHMM&#8212;May be eligible for 8.0 CMP</li></ul><p style='PADDING-BOTTOM: 9px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px'><a style='FONT-FAMILY: verdana; COLOR: rgb(95,115,186); TEXT-DECORATION: none' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></div>",
                'sku' => uniqid(),
                'price' => 449,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course34()->id,
            'category_id' => 3
        ]);

        $course35 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'SARA Title III - How to Comply with Emergency Release & Notification Requirements - Webcast',
                'description' => "Learn what is covered by the Emergency Planning and Community Right-to-Know Act at this four-hour webcast. Topics include how to identify and accurately report OSHA hazardous chemicals, CERCLA hazardous substances, and extremely hazardous substances at your facility. You will also learn how to determine whether the Tier II inventory reporting requirements apply to your site and how to complete the form. To help your reporting go smoothly, you will learn how to calculate inventory amounts of liquids, gases, and mixtures using the latest guidance from EPA.&nbsp;<br><br>The&nbsp;<i>How to Comply with Emergency Planning and Community Right-to-Know Act</i>&nbsp;<em>is&nbsp;included</em> as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Please contact <a href=mailto:aknight@ercweb.com><span style='color: #800080; text-decoration: underline;'>Amy Knight</span></a>&nbsp;if you&nbsp;would like to arrange for&nbsp;an onsite presentation of this material prior to the next class.</p>" .
                                 "<div><span class=Apple-style-span style='LINE-HEIGHT: 14px'>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 4.0 CMP</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p></span><br></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course35()->id,
            'category_id' => 3
        ]);

        $course36 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'SARA Title III - Form R (EPCRA Section 313) - Webcast',
                'description' => "At this webcast you will learn the latest changes to the Section 313 requirements, including the threshold requirements for PBT chemicals and lead. You will also learn how to determine if you must complete the Form R and whether you qualify for any Section 313 exemptions, how to complete and document your 313 threshold determination, and how to identify release sources at your facility.&nbsp;<br><br>Learn how to complete your Form R report and avoid common errors.&nbsp;<br><br>The&nbsp;<i>How to Comply with Emergency Planning and Community Right-to-Know Act</i>&nbsp;handbook&nbsp;is&nbsp;included as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> if you would like to arrange for onsite&nbsp;training, or call 919-469-1585. </p>" .
                                 "<div><br></div><div><span class=Apple-style-span style='LINE-HEIGHT: 14px'>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'></ul></span></div>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course36()->id,
            'category_id' => 3
        ]);

        $course37 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'OSHA 10-Hour Compliance Course',
                'description' => "<p>Learn how to assure the health and safety of your employees while complying with OSHA regulations. In this session, you will learn how determine which rules apply to your operations, what steps must be taken to comply, as well as what documentation and training are required to comply with each standard. The training focuses on OSHA&#8217;s general industry safety and health standards, including an introduction to OSHA, walking and working surfaces, emergency action and fire prevention plans, electrical, personal protective equipment, hazard communication, hazardous materials, materials handling, machine guarding, industrial hygiene, bloodborne pathogens, ergonomics, safety and health programs, and fall protection. Our instructors are OSHA-authorized, and students receive OSHA course completion cards. </p>" .
                                 "<p>Although OSHA requires only 10 hours of training to receive your certification, Environmental Resource Center&#8217;s 10-hour course is actually 14 hours in length. This allows additional time for exercises and in-depth training on topics that are not covered in shorter courses. <br><br><i><i>Environmental Resource Center's comprehensive <a href=/products/view.aspx?id=1013 target=_blank>Occupational Safety and Health Standards for General Industry Handbook</a> i</i></i>s included with the class.<br></p>" .
                                 "<div><br><br style='LINE-HEIGHT: 9px'></div>",
                'sku' => uniqid(),
                'price' => 499,
                'duration' => '2 Days; Lunch is provided both days',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course37()->id,
            'category_id' => 4
        ]);

        $course38 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazmat Security - Webcast',
                'description' => "DOT has established stringent hazmat security requirements to enhance the security of hazardous materials transportation. This requires most hazmat employers to provide security training to their employees as part of their regular hazmat training.&nbsp;<br><br>At this two-hour webcast you will learn how to recognize the hazardous material security vulnerabilities at your site and how to develop and implement an effective security plan. It is designed to meet your mandatory hazmat security awareness training requirements.&nbsp;<br><br>The course handbook, <em>How to Develop Hazmat Security Plans</em>, is included as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Please contact Amy Knight&nbsp;at <a href=mailto:aaknight@ercweb.com>aknight@ercweb.com</a> if you would like to arrange for an onsite&nbsp;presentation of this material.</p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course38()->id,
            'category_id' => 2
        ]);

        $course39 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Manifesting - Webcast',
                'description' => "When you sign hazardous waste manifests, you make a declaration that the shipment has been offered correctly.&nbsp;Avoid costly mistakes by learning the proper way to prepare and maintain your manifest, and prevent the most common problem: an inaccurate DOT basic description.&nbsp;&nbsp;<br><br>During this webcast you will learn which manifest to use for hazardous waste, universal waste, used oil, precious metals, and recyclable wastes. You will also learn how to determine the DOT basic description, exception reports, discrepancy reports, and emergency response information.&nbsp;In addition, special designations for hazardous substances (RQs), generic shipping names, marine pollutants, and inhalation hazards will be discussed.&nbsp;<br><br>The course handbook, <em>Hazardous Waste Manifesting</em>, is included as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course39()->id,
            'category_id' => 1
        ]);

        $course40 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Land Disposal Restrictions - Webcast',
                'description' => "<p>Unless you've studied the complex land disposal regulations in depth, there is a high probability that you might not be making the proper land disposal notice or certification.&nbsp;Even if you completely rely on your TSDF to complete your notifications/certifications, it is still your responsibility.&nbsp; Errors made result in RCRA penalties that are assessed to the generator.&nbsp;</p>" .
                                 "<p>This webcast reviews the intricate LDR regulations in an easy-to-understand format.&nbsp;&nbsp; Our step by step procedures help you determine to whom and what the land disposal restriction regulations apply; which treatment, storage, disposal, and dilution prohibitions apply to your waste; wastewater and nonwastewater treatment standards outlined in the regulations for your waste; soil and debris alternative standards; and recordkeeping and documentation requirements.&nbsp;</p>" .
                                 "<p><span color=#0000ff><span color=#333333>The course handbook, <em>Land Disposal Restrictions</em>, </span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course40()->id,
            'category_id' => 1
        ]);

        $course41 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Universal Waste - Webcast',
                'description' => "Does your facility generate any waste batteries, light bulbs, mercury-containing equipment, or pesticides? All personnel who handle these wastes must be trained to manage them in compliance with the regulations and know what to do in an emergency. Moreover, many states classify additional wastes as universal wastes, such as paint, electronic waste, antifreeze, aerosol cans, CRTs, etc.&nbsp;<br><br>In this two-hour webcast you will learn how to classify, store, and prepare universal waste for off-site shipment and how to respond to releases. You will also learn about the new EPA universal waste regulations for mercury-containing devices.&nbsp;<br><br>The course handbook, <em>Universal Waste Management</em>, is included as a downloadable document.<br>" .
                                 "<p><br></p><p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>" .
                                 "<p>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight </a>if you would like to arrange for an onsite presentation of this material.</p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course41()->id,
            'category_id' => 1
        ]);

        $course42 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Used Oil and Special Waste - Webcast',
                'description' => "Although they are subject to the Resource Conservation and Recovery Act, used oil, precious metals, lead acid batteries, and hazardous waste fuels have somewhat relaxed requirements. Learn how to manage these wastes properly and understand how they can become fully regulated hazardous wastes if they are mishandled.&nbsp;<br><br>The course handbook, <em>Used Oil and Special Waste</em>, is included as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> if you would like to arrange for an onsite presentation of this course. Or, call 919-469-1585 x 224.</p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course42()->id,
            'category_id' => 1
        ]);

        $course43 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Ship Consumer Commodities and Limited Quantities - Webcast',
                'description' => "Consumer commodities and limited quantities are subject to somewhat less stringent regulations than other hazardous materials. However, this does not mean that DOT doesn't regulate them at all. And, when you ship them by air, these materials are subject to more stringent regulations established by IATA and ICAO. <br><br>Any personnel at your site that perform pre-transportation functions (such as filling packages, or placing marks or labels on packages), or transportation functions (such as loading or unloading) are required to be trained in the DOT regulations that apply to their job functions.<br><br>In this webcast, you will learn how to prepare consumer commodities and limited quantities for ground and air transportation. You&#8217;ll learn how the phase-out of the ORM-D consumer commodity designation will impact your shipments, and how to take advantage of other exceptions that can save you time and money, including those for materials of trade, samples, small quantities, and excepted quantities. <br><br>The handbook is included as a downloadable document.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><br>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course43()->id,
            'category_id' => 2
        ]);

        $course44 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Operations and Emergency Response 40-Hour Training (HAZWOPER)',
                'description' => "<p>Personnel who are involved in cleanups at waste sites-including Superfund sites, RCRA corrective action sites, or even voluntary cleanups involving hazardous substances must receive 40 hours of initial classroom instruction. </p>" .
                           "<p>Topics in this course include site characterization, safety and health program requirements, waste handling, handling drums and containers, decontamination, hazardous substance and radiological terms and behavior, work environment hazards, and confined space entry. </p>" .
                           "<p>You will have the opportunity to apply your training during a hands-on simulated incident response.</p>" .
                           "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                           "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                           "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;4.0</li>" .
                           "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 40.0 CMP</li></ul>" .
                           "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.</p><div></div>",
                'sku' => uniqid(),
                'price' => 749,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course44()->id,
            'category_id' => 4
        ]);

        $course45 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'HAZWOPER 8 Hour Refresher - Webcast',
                'description' => "OSHA's&nbsp; Hazardous Waste Operations and Emergency Response rule at 29 CFR 1910.120 regulates cleanup sites, TSDs, and emergency responders. Sections 1910.120 (e) and (p) require workers engaged in hazardous waste operations and workers at TSDs to receive at least eight hours of refresher training annually. Emergency responders must receive annual training of sufficient duration and content to maintain their competencies.&nbsp;<br><br>This eight-hour webcast is presented in two sessions, four hours each, over two consecutive days. Course topics include how to assess the risk of chemical releases, employee safety, environmental monitoring with direct reading instruments, and incident control and management. This webcast meets your refresher training requirements.&nbsp;<br><br>".
                                 "<p><span color=#0000ff><span color=#333333>The <em>Hazardous Waste Operations and Emergency Response Refresher Training Manual</em> and <em>Emergency Response Guidebook </em></span></span>are included as downloadable documents.</p>".
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '8 Hour Webcast (2 4-hour sessions)',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course45()->id,
            'category_id' => 4
        ]);

        $course46 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Incident Command System - Webcast',
                'description' => "The Incident Command System (ICS) is the model for command, control, and coordination (C3) of an emergency response and provides a way to coordinate efforts of individual agencies as they work to stabilize an incident and protect life, property, and the environment.&nbsp;<br><br>OSHA 29 CFR 1910.120; EPA 311; NFPA standards 471, 472, and 473; and DOT/Federal Emergency Management Agency (FEMA) Guidelines for Public Sector Hazardous Materials Training refer to the need for ICS training.&nbsp;<br><br>In this five-hour webcast you will learn the procedures for a safe and effective emergency response. The course covers ICS organization, ICS assignments, incident resource management, and HAZWOPER Incident Command.&nbsp;<br><br>A downloadable copy of<em> The Incident Command System</em> is included with the course.<br><br>" .
                                 "<p>If you would like to schedule an Incident Command System webcast or on-site, please contact Amy Knight at 919-469-1585, Ext. 224, or <a href=mailto:aknight@ercweb.com>aknight@ercweb.com</a>.<br><br>To participate in this live training, please view the technical requirements <a href=/training/webcast-requirements.aspx>here</a>&nbsp;and&nbsp;have an active internet connection.&nbsp;&nbsp;<br></p><strong>&nbsp;</strong>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course46()->id,
            'category_id' => 4
        ]);

        $course47 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'OSHA GHS Hazard Communication Standard: Training for Trainers',
                'description' => "With OSHA&#8217;s adoption of the Globally Harmonized System (GHS) for the classification and labeling of hazardous chemicals, virtually every chemical label, MSDS&#8212;now called Safety Data Sheet (SDS), and written hazard communication plan must be revised to meet the new standard. Worker hazcom training must be updated so that workers can recognize and understand the new labels, pictograms and new hazard statements and precautions on SDSs.",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '1 Day; Lunch is provided',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course47()->id,
            'category_id' => 4
        ]);

        $course48 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'OSHA Hazard Communication Standard - Webcast',
                'description' => "At this four-hour course you will learn OSHA's requirements for hazard determination, material safety data sheets, and labels. The written hazard communication program, employee information, and recordkeeping requirements are also covered.&nbsp;<br><br>" .
                                 "<p><span color=#0000ff><span color=#333333>The <em><em>How to Comply with the OSHA Hazard Communication Standard</em>&nbsp;is&nbsp;</em></span></span><em>included</em> as a downloadable document.<br><br></p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Please contact <a href=mailto:aknight@ercweb.com>Amy Knight</a> to schedule the&nbsp;webcast for your facility.</p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course48()->id,
            'category_id' => 4
        ]);

        $course49 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'REACH, WEEE, and RoHS: How European Environmental Initiatives Impact U.S. Companies - Webcast',
                'description' => "U.S. companies that export products to Europe are finding that they must comply with a myriad of regulations that are significantly different and more stringent than U.S. regulations. Worldwide, many countries, as well as California, have adopted regulations that parallel the European regulations. Learn about the major European environmental regulations, how they impact your business, and steps to take to ensure compliance.<br><br>" .
                                 "<p><span color=#0000ff><span color=#333333>The handbook </span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course49()->id,
            'category_id' => 3
        ]);

        $course50 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'TSCA Import Requirements - Webcast',
                'description' => "<!--StartFragment--> <p class=MsoNormal>EPA requires importers to certify that their importedchemical substances or mixtures are either in compliance with TSCA Sections 5,6, and 7 at the time of import or that they are not subject to TSCA. <span style='mso-spacerun: yes'>&nbsp;</span>Section 13 of TSCA requires that chemicals,mixtures, and articles be refused entry into the United States if it fails tocomply with any TSCA rule in effect or is offered for entry in violation ofTSCA Section 5, 6, or 7.</p>" .
                                 "<p class=MsoNormal>In this session, you will learn how to determine if your imports are subject to TSCA, how to make positive and negative certifications, and when no certifications are required.</p>" .
                                 "<p><span color=#0000ff><span color=#333333>The course hand book </span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>".
                                 "<p><br></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course50()->id,
            'category_id' => 3
        ]);

        $course51 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'TSCA Pre-Manufacture Notices and Updates - Webcast',
                'description' => "<p>Under EPA&#8217;s New Chemicals Program, anyone who plans to manufacture or import a new chemical substance for a non-exempt commercial purpose is required to provide the EPA with a Pre-Manufacture Notice (PMN). </p>" .
                                 "<p>EPA&#8217;s Inventory Report Rule requires chemical manufacturers and importers to update TSCA inventory data with the EPA every five years using a Form U. The report must include the identity of, and certain manufacturing information for, organic chemical substances manufactured or imported annually in quantities of 25,000 pounds or more. <br><br>In this webcast, you&#8217;ll learn how to identify which of your substances are subject to and which are exempt from PMNs, how to develop successful PMNs, and how to complete the Form U.<br><br></p>" .
                                 "<p><span color=#0000ff><span color=#333333>The coursebook </span></span>is included as a downloadable document.</p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>" .
                                 "<p><i>There are no courses scheduled this year. &nbsp;If you have several students that could benefit from this course, contact Amy Knight at aknight@ercweb.com to obtain a quote for a session tailored for your needs.</i></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course51()->id,
            'category_id' => 3
        ]);

        $course53 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'OSHA Safety Regulations - Webcast',
                'description' => "This five-hour course is designed to give you an overview of the critical OSHA general industry regulations and help you determine which standards apply to your facility. Course topics include hazard communication, PPE, confined space, process safety management, and HAZWOPER. Recordkeeping, reporting, and training requirements are also covered.&nbsp;<br><br>The coursebook,&nbsp;<em><a href=/products/view.aspx?id=1317 target=_blank><em><span style='color: #0000ff; text-decoration: none;'>Environmental, Health, and Safety Laws and Regulations</span></em></a></em> is included as a downloadable document.&nbsp;<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. <br><br>Pre-approved for the following&nbsp;Continuing Education Credits:<br></p>" .
                                 "<p></p><ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.5</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;May be eligible for 5.0 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>ABIH&#8212;.84 Safety Points</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'>&nbsp;</p><p></p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '5 Hours Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course53()->id,
            'category_id' => 4
        ]);

        $course55 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Transportation of Dangerous Goods: Compliance with IATA Regulations - Webcast',
                'description' => "This course will provide you with an in-depth look and practical experience in the domestic and international regulations governing the transportation of dangerous goods (hazardous materials) by air. You will learn how to determine which materials are classified as dangerous goods, how to select and prepare packages, how to mark and label packages, as well as how to prepare consumer commodities, excepted quantities, and limited quantities for transport by air. You will also learn how to prepare the IATA Shipper's Declaration for Dangerous Goods and air waybill. This course gives you the tools you need to comply with the international regulations within the guidelines of the DOT Hazardous Materials Regulations and how to avoid rejected shipments.&nbsp; This course fulfills both your initial and recurrent training requirements.&nbsp;" .
                                 "<div><br></div><div>A copy of the 57<sup>th</sup>&nbsp;<i>IATA Dangerous Goods Regulations </i>(international shipments will be subject to shipping charges)&nbsp;and a downloadable copy of Environmental Resource Center's <i>Transportation of Dangerous Goods</i> handbook are included with your course materials. " .
                                 "<div><span class=Apple-style-span style='LINE-HEIGHT: 14px'><br></span></div><div>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'>Pre-approved for the following&nbsp;Continuing Education Credits:</p>" .
                                 "<ul style='LIST-STYLE-TYPE: none; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 5px; PADDING-RIGHT: 0px'>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CEU&#8212;0.5</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>CM Points IHMM&#8212;5.0 CMP</li>" .
                                 "<li style='BACKGROUND-IMAGE: url(http://www.ercweb.com/images/bullet_raquo.gif); BACKGROUND-REPEAT: no-repeat; BACKGROUND-POSITION: 0px 3px; PADDING-BOTTOM: 3px; PADDING-LEFT: 13px; -webkit-background-clip: initial; -webkit-background-origin: initial'>ABIH&#8212;0.84 Industrial Hygiene Points</li></ul>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><a style='TEXT-DECORATION: none; FONT-FAMILY: verdana; COLOR: rgb(95,115,186)' href=/training/ceus.aspx>Click here</a>&nbsp;for a complete list of continuing education&nbsp;credits and approval numbers.<br style='LINE-HEIGHT: 9px'><br></p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>" .
                                 "<p style='PADDING-BOTTOM: 9px; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; PADDING-RIGHT: 0px'><br>&nbsp;</p></div></div>",
                'sku' => uniqid(),
                'price' => 559,
                'duration' => '8 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course55()->id,
            'category_id' => 2
        ]);

        $course56 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Ship Batteries by Ground and Air - Webcast',
                'description' => "<p>If you ship batteries by ground or air, you must comply with very detailed DOT and IATA regulations regarding how batteries are classified, packaged, marked, labeled, transported, and identified on shipping papers. The rules apply not only to batteries, but also to equipment containing batteries and batteries packed with equipment. Virtually all types of batteries are regulated, including lithium, lead-acid, nickel cadmium, metal hydride, and alkaline. </p>" .
                                 "<p>According to 49 CFR 172.704, all personnel involved in the classification, packaging, marking, labeling, or shipment of fully regulated batteries must receive initial and recurrent transportation training. This live, interactive webcast will satisfy your mandatory general awareness, function-specific, safety, and security training requirements for battery shipments. </p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course56()->id,
            'category_id' => 2
        ]);

        $course57 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Implement OSHA\'s Globally Harmonized Hazard Communication Standard (GHS) - Webcast',
                'description' => "OSHA published a final rule that will require US employers to begin adoption of the globally harmonized system for the classification and labeling of hazardous chemicals. This means that virtually every chemical label, MSDS (soon to be called safety data sheet) and written hazard communication plan must be revised to meet the new standard. Worker training must be updated so that workers can recognize and understand the symbols and pictograms on the new labels as well as the new hazard statements and precautions on SDSs.Theses dramatic changes will also impact other OSHA standards such as Flammable and Combustible Liquids, Process Safety Management, Hazardous Waste Operations and Emergency Response (Hazwoper), Fire Prevention and Protection, Occupational Exposure to Hazardous Chemicals in Laboratories, and many of the chemical-specific OSHA standards such as the Lead Standard.In this webcast, you will learn the GHS standards that OSHA has adopted, how the new standards differ from former Hazcom requirements, how to implement the changes, and the timetable for implementation.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour 30 Minutes',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course57()->id,
            'category_id' => 4
        ]);

        $course58 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Author GHS Safety Data Sheets - Webcast',
                'description' => "OSHA has adopted the new Globally Harmonized System for the classification and labeling of hazardous chemicals. A cornerstone of GHS is the adoption of a completely revised Safety Data Sheet. In this webcast, you will learn not only the differences between the MSDS and SDS and how to author SDSs that meet the latest OSHA standards, you will also learn how to classify your products according to the 28 GHS hazard classes and 88 categories, what must be entered in each section of the SD, essential references you can use to locate data for each section of the SDS, and how to handle trade secrets.<br><br>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><br>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4-Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course58()->id,
            'category_id' => 4
        ]);

        $course59 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Label Hazardous Chemicals Using OSHA&#8217;s New GHS Hazard Communication Standard',
                'description' => "Workplace hazard communication labels are being reinvented as OSHA adopts the new Globally Harmonized System for labeling hazardous chemicals. In this interactive webcast, you will learn the difference between workplace and supplier label, what signal words, hazard statements, precautionary statements, and pictograms must be on your labels, essential references you can use to locate required label information, and how to label products with existing HMIS, DOT, and CPSC labels. ",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2-Hour Webcast',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course59()->id,
            'category_id' => 4
        ]);

        $course60 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Comply with EPA\'s Chemical Data Reporting Rule &#8211; Webcast',
                'description' => "The Chemical Data Reporting Rule requires manufacturers and importers to report full manufacturing data and production volume data for all reportable chemical substances when site-specific production volume reaches certain thresholds. The data that EPA collects constitutes the most comprehensive source of basic screening-level, exposure-related information on chemicals available to the Agency. In August of last year, EPA issued a final Chemical Data Reporting (CDR) Rule which began requiring electronic submission of CDR data using the e-CDRweb reporting tool. The new rule modifies the reporting thresholds, updates industry classifications, and makes changes to the situations in which confidentiality may be claimed. The next CDR submission is due by June 30, 2012. In this session, you will learn: <br>" .
                                 "<ul><li>How to determine if you are subject to the CDR </li><li>Which chemicals you must report on </li>".
                                 "<li>How to report chemical-specific processing and use information </li>" .
                                 "<li>How to report imported chemicals, if they are never physically on-site </li>" .
                                 "<li>How to determine the number of workers likely to be exposed </li>" .
                                 "<li>How to complete and submit the CDR </li>" .
                                 "<li>Changes in the 2016 report that you should start preparing for now </li></ul>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><p><br></p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '2-Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course60()->id,
            'category_id' => 3
        ]);

        $course61 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Solvent Wipe Rule - Webcast',
                'description' => "<p>This new rule will provide significant new exclusions for shop towels and wipes, provided you manage them correctly. In this webcast, you will learn:</p>" .
                                 "<ul type=disc><li style='COLOR: black'>Does the rule apply to both cloth and paper wipes and rags?</li>" .
                                 "<li style='COLOR: black'>What solvents can be on the towels, and which are prohibited?</li>" .
                                 "<li style='COLOR: black'>Does the rule also apply to towels that contain characteristic hazardous waste?</li>" .
                                 "<li style='COLOR: black'>Can P or U-listed wastes be on the towels?</li>" .
                                 "<li style='COLOR: black'>How must the towels be stored on-site?</li>" .
                                 "<li style='COLOR: black'>Do they need to be tested for anything?</li>" .
                                 "<li style='COLOR: black'>How long can they be stored?</li>" .
                                 "<li style='COLOR: black'>How must the containers be marked or labeled?</li>" .
                                 "<li style='COLOR: black'>How must they be prepared for transportation?</li>" .
                                 "<li style='COLOR: black'>Where can you ship them and what are the disposal and recycling options?</li>" .
                                 "<li style='COLOR: black'>What are the documentation requirements?</li>" .
                                 "<li style='COLOR: black'>How is the new rule impacted by current state regulations?</li></ul>" .
                                 "<p><br>Attend Environmental Resource Center&#8217;s Solvent Wipe Rule webcast and get the facts you need to comply with the new rule. <br><br></p>" .
                                 "<p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course61()->id,
            'category_id' => 1
        ]);

        $course62 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Ship Hazardous Materials/Dangerous Goods by Ground and Air - Webcast',
                'description' => "If you ship hazardous materials/dangerous goods by ground and air, this course is an ideal way to meet both your Department of Transportation (49 CFR 172.704) and the International Air Transport Association (IATA DGR 1.5) training requirements. Learn how to select and fill packages, label containers, complete shipping papers, and load and unload vehicles used in the transport of hazardous materials or dangerous goods in accordance with the DOT Hazardous Materials Regulations and the IATA Dangerous Goods Regulations.<br><br><div>
                    <p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>
                    <p>Please contact <a href=mailto:aknight@ercweb.com><span style='color: #0000ff; text-decoration: underline'>Amy Knight</span></a> if you would like to arrange for an onsite presentation of this material.&nbsp;<!--?xml:namespace prefix=o ns=urn:schemas-microsoft-com:office:office /--></p></div>",
                'sku' => uniqid(),
                'price' => 479,
                'duration' => '8 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course62()->id,
            'category_id' => 2
        ]);

        $course64 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management in California Webcast',
                'description' => "If your facility generates hazardous waste, you must comply not only with the California hazardous waste regulations, but also with federal regulations that have not yet been incorporated into the DTSC rules.&nbsp;<br><br>Attend this live, interactive webcast to meet both the California and federal annual training requirements for generators of hazardous waste.&nbsp;<br><br>Topics covered include how to determine which wastes are classified as EPA and California hazardous waste, how to properly manage your wastes at accumulation points and satellite accumulation points, how to complete hazardous waste manifests, and common errors to avoid, what you must enter on land disposal notices and certifications, when you must apply for a tiered permit, source reduction requirements, how to manage universal wastes and other special wastes, and recent changes in the hazardous waste regulations that can affect your facility&#8217;s hazardous waste management program.&nbsp;<br><br>The&nbsp;<em><a href=/products/view.aspx?id=1308 target=_blank><em>Handbook for the Management of Hazardous Waste in California</em></a></em><em></em> is included as a downloadable document.&nbsp;<br><br>
                    <p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p><br><br>",
                'sku' => uniqid(),
                'price' => 599,
                'duration' => '8 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course64()->id,
            'category_id' => 1
        ]);

        $course65 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-IATA-IS-1',
                'course_mill_session_id' => 49,
                'title' => 'How to Ship Infectious Substances by Ground and Air - Online Training',
                'description' => "<p style='TEXT-AUTOSPACE: '>This convenient computer-based course will guide you through the unique requirements for the transportation of infectious substances by ground and air and gives you the skills you need to properly ship these materials.</p><p style='TEXT-AUTOSPACE: '>In this interactive course, you will meet your DOT and IATA training requirements and learn how to:</p><ul><li>Classify infectious substances for ground and air transport</li><li>Select the correct packaging systems and prepare them for shipment</li><li>Properly mark, label, and document your infectious substance shipments</li></ul><p>You can complete the training anytime when it's convenient for you. Interactive games and quizzes make the training engaging, for maximum retention of information and skills.<br><br></p><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482;&nbsp;that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174;&nbsp;Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li><li>Minimum 2GB RAM</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'><a href=https://get.adobe.com/flashplayer/>span style='color: #0066cc; text-decoration: underline;'>Adobe Flash</font></u></a>&nbsp;version 10 or higher</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 199,
                'duration' => '8 hours (self-paced)',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course65()->id,
            'category_id' => 2
        ]);

        $course66 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'EPA\'s New Exclusions for Solvent Recycling and Hazardous Secondary Materials - Webcast',
                'description' => "<p>EPA's new final rule on the definition of solid waste creates new opportunities for waste recycling outside the scope of the full hazardous waste regulations. This rule, which went into effect on July 13, 2015, streamlines the regulatory burden for wastes that are legitimately recycled.</p><p>The first of the two exclusions is an exclusion from the definition of solid waste for high-value solvents transferred from one manufacturer to another for the purpose of extending the useful life of the original solvent product by keeping the materials in commerce to reproduce a commercial grade of the original solvent product. </p><p>The second, and more wide-reaching of the two exclusions, is a revision of the existing hazardous secondary material recycling exclusion. This exclusion allows you to recycle, or send off-site for recycling, virtually any hazardous secondary material. Provided you meet the terms of the exclusion, the material will no longer be hazardous waste.</p><ul><li>Which of your materials qualify under the new exclusions</li><li>What qualifies as a hazardous secondary material</li><li>Which solvents can be remanufactured, and which cannot</li><li>What is a tolling agreement</li><li>What is legitimate recycling</li>
                    <li>Generator storage requirements</li><li>What documentation you must maintain</li>
                    <li>Requirements for off-site shipments</li><li>Training and emergency planning requirements</li><li>If it is acceptable for the recycler to be outside the US</li><p><br>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>.</p></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course66()->id,
            'category_id' => 1
        ]);

        $course67 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'EPA&#8217;s Revised Underground Storage Tank Regulations - Webcast',
                'description' => "<p>EPA has revised the federal Underground Storage Tank (UST) regulations for all facilities&#8212;including those with emergency generator tanks. The new regulations are the first major revisions to the federal UST regulations published in 1988.</p>The new rules require the use of equipment to reduce releases to the environment, as well as detect releases should they occur. Attend Environmental Resource Center&#8217;s live, 90-minute webcast to learn how to meet the UST requirements that impact your site. You will learn:<ul><li>Existing UST regulations and requirements</li><li>EPA&#8217;s approved leak detection methods</li><li>New UST requirements for:<br><table cellPadding=0><tbody><tr><td></td><td><ul>
                    <li>Secondary containment for new and replaced tanks and piping</li>
                    <li>Operator training</li><li>UST system capability for certain biofuel blends</li>
                    <li>Operation and maintenance for UST systems</li>
                    <li>Removed deferrals for emergency generator tanks, airport hydrant systems, and field constructed tanks&#8212;making these tanks fully regulated</li>
                    <li>Updating codes and practices</li></ul></td></tr></tbody></table></li><ul><p>Bring your questions to this live, interactive webcast.<br>&nbsp;<br></p><p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p></ul></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1.5 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course67()->id,
            'category_id' => 3
        ]);

        $course68 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Use the DOT Hazardous Materials Table - Webcast',
                'description' => "<p>The Department of Transportation&#8217;s Hazardous Materials Table at 49 CFR 172.101 contains ten columns and thousands of entries. Do you know how to use the table properly? Choosing the correct proper shipping name is critical to shipping of hazardous materials in accordance with the DOT regulations. Using an incorrect shipping name can lead to fines and can create a risk during transportation.<br><br>Attend Environmental Resource Center&#8217;s webcast, &#8220;How to Use the DOT Hazardous Materials Table&#8221; to learn step-by-step procedures on how to:</p><ul type=disc><li>Determine the correct basic description that must be entered on your shipping papers</li><li>Apply DOT&#8217;s hierarchy when more than one shipping name can apply</li><li>Find exceptions and special provisions that apply to your shipments </li><li>Determine the marks and labels that must be on your packages</li><li>Select authorized shipping containers</li><p><br>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1.5 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course68()->id,
            'category_id' => 3
        ]);

        $course69 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Generator Improvements Rule Webcast',
                'description' => "<div><div>In the first major modification to the hazardous waste regulations in over 10 years, EPA plans to modify and reorganize the hazardous waste generator rule. When adopted, the rule will provide greater flexibility in how hazardous waste is managed and close important gaps in the regulations.</div><div></div><div><br>Attend Environmental Resource Center&#8217;s live, online session where you will learn: <br></div><ul><li>New requirements for documenting hazardous waste determinations</li><li>Revised requirements for when and how to submit the Notification of Generator Status form to EPA</li><li>How to take advantage of the episodic generation exclusion to avoid reclassification to a larger generator status</li><li>Definitions of important new terms &#8211; &#8220;Very Small Quantity Generator&#8221; and &#8220;Central Accumulation Area&#8221;</li><li>How to mark containers, tanks, and containment buildings with new information required at central accumulation areas and satellites</li><li>New conditions under which containers can be left open at satellite accumulation areas</li><li>Updated time and volume limits for satellite accumulation areas</li><li>New documentation requirements for contingency plans and biennial reports</li><li>New requirements for shipping hazardous waste from a VSQG to another facility owned by the same organization</li><p><br>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p></ul></div>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1.5 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course69()->id,
            'category_id' => 1
        ]);

        $course70 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'EPA&#8217;s Proposed New Standards for Hazardous Waste Pharmaceuticals',
                'description' => "EPA has proposed new flexible rules at 40 CFR 266 Subpart P for the management of hazardous waste pharmaceuticals by healthcare facilities, long-term care facilities, pharmacies, retail stores, and reverse distributors. The rule is projected to prevent the flushing of more than 6,400 tons of hazardous waste pharmaceuticals annually by banning healthcare facilities from flushing hazardous waste pharmaceuticals down the sink and toilet. At this session, you will learn: <ul><li>The difference between creditable, non-creditable, and evaluated pharmaceuticals &#8211; and who must make this determination</li><li>How the new rule will impact your hazardous waste generator status </li><li>New &#8220;empty&#8221; definition for unit dose containers, syringes, and other containers&nbsp; </li><li>Conditional exemption for DEA controlled substances</li><li>Requirements for on-site storage </li><li>Waste container management and labeling </li><li>Accumulation time limits </li><li>Sewer disposal prohibition </li><li>Authorized disposal and other management options </li><li>Impact of the new rule on household collection programs </li><li>Manifest and alternative transport tracking documents </li><li>Recordkeeping requirements </li><li>Requirements for reverse distributors </li><p><br>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course70()->id,
            'category_id' => 1
        ]);    

        $course71 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Eaton IATA Webcast',
                'description' => "<p style='BACKGROUND: white; LINE-HEIGHT: 10.5pt'>Please register each attendee that will be participating in the Eaton IATA Webcast on April 15, 2016.</p><p style='BACKGROUND: white; LINE-HEIGHT: 10.5pt'>Each attendee will receive a downloadable copy of Environmental Resource Center's <i>Transportation of Dangerous Goods</i> handbook. You have the option to purchase hardcopies of our book as well as copies of the 57<sup>th</sup> <i>IATA Dangerous Goods Regulations. </i>Depending on location all book orders should be received by April 8<sup>th </sup>to guarantee on time delivery.</p><p style='BACKGROUND: white; LINE-HEIGHT: 10.5pt'>Detailed instructions on how to participate will be emailed to each attendee/facility closer to the date of training.</p><p style='BACKGROUND: white; WORD-SPACING: 0px; WIDOWS: 1; LINE-HEIGHT: 10.5pt; -webkit-text-stroke-width: 0px'>To participate in this live training, please view the technical requirements <a href=/training/webcast-requirements.aspx>here</a> and have an active internet connection.</p>",
                'sku' => uniqid(),
                'price' => 95,
                'duration' => '4 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course71()->id,
            'category_id' => 1
        ]);    

        $course72 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-1GA-2',
                'course_mill_session_id' => 73,
                'title' => 'DOT Hazardous Materials: General Awareness - Online Training',
                'description' => "<p>In this session, you&#8217;ll gain a complete understanding of the goals of the hazardous materials regulations and how they impact your shipments. You&#8217;ll learn:</p><ul><li>Who is responsible for compliance with the hazardous materials regulations</li><li>Where to find the regulations that apply to your activities and how to understand them</li><li>What can happen if you don&#8217;t follow the rules</li><li>The full scope of activities at your site that are covered by the hazardous materials regulations, such as packaging, marking, labeling, documentation, and shipping</li><li>How to determine which of your shipments are subject to the hazardous materials regulations</li><li>The meaning of essential terms that are used throughout the regulations</li><li>Which regulations apply to your shipments, and which rules don&#8217;t apply</li><li>How to classify your hazardous materials by using the DOT hazardous materials table and other essential references</li><li>How to determine the &#8220;proper shipping name&#8221; for your hazardous materials, and how this name impacts every aspect of your shipments</li></ul><p></p><p>You&#8217;ll also be introduced to the key aspects of the hazardous materials regulations that will ensure your shipments are safe: packaging, marking, labeling, placarding, shipping papers, safety, security, and recordkeeping.</p><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course72()->id,
            'category_id' => 2
        ]);    

        $course73 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-BULK-3',
                'course_mill_session_id' => 91,
                'title' => 'How to Ship Bulk Hazardous Materials by Ground - Online Training',
                'description' => "<p>The Department of Transportation has established stringent requirements for bulk shipments of hazardous materials. Learn how to prepare bulk shipments in Environmental Resource Center&#8217;s web-based training that makes the DOT rules easy to understand. In this session, you will learn how to: </p><ul type='disc'><li style='COLOR: black'>Determine which materials of the materials that you ship are classified as hazardous materials when shipped in bulk quantities </li><li style='COLOR: black'>Properly select, fill, and seal bulk packages </li><li style='COLOR: black'>Determine which marks and placards apply to your shipments </li><li style='COLOR: black'>Properly complete shipping papers </li><li style='COLOR: black'>Protect yourself from hazards and emergencies </li><li style='COLOR: black'>Comply with security requirements applicable to your shipments </li></ul><p></p><p>All personnel involved in bulk shipments of hazardous materials must receive general awareness, function-specific, safety, and security training. This interactive online training fulfills each of these requirements for anyone involved in highway or rail bulk shipments. You can complete the training anytime when it's convenient for you. Interactive exercises and quizzes make the training fun and engaging. <br><br></p><p><br>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482;&nbsp;that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174;&nbsp;Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li><li>Minimum 2GB RAM</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'><a href='https://get.adobe.com/flashplayer/'>Adobe Flash</a>&nbsp;version 10 or higher</li><li class=rvps9 style='MARGIN: 0px; TEXT-INDENT: 0px'>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 149,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course73()->id,
            'category_id' => 2
        ]);   

        $course74 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-SSA-2',
                'course_mill_session_id' => 79,
                'title' => 'DOT Hazardous Materials: Safety and Security Awareness - Online Training',
                'description' => "<p>Although hazardous materials are inherently dangerous&#8212;if handled properly they need not be hazardous to you. One of the primary goals of the hazardous materials regulations is to protect your safety and the safety of the public. And, because hazardous materials are an attractive target or terrorist tool, we must be diligent in keeping hazardous materials out of the hands of anyone who is not authorized to access them.</p><p>In this session, you will learn:</p><ul><li>How to recognize the hazards of the materials you handle, ship, or receive</li><li>Ways to prevent typical hazards that occur when handling hazardous materials</li><li>Where to get information on the appropriate personal protective equipment that you must wear when you work with or near hazardous materials</li><li>How to recognize security threats</li><li>What to do if you suspect a security threat</li><li>Security procedures that can help to ensure hazardous materials cannot be accessed by unauthorized personnel</li><li>How to determine if your site is required to implement a Hazardous Materials Security Plan</li></ul><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course74()->id,
            'category_id' => 2
        ]); 

        $course75 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-FS-B-3',
                'course_mill_session_id' => 90,
                'title' => 'DOT Hazardous Materials Function-Specific: Bulk Loading and Unloading - Online Training',
                'description' => "<p>If not loaded or unloaded properly, bulk shipments can pose a significant hazard to your site personnel and the public. This training is required for all personnel involved in loading or unloading bulk shipments of hazardous materials.</p><p>In this session, you will learn the DOT&#8217;s requirements for safe loading and unloading of hazardous materials, including:</p><ul><li>Procedures for securing the vehicle and the load</li><li>Compatibility and segregation of mixed loads</li><li>Safe loading and unloading procedures</li><li>Inspection and re-test requirements for cargo tanks</li><li>When a qualified attendant must be present and the attendant&#8217;s responsibilities</li><li>Operation of emergency control features</li><li>What procedures must be implemented to prevent releases</li><li>How and when to choose the correct placard(s)</li></ul><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course75()->id,
            'category_id' => 2
        ]); 

        $course76 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-FS-ML-2',
                'course_mill_session_id' => 75,
                'title' => 'DOT Hazardous Materials Function Specific: Marking and Labeling - Online Training',
                'description' => "<p>The marks and labels required on hazardous material packages communicate essential information to everyone who handles these materials in your supply chain, including personnel at your site, carriers, customers, emergency responders, and disposal facilities. This training is required for employees who determine what marks or labels must be placed on packages, as well as employees who place marks or labels on packages of hazardous materials. It is recommended for anyone who must understand the information on packages, such as those who receive hazardous material shipments, as well as emergency responders.</p><p>In this session, you will learn:</p><ul><li>Which of your shipments must bear marks and labels, and which are exempt</li><li>How to determine if your shipments qualify for reduced marking requirements, if they are small quantities, limited quantities, or consumer commodities</li><li>The DOT&#8217;s required marks and labels that shippers must ensure are on each package of hazardous materials</li><li>How to determine which special marks are required for shipments of hazardous substances, generic shipping names, marine pollutants, combination packages holding liquids, hazardous waste, overpacks, salvage drums, and other materials with unique requirements</li><li>What special handling labels are, and when you must apply them</li><li>How to mark and label packages that contain different chemicals or products</li><li>What subsidiary labels are, and when to use them</li></ul><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><u><font color=#0066cc>Adobe Flash</font></u></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course76()->id,
            'category_id' => 2
        ]); 

        $course77 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-FS-NB-2',
                'course_mill_session_id' => 76,
                'title' => 'DOT Hazardous Materials Function-Specific: Non-Bulk Loading and Unloading - Online Training',
                'description' => "<p>The risk of injury and environmental contamination is greatest when loading and unloading hazardous materials. And, if hazardous materials are not properly loaded, they can be released in a manner that contaminates other cargo or the environment during transportation. This training is required for personnel who load or unload hazardous materials.</p><p>In this session, you will learn:</p><ul><li>Who is responsible for ensuring the vehicle is properly loaded and unloaded</li><li>How you must secure the vehicle before loading or unloading</li><li>Procedures for securing the load to prevent it from shifting</li><li>How to protect packages from damage during transport</li><li>Procedures for safe loading and unloading</li><li>Precautions to prevent releases</li></ul><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course77()->id,
            'category_id' => 2
        ]);

        $course78 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-FS-SP-2',
                'course_mill_session_id' => 78,
                'title' => 'DOT Hazardous Materials Function-Specific: Shipping Papers - Online Training',
                'description' => "<p>Shipping papers (bills of lading or manifests) are required to accompany most shipments of hazardous materials. If you are a shipper of hazardous materials, you must ensure that your shipping papers are complete and accurate. This training is required for all personnel who complete or sign shipping papers. It is recommended for anyone who must understand the information on shipping papers, such hazardous material shipment receivers and emergency responders.</p><p>In this session, you will learn:</p><ul><li>When shipping papers are required, and what shipments need not be accompanied by shipping papers</li><li>Who is responsible for the accuracy of the shipping paper</li><li>What must be entered on the shipping paper</li><li>How to determine the entries that must be placed on shipping papers for certain materials, such as hazardous substances, marine pollutants, and generic descriptions</li><li>How you must distinguish hazardous from non-hazardous materials when they are in the same shipment</li><li>How to determine the &#8220;basic description&#8221; that must be entered for each material you ship</li><li>How to correctly identify the number of packages, weight, and volume</li><li>Emergency information that must be specified on each shipping paper</li><li>Who can certify the shipping paper and the text that must be in the certification</li><li>Recordkeeping requirements</li></ul><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course78()->id,
            'category_id' => 2
        ]);

        $course79 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-FS-P-2',
                'course_mill_session_id' => 77,
                'title' => 'DOT Hazardous Materials Function Specific: Packaging - Online Training',
                'description' => "<p>Using the correct packaging for your hazardous materials will not only ensure you are in compliance with the regulations, but you&#8217;ll also know that your shipments are safe. Selecting the correct non-bulk (&#8804; 119 gal) or bulk package might seem like a complex process, but by using the easy-to-follow procedures in this session, you&#8217;ll be correctly packing your hazardous materials in no time.</p><p>This session is required for employees who perform any of these job functions:</p><ul><li>Select or determine the correct packaging for hazardous materials</li><li>Fill hazardous material packages</li><li>Close or seal packages</li><li>Manage emptied packages</li></ul><p>In this session, you&#8217;ll learn how to:</p><ul><li>Use the hazardous materials table to identify the packaging instructions that apply to your shipments</li><li>Determine which packages are approved for your shipments</li><li>Determine if the packages are compatible with your shipments</li><li>Understand the manufacturer&#8217;s codes that are marked on hazardous material packages, and use the codes to ensure you are using the correct packages</li><li>Translate &#8220;packing groups&#8221; into the &#8220;performance standards&#8221; that apply to your packages</li><li>Determine the maximum weight for solids and specific gravity for liquids allowed in the packages you use</li><li>Obtain and follow the manufacturer&#8217;s instructions for filling and sealing packages</li><li>Ensure your packages are in good condition and meet DOT&#8217;s general packaging requirements</li><li>Find out if there are exceptions in the regulations that will allow you to use packages that don&#8217;t meet the DOT&#8217;s stringent specifications</li><li>Manage empty containers and meet the requirements for container reuse</li></ul><p><br>Technical Requirements:</p><ul><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft&#174; Internet Explorer, Google Chrome&#8482;, and Apple Safari&#8482;.)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound Card with Headphones/Speakers (All courses have voiceovers and require sound to be enabled.)</li></ul></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => 'Self Placed',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course79()->id,
            'category_id' => 2
        ]);

        $course80 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'California Hazardous Waste and DOT Hazardous Materials Update and Refresher Training',
                'description' => "This course is the perfect way to meet both your California Title 22 hazardous waste and DOT 49 CFR 172.704 (c)(2) hazardous material recurrent training requirements. During this one-day class you will learn the important concepts and procedures required for compliance with hazardous waste (DTSC/EPA/RCRA) and hazardous materials transport (DOT/HMTA) regulations. You'll also learn how to avoid the most common violations cited by EPA and DOT, as well as how to comply with new requirements that have been enacted over the past year. <br><br>Your course materials include <a href=/products/view.aspx?id=1308><i>Handbook for the Management of Hazardous Waste in California</i></a>, <a href=/products/view.aspx?id=1311><i>Hazardous Materials Management - Compliance with the DOT Requirements</i></a>, and the <i><a href=/products/view.aspx?id=1014 target=_blank>Emergency Response Guidebook</a></i>. ",
                'sku' => uniqid(),
                'price' => 549,
                'duration' => '8 Hours',
                'course_type_id' => 1,
            ]);
        };

        for ($i=1; $i<=2; $i++) {
            CourseCategory::create([
                'course_id' => $course80()->id,
                'category_id' => $i
            ]);
        }

        $course81 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'New DOT Rule on Reverse Logistics of Hazardous Materials',
                'description' => "<p>DOT has created new streamlined alternatives for the safe return (reverse logistics) of hazardous materials from retail outlets back to a distribution or reclamation facility. </p><p>Now, when retail outlets need to return hazardous products to receive manufacturer&#8217;s credit due to a recall, for replacement, recycling, or a similar reason, store employees need only comply with DOT&#8217;s new relaxed requirements for product classification, packaging, and marking. This rule also expands a previously existing exception for shipments of lead acid batteries for the purpose of recycling.</p><p>Attend this live, interactive webcast to learn:</p><ul><li>What hazardous retail products qualify for the new exceptions</li><li>Quantity limits</li><li>How to package hazardous retail products for shipments</li><li>Options for handling damaged goods</li><li>What marks and labels are required on each package</li><li>Employee training requirements</li><li>What &#8220;clear instructions&#8221; must be given to employees that prepare return shipments</li><li>Documentation and recordkeeping requirements</li></ul><p>For on-site training, or the development of clear instructions that can be used as an alternative to training, contact Environmental Resource Center at <a href=mailto:service@ercweb.com>service@ercweb.com</a>.<br><br></p><p>To participate in this live training you will need an active internet connection. You may test your connection <a href=http://www.webex.com/test-meeting.html>here</a>. To view the technical requirements click the following link: <a href=https://help.webex.com/docs/DOC-4748>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour',
                'course_type_id' => 1,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course81()->id,
            'category_id' => 2
        ]);

        $course82 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-MW-1',
                'course_mill_session_id' => 71,
                'title' => 'How to Ship Medical Waste - Online Training',
                'description' => "<p>This convenient online course will guide you through the requirements for the transportation of regulated medical waste by ground. </p><p>Satisfy your DOT training requirements and learn how to: </p><ul><li>Classify infectious substances </li><li>Determine the correct packaging and properly prepare packages for shipment </li><li>Properly mark, label, and document medical waste shipments </li></ul><p></p><p>You can complete the training anytime when it's convenient for you. Interactive activities help you learn and retain the information that you need to know. <br></p><p>Technical Requirements:</p><ul><li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li><li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li><li>Minimum 2GB RAM</li><li class=rvps9><a href=https://get.adobe.com/flashplayer/><span style='color:#0066cc; text-decoration: underline;'>Adobe Flash</span></a> version 10 or higher</li><li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => 'Self Paced',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course82()->id,
            'category_id' => 2
        ]);

        $course83 = function() {
            return Course::create([
                'course_mill_course_id' => 'RCRA-UW-1',
                'course_mill_session_id' => 92,
                'title' => 'How to Manage Universal Waste - Online Training',
                'description' => "N/A",
                'sku' => uniqid(),
                'price' => 99,
                'duration' => 'Self Paced',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course83()->id,
            'category_id' => 1
        ]);

        $course84 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management - Webcast',
                'description' => "In this interactive course with a live instructor, you will gain the knowledge you need to manage hazardous waste in accordance with the latest regulatory requirements. While meeting your 40 CFR 265.16 training requirements, you will learn how to properly classify and accumulate hazardous waste, how to ensure your waste containers meet EPA and DOT requirements, how to avoid hazardous waste manifest errors, and how you must prepare for and respond to emergencies. You will also have the opportunity to ask questions regarding your site-specific waste management procedures.<br><br><span style='color:#0000ff; text-decoration: underline;'><a href='/products/view.aspx?id=1307'><span style='color:#0000ff; text-decoration: underline;'>Handbook for the Management of Hazardous Waste</span></a></span></em>&nbsp;</em>is included as a downloadable document.<br><br><p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. <br><br>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul><li>CEU—0.8</li><li>NEHA—8 contact hours</li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours—7 contact hours<br></span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>IDEM Waste Water—8 general contact hours<br></span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>IDEM Drinking Water—8 contact hours</span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 7 Continuing Education Hours</li><li>Kentucky Drinking Water - 7 Continuing Education Hours</li></ul><p><a href='/training/ceus.aspx'><u><font color='#81008'>Click here</font></u></a> for a complete list of continuing education&nbsp;credits and approval numbers.</p>",
                'sku' => uniqid(),
                'price' => 599,
                'duration' => '8 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course84()->id,
            'category_id' => 1
        ]);

        $course85 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Management Annual Update - Webcast',
                'description' => "This live interactive web-based training is the perfect way to meet your RCRA 40 CFR 265.16 annual refresher training requirement. In addition to gaining insight on recent changes in the regulations, you will learn how to determine which wastes are hazardous, how to properly accumulate waste on-site, how to prepare hazardous waste for off-site shipment, emergency response and reporting procedures, and how to prepare hazardous waste manifests and land disposal notices.&nbsp;&nbsp;<br><br><em><em><em><em><u><font color='#0000ff'><a href='/products/  view.aspx?id=1307'><em><em><em><u><font color='#0000ff'>Handbook for the Management of Hazardous Waste</font></u></em></em></em></a></font></u></em></em></em> </em>is included as a downloadable document.<br><br><p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. </p><p>Pre-approved for the following&nbsp;Continuing Education Credits:</p><ul><li>CEU—0.4</li><li>CM Points IHMM—May be eligible for 4 CMP</li><li>IDEM Drinking Water—4 contact hours</li><li>IDEM Waste Water—4 general contact hours</li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'>Ohio EPA Contact Hours—7 contact hours</span></li><li><span class='Apple-style-span' style='LINE-HEIGHT: 14px'></span>Kentucky Wastewater - 4 Continuing Education Hours</li><li>Kentucky Drinking Water - 4 Continuing Education Hours</li></ul><p><a href='/training/ceus.aspx'><u><font color='#810081'>Click here</font></u></a> for a complete list of continuing education&nbsp;credits and approval numbers.</p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course85()->id,
            'category_id' => 1
        ]);

        $course86 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'Hazardous Waste Manifest Training - Webcast',
                'description' => "When hazardous waste is shipped from your site, it must be accompanied by a hazardous waste manifest.&nbsp; Even if your waste transporter fills out the manifest, you are responsible for the portions of the manifest that they complete on your behalf.&nbsp; There are over 35 sections on the manifest and its continuation sheet – are you sure that all of them are completed properly in accordance with EPA’s requirements?</div>
<div>&nbsp;</div>
<div>Because the manifest is a Department of Transportation (DOT) hazardous material shipping paper, anyone&nbsp;who fills out or signs the manifest must be trained and tested in the DOT’s required topics: general awareness, function-specific, safety, and security.</div>
<div>&nbsp;</div>
<div>This live online training session is designed to satisfy the training requirements for anyone&nbsp;who completes or signs hazardous waste manifests.&nbsp; You will learn:</div>
<ul>
<li>When the hazardous waste manifest must be used</li>
<li>What types of waste and which waste generators are not required to use a hazardous waste manifest</li>
<li>What must be entered in each section of the manifest, and where to find the information</li>
<li>How to determine the DOT basic description, which is critical to ensuring that the waste is handled properly</li>
<li>How to identify the applicable hazardous waste codes</li>
<li>How to handle exceptions, discrepancies, rejected shipments, and exports</li>
<li>How to certify that the waste is properly classified, packaged, marked, labeled, and placarded for shipment</li>
<li>What emergency response information must be included on or attached to the manifest</li>
<li>How to meet the safety and security requirements for hazardous waste&nbsp;&nbsp;</li></ul>
<div>&nbsp;</div>
<div>To participate in this live training you will need an active internet connection. You may test your connection&nbsp;<a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link:<a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>.",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '4 Hour Webcast',
                'course_type_id' => 2,
            ]);
        };

        for ($i=1; $i<=2; $i++) {
            CourseCategory::create([
                'course_id' => $course86()->id,
                'category_id' => $i
            ]);
        }

        $course87 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'TSCA Requirements for Fluorescent Light Ballasts - Webcast',
                'description' => "If you have fluorescent lamp fixtures containing ballasts manufactured before July 2, 1979, the ballasts will most likely contain PCBs. These ballasts are subject to TSCA storage and disposal requirements. In this webcast, you will learn the various classifications that apply to PCB ballasts, how to accumulate them on-site, prepare them for off-site shipment, and properly dispose of them.<br><br><p><font color='#0000ff'><font color='#333333'>The course handbook </font></font>is included as a downloadable document.</p><p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1 Hour',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course87()->id,
            'category_id' => 3
        ]);

        $course88 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => 'How to Manage a DOT Driver Compliance Program – Webcast',
                'description' => "Did you know that at least 9 different records are required in every driver’s qualification file, and that each of these records must be retained for a specific amount of time? Do your drivers know when they must report an illness or disclose their own personal motor vehicle violations? </p><p>Get answers to these and other challenging questions in this live online course that’s designed for anyone who manages a DOT Driver Compliance Program.</p><p>At this interactive session, you will learn how to manage your driver and motor vehicle compliance program—including:</p><ul><li>What operations and activities are covered by the FMCSA regulations?</li><li>Drug and alcohol testing</li><li>Driver qualification</li><li>Medical requirements</li><li>Motor vehicle records</li><li>Annual driver’s record of violations</li><li>Hours of service</li><li>Vehicle inspection report</li><li>Accident register</li><li>Motor vehicle numbers</li><li>Hazardous materials</li><li>Records retention</li></ul><p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. </p>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1:00 pm - 3:00 pm',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course88()->id,
            'category_id' => 2
        ]);

        $course89 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => "EPA’s Revised Underground Storage Tank Regulations - Webcast",
                'description' => "<p>EPA has revised the federal Underground Storage Tank (UST) regulations for all facilities—including those with emergency generator tanks. The new regulations are the first major revisions to the federal UST regulations published in 1988.</p>The new rules require the use of equipment to reduce releases to the environment, as well as detect releases should they occur. Attend Environmental Resource Center’s live, 90-minute webcast to learn how to meet the UST requirements that impact your site. You will learn: <ul><li>Existing UST regulations and requirements</li><li>EPA’s approved leak detection methods</li><li>New UST requirements for:<br><table cellpadding='0'><tbody><tr><td></td><td><ul><li>Secondary containment for new and replaced tanks and piping</li><li>Operator training</li><li>UST system capability for certain biofuel blends</li><li>Operation and maintenance for UST systems</li><li>Removed deferrals for emergency generator tanks, airport hydrant systems, and field constructed tanks—making these tanks fully regulated</li><li>Updating codes and practices</li></ul></td></tr></tbody></table></li><ul><p>Bring your questions to this live, interactive webcast.<br>&nbsp;<br></p><p>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. </p></ul></ul>",
                'sku' => uniqid(),
                'price' => 129,
                'duration' => '1.5 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course89()->id,
            'category_id' => 3
        ]);

        $course90 = function() {
            return Course::create([
                'course_mill_course_id' => NULL,
                'title' => "OSHA GHS Hazard Communication Standard - Webcast",
                'description' => "<p>Receive in depth training on OSHA's Hazard Communication Standard (29 CFR 1910.1200) and the United Nation's Globally Harmonized System (GHS) for the classification and labeling of hazardous chemicals. You will learn the requirements for hazardous chemical labels, Safety Data Sheets (SDSs), and written hazard communication plans. You’ll also learn how to classify hazardous chemicals, what chemicals are exempt, and how to implement an effective hazard communication program. Ensure all workers can recognize and understand the symbols and pictograms on labels, as well as the hazard statements and precautions required on labels and SDSs.<br><br>At this live interactive online training, you will learn:</p>
<ul type='disc'>
<li style='COLOR: black'>The GHS classification system</li>
<li style='COLOR: black'>How OSHA’s hazard communication system differs from GHS</li>
<li style='COLOR: black'>The meaning of GHS hazard statements, precautionary statements, signal words, and pictograms</li>
<li style='COLOR: black'>What information must be in each of the 16 SDS sections, and how to use that information to protect your health and safety</li>
<li style='COLOR: black'>The signal words, hazard and precautionary statements, and pictograms required on labels</li>
<li style='COLOR: black'>The difference between workplace and supplier labels</li>
<li style='COLOR: black'>How to label stationary process equipment</li>
<li style='COLOR: black'>How to implement hazard communication at your workplace</li>
<li style='COLOR: black'>How to avoid the most common mistakes made on product labels and SDSs</li>
<li style='COLOR: black'>What must be in your site’s written hazard communication program</li>
<li style='COLOR: black'>Worker training: who must be trained, what must be covered, and how to document</li></ul>
<p style='COLOR: black'>To participate in this live training you will need an active internet connection. You may test your connection <a href='http://www.webex.com/test-meeting.html'>here</a>. To view the technical requirements click the following link: <a href='https://help.webex.com/docs/DOC-4748'>https://help.webex.com/docs/DOC-4748</a>. <br></p>",
                'sku' => uniqid(),
                'price' => 399,
                'duration' => '3 Hours',
                'course_type_id' => 2,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course90()->id,
            'category_id' => 4
        ]);

        $course91 = function() {
            return Course::create([
                'course_mill_course_id' => 'DOT-RL-1',
                'course_mill_session_id' => 119,
                'title' => "Reverse Logistics Shipments of Hazardous Materials - Online Training",
                'description' => "<p><span style='FONT-FAMILY: Arial,sans-serif'>DOT has created new streamlined alternatives for the safe return of hazardous materials from retail outlets back to a manufacturer, supplier, or distribution facility.</span></p>" .
                                 "<p><span style='FONT-FAMILY: Arial,sans-serif'>Now, when retail outlets need to return hazardous products to receive manufacturer&#8217;s credit due to a recall, for replacement, recycling, or a similar reason, store employees need only comply with DOT&#8217;s new reverse logistics requirements. But, before you can take advantage of this new option, your employees must be trained.</span></p>" .
                                 "<p><span style='FONT-FAMILY: Arial,sans-serif'>In this online course, you will learn:</span></p>" .
                                 "<ul>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>What retail products qualify for the new exception</span></li>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>Quantity limits</span></li>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>How to package hazardous retail products for shipments</span></li>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>What marks and labels are required on each package</span></li>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>Employee training requirements</span></li>" .
                                 "<li><span style='FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>What &#8220;clear instructions&#8221; must be given to employees that prepare return shipments</span></li>" .
                                 "<li><span style=''FONT-FAMILY: Symbol'><span style='FONT-FAMILY: Arial,sans-serif'>Documentation and recordkeeping requirements</span></li></ul>" .
                                 "<p></p>" .
                                 "<p><span style='FONT-FAMILY: Arial,sans-serif'>You can complete the training anytime when it's convenient for you. Interactive exercises and quizzes help you learn and retain the information that you need to know.</span></p></span></span></span></span></span></span></span>" .
                                 "<p><br>Technical Requirements:</p>" .
                                 "<ul>" .
                                 "<li>A PC or Macintosh&#8482; that is connected to the Internet (broadband recommended)</li>" .
                                 "<li>An Internet browser (Mozilla Firefox&#8482;, Microsoft<sup>&#174;</sup> Internet Explorer, Google Chrome&#8482;, or Apple Safari&#8482;)</li>" .
                                 "<li>Minimum 2GB RAM</li>" .
                                 "<li class=rvps9><a href='https://get.adobe.com/flashplayer/'><u><font color=#0066cc>Adobe Flash</font></u></a> version 10 or higher</li>" .
                                 "<li class=rvps9>Sound card with headphones/speakers (All courses have voiceovers and require sound to be enabled.)</li></ul>",
                'sku' => uniqid(),
                'price' => 49,
                'duration' => '1.5 Hours',
                'course_type_id' => 3,
            ]);
        };

        CourseCategory::create([
            'course_id' => $course91()->id,
            'category_id' => 2
        ]);

    }

}