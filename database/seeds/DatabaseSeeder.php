<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('TopicsTableSeeder');
        $this->call('CategoryTableSeeder');
        $this->call('BooksTableSeeder');
        $this->call('CourseTypeTableSeeder');
        $this->call('CourseTableSeeder');
        $this->call('StateTableSeeder');
        $this->call('ZipCodeTableSeeder');
        $this->call('InstructorSeeder');
        $this->call('LocationsSeeder');
        $this->call('ClassTableSeeder');
        $this->call('RoleTableSeeder');
        $this->call('UserAccountSeeder');
        $this->call('CouponsTableSeeder');
        $this->call('CouponCoursesTableSeeder');
        $this->call('CorpYearlySeeder');
        $this->call('EmailTemplates');
        $this->call('CmsSeeder');
        // $this->call('EmailTemplateApplicationSeeder');
        // $this->call('CourseOrderSeeder');

    }
}
