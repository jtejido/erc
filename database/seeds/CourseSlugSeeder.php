<?php

use Illuminate\Database\Seeder;

class CourseSlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = \App\Models\Course::all();

        foreach ($courses as $course) {
            $slug = str_slug($course->title);
            $slug = $this->checkIfHasDuplicate($slug, 1);
            $course->update([
                'slug'   => $slug
            ]);
        }
    }

    private function checkIfHasDuplicate($slug, $ctr) {

        if(\App\Models\Course::where('slug',$slug)->count() == 0) {
            return $slug;
        }

        $slug = $slug . '-' . $ctr;

        return $this->checkIfHasDuplicate($slug, $ctr+1);
    }
}
