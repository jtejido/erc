<?php

use Illuminate\Database\Seeder;
use App\Utilities\Constant;

class PaymentItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentTransactions = \App\Models\PaymentTransaction::all();

        if ($paymentTransactions->count()) {
            foreach ($paymentTransactions as $paymentTransaction) {
                $order = $paymentTransaction->order;
                $paymentTransaction->items = json_encode($this->createPaymentItems($order->items,  $paymentTransaction->amount));
                $paymentTransaction->save();

                echo ".";
            }
        }

        echo "\nDone.";
    }

    /**
     * @param $items
     * @param $totalAmount
     * @return object
     */
    private function createPaymentItems($items, $totalAmount)
    {
        $data = (object) [];
        $itemObjects = [];

        $data->total_amount = $totalAmount;

        if ($items->count()) {
            foreach ($items as $item) {
                $itemObjects[] = (object) [
                    'id' => $item->orderable_id,
                    'object' => $item->orderable_type,
                    'contents' => $this->getItemsFromOrderItems($item->orderable)
                ];
            }

            $data->items = $itemObjects;
        }

        return $data;
    }

    /**
     * @param $orderable
     * @return array
     */
    private function getItemsFromOrderItems($orderable)
    {
        $content = [];

        switch (class_basename($orderable)) {
            case Constant::REGISTRATION_ORDER:

                foreach ($orderable->classRegistrations as $classRegistration) {
                    $content[] = (object) [
                        'id' => $classRegistration->id,
                        'price_paid' => $classRegistration->paid_charge,
                        'name' => $classRegistration->contact->name
                    ];
                }

                break;

            case Constant::YEARLY_SUBSCRIPTION:

                foreach ($orderable->clientSubscriptions as $clientSubscription) {
                    $content[] = (object) [
                        'id' => $clientSubscription->id,
                        'price_paid' => $clientSubscription->paid_price,
                        'name' => $clientSubscription->agent->contact->name
                    ];
                }
                break;

            case Constant::CORPORATE_SEAT:
                $content[] = (object) [
                    'id' => $orderable->id,
                    'price_paid' => $orderable->paid_price,
                    'seats' => $orderable->credits
                ];
                break;

            case Constant::PRODUCT_ORDERS:
                $content[] = (object) [
                    'id' => $orderable->id,
                    'price_paid' => $orderable->paid_price,
                    'quantity' => $orderable->quantity,
                    'title' => $orderable->title()
                ];

                break;
        }

        return $content;
    }
}
