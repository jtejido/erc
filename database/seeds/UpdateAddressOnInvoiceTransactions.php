<?php

use Illuminate\Database\Seeder;

class UpdateAddressOnInvoiceTransactions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\InvoicedPayment::all() as $trans) {
            $order = $trans->order;

            if(!$order) continue;

            $trans->update([
                'billing_company'           => $order->billing_company,
                'billing_address'           => $order->billing_address,
                'billing_city'              => $order->billing_city,
                'billing_state'             => $order->billing_state,
                'billing_zip'               => $order->billing_zip,
                'shipping_company'           => $order->shipping_company,
                'shipping_address'           => $order->shipping_address,
                'shipping_city'              => $order->shipping_city,
                'shipping_state'             => $order->shipping_state,
                'shipping_zip'               => $order->shipping_zip,
                'trans_total'               => $order->total,
                'shipping_rate'               => $order->shipping_rates,
            ]);

        }

    }
}
