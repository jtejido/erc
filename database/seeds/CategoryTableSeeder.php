<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder {

    /**
     * Run the database seed
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Hazardous Waste Management'],
            ['name' => 'Hazardous Materials Transportation'],
            ['name' => 'Environmental'],
            ['name' => 'Safety']
        ];

        Category::insert($categories);
    }
}