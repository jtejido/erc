<?php

use Illuminate\Database\Seeder;

class AuthorizeTransIdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen('database/seeds/authorize-filtered.log','r') or die ('File opening failed');
        $requestsCount = 0;
        $num404 = 0;

        while (!feof($handle)) {
            $dd = fgets($handle);
            $requestsCount++;

            $dd = trim($dd);

            if(!$dd) continue;

            // response code
            $pattern = "/(?<=<resultCode>)(.*?)(?=<\/resultCode>*)/";
            preg_match($pattern, $dd, $matches);
            $response_code = $matches[0];

            if($response_code != 'Ok') continue;

            $pattern = "/(?<=<authCode>)(.*?)(?=<\/authCode>*)/";
            preg_match($pattern, $dd, $matches);

            if(count($matches) == 0) continue;

            $code = $matches[0];

            $pattern = "/(?<=<transId>)(.*?)(?=<\/transId>*)/";
            preg_match($pattern, $dd, $matches);
            $transId = $matches[0];

            $pattern = "/(?<=<accountType>)(.*?)(?=<\/accountType>*)/";
            preg_match($pattern, $dd, $matches);
            $card = $matches[0];

            \App\Models\PaymentTransaction::where('auth_code', $code)->update([
                'transaction_id'    => $transId,
                'card_brand'        => $card
            ]);

        }


        fclose($handle);

    }
}
