<?php

use App\Utilities\Constant;
use Illuminate\Database\Seeder;

class UpdatePaymentItems extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\PaymentTransaction::all() as $trans) {

            $order = $trans->order;

            $trans->update([
                'shipping_rates' => ($order) ? $order->shipping_rates : null
            ]);

        }
    }

}
