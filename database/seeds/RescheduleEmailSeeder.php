<?php

use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use Illuminate\Database\Seeder;

class RescheduleEmailSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->dropEmailTemplate('class-reschedule');

        $emailTemplate = EmailTemplate::firstOrCreate([
            'slug' => 'class-reschedule',
            'description' => 'Class Registration Rescheduled'
        ]);

        $eContent = new EmailTemplateContent();
        $eContent->email_template_id = $emailTemplate->id;
        $eContent->content = view('emails.templates.classes.class_rescheduled')->render();
        $eContent->save();

    }

    private function dropEmailTemplate($slug)
    {
        $template = EmailTemplate::where('slug', $slug)->first();

        if(!$template) return ;

        EmailTemplateContent::where('email_template_id', $template->id)->delete();

        $template->delete();

    }

}