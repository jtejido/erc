<?php

use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Utilities\Constant;

class RoleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::firstOrCreate([
            'role' => Constant::ROLE_ADMIN
        ]);

        Role::firstOrCreate([
            'role' => Constant::ROLE_MEMBER
        ]);

        Role::firstOrCreate([
            'role' => Constant::ROLE_CSR
        ]);
    }

}
