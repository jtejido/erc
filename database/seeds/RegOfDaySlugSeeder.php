<?php

use Illuminate\Database\Seeder;

class RegOfDaySlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tips = \App\Models\Regulation::all();

        foreach ($tips as $tip) {
            $slug = str_slug($tip->title);
            $slug = $this->checkIfHasDuplicate($slug, 1);
            $tip->update([
                'slug'   => $slug
            ]);
        }
    }

    private function checkIfHasDuplicate($slug, $ctr) {

        if(\App\Models\Regulation::where('slug',$slug)->count() == 0) {
            return $slug;
        }

        $slug = $slug . '-' . $ctr;

        return $this->checkIfHasDuplicate($slug, $ctr+1);
    }
}
