<?php

use Illuminate\Database\Seeder;

class BookSlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = \App\Models\Book::all();

        foreach ($books as $book) {
            $slug = str_slug($book->name);
            $slug = $this->checkIfHasDuplicate($slug, 1);
            $book->update([
                'slug'   => $slug
            ]);
        }
    }

    private function checkIfHasDuplicate($slug, $ctr) {

        if(\App\Models\Book::where('slug',$slug)->count() == 0) {
            return $slug;
        }

        $slug = $slug . '-' . $ctr;

        return $this->checkIfHasDuplicate($slug, $ctr+1);
    }
}
