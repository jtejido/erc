<?php

use App\Models\GeneralLocation;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_locations')->delete();
        $locations = [
            ['location' => 'Raleigh, North Carolina'],
            ['location' => 'New York Downtown'],
            ['location' => 'IL, Chicago'],
            ['location' => 'Redding, California']
        ];

        GeneralLocation::insert($locations);
    }
}
