<?php

use App\Models\Location;
use Illuminate\Database\Seeder;
use App\Models\CourseClass;
use App\Models\CourseType;
use App\Models\Course;
use App\Models\CourseCategory;
use Carbon\Carbon;
use App\Utilities\Constant;
use Faker\Factory as FakerFactory;
use Maatwebsite\Excel\Facades\Excel;

class ClassTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $hours = function($hours = null) {
//            $start_hour = rand(7,10);
//            $num_hours = rand(8,10);
//            if ($hours) {
//                $num_hours = $hours;
//            }
//
//            $start = Carbon::now()->addWeeks(rand(2,4))->hour(rand(7,10))->minute(0)->second(0);
//            $end   = $start->copy()->addHours($num_hours);
//
//            return compact('start','end');
//        };
//
//        $days = function($_days = null) use ($hours) {
//            // multiple days
//            //
//            $days  = rand(1,3);
//            if ($_days) {
//                $days = $_days;
//            }
//
//            $duration = $hours();
//
//            $start = $duration['start'];
//            $end   = $duration['end']->addDays($days-1);
//
//            return compact('start','end');
//        };
//
//        $dateify = function($course, $class) use ($hours, $days) {
//            $span = $course->span;
//
//            $schedule = null;
//            if ($span) {
//                if ($span['days'] > 0) {
//                    $schedule = $days($span['days']);
//                }
//
//                if ($span['hours'] > 0) {
//                    $schedule = $days($span['hours']);
//                }
//            }
//
//            if ($schedule) {
//                $class['start_date'] = $schedule['start']->toDateString();
//                $class['start_time'] = $schedule['start']->toTimeString();
//
//                $class['end_date'] = $schedule['end']->toDateString();
//                $class['end_time'] = $schedule['end']->toTimeString();
//
//                return $class;
//            }
//        };
//
//        $notCbt = CourseType::whereIn('id', [Constant::SEMINAR, Constant::WEBCAST])->lists('id')->toArray();
//        $courses = Course::whereIn('course_type_id', [1, 2])->get();
//        $locations = Location::all()->pluck('id')->toArray();
//        $instructors = \App\Models\Instructor::all()->pluck('id')->toArray();
//
//        $courses->each(function($course) use ($dateify, $locations, $instructors) {
//            $getClass = function()  {
//                $faker = FakerFactory::create();
//
//                return [
//                    'code'            => uniqid(),
//                    'address'         => $faker->streetName,
//                    'city'            => $faker->city,
//                    'state'           => $faker->state,
//                    'zip'             => $faker->postcode,
//                    'website'         => $faker->url,
//                    'instructions'    => $faker->paragraph()
//                ];
//            };
//
//
//            $numClasses = collect(range(1, rand(2, 4)));
//
//            $numClasses->each(function($numClass) use ($course, $getClass, $dateify, $locations, $instructors) {
//                $faker = FakerFactory::create();
//                $class = $getClass();
//                $class = $dateify($course, $class);
//                $class['course_id'] = $course->id;
//                if($course->isSeminar())
//                $class['location_id'] = $faker->randomElement($locations);
//                $class['instructor_id'] = $faker->randomElement($instructors);
//                CourseClass::create($class)->save();
//            });
//        });


        // Seminars

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load('public/imports/seminars.xlsx')
            ->chunk(100, function($results) use (&$existingEmployeeCount)  {
                foreach($results as $row) {
                    echo ".";

                    $course = Course::where('title', trim($row->name))->first();
                    $location = Location::where('address', trim($row->address))->first();

                    if (!$course) {
                        $course = Course::create([
                            'title' => $row->name,
                            'description' => $row->description,
                            'sku' => $row->code,
                            'course_type_id' => 1,
                            'price' => $row->price,
                            'duration' => $row->duration
                        ]);

                        CourseCategory::create([
                            'course_id' => $course->id,
                            'category_id' => $row->category_id
                        ]);
                    }

                    CourseClass::create([
                        'course_id' => $course->id,
                        'code' => $row->code,
                        'start_date' => $row->start_date,
                        'end_date' => $row->end_date,
                        'start_time' => $row->start_time,
                        'end_time' => $row->start_time,
                        'location_id' => $location ? $location->id : null,
                        'website' => $row->url
                    ]);
                }
            });

        //WebCasts

        Excel::filter('chunk')
            ->selectSheetsByIndex(0)
            ->load('public/imports/webcasts.xlsx')
            ->chunk(100, function($results) use (&$existingEmployeeCount)  {
                foreach($results as $row) {
                    echo ".";

                    $course = Course::where('title', trim($row->name))->first();
                    $location = Location::where('address', trim($row->address))->first();

                    if (!$course) {
                        $course = Course::create([
                            'title' => $row->name,
                            'description' => $row->description,
                            'sku' => $row->code,
                            'course_type_id' => 2,
                            'price' => $row->price,
                            'duration' => $row->duration
                        ]);

                        CourseCategory::create([
                            'course_id' => $course->id,
                            'category_id' => $row->category_id
                        ]);
                    }

                    CourseClass::create([
                        'course_id' => $course->id,
                        'code' => $row->code,
                        'start_date' => $row->start_date,
                        'end_date' => $row->end_date,
                        'start_time' => $row->start_time,
                        'end_time' => $row->start_time,
                        'location_id' => $location ? $location->id : null,
                        'website' => $row->url
                    ]);
                }
            });

        echo "\n";
    }
}