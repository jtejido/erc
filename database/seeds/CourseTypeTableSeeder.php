<?php

use Illuminate\Database\Seeder;
use App\Models\CourseType;

class CourseTypeTableSeeder extends Seeder {

    /**
     * Run the database seed
     *
     * @return void
     */
    public function run()
    {
        $courseTypes = [
            ['name' => 'Seminar'],
            ['name' => 'WebCasts'],
            ['name' => 'Online Training']
        ];

        CourseType::insert($courseTypes);
    }
}