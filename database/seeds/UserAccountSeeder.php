<?php

use App\Models\User;
use App\Models\Role;
use App\Models\Contact;
use App\Models\AddressBook;
use App\Utilities\Constant;
use Illuminate\Database\Seeder;
use Faker\Factory as FakerFactory;

class UserAccountSeeder extends Seeder {

    /**
     * Run the database seed
     *
     * @return void
     */
    public function run()
    {

        $contact = function() {
            $faker = FakerFactory::create();
            return Contact::create([
                'first_name' => $faker->firstName,
                'last_name'  => $faker->lastName,
                'company'    => $faker->company,
                'title'      => $faker->sentence(),
                'address1'   => $faker->streetAddress,
                'address2'   => $faker->streetSuffix,
                'zip'        => $faker->postcode,
                'city'       => $faker->city,
                'state'      => $faker->state,
            ]);
        };

        $this->createAccount('erc_admin@ercweb.com', 'ercadm1n', Constant::ROLE_ADMIN, $contact());
    }

    public function createAccount($email, $password, $role, $contact = null) {
        $user = User::whereEmail($email)->first();

        if (!$user) {
            $password = bcrypt($password);
            $contact_id = $contact ? $contact->id: null;
            $state = ($role === Constant::ROLE_MEMBER)?: 1;
            $status = 'active';

            $user = User::create(compact('email','password','contact_id','state','status'));
            $role = Role::where('role', '=', $role)->first();

            $user->assignRole($role);
        }

        return $user;
    }
}