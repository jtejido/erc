<?php

use Illuminate\Database\Seeder;

use App\Utilities\Constant;

class CouponCoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $discountableTypes = [Constant::COURSE_OBJECT, Constant::COURSE_CLASS_OBJECT];

        for ($i = 1; $i <= 20; $i++) {
            $k = array_rand($discountableTypes);
            $discountableType = $discountableTypes[$k];

            DB::table('coupon_courses')->insert([
                [
                    'coupon_id' => $i,
                    'discountable_id'   => 1,
                    'discountable_type' => $discountableType,
                    'created_at'    => new \DateTime('now'),
                    'updated_at'    => new \DateTime('now')
                ]
            ]);

        }
    }
}
