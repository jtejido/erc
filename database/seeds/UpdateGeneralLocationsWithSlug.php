<?php

use Illuminate\Database\Seeder;

class UpdateGeneralLocationsWithSlug extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = \App\Models\GeneralLocation::all();
        foreach ($locations as $location) {
            $location->update([
               'slug'   => str_slug($location->location)
            ]);
        }
    }
}
