<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FirstTimeLogin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('id', '>', '1')->update(['first_login' => 1]);
    }
}
