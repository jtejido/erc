<?php

use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use Illuminate\Database\Seeder;

class OrderEmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->dropEmailTemplate('order-confirmation');

        $emailTemplate = EmailTemplate::firstOrCreate([
            'slug' => 'order-confirmation',
            'description' => 'Order Confirmation'
        ]);

        $eContent = new EmailTemplateContent();
        $eContent->email_template_id = $emailTemplate->id;
        $eContent->content = view('emails.templates.order_confirmation')->render();
        $eContent->save();

    }

    private function dropEmailTemplate($slug)
    {
        $template = EmailTemplate::where('slug', $slug)->first();

        if(!$template) return ;

        EmailTemplateContent::where('email_template_id', $template->id)->delete();

        $template->delete();

    }

}
