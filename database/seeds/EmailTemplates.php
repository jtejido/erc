<?php

use App\Models\EmailTemplate;
use App\Models\EmailTemplateContent;
use Illuminate\Database\Seeder;

class EmailTemplates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('email_template_contents')->truncate();
        DB::table('email_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $build = function($file) {
            $templates = base_path('resources/views/emails/templates/').'/';
            $css       = file_get_contents($templates.'zurb-foundation/foundation-live.css');
            $content   = file_get_contents($templates.$file);

            $css       = "<style>{$css}.</style>";
            $content   = str_replace('<!-- <style> -->', $css, $content);
            $email_tag = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';

            $content   = $email_tag."\n".$content;
            return $content;
        };

        // TODO - use blade template instead of html e.g. ua-register for all 29 commented out emails below

        $content_mapping = [
            'ua-register'                               => 'emails.templates.customer.new-account',
            'reg-seminars-confirmation-attendee'        => 'emails.templates.classes.attendee.registration',
            'ua-forgot-password'                        => 'emails.templates.customer.forgot-password',
            'ua-profile-update'                         => 'emails.templates.customer.profile-update',
            'reg-onsite-confirmation'                   => 'emails.templates.classes.onsite-registration',
            'reg-cancellation'                          => 'emails.templates.classes.cancellation',
            'ua-contact-add'                            => 'emails.templates.contacts.add',
            'ua-activation-request'                     => 'emails.templates.customer.activation-request',
            'attendance-confirmation'                   => 'emails.templates.classes.attendee.attendance-confirmation',
            'course-update'                             => 'emails.templates.classes.attendee.course-update',
            'course-mill-account-create'                => 'emails.templates.customer.course-mill-new-account',
            'webcast-instruction'                       => 'emails.templates.reminder.webcast-instruction',
            'cert-reminder'                             => 'emails.templates.reminder.cert-reminder',
            'notify-swap-agent'                         => 'emails.templates.completed-registration.agent.swap',
            'reg-training-swap-user'                    => 'emails.templates.classes.attendee.swap',
            'class-registration-cancel-agent'           => 'emails.templates.completed-registration.agent.class-registration-cancel',
            'order-modification-confirmed-agent'        => 'emails.templates.completed-registration.agent.confirmed-order-modification',
            'reg-onsite-update'                         => 'emails.templates.classes.onsite-update',
            'subscription-used'                         => 'emails.templates.classes.subscription-used',
            'yearly-subscription-purchase'              => 'emails.templates.customer.yearly-subscription',
            'corp-seat-subscription-purchase'           => 'emails.templates.customer.corporate-seat-subscription',
            'course-material-added'                     => 'emails.templates.customer.course-material-added',

        ];

        $templates = collect([
            ['slug'        => 'ua-register',
             'description' => 'User Registration'],
            ['slug'        => 'ua-forgot-password',
             'description' => 'Forgot Password'],
            ['slug'        => 'ua-profile-update',
             'description' => 'Profile Update'],
            ['slug'        => 'reg-seminars-confirmation-attendee',
             'description' => 'Confirmation - Attendee'],
            ['slug'        => 'reg-onsite-confirmation',
             'description' => 'Confirmation - Onsite'],
             ['slug'        => 'reg-cancellation',
             'description' => 'Training Cancellation'],
             ['slug'        => 'ua-contact-add',
                'description' => 'Confirmation - Address Book'],
            ['slug'        => 'ua-activation-request',
                'description' => 'Request re-activation of user'],
            ['slug'        => 'attendance-confirmation',
                'description' => 'Attendance Confirmation'],
            ['slug'        => 'course-update',
                'description' => 'Course Update'],
            ['slug'        => 'course-mill-account-create',
                'description' => 'CourseMill Account Created'],
            ['slug'        => 'webcast-instruction',
                'description' => 'Webcast Instruction'],
            ['slug'        => 'cert-reminder',
                'description' => 'Certificate Reminder'],
            ['slug'        => 'notify-swap-agent',
                'description' => 'Attendees Swapped'],
            ['slug'         => 'reg-training-swap-user',
                    'description'   => 'Swap - Student Update'],
            ['slug'        => 'class-registration-cancel-agent',
                'description' => 'Class Registration Cancelled'],
            ['slug'        => 'order-modification-confirmed-agent',
                'description' => 'Confirmed Order Modification'],
            ['slug'        => 'reg-onsite-update',
                'description' => 'Update - Onsite'],
            ['slug'        => 'subscription-used',
                'description' => 'Subscription Used'],
            ['slug'        => 'yearly-subscription-purchase',
                'description' => 'Yearly Subscription Purchase'],
            ['slug'        => 'corp-seat-subscription-purchase',
                'description' => 'Corporate Subscription Purchase'],
            ['slug'        => 'course-material-added',
                'description' => 'Course Material Added'],
        ]);

        $templates->each(function($template) use ($content_mapping, $build) {
            $template = EmailTemplate::firstOrCreate($template);
            if (array_key_exists($template->slug, $content_mapping)) {
                $content = view($content_mapping[$template->slug])->render();
                $template->contents()->save(new EmailTemplateContent([
                    'content' => $content
                ]));
            }
        });

    }
}
