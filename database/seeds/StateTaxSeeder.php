<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateTaxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {
            $handle = fopen('database/seeds/nc_state_tax_2017.csv','r') or die ('File opening failed');
            $zip_codes = [];
            while (!feof($handle)) {
                $dd = fgets($handle);

                $dd = trim($dd);

                if(!$dd) continue;

                $str_arr = explode(',', $dd);

                $state_taxes[] = [
                    'state_abbrev'      => 'NC',
                    'county'            => $str_arr[0],
                    'state_rate'        => $str_arr[1],
                    'county_rate'       => $str_arr[2],
                    'transit_rate'      => $str_arr[3],
                ];

            }
            fclose($handle);

            DB::table('state_tax')->truncate();

            DB::table('state_tax')->insert($state_taxes);

        } catch (\Exception $e) {
            DB::rollback();
            echo 'Error Encountered, rolling back data.';
            return ;
        }

        DB::commit();
        echo 'Success.';
    }
}
