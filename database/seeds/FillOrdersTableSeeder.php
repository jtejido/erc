<?php

use App\Models\Order;
use Illuminate\Database\Seeder;

class FillOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Order::all() as $order) {
            if(empty(trim($order->billing_zip))) {
                $order->update([
                    'billing_zip'   => $order->user->contact->zip
                ]);
            }
            if(empty(trim($order->billing_company))) {
                $order->update([
                    'billing_company'   => $order->user->contact->company
                ]);
            }
            if(empty(trim($order->billing_address))) {
                $order->update([
                    'billing_address'   => $order->user->contact->address1
                ]);
            }

            if(empty(trim($order->shipping_zip))) {
                $order->update([
                    'shipping_zip'   => $order->user->contact->zip
                ]);
            }
            if(empty(trim($order->shipping_company))) {
                $order->update([
                    'shipping_company'   => $order->user->contact->company
                ]);
            }
            if(empty(trim($order->shipping_address))) {
                $order->update([
                    'shipping_address'   => $order->user->contact->address1
                ]);
            }

        }

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s";
        $api_key = "AIzaSyBGFI4qchVQgtLUV-t75-utkVnPhl7V1D8";


        foreach (Order::all() as $order) {
            if($order->billing_zip == $order->user->contact->zip) {
                if(empty(trim($order->billing_state))) {
                    $order->update([
                        'billing_state'   => $order->user->contact->state
                    ]);
                }
                if(empty(trim($order->billing_city))) {
                    $order->update([
                        'billing_city'   => $order->user->contact->city
                    ]);
                }
            } else {
                $get_string = sprintf($url, '00501', $api_key);

                $client = new \GuzzleHttp\Client();
                $res = $client->request('GET', $get_string);
                if($res->getStatusCode() == 200) {
                    $body = json_decode($res->getBody());
                    foreach($body->results[0]->address_components as $elem) {
                        if(in_array('administrative_area_level_1', $elem->types)) {
                            $state = $elem->long_name;
                            $order->update([
                                'billing_state'   => $state
                            ]);
                        }
                        if(in_array('locality', $elem->types)) {
                            $city = $elem->long_name;
                            $order->update([
                                'billing_city'   => $city
                            ]);
                        }
                    }
                }
            }
            if($order->shipping_zip == $order->user->contact->zip) {
                if(empty(trim($order->shipping_state))) {
                    $order->update([
                        'shipping_state'   => $order->user->contact->state
                    ]);
                }
                if(empty(trim($order->shipping_city))) {
                    $order->update([
                        'shipping_city'   => $order->user->contact->city
                    ]);
                }
            } else {
                $get_string = sprintf($url, '00501', $api_key);

                $client = new \GuzzleHttp\Client();
                $res = $client->request('GET', $get_string);
                if($res->getStatusCode() == 200) {
                    $body = json_decode($res->getBody());
                    foreach($body->results[0]->address_components as $elem) {
                        if(in_array('administrative_area_level_1', $elem->types)) {
                            $state = $elem->long_name;
                            $order->update([
                                'billing_state'   => $state
                            ]);
                        }
                        if(in_array('locality', $elem->types)) {
                            $city = $elem->long_name;
                            $order->update([
                                'billing_city'   => $city
                            ]);
                        }
                    }
                }
            }
        }
    }
}
