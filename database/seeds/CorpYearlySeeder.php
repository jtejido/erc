<?php

use Illuminate\Database\Seeder;
use App\Models\YearlySubscriptionType;
use App\Models\CorporateSeat;
use App\Utilities\Constant;
use Carbon\Carbon;

class CorpYearlySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $yearlySubscriptionTypes = [

            [
                'name'          => Constant::WEBCASTS_SEMINARS,
                'price'         => 2700,
                'created_at'    => Carbon::now()
            ],

            [
                'name'          => Constant::WEBCASTS,
                'price'         => 1629,
                'created_at'    => Carbon::now()
            ]
        ];

        $corporateSeats = [


            [
                'credit_number'          => 10,
                'price'                  => 3750,
                'created_at'             => Carbon::now()
            ],

            [
                'credit_number'          => 20,
                'price'                  => 7000,
                'created_at'             => Carbon::now()
            ],

            [
                'credit_number'          => 30,
                'price'                  => 9750,
                'created_at'             => Carbon::now()
            ],

            [
                'credit_number'          => 40,
                'price'                  => 12000,
                'created_at'             => Carbon::now()
            ],

            [
                'credit_number'          => 50,
                'price'                  => 13750,
                'created_at'             => Carbon::now()
            ]
        ];
        
        foreach ($yearlySubscriptionTypes as $yearlySubscriptionType) {
            YearlySubscriptionType::create($yearlySubscriptionType)->save();
        }
        
        foreach($corporateSeats as $corporateSeat) {
            CorporateSeat::create($corporateSeat)->save();
        }
        
    }
}