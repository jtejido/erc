<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Stream;
use Prewk\XmlStringStreamer\Parser;
use App\Models\Tip;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $memLimit = ini_get('memory_limit');

        $tips = Tip::all();

        if ($tips->count()) {
            $tips->each(function($tip) {
                $tip->delete();
            });
        }

        ini_set('memory_limit', '500M');

        $streamer = XmlStringStreamer::createStringWalkerParser("public/imports/tipsoftheweek.xml");

        while ($node = $streamer->getNode()) {
            $simpleXmlNode = simplexml_load_string($node);

            Tip::create([
                'category_id' => (string) $simpleXmlNode->category_id,
                'title' => (string) $simpleXmlNode->title,
                'content' => (string) $simpleXmlNode->content,
                'published_date' => \Carbon\Carbon::parse((string) $simpleXmlNode->published_date)->format('Y-m-d'),
                'is_published' => ((int) $simpleXmlNode->is_published == 1) ? true : false
            ]);

            echo ".";
        }

        ini_set('memory_limit', $memLimit);
        echo "\n\nContent Seeded.";
    }
}
