<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentTransaction;

class PaymentTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentTransactions = PaymentTransaction::all();

        if ($paymentTransactions->count()) {
            $paymentTransactions->each(function($paymentTransaction) {
                $maskedCC = $paymentTransaction->masked_cc;

                if (strlen($maskedCC) > 12) {
                    $paymentTransaction->masked_cc = substr($maskedCC, 4);
                    $paymentTransaction->save();
                }
            });

            echo "\nDone.";
        }
    }
}
