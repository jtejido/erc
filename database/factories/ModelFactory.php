<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'email'    => $faker->email,
        'password' => bcrypt(str_random(10)),
        'contact_id' => factory(App\Models\Contact::class)->create()->id,
        'state'    => 1,
        'status'    => 'active'
    ];
});

$factory->define(App\Models\Contact::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'company'    => $faker->company,
        'title'      => $faker->sentence(),
        'address1'   => $faker->streetName,
        'address2'   => $faker->streetSuffix,
        'zip'        => $faker->postcode,
        'city'       => $faker->city,
    ];
});

$factory->defineAs(App\Models\Contact::class, 'email', function (Faker\Generator $faker) use ($factory) {
    $contact = $factory->raw(App\Models\Contact::class);

    return array_merge($contact, ['email' => $faker->email]);
});

$factory->define(App\Models\Book::class, function (Faker\Generator $faker) {
    return [
        'code'                  => $faker->ean8,
        'name'                  => $faker->sentence(4),
        'description'           => $faker->paragraph(),
        'price'                 => $faker->randomFloat(2, 0, 1000),
        'weight'                => $faker->randomFloat(1, 0, 1000),
    ];
});

$factory->define(App\Models\Location::class, function (Faker\Generator $faker) {
    return [
        'location_name'             => $faker->secondaryAddress . ' ' . $faker->company,
        'address'                   => $faker->streetAddress,
        'state'                     => $faker->state,
        'city'                      => $faker->cityPrefix,
        'zip'                       => $faker->postcode,
        'phone_number'              => $faker->tollFreePhoneNumber,
        'url'                       => $faker->url,
        'description'               => $faker->paragraph,
    ];
});

$factory->define(App\Models\Topic::class, function (Faker\Generator $faker) {
    return [
        'name'             => $faker->word,
        'description'                       => $faker->paragraph,
    ];
});